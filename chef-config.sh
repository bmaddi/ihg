ENV=Integration

echo $ENV
REPO=/home/jenkins/databags

DATABAG=concerto-fdk-ui-solution

JSON=$ENV.json

mkdir -p $REPO/$DATABAG
cd $REPO/$DATABAG

ENVIRONMENT=$ENV

getCurAppVersion()
{
  knife data bag show $DATABAG $ENVIRONMENT 2>&1 >/dev/null
  RETVAL=$?
  if [ "$RETVAL" != "0" ] ; then
     echo "Problem encountered with databag or chef-server not available"
     exit $RETVAL
  fi
}
updateAppVersion()
{
  pwd
cat << EOF > $REPO/$DATABAG/$JSON
{
  "id": "$ENVIRONMENT",
  "ui_deployer_url": "http://artifactory.ihgext.global/artifactory/concerto-snapshot-local/fdk-1.0.0-SNAPSHOT.tar"
  }
EOF
knife data bag from file $DATABAG $REPO/$DATABAG/$JSON
RC=$?
if [ $RC -ne "0" ] ;  then
        echo "knife update failed, aborting job"
        exit $RC
fi
}
showDataBag()
{
  set +x
  echo -e "\n******************************************************************"
  knife data bag show $DATABAG $ENVIRONMENT
  echo -e "******************************************************************\n"
  set -x
}
echo "*** Checking versions ****"
CUR_VERSION=$(getCurAppVersion)
updateAppVersion
knife data bag from file $DATABAG $JSON
showDataBag
