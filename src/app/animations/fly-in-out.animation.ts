import {
  state,
  animate,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const flyInOut = trigger('flyInOut', [
  state('in', style({ transform: 'translateX(0)' })),
  transition('void => *', [
    style({ transform: 'translateX(100%)' }),
    animate('300ms 10ms cubic-bezier(0.4, 0.0, 1, 1)')
  ]),
  transition('* => void', [
    animate('300ms 10ms cubic-bezier(0.0, 0.0, 0.4, 1)', style({ transform: 'translateX(120%)' }))
  ])
]);