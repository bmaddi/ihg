import {
  state,
  animate,
  animation,
  style,
  transition,
  trigger,
  query,
  animateChild
} from '@angular/animations';

export const slideAnimationDefaults = {
  time: '400ms',
  delay: '10ms',
  effect: 'ease-in-out'
};

export const slideUpDown = trigger('slideUpDown', [
  state('false', style({ height: 0, opacity: 0, overflow: 'hidden' })),
  state('true', style({ height: '*', opacity: 1 })),
  transition('* <=> *', animate('{{ time }} {{ delay }} {{ effect }}'),
    {
      params: {
        time: slideAnimationDefaults.time,
        delay: slideAnimationDefaults.delay,
        effect: slideAnimationDefaults.effect
      }
    })
]);

export const slideUp = animation([
  style({ height: 0, opacity: 0, overflow: 'hidden' }),
  animate('{{ time }} {{ delay }} {{ effect }}', style({ height: '*', opacity: 1 }))],
  {
    params: {
      time: slideAnimationDefaults.time,
      delay: slideAnimationDefaults.delay,
      effect: slideAnimationDefaults.effect
    }
  });

export const slideDown = animation([
  style({ overflow: 'hidden' }),
  animate('{{ time }} {{ delay }} {{ effect }}', style({ height: 0, opacity: 0 }))],
  {
    params: {
      time: slideAnimationDefaults.time,
      delay: slideAnimationDefaults.delay,
      effect: slideAnimationDefaults.effect
    }
  });

export const animateChildren = query("@*", [animateChild()], { optional: true });

export const slideUpDownNgIf = trigger('slideUpDownNgIf', [
  transition(':enter', [slideUp]),
  transition(':leave', [slideDown, animateChildren]),
]);