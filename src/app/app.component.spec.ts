import { TestBed, ComponentFixture, async } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { Angulartics2Module } from 'angulartics2';
import { MessageService } from '@progress/kendo-angular-l10n';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { EnvironmentService } from 'ihg-ng-common-core';
import { CommonPagesListenerService, ActivityModalService, ReportIssueService } from 'ihg-ng-common-pages';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { mockEnvs } from '@app/constants/app-test-constants';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonTestModule,
        Angulartics2Module.forRoot([Angulartics2GoogleAnalytics], {
          pageTracking: {
            autoTrackVirtualPages: false
          }
        }),
        HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [EnvironmentService, MessageService, CommonPagesListenerService, ActivityModalService, ReportIssueService, NgbActiveModal],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
