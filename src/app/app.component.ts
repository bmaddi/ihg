import { Component, OnInit, HostListener, OnDestroy, LOCALE_ID, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { some } from 'lodash';
import { Subscription } from 'rxjs';
import { IntlService, CldrIntlService } from '@progress/kendo-angular-intl';
import { MessageService } from '@progress/kendo-angular-l10n';

import { CommonPagesListenerService } from 'ihg-ng-common-pages';
import {
  LoginService,
  UserService,
  IdleTimerService,
  IdleSiteminderTimerService,
  LanguageSelectorService,
  HotelSelectionService,
  MenuService,
  UserData,
  GoogleAnalyticsService,
  ToastMessageOutletService,
  BreadcrumbsService,
  EnvironmentService
} from 'ihg-ng-common-core';
import { environment } from '@env/environment';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { FDKUnTranslatedTranslations } from '../translation/translated-files/un.translations';
import { FDKEnglishTranslations, FDKSimplifiedChineseTranslations } from '../translation';
import { KendoLocaleMessageService } from '../translation/kendo-localization/kendo-locale-message.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  userDetails: UserData;
  loggedInUserDetails;
  brandColor: string;
  showPageTitle: boolean;
  slnmId: string;
  initialized: boolean;
  disablePageError = false;
  maxWidth = false;
  showHelp = false;
  private hotelChangeSubscription: Subscription;
  private languageChange$: Subscription;

  constructor(private loginService: LoginService,
              private userService: UserService,
              private IdleTimer: IdleTimerService,
              private IdleSiteminderTimer: IdleSiteminderTimerService,
              private languageSelectorService: LanguageSelectorService,
              private hotelSelectionService: HotelSelectionService,
              private menuService: MenuService,
              private translate: TranslateService,
              private gaService: GoogleAnalyticsService,
              private location: Location,
              private toastOutletService: ToastMessageOutletService,
              private angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
              private commonPageListener: CommonPagesListenerService,
              private router: Router,
              private route: ActivatedRoute,
              @Inject(LOCALE_ID) private localeId: string,
              private intlService: IntlService,
              private kendoMessageService: MessageService,
              private breadcrumbsService: BreadcrumbsService,
              private environmentService: EnvironmentService
  ) {

    languageSelectorService.translateOnApplicationLoad();
    this.setTranslationFiles();
  }

  ngOnInit() {
    this.userService.getUserDetails().subscribe(
      (response: any) => {
        this.handleLoginResponse(response);
        this.initializeGoogleAnalytics();
        this.initializeIdleTimer();
        this.subscribeMenuChange();
        this.subscribeToastService();
        this.listenForHotelChange();
        this.updateKendoLocale(this.userService.getUserLanguage());
        this.subscribeToLanguageChange();
        const brandCode = response.user.currentLocation.brandCode;
        this.setBrandCode(brandCode);
        this.setBreadCrumbLabel();
        this.handleRouteChange();
        this.initialized = true;
      }, (err: any) => {
        this.initialized = false;
        this.setBrandCode();
        this.handleRouteChange();
      });
  }

  private handleLoginResponse(response): void {
    this.userDetails = response.user;
    this.loggedInUserDetails = response;
    this.loginService.sendLoginSuccess('Login Success');
    this.showPageTitle = true;
  }

  private initializeGoogleAnalytics(): void {
    this.gaService.invokePageViewTrackOnNavigation();
    this.gaService.trackPage(this.location.path());
  }

  private initializeIdleTimer(): void {
    this.IdleTimer.startTimer();
    this.IdleSiteminderTimer.startTimer();
  }

  private subscribeMenuChange() {
    this.menuService.isMenuChanged.subscribe(() => {
      this.initialized = true;
    });
  }

  ngOnDestroy() {
    if (this.hotelChangeSubscription) {
      this.hotelChangeSubscription.unsubscribe();
    }
    if (this.languageChange$) {
      this.languageChange$.unsubscribe();
    }
  }

  private setTranslationFiles(): void {
    this.translate.setTranslation('en', FDKEnglishTranslations.en());
    this.translate.setTranslation('un', FDKUnTranslatedTranslations.un());
    this.translate.setTranslation('zh', FDKSimplifiedChineseTranslations.zh());
  }

  private subscribeToastService() {
    this.toastOutletService.isPageErrorDisabled.subscribe((isDisabled) => {
      setTimeout(() => {
        this.disablePageError = isDisabled;
      });
    });
  }

  listenForHotelChange(): void {
    this.hotelChangeSubscription = this.hotelSelectionService.isHotelChanged.subscribe((isHotelChanged) => {
      if (isHotelChanged) {
        this.navigateBackToHomeApp();
      }

      this.setBrandCode(this.userService.getCurrentLocation().brandCode);
    });
  }

  private navigateBackToHomeApp(): void {
    window.location.href = this.environmentService.getAppLinks()['hin'];
  }

  private handleRouteChange(): void {
    this.updateProperties();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const stateView = this.router.routerState.snapshot.root.firstChild.data.stateView;
        const stateName = this.router.routerState.snapshot.root.firstChild.data.stateName;
        this.showPageTitle = stateName !== 'page-error';
        this.setStateView(stateView, stateName);
        this.updateProperties();
        this.scrollToTop();
      }
    });
  }

  private updateProperties() {
    if (this.route.root.children[0]) {
      const routeData = this.route.root.children[0].snapshot.data;
      this.maxWidth = routeData.maxWidth;
      this.slnmId = routeData.slnmId || 'FDK-SID';
      this.showHelp = routeData.showHelp;
    }
  }

  private scrollToTop() {
    window.scrollTo(0, 0);
  }

  private subscribeToLanguageChange() {
    this.languageChange$ = this.translate.onLangChange.subscribe((evt: LangChangeEvent) => {
      const language = evt.lang !== 'un' ? evt.lang : 'en';
      this.updateKendoLocale(language);
    });
  }

  private updateKendoLocale(locale: string): void {
    (<CldrIntlService>this.intlService).localeId = locale || 'en';
    this.updateKendoLocaleMessages(locale);
  }

  private updateKendoLocaleMessages(locale: string): void {
    const msgService = <KendoLocaleMessageService>this.kendoMessageService;
    msgService.language = locale || 'en';
  }

  private setBreadCrumbLabel(): void {
    this.breadcrumbsService.setLabel('dashboard', 'PAG_TITLE_HOME');
  }

  @HostListener('keydown') onkeydown(event) {
    this.IdleTimer.resetTimer();
  }

  @HostListener('mousewheel') onmousewheel(event) {
    this.IdleTimer.resetTimer();
  }

  @HostListener('mousedown') mousedown(event) {
    this.IdleTimer.resetTimer();
  }

  setStateView(stateView: string = '', stateName: string = ''): void {
    if (typeof document !== 'undefined') {
      document.body.setAttribute('state-view', stateView);
      document.body.setAttribute('state-name', stateName);
    }
  }

  setBrandCode(brandCode: string = ''): void {
    if (typeof document !== 'undefined') {
      document.body.setAttribute('brand-code', brandCode.toUpperCase());
    }
  }

  openCardCatalog() {
    this.router.navigate(['card-catalog']);
  }

  restoreLayout() {
    this.router.navigate(['dashboard'], { queryParams: { restoreLayout: true } });
  }

  reorderLayout() {
    this.router.navigate(['dashboard'], { queryParams: { reorderLayout: true } });
  }
}

