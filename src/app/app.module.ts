import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, LOCALE_ID } from '@angular/core';
import { CommonModule, Location, TitleCasePipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { MessageService } from '@progress/kendo-angular-l10n';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonPagesModule } from 'ihg-ng-common-pages';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { InterceptorsService } from './services/interceptors/interceptors.service';
import { StartupService } from './services/startup/startup.service';
import { KendoLocaleMessageService } from '../translation/kendo-localization/kendo-locale-message.service';

import { AppComponent } from './app.component';
import { PromptModalComponent } from './components/prompt-modal/prompt-modal.component';
import { AppRoutingModule } from '@app/app.routes';
import { GuestsModule } from '@modules/guests/guests.module';
import { SalesDemoModule } from '@app/modules/sales-demo/sales-demo.module';
import { CheckInModule } from '@modules/check-in/check-in.module';
import { AppSharedModule } from '@modules/shared/app-shared.module';
import { WalkInModule } from '@modules/walk-in/walk-in.module';

export function startupServiceFactory(startupService: StartupService): Function {
  return () => startupService.load();
}

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DropDownsModule,
    NgbModule.forRoot(),
    IhgNgCommonCoreModule.forRoot(),
    IhgNgCommonPagesModule.forRoot(),
    IhgNgCommonKendoModule.forRoot(),
    IhgNgCommonComponentsModule.forRoot(),
    TranslateModule.forRoot(),
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics], {
      pageTracking: {
        autoTrackVirtualPages: false
      }
    }),
    RouterModule,
    AppRoutingModule,
    GuestsModule,
    SalesDemoModule,
    CheckInModule,
    WalkInModule,
    AppSharedModule
  ],
  declarations: [
    AppComponent,
    PromptModalComponent,
  ],
  providers: [
    StartupService,
    TitleCasePipe,
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorsService,
      multi: true
    },
    HttpClientModule,
    {
      provide: LOCALE_ID,
      useValue: 'en'
    },
    {
      provide: MessageService,
      useClass: KendoLocaleMessageService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
