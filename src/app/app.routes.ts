import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PromptModalComponent } from './components/prompt-modal/prompt-modal.component';
import { DashboardGuardService } from './services/guards/dashboard-guard/dashboard-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'fdk-dashboard',
    pathMatch: 'full'
  },
  {
    path: 'fdk-dashboard',
    loadChildren: './modules/demo/demo.module#DemoModule',
    canActivate: [DashboardGuardService],
    data: {
      crumb: [],
      pageTitle: 'Home',
      noSubtitle: true,
      gaPageTitle: 'Concerto',
      stateName: 'demo',
      stateView: 'demo'
    }
  },
  {
    path: 'prepare',
    component: PromptModalComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}

