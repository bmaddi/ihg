import { Component, OnInit, Input, Injector } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

export interface IPromptModalOptions {
  titleKey: string;
  textKey: string;
  cancelKey: string;
  continueKey: string;
}

@Component({
  selector: 'hin-prompt-modal',
  templateUrl: './prompt-modal.component.html',
  styleUrls: ['./prompt-modal.component.scss']
})
export class PromptModalComponent implements OnInit {
  @Input() titleKey = 'TITLE';
  @Input() textKey = 'TEXT';
  @Input() cancelKey = 'COM_BTN_CANCEL';
  @Input() continueKey = 'COM_BTN_CNTNU';

  resolve: IPromptModalOptions;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.dismiss();
  }

  continue() {
    this.activeModal.close(true);
  }
}
