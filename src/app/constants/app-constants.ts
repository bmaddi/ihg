export const AppConstants = {
  EMDASH: '—',
  HYPHEN: '-',
  VW_DT_FORMAT: 'DDMMMYYYY',
  DOW_FORMAT: 'ddd',
  DB_DT_FORMAT: 'YYYY-MM-DD',
  QP_FORMAT: 'MM-DD-YYYY',
  PIPE_DD_MM_FORMAT: 'dd MMM yyyy',
  PIPE_DD_MM_FORMAT_NO_SPACE: 'ddMMMyyyy',
  MM_DD_FORMAT: 'MMDDYYYY',
  MMM_FORMAT: 'MMM',
  HH_MM_FORMAT: 'HH:mm',
  SAVE: 'SAVE',
  LOAD: 'LOAD',
  DD_MMM_FORMAT: 'DDMMM',
  FULL_DOW_FORMAT: 'dddd',
  DT_TIME_FORMAT: 'DDMMMYYYY HH:mm z',
  LANGUAGE_LOCALE: {
    en: 'en-US',
    zh: 'zh-CN',
    un: 'en-US'
  }
};

export const searchValidRegex = new RegExp(/^[A-Za-z'\-, 0-9]*$/);

export const getHotelLogoPath = (logo: string): string => logo ? `assets/images/Logos_for_Global_Header/${logo}` : '';

export const calculatePageNumber = (skip: number, pageSize: number): number => {
  return (skip / pageSize) + 1;
};

