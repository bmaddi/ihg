export const mockEnvs = {
  hinAPI: 'https://concerto.mock.test/hin/hinservices/v1/',
  fdkAPI: 'https://concerto.mock.test/fdk/fdkservices/v1/',
  apiUrl: 'https://concerto.mock.test/fdk/concertoservices/v1/',
  applCd: 'fdk',
};

export const mockUser = {
  user: {
    'userName': 'jdoe',
    'securityId': '4f21ee9d73d5942c0f6bfc8fdb91c60e',
    'firstName': 'John',
    'lastName': 'Doe',
    'email': 'john.doe@email.com',
    'phoneNumber': '7706042906',
    'lastLoginTime': 1471900753000,
    'currentLocationId': null,
    'corporateUser': true,
    'isOwner': false,
    'multipleHoteluser': true,
    'roles': [
      {
        'id': 5000,
        'name': 'HOTEL_MANAGEMENT',
        'description': 'HOTEL_MANAGEMENT',
        'functions': null
      }],
    'hotels': null,
    'currentLocation': {
      'mnemonic': 'ATLCP',
      'name': 'Atlanta Crowne Plaza',
      'brandCode': 'HICP',
      'brandName': '',
      'brandColor': '#821953',
      'brandImage': 'crowne_plaza.png',
      'addressLine1': '3 ',
      'addressLine2': 'Ravinia Drive',
      'city': 'Atlanta',
      'state': 'GA',
      'country': 'US',
      'zipCode': '30046',
      'currencyCode': 'USD',
      'regionId': null,
      'regionName': '',
      'regionDescription': '',
      'subRegionId': null,
      'subRegionName': '',
      'subRegionDescription': '',
      'managementTypeCode': '',
      'managementTypeName': '',
      'poEnabled': true,
      'tiersEnabled': false,
      'hpBindFlag': true,
      'collectDate': '2017-02-15'
    },
    'domainName': 'AMER'
  }
};

export const translateServiceStub = {
  instant: (key: string | Array<string>, interpolateParams?: Object): void => {
  }
};

export const googleAnalyticsStub = {
  trackEvent: (category, action, label): void => {
  }
};

export const sessionStorageServiceStub = {
  setSessionStorage: (category, action): void => {
  }
};
