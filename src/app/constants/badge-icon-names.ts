export const BadgeIconNames = {
    'spire': 'Spire Elite',
    'platinum': 'Platinum Elite',
    'gold': 'Gold',
    'club': 'Club',
    'royalambassador': 'Royal Ambassador',
    'ambassador': 'Ambassador',
    'innercircle': 'Inner Circle',
    'karma': 'Karma',
    'employee': 'Employee',
};
