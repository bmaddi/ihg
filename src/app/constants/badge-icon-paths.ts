export const BadgeIconPaths = {
    'spire': 'assets/images/badges/spire.svg',
    'platinum': 'assets/images/badges/platinum.svg',
    'gold': 'assets/images/badges/gold.svg',
    'club': 'assets/images/badges/club.svg',
    'royalambassador': 'assets/images/badges/royalambassador.svg',
    'ambassador': 'assets/images/badges/ambassador.svg',
    'innercircle': 'assets/images/badges/innercircle.svg',
    'karma': 'assets/images/badges/karma.svg',
    'employee': 'assets/images/badges/employee.svg',
    'ihg': 'assets/images/ihg_bmk_rgb_mango_MRedit.svg',
    'priorityEnrollment': 'assets/images/badges/priorityEnrollment',
    'potentialMember': 'assets/images/badges/potentialMember'
};
