import {ReportIssueAutoFillObject} from 'ihg-ng-common-pages';
import {ReportIssueAutoFillData} from 'ihg-ng-common-pages';

export const ARRIVALS_GRID_REPORT_ISSUE: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Error selecting View All Guests on Guest List - Arrivals'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Data Timeout selecting View All Guests on Guest List - Arrivals'
  }
};

export const ARRIVALS_GROUP_REPORT_ISSUE: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Error selecting group name on Groups grid of Guest List - Arrivals'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Data Timeout while selecting group name on Groups grid of Guest List - Arrivals'
  }
};

export const DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL: { [key: string]: ReportIssueAutoFillData } = {
  GENERAL: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Room Assignment',
    subject: 'Error changing Do Not Move indicator'
  },
  TIME_OUT: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Room Assignment',
    subject: 'Data Timeout changing Do Not Move indicator'
  }
};
export const DO_NOT_MOVE_PREPARE_REPORT_ISSUE_AUTOFILL: { [key: string]: ReportIssueAutoFillData } = {
  GENERAL: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Error changing Do Not Move indicator'
  },
  TIME_OUT: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Data Timeout changing Do Not Move indicator'
  }
};

export const ARRIVALS_REPORT_ISSUE: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Error Guest List Arrivals Tab System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Error Guest List Arrivals Tab Data Timeout'
  }
};

export const ARRIVAL_REPORT_ISSUE_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Error Printing Guest List'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Listing',
    subject: 'Data Timeout while printing Guest List'
  }
};

export const AUTH_ERR_REPORT_ISSUE_AUTOFILL = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Payment',
  subject: 'Payment Failed Authorization at Check In'
};

export const CUT_KEY_ISSUE_AUTOFILL = {
  suggestedRoomError: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Cut Keys',
    subject: 'Error Cutting Keys at Check In'
  }
};

export const GROUP_LIST_GRID_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Groups',
    subject: 'Error Selecting Show Groups'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Groups',
    subject: 'Error Retrieving Show Groups Results Data Timeout'
  }
};

export const GROUP_LIST_REPORT_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Groups',
    subject: 'Error selecting Show Groups on Guest List - Arrivals'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Arrivals',
    subtopic: 'Groups',
    subject: 'Data Timeout selecting Show Groups on Guest List - Arrivals'
  }
};

export const GUEST_PROFILE_HEARTBEAT_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Heartbeat Card',
    subject: 'Error HeartBeat Card System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Heartbeat Card',
    subject: 'Error HeartBeat Card Data Timeout'
  }
};

export const GUEST_PROFILE_INSITE_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Insights Card',
    subject: 'Error Insights Card System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Insights Card',
    subject: 'Error Insights Card Data Timeout'
  }
};

export const GUEST_PROFILE_OBSERVATIONS_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Observations',
    subject: 'Error Observations Card System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Observations',
    subject: 'Error Observations Card Data Timeout'
  }
};

export const GUEST_PROFILE_PREFERENCE_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Preferences',
    subject: 'Error Edit Guest Preferences System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Preferences',
    subject: 'Error Data Timeout Editing Guest Preferences'
  }
};

export const GUEST_PROFILE_STAY_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Stay Card',
    subject: 'Error Stay Card System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest Profile',
    subtopic: 'Stay Card',
    subject: 'Error Stay Card Data Timeout'
  }
};

export const IN_HOUSE_GUEST_LIST_AUTOFILL = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - In House',
    subtopic: 'Listing',
    subject: 'Error Guest List In House Tab System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - In House',
    subtopic: 'Listing',
    subject: 'Error Guest List In House Tab Data Timeout'
  }
};

export const MANAGE_CUT_KEY_AUTOFILL: ReportIssueAutoFillData = {
  function: 'Guests',
  topic: 'Manage Stay',
  subtopic: 'Cut Keys',
  subject: 'Error Cutting Keys from In House Manage Stay'
};

export const MANAGE_PAYMENT_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Payment',
    subject: 'Error Payment Panel from In House Manage Stay System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Payment',
    subject: 'Error Payment Panel from In House Manage Stay Data Timeout'
  }
};

export const MANAGE_TASKS_ADD_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Tasks',
    subject: 'Error Adding Tasks'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Tasks',
    subject: 'Error Manage Stay Tasks Data Timeout'
  }
};

export const MANAGE_TASKS_SYSTEM_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Tasks',
    subject: 'Error Manage Stay Tasks System Error'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Tasks',
    subject: 'Error Manage Stay Tasks Data Timeout'
  }
};

export const MANAGE_STAY_SHOP_OFFERS = {
  function: 'Guests',
  topic: 'Manage Stay',
  subtopic: 'Shop Offers'
};

export const OFFERS_LOAD_ERROR: ReportIssueAutoFillObject = {
  genericError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Error Check In Manage Stay System Error'
  },
  timeoutError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Error Check In Manage Stay Data Timeout Error'
  }
};

export const OFFERS_SAVE_ERROR: ReportIssueAutoFillObject = {
  genericError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Error Confirming Reservation Changes System Error'
  },
  timeoutError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Error Confirming Reservation Changes Data Timeout Error'
  }
};

export const UPDATE_TOTALS_SAVE_ERROR = (reservationNumber: string): ReportIssueAutoFillObject => {
  return {
    genericError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Error Saving Pending Reservation for Rate Override at Check In [Res #: ${reservationNumber}]`
    },
    timeoutError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Error Saving Pending Reservation for Rate Override at Check In [Res #: ${reservationNumber}]`
    }
  }
};

export const IN_HOUSE_SAVE_GUEST_INFO: ReportIssueAutoFillObject = {
  genericError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Service Error saving the guest information to reservation'
  },
  timeoutError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Data Timeout saving the guest information to reservation'
  }
};

export const MANAGE_STAY_OFFERS_SAVE_ERROR = (reservationNumber: string): ReportIssueAutoFillObject => {
  return {
    genericError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Service Error while updating reservation ${reservationNumber}`
    },
    timeoutError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Data Timeout while updating reservation ${reservationNumber}`
    }
  };
};

export const MANAGE_STAY_LOAD_ERROR: ReportIssueAutoFillObject = {
  genericError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: ' Error Confirming Reservation Update'
  },
  timeoutError: {
    ...MANAGE_STAY_SHOP_OFFERS, subject: 'Data Timeout Confirming Reservation Update'
  }
};

export const MANAGE_STAY_ASSIGN_ROOM_AUTOFILL = (reservationNumber: string, roomNumber: string): ReportIssueAutoFillObject => {
  return {
    genericError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Error manually changing the room to ${roomNumber} for reservation ${reservationNumber}`
    },
    timeoutError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Error manually changing the room to ${roomNumber} for reservation ${reservationNumber}`
    }
  };
};

export const CUT_KEY_SAVE_ERROR = (reservationNumber: string, roomNumber: string): ReportIssueAutoFillObject => {
  return {
    genericError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Error cutting keys for room ${roomNumber} for reservation ${reservationNumber}`
    },
    timeoutError: {
      ...MANAGE_STAY_SHOP_OFFERS, subject: `Error cutting keys for room ${roomNumber} for reservation ${reservationNumber}`
    }
  };
};

export const COST_ESTIMATE = (topic: string) => {
  return {
    function: 'Guests',
    topic,
    subtopic: 'Reservation Details'
  }
};

export const COST_ESTIMATE_AUTOFILL = (reservationNumber: string, reportIssueTopicVal: string): ReportIssueAutoFillObject => {
  return {
    genericError: {
      ...COST_ESTIMATE(reportIssueTopicVal), subject: `Error Retrieving Cost Estimate ${reservationNumber}`
    },
    timeoutError: {
      ...COST_ESTIMATE(reportIssueTopicVal), subject: `Timeout Error Retrieving Cost Estimate ${reservationNumber}`
    }
  };
};

export const POINTS_ERR_REPORT_ISSUE_AUTOFILL = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Reservation Details',
  subject: 'Error Selecting Welcome Amenity'
};

export const PREPARE_GRID_ISSUE_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Groups',
    subject: 'Error Selecting Viewing All Guests on Prepare Tab'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Groups',
    subject: 'Error Viewing All Guests on Prepare Tab Data Timeout'
  }
};

export const PREPARE_GROUP_ISSUE_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Groups',
    subject: 'Error Selecting Group Name in Groups Grid'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Groups',
    subject: 'Error Selecting Group Name in Groups Grid Data Timeout'
  }
};

export const PREPARE_NOTIFICATION_ISSUE_AUTOFILL = {
  notificationError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Ready Notification',
    subject: 'Error Room Ready Failure'
  },
  bulkNoticationError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Ready Notification',
    subject: 'Error Bulk Room Ready Failure'
  }
};

export const GUEST_COMPLAINTS_COUNT_AUTOFILL = {
  prepare: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Guest Complaints',
    subject: 'Error retrieving number of guest complaints for a loyalty member'
  },
  checkIn: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Guest Complaints',
    subject: 'Error retrieving number of guest complaints for a loyalty member'
  }
};

export const PREPARE_GUEST_COMPLAINTS_AUTOFILL = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Guest Complaints',
    subject: 'Error retrieving the guest complaints records from Prepare tab'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Guest Complaints',
    subject: 'Data Timeout retrieving the guest complaints records from Prepare tab'
  }
};

export const CHECKIN_GUEST_COMPLAINTS_AUTOFILL = {
  genericError: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Guest Complaints',
    subject: 'Error retrieving the guest complaints records from Check In'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Guest Complaints',
    subject: 'Data Timeout retrieving the guest complaints records from Check In'
  }
};

export const PREPARE_REPORT_ISSUE_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Listing',
    subject: 'Error while loading guest list on Guest List - Prepare'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Listing',
    subject: 'Data Timeout while loading guest list on Guest List - Prepare'
  }
};

export const PMS_CONF_UNAVL_AUTOFILL: ReportIssueAutoFillData = {
  function: 'Guests',
  topic: 'Guest List - Prepare',
  subtopic: 'Listing',
  subject: 'Error Retrieving Reservation Details on Expansion'
};

export const NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL: { [key: string]: ReportIssueAutoFillData } = {
  GENERAL: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Payment',
    subject: 'Error setting No Post flag on reservation'
  },
  TIME_OUT: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Payment',
    subject: 'Data timeout setting No Post flag on reservation'
  }
};

export const NO_POST_MANAGE_STAY_REPORT_ISSUE_AUTOFILL: { [key: string]: ReportIssueAutoFillData } = {
  GENERAL: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Payment',
    subject: 'Error setting No Post flag on reservation'
  },
  TIME_OUT: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Payment',
    subject: 'Data timeout setting No Post flag on reservation'
  }
};

export const MANAGE_STAY_ROOM_REPORT_ISSUE = {
  suggestedRoomError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Room Assignment',
    subject: 'Error Loading Suggested Rooms for In House Guest'
  },
  manualRoomError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Room Assignment',
    subject: 'Error Manual Room Assignment Load/Timeout'
  },
  changeRoomError: {
    function: 'Guests',
    topic: 'Change Room',
    subtopic: 'Error Message'
  },
  suggestedAssignRoomError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Room Assignment',
    subject: 'Error Assigning Suggested Room for In House Guest'
  },
  manualRoomAssignError: {
    function: 'Guests',
    topic: 'Manage Stay',
    subtopic: 'Room Assignment',
    subject: 'Error Manually Assigning Room for In House Guest'
  }
};

export const ROOM_REPORT_ISSUE_AUTOFILL = {
  suggestedRoomError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Error Suggested Room Assignment Load/Timeout'
  },
  manualRoomError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Error Manual Room Assignment Load/Timeout'
  },
  changeRoomError: {
    function: 'Guests',
    topic: 'Change Room',
    subtopic: 'Error Message'
  },
  suggestedAssignRoomError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Error Suggested Room Assignment Failure'
  },
  manualRoomAssignError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Error Manual Room Assignment Failure'
  },
  dnmReasonError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Room Assignment',
    subject: 'Error Saving Do Not Move Reason [Res #: <Reservation #>]'
  },
  dnmSwitchError: {
    function: 'Guests',
    topic: 'Guest List - Prepare',
    subtopic: 'Do Not Move',
    subject: 'Error Do Not Move Failure'
  },
  dnmReasonCheckInError: {
    function: 'Guests',
    topic: 'Check In',
    subtopic: 'Room Assignment',
    subject: 'Error Saving Do Not Move Reason'
  }
};

export const TASKS_CHECK_IN_ADD_AUTOFILL: ReportIssueAutoFillData = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Tasks',
  subject: 'Error Adding Tasks'
};

export const TASKS_CHECK_IN_FETCH_AUTOFILL: ReportIssueAutoFillData = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Tasks',
  subject: 'Error Displaying Tasks'
};

export const GUEST_SAVE_AUTOFILL: ReportIssueAutoFillData = {
  function: 'Guests',
  topic: 'Manage Stay',
  subtopic: 'Guest Info',
  subject: 'Error Saving Guest Info'
};

export const FETCH_AUTH_ERR_AUTOFILL = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Payment',
  subject: 'Error Fetching Authorization Details'
};

export const SAVE_AUTH_ERR_AUTOFILL = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Payment',
  subject: 'Error Saving Authorization Details'
};

export const AUTH_ERR_REPORT_ISSUE_NOTSYNC = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Reservation Details',
  subject: 'Error Retrieving Reservation Details'
};

export const ALLGUESTS_GRID_REPORT_ISSUE: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: 'Guest List - All Guests',
    subtopic: 'Listing',
    subject: 'Error Generating All Guests Listing'
  },
  timeoutError: {
    function: 'Guests',
    topic: 'Guest List - All Guests',
    subtopic: 'Listing',
    subject: 'Data Timeout Generating All Guests Listing'
  }
};

export const RATE_CODE_MODAL_ERROR_AUTOFILL: ReportIssueAutoFillObject = {
  genericError: {
    function: 'Guests',
    topic: '',
    subtopic: 'Error Message',
    subject: 'Error Message System Error Viewing Rate Code Modal'
  },
  timeoutError: {
    function: 'Guests',
    topic: '',
    subtopic: 'Error Message',
    subject: 'Error Message Data Timeout Viewing Rate Code Modal'
  }
};
