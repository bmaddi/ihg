import { AllGuestsModule } from './all-guests.module';

describe('AllGuestsModule', () => {
  let allGuestsModule: AllGuestsModule;

  beforeEach(() => {
    allGuestsModule = new AllGuestsModule();
  });

  it('should create an instance', () => {
    expect(allGuestsModule).toBeTruthy();
  });
});
