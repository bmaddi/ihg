import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { RouterModule } from '@angular/router';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { ActionToggleDropdownModule } from 'ihg-ng-common-components';
import { AllGuestsComponent } from './components/all-guests/all-guests.component';
import { AllGuestsGridComponent } from './components/all-guests-grid/all-guests-grid.component';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { BadgeListModule } from '@modules/badge-list/badge-list.module';
import { AllGuestsActionsComponent } from './components/all-guests-actions/all-guests-actions.component';
import { GuestSearchModule } from '../guest-search/guest-search.module';
import {IhgDateRangeModule} from '@modules/ihg-date-range/ihg-date-range.module';
import { ManageStayModule } from '@modules/manage-stay/manage-stay.module';
import { OffersModule } from '@modules/offers/offers.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    AppSharedModule,
    GridModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    ActionToggleDropdownModule,
    AppSharedModule,
    BadgeListModule,
    RouterModule,
    NgbTooltipModule,
    GuestSearchModule,
    IhgDateRangeModule,
    ManageStayModule,
    OffersModule
  ],
  declarations: [
    AllGuestsComponent,
    AllGuestsGridComponent,
    AllGuestsActionsComponent
  ],
  exports: [AllGuestsComponent]
})
export class AllGuestsModule {
}
