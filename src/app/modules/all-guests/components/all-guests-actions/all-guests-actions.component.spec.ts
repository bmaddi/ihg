import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateStore } from '@ngx-translate/core';

import { ActionToggleDropdownModule } from 'ihg-ng-common-components';
import { EnvironmentService, SessionStorageService, ToastMessageOutletService, GoogleAnalyticsService } from 'ihg-ng-common-core';

import { AllGuestsActionsComponent } from './all-guests-actions.component';
import {
  environmentServiceServiceStub,
  googleAnalyticsServiceStub
} from '@modules/guest-relations/mocks/guest-relations.mock';
import { allGuestsMockResponse } from '@modules/all-guests/constants/all-guests-test.constants';
import { AllGuestsService } from '@modules/all-guests/services/all-guests.service';
import { cloneDeep } from 'lodash';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { By } from '@angular/platform-browser';
import { MOCK_GUEST_RESERVATION_DATA } from '../../mocks/guest-reservation.mock';

export class AllGuestsServiceStub {
  public emitGuestTabEvent(data) { }
  public emitUserDataSource(data) { }
  public mapAllGuestData() {
    return cloneDeep(allGuestsMockResponse).items[0];
  }
}

xdescribe('AllGuestsActionsComponent', () => {
  let component: AllGuestsActionsComponent;
  let fixture: ComponentFixture<AllGuestsActionsComponent>;
  let gaService: GoogleAnalyticsService;
  let allGuestService: AllGuestsService;
  let ssService: SessionStorageService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .overrideProvider(GuestListService, {
        useValue: {}
      })
      .overrideProvider(SessionStorageService, {
        useValue: {}
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
      .configureTestingModule({
        imports: [ActionToggleDropdownModule, RouterTestingModule, HttpClientTestingModule],
        declarations: [AllGuestsActionsComponent],
        providers: [TranslateStore, EnvironmentService, SessionStorageService, GoogleAnalyticsService,
          { provide: AllGuestsService, useClass: AllGuestsServiceStub }]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGuestsActionsComponent);
    component = fixture.componentInstance;
    gaService = TestBed.get(GoogleAnalyticsService);
    ssService = TestBed.get(SessionStorageService);
    allGuestService = TestBed.get(AllGuestsService);
    component.guestItem = cloneDeep(allGuestsMockResponse).items[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load the component', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.showDropdown).toBeDefined();
  });

  it('should display dropdown items on hover', () => {
    expect(component.isToday(component.guestItem.checkInDate)).toBeTruthy();
    expect(component.isPrepareThreshold(component.guestItem.checkInDate)).toBeFalsy();
    expect(component.showDropdown).toBeTruthy();
    const mockShow = fixture.nativeElement.querySelector('.drop__down');
    expect(mockShow).toBeDefined();
    mockShow.click();
    fixture.detectChanges();
    const mockdropdown = fixture.nativeElement.querySelector('.drop__down_item');
    expect(mockdropdown).toBeDefined();
    const mockDropdownElement = fixture.debugElement.query(By.css('[data-slnm-ihg="AllGuestsActionsPrepare1-SID"]'));
    expect(mockDropdownElement).toBeDefined();
  });

  it('Manage stay action should only display for in-house reservation', () => {
    expect(component.isToday(component.guestItem.checkInDate)).toBeTruthy();
    expect(component.showDropdown).toBeTruthy();
    const mockShow = fixture.nativeElement.querySelector('.drop__down');
    expect(mockShow).toBeDefined();
    mockShow.click();
    fixture.detectChanges();
    const mockdropdown = fixture.nativeElement.querySelector('.drop__down_item');
    expect(mockdropdown).toBeDefined();
    const mockDropdownElement = fixture.debugElement.query(By.css('[data-slnm-ihg="ManageStay1-SID"]'));
    expect(mockDropdownElement).toBeDefined();
  });

  it('On click of action button user should be redirected to manage stay and display that record', () => {
    expect(component.isToday(component.guestItem.checkInDate)).toBeTruthy();
    expect(component.showDropdown).toBeTruthy();
    const mockShow = fixture.nativeElement.querySelector('.drop__down');
    expect(mockShow).toBeDefined();
    mockShow.click();
    fixture.detectChanges();
    const mockdropdown = fixture.nativeElement.querySelector('[data-slnm-ihg="ManageStay1-SID"]');
    expect(mockdropdown).toBeDefined();
    const mockDropdownElement = fixture.debugElement.query(By.css('[data-slnm-ihg="ManageStay1-SID"]'));
    expect(mockDropdownElement).toBeDefined();
  });

  it('should test google analytics method if parameters are passed correctly', () => {
    spyOn(gaService, 'trackEvent').and.callThrough();
    component.trackEvent(component.activeTab, component.ga.ACTION, component.ga.PREPARE);
    expect(gaService.trackEvent).toHaveBeenCalledWith('Guest List - All Guests', 'Action Menu', 'Prepare');
  });

  it('should test navigateToPrepare method', () => {
    spyOn(component, 'trackEvent').and.callThrough();
    component.navigateToPrepare(MOCK_GUEST_RESERVATION_DATA);
    expect(component.trackEvent).toHaveBeenCalled();
  });

  it('should test navigateToCheckIn method', () => {
    spyOn(component, 'trackEvent').and.callThrough();
    component.navigateToCheckIn(MOCK_GUEST_RESERVATION_DATA);
    expect(component.trackEvent).toHaveBeenCalled();
  });

  it('should test isGuestCheckedIn method', () => {
    component.guestItem.reservationStatus = 'NO_RESERVATION';
    const res = component.isGuestCheckedIn();
    expect(res).toBeFalsy();
    component.guestItem.reservationStatus = 'CHECKEDIN';
    const result = component.isGuestCheckedIn();
    expect(result).toBeTruthy();
  });

  it('should test isGuestInHouse method', () => {
    component.guestItem.reservationStatus = 'NO_RESERVATION';
    const res = component.isGuestInHouse();
    expect(res).toBeFalsy();
    component.guestItem.reservationStatus = 'CHECKEDIN';
    const result = component.isGuestCheckedIn();
    expect(result).toBeTruthy();
  });

  it('should test isToday method', () => {
    component.localDate = new Date();
    const dateparam = new Date().toISOString();
    const res = component.isToday(dateparam);
    expect(res).toBeTruthy();
  });

  it('should test isPrepareThreshold method for correct value', () => {
    component.localDate = new Date();
    const dateparam = new Date(new Date().getDate() - 10).toISOString();
    const res = component.isPrepareThreshold(dateparam);
    expect(res).toBeFalsy();
  });

});
