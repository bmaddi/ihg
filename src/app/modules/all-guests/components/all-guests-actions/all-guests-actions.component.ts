import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import * as moment from 'moment';
import { GoogleAnalyticsService, SessionStorageService } from 'ihg-ng-common-core';

import { GuestReservation } from '@modules/all-guests/interfaces/guest-reservation.interface';
import { AllGuestsService } from '@modules/all-guests/services/all-guests.service';
import { ReservationStatus } from '@modules/all-guests/enums/reservation-status-map.enum';

@Component({
  selector: 'app-all-guests-actions',
  templateUrl: './all-guests-actions.component.html',
  styleUrls: ['./all-guests-actions.component.scss']
})
export class AllGuestsActionsComponent implements OnInit, OnChanges {

  @Input() guestItem: GuestReservation;
  @Input() localDate: Date;
  @Input() rowIndex: number;

  keyMap = GUEST_CONST.KEYMAP;
  dateIndex: number;
  showDropdown: boolean;
  activeTab = '';
  ga = GUEST_CONST.GA;

  constructor(private router: Router,
              private allGuestsService: AllGuestsService,
              private storageService: SessionStorageService,
              private gaService: GoogleAnalyticsService) {
  }

  ngOnInit() {
    this.activeTab = this.ga.CATEGORY + this.ga.SUB_CATEGORY.ALLGUESTTAB;
    if (this.guestItem) {
      this.displayDropdown();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.displayDropdown();
  }

  navigateToPrepare(guestItemVal: GuestReservation) {
    this.trackEvent(this.activeTab, this.ga.ACTION, this.ga.PREPARE);
    this.allGuestsService.emitGuestTabEvent({
      tabFlag: true,
      dateIndex: moment(guestItemVal.checkInDate).diff(this.localDate),
      selectedData: guestItemVal,
      searchedDate: moment(guestItemVal.checkInDate).toDate()
    });
  }

  isGuestCheckedIn(): boolean {
    return this.guestItem.reservationStatus === ReservationStatus.CHECKEDIN ||
      this.guestItem.reservationStatus === ReservationStatus.PRECHECKEDIN ||
      this.guestItem.reservationStatus === ReservationStatus.INHOUSE;
  }

  isGuestInHouse(): boolean {
    return this.guestItem.reservationStatus === ReservationStatus.CHECKEDIN ||
      this.guestItem.reservationStatus === ReservationStatus.INHOUSE;
  }

  navigateToCheckIn(guestItemVal: GuestReservation) {
    this.trackEvent(this.activeTab, this.ga.ACTION, this.ga.CHECK_IN);
    this.allGuestsService.emitUserDataSource(guestItemVal);
    this.router.navigate(['/check-in']).then();
  }

  isToday(date: string): boolean {
    return moment(date).startOf('day').isSame(moment(this.localDate).startOf('day'));
  }

  isPrepareThreshold(date: string): boolean {
    return moment(date).diff((moment(this.localDate)), 'days') < 7 && moment(date).diff((moment(this.localDate)), 'days') > 0;
  }

  navigateToStayManagementPage({reservationNumber}: GuestReservation) {
    this.trackEvent(this.activeTab, this.ga.ACTION, this.ga.MANAGE_STAY);
    this.storageService.setSessionStorage('manageStayParams',
      {
        reservationNumber,
        inHouseParams: {
          rowCount: null
        }
      });
    this.router.navigate(['/manage-stay']).then();
  }

  private displayDropdown() {
    this.showDropdown = !this.isCanceledReservation() && (this.isGuestInHouse() || this.isToday(this.guestItem.checkInDate) ||
      this.isPrepareThreshold(this.guestItem.checkInDate));
  }

  private isCanceledReservation(): boolean {
    return this.guestItem.reservationStatus === ReservationStatus.CANCELED;
  }

  public trackEvent(category: string, action: string, label: string) {
    this.gaService.trackEvent(category, action, label);
  }
}
