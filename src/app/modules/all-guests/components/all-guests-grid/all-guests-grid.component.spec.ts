import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GridModule, GridDataResult } from '@progress/kendo-angular-grid';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import {
  ApiErrorService,
  EnvironmentService,
  GoogleAnalyticsService,
  MenuService,
  SessionStorageService,
  ToastMessageOutletService
} from 'ihg-ng-common-core';
import { ActionToggleDropdownModule, ErrorContainerComponent } from 'ihg-ng-common-components';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { AllGuestsGridComponent } from './all-guests-grid.component';
import { defaultGridState, viewPortColumnWidths } from '../../constants/all-guests.constants';
import { TruncatedTooltipComponent } from '@app-shared/components/truncated-tooltip/truncated-tooltip.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BadgeListComponent } from '@modules/badge-list/badge-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { sessionStorageServiceStub } from '@app/constants/app-test-constants';
import { HttpErrorResponse } from '@angular/common/http';

import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';

import {
  environmentServiceServiceStub,
  googleAnalyticsServiceStub
} from '@modules/guest-relations/mocks/guest-relations.mock';

import { AllGuestsService } from '@modules/all-guests/services/all-guests.service';
import { allGuestsMockResponse, allGuestsNoDataMockResponse } from '@modules/all-guests/constants/all-guests-test.constants';
import { ReservationStatusMap } from '@modules/all-guests/enums/reservation-status-map.enum';
import { AppConstants } from '@app/constants/app-constants';
import { AllGuestsActionsComponent } from '@modules/all-guests/components/all-guests-actions/all-guests-actions.component';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { AllGuestsResponse } from '@modules/all-guests/interfaces/guest-reservation.interface';
import { ServiceErrorComponent } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { SmallErrorCardComponent } from '@app-shared/components/small-error-card/small-error-card.component';
import { AllGuestsQueryParams } from '@modules/all-guests/interfaces/all-guests-query-params.interface';
import { HighlightPipe } from '@app-shared/pipes/text-highlight';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { MOCK_GUEST_RESERVATION_DATA } from '../../mocks/guest-reservation.mock';
import { ManageStayService } from '@modules/manage-stay/services/manage-stay.service';
import { RateCategoryModel } from '@modules/offers/models/offers.models';
import { RoomRateDescriptionModalComponent } from '@modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';
import {MAPPED_GUEST_DATA,
        MOCK_RESERVATION_DATA_RESPONSE,
        RATE_CODE_DETAILS,
        RATE_CODE_MODAL_PARAMS,
        ALL_GUEST_DATA} from '@modules/manage-stay/mocks/manage-stay-mock';

export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve('x'));
  componentInstance = RATE_CODE_MODAL_PARAMS;
}

describe('AllGuestsGridComponent', () => {
  let component: AllGuestsGridComponent;
  let fixture: ComponentFixture<AllGuestsGridComponent>;
  const error = new BehaviorSubject<EmitErrorModel>(null);
  const errorObs = error.asObservable();
  let gaService: GoogleAnalyticsService;
  let ssService: SessionStorageService;
  let check_api: boolean;
  let modalService: NgbModal;
  const mockModalRef: MockNgbModalRef = new MockNgbModalRef();

  class MockManageStayService {
    public getRateCodeData(reservationNumber) {
      return check_api ? of(RATE_CODE_DETAILS) : throwError('error');
    }
  }

  const getTruncatedTooltipComponentsForColumn = (columnId: string): DebugElement[] => {
    return fixture.debugElement
      .queryAll(By.directive(TruncatedTooltipComponent))
      .filter(d => d.componentInstance.slnmTag.indexOf(columnId) > -1);
  };

  const getFormattedMockResponse = (data = true): AllGuestsResponse => {
    return TestBed.get(AllGuestsService)['mapAllGuestData'](cloneDeep(data ? allGuestsMockResponse : allGuestsNoDataMockResponse));
  };

  const getDefaultMappedQuery = (page = 1, pageSize = 10, sortBy = 'checkInDate', sortDirection = 'asc'): AllGuestsQueryParams => {
    return { page, pageSize, sortBy, sortDirection };
  };

  beforeEach(async(() => {
    TestBed
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .overrideProvider(MenuService, {
        useValue: {}
      })
      .overrideProvider(SessionStorageService, {
        useValue: sessionStorageServiceStub
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: {
          openAutofilledReportIssueModal: (value: object): void => {

          }
        }
      })
      .overrideProvider(GuestListService, {
        useValue: {}
      })
      .configureTestingModule({
        imports: [
          GridModule,
          IhgNgCommonKendoModule,
          GridModule,
          NgbModule,
          TranslateModule.forChild(),
          HttpClientTestingModule,
          ActionToggleDropdownModule,
          RouterTestingModule
        ],
        declarations: [
          AllGuestsGridComponent,
          AllGuestsGridComponent,
          TruncatedTooltipComponent,
          AllGuestsActionsComponent,
          BadgeListComponent,
          ServiceErrorComponent,
          SmallErrorCardComponent,
          ErrorContainerComponent,
          HighlightPipe
        ],
        schemas: [],
        providers: [
          TranslateStore,
          EnvironmentService,
          SessionStorageService,
          GoogleAnalyticsService,
          AllGuestsService,
          ToastMessageOutletService,
          ApiErrorService,
          AppErrorsService,
          ReportIssueService,
          MenuService,
          {provide: ManageStayService, useClass: MockManageStayService}
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGuestsGridComponent);
    component = fixture.componentInstance;
    gaService = TestBed.get(GoogleAnalyticsService);
    ssService = TestBed.get(SessionStorageService);
    component.gridState = cloneDeep(defaultGridState);
    component.allGuestDataErrorEvent = errorObs;
    component.localCurrentDate = new Date();
    modalService = TestBed.get(NgbModal);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.columnWidths).toEqual(viewPortColumnWidths.desktop);
    expect(component.gridState).toEqual(defaultGridState);
  });

  it('check if all options is set to false in grid options', () => {
    const defaultAllOption = false;    
    expect(component.gridSettings.rows.allOption).toEqual(defaultAllOption);
  })

  it('should create grid with correct grid headers', () => {
    const expectedHeader = ['LBL_GST_GUEST_NAME',
      'LBL_GST_LOYALTY', 'LBL_GST_ARRIVAL',
      'LBL_GST_DEPARTURE', 'LBL_GRP_COMP', 'LBL_RATE',
      'LBL_ROOM', 'LBL_STATUS', 'COM_LBL_ACTIONS'];

    const slnmIds = ['GuestNameLabel', 'LoyaltyLabel',
      'ArrivalLabel', 'DepartureLabel', 'GroupCompanyNameLabel',
      'RateLabel', 'RoomLabel', 'StatusLabel', 'ActionsLabel'];

    slnmIds.forEach((s, i) => {
      const headerSpan: DebugElement = fixture.debugElement.query(By.css(`[data-slnm-ihg="${s}-SID"]`));
      expect(headerSpan).not.toBeNull();
      expect(headerSpan.nativeElement.textContent).toEqual(expectedHeader[i]);
    });
  });

  it('should set grid data on guestsData updates', () => {
    component.guestsData = TestBed.get(AllGuestsService)['mapAllGuestData'](cloneDeep(allGuestsMockResponse));
    fixture.detectChanges();

    expect(component.gridData.total).toEqual(allGuestsMockResponse.totalItems);
    expect(component.gridData.data.length).toEqual(allGuestsMockResponse.items.length);
  });

  xit('should have tooltip when required for overlapping Guest Name column', () => {
    component.guestsData = getFormattedMockResponse();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const guestNamesComponents: DebugElement[] = getTruncatedTooltipComponentsForColumn('GuestName');
      // expected outputs based on currently column width Desktop for guestName
      const expectedTruncation = [false, true, false, true];
      guestNamesComponents.forEach((dbElem, i) => {
        expect(dbElem.componentInstance.truncatedText).toEqual(expectedTruncation[i]);
      });
    });
  });

  it('should have tooltip when required for overlapping Group/Company column', () => {
    component.guestsData = getFormattedMockResponse();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const groupCompanyNameComponents: DebugElement[] = getTruncatedTooltipComponentsForColumn('GroupCompanyName');

      // expected outputs based on currently column width Desktop for groupCompanyName
      const expectedTruncation = [true, true, true, true];

      groupCompanyNameComponents.forEach((elm, index) => {
        expect(elm.componentInstance.truncatedText).toEqual(expectedTruncation[index]);
      });
    });
  });

  it('should show correct mapped reservation status for the Status column', () => {
    component.guestsData = TestBed.get(AllGuestsService)['mapAllGuestData'](cloneDeep(allGuestsMockResponse));
    fixture.detectChanges();
    const expectResults = [ReservationStatusMap.DUEIN, ReservationStatusMap.RESERVED, ReservationStatusMap.NOSHOW, AppConstants.EMDASH];
    component.gridData.data.forEach((item, index) => {
      const columnSelectorIndex = 'ReservationStatus' + (index + 1);
      const columnReservationStatus: DebugElement = fixture.debugElement.query(By.css(`[data-slnm-ihg="${columnSelectorIndex}-SID"]`));
      expect(columnReservationStatus).not.toBeNull();
      expect(columnReservationStatus.nativeElement.textContent.trim()).toEqual(expectResults[index]);
    });
  });

  it('should call the setter setDataFromResponse function on input change from parent component', () => {
    const setterSpy = spyOn<any>(component, 'setDataFromResponse').and.callThrough();
    component.guestsData = getFormattedMockResponse();
    fixture.detectChanges();
    expect(setterSpy).toHaveBeenCalled();
    expect(component.gridData.data).toEqual(getFormattedMockResponse().items);
    expect(component.gridData.total).toEqual(getFormattedMockResponse().totalItems);
    expect(component.localCurrentDate).toEqual(getFormattedMockResponse().localCurrentDate as Date);
  });

  it('should calculate Page Number Query correctly from DataStateChange', () => {
    expect(component['getAllGuestsQuery']({ take: 10, skip: 10 })).toEqual(getDefaultMappedQuery(2, 10, '', ''));
    expect(component['getAllGuestsQuery']({ take: 100, skip: 200 })).toEqual(getDefaultMappedQuery(3, 100, '', ''));
    expect(component['getAllGuestsQuery']({ take: 25, skip: 100 })).toEqual(getDefaultMappedQuery(5, 25, '', ''));
    expect(component['getAllGuestsQuery']({ take: 50, skip: 150 })).toEqual(getDefaultMappedQuery(4, 50, '', ''));
  });

  it('should display the correct list item translation key for Pager', () => {
    component.guestsData = getFormattedMockResponse();
    fixture.detectChanges();
    const itemType: DebugElement = fixture.debugElement.query(By.css(`[data-slnm-ihg="ListType-SID"]`));
    expect(itemType).toBeDefined();
    expect(itemType.nativeElement.textContent).toEqual(component.pagerTranslations.gridPagerInfo.itemsText);
  });

  it('should display the correct no items text translation key for Pager when do data is present', () => {
    component.guestsData = getFormattedMockResponse(false);
    fixture.detectChanges();
    const itemType: DebugElement = fixture.debugElement.query(By.css(`.pager-info`));
    expect(itemType).not.toBeNull();
    expect(itemType.children.length).toEqual(1);
    expect(itemType.children[0].nativeElement.textContent).toEqual(component.pagerTranslations.gridPagerInfo.noItemsText);
  });

  it('should enabled forward pagination controls if more pages are available', () => {
    component.guestsData = getFormattedMockResponse();
    fixture.detectChanges();
    const nextButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-next-buttons'));
    const previousButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-prev-buttons'));
    nextButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeFalsy();
    });
    previousButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });
  });

  it('should disable pagination controls if no data is available', () => {
    component.guestsData = getFormattedMockResponse(false);
    fixture.detectChanges();
    const nextButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-next-buttons'));
    const previousButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-prev-buttons'));
    nextButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });
    previousButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });
  });

  it('should emit pagination change to parent component', () => {
    spyOn(component.gridStateChange, 'emit').and.callThrough();
    spyOn(component, 'onDataStateChange').and.callThrough();
    component.guestsData = getFormattedMockResponse();
    fixture.detectChanges();
    const nextButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-next-buttons'));
    nextButtons.children[0].triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onDataStateChange).toHaveBeenCalled();
    expect(component.gridStateChange.emit).toHaveBeenCalledWith({
      page: 2,
      pageSize: 10,
      sortBy: 'checkInDate',
      sortDirection: 'asc'
    });
  });

  it('should verify the default number of records is 10', () => {

    component.guestsData = TestBed.get(AllGuestsService)['mapAllGuestData'](cloneDeep(allGuestsMockResponse));
    fixture.detectChanges();

    expect(component.gridData.total).toEqual(allGuestsMockResponse.totalItems);
    expect(component.gridData.data.length).toEqual(allGuestsMockResponse.items.length);
  });

  it('should on user clicks update the settings icon and validate 10, 25, 50, 100 or All rows displayed', () => {
    const settingSpy = spyOn<any>(component, 'setGridSettings').and.callThrough();
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();

    component.guestsData = getFormattedMockResponse(false);
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.grid.pageSize).toEqual(10);
    expect(component.grid.skip).toEqual(0);
    expect(settingSpy).toHaveBeenCalled();
    expect(component.gridSettings).toBeDefined();


    component.pageSizeUpdate(25);
    fixture.detectChanges();
    expect(component.gridState.take).toEqual(25);
    expect(component.gridState.skip).toEqual(0);
    expect(component.grid.pageSize).toEqual(25);
    expect(component.grid.skip).toEqual(0);
    // Manually Simulate Flow by use grid values to determine they care correctly set//
    component.grid.dataStateChange.emit({ skip: component.grid.skip, take: component.grid.pageSize });
    fixture.detectChanges();
    expect(onDataStateChangeSpy).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalled();
  });

  it('On making a new record selection UI should fetch the data', () => {
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();

    component.guestsData = getFormattedMockResponse();
    component.ngOnInit();
    fixture.detectChanges();
    component.pageSizeUpdate(50);
    fixture.detectChanges();
    expect(component.grid.pageSize).toEqual(50);
    expect(component.grid.skip).toEqual(0);
    // Manually Simulate Flow by use grid values to determine they care correctly set//
    component.grid.dataStateChange.emit({ skip: component.grid.skip, take: component.grid.pageSize, sort: component.grid.sort });
    fixture.detectChanges();
    expect(onDataStateChangeSpy).toHaveBeenCalledWith({ skip: 0, take: 50, sort: [Object({ field: 'checkInDate', dir: 'asc' })] });
    expect(emitSpy).toHaveBeenCalledWith(getDefaultMappedQuery(1, 50));
  });


  it('Switch from one page to another to validate the normal flow', () => {
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();

    component.guestsData = getFormattedMockResponse();
    component.gridState = {
      skip: 0,
      take: 50,
      sort: [{ field: 'checkInDate', dir: 'asc' }],
    };
    component.ngOnInit();
    fixture.detectChanges();
    const nextButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-next-buttons'));
    nextButtons.children[0].triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(onDataStateChangeSpy).toHaveBeenCalledWith({
      filter: undefined,
      group: [],
      skip: 50,
      sort: [{ field: 'checkInDate', dir: 'asc' }],
      take: 50
    });
    expect(emitSpy).toHaveBeenCalledWith(getDefaultMappedQuery(2, 50));
  });

  it('should display the template correctly when there is no data', () => {
    error.next({ hasNoData: true });
    fixture.detectChanges();
    const errorContainer: DebugElement = fixture.debugElement.query(By.css('[data-slnm-ihg="NoSearchResultAllGuests-SID"]'));
    const serviceErrorComponent: DebugElement = fixture.debugElement.query(By.directive(ServiceErrorComponent));
    const header: DebugElement = fixture.debugElement.query(By.css('.error-header'));
    expect(errorContainer).toBeDefined();
    expect(header.nativeElement.textContent).toEqual('LBL_NO_GUESTS_TO_DISPLAY');
  });

  it('should show correct error template and show Report an Issue button on third failure having the correct autofill provided for General Error', () => {
    const mockEmitErrorModel = {
      apiErrors: [
        { code: '500', message: 'Internal Server Error' }], httpError: new HttpErrorResponse({ status: 500 })
    };
    component['setReportIssueConfig']();
    error.next(mockEmitErrorModel);
    fixture.detectChanges();

    const errorContainer: DebugElement = fixture.debugElement.query(By.css('[data-slnm-ihg="NoSearchResultAllGuests-SID"]'));
    const serviceErrorComponent: DebugElement = fixture.debugElement.query(By.directive(ServiceErrorComponent));
    const header: DebugElement = fixture.debugElement.query(By.css('.error-header'));
    expect(errorContainer).toBeDefined();
    expect(header.nativeElement.textContent).toEqual('LBL_ERR');
    expect(serviceErrorComponent.componentInstance.showReportAnIssue).toEqual(false);
    error.next(mockEmitErrorModel);
    fixture.detectChanges();
    error.next(mockEmitErrorModel);
    fixture.detectChanges();
    expect(serviceErrorComponent.componentInstance.showReportAnIssue).toEqual(true);

    const reportAnIssueButton: DebugElement = fixture.debugElement.query(By.css('.ref-btn'));
    const openReportIssueModalSpy = spyOn(TestBed.get(AppErrorsService), 'openReportIssueModal').and.callThrough();
    reportAnIssueButton.triggerEventHandler('click', null);

    fixture.detectChanges();
    expect(openReportIssueModalSpy).toHaveBeenCalledWith(component.reportIssueConfig.genericError);
    component.resetAnyErrors();
  });

  it('should show correct error template and show Report an Issue button on third failure having the correct autofill provided for Timeout Error', () => {
    const mockEmitErrorModel = {
      apiErrors: [
        { code: '504', message: 'Timeout Gateway' }], httpError: new HttpErrorResponse({ status: 504 })
    };
    component['setReportIssueConfig']();
    error.next(mockEmitErrorModel);
    fixture.detectChanges();

    const errorContainer: DebugElement = fixture.debugElement.query(By.css('[data-slnm-ihg="NoSearchResultAllGuests-SID"]'));
    const serviceErrorComponent: DebugElement = fixture.debugElement.query(By.directive(ServiceErrorComponent));
    const header: DebugElement = fixture.debugElement.query(By.css('.error-header'));
    expect(errorContainer).toBeDefined();
    expect(header.nativeElement.textContent).toEqual('LBL_ERR_DATA_TIMEOUT');
    expect(serviceErrorComponent.componentInstance.showReportAnIssue).toEqual(false);
    error.next(mockEmitErrorModel);
    fixture.detectChanges();
    error.next(mockEmitErrorModel);
    fixture.detectChanges();
    expect(serviceErrorComponent.componentInstance.showReportAnIssue).toEqual(true);

    const reportAnIssueButton: DebugElement = fixture.debugElement.query(By.css('.ref-btn'));
    const openReportIssueModalSpy = spyOn(TestBed.get(AppErrorsService), 'openReportIssueModal').and.callThrough();
    reportAnIssueButton.triggerEventHandler('click', null);

    fixture.detectChanges();
    expect(openReportIssueModalSpy).toHaveBeenCalledWith(component.reportIssueConfig.timeoutError);
    component.resetAnyErrors();
  });

  it('should custom sort on groupName descending', () => {
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();
    expect(component.grid.sort[0].field).toEqual('checkInDate');
    expect(component.grid.sort[0].dir).toEqual('asc');

    fixture.detectChanges();
    component.grid.sortChange.emit([{ field: 'groupName', dir: 'desc' }]);

    expect(onDataStateChangeSpy).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalledWith(getDefaultMappedQuery(1, 10, 'groupName', 'desc'));
  });

  it('should custom sort on guestName descending and send correct query parameter request', () => {
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();
    expect(component.grid.sort[0].field).toEqual('checkInDate');
    expect(component.grid.sort[0].dir).toEqual('asc');

    fixture.detectChanges();
    component.grid.sortChange.emit([{ field: 'guestName', dir: 'desc' }]);

    expect(onDataStateChangeSpy).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalledWith(getDefaultMappedQuery(1, 10, 'lastName,firstName', 'desc'));

  });

  it('should custom sort on Arrival descending and send correct query parameter request', () => {
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();

    fixture.detectChanges();
    component.grid.sortChange.emit([{ field: 'arrivalTime', dir: 'desc' }]);

    expect(onDataStateChangeSpy).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalledWith(getDefaultMappedQuery(1, 10, 'arrivalTime', 'desc'));
  });

  it('should custom sort on Reservation Status descending correct query parameter request', () => {
    const onDataStateChangeSpy = spyOn(component, 'onDataStateChange').and.callThrough();
    const emitSpy = spyOn(component.gridStateChange, 'emit').and.callThrough();

    fixture.detectChanges();
    component.grid.sortChange.emit([{ field: 'status', dir: 'desc' }]);

    expect(onDataStateChangeSpy).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalledWith(getDefaultMappedQuery(1, 10, 'status', 'desc'));
  });

  it('Should track when select 10 as rows per page.', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.GEAR_SELECTED;
    const label = ga[`ROWS_PER_PAGE_${10}`];
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.pageSizeUpdate(10);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('Should track when select 25 as rows per page.', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.GEAR_SELECTED;
    const label = ga[`ROWS_PER_PAGE_${25}`];
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.pageSizeUpdate(25);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('Should track when select 50 as rows per page.', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.GEAR_SELECTED;
    const label = ga[`ROWS_PER_PAGE_${50}`];
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.pageSizeUpdate(50);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('Should track when select 100 as rows per page.', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.GEAR_SELECTED;
    const label = ga[`ROWS_PER_PAGE_${100}`];
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.pageSizeUpdate(100);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should test google analytics based on sort on departure date', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.SORT;
    const checkVal = 'Sort by Departure';
    spyOn(gaService, 'trackEvent').and.callThrough();
    component.trackEvent(category, action, 'deparatureDate');
    expect(gaService.trackEvent).toHaveBeenCalledWith(category, action, checkVal);
  });

  it('should test google analytics based on sort on rate', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.SORT;
    const checkVal = 'Sort by Rate';
    spyOn(gaService, 'trackEvent').and.callThrough();
    component.trackEvent(category, action, 'rateCategoryCode');
    expect(gaService.trackEvent).toHaveBeenCalledWith(category, action, checkVal);
  });

  it('should track google analytics when click Guest Profile', () => {
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.GUEST_PROFILE_GA;
    const label = ga.SELECTED;
    const loyaltyId = '123456';
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.viewMemberProfile(loyaltyId);
    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('Should fetch Rate code data from getRateCodeDetails and Call openDescriptionModal function', () => {
    for (let count = 0; count < 5; count++) {
      component.gridData.data[count] = allGuestsMockResponse;
    }
    check_api = true;
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    spyOn(gaService, 'trackEvent').and.callThrough();
    spyOn(component, 'openDescriptionModal');
    component.getRateCodeDetails(1);
    expect(component.openDescriptionModal).toHaveBeenCalledWith(null, RATE_CODE_DETAILS);
    expect(gaService.trackEvent).toHaveBeenCalledWith(category, ga.RATE_CATEGORY, ga.SELECTED);
  });

  it('Should open the Rate code modal from openDescriptionModal function', () => {
    this.data = RATE_CODE_DETAILS;
    spyOn(modalService, 'open').and.returnValue(mockModalRef);
    component.openDescriptionModal(null, RATE_CODE_DETAILS);
    expect(modalService.open).toHaveBeenCalledWith(RoomRateDescriptionModalComponent,
                                { windowClass: 'large-modal', backdrop: 'static', keyboard: false });
  });

  it('Should not show Room number and type when both are empty', () => {
    component.gridData = ALL_GUEST_DATA;
    fixture.detectChanges();
    const isRoomExist = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomNumber1-SID"]'));
    expect(isRoomExist.nativeElement.innerHTML).toContain(AppConstants.EMDASH);
  });

  it('Should not show Room number and type when Room number is empty, but type has value', () => {
    component.gridData = ALL_GUEST_DATA;
    fixture.detectChanges();
    const isRoomExist = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomNumber2-SID"]'));
    expect(isRoomExist.nativeElement.innerHTML).toContain(AppConstants.EMDASH);
  });

  it('Should show Room number and type when Room number and type has value', () => {
    component.gridData = ALL_GUEST_DATA;
    fixture.detectChanges();

    const roomNumberElement = fixture.debugElement.query(By.css('.room-number'));
    const roomTypeElement = fixture.debugElement.query(By.css('.room-type'));

    expect(roomNumberElement.nativeElement.innerHTML).toContain(ALL_GUEST_DATA.data[2].roomNumber + ' ');
    expect(roomTypeElement.nativeElement.innerHTML).toContain(ALL_GUEST_DATA.data[2].roomType);
  });

  it('should test onDataStateChange method', () => {
    const change = {
      skip: 10,
      take: 10,
      sort: [{ field: 'guestName' }],
    };
    component.gridState = undefined;
    spyOn(component, 'trackEvent').and.callThrough();
    spyOn(component.gridStateChange, 'emit').and.callThrough();
    component.onDataStateChange(change);
    expect(component.trackEvent).toHaveBeenCalled();
    expect(component.gridStateChange.emit).toHaveBeenCalled();
    expect(component.gridState).toEqual(change);
  });

  it('should test viewMemberProfile method', () => {
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    const GuestSpy = spyOn(TestBed.get(AllGuestsService), 'setMemberProfile');
    component.viewMemberProfile('1000');
    expect(trackSpy).toHaveBeenCalled();
    expect(GuestSpy).toHaveBeenCalledWith(1000);
  });

  it('should test pageSizeUpdate method', () => {
    component.gridState.take = 10;
    component.gridState.skip = 100;
    const pageSize = 25;
    const spytest = spyOn(ssService, 'setSessionStorage').and.callThrough();
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.pageSizeUpdate(pageSize);
    expect(component.gridState.take).toEqual(pageSize);
    expect(component.gridState.skip).toEqual(0);
    expect(component.gridSettings.rows.default).toEqual(pageSize);
    expect(spytest).toHaveBeenCalled();
    expect(trackSpy).toHaveBeenCalled();
  });

  it('should test trackBy method', () => {
    const result = component.trackBy(1, MOCK_GUEST_RESERVATION_DATA);
    expect(result).toEqual(MOCK_GUEST_RESERVATION_DATA.reservationNumber);
  });

  it('should display the guest name in title case', () => {
    component.guestsData = TestBed.get(AllGuestsService)['mapAllGuestData'](cloneDeep(allGuestsMockResponse));
    fixture.detectChanges();
    const guestDataEl = fixture.debugElement.query(By.css('.guest-name-container')).nativeElement;
    fixture.detectChanges();
    expect(guestDataEl.textContent).not.toEqual('DOE, JOHN');
    expect(guestDataEl.textContent).toEqual('Doe, John');
  });
});
