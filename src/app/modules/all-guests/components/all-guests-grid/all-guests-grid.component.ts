import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { State } from '@progress/kendo-data-query';
import { DataStateChangeEvent, GridComponent, GridDataResult } from '@progress/kendo-angular-grid';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';

import { GridSettings } from 'ihg-ng-common-kendo';
import { SessionStorageService, GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
// tslint:disable-next-line:max-line-length
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { ALLGUESTS_GRID_REPORT_ISSUE } from '@app/constants/error-autofill-constants';
import {
  AllGuestPagerTranslations,
  defaultGridSettings,
  noDataErrorConstants,
  searchErrorConstants,
  viewPortColumnWidths
} from '../../constants/all-guests.constants';
import { AllGuestsResponse, GuestReservation } from '@modules/all-guests/interfaces/guest-reservation.interface';
import { AllGuestsService } from '@modules/all-guests/services/all-guests.service';
import { AppConstants, calculatePageNumber } from '@app/constants/app-constants';
import { ReservationStatusMap } from '@modules/all-guests/enums/reservation-status-map.enum';
import { AllGuestsQueryParams } from '@modules/all-guests/interfaces/all-guests-query-params.interface';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import {FormGroup} from '@angular/forms';
import { ManageStayService } from '@modules/manage-stay/services/manage-stay.service';
import { RateCategoryModel, RateCategoryDataModel } from '@modules/offers/models/offers.models';
import { RoomRateDescriptionModalComponent } from '@modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';

@Component({
  selector: 'app-all-guests-grid',
  templateUrl: './all-guests-grid.component.html',
  styleUrls: ['./all-guests-grid.component.scss']
})
export class AllGuestsGridComponent extends AppErrorBaseComponent implements OnInit {
  @ViewChild(GridComponent) grid: GridComponent;
  @Input() isSearchResult: boolean;
  @Input() gridState: State;
  @Input() allGuestDataErrorEvent: Observable<EmitErrorModel>;
  @Input() set guestsData(data: AllGuestsResponse) {
    this.setDataFromResponse(data);
  }
  @Input() search: string;
  @Output() gridStateChange: EventEmitter<AllGuestsQueryParams> = new EventEmitter();
  @Output() refreshAction = new EventEmitter();

  isDesktopView = true;
  appConstants = AppConstants;
  reservationStatusMap = ReservationStatusMap;
  columnWidths = viewPortColumnWidths.desktop;
  gridSettings: GridSettings = null;
  gridData: GridDataResult = {
    data: [],
    total: 0
  };
  localCurrentDate: Date;
  pagerTranslations = AllGuestPagerTranslations;
  searchErrorConfig: ServiceErrorComponentConfig = searchErrorConstants;
  noDataErrorConfig: ServiceErrorComponentConfig = noDataErrorConstants;
  reportIssueConfig: ReportIssueAutoFillObject;
  ga = GUEST_CONST.GA;
  activeTab = '';

  constructor(private allGuestsService: AllGuestsService,
              private storageService: SessionStorageService,
              private gaService: GoogleAnalyticsService,
              private modalService: NgbModal,
              private manageStayService: ManageStayService) {
    super();
  }

  ngOnInit() {
    this.activeTab = this.ga.CATEGORY + this.ga.SUB_CATEGORY.ALLGUESTTAB;
    this.setGridSettings();
    this.setReportIssueConfig();
  }

  onDataStateChange(change: DataStateChangeEvent) {
    if (change && change.sort) {
      this.trackEvent(this.activeTab, this.ga.SORT, change.sort[0].field);
    }
    this.gridState = change;
    this.gridStateChange.emit(this.getAllGuestsQuery(change));
  }

  viewMemberProfile(loyaltyId: string) {
    this.gaService.trackEvent(this.activeTab, this.ga.GUEST_PROFILE_GA, this.ga.SELECTED);
    this.allGuestsService.setMemberProfile(+loyaltyId);
  }

  pageSizeUpdate(val: number) {
    this.gridState.take = val;
    this.gridState.skip = 0;
    this.gridSettings.rows.default = val;
    this.storageService.setSessionStorage('allguestparams', this.gridState);
    this.gaService.trackEvent(this.activeTab, this.ga.GEAR_SELECTED, this.ga[`ROWS_PER_PAGE_${val}`]);
  }

  trackBy(index: number, item: GuestReservation): string {
    return item.reservationNumber;
  }

  private getAllGuestsQuery(change: DataStateChangeEvent): AllGuestsQueryParams {
    return <AllGuestsQueryParams>{
      page: calculatePageNumber(change.skip, change.take),
      pageSize: change.take,
      sortBy: change && change.sort ? `${change.sort[0].field === 'guestName' ? 'lastName,firstName' : change.sort[0].field}` : '',
      sortDirection: change && change.sort ? `${change.sort[0].dir}` : ''
    };
  }

  private setDataFromResponse(data: AllGuestsResponse) {
    if (data) {
      this.gridData = <GridDataResult>{
        data: data.items,
        total: data.totalItems
      };
      this.gridState['skip'] = data.pageSize * (data.pageNumber - 1);
      this.localCurrentDate = data.localCurrentDate as Date;
    } else {
      this.gridData = <GridDataResult>{
        data: [],
        total: 0
      };
    }
  }

  private setGridSettings() {
    this.gridSettings = cloneDeep(defaultGridSettings);
    this.gridSettings.rows.allOption = false;
  }

  private setReportIssueConfig() {
    this.reportIssueConfig = ALLGUESTS_GRID_REPORT_ISSUE;
  }

  private onReportIssueClicked() {
  }

  private onRefresh(error) {
    this.gridStateChange.emit(this.getAllGuestsQuery(this.gridState as DataStateChangeEvent));
  }

  public trackEvent(category: string, action: string, eventInfo: string): void {
    let label: string;
    if (action === this.ga.SORT) {
      label = `${this.ga.SORT_BY} `;
      switch (eventInfo) {
        case 'deparatureDate': label += this.ga.SORT_FIELDS.departureDate;
          break;
        case 'rateCategoryCode': label += this.ga.SORT_FIELDS.rate;
          break;
        case 'room': label += this.ga.SORT_FIELDS.room;
          break;
        case 'guestName': label += this.ga.guestName;
          break;
        default: label = null;
      }
    }
     if (label) {
      this.gaService.trackEvent(category, action, label);
    }
  }

  public getRateCodeDetails(index) {
    this.gaService.trackEvent(this.activeTab, this.ga.RATE_CATEGORY, this.ga.SELECTED);
    const rateDetailsData = {
        'checkInDate': this.gridData.data[index - this.gridState.skip].checkInDate,
        'checkOutDate': this.gridData.data[index - this.gridState.skip].deparatureDate,
        'ihgRcNumber': this.gridData.data[index - this.gridState.skip].loyaltyId,
        'roomTypeCode': this.gridData.data[index - this.gridState.skip].roomType,
        'rateCategoryCode': this.gridData.data[index - this.gridState.skip].rateCategoryCode
    };
    this.manageStayService.getRateCodeData(rateDetailsData).subscribe((response) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.openDescriptionModal(null, response);
      } else {
        this.openDescriptionModal(rateDetailsData, null, true);
      }
    }, (error) => {
      this.processHttpErrors(error);
      this.openDescriptionModal(rateDetailsData, null);
    });
  }

  public openDescriptionModal(serviceDetails: RateCategoryDataModel, data: RateCategoryModel, noData: boolean = false) {
    const modal = this.modalService.open(RoomRateDescriptionModalComponent,
      { windowClass: 'large-modal', backdrop: 'static', keyboard: false });
    modal.componentInstance.topic = 'Guest List - All Guests';
    if (data) {
      modal.componentInstance.rateName = data.name;
      modal.componentInstance.rateCode = data.code;
      modal.componentInstance.roomName = '';
      modal.componentInstance.roomCode = '';
      modal.componentInstance.roomDesc = '';
      modal.componentInstance.pointsEligible = data.pointsEligible;
      modal.componentInstance.rateInfo = data.rateInfo;
      modal.componentInstance.arrivalDate = '';
      modal.componentInstance.departureDate = '';
      modal.componentInstance.isOnlyRateInfo = true;
    } else {
      modal.componentInstance.noData = noData;
      modal.componentInstance.noRateDetailsErrorEvent = this.emitError;
      modal.componentInstance.rateCodeData = serviceDetails;
    }
    return modal;
  }
}
