import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GridModule } from '@progress/kendo-angular-grid';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { of, throwError } from 'rxjs';
import { NO_ERRORS_SCHEMA, SimpleChange, Component, Input, Output, EventEmitter } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import {
  EnvironmentService,
  GoogleAnalyticsService,
  SessionStorageService
} from 'ihg-ng-common-core';

import { AllGuestsComponent } from './all-guests.component';
import { AllGuestsGridComponent } from '@modules/all-guests/components/all-guests-grid/all-guests-grid.component';
import {
  environmentServiceServiceStub,
  googleAnalyticsServiceStub
} from '@modules/guest-relations/mocks/guest-relations.mock';
import { AllGuestsService } from '@modules/all-guests/services/all-guests.service';
import { allGuestsMockResponse, MOCK_QUERY_DATA } from '@modules/all-guests/constants/all-guests-test.constants';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { RouterTestingModule } from '@angular/router/testing';
import { sessionStorageServiceStub } from '@app/constants/app-test-constants';
import { HighlightPipe } from '@app-shared/pipes/text-highlight';

@Component({
  selector: 'app-datepicker',
  template: ''
})
export class MockDatePickerComponent {
  @Input() inputConfigObj;
  @Input() parentForm;
  @Output() changeDatePickerValue = new EventEmitter();
}

xdescribe('AllGuestsComponent', () => {
  let component: AllGuestsComponent;
  let fixture: ComponentFixture<AllGuestsComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
      .overrideProvider(GuestListService, {
        useValue: {}
      })
      .overrideProvider(SessionStorageService, {
        useValue: sessionStorageServiceStub
      })
      .configureTestingModule({
        imports: [
          GridModule,
          IhgNgCommonKendoModule,
          GridModule,
          NgbModule,
          TranslateModule.forChild(),
          HttpClientTestingModule, RouterTestingModule],
        declarations: [AllGuestsComponent, AllGuestsGridComponent, HighlightPipe, MockDatePickerComponent],
        providers: [EnvironmentService, SessionStorageService, TranslateStore, GoogleAnalyticsService],
        schemas: [NO_ERRORS_SCHEMA]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGuestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch all guest data on selection of All Guests Tab isActiveTab being true', () => {
    const dataFuncSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    const getAllGuestsSpy = spyOn(TestBed.get(AllGuestsService), 'getAllGuests').and.returnValue(of(allGuestsMockResponse));
    component.isActiveTab = true;
    component.ngOnChanges({
      isActiveTab: new SimpleChange(null, true, false)
    });
    fixture.detectChanges();
    expect(dataFuncSpy).toHaveBeenCalled();
    expect(getAllGuestsSpy).toHaveBeenCalled();
  });

  it('should not fetch all guest data again when All Guests Tab isActiveTab being false', () => {
    const dataFuncSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    const getAllGuestsSpy = spyOn(TestBed.get(AllGuestsService), 'getAllGuests').and.returnValue(of(allGuestsMockResponse));
    component.isActiveTab = false;
    component.ngOnChanges({
      isActiveTab: new SimpleChange(true, false, false)
    });
    fixture.detectChanges();
    expect(dataFuncSpy).not.toHaveBeenCalled();
    expect(getAllGuestsSpy).not.toHaveBeenCalled();
  });

  it('should fetch all guest data on grid state change with new query', () => {
    const dataFuncSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    const getAllGuestsSpy = spyOn(TestBed.get(AllGuestsService), 'getAllGuests').and.returnValue(of(allGuestsMockResponse));
    component.onStateChange({page: 2, pageSize: 10, searchBy: ''});
    fixture.detectChanges();
    expect(dataFuncSpy).toHaveBeenCalledWith({page: 2, pageSize: 10, searchBy: ''});
    expect(getAllGuestsSpy).toHaveBeenCalledWith({page: 2, pageSize: 10, searchBy: ''});
  });


  it('should handle General Errors / Timeout Errors', () => {
    const handleResponseFailureSpy = spyOn<any>(component, 'handleResponseFailure').and.callThrough();
    const getAllGuestsSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    const processHttpErrorsSpy = spyOn<any>(TestBed.get(AllGuestsService), 'getAllGuests').and.returnValue(throwError({
      name: '"500"',
      message: 'Failure'
    }));
    expect(component.hasErrors).not.toBeDefined();
    expect(component.emitError.getValue()).toBeNull();
    component.ngOnChanges({
      isActiveTab: new SimpleChange(null, true, false)
    });
    fixture.detectChanges();

    expect(getAllGuestsSpy).toHaveBeenCalled();
    expect(handleResponseFailureSpy).toHaveBeenCalled();
    expect(processHttpErrorsSpy).toHaveBeenCalled();

    expect(component.emitError.getValue().apiErrors[0].code).toEqual('"500"');
    expect(component.hasErrors).toEqual(true);
    expect(component.emitError.getValue()).not.toBeNull();
  });

  it('should handle 204 No data', () => {
    const handleSuccessfulResponseSpy = spyOn<any>(component, 'handleSuccessfulResponse').and.callThrough();
    const getAllGuestsSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    const processHttpErrorsSpy = spyOn<any>(TestBed.get(AllGuestsService), 'getAllGuests').and.returnValue(of(null));
    expect(component.hasErrors).not.toBeDefined();
    expect(component.emitError.getValue()).toBeNull();
    component.ngOnChanges({
      isActiveTab: new SimpleChange(null, true, false)
    });
    fixture.detectChanges();

    expect(getAllGuestsSpy).toHaveBeenCalled();
    expect(handleSuccessfulResponseSpy).toHaveBeenCalled();
    expect(processHttpErrorsSpy).toHaveBeenCalled();

    expect(component.hasErrors).toEqual(true);
    expect(component.emitError.getValue().hasNoData).toEqual(true);
    expect(component.emitError.getValue()).not.toBeNull();
  });

  it('should handle perform #onSearch when search string changes and send correct  searchBy query to the API', () => {
    const getAllGuestsSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();

    component.onSearch('Test');
    fixture.detectChanges();

    expect(component.search).toEqual('Test');
    expect(getAllGuestsSpy).toHaveBeenCalledWith({page: 1, pageSize: component.gridState.take, searchBy: component.search});
  });

  it('should check handleGuestSelection when guest is selected from the preview', () => {
    const getAllGuestsSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    component.handleGuestSelection('Test');
    expect(getAllGuestsSpy).toHaveBeenCalledWith({ page: 1, pageSize: component.gridState.take, searchBy: 'Test' });
  });

  it('should check checkForCalendarDates false case', () => {
    component.calendarIsEmpty = false;
    spyOn(component, 'calendarHasDates').and.returnValue(false);
    component.checkForCalendarDates(MOCK_QUERY_DATA);
    expect(component.calendarIsEmpty).toBeTruthy();
  });

  it('should check checkForCalendarDates true case', () => {
    component.allGuestsForm.patchValue({
      dateFormat: 'MMDDYY',
      arrivalDate: new Date().toISOString(),
      departureDate: new Date().toISOString()
    });
    component.calendarIsEmpty = false;
    spyOn(component, 'calendarHasDates').and.returnValue(false);
    const newQuery = component.checkForCalendarDates(MOCK_QUERY_DATA);
    const start = newQuery.hasOwnProperty('startDate');
    const end = newQuery.hasOwnProperty('endDate');
    expect(start).toBeTruthy();
    expect(end).toBeTruthy();
  });

  it('should check changeDatePickerValue method', () => {
    const getAllGuestsSpy = spyOn<any>(component, 'getAllGuests').and.callThrough();
    component.changeDatePickerValue();
    expect(getAllGuestsSpy).toHaveBeenCalled();
  });

  it('should check calendarHasDates method', () => {
    component.allGuestsForm.patchValue({
      arrivalDate: new Date().toISOString(),
      departureDate: new Date().toISOString()
    });
    const res = component.calendarHasDates();
    expect(res).toBeTruthy();
    component.allGuestsForm.patchValue({
      arrivalDate: new Date().toISOString(),
      departureDate: null
    });
    const result = component.calendarHasDates();
    expect(result).toBeFalsy();
  });

  it('should check resetCalled method', () => {
    component.allGuestsForm.patchValue({
      arrivalDate: new Date().toISOString(),
    });
    spyOn<any>(component, 'calendarHasDates').and.returnValue(true);
    const changeDatePickerValueSpy = spyOn<any>(component, 'changeDatePickerValue');
    component.resetCalled(true);
    expect(changeDatePickerValueSpy).toHaveBeenCalled();
    const test = component.allGuestsForm.get('arrivalDate').value;
    expect(test).toBeNull();
  });

});
