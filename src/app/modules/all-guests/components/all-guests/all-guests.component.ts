import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { cloneDeep } from 'lodash';
import { State } from '@progress/kendo-data-query';

import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import * as moment from 'moment';
import { AllGuestsService } from '@modules/all-guests/services/all-guests.service';
import { AllGuestsQueryParams } from '@modules/all-guests/interfaces/all-guests-query-params.interface';
import { AllGuestsResponse } from '@modules/all-guests/interfaces/guest-reservation.interface';
import { defaultGridState } from '@modules/all-guests/constants/all-guests.constants';
import { HttpErrorResponse } from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import { SessionStorageService } from 'ihg-ng-common-core';
import {ConfigDates} from '@modules/ihg-date-range/models/datepicker-config.model';

@Component({
  selector: 'app-all-guests',
  templateUrl: './all-guests.component.html',
  styleUrls: ['./all-guests.component.scss']
})
export class AllGuestsComponent extends AppErrorBaseComponent implements OnInit, OnChanges {
  @Input() isActiveTab: boolean;
  @Input() topic: string;

  search = '';
  allGuestResponse: AllGuestsResponse;
  gridState: State = cloneDeep(defaultGridState);
  calendarIsEmpty = true;
  dateConfigObject: ConfigDates = {
    'hotelDate': undefined,
    'closeOnDepartureClick': true,
    'minDays': 14,
    'maxDays': 14,
  };
  calendarFormat = 'YYYY-MM-DD'; // Format for the service call

  allGuestsForm = new FormGroup ({
    arrivalDate: new FormControl(),
    departureDate: new FormControl(),
    dateFormat: new FormControl(),
  });

  constructor(
    private allGuestsService: AllGuestsService,
    private storageService: SessionStorageService) {
    super();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isActiveTab && changes.isActiveTab.currentValue && !changes.isActiveTab.isFirstChange()) {
      this.getAllGuests({
        page: 1, pageSize: this.gridState.take,
        searchBy: this.search,
        sortBy: this.getGeneralSort(),
        sortDirection: this.getSortDirection()
      });
    }
  }

  getGeneralSort = () => this.gridState && this.gridState.sort ?
    `${this.gridState.sort[0].field === 'guestName' ? 'lastName,firstName' : this.gridState.sort[0].field}` : '';

  getSortDirection = () => this.gridState && this.gridState.sort ? `${this.gridState.sort[0].dir}` : '';

  onStateChange(query: AllGuestsQueryParams) {
    if (query.sortBy && query.sortDirection) {
      this.gridState.sort[0].field = query.sortBy;
      this.gridState.sort[0].dir = query.sortDirection as 'asc' | 'desc';
    }
    this.getAllGuests({ ...query, ...{ searchBy: this.search } });
  }

  private getAllGuests(query: AllGuestsQueryParams = { page: 1, pageSize: 10 }) {
    this.allGuestsService.getAllGuests(this.checkForCalendarDates(query))
      .subscribe(allReservations => {
          this.handleSuccessfulResponse(allReservations);
        },
        (error) => {
          this.handleResponseFailure(error);
        });
  }

  onSearch(search: string) {
    this.search = search;
    this.getAllGuests({ page: 1, pageSize: this.gridState.take, searchBy: this.search });
  }

  handleGuestSelection(search: string) {
    this.gridState.skip = 0;
    this.getAllGuests({ page: 1, pageSize: this.gridState.take, searchBy: search });
  }

  private handleSuccessfulResponse(response: AllGuestsResponse) {
    this.checkIfAnyApiErrors(response, data => !data || !data.items || data.items.length === 0);
    this.allGuestResponse = response;
    if (this.allGuestResponse && this.allGuestResponse.localCurrentDate) {
      this.dateConfigObject.hotelDate = this.allGuestResponse.localCurrentDate.toString();
    }
  }

  private handleResponseFailure(error: HttpErrorResponse) {
    this.allGuestResponse = null;
    super.processHttpErrors(error);
  }

  checkForCalendarDates(query: AllGuestsQueryParams): AllGuestsQueryParams {
    if (this.calendarHasDates()) {
      const inputFormat = this.allGuestsForm.get('dateFormat').value;
      query.startDate = moment(this.allGuestsForm.get('arrivalDate').value, inputFormat).format(this.calendarFormat);
      query.endDate = moment(this.allGuestsForm.get('departureDate').value, inputFormat).format(this.calendarFormat);
      this.calendarIsEmpty = false;
    } else {
      this.calendarIsEmpty = true;
    }
    return query;
  }

  changeDatePickerValue() {
    this.getAllGuests({page: 1, pageSize: this.gridState.take, searchBy: this.search, sortBy: this.getGeneralSort(), sortDirection: this.getSortDirection()});
  }

  calendarHasDates = (): boolean => this.allGuestsForm.get('arrivalDate').value && this.allGuestsForm.get('departureDate').value;

  resetCalled(resetCalendar) {
    if (resetCalendar && this.calendarHasDates()) {
      this.allGuestsForm.reset();
      this.changeDatePickerValue();
    }
  }

  isSearched = (): boolean => !this.calendarIsEmpty || !!this.search;
}
