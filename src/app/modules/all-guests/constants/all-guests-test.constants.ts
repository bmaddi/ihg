import * as moment from 'moment';
import { AppConstants } from '@app/constants/app-constants';

const mockDate = (numberOfDays: number = 0): string => {
  return numberOfDays ? moment().startOf('day').add(numberOfDays, 'days').format(AppConstants.DB_DT_FORMAT) :
    moment().startOf('day').format(AppConstants.DB_DT_FORMAT);
};


export const allGuestsMockResponse = {
  'localCurrentDate': mockDate(),
  'pageNumber': 1,
  'pageSize': 10,
  'totalPages': 40,
  'totalItems': 400,
  'startItem': 1,
  'endItem': 10,
  'items': [
    {
      'firstName': 'JOHN',
      'lastName': 'DOE',
      'badges': [
        'gold',
        'karma',
        'spire'
      ],
      'loyaltyId': '150765771',
      'ihgRcNumber': '150765771',
      'reservationNumber': '44002733',
      'deparatureDate': mockDate(1),
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '1210',
      'roomType': 'KNGN',
      'checkInDate': mockDate(),
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': 'DUEIN',
      'prepStatus': 'In progress'
    },
    {
      'firstName': 'Sruthisdfgsdfgdgf',
      'lastName': 'Kuruvilladfgsdfgsdfg',
      'badges': [
        'gold',
        'karma',
        'spire',
        'ambassador'
      ],
      'loyaltyId': '150765771',
      'deparatureDate': '2019-11-22',
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '1210 KNGN',
      'checkInDate': '2019-11-21',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': 'RESERVED',
      'ihgRcNumber': '150765771',
      'reservationNumber': null,
      'prepStatus': 'In progress'
    },
    {
      'firstName': 'Tim',
      'lastName': 'Smith',
      'badges': [
        'gold',
        'karma',
        'spire'
      ],
      'loyaltyId': '150765771',
      'deparatureDate': '2019-12-02',
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '1210 KNGN',
      'checkInDate': '2019-12-02',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': 'NOSHOW',
      'ihgRcNumber': '150765771',
      'reservationNumber': null,
      'prepStatus': 'In progress'
    },
    {
      'firstName': 'SuperLongName',
      'lastName': 'ReallyLongLastName',
      'badges': [
        'gold',
        'karma'
      ],
      'loyaltyId': '150765771',
      'deparatureDate': mockDate(3),
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '1210 KNGN',
      'checkInDate': mockDate(2),
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': '',
      'ihgRcNumber': '150765771',
      'reservationNumber': null,
      'prepStatus': 'In progress'
    }
  ]
};

export const allGuestsNoDataMockResponse = {
  'localCurrentDate': mockDate(),
  'pageNumber': 0,
  'pageSize': 0,
  'totalPages': 0,
  'totalItems': 0,
  'startItem': 0,
  'endItem': 0,
  'items': [
  ]
};

export const MOCK_QUERY_DATA = {
  page: 1,
  pageSize: 10,
  sortBy: 'firstname',
  sortDirection: 'asc',
  searchBy: '',
  startDate: new Date().toISOString(),
  endDate: new Date().toISOString()
};
