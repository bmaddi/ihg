import { State } from '@progress/kendo-data-query';

import { GridSettings } from 'ihg-ng-common-kendo';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';


export const viewPortColumnWidths = {
  desktop: {
    guestNameWidth: 170,
    profileWidth: 55,
    loyaltyWidth: 90,
    arrivalWidth: 105,
    departureWidth: 105,
    groupWidth: 160,
    rateWidth: 65,
    roomWidth: 90,
    statusWidth: 140,
    actionsWidth: 90,
  }
};

export const defaultGridState: State = {
  skip: 0,
  take: 10,
  sort: [{field: 'checkInDate', dir: 'asc'}],
};

export const defaultGridSettings = <GridSettings>{
  rows: {
    allOption: true,
    title: 'COM_LBL_ROWS',
    values: [10, 25, 50, 100],
    default: 10
  }
};

export const AllGuestPagerTranslations = {
  gridPagerInfo: {
    itemsText: 'LBL_GUESTS',
    noItemsText: 'LBL_NO_GUESTS_TO_DISPLAY'
  }
};

export const noDataErrorConstants = {
  noData: {
    header: 'LBL_NO_GUESTS_TO_DISPLAY',
    message: ''
}
};

export const searchErrorConstants = {
  noData: {
    header: GUEST_CONST.KEYMAP.noSearchResultsTitle,
    message: GUEST_CONST.KEYMAP.noSearchResultsText
  }
};
