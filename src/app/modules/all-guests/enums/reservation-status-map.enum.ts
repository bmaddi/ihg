export enum ReservationStatusMap {
  RESERVED = 'Reserved',
  PROSPECT = 'Prospect',
  NOSHOW = 'No Show',
  CANCELED = 'Canceled',
  INHOUSE = 'In House',
  CHECKEDOUT = 'Checked Out',
  CHANGED = 'Changed',
  WAITLISTED = 'Wait Listed',
  CHECKEDIN = 'Checked In',
  PRECHECKEDIN = 'Pre Checked In',
  DUEOUT = 'Due Out',
  DUEIN = 'Due In',
}

export enum ReservationStatus {
  RESERVED = 'RESERVED',
  PROSPECT = 'PROSPECT',
  NOSHOW = 'NOSHOW',
  CANCELED = 'CANCELED',
  INHOUSE = 'INHOUSE',
  CHECKEDOUT = 'CHECKEDOUT',
  CHANGED = 'CHANGED',
  WAITLISTED = 'WAITLISTED',
  CHECKEDIN = 'CHECKEDIN',
  PRECHECKEDIN = 'PRECHECKEDIN',
  DUEOUT = 'DUEOUT',
  DUEIN = 'DUEIN',
}
