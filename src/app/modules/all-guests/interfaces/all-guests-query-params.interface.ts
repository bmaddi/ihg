export interface AllGuestsQueryParams {
  page?: number;
  pageSize?: number;
  sortBy?: string;
  sortDirection?: string;
  searchBy?: string;
  startDate?: string;
  endDate?: string;
}
