import { GuestGridModel } from '@modules/guests/models/guest-list.models';

export interface GuestReservation extends GuestGridModel {
  loyaltyId: string;
  firstName: string;
  lastName: string;
  guestName: string;
  badges: string[];
  groupName: string;
  companyName?: string;
  groupCompanyName?: string;
  ihgRcNumber: string;
  reservationNumber: string;
  checkInDate: string;
  deparatureDate: string;
  roomNumber: string;
  roomType: string;
  reservationStatus: string;
  rateCategoryCode?: string;
  prepStatus: string;
  ineligibleReservation?: boolean;
  roomCount: number;
  paymentType: string;
}

export interface AllGuestsResponse {
  localCurrentDate: string | Date;
  pageNumber: number;
  pageSize: number;
  totalPages: number;
  totalItems: number;
  startItem: number;
  endItem: number;
  items: GuestReservation[];
}
