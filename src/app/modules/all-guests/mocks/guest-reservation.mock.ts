import { GuestReservation } from '../interfaces/guest-reservation.interface';
import { HttpHeaders } from '@angular/common/http';

export const MOCK_GUEST_RESERVATION_DATA: GuestReservation = {
    loyaltyId: '',
    firstName: 'Jack',
    lastName: 'Smith',
    guestName: 'Jack Smith',
    badges: [],
    groupName: '',
    ihgRcNumber: '',
    reservationNumber: '25289265',
    checkInDate: '2020-01-21',
    deparatureDate: '2020-01-22',
    roomNumber: '',
    roomType: '',
    reservationStatus: '',
    rateCategoryCode: 'IGCOR',
    prepStatus: '',
    roomCount: 1,
    paymentType: 'Card',
    badgeIconPaths: [],
    ineligibleReservation: false,
    arrival: '',
    arrivalTime: '',
    nights: '',
};


export const MOCK_SERVER_ERROR =  {
    httpError: {
        headers: new HttpHeaders(),
        status: 500,
        statusText: 'Server Error',
        url: 'http://test.com',
        ok: false,
        name: 'HttpErrorResponse',
        message: 'Http failure response for http://test.com',
        type: 4,
        error: {
            'message': 'Internal Server Error',
            'code': 'ERR-500',
        }
    }
};
