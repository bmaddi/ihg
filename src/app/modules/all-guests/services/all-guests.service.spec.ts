import { TestBed } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { AllGuestsService } from './all-guests.service';
import { ApiService, EnvironmentService, SessionStorageService, UserService } from 'ihg-ng-common-core';
import { environmentServiceServiceStub, mockAPIURL } from '@modules/guest-relations/mocks/guest-relations.mock';
import { HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { AllGuestsResponse } from '@modules/all-guests/interfaces/guest-reservation.interface';
import { allGuestsMockResponse } from '@modules/all-guests/constants/all-guests-test.constants';
import { GuestListService } from '@modules/guests/services/guest-list.service';

describe('AllGuestsService', () => {
  const endPoint = `${mockAPIURL}`;
  const expectedEndPointCounts = `${endPoint}allGuests`;
  const checkAPICall = (req: HttpRequest<any>) => {
    return req.method === 'GET' && req.url.indexOf(expectedEndPointCounts) !== -1;
  };
  const fakeApiCall = (mockResponse: AllGuestsResponse | any, error: boolean = false, params?:string[]) => {
    request = httpMockController.expectOne(checkAPICall);
    expect(request.request.method).toBe('GET');

    if (params) {
      params.forEach(p => expect(request.request.params.get(p)).toBeTruthy());
    } else {
      expect(request.request.params.get('page')).toBeTruthy();
      expect(request.request.params.get('pageSize')).toBeTruthy();
    }
    error ? request.error(mockResponse) : request.flush(mockResponse);
    httpMockController.verify();
  };

  let service: AllGuestsService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;

  beforeEach(() => {

    TestBed.overrideProvider(EnvironmentService, {
      useValue: environmentServiceServiceStub
    })
      .overrideProvider(SessionStorageService, {
        useValue: {}
      })
      .overrideProvider(GuestListService, {
        useValue: {}
      })
      .configureTestingModule({
        declarations: [],
        providers: [ApiService, EnvironmentService, UserService, SessionStorageService],
        imports: [HttpClientTestingModule]
      });

    service = TestBed.get(AllGuestsService);
    httpMockController = TestBed.get(HttpTestingController);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return Observable AllGuestsResponse', () => {

    const mockResponse = cloneDeep(allGuestsMockResponse);

    service.getAllGuests({page: 1, pageSize: 10}).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse);
  });

  it('should call mapper function on successful Observable AllGuestsResponse response', () => {
    const dataMapperFuncSpy = spyOn<any>(service, 'mapAllGuestData').and.callThrough();
    const mockResponse = cloneDeep(allGuestsMockResponse);

    service.getAllGuests({page: 1, pageSize: 10}).subscribe(data => {
      expect(data).toBeDefined();
      expect(dataMapperFuncSpy).toHaveBeenCalled();
    });
    fakeApiCall(mockResponse);
  });

  it('should properly map AllGuestsResponse response items if available', () => {
    const mockResponse = cloneDeep(allGuestsMockResponse);
    mockResponse.items.forEach(s => {
      expect(s.guestName).not.toBeDefined();
      expect(s.guestName).not.toBeDefined();
    });
    const mapped = service['mapAllGuestData'](mockResponse);
    mockResponse.items.forEach(s => {
      expect(s.guestName).toEqual(`${s.lastName}, ${s.firstName}`);
      expect(s.groupCompanyName).toEqual(`${s.groupName} / ${s.companyName}`);
    });
  });

  it('should append all requested query parameters -searchBy, sortBy, sortDirection', () => {

    const mockResponse = cloneDeep(allGuestsMockResponse);

    const mockRequest = {page: 1, pageSize: 10, searchBy: 'Test String', sortBy: 'arrival', sortDirection: 'asc'};
    service.getAllGuests(mockRequest).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse, false, Object.keys(mockRequest));
  });

});
