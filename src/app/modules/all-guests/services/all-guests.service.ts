import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { omitBy } from 'lodash';

import { ApiService, ApiUrl, EnvironmentService, SessionStorageService } from 'ihg-ng-common-core';

import { AllGuestsQueryParams } from '@modules/all-guests/interfaces/all-guests-query-params.interface';
import { AllGuestsResponse, GuestReservation } from '@modules/all-guests/interfaces/guest-reservation.interface';
import { BadgeIconPaths } from '@app/constants/badge-icon-paths';
import { AppConstants } from '@app/constants/app-constants';
import { GuestTabFlag } from '@modules/guests/models/guest-list.models';
import { checkForIneligibleReservation } from '@modules/guests/functions/guest-arrivals.functions';
import { GuestListService } from '@modules/guests/services/guest-list.service';

@Injectable({
  providedIn: 'root'
})
export class AllGuestsService {
  readonly memberIdStorageKey = 'guestProfileMemberId';

  constructor(private apiService: ApiService,
              private environmentService: EnvironmentService,
              private storageService: SessionStorageService,
              private guestList: GuestListService) {
  }

  getAllGuests(params: AllGuestsQueryParams): Observable<AllGuestsResponse> {
    const queryParams: AllGuestsQueryParams = omitBy(params, value => !value);
    return this.apiService.get(
      new ApiUrl(this.environmentService.getEnvironmentConstants()['fdkAPI'], 'allGuests'),
      queryParams as { [key: string]: string },
    ).pipe(
      map((data: AllGuestsResponse) => this.mapAllGuestData(data)));
  }

  private mapAllGuestData(data: AllGuestsResponse): AllGuestsResponse {
    if (data) {
      data.localCurrentDate = moment(data.localCurrentDate, AppConstants.DB_DT_FORMAT).toDate();
      if (data.items) {
        const separator = ' / ';
        data.items.forEach(res => {
            res.groupCompanyName = `${res.groupName}${res.groupName && res.companyName ? separator + res.companyName : res.companyName}`;
            res.guestName = `${res.lastName}, ${res.firstName}`;
            res.badges = this.sortBadges(res.badges);
            res.ineligibleReservation = checkForIneligibleReservation(res.roomCount, res.paymentType);
          }
        );
      }
    }
    return data;
  }

  private sortBadges(badges: string[]): string[] {
    if (badges.length) {
      return Object.keys(BadgeIconPaths)
        .reduce((badgeList, badgeCode) => {
          if (badges.indexOf(badgeCode) !== -1) {
            badgeList.push(badgeCode);
          }
          return badgeList;
        }, []);
    } else {
      return badges;
    }
  }

  setMemberProfile(memberId: number) {
    this.storageService.setSessionStorage(this.memberIdStorageKey, memberId);
  }

  emitGuestTabEvent(newData: GuestTabFlag) {
    this.guestList.emitGuestTabEvent(newData);
  }

  emitUserDataSource(newData: GuestReservation) {
    this.guestList.userDataSource.next(newData);
  }
}
