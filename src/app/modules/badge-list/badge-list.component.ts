import { Component, Input, OnInit } from '@angular/core';

import { TOOL_TIP_DATA } from '@modules/prepare/constants/arrivals-constants';

@Component({
  selector: 'app-badge-list',
  templateUrl: './badge-list.component.html',
  styleUrls: ['./badge-list.component.scss']
})
export class BadgeListComponent implements OnInit {
  @Input() badges: string[];
  @Input() usePrintBadges = false;

  public tooltipMap: { [index: string]: string } = {};
  public badgeExtension = '.svg';

  constructor() {
  }

  ngOnInit() {
    this.checkForPrintBadges();
    this.setBadgeTooltips();
  }

  private setBadgeTooltips() {
    TOOL_TIP_DATA.KEYMAP.forEach(item => {
      this.tooltipMap[item.badge] = item.tooltipLabel;
    });
  }

  private checkForPrintBadges() {
    if (this.usePrintBadges) {
      this.badgeExtension = '_grayscale.svg';
    }
  }
}
