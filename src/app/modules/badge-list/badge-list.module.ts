import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BadgeListComponent } from './badge-list.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NgbModule
  ],
  declarations: [
    BadgeListComponent
  ],
  exports: [
    BadgeListComponent
  ]
})
export class BadgeListModule { }
