import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { IhgNgCommonPagesModule } from 'ihg-ng-common-pages';

import { AppSharedModule } from '@app-shared/app-shared.module';
import { CheckInRoutingModule } from '@modules/check-in/check-in.routes';
import { CheckInWorkflowModule } from '@modules/check-in/components/check-in-workflow/check-in-workflow.module';
import { CheckInComponent } from '@modules/check-in/components/check-in.component';
import { CheckInInfoComponent } from './components/check-in-info/check-in-info.component';
import { HotelFeaturesModule } from '@modules/check-in/components/hotel-features/hotel-features.module';
import { CheckInPrepStatusComponent } from './components/check-in-prep-status/check-in-prep-status.component';
import { RoomAndRateComponent } from './components/room-and-rate/room-and-rate.component';
import { TalkAboutModule } from '@app/modules/check-in/components/talk-about/talk-about.module';
import { ReservationSummaryComponent } from './components/reservation-summary/reservation-summary.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { CostEstimateComponent } from './components/cost-estimate/cost-estimate.component';
import { ProgressStatusModule } from '@modules/progress-status/progress-status.module';
import { CommentsModule } from '../comments/comments.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    TranslateModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    IhgNgCommonPagesModule,
    GridModule,
    AppSharedModule,
    CheckInRoutingModule,
    CheckInWorkflowModule,
    HotelFeaturesModule,
    TalkAboutModule,
    CommentsModule,
    DeviceDetectorModule.forRoot(),
    ProgressStatusModule
  ],
  declarations: [
    CheckInComponent,
    CheckInInfoComponent,
    CheckInPrepStatusComponent,
    RoomAndRateComponent,
    ReservationSummaryComponent,
    CostEstimateComponent
  ],
  entryComponents: [
    CostEstimateComponent
  ]
})
export class CheckInModule {
}
