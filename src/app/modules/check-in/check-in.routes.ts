import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TransitionGuardService } from 'ihg-ng-common-core';
import { CheckInComponent } from '../check-in/components/check-in.component';

const routes: Routes = [
  {
    path: 'check-in',
    component: CheckInComponent,
    canDeactivate: [
      TransitionGuardService
    ],
    data: {
      crumb: ['dashboard', 'guest', 'guest-list-details'],
      pageTitle: 'LBL_CHK_IN',
      stateName: 'check-in',
      slnmId: 'CheckIn-SID',
      maxWidth: true,
      noSubtitle: true,
      showHelp: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CheckInRoutingModule {
}
