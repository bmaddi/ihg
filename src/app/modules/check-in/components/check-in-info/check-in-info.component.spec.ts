import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { DetachedToastMessageService, ToastMessageOutletService } from 'ihg-ng-common-core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { of, throwError, Subject } from 'rxjs';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { OffersService } from '@modules/offers/services/offers.service';
import {
  AnalyticsTrackingService
} from '@app/modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';

import { RouterTestingModule } from '@angular/router/testing';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckInInfoComponent } from './check-in-info.component';
import { MOCK_RESERVATION } from '../../mocks/check-in-info-mock';

import * as cloneDeep from 'lodash/cloneDeep';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

export class MockDetachedToastMessageService {
}

export class MockToastMessageOutletService {
}
export class MockCheckInService {
  public formatGuestName(guestData: ReservationDataModel) {
    const guestName = 'Test User';
    return guestName;
  }

  public setRefreshPointSubject() {
  }
  public openEnrollmentModal() {
  }
}
export class MockOffersService {
}
export class MockGuestListService {
  public setMemberProfile() {
  }
}
export class MockAnalyticsTrackingService {
  public trackClickProfileIcon() {
  }
}

describe('CheckInInfoComponent', () => {
  let component: CheckInInfoComponent;
  let fixture: ComponentFixture<CheckInInfoComponent>;
  let checkInService, guestListService, trackingService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInInfoComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [ RouterTestingModule,
                  FormsModule, ReactiveFormsModule, NgbModule.forRoot(), HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [UserService, TranslateService,
        { provide: DetachedToastMessageService, useClass: MockDetachedToastMessageService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: ToastMessageOutletService, useClass: MockToastMessageOutletService },
        { provide: CheckInService, useClass: MockCheckInService },
        { provide: OffersService, useClass: MockOffersService },
        { provide: GuestListService, useClass: MockGuestListService },
        { provide: AnalyticsTrackingService, useClass: MockAnalyticsTrackingService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInInfoComponent);
    component = fixture.componentInstance;
    checkInService = TestBed.get(CheckInService);
    guestListService = TestBed.get(GuestListService);
    trackingService = TestBed.get(AnalyticsTrackingService);
    component.guestData = cloneDeep(MOCK_RESERVATION);
    fixture.detectChanges();
  });

  it('should create CheckInInfoComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should call checkInService setRefreshPointSubject function from refreshForPointsPost function', () => {
    spyOn(checkInService, 'setRefreshPointSubject');
    component.refreshForPointsPost();
    expect(checkInService.setRefreshPointSubject).toHaveBeenCalled();
  });

  it('should call setMemberProfile and trackClickProfileIcon function from setMemberProfile function', () => {
    const loyaltyId = '1234';
    const stateName = 'state';
    spyOn(guestListService, 'setMemberProfile');
    spyOn(trackingService, 'trackClickProfileIcon');
    component.setMemberProfile(loyaltyId);
    expect(guestListService.setMemberProfile).toHaveBeenCalled();
    expect(trackingService.trackClickProfileIcon).toHaveBeenCalled();
  });

  it('should call checkInService openEnrollmentModal function from openEnrollmentModal function', () => {
    spyOn(checkInService, 'openEnrollmentModal');
    component.openEnrollmentModal();
    expect(checkInService.openEnrollmentModal).toHaveBeenCalled();
  });

  it('should test manage stay icon is not visible for group reservation', () => {
    component.guestData.groupName = "123456";
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('.mng-stay-btn'));
    expect(element).toBeNull();
  });

  it('should test manage stay icon is visible if it is not a group reservation', () => {
    const element = fixture.debugElement.query(By.css('.mng-stay-btn')).nativeElement;
    expect(element).toBeTruthy();
  });
});
