import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { GuestListService } from '@modules/guests/services/guest-list.service';
import { BADGE_LABELS } from '@modules/check-in/constants/check-in-constants';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { OffersService } from '@modules/offers/services/offers.service';
import {
  AnalyticsTrackingService
} from '@app/modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';

@Component({
  selector: 'app-check-in-info',
  templateUrl: './check-in-info.component.html',
  styleUrls: ['./check-in-info.component.scss']
})
export class CheckInInfoComponent implements OnInit, OnDestroy {
  public badgeLabels = BADGE_LABELS;
  public characterCount = 15;
  public guestName = '';
  private stateName: string;
  private rootSubscription = new Subscription();

  @Input() guestData: ReservationDataModel;

  constructor(
    private gaService: GoogleAnalyticsService,
    private guestListService: GuestListService,
    private checkInService: CheckInService,
    private activatedRoute: ActivatedRoute,
    private trackingService: AnalyticsTrackingService,
    private router: Router,
    private offersService: OffersService
  ) {
  }

  ngOnInit() {
    this.setStateName();
    this.setGuestName();
  }

  private setStateName() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.stateName = data.stateName;
    });
  }

  private setGuestName() {
    this.guestName = this.checkInService.formatGuestName(this.guestData);
  }

  public refreshForPointsPost() {
    this.checkInService.setRefreshPointSubject();
  }

  public setMemberProfile(loyaltyId: string) {
    this.trackingService.trackClickProfileIcon(this.stateName);
    this.guestListService.setMemberProfile(+loyaltyId);
  }

  public openEnrollmentModal(): void {
    this.checkInService.openEnrollmentModal(this.guestData);
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.GA_ENROLL_LNK,
      CheckinAnalyticsConstants.SELECTED_CHECK_IN);
  }

  public navigateToManageStay(): void {
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.GA_MNG_STAY,
      CheckinAnalyticsConstants.SELECTED_CHECK_IN);
    this.rootSubscription.add(this.offersService.showAllOffers(false));
    this.router.navigate(['/edit-stay', this.guestData.reservationNumber]);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
