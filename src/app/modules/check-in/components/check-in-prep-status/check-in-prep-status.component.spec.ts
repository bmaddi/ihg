import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CheckInPrepStatusComponent } from './check-in-prep-status.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

xdescribe('CheckInPrepStatusComponent', () => {
  let component: CheckInPrepStatusComponent;
  let fixture: ComponentFixture<CheckInPrepStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInPrepStatusComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInPrepStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
