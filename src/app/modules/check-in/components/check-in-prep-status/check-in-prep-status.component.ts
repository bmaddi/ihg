import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { PREP_STATUSES } from '@modules/check-in/constants/check-in-constants';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ROOM_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';

@Component({
  selector: 'app-check-in-prep-status',
  templateUrl: './check-in-prep-status.component.html',
  styleUrls: ['./check-in-prep-status.component.scss']
})
export class CheckInPrepStatusComponent implements OnInit, OnDestroy {
  public checkInInfo$: Subscription = new Subscription();

  @Input() guestData: ReservationDataModel;

  constructor(private roomsService: RoomsService, private prepareArrivalsService: ArrivalsService) {
  }

  ngOnInit() {
    this.setCheckedInStatus();
    this.initializePrepStatus();
    this.subscribeRefreshTasksStatus();
    this.subscribeToRoomSet();
  }

  private setCheckedInStatus(): void {
    this.guestData.isCheckedIn = this.guestData.prepStatus === PREP_STATUSES.checkedIn;
  }

  private initializePrepStatus(): void {
    if (!this.guestData.prepStatus) {
      this.updatePrepStatus();
    }
  }

  private subscribeToRoomSet(): void {
    this.checkInInfo$.add(this.roomsService.getRoom().subscribe(() => {
      this.updatePrepStatus();
    }));
  }

  private subscribeRefreshTasksStatus(): void {
    this.checkInInfo$.add(this.prepareArrivalsService.refreshTasksStatus.subscribe(() => {
      this.updatePrepStatus();
    }));
  }

  private updatePrepStatus(): void {
    if (!this.guestData.isCheckedIn) {
      if (this.isRoomNotAssigned()) {
        this.checkTasksStatus();
      } else {
        this.checkRoomStatus();
      }
    }
  }

  private isRoomNotAssigned(): boolean {
    return !this.guestData.assignedRoomNumber;
  }

  private checkTasksStatus(): void {
    if (this.hasTasks() && this.hasResolvedTasks()) {
      this.setPrepStatus(PREP_STATUSES.inProgress);
    } else {
      this.setPrepStatus(PREP_STATUSES.notStarted);
    }
  }

  private hasTasks(): boolean {
    return !!(this.guestData.tasks && this.guestData.tasks.length);
  }

  private hasResolvedTasks(): boolean {
    return this.guestData.tasks.some(item => item.isResolved);
  }

  private checkRoomStatus(): void {
    if (this.isRoomVacant() && this.isRoomCleanOrInspected()) {
      this.setPrepStatus(PREP_STATUSES.ready);
    } else {
      this.setPrepStatus(PREP_STATUSES.inProgress);
    }
  }

  private isRoomVacant(): boolean {
    const status = this.guestData.assignedRoomStatus;
    return status ? status.frontOfficeStatus === ROOM_STATUS.Vacant.value : false;
  }

  private isRoomCleanOrInspected(): boolean {
    if (this.guestData.assignedRoomStatus && this.guestData.inspected) {
      return this.guestData.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value;
    }
    return this.guestData.assignedRoomStatus && (this.guestData.assignedRoomStatus.roomStatus === ROOM_STATUS.Clean.value
      || this.guestData.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value);
  }

  private setPrepStatus(value: string): void {
    this.guestData.prepStatus = value;
  }

  ngOnDestroy() {
    this.checkInInfo$.unsubscribe();
  }
}
