import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { CheckInWorkflowComponent } from './components/check-in-workflow.component';
import { CheckInReservationComponent } from './components/check-in-reservation/check-in-reservation.component';
import { CheckInRoomAssignComponent } from './components/check-in-room-assign/check-in-room-assign.component';
import { PrepareArrivalsDetailsModule } from '@modules/prepare/components/prepare-arrivals-details/prepare-arrivals-details.module';
import { CheckInKeysModule } from '@modules/check-in/components/check-in-workflow/components/check-in-keys/check-in-keys.module';
import { CheckInAmenityComponent } from './components/check-in-amenity/check-in-amenity.component';
import { CheckInPaymentModule } from '@modules/check-in/components/check-in-workflow/components/check-in-payment/check-in-payment.module';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { PrintRecordModule } from '@app/modules/print-record/print-record.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    TranslateModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    PrepareArrivalsDetailsModule,
    CheckInKeysModule,
    CheckInPaymentModule,
    AppSharedModule,
    PrintRecordModule
  ],
  declarations: [
    CheckInWorkflowComponent,
    CheckInReservationComponent,
    CheckInAmenityComponent,
    CheckInRoomAssignComponent
  ],
  exports: [
    CheckInWorkflowComponent
  ]
})
export class CheckInWorkflowModule {
}
