import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CheckInAmenityComponent } from '@app/modules/check-in/components/check-in-workflow/components/check-in-amenity/check-in-amenity.component';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { CheckInWorkflowService } from '../../services/check-in-workflow.service';
import { of } from 'rxjs';
import { googleAnalyticsServiceStub } from '@modules/guest-relations/mocks/guest-relations.mock';
import { Component, Input, Output } from '@angular/core';
import { MOCK_AMENITY_DATA, MOCK_AMENITY_POINTS } from '../../mocks/check-in-amenity-mock';

export class MockCheckInWorkflowService {
  public getBrandCode(): string {
    return 'KIKI';
  }

  public retrieveAmenityPoints(data) {
    return of(MOCK_AMENITY_POINTS);
  }
}

@Component({
  selector: 'app-service-error',
  template: ''
})

export class StubServiceErrorComponent {
  @Input() showSpinner;
  @Input() errorEvent;
  @Input() config;
  @Input() cardType;
  @Input() showReportAnIssue;
  @Output() refreshAction;
}

describe('CheckInAmenityComponent', () => {
  let component: CheckInAmenityComponent;
  let fixture: ComponentFixture<CheckInAmenityComponent>;

  beforeEach(async(() => {
    TestBed.overrideProvider(GoogleAnalyticsService, { useValue: googleAnalyticsServiceStub });
    TestBed.configureTestingModule({
      declarations: [CheckInAmenityComponent, StubServiceErrorComponent],
      providers: [GoogleAnalyticsService, { provide: CheckInWorkflowService, useClass: MockCheckInWorkflowService }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInAmenityComponent);
    component = fixture.componentInstance;
    component.guestData = MOCK_AMENITY_DATA;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test getAmenityPoints method', () => {
    const handleAmenitySpy = spyOn<any>(component, 'handleAmenityResponse').and.callThrough();
    component.getAmenityPoints();
    expect(handleAmenitySpy).toHaveBeenCalledWith(MOCK_AMENITY_POINTS);
  });

});
