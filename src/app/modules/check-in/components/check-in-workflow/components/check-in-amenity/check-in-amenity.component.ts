import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import { AMENITY_MAP } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-constants';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { LoyaltyAmenityDataModel } from '@modules/check-in/components/check-in-workflow/models/check-in-workflow.models';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { GET_AMENITY_ERROR_CONTENT } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-error-constants';

@Component({
  selector: 'app-check-in-amenity',
  templateUrl: './check-in-amenity.component.html',
  styleUrls: ['./check-in-amenity.component.scss']
})
export class CheckInAmenityComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public amenityMap = AMENITY_MAP;
  public initialized = false;
  public errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  public errorConfig = GET_AMENITY_ERROR_CONTENT;

  private amenity$: Subscription = new Subscription();

  @Input() guestData: ReservationDataModel;
  @Output() emitAmenitySelected: EventEmitter<void> = new EventEmitter();

  constructor(private gaService: GoogleAnalyticsService, private workflowService: CheckInWorkflowService) {
    super();
  }

  ngOnInit(): void {
    if (this.isClubMember() || this.isKimptonBrand() || this.isOtaReservation() || !this.guestData.amenityFlag) {
      this.initialized = true;
    } else {
      this.getAmenityPoints();
    }
  }

  private isClubMember(): boolean {
    return this.guestData.badges.some(item => item === 'club');
  }

  private isKimptonBrand(): boolean {
    return this.workflowService.getBrandCode() === 'KIKI';
  }

  private isOtaReservation(): boolean {
    return this.guestData.otaFlag;
  }

  public getAmenityPoints(): void {
    this.amenity$.add(this.workflowService.retrieveAmenityPoints(this.guestData)
      .subscribe((response: LoyaltyAmenityDataModel) => this.handleAmenityResponse(response),
        (error) => {
          super.processHttpErrors(error);
          this.handleErrorResponse();
        }));
  }

  private handleAmenityResponse(response: LoyaltyAmenityDataModel): void {
    if (!super.checkIfAnyApiErrors(response)) {
      this.guestData.amenityPoints = response.loyaltyAmenityPoints ? response.loyaltyAmenityPoints.amenityPoints : null;
      this.initialized = true;
    } else {
      this.handleErrorResponse();
    }
  }

  private handleErrorResponse(): void {
    this.guestData.amenityPoints = null;
    this.initialized = true;
  }

  public selectAmenity(selected: string): void {
    this.guestData.selectedAmenity = selected;
    this.trackChange(selected);
    this.emitAmenitySelected.emit();
  }

  private trackChange(selected: string): void {
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.GA_AMENITY_ACTION, selected);
  }

  ngOnDestroy(): void {
    this.amenity$.unsubscribe();
  }
}
