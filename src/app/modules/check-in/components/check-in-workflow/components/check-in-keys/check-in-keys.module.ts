import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AppSharedModule } from '@app-shared/app-shared.module';

import { CheckInKeysComponent } from './components/check-in-keys.component';
import { KeysComponent } from './components/keys/keys.component';
import { SetValuePlusMinusComponent } from './components/set-value-plus-minus/set-value-plus-minus.component';
import {EncoderModule} from '@modules/encoder/encoder.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    AppSharedModule,
    EncoderModule
  ],
  declarations: [
    CheckInKeysComponent,
    KeysComponent,
    SetValuePlusMinusComponent
  ],
  exports: [
    CheckInKeysComponent
  ]
})
export class CheckInKeysModule {
}
