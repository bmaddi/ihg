import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInKeysComponent } from './check-in-keys.component';

xdescribe('CheckInKeysComponent', () => {
  let component: CheckInKeysComponent;
  let fixture: ComponentFixture<CheckInKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInKeysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
