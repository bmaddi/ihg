import { Component, Input, OnInit, Output, EventEmitter, AfterViewInit, ViewChild } from '@angular/core';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { KeysComponent } from '../components/keys/keys.component';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { CUT_KEY_ERROR_CONTENT } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-error-constants';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { Subscription, of, Subject } from 'rxjs';
import {CUT_KEY_ISSUE_AUTOFILL} from '@app/constants/error-autofill-constants';
import {EncoderModel} from '@modules/encoder/models/encoder.model';

@Component({
  selector: 'app-check-in-keys',
  templateUrl: './check-in-keys.component.html',
  styleUrls: ['./check-in-keys.component.scss']
})
export class CheckInKeysComponent extends AppErrorBaseComponent implements OnInit {

  @Input() guestData: ReservationDataModel;
  @ViewChild(KeysComponent) cutKey;
  public cutKeyAssignmentError = new Subject<EmitErrorModel>();
  public cutKeyErrorContent = CUT_KEY_ERROR_CONTENT;
  public reportIssueAutoFill = CUT_KEY_ISSUE_AUTOFILL;
  selectedEncoder: EncoderModel;

  constructor() {
    super();
   }

  ngOnInit() {
  }

  isCutKeyError (error): void {
    if (error.isError === true) {
      super.processHttpErrors(error.error, this.cutKeyAssignmentError);
    } else {
      super.resetSpecificError(this.cutKeyAssignmentError);
    }
  }

  onCutKeyRetry() {
    this.cutKey.onCutKey();
  }

  encoderSelected(encoder) {
    this.selectedEncoder = encoder as EncoderModel;
  }
}
