import { TranslateService } from '@ngx-translate/core';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { GoogleAnalyticsService, DetachedToastMessageService } from 'ihg-ng-common-core';

import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import {EncoderModel} from '@modules/encoder/models/encoder.model';

@Component({
  selector: 'app-keys',
  templateUrl: './keys.component.html',
  styleUrls: ['./keys.component.scss']
})
export class KeysComponent extends AppErrorBaseComponent implements OnInit {
  public keyCount: number;
  public isKeysCut: boolean;
  @Input() guestData: ReservationDataModel;
  @Input() encoder: EncoderModel;
  @Output() isCutKeyError: EventEmitter<Object> = new EventEmitter();

  constructor(private checkInWorkflowService: CheckInWorkflowService,
              private gaService: GoogleAnalyticsService,
              private toastMessageService: DetachedToastMessageService,
              private translate: TranslateService) {
    super();
  }

  ngOnInit() {
    this.isKeysCut = this.guestData.keysCut;
  }

  onKeyCountSelect (keyCount: number): void {
    this.keyCount = keyCount;
  }

  onCutKey (): void {
    this.checkInWorkflowService.generateKey(this.keyCount, this.guestData.pmsReservationNumber, true, this.encoder.encoderId).subscribe((data: Object) => {
      if (data) {
        this.isKeysCut = true;
        this.guestData.keysCut = true;
        this.guestData.isCutkeyError = false;
        this.isCutKeyError.emit({
            'isError' : false,
            'error' : {}
        });
        this.showCutKeySuccessToast();
      } else {
        this.handleCutKeyError({});
      }
    }, (error) => {
      this.handleCutKeyError(error);
    });
  }

  showCutKeySuccessToast() {
    let cutKetSuccessMsg = '';
    const count = this.keyCount;
    const roomNumber = this.guestData.assignedRoomNumber;

    cutKetSuccessMsg = this.translate.instant('TOAST_CUT_KEY_SUCCESS_NEW', {'keyCount': count, 'roomNumber': roomNumber});
    this.toastMessageService.success(cutKetSuccessMsg, '');
  }

  handleCutKeyError(error) {
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN,
              CheckinAnalyticsConstants.GA_CUT_KEY_ACTION,
              CheckinAnalyticsConstants.GA_CUT_KEY_ERROR);
    this.guestData.isCutkeyError = true;
    this.guestData.keysCut = false;
    this.isCutKeyError.emit({
        'isError' : true,
        'error' : error
    });
  }

  shouldEnableButton = (): boolean => !!this.encoder && !!this.encoder.encoderId;
}
