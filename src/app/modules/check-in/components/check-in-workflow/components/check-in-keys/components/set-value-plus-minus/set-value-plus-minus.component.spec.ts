import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetValuePlusMinusComponent } from './set-value-plus-minus.component';

xdescribe('SetValuePlusMinusComponent', () => {
  let component: SetValuePlusMinusComponent;
  let fixture: ComponentFixture<SetValuePlusMinusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetValuePlusMinusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetValuePlusMinusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
