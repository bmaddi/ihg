import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-set-value-plus-minus',
  templateUrl: './set-value-plus-minus.component.html',
  styleUrls: ['./set-value-plus-minus.component.scss']
})
export class SetValuePlusMinusComponent implements OnInit {

  public keyCount: number = 1;
  public maxKeyCount: number = 4;
  public minKeyCount: number = 1;



  @Output() keyCountSelected: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.keyCountSelected.emit(this.keyCount);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  validateCutKeyCount(event, count): boolean {
    if (this.numberOnly(event)) {
      this.keyCount = null;
      if (Number(event.key) > this.maxKeyCount) {
        this.keyCount = this.maxKeyCount;
        this.emitKeyCount(this.keyCount);
        return false;
      } else if (Number(event.key) < this.minKeyCount) {
        this.keyCount = this.minKeyCount;
        this.emitKeyCount(this.keyCount);
        return false;
      } else {
        this.emitKeyCount(Number(event.key));
        return true;
      }
    } else {
      return false;
    }
  }

  onBlurEvent() {
    if (this.keyCount === null) {
      this.keyCount = 1;
    } else if (this.keyCount > this.maxKeyCount) {
      this.keyCount = this.maxKeyCount;
    } else if (this.keyCount < this.minKeyCount) {
      this.keyCount = this.minKeyCount;
    }
    this.emitKeyCount(Number(this.keyCount));
  }

  increaseValue() {
    if (Number(this.keyCount) < this.maxKeyCount) {
      this.keyCount = this.keyCount + 1;
      this.emitKeyCount(this.keyCount);
    }
  }
  decreaseValue() {
    if (Number(this.keyCount) > this.minKeyCount) {
      this.keyCount = this.keyCount - 1;
      this.emitKeyCount(this.keyCount);
    }
  }
  emitKeyCount(count) {
    this.keyCountSelected.emit(count);
  }
}
