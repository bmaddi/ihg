import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

import { PipesModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { CheckInPaymentComponent } from './components/check-in-payment.component';
import { CreditCardComponent } from './components/credit-card/credit-card.component';
import { PaymentActionsComponent } from './components/payment-actions/payment-actions.component';
import { TalkAboutModule } from '@modules/check-in/components/talk-about/talk-about.module';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { NoPostModule } from '@modules/no-post/no-post.module';
import { CreditCardFormComponent } from './components/credit-card-form/credit-card-form.component';
import { DisplayAuthDetailsComponent } from './components/display-auth-details/display-auth-details.component';
// tslint:disable-next-line:max-line-length
import { NonEditableCreditCardDetailsComponent } from './components/non-editable-credit-card-details/non-editable-credit-card-details.component';
import { PaymentWindowRoutingComponent } from './components/payment-window-routing/payment-window-routing.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    PopupModule,
    TalkAboutModule,
    IhgNgCommonComponentsModule,
    AppSharedModule,
    NoPostModule,
    DatePickerModule,
    DateInputsModule,
    InputsModule,
    DropDownsModule,
    AppSharedModule,
    PipesModule
  ],
  declarations: [
    CheckInPaymentComponent,
    CreditCardComponent,
    PaymentActionsComponent,
    CreditCardFormComponent,
    DisplayAuthDetailsComponent,
    NonEditableCreditCardDetailsComponent,
    PaymentWindowRoutingComponent
  ],
  exports: [
    CheckInPaymentComponent
  ]
})
export class CheckInPaymentModule {
}
