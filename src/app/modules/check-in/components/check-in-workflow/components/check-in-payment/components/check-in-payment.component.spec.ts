import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { of } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConfirmationModalService } from 'ihg-ng-common-components';
import { EnrollNewMemberService } from 'ihg-ng-common-pages';
import { GoogleAnalyticsService, ToastMessageOutletService } from 'ihg-ng-common-core';

import { TruncatedTooltipComponent } from '@app/modules/shared/components/truncated-tooltip/truncated-tooltip.component';
// tslint:disable-next-line:max-line-length
import { CheckInPaymentService } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/services/check-in-payment.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { CheckInPaymentComponent } from './check-in-payment.component';
import {
  mockGuestData,
  mockResourceError,
  mockRoutings,
  outputWhenEditIsFalse,
  outputWhenEditIsTrue
} from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/mock/check-in-payment-mock';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { ActOnContainerComponent } from '@modules/check-in/components/talk-about/components/act-on-container/act-on-container.component';
import { RoutingType } from '@modules/check-in/enums/checkin-payment-routing.enum';
import { PaymentWindowRoutingComponent } from './payment-window-routing/payment-window-routing.component';
import { PayRouting, ReservationDataModel } from '@modules/check-in/models/check-in.models';


describe('CheckInPaymentComponent', () => {
  let checkInPaymentservice: CheckInPaymentService;
  let component: CheckInPaymentComponent;
  let fixture: ComponentFixture<CheckInPaymentComponent>;
  let httpMock: HttpTestingController;
  let gaservice: GoogleAnalyticsService;
  const getMockReservationData = (routingType: RoutingType,
                                  payRoutings: PayRouting[],
                                  rateCode: string = 'JB',
                                  assignedRoomNumber: string = '121'): ReservationDataModel => {
    return {
      ...mockGuestData, ...{
        assignedRoomNumber,
        amenityFlag: true,
        isCheckedIn: false,
        payRoutings,
        rateCode,
        routingType
      }
    };
  };

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        declarations: [
          CheckInPaymentComponent,
          TruncatedTooltipComponent,
          ActOnContainerComponent,
          PaymentWindowRoutingComponent
        ],
        imports: [
          BrowserModule,
          HttpClientModule,
          DatePickerModule,
          CommonTestModule,
          BrowserAnimationsModule
        ],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          ConfirmationModalService,
          CheckInPaymentService,
          GoogleAnalyticsService,
          ToastMessageOutletService,
          EnrollNewMemberService
        ]
      })
      .compileComponents().then(() => {
      setTestEnvironment();
      fixture = TestBed.createComponent(CheckInPaymentComponent);
      component = fixture.componentInstance;
      checkInPaymentservice = TestBed.get(CheckInPaymentService);
      component.guestData = mockGuestData;
      gaservice = TestBed.get(GoogleAnalyticsService);
      fixture.detectChanges();
    });
  }));

  it('should validate Act On Message Box is present', () => {
    component.guestData = {
      ...mockGuestData, ...{
        payRoutings: mockRoutings,
        rateCode: '51',
        routingType: RoutingType.singleUseCC
      }
    };

    component.ngOnInit();
    const actOnContainer = fixture.debugElement.query(By.css('[data-slnm-ihg="ActOn-SID"]'));

    expect(actOnContainer).not.toBeNull();
    expect(actOnContainer.children.length).toEqual(1);
    expect(actOnContainer.children[0].nativeElement.textContent).toContain('LBL_ASGN_RM_IN_TAB');
  });

  it('should validate Act On Message Box matches WF color', () => {
    component.guestData = getMockReservationData(RoutingType.singleUseCC, mockRoutings);

    component.ngOnInit();
    fixture.detectChanges();
    const actOn = fixture.debugElement.query(By.css('[data-slnm-ihg="ActOn-SID"]'));
    const actOnContainer = fixture.debugElement.query(By.css('.act-on-container'));

    expect(actOn).not.toBeNull();
    expect(actOnContainer).not.toBeNull();
    expect(actOnContainer.classes[component.cardTypes.SUCCESS]).toBeTruthy();
    expect(component.actOnCardType).toEqual(component.cardTypes.SUCCESS);
  });

  it('should validate proper messages are shown when windows are present & required for Pay Integrated', () => {;
    component.payIntegratedFlag = 'Y';
    component.guestData = getMockReservationData(RoutingType.singleUseCC, mockRoutings);

    component.ngOnInit();
    fixture.detectChanges();
    const actOn = fixture.debugElement.query(By.css('[data-slnm-ihg="ActOn-SID"]'));

    expect(actOn).not.toBeNull();
    component.routingMessages.multipleWindows.forEach((message, i) => {
      const routingMessage = fixture.debugElement.query(By.css(`[ng-reflect-translate=${message}]`));
      expect(routingMessage).not.toBeNull();
      expect(routingMessage.nativeElement.textContent).toContain(message);
    });
  });

  it('should validate existing messages are shown when reservation is Guest Pay(non single use credit card SUCC)', () => {
    component.payIntegratedFlag = 'Y';
    component.guestData = getMockReservationData(RoutingType.guestPay, mockRoutings);

    component.ngOnInit();
    fixture.detectChanges();
    const actOn = fixture.debugElement.query(By.css('[data-slnm-ihg="ActOn-SID"]'));
    const messages = fixture.debugElement.queryAll(By.css('.pt-2.display-flex'));

    expect(actOn).not.toBeNull();
    component.routingMessages.multipleWindows.forEach((message, i) => {
      const routingMessage = fixture.debugElement.query(By.css(`[ng-reflect-translate=${message}]`));
      expect(routingMessage).toBeNull();
    });
    expect(messages.length).toBeGreaterThan(0);
  });

  it('should validate missing payment window messages show if only 1 is present and others required', () => {
    component.payIntegratedFlag = 'Y';
    component.guestData = getMockReservationData(RoutingType.singleUseCC, mockRoutings.slice(0, 1));

    component.ngOnInit();
    fixture.detectChanges();
    const actOn = fixture.debugElement.query(By.css('[data-slnm-ihg="ActOn-SID"]'));

    expect(actOn).not.toBeNull();
    component.routingMessages.missingWindows.forEach((message, i) => {
      const routingMessage = fixture.debugElement.query(By.css(`[ng-reflect-translate=${message}]`));
      expect(routingMessage).not.toBeNull();
      expect(routingMessage.nativeElement.textContent).toContain(message);
    });
  });

  it('should validate window routing subheading for Payment Method header only shows for integrated view', () => {
    component.payIntegratedFlag = 'Y';
    component.guestData = getMockReservationData(RoutingType.guestPay, mockRoutings);

    component.ngOnInit();
    fixture.detectChanges();
    let subHeading = fixture.debugElement.query(By.css('[data-slnm-ihg="PaymentSubHeading-SID"]'));
    expect(subHeading).toBeNull();
    component.guestData.routingType = RoutingType.singleUseCC;
    fixture.detectChanges();
    subHeading = fixture.debugElement.query(By.css('[data-slnm-ihg="PaymentSubHeading-SID"]'));

    expect(subHeading).toBeDefined();
    expect(subHeading.nativeElement.textContent).toContain('LBL_WNDW_ROUTING');
  });

  it('should validate routing windows are shown in check-in payment if reservation has SUCC', () => {
    component.payIntegratedFlag = 'Y';
    component.guestData = getMockReservationData(RoutingType.singleUseCC, mockRoutings);

    component.ngOnInit();
    fixture.detectChanges();
    const windowRouting = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingContainer1-SID"]'));

    expect(windowRouting).not.toBeNull();
  });

  it('should validate routing windows are shown in check-in payment if reservation has Extend Credit', () => {
    component.payIntegratedFlag = 'Y';
    component.guestData = getMockReservationData(RoutingType.extendCredit, mockRoutings);

    component.ngOnInit();
    fixture.detectChanges();
    const windowRouting = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingContainer1-SID"]'));

    expect(windowRouting).not.toBeNull();
  });

  it('should test if successs toast is displayed when form is saved', async(() => {
    fixture.whenRenderingDone().then(() => {
      component.guestData = mockGuestData;
      spyOn(checkInPaymentservice, 'saveAuthDetails').and.returnValue(of(outputWhenEditIsTrue));
      component.handleFormSubmit(outputWhenEditIsFalse);
      const spyObj = spyOn(component, 'handleSuccesssullAuthDetailsSave');
      fixture.detectChanges();
      expect(component.saveAuthDetailsSuccessToast).not.toBeNull();
    });
  }));

  it('should check whether toast message shows correct error information', () => {
    expect(component).toBeTruthy();
    component.guestData = mockGuestData;
    const data = mockResourceError;
    spyOn(checkInPaymentservice, 'getAuthorizePaymentOnFile').and.returnValue(of(data));
    spyOn(component, 'checkForError');
    component.checkForAuthorizeCard();
    fixture.detectChanges();
    expect(component.checkForError).toHaveBeenCalled();
    expect(checkInPaymentservice.getAuthorizePaymentOnFile).toHaveBeenCalledWith(mockGuestData);
  });

  it('should check whether toast message shows correct error information for non integrated', () => {
    expect(component).toBeTruthy();
    component.guestData = mockGuestData;
    const data = mockResourceError;
    spyOn(checkInPaymentservice, 'getAuthorizePaymentOnFile').and.returnValue(of(data));
    spyOn(component, 'checkForError');
    component.checkInForNonIntegrated();
    fixture.detectChanges();
    expect(component.checkForError).toHaveBeenCalled();
    expect(checkInPaymentservice.getAuthorizePaymentOnFile).toHaveBeenCalledWith(mockGuestData);
  });

  it('should check resource busy error handling is performed', () => {
   const errorSubject = component.getErrorSubject(mockResourceError.reservationDataList[0]);
   expect(errorSubject).toEqual(component.resourceBusyError);
  });
});
