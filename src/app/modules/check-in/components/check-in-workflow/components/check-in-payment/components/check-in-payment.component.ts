import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

import { Alert, AlertButtonType, AlertType, GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { ACT_ON_CARD_TYPES } from '@modules/check-in/components/talk-about/constants/act-on-constants';
import { UtilityService } from '@services/utility/utility.service';
import { AMENITY_MAP, WORKFLOW_TABS_POS } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-constants';
import { PaymentCheckInModel, SaveAuthDetailsModel } from '../models/check-in-payment.models';
import { CheckInPaymentService } from './../services/check-in-payment.service';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { LoyaltyAmenityDataModel } from '@modules/check-in/components/check-in-workflow/models/check-in-workflow.models';
import { CheckInService } from '@app/modules/check-in/services/check-in.service';
import {
  AUTH_ERROR_MSG,
  CHECK_IN_TAB,
  CheckinPaymentAnalyticsConstants,
  NON_INTG_AUTH_ERROR_MSG,
  PayRoutingMessages,
  SAVE_AUTH_ERR_REPORT_ISSUE_AUTOFILL,
  SAVE_AUTH_ERROR_MSG,
  SAVE_AUTH_NON_INTEGRATED_ERR_REPORT_ISSUE_AUTOFILL
} from '../constants/check-in-payment-constants';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { PREP_STATUSES } from '@app/modules/check-in/constants/check-in-constants';
import { FETCH_AUTH_ERROR_CONTENT } from '@modules/check-in/constants/check-in-error-constants';
import { AUTH_ERR_REPORT_ISSUE_AUTOFILL, FETCH_AUTH_ERR_AUTOFILL } from '@app/constants/error-autofill-constants';
import { RoutingType } from '@modules/check-in/enums/checkin-payment-routing.enum';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';

@Component({
  selector: 'app-check-in-payment',
  templateUrl: './check-in-payment.component.html',
  styleUrls: ['./check-in-payment.component.scss']
})
export class CheckInPaymentComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public alertTypeWarning = AlertType.Warning;
  public alertButtonTypeWarning = AlertButtonType.Warning;
  public cardTypes = ACT_ON_CARD_TYPES;
  public actOnCardType: string;
  public disablePayment = false;
  public authorizationSuccessToast: Alert;
  public saveAuthDetailsSuccessToast: Alert;
  public reportAuthErrorAutoFill = AUTH_ERR_REPORT_ISSUE_AUTOFILL;
  public reportAuthNonIntgAutoFill = SAVE_AUTH_NON_INTEGRATED_ERR_REPORT_ISSUE_AUTOFILL;
  public authError = new Subject<EmitErrorModel>();
  public resourceBusyError = new Subject<EmitErrorModel>();
  public authErrorContent = AUTH_ERROR_MSG;
  public stateName = CHECK_IN_TAB;
  public authNonIntgErrorContent = NON_INTG_AUTH_ERROR_MSG;
  public cardDetailsSaved = false;
  public saveAuthErrorAutoFill = SAVE_AUTH_ERR_REPORT_ISSUE_AUTOFILL;
  public saveAuthErrorContent = SAVE_AUTH_ERROR_MSG;
  public authData = {};
  public fetchAuthDetailsError = new Subject<EmitErrorModel>();
  public displayFetchAuthError = false;
  public reportIssueAutoFill = FETCH_AUTH_ERR_AUTOFILL;
  public errorConfig = FETCH_AUTH_ERROR_CONTENT;
  public fetchAuthRefresh = new Subject<null>();
  public resourceBusyErrorContent: Partial<ErrorToastMessageTranslations>;
  public isTabletPortraitView: boolean;
  public routingTypes = RoutingType;
  public routingMessages = PayRoutingMessages;

  private checkIn$: Subscription = new Subscription();

  @Input() guestData: ReservationDataModel;
  @Input() payIntegratedFlag: string;
  @Input() pmsAuthSaveInd: string;
  @Output() emitChangeTab: EventEmitter<number> = new EventEmitter();
  @Output() handleSaveAuthDetails: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private utility: UtilityService,
    private checkInService: CheckInService,
    private paymentService: CheckInPaymentService,
    private workflowService: CheckInWorkflowService,
    private gaService: GoogleAnalyticsService) {
    super();
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.isTabletPortraitView = window.outerWidth < 769;
  }

  ngOnInit() {
    super.resetSpecificError(this.authError);
    this.setActOnCardType();
    this.subscribeRefreshPoints();
    if (this.payIntegratedFlag === 'N') {
      this.subscribeToCheckInListener();
    }
    this.isTabletPortraitView = (window.outerWidth < 769);
    this.getScreenSize();
  }

  private subscribeToCheckInListener(): void {
    this.checkIn$.add(
      this.checkInService.listenTriggerSaveAuthDetails().subscribe(() => {
        this.checkInForNonIntegrated();
      })
    );
  }

  public setActOnCardType(): void {
    if (this.isRoomAssigned() && this.isAmenitySelected() && this.isRoomReady()) {
      this.actOnCardType = this.cardTypes.SUCCESS;
    } else {
      this.actOnCardType = this.cardTypes.WARNING;
      this.disablePayment = true;
    }
  }

  public isRoomReady(): boolean {
    return this.guestData.prepStatus !== PREP_STATUSES.inProgress;
  }

  private isRoomAssigned(): boolean {
    return !!this.guestData.assignedRoomNumber;
  }

  private isAmenitySelected(): boolean {
    return this.isLoyaltyNonOtaReservation() && this.guestData.amenityFlag ? (!this.guestData.isMasterReservation && this.guestData.amenityFlag) || !!this.guestData.selectedAmenity : true;
  }

  private isLoyaltyNonOtaReservation(): boolean {
    return !!this.guestData.ihgRcNumber && !this.guestData.otaFlag;
  }

  public getGuestName(): string {
    return this.utility.concatNameWithPrefix(this.guestData.prefixName, this.guestData.firstName, this.guestData.lastName);
  }

  navigateToReservationTab(): void {
    this.emitChangeTab.emit(WORKFLOW_TABS_POS.reservation);
  }

  navigateToRoomAssignmentTab(): void {
    this.emitChangeTab.emit(WORKFLOW_TABS_POS.assignment);
  }

  navigateToKeysTab(): void {
    this.emitChangeTab.emit(WORKFLOW_TABS_POS.keys);
  }

  private subscribeRefreshPoints() {
    this.checkIn$.add(this.checkInService.getRefreshPointSubject().subscribe(refreshData => {
      this.handlePointsPostDecline();
    }));
  }

  public checkForAuthorizeCard() {
    this.checkIn$.add(this.paymentService.getAuthorizePaymentOnFile(this.guestData).subscribe((response: PaymentCheckInModel) => {
      this.resourceBusyErrorContent = this.paymentService.getResourceBusyErrorContent();
      if (response && !super.checkIfAnyApiErrors(response.reservationDataList, null, this.getErrorSubject(response.reservationDataList[0]))) {
        super.resetSpecificError(this.resourceBusyError);
        this.authData = response.reservationDataList[0];
        this.updateCheckInStatus();
        this.authorizationSuccessToast =
          this.setToastMessageData(AlertType.Success, 'LBL_CHIN_AUTH_SUCCS', 'LBL_AUTH_CHIN_SUCCS_DESC', false);
        this.handlePointsPostDecline();
      } else {
        if (response.reservationDataList && !this.checkForError(response.reservationDataList[0])) {
          this.guestData['authError'] = true;
        }
      }
    }, (error) => {
      super.processHttpErrors(error, this.authError);
      this.guestData['authError'] = true;
    }));
  }

  public checkInForNonIntegrated(): void {
    this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
      CheckinPaymentAnalyticsConstants.EVENT_CHCKIN_LABEL);
    this.checkIn$.add(
      this.paymentService.getAuthorizePaymentOnFile(this.guestData).subscribe((response: PaymentCheckInModel) => {
        this.resourceBusyErrorContent = this.paymentService.getResourceBusyErrorContent();
        if (response && !super.checkIfAnyApiErrors(response.reservationDataList, null, this.getErrorSubject(response.reservationDataList[0]))) {
          super.resetSpecificError(this.resourceBusyError);
          this.updateCheckInStatus();
          this.handlePointsPostDecline();
          this.navigateToKeysTab();
        } else {
          if (response.reservationDataList && !this.checkForError(response.reservationDataList[0])) {
            this.updateGoogleAnalytics();
          }
          this.handleErrorCheckInNonIntg();
        }
      }, (error) => {
        super.processHttpErrors(error, this.authError);
        this.handleErrorCheckInNonIntg();
        this.updateGoogleAnalytics();
      })
    );
  }

  private updateGoogleAnalytics(){
      this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
        CheckinPaymentAnalyticsConstants.EVENT_ERROR_CHCKIN_LABEL);
  }
  private handleErrorCheckInNonIntg(): void {
    delete this.saveAuthDetailsSuccessToast;
    super.resetSpecificError(this.emitError);
  }

  private updateCheckInStatus(): void {
    this.disablePayment = true;
    this.guestData.isCheckedIn = true;
    this.guestData.prepStatus = PREP_STATUSES.checkedIn;
    this.guestData['authError'] = false;
  }

  private setSaveAuthDetailsSuccess(): void {
    this.saveAuthDetailsSuccessToast = {
      type: AlertType.Success,
      message: 'COM_TOAST_SAVE_SUCCESS',
      detailMessage: 'LBL_AUTHDETAILS_SUCCESS_MSG',
      dismissible: false
    };
  }

  private setToastMessageData(type: AlertType, message: string, detailMessage: string, dismissible: boolean): Alert {
    return {
     type,
     message,
     detailMessage,
     dismissible
   };
 }

  private handlePointsPostDecline(): void {
    if (this.hasAmenityPointsAndNotNone()) {
      this.checkIn$.add(this.workflowService.postDeclineAmenityPoints(this.guestData).subscribe((response: LoyaltyAmenityDataModel) => {
        this.handleAmenityPointsResponse(response);
      }, error => {
        this.handlePointsPostedStatus(false, this.guestData.ihgRcPoints);
      }));
    }
  }

  private handleAmenityPointsResponse(response: LoyaltyAmenityDataModel): void {
    if (response && response.errors) {
      this.handlePointsPostedStatus(false, this.guestData.ihgRcPoints);
    } else {
      const points = response.loyaltyAmenityPoints ? response.loyaltyAmenityPoints.amenityPoints : this.guestData.ihgRcPoints;
      this.handlePointsPostedStatus(true, points);
    }
  }

  private handlePointsPostedStatus(status: boolean, points: string): void {
    if (this.guestData.selectedAmenity === AMENITY_MAP.points) {
      this.guestData.pointsPosted = status;
      this.guestData.ihgRcPoints = points;
    } else {
      this.guestData.pointsPosted = null;
    }
  }

  private hasAmenityPointsAndNotNone(): boolean {
    return !!this.guestData.amenityPoints && this.guestData.selectedAmenity !== AMENITY_MAP.none;
  }

  public handleFormSubmit(evt: SaveAuthDetailsModel): void {
    this.checkIn$.add(this.paymentService.saveAuthDetails(evt,this.guestData.reservationNumber)
    .subscribe((data: SaveAuthDetailsModel) => {
      if (data && (!super.checkIfAnyApiErrors(data))) {
        this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
          CheckinPaymentAnalyticsConstants.SAVE_AUTH_DETAIL);
        this.handleSuccesssullAuthDetailsSave();
      } else {
        this.notifySaveAuthError();
        this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
          CheckinPaymentAnalyticsConstants.ERROR_SAVE_AUTH_DETAIL);
      }
    }, (error) => {
      this.handleErrorSaveAuthDetails(error);
      this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
        CheckinPaymentAnalyticsConstants.ERROR_SAVE_AUTH_DETAIL);
    }
    ));
  }

  public handleFetchAuthDetailsError(error: { type: string, data }): void {
    if (error.type === 'dataError') {
      super.checkIfAnyApiErrors(error.data, null, this.fetchAuthDetailsError);
    } else if (error.type === 'httpError') {
      super.processHttpErrors(error.data, this.fetchAuthDetailsError);
    }
    this.displayFetchAuthError = !!error.type;
  }

  public triggerSaveDetailsFormSubmit() {
    this.paymentService.triggerSaveAuthDetails();
  }

  private notifySaveAuthError(): void {
    this.cardDetailsSaved = false;
    this.handleSaveAuthDetails.emit(false);
    super.resetSpecificError(this.authError);
  }

  private handleErrorSaveAuthDetails(error): void {
    this.notifySaveAuthError();
    super.processHttpErrors(error);
  }

  public handleSuccesssullAuthDetailsSave():void {
    this.handleSaveAuthDetails.emit(true);
    this.cardDetailsSaved = true;
    this.setSaveAuthDetailsSuccess();
    super.resetSpecificError(this.authError);
  }

  public handleFetchAuthRefresh(): void {
    this.fetchAuthRefresh.next();
  }

  ngOnDestroy() {
    this.checkIn$.unsubscribe();
  }

  checkForError(data): boolean {
    return !!(data.errors && (data.errors as Array<any>).filter(err => (err.code === 'ERR-00054')));
  }

  getErrorSubject(data) {
    return this.checkForError(data) ? this.resourceBusyError : this.authError;
  }

}
