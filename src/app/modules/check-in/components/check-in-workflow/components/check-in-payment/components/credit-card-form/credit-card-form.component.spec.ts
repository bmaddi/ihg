import { async, ComponentFixture, TestBed, fakeAsync, inject } from '@angular/core/testing';

import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { EnvironmentService, useDefault, AlertType } from 'ihg-ng-common-core';
import { environment } from '@env/environment.local-int';
import { GridDataResult, GridModule } from '@progress/kendo-angular-grid';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { CreditCardFormComponent } from './credit-card-form.component';
import { DisplayAuthDetailsComponent } from './../display-auth-details/display-auth-details.component';
import { TruncatedTooltipComponent } from '@app/modules/shared/components/truncated-tooltip/truncated-tooltip.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from 'ihg-ng-common-core';
import { CheckInPaymentService } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/services/check-in-payment.service';
import { AlphanumericDirective } from '@app/modules/shared/directives/alpha-numeric/alpha-numeric.directive';
import { mockCardDetails, mockRatesInfo, mockDate, outputWhenEditIsFalse, outputWhenEditIsTrue } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/mock/check-in-payment-mock';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';


describe('CreditCardFormComponent', () => {
  let component: CreditCardFormComponent;
  let fixture: ComponentFixture<CreditCardFormComponent>;
  let store: CheckInPaymentService;
  let arrivalsService: ArrivalsService;
  let componentService: CheckInPaymentService;
  let input: DebugElement;

  function patchValueIntoField(fieldName: string, value: string) {
    component.getFormControl(fieldName).patchValue(value);
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreditCardFormComponent,
        DisplayAuthDetailsComponent,
        TruncatedTooltipComponent,
        AlphanumericDirective
      ],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        DatePickerModule,
        CommonTestModule,
        TranslateModule.forRoot(),
        GridModule,
        InputsModule,
        IhgNgCommonKendoModule,
        DropDownsModule
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserService, CheckInPaymentService, TranslateService, EnvironmentService, ConfirmationModalService, CheckInPaymentService, ArrivalsService]
    })
      .compileComponents().then(() => {

      });
  });

  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000000;
    fixture = TestBed.createComponent(CreditCardFormComponent);
    component = fixture.componentInstance;
    store = TestBed.get(CheckInPaymentService);
    arrivalsService = TestBed.get(ArrivalsService);
    componentService = fixture.debugElement.injector.get(CheckInPaymentService);
    input = fixture.debugElement.query(By.directive(AlphanumericDirective));
    fixture.detectChanges();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should test form is invalid when amount and authorizationCode are blank and cardDetailsEditable false', fakeAsync(() => {
    fixture.whenRenderingDone().then(() => {
      component.creditCardPaymentForm.controls['amount'].setValue('');
      component.creditCardPaymentForm.controls['authorizationCode'].setValue('');
      const buuton = fixture.debugElement.nativeElement.querySelector('.button-submit');
      expect(component.creditCardPaymentForm.valid).toBeFalsy();
    })
  }));

  it('Should test toggle comment inequality before and after executing the function', fakeAsync(() => {
    const oldVal = component.toggleCommentBox;
    component.toggleCommentField();
    const newval = component.toggleCommentBox;
    expect(oldVal).not.toEqual(newval);
  }))

  it('Should test form is valid when amount and authorizationCode has values and cardDetailsEditable false', () => {
    component.creditCardPaymentForm.get('amount').setValue('2222');
    component.creditCardPaymentForm.get('authorizationCode').setValue('1122');
    expect(component.creditCardPaymentForm.valid).toBeTruthy();
  });

  it('Should test Service injected via injecting (...) and TestBed.get (...) are same instance',
    inject([CheckInPaymentService], (injectService: CheckInPaymentService) => {
      expect(injectService).toBe(store);
    })
  );

  it('Should call the right funtion', () => {
    component.creditCardPaymentForm.get('amount').setValue('2222');
    component.creditCardPaymentForm.get('authorizationCode').setValue('1122');
    const mySpy = spyOn(component, 'onSubmit');
    fixture.debugElement.query(By.css('form')).triggerEventHandler('ngSubmit', null);
    expect(mySpy).toHaveBeenCalledTimes(1);
  });

  it('Should prevent keypress event on entering non alpha-numeric characters', () => {
    const event = new KeyboardEvent('keydown', {
      'key': '$',
      cancelable: true
    });
    input = fixture.debugElement.query(By.directive(AlphanumericDirective));
    input.nativeElement.dispatchEvent(event);
    fixture.detectChanges();
    expect(event.defaultPrevented).toBeTruthy();
  });

  xit('Should allow keypress event on entering alpha-numeric characters', () => {
    const event = new KeyboardEvent('keydown', {
      'key': 'A12',
      cancelable: true
    });
    input = fixture.debugElement.query(By.directive(AlphanumericDirective));
    input.nativeElement.dispatchEvent(event);
    fixture.detectChanges();
    expect(event.defaultPrevented).toBeFalsy();
  });

  it('Should test if the amount is rounded up to 2 digits after decimal',() => {
    fixture.whenRenderingDone().then(() => {
      const event = {
        target: {
          value: '25.3812'
        }
      };
      component.onValueEntering(event);
      fixture.detectChanges();
      const newval = component.creditCardPaymentForm.get('amount').value;
      expect(newval).toBe(25.38);
    });
  });

  it('Should test if the amount is rounded up to 2 digits on value change', () => {
    fixture.whenRenderingDone().then(() => {
      const event = {
        target: {
          value: '25'
        }
      };
      component.onChangeAmount(event);
      fixture.detectChanges();
      const newval = component.creditCardPaymentForm.get('amount').value;
      expect(newval).toBe('25.00');
    });
  });

  it('Should test Comment field value is not exceeding 150 char long', () => {
    fixture.whenRenderingDone().then(() => {
      const event = {
        target: {
          value: `Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment
         Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment Test Comment`
        }
      };
      component.onChangeComment(event);
      fixture.detectChanges();
      const newval = component.creditCardPaymentForm.get('comment').value;
      expect(newval.length).toBe(150);
    });
  });

  it('Should test edit icon, click event', () => {
    const spyon = spyOn(component, 'toggleCardDetailsEditableFeature');
    let button = fixture.debugElement.nativeElement.querySelector('.edit-icon');
    button.click();
    fixture.whenStable().then(() => {
      const showToggleVal = component.toggleCommentBox;
      expect(spyon).toHaveBeenCalledTimes(1);
    });
  });

  it('Should test last 4 digits field allowing only 4 digits', () => {
    fixture.whenRenderingDone().then(() => {
      const event = {
        target: {
          value: "22345"
        }
      };
      component.onLastFourDigitsValueEntering(event);
      const newval = component.creditCardPaymentForm.get('cCardLastFourDigits').value;
      expect(newval).toBe("2234");
    });
  });

  it('Should test last4digits field allowing only numbers', () => {
    fixture.whenRenderingDone().then(() => {
      const event = {
        target: {
          value: "text"
        }
      };
      component.onLastFourDigitsValueEntering(event);
      const newval = component.creditCardPaymentForm.get('cCardLastFourDigits').value;
      expect(newval).toBe("");
    });
  });

  it('Should test Expiration field will allow only numbers', () => {
    component.cardDetailsEditable = true;
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      let inputElm = fixture.debugElement.query(By.css('#expiration-field'));
      inputElm.nativeElement.value = 'text';
      inputElm.nativeElement.dispatchEvent(new Event('input'));
      const newval = component.creditCardPaymentForm.get('expiration').value;
      expect(newval).toBe("");
    });
  });

  it('should test cancel button reverses cardDetailsEditable', () => {
    component.cardDetailsEditable = true;
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      let button = fixture.debugElement.nativeElement.querySelector('.button-cancel');
      button.click();
      expect(component.cardDetailsEditable).toEqual(false);
    });
  });


  it('Authorization code is limit upto 15 alphanumeric characters and "Enter a valid code" message is displayed if entered no. is <2', () => {
    component.cardDetailsEditable = true;
    component.creditCardPaymentForm.controls['authorizationCode'].setValue('2');
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let button = fixture.debugElement.query(By.css('.button-submit'));
      expect(button.nativeElement.disabled).toBeTruthy();
      expect(component.creditCardPaymentForm.controls['authorizationCode'].errors).not.toBe(null)
    });
  });

  it('Verify Authorization code does not exceed 15 alphanumeric characters', () => {
    fixture.whenStable().then(() => {
      component.cardDetailsEditable = true;
      const event = {
        target: {
          value: '12345678901234567890'
        }
      };
      component.onChangeAuthCode(event);
      fixture.detectChanges();
      expect(component.creditCardPaymentForm.controls['authorizationCode'].value).toBe('123456789012345')
    });
  });

  it('should confirm payload is proper when user edited card details', () => {
    fixture.whenStable().then(() => {
      spyOn(component.handleFormSubmit, 'emit');
      const output = cloneDeep(outputWhenEditIsTrue);
      component.ratesInfo = cloneDeep(mockRatesInfo);
      component.cardDetails = cloneDeep(mockCardDetails);
      component.cardDetailsEditable = true;
      patchValueIntoField('creditCardType', component.creditCardTypes[0].value);
      patchValueIntoField('cCardLastFourDigits', output.cardDetails[0].cardNumber);
      patchValueIntoField('expiration', output.cardDetails[0].expiryDate);
      patchValueIntoField('amount', output.cardDetails[0].authAmount);
      patchValueIntoField('authorizationCode', output.cardDetails[0].authCode);
      fixture.debugElement.query(By.css('form')).triggerEventHandler('ngSubmit', null);
      fixture.detectChanges();
      expect(component.handleFormSubmit.emit).toHaveBeenCalledWith(output);
    })
  });

  it('should confirm payload is proper when user did not edit card details', () => {
    fixture.whenStable().then(() => {
      spyOn(component.handleFormSubmit, 'emit');
      component.ratesInfo = cloneDeep(mockRatesInfo);
      component.cardDetails = cloneDeep(mockCardDetails);
      const date = cloneDeep(mockDate);
      const output = outputWhenEditIsFalse;
      component.cardDetailsEditable = false;
      patchValueIntoField('amount', output.cardDetails[0].authAmount);
      patchValueIntoField('authorizationCode', output.cardDetails[0].authCode);
      fixture.debugElement.query(By.css('form')).triggerEventHandler('ngSubmit', null);
      fixture.detectChanges();
      expect(component.handleFormSubmit.emit).toHaveBeenCalledWith(output);
    });
  });

  it('should check if tooltip is present when user edits card details', () => {
    fixture.whenStable().then(() => {
      component.cardDetailsEditable = true;
      fixture.detectChanges();
      let tooltipIcon = fixture.debugElement.query(By.css('.expiration-tooltip'));
      expect(tooltipIcon.nativeElement).toBeTruthy();
    });
  });

  it('should check if tooltip is not present when user did not edit card details', () => {
    component.cardDetailsEditable = false;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let tooltipIcon = fixture.debugElement.query(By.css('.expiration-tooltip'));
      expect(tooltipIcon).toBe(null);
    });
  });

});
