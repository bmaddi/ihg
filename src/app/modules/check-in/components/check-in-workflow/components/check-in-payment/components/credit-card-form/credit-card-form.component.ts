import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Subscription, Observable } from 'rxjs';
import * as moment from 'moment';

import { CheckInPaymentService } from './../../services/check-in-payment.service';
import { CardModel, RateInfoModel } from '@app/modules/check-in/models/check-in.models';
import { CARD_CODE_ICON_MASK_MAP, CARD_LIST } from './../../constants/check-in-payment-constants';
import { SaveAuthDetailsModel, CardDetailsModel } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/models/check-in-payment.models';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { AppConstants } from '@app/constants/app-constants';

import { ValidateCardExpiration } from '../../validators/expiration-date-validator';

import { ArrivalsService } from '../../../../../../../prepare/services/arrivals-service/arrivals.service';

@Component({
  selector: 'app-credit-card-form',
  templateUrl: './credit-card-form.component.html',
  styleUrls: ['./credit-card-form.component.scss']
})
export class CreditCardFormComponent extends AppErrorBaseComponent implements OnInit {

  public creditCardPaymentForm: FormGroup;
  public toggleCommentBox = false;
  public cardCodeIconMaskMap = CARD_CODE_ICON_MASK_MAP;
  public dateFormat = AppConstants.VW_DT_FORMAT;

  
  public mask: string = "00/00";
  public cardDetailsEditable = false;
  public creditCardTypes = CARD_LIST;
  public defaultCreditCardSelectedItem = { text: 'Select', value: '' };
  public authCodeMaxLength = 15;

  private rootSubscription = new Subscription();

  @Input() ratesInfo: RateInfoModel;
  @Input() cardDetails: CardModel;
  @Input() reservationNumber: string;
  @Input() pmsAuthSaveInd: string;
  @Input() fetchAuthRefresh: Observable<null>;
  @Input() cardDetailsSaved: boolean;
  @Output() saveAuthDetailsSuccess: EventEmitter<boolean> = new EventEmitter();
  @Output() emitFetchError = new EventEmitter<{ type: string, data }>();  
  @Output() handleFormSubmit: EventEmitter<SaveAuthDetailsModel> = new EventEmitter();

  constructor(private fb: FormBuilder, private checkInPaymentService: CheckInPaymentService, private arrivalsService: ArrivalsService) {
    super();
  }

  ngOnInit() {
    this.rootSubscription.add(
      this.checkInPaymentService.listenTriggerSaveAuthDetails().subscribe(()=> {
        this.onSubmit();
      })
    );
    this.createForm();
  }

  private createForm(): void {
    this.creditCardPaymentForm = this.fb.group({
      creditCardType: [this.defaultCreditCardSelectedItem.value, Validators.required],
      cCardLastFourDigits: ["", [Validators.required,Validators.pattern("[0-9]{4,}")]],
      expiration: ["", [Validators.required, ValidateCardExpiration]],
      date: [{ value: moment().toDate(), disabled: true }, [Validators.required]],
      amount: ["", { validators: [Validators.required] }],
      authorizationCode: ["", { validators: [Validators.required, Validators.minLength(2), Validators.maxLength(this.authCodeMaxLength)] }],
      comment: [""]
    });
    this.onAmountFieldValueChange();
    this.disableCardRelatedValidation();
    this.onFormValueChange();
  }

  private onFormValueChange() {
    this.rootSubscription.add(this.creditCardPaymentForm.valueChanges.subscribe((data) => {
      if (data && (this.creditCardPaymentForm.dirty || this.creditCardPaymentForm.touched)) {
        this.arrivalsService.setGenericUnsavedDataChange(true);
        this.arrivalsService.creditCardUnsavedData = true;
      }
    }));
  }

  private disableCardRelatedValidation()  : void {
    this.getFormControl('creditCardType').disable();
    this.getFormControl('cCardLastFourDigits').disable();
    this.getFormControl('expiration').disable();
    this.updateValueAndValidityOfCardFields();
  }

  private enableCardRelatedValidation(): void {
    this.getFormControl('creditCardType').enable();
    this.getFormControl('cCardLastFourDigits').enable();
    this.getFormControl('expiration').enable();
    this.updateValueAndValidityOfCardFields();
  }

  private updateValueAndValidityOfCardFields() {
    this.getFormControl('creditCardType').updateValueAndValidity();
    this.getFormControl('cCardLastFourDigits').updateValueAndValidity();
    this.getFormControl('expiration').updateValueAndValidity();
  }

  public onAmountFieldValueChange(): void {
    const amtContrl = this.creditCardPaymentForm.get('amount');
    this.rootSubscription.add(amtContrl.valueChanges
      .subscribe((value: string) => {
        if(value !== "" && value !== null) {
          if(Number(value) > 0) {          
            amtContrl.setErrors(null);
          } else {
            amtContrl.setErrors({'valueLessThanZero': true});
          }
        } else {
          amtContrl.setErrors({'required': true});
      }
    })); 
  }

  public toggleCommentField(): void {
    this.toggleCommentBox = !this.toggleCommentBox;
  }

  public get cardExpirationDate(): string {
    return (this.cardDetails) ? this.cardDetails['expireDate'].substr(0, 2) + "/" + this.cardDetails['expireDate'].substr(2, 2) : '';
  }

  public validateDate(): void{
    //const expirationControl= this.creditCardPaymentForm.get('expiration')
    //const expiration = expirationControl.value;
    //expirationControl.setErrors(null);
    //if(expiration){
      //if(parseInt(expiration.substr(0,2)) > 12){
        //expirationControl.setErrors({ 'inValidMonth': true });
      //}
    //}
  }
  public getFormControl(controlName: string): FormControl {
    return this.creditCardPaymentForm.get(controlName) as FormControl;
  }

  private get defaultCardTypeVal(): string {
    return (this.cardDetails) ? this.cardDetails.code : '';
  }

  private get defaultLastFourDigits(): string {
    return (this.cardDetails) ? this.cardDetails.number.substr(this.cardDetails.number.length - 4) : '';
  }

  private get payload(): SaveAuthDetailsModel {
    const date = moment().format(this.dateFormat);
    const cardTypeVal = (this.cardDetailsEditable) ? this.getFormControl('creditCardType').value : this.defaultCardTypeVal;
    const cardNumberVal = (this.cardDetailsEditable) ? this.getFormControl('cCardLastFourDigits').value : this.defaultLastFourDigits; 
    const expiryDateVal = (this.cardDetailsEditable) ? this.getFormControl('expiration').value : this.cardExpirationDate;

    return {
      cardDetails: [{
        authAmount: this.getFormControl('amount').value,
        authCode: this.getFormControl('authorizationCode').value,
        authDate: date.toUpperCase(),
        cardNumber: cardNumberVal,
        cardType: cardTypeVal,
        commentText: this.getFormControl('comment').value,
        currencyCode: (this.ratesInfo) ? this.ratesInfo.currencyCode : '',
        expiryDate: expiryDateVal
      }]
    }
  }

  public onSubmit(): void {
    if(this.creditCardPaymentForm.valid) {      
      this.handleFormSubmit.emit(this.payload);
    }
    return;
  }

  //This method is added to don't accept more than <authCodeMaxLength> length value in android
  public onChangeAuthCode(event: any) {
    if (event.target.value.length > this.authCodeMaxLength) {
      this.creditCardPaymentForm.controls.authorizationCode.patchValue(event.target.value.substring(0, this.authCodeMaxLength));
    }
  }

  public onChangeComment(event: any) {
    if (event.target.value.length > 150) {
      this.creditCardPaymentForm.controls.comment.patchValue(event.target.value.substring(0, 150));
    }
  }

  //This method is added for accepting only two digits after the decimals in android 
  public onValueEntering(event: any){
    let amount = event.target.value;
    if(event.target.value.toString().indexOf('.') !=-1 ){
      let values =event.target.value.split('.');
      if(values[1] && values[1].length> 2){        
        amount= values[0]+'.'+ values[1].substring(0,2);
        this.creditCardPaymentForm.controls.amount.patchValue(parseFloat(amount));
      }
    } else{
       if(amount.length > 14){         
        this.creditCardPaymentForm.controls.amount.patchValue(amount.substring(0,14));
       }
    }
  }

  public onChangeAmount(event: any){
    let amount =  event.target.value;
    if(amount !==''){
      this.creditCardPaymentForm.controls.amount.patchValue(parseFloat(amount).toFixed(2));
    } 
  }

  public handleFetchAuthDetailsError(error: { type: string, data }): void {
    this.emitFetchError.next(error);
  }

  public toggleCardDetailsEditableFeature() {
    this.cardDetailsEditable = !this.cardDetailsEditable;
    if(this.cardDetailsEditable) {
      this.enableCardRelatedValidation();
    } else {
      this.disableCardRelatedValidation();
    }
  }

  public onLastFourDigitsValueEntering(event: any) {
    const lastFourDigitsVal = event.target.value;
    if(lastFourDigitsVal.toString().indexOf('.')>-1){
      this.creditCardPaymentForm.controls.cCardLastFourDigits.patchValue(lastFourDigitsVal.toString().replace('.',''));
      return;
    }   
    if(lastFourDigitsVal.length > 4) {
      this.creditCardPaymentForm.controls.cCardLastFourDigits.patchValue(lastFourDigitsVal.substring(0,4));
    }
  }

  private clearFields(): void {
    if(this.cardDetailsEditable) {
      this.toggleCardDetailsEditableFeature();
    }
    if(this.toggleCommentBox) {
      this.toggleCommentField();
    }
    this.creditCardPaymentForm.reset({
      'creditCardType': this.defaultCreditCardSelectedItem.value,
      'cCardLastFourDigits': "",
      'expiration': "",
      'date': moment().toDate(),
      'amount': "",
      'authorizationCode': "",
      'comment': ""
    });
    this.creditCardPaymentForm.markAsPristine;
  }

  public onCancel():void {
    this.clearFields()
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe(); 
  }
}
