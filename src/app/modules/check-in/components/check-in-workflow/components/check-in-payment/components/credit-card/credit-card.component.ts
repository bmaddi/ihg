import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

import { CardModel } from '@modules/check-in/models/check-in.models';
import { CARD_CODE_ICON_MASK_MAP } from './../../constants/check-in-payment-constants';
import { ScrollUtilService } from '@app-shared/services/scroll-util/scroll-util.service';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent implements OnInit, OnDestroy {
  private rootSubscription = new Subscription();
  public cardCodeIconMaskMap = CARD_CODE_ICON_MASK_MAP;
  public initialized = false;
  public animation = {
    type: 'slide',
    direction: 'up',
    duration: 500
  };

  constructor(private scrollUtil: ScrollUtilService) {
  }

  @Input() cardDetails: CardModel;
  @Input() firstName: string;
  @Input() lastName: string;
  @Input() animate = true;

  ngOnInit() {
    this.scrollIntoView();
    this.handleInit();
  }

  private scrollIntoView(): void {
    this.scrollUtil.scrollIntoBottomView('credit-card-container');
  }

  private handleInit(): void {
    if (this.animate) {
      this.handleAnimation();
    } else {
      this.animation = null;
      this.initialized = true;
    }
  }

  private handleAnimation(): void {
    this.rootSubscription.add(of(true).pipe(
      delay(200), tap(() => this.initialized = true),
      delay(600), tap(() => {
        this.animate = false;
        this.animation = null;
      })).subscribe());
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

}
