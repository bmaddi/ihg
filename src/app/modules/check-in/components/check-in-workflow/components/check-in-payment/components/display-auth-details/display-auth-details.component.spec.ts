import { async, ComponentFixture, TestBed, fakeAsync, inject } from '@angular/core/testing';

import { CreditCardFormComponent } from '../credit-card-form/credit-card-form.component';
import { DisplayAuthDetailsComponent } from './display-auth-details.component';
import { TruncatedTooltipComponent } from '@app/modules/shared/components/truncated-tooltip/truncated-tooltip.component';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { CheckInPaymentService } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/services/check-in-payment.service';
import { not } from '@progress/kendo-angular-grid/dist/es2015/utils';
import { AlphanumericDirective } from '@app/modules/shared/directives/alpha-numeric/alpha-numeric.directive';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { Subject } from 'rxjs';

describe('DisplayAuthDetailsComponent', () => {
  let component: DisplayAuthDetailsComponent;
  let fixture: ComponentFixture<DisplayAuthDetailsComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DisplayAuthDetailsComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        DatePickerModule,
        CommonTestModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [UserService, ConfirmationModalService, CheckInPaymentService]
    })
      .compileComponents().then(() => {
      setTestEnvironment();
      fixture = TestBed.createComponent(DisplayAuthDetailsComponent);
      component = fixture.componentInstance;
    });
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the right funtion', () => {
    const mySpy = spyOn(component, 'fetchDetails');
    const fetchAuthSubject = new Subject<null>();
    component.fetchAuthRefresh = fetchAuthSubject.asObservable();
    fetchAuthSubject.next();
    fixture.detectChanges();
    expect(mySpy).toHaveBeenCalledTimes(1);
  });
});
