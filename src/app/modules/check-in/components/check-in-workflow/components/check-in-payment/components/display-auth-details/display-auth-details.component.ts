import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { CardModel } from '@app/modules/check-in/models/check-in.models';
import { CARD_CODE_ICON_MASK_MAP, CheckinPaymentAnalyticsConstants } from './../../constants/check-in-payment-constants';
import { CheckInPaymentService } from './../../services/check-in-payment.service';
import { SaveAuthDetailsModel, CardDetailsModel } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/models/check-in-payment.models';

@Component({
  selector: 'app-display-auth-details',
  templateUrl: './display-auth-details.component.html',
  styleUrls: ['./display-auth-details.component.scss']
})
export class DisplayAuthDetailsComponent implements OnInit {

  public showComment = true;
  public cardDetails: CardDetailsModel[] = [];
  public cardCodeIconMaskMap = CARD_CODE_ICON_MASK_MAP;
  private rootSubscription = new Subscription();

  @Input() reservationNumber: string;
  @Input() cardDetailsFromReservation: CardModel;
  @Input() fetchAuthRefresh: Observable<null>;
  @Output() emitFetchError = new EventEmitter<{ type: string, data }>();

  constructor(private checkInPaymentService: CheckInPaymentService, private gaService: GoogleAnalyticsService) { }

  ngOnInit() {
    this.fetchDetails();
    this.subscribeToFetchAuthRefresh();
  }

  private subscribeToFetchAuthRefresh(): void {
    this.rootSubscription.add(this.fetchAuthRefresh.subscribe(() => this.fetchDetails()));
  }

  public fetchDetails(): void {
    this.rootSubscription.add(
      this.checkInPaymentService.fetchAuthDetails(this.reservationNumber)
        .subscribe((data: SaveAuthDetailsModel) => {
          if (!data.errors) {
            this.cardDetails.push(data.cardDetails[data.cardDetails.length - 1]);
            this.handleError(null, data);
          } else {
            this.handleError('dataError', data);
            this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
              CheckinPaymentAnalyticsConstants.FETCH_AUTH_ERROR);
          }
        }, error => {
          this.handleError('httpError', error);
          this.gaService.trackEvent(CheckinPaymentAnalyticsConstants.CATEGORY_CHECK_IN, CheckinPaymentAnalyticsConstants.ACTION,
            CheckinPaymentAnalyticsConstants.FETCH_AUTH_ERROR);;
        })
    );
  }

  private handleError(type: string, data): void {
    this.emitFetchError.next({ type, data });
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

}
