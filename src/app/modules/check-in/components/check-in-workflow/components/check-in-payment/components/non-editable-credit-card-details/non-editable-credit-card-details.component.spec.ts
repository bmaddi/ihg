import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonEditableCreditCardDetailsComponent } from './non-editable-credit-card-details.component';
import { TruncatedTooltipComponent } from '@app-shared/components/truncated-tooltip/truncated-tooltip.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

describe('NonEditableCreditCardDetailsComponent', () => {
  let component: NonEditableCreditCardDetailsComponent;
  let fixture: ComponentFixture<NonEditableCreditCardDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbTooltipModule],
      declarations: [NonEditableCreditCardDetailsComponent, TruncatedTooltipComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonEditableCreditCardDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
