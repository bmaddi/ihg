import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-non-editable-credit-card-details',
  templateUrl: './non-editable-credit-card-details.component.html',
  styleUrls: ['./non-editable-credit-card-details.component.scss']
})
export class NonEditableCreditCardDetailsComponent implements OnInit {

  @Input() cardCode: string;
  @Input() cardNumber: string;
  @Input() cardExpirationDate: string;

  constructor() { }

  ngOnInit() {
  }

}
