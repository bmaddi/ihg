import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckInPaymentService } from '../../services/check-in-payment.service';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';

@Component({
  selector: 'app-payment-actions',
  templateUrl: './payment-actions.component.html',
  styleUrls: ['./payment-actions.component.scss']
})
export class PaymentActionsComponent implements OnInit {

  @Input() guestData: ReservationDataModel;
  @Input() disablePayment: boolean;
  @Output() emitToAuthorizeCard: EventEmitter<boolean> = new EventEmitter();

  constructor(private gaService: GoogleAnalyticsService,
              private checkInPaymentService: CheckInPaymentService) {
  }

  ngOnInit() {
    if (this.guestData.isCheckedIn) {
      this.disablePayment = true;
    }
  }

  handleAuthorizeCardOnFile() {
    this.openWarningModal();
  }

  private openWarningModal(): void {
    this.checkInPaymentService.openAuthorizeCardWarning().subscribe(() => {
        this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.GA_PAYMENT_ACTION,
          CheckinAnalyticsConstants.GA_AUTHORIZE_LBL);
        this.emitToAuthorizeCard.emit(true);
      }, () => {
      }
    );
  }
}
