import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { By } from '@angular/platform-browser';

import { PipesModule } from 'ihg-ng-common-core';

import { PaymentWindowRoutingComponent } from './payment-window-routing.component';
import { mockRoutings } from '@modules/check-in/components/check-in-workflow/components/check-in-payment/mock/check-in-payment-mock';

describe('PaymentWindowRoutingComponent', () => {
  let component: PaymentWindowRoutingComponent;
  let fixture: ComponentFixture<PaymentWindowRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PipesModule,
        TranslateModule.forRoot()
      ],
      declarations: [PaymentWindowRoutingComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentWindowRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const routingWindow = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRouting-SID"]'));
    expect(routingWindow).toBeDefined();
  });

  it('should validate payment window shows if only 1 is setup', () => {
    component.windows = mockRoutings.slice(0, 1) as any;

    fixture.detectChanges();
    const routingWindow = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRouting-SID"]'));

    expect(routingWindow).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingTitle1-SID"]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingTitle2-SID"]'))).toBeNull();
  });

  it('should validate proper information is show for CC payment windows', () => {
    component.windows = mockRoutings as any;

    fixture.detectChanges();
    const title = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingTitle1-SID"]'));
    const payment = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingPayment1-SID"]'));
    const charges = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingChargesIncluded1-SID"]'));

    expect(title).not.toBeNull();
    expect(title.childNodes[0].nativeNode.textContent).toContain('LBL_WINDOW ' + component.windows[0].window);
    expect(payment).not.toBeNull();
    expect(payment.childNodes[1].nativeNode.textContent).toContain(component.windows[0].payMethod.payMethod.value);
    expect(payment.childNodes[2].nativeNode.textContent).toContain('*' + component.windows[0].payMethod.last4Digits);
    expect(payment).not.toBeNull();
    expect(charges.childNodes[1].nativeNode.textContent).toContain(component.windows[0].routingInstruction);
  });

  it('should validate proper information is show for other payment windows', () => {
    component.windows = mockRoutings as any;

    fixture.detectChanges();
    const title = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingTitle2-SID"]'));
    const payment = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingPayment2-SID"]'));
    const charges = fixture.debugElement.query(By.css('[data-slnm-ihg="WindowRoutingChargesIncluded2-SID"]'));

    expect(title).not.toBeNull();
    expect(title.childNodes[0].nativeNode.textContent).toContain('LBL_WINDOW ' + component.windows[1].window);
    expect(payment).not.toBeNull();
    expect(payment.childNodes[1].nativeNode.textContent).toContain(component.windows[1].payMethod.payMethod.value);
    expect(payment.childNodes[2].nativeNode.textContent).toEqual(component.emptyValue);
    expect(payment).not.toBeNull();
    expect(charges.childNodes[1].nativeNode.textContent).toContain(component.windows[1].routingInstruction);
  });

  it('should validate a limit of 2 windows are shown as per the WF', () => {
    component.windows = mockRoutings as any;

    expect(component.limit).toEqual(2);
    fixture.detectChanges();

    component.windows.forEach((w, i) => {
      const routing = fixture.debugElement.query(By.css(`[data-slnm-ihg="WindowRoutingContainer${i + 1}-SID"]`));
      (i + 1) > component.limit
        ? expect(routing).toBeNull()
        : expect(routing).not.toBeNull();
    });
  });
});
