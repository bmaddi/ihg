import { Component, Input, OnInit } from '@angular/core';

import { PayRouting } from '@modules/check-in/models/check-in.models';
import { AppConstants } from '@app/constants/app-constants';

@Component({
  selector: 'app-payment-window-routing',
  templateUrl: './payment-window-routing.component.html',
  styleUrls: ['./payment-window-routing.component.scss']
})
export class PaymentWindowRoutingComponent implements OnInit {
  @Input() windows: PayRouting[] = [];
  @Input() limit = 2;

  emptyValue = AppConstants.EMDASH;

  constructor() {
  }

  ngOnInit() {
  }

}
