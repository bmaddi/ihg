import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';

export const CARD_CODES = {
  VISA: 'VS',
  CARTE_BlANCHE: 'CB',
  MASTER_CARD: 'MC',
  DISCOVER: 'DS',
  AMEX: 'AX',
  DINERS_CLUB: 'DC',
  JAPAN_CREDIT_BUREAU: 'JC',
  VISA_ALTER: 'VA'
};

export const CARD_LIST: Array<{ text: string, value: string }> = [
  {text: 'American Express', value: CARD_CODES.AMEX},
  {text: 'Carte Blanche', value: CARD_CODES.CARTE_BlANCHE},
  {text: 'Diners Club', value: CARD_CODES.DINERS_CLUB},
  {text: 'Discover', value: CARD_CODES.DISCOVER},
  {text: 'Japan Credit Bureau', value: CARD_CODES.JAPAN_CREDIT_BUREAU},
  {text: 'MasterCard', value: CARD_CODES.MASTER_CARD},
  {text: 'Visa', value: CARD_CODES.VISA},
];

export const CARD_CODE_ICON_MASK_MAP = {
  [CARD_CODES.VISA]: {
    icon: 'fab fa-cc-visa',
    pattern: 'XXXX XXXX XXXX ',
    text: 'Visa'
  },
  [CARD_CODES.CARTE_BlANCHE]: {
    icon: 'fab fa-cc-diners-club',
    pattern: 'XXXX XXXXXX ',
    text: 'Carte Blanche'
  },
  [CARD_CODES.MASTER_CARD]: {
    icon: 'fab fa-cc-mastercard',
    pattern: 'XXXX XXXX XXXX ',
    text: 'MasterCard'
  },
  [CARD_CODES.DISCOVER]: {
    icon: 'fab fa-cc-discover',
    pattern: 'XXXX XXXX XXXX ',
    text: 'Discover'
  },
  [CARD_CODES.AMEX]: {
    icon: 'fab fa-cc-amex',
    pattern: 'XXXX XXXXXX X',
    text: 'American Express'
  },
  [CARD_CODES.DINERS_CLUB]: {
    icon: 'fab fa-cc-diners-club',
    pattern: 'XXXX XXXX XXXX ',
    text: 'Diners Club'
  },
  [CARD_CODES.JAPAN_CREDIT_BUREAU]: {
    icon: 'fab fa-cc-jcb',
    pattern: 'XXXX XXXX XXXX ',
    text: 'Japan Credit Bureau'
  },
  [CARD_CODES.VISA_ALTER]: {
    icon: 'fab fa-cc-visa',
    pattern: 'XXXX XXXX XXXX '
  }
};

export const SAVE_AUTH_ERR_REPORT_ISSUE_AUTOFILL = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Payment',
  subject: 'Error Saving Authorization Details'
};

export const SAVE_AUTH_NON_INTEGRATED_ERR_REPORT_ISSUE_AUTOFILL = {
  function: 'Guests',
  topic: 'Check In',
  subtopic: 'Payment',
  subject: 'Error checking in guest'
};

export const AUTH_ERROR_MSG: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_AUTH_FLD'
  }
};

export const NON_INTG_AUTH_ERROR_MSG: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_NON_INTG_CHECK_IN_ERROR'
  }
};

export const SAVE_AUTH_ERROR_MSG: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  generalError: {
    message: 'LBL_ERR_SAVE_AUTH_MAIN_ALERT',
    detailMessage: 'LBL_ERR_SAVE_AUTH_DETAILS'
  }
};

export const PMS_NOT_IN_SYNC_ERROR_MSG: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_BACK_TO_GUESTLIST',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_PMS_NOT_IN_SYNC'
  }
};

export const AUTH_ROOM_TYPES = {
  VACANT: 'Vacant',
  CLEAN: 'Clean',
  INSPECTED: 'Inspected'
};

export const PMS_NOT_IN_SYNC_UPDATE: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'COM_BTN_RFRSH',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  generalError: {
    message: 'LBL_TOAST_INFO',
    detailMessage: 'LBL_ERR_PMS_NOT_UPDATE'
  }
};

export const PMS_NOT_IN_SYNC_SECONDRY: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_VW_DTLS',
  reportAnIssueButton: '',
  generalError: {
    message: 'COM_TOAST_WARNING',
    detailMessage: 'LBL_SCND_PMS_INCONSIST'
  }
};


export const CHECK_IN_TAB = 'check-in';

export const CheckinPaymentAnalyticsConstants = {
  CATEGORY_CHECK_IN : 'Check In',
  ACTION : 'Payment',
  FETCH_AUTH_ERROR: 'Error Fetching Auth Details',
  SAVE_AUTH_DETAIL: 'Save Authorization Details',
  ERROR_SAVE_AUTH_DETAIL : 'Error Saving Authorization Details',
  EVENT_CHCKIN_LABEL: 'Check In',
  EVENT_ERROR_CHCKIN_LABEL: 'Error Checking In'
};

export const PayRoutingMessages: { multipleWindows: string[], missingWindows?: string[], singleWindow?: string[] } = {
  multipleWindows: ['LBL_SWP_CARD_ON_PMS', 'LBL_TELL_GST_CARD_AUTH'],
  missingWindows: ['LBL_CREATE_NEW_PAYMENT', 'LBL_TELL_GST_CARD_AUTH'],
  singleWindow: ['LBL_TELL_GST_CARD_AUTH'],
};
