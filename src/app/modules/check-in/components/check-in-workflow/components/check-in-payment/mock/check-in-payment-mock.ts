import { SaveAuthDetailsModel } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/models/check-in-payment.models';
import { AppConstants } from '@app/constants/app-constants';
import * as moment from 'moment';
import { CARD_LIST } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/constants/check-in-payment-constants';

export const mockFetchCardDetails: SaveAuthDetailsModel = {
  cardDetails: [{
    commentId: 1223,
    authAmount: '12312323.3444444',
    authCode: 'sdfsdf213',
    authDate: '02SEP1982',
    cardNumber: '1234',
    cardType: 'Japanese Card Type',
    commentText: 'this is a long comment. this is a long comment',
    currencyCode: 'USD',
    expiryDate: '06/56'
  }]
};

export const mockDate = moment().format(AppConstants.VW_DT_FORMAT);

export const mockRatesInfo = {
  'currencyCode': 'EUR',
  'totalAmountBeforeTax': '755.14',
  'totalAmountAfterTax': '808.00',
  'totalTaxes': '52.86'
};

export const mockCardDetails = {
  'code': 'VS',
  'expireDate': '0420',
  'number': '7472471499080010'
};

export const outputWhenEditIsFalse = {
  cardDetails: [{
    authAmount: '222222.22',
    authCode: 'ssss',
    authDate: mockDate.toUpperCase(),
    cardNumber: '0010',
    cardType: mockCardDetails.code,
    commentText: '',
    currencyCode: mockRatesInfo.currencyCode,
    expiryDate: '04/20'
  }]
};

export const outputWhenEditIsTrue = {
  cardDetails: [{
    authAmount: '222222.22',
    authCode: 'ssss',
    authDate: mockDate.toUpperCase(),
    cardNumber: '2222',
    cardType: CARD_LIST[0].value,
    commentText: '',
    currencyCode: mockRatesInfo.currencyCode,
    expiryDate: '1134'
  }]
};

export const mockGuestData: any = {
  'prefixName': 'Mr',
  'firstName': 'Saurav',
  'lastName': 'Agrawal',
  'suffixName': '',
  'keysCut': true,
  'reservationNumber': '44002733',
  'pmsReservationNumber': '447469',
  'pmsReservationStatus': 'DUEIN',
  'pmsUniqueNumber': '466611',
  'arrivalTime': '',
  'checkInDate': '2019-06-05',
  'checkOutDate': '2019-06-07',
  'numberOfNights': 2,
  'ihgRcNumber': '123456',
  'ihgRcMembership': 'Spire Elite',
  'ihgRcPoints': '5000',
  'vipCode': '',
  'companyName': 'ARKANSAS HOSPICE INC',
  'groupName': 'IHG Concerto',
  'rateCategoryCode': 'IGBBB',
  'roomTypeCode': 'KEXG',
  'roomTypeDescription': '1 KING BED EXECUTIVE',
  'numberOfRooms': 1,
  'badges': [
    'spire',
    'ambassador',
    'employee'
  ],
  'assignedRoomNumber': '',
  'assignedRoomStatus': null,
  'numberOfAdults': 1,
  'numberOfChildren': 0,
  'preferences': [
    'HIGH FLOOR',
    'BALCONY',
    'ROOM NEAR ELEVATOR',
    'OCEAN VIEW',
    'NEAR FITNESS ROOM'
  ],
  'specialRequests': [
    'Need Extra Towels and Water Bottles'
  ],
  'infoItems': [],
  'tasks': [
    {
      'traceId': 405806,
      'traceText': 'Need Extra Towels and Water Bottles',
      'departmentCode': 'ZIF',
      'departmentName': 'CRS Information/Comments',
      'traceTimestamp': '2019-06-06T00:00:00.0000000-04:00',
      'isResolved': true,
      'resolvedTimestamp': '2019-06-04',
      'resolvedBy': 'SUPERVISOR'
    },
    {
      'traceId': 405809,
      'traceText': 'Need Shampoo',
      'departmentCode': 'HSK',
      'departmentName': 'House Keeping',
      'traceTimestamp': '2019-06-04T10:43:00.0000000-04:00',
      'isResolved': true,
      'resolvedTimestamp': '2019-06-04',
      'resolvedBy': 'SUPERVISOR'
    },
    {
      'traceId': 405811,
      'traceText': 'Need Chocolates',
      'departmentCode': 'MGT',
      'departmentName': 'Management',
      'traceTimestamp': '2019-06-04T13:24:00.0000000-04:00',
      'isResolved': false
    }
  ],
  'prepStatus': 'CHECKEDIN',
  'heartBeatActions': {
    'myHotelActions': [
      'GuestProblem',
      'GuestComment',
      'SleepIssue'
    ],
    'otherHotelActions': [
      'GuestLove'
    ]
  },
  'heartBeatComment': 'Good Hospitality. Looking forward to revisit again',
  'heartBeatImprovement': 'Add more Indoor Events',
  'otaFlag': false,
  'resVendorCode': 'BKGTL',
  'ratesInfo': {
    'currencyCode': 'EUR',
    'totalAmountBeforeTax': '755.14',
    'totalAmountAfterTax': '808.00',
    'totalTaxes': '52.86'
  },
  'payment': {
    'paymentInformation': {
      'card': {
        'code': 'VS',
        'expireDate': '0420',
        'number': '7472471499080010'
      }
    }
  },
  'inspected': false,
  'paymentType': 'CARD',
  'isCheckedIn': true,
  'amenityPoints': '500',
  'authError': false
};

export const mockPaymentError = {
  errors: [{
    code: 'Error code',
    message: 'Error message'
  }]
};

export const mockResourceError = {
  'reservationDataList': [
    {
      'reservationNumber': '21778854',
      'pmsReservationNumber': '42777',
      'pmsUniqueNumber': '42048',
      'membershipId': '147502299',
      'authorizedAmount': '',
      'errors': [
        {
          'code': 'ERR-00054',
          'detailText': 'kiosk ifc res->Checkin->ORA-00054: resource busy and acquire with NOWAIT specified or timeout expired<->Locator : Checkin : reservation.lock instance '
        }
      ]
    }
  ]
};


export const mockRoutings = [
  {
    'routingInstruction': 'Room and Tax',
    'owner': 'New1, Ota',
    'window': 1,
    'room': '',
    'routingType': '',
    'payMethod': {
      'payMethod': {
        'type': '',
        'value': 'VS'
      },
      'last4Digits': '0727'
    }
  },
  {
    'routingInstruction': 'OTHER',
    'owner': 'New1, Ota',
    'window': 2,
    'room': 'ALPHA',
    'routingType': '',
    'payMethod': {
      'payMethod': {
        'type': '',
        'value': 'CASH'
      },
      'last4Digits': ''
    }
  },
  {
    'routingInstruction': 'OTHER',
    'owner': 'New1, Ota',
    'window': 2,
    'room': '',
    'routingType': '',
    'payMethod': {
      'payMethod': {
        'type': '',
        'value': 'VS'
      },
      'last4Digits': '0727'
    }
  },
  {
    'routingInstruction': 'OTHER',
    'owner': 'New1, Ota',
    'window': 3,
    'room': 'ALPHA',
    'routingType': '',
    'payMethod': {
      'payMethod': {
        'type': '',
        'value': 'CASH'
      },
      'last4Digits': ''
    }
  },
  {
    'routingInstruction': 'OTHER',
    'owner': 'New1, Ota',
    'window': 4,
    'room': 'ALPHA',
    'routingType': '',
    'payMethod': {
      'payMethod': {
        'type': '',
        'value': 'CASH'
      },
      'last4Digits': ''
    }
  },
  {
    'routingInstruction': 'OTHER',
    'owner': 'New1, Ota',
    'window': 7,
    'room': 'ALPHA',
    'routingType': '',
    'payMethod': {
      'payMethod': {
        'type': '',
        'value': 'CASH'
      },
      'last4Digits': ''
    }
  },
  {
    'routingInstruction': 'Breakfast Only',
    'owner': 'Smith, Jack',
    'window': 1,
    'room': '102',
    'routingType': '',
    'payMethod': null
  },
  {
    'routingInstruction': 'Room and Tax',
    'owner': 'Smith, Danny',
    'window': 1,
    'room': '',
    'routingType': '',
    'payMethod': null
  },
  {
    'routingInstruction': 'Telephone',
    'owner': 'Samurai, Test User D',
    'window': 1,
    'room': '1119',
    'routingType': '',
    'payMethod': null
  }
];
