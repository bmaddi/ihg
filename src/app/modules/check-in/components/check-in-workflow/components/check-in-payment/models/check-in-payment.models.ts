import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export interface PaymentCheckInModel {
  reservationDataList?: CheckInGuestInputData;
  errors?: ErrorsModel[];
}

export interface CheckInGuestInputData {
  brandCode: string;
  hotelCode: string;
  reservationDataList: ReservationDataList[];
}

export interface ReservationDataList {
  membershipId: string;
  pmsReservationNumber: string;
  pmsUniqueNumber: string;
  reservationNumber: string;
  errors?: ErrorsModel[];
}

export interface SaveAuthDetailsModel {
  cardDetails: CardDetailsModel[];
  errors?: ErrorsModel[];
}


export interface CardDetailsModel {
  commentId?: Number;
  authAmount: string;
  authCode: string;
  authDate: string;
  cardNumber: string;
  cardType: string;
  commentText: string;
  currencyCode: string;
  expiryDate: string;
}

