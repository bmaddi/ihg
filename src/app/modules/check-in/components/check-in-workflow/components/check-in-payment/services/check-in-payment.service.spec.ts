import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule,  HttpTestingController } from '@angular/common/http/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { HttpRequest } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CheckInPaymentService } from './check-in-payment.service';

import { SaveAuthDetailsModel } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/models/check-in-payment.models';
import { ApiService, EnvironmentService, PageAccessService, UserConfirmationModalConfig, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalComponent, ConfirmationModalService } from 'ihg-ng-common-components';
import { mockFetchCardDetails } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/mock/check-in-payment-mock';

describe('ArrivalsService', () => {
  let service: CheckInPaymentService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmationModalComponent],
      imports: [HttpClientTestingModule, TranslateModule.forChild(), NgbModule],
      providers: [ApiService, EnvironmentService, UserService, PageAccessService, ConfirmationModalService, CheckInPaymentService, TranslateStore]
    })

    service = TestBed.get(CheckInPaymentService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  })

  it('should retrieve posts from api', () => {
    service.fetchAuthDetails('234234').subscribe((data: SaveAuthDetailsModel) => {
      expect(data.cardDetails.length).toBe(1);
      expect(data).toEqual(mockFetchCardDetails);
    });

    const request = httpMock.expectOne(req => req.url.includes('approvalCardInfo'));

    expect(request.request.method).toBe('GET');

    request.flush(mockFetchCardDetails);
  });
});
