import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { ConfirmationModalService } from 'ihg-ng-common-components';
import { UserService } from 'ihg-ng-common-core';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import {
  CheckInGuestInputData,
  PaymentCheckInModel,
  SaveAuthDetailsModel
} from '@modules/check-in/components/check-in-workflow/components/check-in-payment/models/check-in-payment.models';
import { environment } from '@env/environment';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class CheckInPaymentService {
  private readonly API_URL = environment.fdkAPI;
  private _listners = new Subject();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private translate: TranslateService,
    private modalService: ConfirmationModalService) {
  }

  listenTriggerSaveAuthDetails(): Observable<any> {
    return this._listners.asObservable();
  }

  triggerSaveAuthDetails() {
    this._listners.next();
  }

  openAuthorizeCardWarning(): Observable<void> {
    return this.modalService.openConfirmationModal({
      modalHeader: 'LBL_WARN_CHKIN_PAYMT_TTTL',
      primaryButtonLabel: 'LBL_AUTH_CARD_FILE',
      primaryButtonClass: 'btn-primary',
      messageHeader: 'LBL_WARN_CHKIN_PAYMT_DESC',
      windowClass: 'modal-medium',
      dismissible: true
    });
  }

  public getAuthorizePaymentOnFile(reservationData: ReservationDataModel): Observable<PaymentCheckInModel> {
    const apiURl = `${this.API_URL}checkIn/authorize/paymentOnFile`;
    const payload: any = this.getPayload(reservationData);
    return this.http.put(apiURl, payload).pipe(map((response: any) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getPayload(reservationData: ReservationDataModel): CheckInGuestInputData {
    return {
      brandCode: this.getBrandCode(),
      hotelCode: this.getLocationId(),
      reservationDataList: [{
        membershipId: reservationData.ihgRcNumber,
        pmsReservationNumber: reservationData.pmsReservationNumber,
        pmsUniqueNumber: reservationData.pmsUniqueNumber,
        reservationNumber: reservationData.reservationNumber
      }]
    };
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  private getBrandCode(): string {
    return this.userService.getCurrentLocation().brandCode;
  }

  public saveAuthDetails(cardDetails: SaveAuthDetailsModel,reservationId: string): Observable<SaveAuthDetailsModel> {
    const apiURl = `${this.API_URL}payment/addCardInfo/${this.getLocationId()}?grsReservationNumber=${reservationId}`;
    return this.http.put(apiURl, cardDetails).pipe(map((response: any) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public fetchAuthDetails(reservationId: string): Observable<SaveAuthDetailsModel> {
    const apiURl = `${this.API_URL}payment/fetch/approvalCardInfo/${this.getLocationId()}?grsReservationNumber=${reservationId}`;
    return this.http.get(apiURl).pipe(map((response: any) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  getResourceBusyErrorContent(): Partial<ErrorToastMessageTranslations> {
    return {
      generalError: {
        message: 'COM_TOAST_ERR',
        detailMessage: this.translate.instant('LBL_RESOURCE_LOCKED_FAILURE_MSG')
      }
    };
  }

}
