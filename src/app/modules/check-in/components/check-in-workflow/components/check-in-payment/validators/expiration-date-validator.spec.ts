import { AbstractControl } from '@angular/forms';
import { ValidateCardExpiration } from './expiration-date-validator';

describe('ValidateCardExpiration Validator', () => {

  it('should test month value zero is invalid', () => {
    const control = { value: '00/22' };
    expect(ValidateCardExpiration(control as AbstractControl)).toEqual({ 'inValidMonth': true });
  });

  it('should test month value greater than 12 is invalid', () => {
    const control = { value: '13/22' };
    expect(ValidateCardExpiration(control as AbstractControl)).toEqual({ 'inValidMonth': true });
  });

  it('Should test if month value is between 0 and 13 the value is valid', () => {
    const control = { value: '11/22' };
    expect(ValidateCardExpiration(control as AbstractControl)).toEqual(null);
  });

});