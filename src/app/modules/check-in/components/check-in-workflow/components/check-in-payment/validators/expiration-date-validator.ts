import { AbstractControl } from '@angular/forms';

export function ValidateCardExpiration(control: AbstractControl) {
  if(control && control.value) {
    const fieldVal = control.value;
    const splittedFieldVal = fieldVal.split('/');
    const monthVal = splittedFieldVal[0];
    const yearVal = splittedFieldVal[1];
    if(monthVal.trim().length == 2 && yearVal.trim().length == 2) {
      if((Number(monthVal) === 0) || (Number(monthVal) > 12)) {      
        return { inValidMonth: true };
      }   
    } 
    return null;
  }
  return null;
}
