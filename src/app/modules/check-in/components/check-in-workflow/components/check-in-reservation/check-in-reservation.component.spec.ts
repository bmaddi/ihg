import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TitleCasePipe } from '@angular/common';

import { CheckInReservationComponent } from './check-in-reservation.component';
import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { CheckInService } from '@app/modules/check-in/services/check-in.service';
import { CheckInWorkflowService } from '../../services/check-in-workflow.service';
import { ReservationDataModel } from '@app/modules/check-in/models/check-in.models';

let gaServiceStub: Partial<GoogleAnalyticsService>;
let userServiceStub: Partial<UserService>;
let checkInServiceStub: Partial<CheckInService>;
let workflowServiceStub: Partial<CheckInWorkflowService>;

userServiceStub = {};
gaServiceStub = {};
checkInServiceStub = {};
workflowServiceStub = {};
import { BrowserModule, By } from '@angular/platform-browser';
import { MemberNameMap } from './constants/check-in-reservation-constants';

xdescribe('CheckInReservationComponent', () => {
  let component: CheckInReservationComponent;
  let fixture: ComponentFixture<CheckInReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckInReservationComponent],
      imports: [BrowserModule, HttpClientTestingModule],
      providers: [
        TitleCasePipe,
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: GoogleAnalyticsService, useValue: gaServiceStub },
        { provide: UserService, useValue: userServiceStub },
        { provide: CheckInService, useValue: checkInServiceStub },
        { provide: CheckInWorkflowService, useValue: workflowServiceStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInReservationComponent);
    component = fixture.componentInstance;

  });

  it('should create an instance of this component', () => {

    const guestData: ReservationDataModel = {
      'amenityFlag': true,
      'prefixName': 'Mr',
      'firstName': 'Saurav',
      'lastName': 'Agrawal',
      'suffixName': '',
      'reservationNumber': '44002733',
      'pmsReservationNumber': '447469',
      'pmsReservationStatus': 'DUEIN',
      'pmsUniqueNumber': '466611',
      'arrivalTime': '',
      'checkInDate': '2019-06-05',
      'checkOutDate': '2019-06-07',
      'numberOfNights': 2,
      'ihgRcNumber': '123456',
      'ihgRcMembership': 'Spire Elite',
      'ihgRcPoints': '5000',
      'vipCode': '',
      'keysCut': false,
      'companyName': 'ARKANSAS HOSPICE INC',
      'groupName': 'IHG Concerto',
      'rateCategoryCode': 'IGBBB',
      'roomTypeCode': 'KEXG',
      'roomTypeDescription': '1 KING BED EXECUTIVE',
      'numberOfRooms': 1,
      'badges': [
        'spire',
        'ambassador',
        'employee'
      ],
      'assignedRoomNumber': '',
      'assignedRoomStatus': null,
      'numberOfAdults': 1,
      'numberOfChildren': 0,
      'preferences': [
        'HIGH FLOOR',
        'BALCONY',
        'ROOM NEAR ELEVATOR',
        'OCEAN VIEW',
        'NEAR FITNESS ROOM'
      ],
      'specialRequests': [
        'Need Extra Towels and Water Bottles'
      ],
      'infoItems': [],
      'tasks': [
        {
          'traceId': 405806,
          'traceText': 'Need Extra Towels and Water Bottles',
          'departmentCode': 'ZIF',
          'departmentName': 'CRS Information/Comments',
          'traceTimestamp': '2019-06-06T00:00:00.0000000-04:00',
          'isResolved': true,
          'resolvedTimestamp': '2019-06-04',
          'resolvedBy': 'SUPERVISOR'
        },
        {
          'traceId': 405809,
          'traceText': 'Need Shampoo',
          'departmentCode': 'HSK',
          'departmentName': 'House Keeping',
          'traceTimestamp': '2019-06-04T10:43:00.0000000-04:00',
          'isResolved': true,
          'resolvedTimestamp': '2019-06-04',
          'resolvedBy': 'SUPERVISOR'
        },
      ],
      'prepStatus': 'INPROGRESS',
      'heartBeatActions': {
        'myHotelActions': [
          'GuestProblem',
          'GuestComment',
          'SleepIssue'
        ],
        'otherHotelActions': [
          'GuestLove'
        ]
      },
      'heartBeatComment': 'Good Hospitality. Looking forward to revisit again',
      'heartBeatImprovement': 'Add more Indoor Events',
      'otaFlag': false,
      'resVendorCode': 'BKGTL',
      'ratesInfo': {
        'currencyCode': 'EUR',
        'totalAmountBeforeTax': '755.14',
        'totalAmountAfterTax': '808.00',
        'totalTaxes': '52.86'
      },
      'payment': {
        'paymentInformation': {
          'card': {
            'code': 'VS',
            'expireDate': '0420',
            'number': '7472471499080010'
          }
        }
      },
      'inspected': false,
      'paymentType': 'CARD'
    };

    component.guestData = guestData;
    fixture.detectChanges();
    expect(component).toBeDefined();
  });

  it('should contain \'the guest\' in the Act on message for loyalty memebers', () => {

    const guestData: ReservationDataModel = {
      'amenityFlag': true,
      'prefixName': 'Mr',
      'firstName': 'Saurav',
      'lastName': 'Agrawal',
      'suffixName': '',
      'reservationNumber': '44002733',
      'pmsReservationNumber': '447469',
      'pmsReservationStatus': 'DUEIN',
      'pmsUniqueNumber': '466611',
      'arrivalTime': '',
      'checkInDate': '2019-06-05',
      'checkOutDate': '2019-06-07',
      'numberOfNights': 2,
      'ihgRcNumber': '123456',
      'ihgRcMembership': 'Spire Elite',
      'ihgRcPoints': '5000',
      'vipCode': '',
      'keysCut': false,
      'companyName': 'ARKANSAS HOSPICE INC',
      'groupName': 'IHG Concerto',
      'rateCategoryCode': 'IGBBB',
      'roomTypeCode': 'KEXG',
      'roomTypeDescription': '1 KING BED EXECUTIVE',
      'numberOfRooms': 1,
      'badges': [
        'spire',
        'ambassador',
        'employee'
      ],
      'assignedRoomNumber': '',
      'assignedRoomStatus': null,
      'numberOfAdults': 1,
      'numberOfChildren': 0,
      'preferences': [
        'HIGH FLOOR',
        'BALCONY',
        'ROOM NEAR ELEVATOR',
        'OCEAN VIEW',
        'NEAR FITNESS ROOM'
      ],
      'specialRequests': [
        'Need Extra Towels and Water Bottles'
      ],
      'infoItems': [],
      'tasks': [
        {
          'traceId': 405806,
          'traceText': 'Need Extra Towels and Water Bottles',
          'departmentCode': 'ZIF',
          'departmentName': 'CRS Information/Comments',
          'traceTimestamp': '2019-06-06T00:00:00.0000000-04:00',
          'isResolved': true,
          'resolvedTimestamp': '2019-06-04',
          'resolvedBy': 'SUPERVISOR'
        },
        {
          'traceId': 405809,
          'traceText': 'Need Shampoo',
          'departmentCode': 'HSK',
          'departmentName': 'House Keeping',
          'traceTimestamp': '2019-06-04T10:43:00.0000000-04:00',
          'isResolved': true,
          'resolvedTimestamp': '2019-06-04',
          'resolvedBy': 'SUPERVISOR'
        },
      ],
      'prepStatus': 'INPROGRESS',
      'heartBeatActions': {
        'myHotelActions': [
          'GuestProblem',
          'GuestComment',
          'SleepIssue'
        ],
        'otherHotelActions': [
          'GuestLove'
        ]
      },
      'heartBeatComment': 'Good Hospitality. Looking forward to revisit again',
      'heartBeatImprovement': 'Add more Indoor Events',
      'otaFlag': false,
      'resVendorCode': 'BKGTL',
      'ratesInfo': {
        'currencyCode': 'EUR',
        'totalAmountBeforeTax': '755.14',
        'totalAmountAfterTax': '808.00',
        'totalTaxes': '52.86'
      },
      'payment': {
        'paymentInformation': {
          'card': {
            'code': 'VS',
            'expireDate': '0420',
            'number': '7472471499080010'
          }
        }
      },
      'inspected': false,
      'paymentType': 'CARD'
    };

    guestData.ihgRcNumber = '';

    component.guestData = guestData;
    fixture.detectChanges();

    const bannerDe: DebugElement = fixture.debugElement;
    const bannerEl: HTMLElement = bannerDe.nativeElement;
    const span: any = bannerEl.querySelectorAll('span[translate=\'LBL_RECMD_LYLTY_ENRL\']');
    expect(span.textContent).toContain('the guests');
  });

  it('should contain \'the guest\' keyword in Act On message for non loyalty members', () => {
    const guestData: ReservationDataModel = {
      'amenityFlag': true,
      'prefixName': 'Mr',
      'firstName': 'Saurav',
      'lastName': 'Agrawal',
      'suffixName': '',
      'reservationNumber': '44002733',
      'pmsReservationNumber': '447469',
      'pmsReservationStatus': 'DUEIN',
      'pmsUniqueNumber': '466611',
      'arrivalTime': '',
      'checkInDate': '2019-06-05',
      'checkOutDate': '2019-06-07',
      'numberOfNights': 2,
      'ihgRcNumber': '123456',
      'ihgRcMembership': 'Spire Elite',
      'ihgRcPoints': '5000',
      'vipCode': '',
      'keysCut': false,
      'companyName': 'ARKANSAS HOSPICE INC',
      'groupName': 'IHG Concerto',
      'rateCategoryCode': 'IGBBB',
      'roomTypeCode': 'KEXG',
      'roomTypeDescription': '1 KING BED EXECUTIVE',
      'numberOfRooms': 1,
      'badges': [
        'spire',
        'ambassador',
        'employee'
      ],
      'assignedRoomNumber': '',
      'assignedRoomStatus': null,
      'numberOfAdults': 1,
      'numberOfChildren': 0,
      'preferences': [
        'HIGH FLOOR',
        'BALCONY',
        'ROOM NEAR ELEVATOR',
        'OCEAN VIEW',
        'NEAR FITNESS ROOM'
      ],
      'specialRequests': [
        'Need Extra Towels and Water Bottles'
      ],
      'infoItems': [],
      'tasks': [
        {
          'traceId': 405806,
          'traceText': 'Need Extra Towels and Water Bottles',
          'departmentCode': 'ZIF',
          'departmentName': 'CRS Information/Comments',
          'traceTimestamp': '2019-06-06T00:00:00.0000000-04:00',
          'isResolved': true,
          'resolvedTimestamp': '2019-06-04',
          'resolvedBy': 'SUPERVISOR'
        },
        {
          'traceId': 405809,
          'traceText': 'Need Shampoo',
          'departmentCode': 'HSK',
          'departmentName': 'House Keeping',
          'traceTimestamp': '2019-06-04T10:43:00.0000000-04:00',
          'isResolved': true,
          'resolvedTimestamp': '2019-06-04',
          'resolvedBy': 'SUPERVISOR'
        },
      ],
      'prepStatus': 'INPROGRESS',
      'heartBeatActions': {
        'myHotelActions': [
          'GuestProblem',
          'GuestComment',
          'SleepIssue'
        ],
        'otherHotelActions': [
          'GuestLove'
        ]
      },
      'heartBeatComment': 'Good Hospitality. Looking forward to revisit again',
      'heartBeatImprovement': 'Add more Indoor Events',
      'otaFlag': false,
      'resVendorCode': 'BKGTL',
      'ratesInfo': {
        'currencyCode': 'EUR',
        'totalAmountBeforeTax': '755.14',
        'totalAmountAfterTax': '808.00',
        'totalTaxes': '52.86'
      },
      'payment': {
        'paymentInformation': {
          'card': {
            'code': 'VS',
            'expireDate': '0420',
            'number': '7472471499080010'
          }
        }
      },
      'inspected': false,
      'paymentType': 'CARD'
    };

    component.guestData = guestData;
    fixture.detectChanges();

    const bannerDe: DebugElement = fixture.debugElement;
    const bannerEl: HTMLElement = bannerDe.nativeElement;
    const span: any = bannerEl.querySelectorAll('span[translate=\'LBL_MTCH_GST\']');
    expect(span.textContent).toContain('the guests');
  });

  it('should have div with \'Welcome\'', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const p = bannerElement.querySelector('div');
    expect(p.textContent).toContain('Welcome');
  });

  it('should welcome user \'GuestName\'', () => {
    const el = fixture.nativeElement.querySelector('.title-text');
    const guestData = {username: 'GuestName'};
    fixture.detectChanges();
    expect(el.textContent).toContain(guestData.username);
  });

  it('should contain text \'Act On\'', () => {
    const el = fixture.nativeElement.querySelector('.act-on-title');
    fixture.detectChanges();
    expect(el.textContent).toContain('Act On');
  });

  it('should hide contents if non-loyalty member', () => {
    let guestData = {ihgRcNumber: null};
    // should not be rendered
    expect(guestData.ihgRcNumber).toBeNull();
    expect(fixture.debugElement.query(By.css('.reward')).nativeElement).toBeFalsy();
    // trigger change
    fixture.detectChanges();
    guestData = {ihgRcNumber: 7777777};
    // should be rendered later
    expect(guestData.ihgRcNumber).toEqual(7777777);
    expect(fixture.debugElement.query(By.css('.reward')).nativeElement).toBeTruthy();
  });

  it('should return complete spire loyalty level', () => {
    const el = fixture.nativeElement.querySelector('ThankGuest-SID');
    const guestData = {ihgRcMembership: 'spire'};
    if (guestData.ihgRcMembership !== MemberNameMap['spire']) {
      expect(el.textContent).toEqual('Spire Elite');
    }
  });

  it('should return complete gold loyalty level', () => {
    const el = fixture.nativeElement.querySelector('ThankGuest-SID');
    const guestData = {ihgRcMembership: 'gold'};
    if (guestData.ihgRcMembership !== MemberNameMap['gold']) {
      expect(el.textContent).toEqual('Gold Elite');
    }
  });

  it('should return complete pltnm loyalty level', () => {
    const el = fixture.nativeElement.querySelector('ThankGuest-SID');
    const guestData = {ihgRcMembership: 'pltnm'};
    if (guestData.ihgRcMembership !== MemberNameMap['pltnm']) {
      expect(el.textContent).toEqual('Platinum Elite');
    }
  });

  it('should return reward level as club', () => {
    const el = fixture.nativeElement.querySelector('ThankGuest-SID');
    const guestData = {ihgRcMembership: 'club'};
    if (guestData.ihgRcMembership === MemberNameMap['club']) {
      expect(el.textContent).toEqual('Club');
    }
  });

  it('should return reward level as potentialMember', () => {
    const el = fixture.nativeElement.querySelector('ThankPotentialGuest-SID');
    const guestData = { badges: ['karma', 'potentialMember'] };
    if (guestData.badges.includes(MemberNameMap['potentialMember'])) {
      expect(el.textContent).toContain('potentialMember');
    }
  });

  it('should convert reward level to Title Case', () => {
    // get the name value and display elements from the DOM
    const hostElement = fixture.nativeElement;
    const nameDisplay: HTMLElement = hostElement.querySelector('span');
    // set value to content
    nameDisplay.textContent = 'spire Elite';
    // Tell Angular to update the display binding through the title pipe
    fixture.detectChanges();
    expect(nameDisplay.textContent).toBe('Spire Elite');
  });

});
