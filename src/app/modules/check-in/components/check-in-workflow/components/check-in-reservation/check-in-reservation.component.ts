import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import { UtilityService } from '@services/utility/utility.service';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import {
  POINTS_ERROR_MSG,
  MemberNameMap
} from '@modules/check-in/components/check-in-workflow/components/check-in-reservation/constants/check-in-reservation-constants';
import { AmenityOptionsResponseModel } from '@modules/check-in/components/check-in-workflow/models/check-in-workflow.models';

import { POINTS_ERR_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';

@Component({
  selector: 'app-check-in-reservation',
  templateUrl: './check-in-reservation.component.html',
  styleUrls: ['./check-in-reservation.component.scss']
})
export class CheckInReservationComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public appendFlag = true;
  public reportPointsErrorAutoFill = POINTS_ERR_REPORT_ISSUE_AUTOFILL;
  public pointsErrorContent = POINTS_ERROR_MSG;
  private reservation$: Subscription = new Subscription();

  @Input() guestData: ReservationDataModel;

  constructor(private gaService: GoogleAnalyticsService, private titleCase: TitleCasePipe, private checkInService: CheckInService,
              private utility: UtilityService, private workflowService: CheckInWorkflowService) {
    super();
  }

  ngOnInit() {
  }

  public getGuestName(): string {
    return this.utility.concatNameWithPrefix(this.guestData.prefixName, this.guestData.firstName, this.guestData.lastName);
  }

  public getRewardsLevel(): string {
    return MemberNameMap[this.guestData.ihgRcMembership];
  }

  public openEnrollmentModal(): void {
    this.checkInService.openEnrollmentModal(this.guestData);
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.GA_ENROLL_LNK,
      CheckinAnalyticsConstants.SELECTED_CHECK_IN);
  }

  public postAmenitySelection(): void {
    this.reservation$.add(this.workflowService.postSelectedAmenityOption(this.guestData)
      .subscribe((response: AmenityOptionsResponseModel) => super.checkIfAnyApiErrors(response),
        (error) => {
          super.processHttpErrors(error);
        }));
  }

  ngOnDestroy(): void {
    this.reservation$.unsubscribe();
  }
}
