import { ErrorToastMessageTranslations } from '@app-shared/components/error-toast-message/error-toast-message.component';

export const POINTS_ERROR_MSG: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_CHKIN_WLC_AMNTY_ERR'
  }
};

export const MemberNameMap = {
  club: 'Club',
  gold: 'Gold Elite',
  pltnm: 'Platinum Elite',
  spire: 'Spire Elite',
  potentialMember: 'Potential Member'
};
