import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInRoomAssignComponent } from './check-in-room-assign.component';

xdescribe('CheckInRoomAssignComponent', () => {
  let component: CheckInRoomAssignComponent;
  let fixture: ComponentFixture<CheckInRoomAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInRoomAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInRoomAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
