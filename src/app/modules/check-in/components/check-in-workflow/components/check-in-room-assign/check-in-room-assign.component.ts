import { Component, OnInit, Input } from '@angular/core';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';

@Component({
  selector: 'app-check-in-room-assign',
  templateUrl: './check-in-room-assign.component.html',
  styleUrls: ['./check-in-room-assign.component.scss']
})
export class CheckInRoomAssignComponent implements OnInit {

  @Input() guestData: ReservationDataModel;

  constructor() {
  }

  ngOnInit() {
  }

}
