import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInWorkflowComponent } from './check-in-workflow.component';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { SingleModeEnrollmentModel } from 'ihg-ng-common-pages';
import * as cloneDeep from 'lodash/cloneDeep';
import 'rxjs/add/observable/of';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';
import { CheckInService } from '@app/modules/check-in/services/check-in.service';
import { UserService, TransitionGuardService, UserConfirmationDialogService, DetachedToastMessageService} from 'ihg-ng-common-core';
import {ConfirmationModalService} from 'ihg-ng-common-components';
import { By } from '@angular/platform-browser';
import { ConcertoGridSettingsModule } from 'ihg-ng-common-kendo';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';

xdescribe('CheckInWorkflowComponent', () => {
  let component: CheckInWorkflowComponent;
  let fixture: ComponentFixture<CheckInWorkflowComponent>;
  let arrivalServ: ArrivalsService;
  let guestInfoData = {
    "payIntegratedFlag": "N",
    "prefixName": "",
    "firstName": "HEMA",
    "lastName": "BALAJI",
    "suffixName": "",
    "reservationNumber": "28081438",
    "pmsReservationNumber": "37661",
    "pmsReservationStatus": "DUEIN",
    "pmsUniqueNumber": "36911",
    "inspected": true,
    "arrivalTime": "00:00",
    "checkInDate": "2019-11-19",
    "checkOutDate": "2019-11-20",
    "numberOfNights": 1,
    "ihgRcNumber": "",
    "ihgRcMembership": "non",
    "ihgRcPoints": null,
    "vipCode": "Y",
    "companyName": "",
    "groupName": "",
    "rateCategoryCode": "IGCOR",
    "roomTypeCode": "CSTN",
    "roomTypeDescription": "STANDARD ROOM NONSMOKING",
    "numberOfRooms": 1,
    "badges": [],
    "assignedRoomNumber": "",
    "assignedRoomType": "",
    "assignedRoomStatus": null,
    "numberOfAdults": 1,
    "numberOfChildren": 0,
    "preferences": [],
    "specialRequests": [
      "FORMERLY PART OF CRS NUMBER 24742727TOTAL # OF ROOMS ON ORIGINAL RESERVATION WERE 3"
    ],
    "infoItems": [],
    "tasks": [
      {
        "traceId": 24765,
        "traceText": "FORMERLY PART OF CRS NUMBER 24742727 TOTAL # OF ROOMS ON ORIGINAL RESERVATION WERE 3",
        "departmentCode": "SPLIT",
        "departmentName": "CRS Information/Comments",
        "traceTimestamp": "2019-11-19 10:17:24.207217",
        "isResolved": false
      }
    ],
    "prepStatus": "NOTSTARTED",
    "heartBeatActions": null,
    "heartBeatComment": "",
    "heartBeatImprovement": "",
    "heartBeatActionsInd": "",
    "amenityPoints": null,
    "otaFlag": false,
    "resVendorCode": "",
    "ratesInfo": {
      "currencyCode": "GBP",
      "totalAmountBeforeTax": "",
      "totalAmountAfterTax": "129.0",
      "totalTaxes": ""
    },
    "payment": {
      "paymentInformation": {
        "card": {
          "code": "MC",
          "expirationDate": "",
          "expireDate": "1219",
          "number": "7541526321471732"
        }
      },
      "guaranteeType": ""
    },
    "confirmationDate": "2019-11-19",
    "corporateId": "",
    "iataNumber": null,
    "selectedAmenity": "",
    "paymentType": "CARD",
    "pmsAuthSaveInd": "N",
    "pmsGRSCompareFlag": "N",
    "doNotMoveRoom": false,
    "noPost": false,
    "cdsResUpdtdTs": null,
    "keysCut": false
  };

  function initialiseGuestData() {
    let clonedObject = cloneDeep(guestInfoData);
    clonedObject.isCheckedIn = false;
    clonedObject.selectedAmenity = true;
    clonedObject.assignedRoomNumber = "545454";
    clonedObject.prepStatus = '';
    return clonedObject;
  }


  class MockCheckInService {
    private enrollmentSuccessSource: Subject<SingleModeEnrollmentModel> = new Subject();
    enrollmentSuccess = this.enrollmentSuccessSource.asObservable();

    openCheckInWarning(){

    }

    emitWorkflowTabEvent(tabId) {

    }

    getCheckInReservationData(): Observable<any> {
      return Observable.of({
        json: () => guestInfoData
      });
    }

    getReservationNumber(): string {
      return "4353453";
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInWorkflowComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        CommonTestModule,
        TranslateModule.forRoot(),
        RouterTestingModule.withRoutes([
        ])],
        providers: [     TranslateService, UserService, ConfirmationModalService, TransitionGuardService, UserConfirmationDialogService,DetachedToastMessageService,
          {provide: CheckInService, useClass:MockCheckInService},
          {provide : ArrivalsService, useClass: ArrivalsService}
         ]
    })
    .compileComponents().then(() => {
      setTestEnvironment();
    });
  }));

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInWorkflowComponent);
    component = fixture.componentInstance;
    arrivalServ = TestBed.get(ArrivalsService);
    component.guestData = cloneDeep(guestInfoData);
    fixture.detectChanges();
  });

  it('Verify if assignment tab is selected if there is unsaved tasks for non integrated hotels', () => {
    component.guestData = initialiseGuestData();
    component.payIntegratedFlag = "N";
    component.guestData.ineligibleReservation = false;
    arrivalServ.emitTabChange();
    arrivalServ.dnmReasonUnsavedData = false;
    arrivalServ.setUnsavedTask(true);
    component.handleUnsavedChangesTabChangeEvent();
    spyOn(component,'navigateToGivenTab');
    fixture.detectChanges();
    const expectedSelectedTabVal = component.workflowTabs[component.workflowTabs[1].pos].id;
    expect(component.selectedTab).toEqual(expectedSelectedTabVal)
  });

  it('Verify if payment tab is selected if there is unsaved auth details for non integrated hotels', () => {
    component.guestData = initialiseGuestData();
    component.payIntegratedFlag = "N";
    component.guestData.ineligibleReservation = false;
    arrivalServ.emitTabChange();
    arrivalServ.dnmReasonUnsavedData = false;
    arrivalServ.setUnsavedTask(false);
    arrivalServ.creditCardUnsavedData = true;
    component.handleUnsavedChangesTabChangeEvent();
    spyOn(component,'navigateToGivenTab');
    fixture.detectChanges();
    const expectedSelectedTabVal = component.workflowTabs[component.workflowTabs[2].pos].id;
    expect(component.selectedTab).toEqual(expectedSelectedTabVal)
  });

  it('Verify if assignment tab is selected if there is unsaved tasks for integrated hotels', () => {
    component.guestData = initialiseGuestData();
    component.payIntegratedFlag = "Y";
    component.guestData.ineligibleReservation = false;
    arrivalServ.emitTabChange();
    arrivalServ.dnmReasonUnsavedData = false;
    arrivalServ.setUnsavedTask(true);
    arrivalServ.creditCardUnsavedData = false;
    component.handleUnsavedChangesTabChangeEvent();
    spyOn(component,'navigateToGivenTab');
    fixture.detectChanges();
    const expectedSelectedTabVal = component.workflowTabs[component.workflowTabs[1].pos].id;
    expect(component.selectedTab).toEqual(expectedSelectedTabVal)
  });

  it('Verify if assignment tab is selected if there is unsaved DNM Reason for integrated hotels', () => {
    component.guestData = initialiseGuestData();
    component.payIntegratedFlag = "Y";
    component.guestData.ineligibleReservation = false;
    arrivalServ.emitTabChange();
    arrivalServ.dnmReasonUnsavedData = true;
    arrivalServ.setUnsavedTask(false);
    arrivalServ.creditCardUnsavedData = false;
    component.handleUnsavedChangesTabChangeEvent();
    spyOn(component,'navigateToGivenTab');
    fixture.detectChanges();
    const expectedSelectedTabVal = component.workflowTabs[component.workflowTabs[1].pos].id;
    expect(component.selectedTab).toEqual(expectedSelectedTabVal);
  });

  it('Verify if assignment tab is selected if there is unsaved DNM Reason for non integrated hotels', () => {
    component.guestData = initialiseGuestData();
    component.payIntegratedFlag = "N";
    component.guestData.ineligibleReservation = false;
    arrivalServ.emitTabChange();
    arrivalServ.dnmReasonUnsavedData = true;
    arrivalServ.setUnsavedTask(false);
    component.handleUnsavedChangesTabChangeEvent();
    spyOn(component,'navigateToGivenTab');
    fixture.detectChanges();
    const expectedSelectedTabVal = component.workflowTabs[component.workflowTabs[1].pos].id;
    expect(component.selectedTab).toEqual(expectedSelectedTabVal)
  });

  it('Verify Check In button Disabled when amenity selection is not made for Non-US Hotel', () => {
    let clonedObject = cloneDeep(guestInfoData);
    clonedObject.isCheckedIn = false;
    clonedObject.selectedAmenity = false;
    component.guestData = clonedObject;
    component.payIntegratedFlag = "N";
    component.selectedTab = "payment";
    component.guestData.ineligibleReservation = false;
    component.ngOnInit();
    fixture.detectChanges();
    const checkinBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="CheckInBtn-SID"]'));
    expect(checkinBtn.nativeElement.disabled).toBeTruthy();
  });

  it('Desktop- Verify Check In button Disabled when room has not been assigned for Non-US Hotel', () => {
    let clonedObject = cloneDeep(guestInfoData);
    clonedObject.isCheckedIn = false;
    clonedObject.selectedAmenity = true;
    clonedObject.assignedRoomNumber = "";
    component.guestData = clonedObject;
    component.payIntegratedFlag = "N";
    component.selectedTab = "payment";
    component.guestData.ineligibleReservation = false;
    component.ngOnInit();
    fixture.detectChanges();
    const checkinBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="CheckInBtn-SID"]'));
    expect(checkinBtn.nativeElement.disabled).toBeTruthy();
  });

  it(' Desktop- Verify Check In button Disabled when assigned room is not in correct status for Non-US Hotel', () => {
    let clonedObject = cloneDeep(guestInfoData);
    clonedObject.isCheckedIn = false;
    clonedObject.selectedAmenity = true;
    clonedObject.assignedRoomNumber = "545454";
    clonedObject.prepStatus = 'INPROGRESS';
    component.guestData = clonedObject;
    component.payIntegratedFlag = "N";
    component.selectedTab = "payment";
    component.guestData.ineligibleReservation = false;
    component.ngOnInit();
    fixture.detectChanges();
    const checkinBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="CheckInBtn-SID"]'));
    expect(checkinBtn.nativeElement.disabled).toBeTruthy();
  });

  it(' Desktop- Verify Check In button Enabled when amenity selected, room assigned and ready', () => {
    let clonedObject = cloneDeep(guestInfoData);
    clonedObject.isCheckedIn = false;
    clonedObject.selectedAmenity = true;
    clonedObject.assignedRoomNumber = "545454";
    clonedObject.prepStatus = '';
    component.guestData = clonedObject;
    component.payIntegratedFlag = "N";
    component.selectedTab = "payment";
    component.guestData.ineligibleReservation = false;
    component.ngOnInit();
    fixture.detectChanges();
    const checkinBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="CheckInBtn-SID"]'));
    expect(checkinBtn.nativeElement.disabled).toBeFalsy();
  });

});
