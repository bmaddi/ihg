import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { CHECK_IN_TABS } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-constants';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { CheckInService } from '@app/modules/check-in/services/check-in.service';
import { STATE_DETAILS, PREP_STATUSES } from '@modules/check-in/constants/check-in-constants';
import { CheckInPaymentComponent } from './check-in-payment/components/check-in-payment.component';

@Component({
  selector: 'app-check-in-workflow',
  templateUrl: './check-in-workflow.component.html',
  styleUrls: ['./check-in-workflow.component.scss']
})
export class CheckInWorkflowComponent implements OnInit, OnDestroy {
  public workflowTabs = CHECK_IN_TABS;
  public selectedTab = this.workflowTabs[0].id;
  private rootSubscription = new Subscription();
  public stateName = STATE_DETAILS.stateName;
  public enableCheckInBool = false;
  public checkinChangedNow = false;

  @Input() guestData: ReservationDataModel;
  @Input() payIntegratedFlag: string;
  @Input() pmsAuthSaveInd: string;
  @ViewChild('paymentCheckInComponent') paymentCheckInComponent: CheckInPaymentComponent;

  constructor(private prepareArrivalsService: ArrivalsService, private router: Router, private checkInService: CheckInService) {
  }

  ngOnInit() {
    this.prepareArrivalsService.ifCheckInPage = true;
    this.prepareArrivalsService.registerInTransitionGuard();
    this.rootSubscription.add(this.prepareArrivalsService.listenTabChange().subscribe(() => {
      this.handleUnsavedChangesTabChangeEvent();
    }));
  }

  private hasUnsavedDNMReasonOrTasks(): boolean {
    return (this.selectedTab === this.workflowTabs[1].id && (this.prepareArrivalsService.getUnsavedTask() || this.prepareArrivalsService.dnmReasonUnsavedData));
  }

  private hasUnsavedAuthDetails(): boolean {
    return (this.selectedTab === this.workflowTabs[2].id && this.prepareArrivalsService.creditCardUnsavedData);
  }

  public handleUnsavedChangesTabChangeEvent(): void {
    if (this.payIntegratedFlag === 'N') {
      if (!(this.hasUnsavedDNMReasonOrTasks() || this.hasUnsavedAuthDetails())) {
        if (this.prepareArrivalsService.getUnsavedTask() || this.prepareArrivalsService.dnmReasonUnsavedData) {
          this.navigateToGivenTab(this.workflowTabs[1].pos);
        } else {
          this.navigateToGivenTab(this.workflowTabs[2].pos);
          this.triggerActOnPanelChange();
        }
      }
    } else {
      this.navigateToGivenTab(this.workflowTabs[1].pos);
    }
  }

  public handleTabChange(event: NgbTabChangeEvent): void {
    event.preventDefault();
    this.checkUnsavedTasks(() => {
      this.checkInService.emitWorkflowTabEvent(event.nextId);
      this.changeTab(event);
    });
    this.triggerActOnPanelChange();
  }

  private triggerActOnPanelChange() {
    if (this.paymentCheckInComponent && (this.selectedTab === this.workflowTabs[2].id) && this.payIntegratedFlag === 'N') {
      this.paymentCheckInComponent.setActOnCardType();
    }
  }

  private checkUnsavedTasks(callbackFn: Function): void {
    this.prepareArrivalsService.handleUnsavedTasks(callbackFn, null, this.guestData);
  }

  private changeTab(event: NgbTabChangeEvent): void {
    this.selectedTab = event.nextId;
  }

  public handleBackToGuestList(): void {
    this.checkUnsavedTasks(this.navigateToGuestList.bind(this));
  }

  private navigateToGuestList(): void {
    this.router.navigate(['guest-list-details']);
  }

  public handleNext(): void {
    this.checkUnsavedTasks(this.navigateToNextTab.bind(this));
    this.triggerActOnPanelChange();
  }

  private navigateToNextTab(): void {
    const currentTab = this.selectedTab;
    const selectedTabPos = this.workflowTabs.filter(function (item) {
      return item.id === currentTab;
    })[0].pos + 1;
    this.navigateToGivenTab(selectedTabPos);
  }

  private handlePreviousTab(): void {
    this.checkUnsavedTasks(this.navigateToPrevTab.bind(this));
  }

  private navigateToPrevTab(): void {
    const currentTab = this.selectedTab;
    const selectedTabPos = this.workflowTabs.filter(function (item) {
      return item.id === currentTab;
    })[0].pos - 1;
    this.navigateToGivenTab(selectedTabPos);
  }

  public navigateToGivenTab(tabPosition: number): void {
    this.selectedTab = this.workflowTabs[tabPosition].id;
    this.checkInService.emitWorkflowTabEvent(this.selectedTab);
  }

  public checkRoomAssignmentWarning(): boolean {
    return !!this.guestData.assignedRoomNumber && this.guestData.prepStatus === PREP_STATUSES.inProgress;
  }

  public checkRoomAssignmentSuccess(): boolean {
    return !!this.guestData.assignedRoomNumber && this.guestData.prepStatus !== PREP_STATUSES.inProgress;
  }

  public isLastActiveTab(): boolean {
    return this.selectedTab === this.workflowTabs[3].id ||
      (this.selectedTab === this.workflowTabs[1].id && this.guestData.ineligibleReservation);
  }

  public enableCheckIn(event: boolean): void {
    this.enableCheckInBool = event;
    if (event) {
      this.prepareArrivalsService.creditCardUnsavedData = false;
      if (!this.prepareArrivalsService.getUnsavedTask() && !this.prepareArrivalsService.dnmReasonData) {
        this.prepareArrivalsService.setGenericUnsavedDataChange(false);
      }
    }
  }

  public triggerCheckIn(): void {
    this.checkinChangedNow = true;
    this.checkInService.triggerCheckInNonIntegrated();
  }

  ngOnDestroy() {
    this.prepareArrivalsService.ifCheckInPage = false;
    this.prepareArrivalsService.creditCardUnsavedData = false;
    this.rootSubscription.unsubscribe();
  }

  public isRoomReady(): boolean {
    return this.guestData.prepStatus !== PREP_STATUSES.inProgress;
  }

  public checkIsRoomReady() {
    if (!this.isAmenitySelected() || !this.isRoomAssigned() || (this.isRoomAssigned() && !this.isRoomReady())) {
      return false;
    }
    return true;
  }

  private isRoomAssigned(): boolean {
    return !!this.guestData.assignedRoomNumber;
  }

  private isAmenitySelected(): boolean {
    return this.isLoyaltyNonOtaReservation() && this.guestData.amenityFlag ? (!this.guestData.isMasterReservation && this.guestData.amenityFlag) || !!this.guestData.selectedAmenity : true;
  }

  private isLoyaltyNonOtaReservation(): boolean {
    return !!this.guestData.ihgRcNumber && !this.guestData.otaFlag;
  }

}
