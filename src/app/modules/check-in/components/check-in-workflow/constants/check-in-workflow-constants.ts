export const CHECK_IN_TABS = [
  {
    id: 'reservation',
    label: 'LBL_RSVN',
    pos: 0
  },
  {
    id: 'assignment',
    label: 'LBL_RM_ASGMT',
    pos: 1
  },
  {
    id: 'payment',
    label: 'LBL_PYMT',
    pos: 2
  },
  {
    id: 'keys',
    label: 'LBL_KEYS',
    pos: 3
  }
];

export const WORKFLOW_TABS_POS = {
  reservation: 0,
  assignment: 1,
  payment: 2,
  keys: 3
};

export const AMENITY_MAP = {
  otherAmenity: 'Other Amenity',
  points: 'Points',
  none: 'None'
};
