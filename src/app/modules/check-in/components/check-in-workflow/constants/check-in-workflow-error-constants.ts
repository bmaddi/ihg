export const GET_AMENITY_ERROR_CONTENT = {
  timeout: {
    message: 'LBL_ERR_ELG_CHK',
    maxErrorMessage: 'LBL_ERR_CHK_AMN_ELG_LOY_CON',
    icon: '',
    showRefreshBtn: true,
    refreshActionText: 'LBL_ERR_REFRESH'
  },
  general: {
    message: 'LBL_ERR_ELG_CHK',
    maxErrorMessage: 'LBL_ERR_CHK_AMN_ELG_LOY_CON',
    icon: '',
    showRefreshBtn: true,
    refreshActionText: 'LBL_ERR_REFRESH'
  }
};

export const CUT_KEY_ERROR_CONTENT = {
  assignRoom: {
    retryButton: 'LBL_ERR_TRYAGAIN',
    reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
    timeoutError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_CUT_KEY'
    },
    generalError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_CUT_KEY'
    }
  }
};

export const MANAGE_STAY_ROOM_ASSIGNMENT_ERROR_AUTOFILL = {
  roomAssignmentError: {
    function: 'Guests',
    topic: 'Manage Stay - Room Assignment',
    subtopic: 'Error Message'
  }
};
