import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export interface LoyaltyAmenityPointsModel {
  hotelCode: string;
  membershipId: string;
  amenityPoints: string;
  status?: string;
}

export interface LoyaltyAmenityDataModel {
  loyaltyAmenityPoints: LoyaltyAmenityPointsModel;
  errors?: ErrorsModel[];
}

export interface AmenityOptionsModel {
  hotelCode: string;
  reservationNumber: string;
  selectedAmenity: string;
  status: string;
}

export interface AmenityOptionsResponseModel {
  amenityOptionSelection: AmenityOptionsModel;
  errors?: ErrorsModel[];
}
