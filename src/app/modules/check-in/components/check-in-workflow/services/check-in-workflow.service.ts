import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { catchError, map } from 'rxjs/operators';
import {
  AmenityOptionsResponseModel,
  LoyaltyAmenityDataModel
} from '@modules/check-in/components/check-in-workflow/models/check-in-workflow.models';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { AMENITY_MAP } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-constants';

@Injectable({
  providedIn: 'root'
})
export class CheckInWorkflowService {
  private readonly API_URL = environment.fdkAPI;

  constructor(private http: HttpClient, private userService: UserService) {
  }

  public retrieveAmenityPoints(reservationData: ReservationDataModel, displaySpinner = false): Observable<LoyaltyAmenityDataModel> {
    const apiURl = `${this.API_URL}loyalty/amenityPoints/retrieve/${this.getLocationId()}${this.getAmenityServiceParams(reservationData)}`;
    const httpOptions = { headers: new HttpHeaders({ spinnerConfig: displaySpinner ? 'Y' : 'N' }) };
    return this.http.get(apiURl, httpOptions).pipe(map((response: LoyaltyAmenityDataModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  private getAmenityServiceParams(data: ReservationDataModel): string {
    return `?brandCode=${this.getBrandCode()}&membershipId=${data.ihgRcNumber}&confirmationNumber=${data.reservationNumber}`;
  }

  public getBrandCode(): string {
    return this.userService.getCurrentLocation().brandCode;
  }

  public postDeclineAmenityPoints(reservationData: ReservationDataModel): Observable<LoyaltyAmenityDataModel> {
    const postOrDecline = reservationData.selectedAmenity === AMENITY_MAP.points ? 'post' : 'decline';
    const apiURl = `${this.API_URL}loyalty/amenityPoints/${postOrDecline}/${this.getLocationId()}`;
    const httpOptions = { params: this.postDeclineAmenityParams(reservationData) };
    return this.http.put(apiURl, null, httpOptions).pipe(map((response: LoyaltyAmenityDataModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private postDeclineAmenityParams(reservationData: ReservationDataModel) {
    return {
      membershipId: reservationData.ihgRcNumber,
      brandCode: this.getBrandCode(),
      checkInDate: reservationData.checkInDate,
      checkOutDate: reservationData.checkOutDate,
      amenityPoints: reservationData.amenityPoints,
      confirmationNumber: reservationData.reservationNumber,
      confirmationDate: reservationData.confirmationDate
    };
  }

  public generateKey(keyCount: number, pmsReservationNumber: string, newKey: boolean, encoderId: string) {
    const apiURl = `${this.API_URL}checkIn/cutKeys?pmsReservationNumber=${pmsReservationNumber}&keyCount=${keyCount}&newKey=${newKey}&encoderId=${encoderId}`;

    return this.http.get(apiURl).pipe(map((response: any) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public postSelectedAmenityOption(reservationData: ReservationDataModel): Observable<AmenityOptionsResponseModel> {
    const apiURl = `${this.API_URL}checkIn/post/amenity/optionSelection/${this.getLocationId()}`;
    const httpOptions = { params: this.getOptionSelectionParams(reservationData) };
    return this.http.put(apiURl, null, httpOptions).pipe(map((response: AmenityOptionsResponseModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getOptionSelectionParams(reservationData) {
    return {
      brandCode: this.getBrandCode(),
      selectedAmenity: reservationData.selectedAmenity,
      reservationNumber: reservationData.reservationNumber
    };
  }

}
