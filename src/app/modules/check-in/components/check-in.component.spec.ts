import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInComponent } from './check-in.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  SessionStorageService, GoogleAnalyticsService, ToastMessageModule, UserService, BreadcrumbsService, MenuService, TransitionGuardService,
  UserConfirmationDialogService, AlertType
} from 'ihg-ng-common-core';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { ConfirmationModalService } from 'ihg-ng-common-components';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { CheckInService } from '../services/check-in.service';
import { Observable, BehaviorSubject, Subject, of } from 'rxjs';
import { SingleModeEnrollmentModel, EnrollNewMemberService } from 'ihg-ng-common-pages';
import * as cloneDeep from 'lodash/cloneDeep';
import { ClipboardService } from 'ngx-clipboard';
import { RoomsService } from '@app/modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';

export class MockSessionStorageService {
  getSessionStorage() {
    return {};
  }
  public setSessionStorage(key, value) { }
}

export class MockMenuService {
}

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

class MockBreadcrumbsService {
  subscribeToMenu(): Observable<any> {
    return Observable.of({
      json: () => {}
    });
  }

  setLabel() {}
}

class MockClipboardService {

}


describe('CheckInComponent', () => {
  let component: CheckInComponent;
  let fixture: ComponentFixture<CheckInComponent>;
  let checkInService: CheckInService;
  let roomServ: RoomsService;

  const guestInfoData = {
    'reservationData': {
      'reservationNumber': '34534534',
      'hotelCode': 'GRVAB',
      'loyaltyMembershipId': '',
      'confirmationNumber': '47593592',
      'roomCategoryCode': '',
      'corporateId': '',
      'checkInDate': null,
      'checkOutDate': '2019-10-19',
      'confirmationDate': null,
      'roomNumber': null,
      'iataNumber': null,
      'amenityPoints': null,
      'arrivalDate': '2019-10-18',
      'numberOfNights': 1,
      'numberOfRooms': 1,
      'adultQuantity': 1,
      'childQuantity': 0,
      'rateCategoryCode': 'IGCOR',
      'roomTypeCode': 'KDXG',
      'pmsUniqueNumber': '',
      'pmsGRSCompareFlag': '',
      'cdsResUpdtdTs': {
        'seconds': '60',
        'time': '',
        'timeFormat': 'dd-MMM-yyyy HH:mm:ss',
        'timezone': 'America/New_York'
      },
      'groupCode': '',
      'appUserId': '',
      'rateInfo': {
        'averageRate': '100.00',
        'currency': 'GBP',
        'totalTaxAmount': '16.67',
        'totalAmountAfterTax': '100.00',
        'totalExtraPersonAmount': '0.00',
        'totalServiceChargeAmount': '0.00',
        'dailyRates': [
          {
            'date': '2019-10-18',
            'amountBeforeTax': '83.33',
            'amountAfterTax': '100.00',
            'baseAmount': '100.00'
          }
        ],
        'rateRules': [
          {
            'rateRuleDesc': '',
            'noShowPolicyDesc': '',
            'extraPersonCharge': '0.00',
            'earlyDeparture': '',
            'guaranteePolicy': '',
            'checkinTime': '',
            'checkoutTime': '',
            'tax': '16.67',
            'serviceCharge': '0.00'
          }
        ],
        'serviceChargeIncluded': false,
        'taxesChargeIncluded': true,
        'extraPersonChargeIncluded': true
      },
      'guestInfo': {
        'firstName': 'debasmita',
        'lastName': 'pradhan',
        'middleName': '',
        'addressLine1': '',
        'addressLine2': '',
        'countryCode': '',
        'countryName': '',
        'zipCode': '',
        'stateProvince': '',
        'cityName': '',
        'phoneType': '',
        'phoneNumber': '12111',
        'email': ''
      },
      'rateCategories': [
        'ADAVP',
        'AG000',
        'IBDNG',
        'IBGP2',
        'IBMTF'
      ],
      'doNotMoveRoom': false,
      'productIncludedInOffer': false,
      'serviceChargeIncluded': false,
      'taxesChargeIncluded': true,
      'confirmationDateString': '',
      'checkInDateString': '',
      'checkOutDateString': '2019-10-19'
    }
  };

  class MockGuestListService {
    public userDataSource = new BehaviorSubject(null);
    public userDataSource$ = this.userDataSource.asObservable();
  }


  class MockCheckInService {
    private enrollmentSuccessSource: Subject<SingleModeEnrollmentModel> = new Subject();
    enrollmentSuccess = this.enrollmentSuccessSource.asObservable();

    openCheckInWarning() {
    }

    getCheckInReservationData(resrvNo: string): Observable<any> {
      return of(cloneDeep(guestInfoData));
    }

    getReservationNumber(): string {
      return '4353453';
    }

    getWorkflowTabEvent(): Subject<string> {
      return new Subject<string>();
    }

    updateReservationNumber(): void {
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckInComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        ToastMessageModule,
        RouterTestingModule.withRoutes([])],
      providers: [TranslateService, UserService, TitleCasePipe, ConfirmationModalService, EnrollNewMemberService,
        BreadcrumbsService, TransitionGuardService, UserConfirmationDialogService, RoomsService,
        { provide: SessionStorageService, useClass: MockSessionStorageService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: MenuService, useClass: MockMenuService },
        { provide: GuestListService, useClass: MockGuestListService },
        { provide: CheckInService, useClass: MockCheckInService },
        { provide: BreadcrumbsService, useClass: MockBreadcrumbsService },
        { provide: ClipboardService, useClass: MockClipboardService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInComponent);
    component = fixture.componentInstance;
    checkInService = TestBed.get(CheckInService);
    roomServ = TestBed.get(RoomsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Verify GRS & PMS out-of-sync message style when last update ts in CDS <= 60 sec in CheckIn page under Arrival tab.', () => {
    const clonedObject = cloneDeep(guestInfoData);
    const date = new Date();
    date.setSeconds(date.getSeconds() - 40);
    clonedObject.reservationData.cdsResUpdtdTs.time = date;
    clonedObject.reservationData.pmsGRSCompareFlag = 'N';
    spyOn(checkInService, 'getCheckInReservationData').and.returnValue(Observable.of(clonedObject));
    component.getGuestCheckInData();
    fixture.detectChanges();
    expect(component.alertType).toEqual(AlertType.Info);
  });

  it('Verify GRS & PMS out-of-sync message style when last update ts in CDS > 60 sec in CheckIn page under Arrival tab.', () => {
    const clonedObject = cloneDeep(guestInfoData);
    const date = new Date();
    date.setMinutes(date.getMinutes() - 10);
    clonedObject.reservationData.cdsResUpdtdTs.time = date;
    clonedObject.reservationData.pmsGRSCompareFlag = 'N';
    spyOn(checkInService, 'getCheckInReservationData').and.returnValue(Observable.of(clonedObject));
    component.getGuestCheckInData();
    fixture.detectChanges();
    expect(component.isPMSNotInSync).toBeTruthy();
  });

  it('Verify when GRS & PMS is sync and Back to CheckIn success', () => {
    const clonedObject = cloneDeep(guestInfoData);
    clonedObject.reservationData.pmsGRSCompareFlag = 'Y';
    spyOn(checkInService, 'getCheckInReservationData').and.returnValue(Observable.of(clonedObject));
    component.getGuestCheckInData();
    fixture.detectChanges();
    expect(component.guestData).not.toBe(null);
  });

  it('verify "subscribeToDnmSaveError" method and check if observable is set to show error toast on dnm save fail', () => {
    const resetErrorSpy = spyOn(Object.getPrototypeOf(Object.getPrototypeOf(component)), 'processHttpErrors');
    const getDNMAddReasonErrorContentSpy = spyOn(roomServ, 'getDNMAddReasonErrorContent');
    roomServ.emitDnmErrorCheckInPage();
    fixture.detectChanges();
    expect(getDNMAddReasonErrorContentSpy).toHaveBeenCalled();
    expect(Object.getPrototypeOf(component).processHttpErrors).toHaveBeenCalledWith(null, component.dnmReasonChangeErrorCheckInPage);
  });

  it('verify "subsscribeToDnmAdded" method and check if observable to hide is error toast is unset on dnm save success', () => {
    const resetErrorSpy = spyOn(Object.getPrototypeOf(Object.getPrototypeOf(component)), 'resetSpecificError');
    roomServ.emitDnmAddedCheckInPage();
    fixture.detectChanges();
    expect(Object.getPrototypeOf(component).resetSpecificError).toHaveBeenCalled();
  });

  it('check if "reportIssueAutoFillDnmReason" subject value is attaching reservation number', () => {
    const guestListService = TestBed.get(GuestListService);
    const clonedObject = cloneDeep(guestInfoData);
    guestListService.userDataSource.next(clonedObject.reservationData);
    component['subscribeToGuestData']();
    fixture.detectChanges();
    const mockReportIssueSubject = `${ROOM_REPORT_ISSUE_AUTOFILL.dnmReasonCheckInError.subject} [Res #: ${clonedObject.reservationData.reservationNumber}]`;
    expect(component.reportIssueAutoFillDnmReason.subject).toEqual(mockReportIssueSubject);
  });

  it('should hide Hotel Features card for Reservation Tab ', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[0].id;
    fixture.detectChanges();
    const featuresElement = fixture.debugElement.query(By.css('.check-in__hotel-features')).nativeElement;
    expect(featuresElement.hidden).toEqual(true);
  });

  it('should hide Hotel Features card for Room Assignment Tab ', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[1].id;
    fixture.detectChanges();
    const featuresElement = fixture.debugElement.query(By.css('.check-in__hotel-features')).nativeElement;
    expect(featuresElement.hidden).toEqual(true);
  });

  it('should hide Hotel Features card for Payment Tab ', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[2].id;
    fixture.detectChanges();
    const featuresElement = fixture.debugElement.query(By.css('.check-in__hotel-features')).nativeElement;
    expect(featuresElement.hidden).toEqual(true);
  });

  it('should check Hotel Features card is visible on Keys tab', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[3].id;
    fixture.detectChanges();
    const featuresComponent = fixture.debugElement.query(By.css('.check-in__hotel-features')).nativeElement;
    expect(featuresComponent.hidden).toEqual(false);
  });

  it('should check if Comments section is present on the Reservation Tab', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[0].id;
    fixture.detectChanges();
    const commentEl = fixture.debugElement.query(By.css('.check-in__comments'));
    fixture.detectChanges();
    expect(commentEl).toBeDefined();
  });

  it('should check if Comments section is present on the RoomAssignment Tab', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[1].id;
    fixture.detectChanges();
    const commentEl = fixture.debugElement.query(By.css('.check-in__comments'));
    fixture.detectChanges();
    expect(commentEl).toBeDefined();
  });

  it('should check if Comments section is present on the Payment Tab', () => {
    component.initialized = true;
    component.selectedTab = component.workflowTabs[2].id;
    fixture.detectChanges();
    const commentEl = fixture.debugElement.query(By.css('.check-in__comments'));
    fixture.detectChanges();
    expect(commentEl).toBeDefined();
  });

});
