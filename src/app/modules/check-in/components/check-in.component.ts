import { Component, OnDestroy, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ClipboardService } from 'ngx-clipboard';

import { BreadcrumbsService, Alert, AlertType, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { SingleModeEnrollmentModel, EnrollNewMemberService } from 'ihg-ng-common-pages';

import { GuestListService } from '../../guests/services/guest-list.service';
import { GuestCheckInModel, ReservationDataModel, LoyaltyInfoModel, CDSResUpdtdTS } from '@modules/check-in/models/check-in.models';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { GuestResponse } from '@modules/guests/models/guest-list.models';
import { CHECK_IN_REPORT_ISSUE_AUTOFILL } from '@modules/check-in/constants/check-in-error-constants';
import { GA, PREP_STATUSES } from '@modules/check-in/constants/check-in-constants';
import { ViewportModel } from '@app-shared/directives/viewport-check/models/viewport.model';
import { PaymentType } from '@modules/guests/enums/guest-arrivals.enums';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import {
  PMS_NOT_IN_SYNC_ERROR_MSG,
  PMS_NOT_IN_SYNC_UPDATE
} from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/constants/check-in-payment-constants';
import { AUTH_ERR_REPORT_ISSUE_NOTSYNC, ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { CheckinAnalyticsConstants } from '../constants/check-in-constants';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';

import { TruncatedTooltipService } from '@app/modules/shared/components/truncated-tooltip/truncated-tooltip.service';
import { UtilityService } from '@app/services/utility/utility.service';
import { RoomsService } from '@app/modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { CHECK_IN_TABS } from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-constants';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.scss']
})
export class CheckInComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public initialized = false;
  public guestData: ReservationDataModel;
  public payIntegratedFlag: string;
  public pmsAuthSaveInd: string;
  public reportIssueAutoFill = CHECK_IN_REPORT_ISSUE_AUTOFILL;
  public dockReservation = false;
  public dockTalkAbout = false;
  public pmsNotInSyncError = new Subject<EmitErrorModel>();
  public pmsNotInSyncErrorContent = PMS_NOT_IN_SYNC_ERROR_MSG;
  public pmsNotInSyncErrorAutoFill = AUTH_ERR_REPORT_ISSUE_NOTSYNC;

  private checkIn$: Subscription = new Subscription();
  public alertType;
  public alertButtonType;
  public isPMSNotInSync: boolean;
  dataErrorToast: any;
  hasErrorBarDisplay: boolean;
  hasSuccessDisplay: boolean;
  successDataToast: any;
  copyMembershipId: any;
  selectedTab: string;
  workflowTabs = CHECK_IN_TABS;
  public reportIssueAutoFillDnmReason = Object.assign({}, ROOM_REPORT_ISSUE_AUTOFILL.dnmReasonCheckInError);
  public addReasonErrorContent: Partial<ErrorToastMessageTranslations>;
  public dnmReasonChangeErrorCheckInPage = new Subject<EmitErrorModel>();

  @ViewChild('talkAbout') talkAbout: ElementRef;
  tracking: any;

  constructor(
    private guestListService: GuestListService,
    private checkInService: CheckInService,
    private breadcrumbs: BreadcrumbsService,
    private translate: TranslateService,
    private enrollmentService: EnrollNewMemberService,
    private gaService: GoogleAnalyticsService,
    private clipboardService: ClipboardService,
    private router: Router,
    private truncatedTooltipService: TruncatedTooltipService,
    private utilityService: UtilityService,
    private roomsService: RoomsService,
    private appCommonService: AppCommonService) {
    super();
    this.enrollmentService.hideSuccessToast();
    this.enrollmentService.successToast.subscribe(successToast => {
      this.handleSuccessToast(successToast);
    });
    this.enrollmentService.toastErrorResponse.subscribe(error => {
      this.hasErrorBarDisplay = true;
      if (!this.dataErrorToast) {
        this.dataErrorToast = error;
      }
    });
  }

  ngOnInit() {
    super.init();
    this.setBreadCrumbLabel();
    this.subscribeToGuestData();
    this.subscribeToDnmSaveError();
    this.subsscribeToDnmAdded();
    this.subscribeTabChange();
  }

  subscribeToDnmSaveError() {
    this.checkIn$.add(
      this.roomsService.listenDnmErrorCheckInPage().subscribe(() => {
        this.handleReasonAddedError();
      })
    );
  }

  subsscribeToDnmAdded() {
    this.checkIn$.add(
      this.roomsService.listenDnmAddedCheckInPage().subscribe(() => {
        super.resetSpecificError(this.dnmReasonChangeErrorCheckInPage);
      })
    );
  }

  private subscribeTabChange() {
    this.checkIn$.add(this.checkInService.getWorkflowTabEvent().subscribe((tabId: string) => {
      this.selectedTab = tabId;
    }));
  }

  handleSuccessToast(success) {
    this.hasSuccessDisplay = true;
    if (success && success.show && success.membershipId) {
      this.successDataToast = { ...success };
      this.copyMembershipId = success.membershipId;
      this.getLoyaltyInformation(success.membershipId);
    }
  }

  copyToClipBoard() {
    this.gaService.trackEvent(GA.ec, GA.ea, GA.copyToClipboard);
    this.clipboardService.copyFromContent(this.copyMembershipId);
  }

  private subscribeToEnrollmentSuccess(): void {
    this.checkIn$.add(this.checkInService.enrollmentSuccess.subscribe((enrolledGuest: SingleModeEnrollmentModel) => {
      this.checkInService.getLoyaltyInfo(enrolledGuest.membershipId).subscribe((loyaltyInfo: LoyaltyInfoModel) => {
        this.updateGuestData(loyaltyInfo);
        this.updateMembershipId(enrolledGuest.membershipId);
      }, () => this.updateMembershipId(enrolledGuest.membershipId));
    }));
  }

  private getLoyaltyInformation(membershipId: string) {
    this.checkInService.getLoyaltyInfo(membershipId).subscribe((loyaltyInfo: LoyaltyInfoModel) => {
      this.updateGuestData(loyaltyInfo);
      this.updateMembershipId(membershipId);
    }, () => this.updateMembershipId(membershipId));
  }

  private updateGuestData(loyaltyInfo: LoyaltyInfoModel): void {
    const info = loyaltyInfo.memberDetails;
    if (this.guestData) {
      this.guestData.ihgRcPoints = info.ihgRcPoints || '0';
      this.guestData.badges = info.badges && info.badges.length ? info.badges : ['club'];
      this.guestData.firstName = info.firstName;
      this.guestData.lastName = info.lastName;
      this.guestData.prefixName = info.prefixName;
      this.truncatedTooltipService.updateContent(this.utilityService.concatNameWithPrefix(this.guestData.prefixName, this.guestData.firstName, this.guestData.lastName));
    }
  }

  private updateMembershipId(membershipId): void {
    if (this.guestData) {
      this.guestData.ihgRcNumber = membershipId;
    }
  }

  private setBreadCrumbLabel(): void {
    this.breadcrumbs.setLabel('guest-list-details', 'LBL_GST_LST_ARR');
  }

  private subscribeToGuestData(): void {
    this.checkIn$.add(this.guestListService.userDataSource$.subscribe((data: GuestResponse) => {
      if (data) {
        this.reportIssueAutoFillDnmReason.subject = `${ROOM_REPORT_ISSUE_AUTOFILL.dnmReasonCheckInError.subject} [Res #: ${data.reservationNumber}]`;
        this.checkInService.updateReservationNumber(data.reservationNumber);
        this.getGuestCheckInData();
      }
    }));
  }

  public getGuestCheckInData(): void {
    const resNo = this.checkInService.getReservationNumber();
    this.appCommonService.setReservationNumber(resNo);
    this.checkIn$.add(this.checkInService.getCheckInReservationData(resNo)
      .subscribe((response: GuestCheckInModel) => {
        this.processGuestChheckInResponse(response);
      }, (error) => {
        super.resetSpecificError(this.pmsNotInSyncError);
        this.processHttpErrors(error);
      }));
  }

  private processGuestChheckInResponse(response: GuestCheckInModel) {
    this.stopGuestCheckInCall();
    if ((!super.checkIfAnyApiErrors(response)) && (response.reservationData.pmsGRSCompareFlag === 'Y')) {
      super.resetSpecificError(this.pmsNotInSyncError);
      this.handleGuestDataResponse(response);
    } else if (response.reservationData.pmsGRSCompareFlag === 'N') {
      this.processCDSUpdateError(response.reservationData.cdsResUpdtdTs);
    }
  }

  private processCDSUpdateError(cdsResUpdtdTS: CDSResUpdtdTS) {
    if (this.checkCDSUpdatedSeconds(cdsResUpdtdTS)) {
      super.processHttpErrors(null, this.pmsNotInSyncError);
      this.pmsNotInSyncErrorContent = PMS_NOT_IN_SYNC_UPDATE;
      this.alertType = AlertType.Info;
      this.alertButtonType = AlertType.Info;
      this.setGuestCheckInCall();
      this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.ACTION_CHECKING_TIME_LAG_MSG, CheckinAnalyticsConstants.ACTION_CHECKING_TIME_LAG_LBL);
    } else {
      this.processError();
    }
  }

  processError() {
    super.processHttpErrors(null, this.pmsNotInSyncError);
    this.pmsNotInSyncErrorContent = PMS_NOT_IN_SYNC_ERROR_MSG;
    this.alertType = AlertType.Danger;
    this.alertButtonType = AlertType.Danger;
    this.isPMSNotInSync = true;
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.ACTION_CHECKING_TIME_ERROR_MSG, CheckinAnalyticsConstants.ACTION_CHECKING_TIME_ERROR_LBL);
  }

  setGuestCheckInCall() {
    this.tracking = setInterval(() => {
      this.getGuestCheckInData();
    }, 60000);
  }

  stopGuestCheckInCall() {
    clearInterval(this.tracking);
    this.tracking = null;
  }

  private checkCDSUpdatedSeconds(cdsResUpdtdTS: CDSResUpdtdTS): boolean {
    if (cdsResUpdtdTS) {
      const lastUpdatedDate = new Date(cdsResUpdtdTS.time).toLocaleString('en-US', { timeZone: cdsResUpdtdTS.timzone });
      const currentDate = new Date().toLocaleString('en-US', { timeZone: cdsResUpdtdTS.timzone });
      const seconds = moment(currentDate).diff(moment(lastUpdatedDate), 'seconds');
      if (seconds < parseInt(cdsResUpdtdTS.seconds)) {
        return true;
      }
    }
    return false;
  }

  private handleGuestDataResponse(response: GuestCheckInModel): void {
    this.guestData = response.reservationData;
    this.payIntegratedFlag = response.payIntegratedFlag;
    this.pmsAuthSaveInd = (response.reservationData.pmsAuthSaveInd == null || response.reservationData.pmsAuthSaveInd === '') ? 'N' : response.reservationData.pmsAuthSaveInd;
    this.resetAnyErrors();
    this.checkIneligibleReservations();
    this.initialized = true;
  }

  private checkIneligibleReservations(): void {
    if (this.guestData.prepStatus !== PREP_STATUSES.checkedIn) {
      if (this.isNotCardPaymentType()) {
        this.checkIn$.add(this.checkInService.openCheckInWarning('LBL_WRN_PAY_METH_NO_SPRT').subscribe());
        this.guestData.ineligibleReservation = true;
      } else if (this.guestData.numberOfRooms > 1) {
        this.checkIn$.add(this.checkInService.openCheckInWarning('LBL_WRN_RSVN_SPLT_PMS').subscribe());
        this.guestData.ineligibleReservation = true;
      }
    }
  }

  private isNotCardPaymentType(): boolean {
    return this.guestData.paymentType !== PaymentType.creditCard;
  }

  public refresh(): void {
    this.getGuestCheckInData();
  }

  public handleViewportCheckForRoomRate(viewportInfo: ViewportModel): void {
    this.dockReservation = this.shouldDockReservation(viewportInfo);
  }

  private shouldDockReservation(viewportInfo: ViewportModel): boolean {
    const headerHeight = (document.querySelector('.navbar.header') as HTMLElement).offsetHeight;
    const dockedPanelHeight = (document.querySelector('.check-in__reservation-docked') as HTMLElement).offsetHeight;
    return !viewportInfo.inViewport ||
      (viewportInfo.inViewport && viewportInfo.elementBoundary.bottom <= headerHeight + dockedPanelHeight);
  }

  public handleViewportCheckForTalkAbout(viewportInfo: ViewportModel): void {
    this.dockTalkAbout = !viewportInfo.inViewport;
  }

  public bringTalkAboutInViewPort(): void {
    this.talkAbout.nativeElement.scrollIntoView();
  }

  public backToGuestList(): void {
    this.router.navigate(['/guest-list-details']);
  }

  public retrySaveReason(){
    this.roomsService.emitDnmErrorRetryCheckInPage();
  }

  public handleReasonAddedError(): void {
    this.addReasonErrorContent = this.roomsService.getDNMAddReasonErrorContent();
    super.processHttpErrors(null, this.dnmReasonChangeErrorCheckInPage);
  }

  ngOnDestroy() {
    super.resetSpecificError(this.dnmReasonChangeErrorCheckInPage);
    this.checkIn$.unsubscribe();
  }
}
