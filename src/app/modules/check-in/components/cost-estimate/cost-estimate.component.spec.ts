import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { CommonTestModule } from '@app/modules/common-test/common-test.module';
import { AppSharedModule } from '@app/modules/shared/app-shared.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { setTestEnvironment } from '@app/modules/common-test/functions/common-test.function';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { CostEstimateComponent } from './cost-estimate.component';
import { CheckInService } from '@app/modules/check-in/services/check-in.service';
import { GridModule } from '@progress/kendo-angular-grid';
import { of, Observable } from 'rxjs';
import { By } from '@angular/platform-browser';
import { COST_ESTIMATE_AUTOFILL, COST_ESTIMATE } from '@app/constants/error-autofill-constants';
import { MOCK_COSTESTIMATE_RESPONSE, MOCK_PAGECHANGE_EVENT } from '@app/modules/check-in/mocks/cost-estimate-mock';

describe('CostEstimateComponent', () => {
  let component: CostEstimateComponent;
  let fixture: ComponentFixture<CostEstimateComponent>;
  let checkInService: CheckInService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(ReportIssueService, {
        useValue: {
          openAutofilledReportIssueModal: (value: object): void => {
          }
        }
      })
      .overrideProvider(CheckInService, {
        useValue: {
          costEstimate(resrvNo: string): Observable<any> {
            return of(MOCK_COSTESTIMATE_RESPONSE);
          }
        }
      })
      .configureTestingModule({
        imports: [
          IhgNgCommonKendoModule,
          GridModule,
          CommonTestModule, 
          AppSharedModule
        ],
        declarations: [ CostEstimateComponent ],
        providers: [
          NgbActiveModal,
          ReportIssueService,
          CheckInService
        ]
      })
      .compileComponents().then(() => {
      setTestEnvironment();
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostEstimateComponent);
    component = fixture.componentInstance;
    checkInService = TestBed.get(CheckInService);
    component.reservationNumber = '413456';
    component.reportIssueTopicVal = 'Manage Stay';
    fixture.detectChanges();
  });

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('verify report issue configuration is being set properly', () => {
    const reportIssueSubject = COST_ESTIMATE_AUTOFILL(component.reservationNumber, component.reportIssueTopicVal);
    expect(component.reportIssueAutoFill.genericError.subject).toEqual(reportIssueSubject.genericError.subject);
    expect(component.reportIssueAutoFill.genericError.function).toEqual(COST_ESTIMATE(component.reportIssueTopicVal).function);
    expect(component.reportIssueAutoFill.genericError.topic).toEqual(COST_ESTIMATE(component.reportIssueTopicVal).topic);
    expect(component.reportIssueAutoFill.genericError.subtopic).toEqual(COST_ESTIMATE(component.reportIssueTopicVal).subtopic);
    expect(component.reportIssueAutoFill.timeoutError.subject).toEqual(reportIssueSubject.timeoutError.subject);
    expect(component.reportIssueAutoFill.timeoutError.function).toEqual(COST_ESTIMATE(component.reportIssueTopicVal).function);
    expect(component.reportIssueAutoFill.timeoutError.topic).toEqual(COST_ESTIMATE(component.reportIssueTopicVal).topic);
    expect(component.reportIssueAutoFill.timeoutError.subtopic).toEqual(COST_ESTIMATE(component.reportIssueTopicVal).subtopic);
  });

  it('verify "loadGridData" method is called on getting successful response', () => {
    const loadGridDataSpy = spyOn(component, 'loadGridData');
    component.getResponse();
    fixture.detectChanges();
    expect(loadGridDataSpy).toHaveBeenCalled();
    expect(component.gridView.total).toEqual(MOCK_COSTESTIMATE_RESPONSE.costEstimateData.length);
  });

  it('verify "getResponse" method is called when "refresh" method is executed', () => {
    const getResponseSpy = spyOn(component, 'getResponse');
    component.refresh();
    fixture.detectChanges();
    expect(getResponseSpy).toHaveBeenCalled();
  });

  it('check if current modal closes when "onReportAnIssue" method is executed', () => {
    const dismissSpy = spyOn(component['activeModal'], 'dismiss');
    component.onReportAnIssue();
    fixture.detectChanges();
    expect(dismissSpy).toHaveBeenCalled();
  });

  it('check if "loadGridData" method is called when "onPageChange" method is executed', () => {
    const loadGridDataSpy = spyOn(component, 'loadGridData');    
    component.onPageChange(MOCK_PAGECHANGE_EVENT);
    fixture.detectChanges();
    expect(component.gridOptions.skip).toEqual(MOCK_PAGECHANGE_EVENT.skip);
    expect(loadGridDataSpy).toHaveBeenCalled();
  })
});
