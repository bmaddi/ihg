import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';

import { GridSettings } from 'ihg-ng-common-kendo';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { CostEstimateResponse } from '@modules/check-in/models/check-in.models';
import { AppConstants } from '@app/constants/app-constants';
import { CHECK_IN_REPORT_ISSUE_AUTOFILL } from '@modules/check-in/constants/check-in-error-constants';
import { GridComponent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { COST_ESTIMATE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { Subject } from 'rxjs';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';

@Component({
  selector: 'app-cost-estimate',
  templateUrl: './cost-estimate.component.html',
  styleUrls: ['./cost-estimate.component.scss']
})
export class CostEstimateComponent extends AppErrorBaseComponent implements OnInit {

  @ViewChild(GridComponent) grid: GridComponent;
  public initialized = false;
  public footerVisible = true;
  public reportIssueAutoFill: ReportIssueAutoFillObject;
  dateFormat =  AppConstants.VW_DT_FORMAT;
  totalEstimateAmount: string;
  currencyCode: string;
  reservationNumber: string;
  reportIssueTopicVal = 'Check In';
  noOfNights: number = 0;
  gridView: GridDataResult;
  gridOptions = {
    pageSize: 10,
    toggleFilter: false,
    skip: 0,
    pageable: true,
    sortable: false
  };
  gridSettings: GridSettings = null;
  gridPagerTranslations = {
    gridPagerInfo: {
      itemsText: 'LBL_GST_NIGHTS'
    }
  };
  gridData = [];
  public costEstimateError = new Subject<EmitErrorModel>();

  constructor(private activeModal: NgbActiveModal, 
    private checkInService: CheckInService) {
      super();
     }

  ngOnInit() {
    this.setReportIssueConfig();
    this.getResponse();
  }

  setReportIssueConfig() {
    this.reportIssueAutoFill = COST_ESTIMATE_AUTOFILL(this.reservationNumber, this.reportIssueTopicVal);
  }

  getResponse() {
    this.checkInService.costEstimate(this.reservationNumber).subscribe((response: CostEstimateResponse) => {
      if (!super.checkIfAnyApiErrors(response, null, this.costEstimateError)) {
        this.handleSuccesfullResponse(response)
      } else {
        super.resetSpecificError(this.emitError);
      }       
    }, (error) => {
      this.footerVisible = false;
      this.processHttpErrors(error, this.costEstimateError);
    });
  }

  handleSuccesfullResponse(response: CostEstimateResponse) {
    this.initialized = true;
      this.footerVisible = true;
      this.gridData = response.costEstimateData;
      if(this.gridData.length !== 0){
        let lastElement = response.costEstimateData[this.gridData.length-1];
        this.totalEstimateAmount = lastElement.estimatedTotal;
        this.noOfNights = this.gridData.length;
      }
      this.loadGridData();
  }

  loadGridData() {
    this.gridView = {
      data: this.gridData.slice(this.gridOptions.skip, this.gridOptions.skip + this.gridOptions.pageSize),
      total: this.gridData.length
    };
  }

  onPageChange(event: PageChangeEvent) {
    this.gridOptions.skip = event.skip;
    this.loadGridData();
  }

  formatDate(date: string) {
    return moment(date).format(this.dateFormat).toUpperCase();
  }

  onClose() {
    this.activeModal.close();
  }

  refresh() {
    this.getResponse();
  }

  onReportAnIssue() {
    this.activeModal.dismiss();
  }

}
