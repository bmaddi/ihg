import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { DiningFeaturesComponent } from './dining-features.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';

describe('DiningFeaturesComponent', () => {
  let component: DiningFeaturesComponent;
  let fixture: ComponentFixture<DiningFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CommonTestModule],
      declarations: [DiningFeaturesComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiningFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
