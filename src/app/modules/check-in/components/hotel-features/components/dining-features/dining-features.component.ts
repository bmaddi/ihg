import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';

import { DiningDataModel } from '@modules/check-in/models/hotel-features.models';
import { HotelFeaturesService } from '@modules/check-in/components/hotel-features/services/hotel-features.service';
import {
  DINING_FEATURES_MAP,
  FEATURE_ERROR_CONTENT
} from '@modules/check-in/components/hotel-features/constants/hotel-features-constants';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';

@Component({
  selector: 'app-dining-features',
  templateUrl: './dining-features.component.html',
  styleUrls: ['./dining-features.component.scss']
})
export class DiningFeaturesComponent extends AppErrorBaseComponent implements OnChanges, OnDestroy {
  private features$: Subscription = new Subscription();
  public objectKeys = Object.keys;
  public diningData: DiningDataModel;
  public keyMap = DINING_FEATURES_MAP;
  public errorConfig = FEATURE_ERROR_CONTENT;

  @Input() isExpanded: boolean;

  constructor(private hotelFeatures: HotelFeaturesService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isExpanded && changes.isExpanded.currentValue && !this.diningData) {
      this.getDiningHotelFeatures();
    }
  }

  private getDiningHotelFeatures(): void {
    if (!this.diningData) {
      this.features$.add(this.hotelFeatures.getDiningFeatures().subscribe((response: DiningDataModel) => {
        if (!super.checkIfAnyApiErrors(response)) {
          this.diningData = response;
        }
      }, error => {
        this.processHttpErrors(error);
      }));
    }
  }

  public newlineToComma(str) {
    return str.trim().split('\n').join(', ');
  }

  ngOnDestroy() {
    this.features$.unsubscribe();
  }
}
