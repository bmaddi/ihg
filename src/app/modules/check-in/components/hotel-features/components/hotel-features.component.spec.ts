import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { EnvironmentService } from 'ihg-ng-common-core';

import { HotelFeaturesComponent } from './hotel-features.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { mockEnvs } from '@app/constants/app-test-constants';

describe('HotelFeaturesComponent', () => {
  let component: HotelFeaturesComponent;
  let fixture: ComponentFixture<HotelFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [HotelFeaturesComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [EnvironmentService]
    })
      .compileComponents();
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
