import { Component, OnInit } from '@angular/core';

import { AnalyticsTrackingService } from '@modules/check-in/services/analytics-tracking/analytics-tracking.service';

@Component({
  selector: 'app-hotel-features',
  templateUrl: './hotel-features.component.html',
  styleUrls: ['./hotel-features.component.scss']
})
export class HotelFeaturesComponent implements OnInit {
  public isDiningExpanded = false;
  public isWellnessExpanded = false;

  constructor(private trackingService: AnalyticsTrackingService) {
  }

  ngOnInit() {
  }

  public handleDiningClick() {
    this.trackingService.trackClickToggleDining();
    this.isDiningExpanded = !this.isDiningExpanded;
    this.isWellnessExpanded = false;
  }

  public handleWellnessClick() {
    this.trackingService.trackClickToggleWellness();
    this.isWellnessExpanded = !this.isWellnessExpanded;
    this.isDiningExpanded = false;
  }

}
