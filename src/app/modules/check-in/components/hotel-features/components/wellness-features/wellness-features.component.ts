import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';

import { WellnessDataModel } from '@modules/check-in/models/hotel-features.models';
import { HotelFeaturesService } from '@modules/check-in/components/hotel-features/services/hotel-features.service';

import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import {
  FEATURE_ERROR_CONTENT,
  WELLNESS_FEATURES_MAP
} from '@modules/check-in/components/hotel-features/constants/hotel-features-constants';


@Component({
  selector: 'app-wellness-features',
  templateUrl: './wellness-features.component.html',
  styleUrls: ['./wellness-features.component.scss']
})
export class WellnessFeaturesComponent extends AppErrorBaseComponent implements OnChanges, OnDestroy {
  private features$: Subscription = new Subscription();
  public objectKeys = Object.keys;
  public wellnessData: WellnessDataModel;
  public keyMap = WELLNESS_FEATURES_MAP;
  public errorConfig = FEATURE_ERROR_CONTENT;

  @Input() isExpanded: boolean;

  constructor(private hotelFeatures: HotelFeaturesService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isExpanded && changes.isExpanded.currentValue && !this.wellnessData) {
      this.getWellnessHotelFeatures();
    }
  }

  private getWellnessHotelFeatures(): void {
    if (!this.wellnessData) {
      this.features$.add(this.hotelFeatures.getWellnessFeatures().subscribe((response: WellnessDataModel) => {
        if (!super.checkIfAnyApiErrors(response)) {
          this.wellnessData = response;
        }
      }, error => {
        this.processHttpErrors(error);
      }));
    }
  }

  ngOnDestroy() {
    this.features$.unsubscribe();
  }

}
