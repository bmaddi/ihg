export const DINING_FEATURES_MAP = {
  onSiteDining: 'LBL_ON_ST_DNG',
  breakfast: 'LBL_BRKFST',
  eveningReception: 'LBL_EVE_RCPTN',
  inRoomDining: 'LBL_IN_RM_DNG',
  bars: 'LBL_BARS',
  restaurants: 'LBL_RSTURNT'
};

export const WELLNESS_FEATURES_MAP = {
  onSiteFitnessCenter: 'LBL_ONST_FTNS_CNTR',
  pool: 'LBL_POOL',
  indoorPool: 'LBL_INDR_POOL',
  outdoorPool: 'LBL_OUTDR_POOL',
  amenities: 'LBL_AMENTS'
};

export const FEATURE_ERROR_CONTENT = {
  timeout: {
    message: 'LBL_ERR_DATA_LOAD',
    icon: '',
    showRefreshBtn: false
  },
  general: {
    message: 'LBL_ERR_DATA_LOAD',
    icon: '',
    showRefreshBtn: false
  },
  noData: {
    header: 'LBL_ERR_NODATAAVAILABLE',
    message: 'LBL_UPD_HTL_CONT'
  }
};

export const FEATURE_NO_DATA_CONTENT = {
  timeout: { hideError: true },
  general: { hideError: true },
  noData: {
    header: 'LBL_ERR_NODATAAVAILABLE',
    message: 'LBL_UPD_HTL_CONT'
  }
};

