import { HotelFeaturesModule } from './hotel-features.module';

describe('HotelFeaturesModule', () => {
  let hotelFeaturesModule: HotelFeaturesModule;

  beforeEach(() => {
    hotelFeaturesModule = new HotelFeaturesModule();
  });

  it('should create an instance', () => {
    expect(hotelFeaturesModule).toBeTruthy();
  });
});
