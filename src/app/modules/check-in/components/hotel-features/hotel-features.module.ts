import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule } from '@ngx-translate/core';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { AppSharedModule } from '@app-shared/app-shared.module';
import { HotelFeaturesComponent } from '@modules/check-in/components/hotel-features/components/hotel-features.component';
import { DiningFeaturesComponent } from './components/dining-features/dining-features.component';
import { WellnessFeaturesComponent } from './components/wellness-features/wellness-features.component';

@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    TranslateModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    AppSharedModule
  ],
  declarations: [
    HotelFeaturesComponent,
    DiningFeaturesComponent,
    WellnessFeaturesComponent
  ],
  exports: [
    HotelFeaturesComponent
  ]
})
export class HotelFeaturesModule {
}
