import { TestBed } from '@angular/core/testing';

import { HotelFeaturesService } from './hotel-features.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonTestModule } from '@modules/common-test/common-test.module';

describe('HotelFeaturesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, CommonTestModule]
  }));

  it('should be created', () => {
    const service: HotelFeaturesService = TestBed.get(HotelFeaturesService);
    expect(service).toBeTruthy();
  });
});
