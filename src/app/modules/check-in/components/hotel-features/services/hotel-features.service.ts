import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { DiningDataModel, WellnessDataModel } from '@modules/check-in/models/hotel-features.models';

@Injectable({
  providedIn: 'root'
})
export class HotelFeaturesService {
  private readonly HIN_API = environment.hinAPI;

  constructor(private http: HttpClient, private userService: UserService) {
  }

  public getDiningFeatures(): Observable<DiningDataModel> {
    const apiURl = `${this.HIN_API}hcm/dining/${this.getLocationId()}`;
    return this.http.get(apiURl).pipe(map((response: DiningDataModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public getWellnessFeatures(): Observable<WellnessDataModel> {
    const apiURl = `${this.HIN_API}hcm/wellness/${this.getLocationId()}`;
    return this.http.get(apiURl).pipe(map((response: WellnessDataModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}
