import { Component, OnInit, Input } from '@angular/core';

import { CheckInService } from '@modules/check-in/services/check-in.service';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { AnalyticsTrackingService } from '@app/modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';

@Component({
  selector: 'app-reservation-summary',
  templateUrl: './reservation-summary.component.html',
  styleUrls: ['./reservation-summary.component.scss']
})
export class ReservationSummaryComponent implements OnInit {
  public guestName = '';

  @Input() guestData: ReservationDataModel;

  constructor(
    private guestListService: GuestListService,
    private checkInService: CheckInService,
    private trackingService: AnalyticsTrackingService) { }

  ngOnInit() {
    this.setGuestName();
  }

  private setGuestName(): void {
    this.guestName = this.checkInService.formatGuestName(this.guestData);
  }

  public setMemberProfile(): void {
    this.trackingService.trackClickProfileIcon('check-in');
    this.guestListService.setMemberProfile(+this.guestData.ihgRcNumber);
  }

}
