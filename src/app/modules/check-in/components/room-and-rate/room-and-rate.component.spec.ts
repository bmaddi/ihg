import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomAndRateComponent } from './room-and-rate.component';
import { By } from '@angular/platform-browser';
import { ReservationDataModel } from '../../models/check-in.models';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { TitleCasePipe } from '@angular/common';

import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GUEST_DATA } from '../../mocks/room-and-rate-mock';
import { ManageStayService } from '@app/modules/manage-stay/services/manage-stay.service';
import { of } from 'rxjs';
import { RATE_CODE_DETAILS } from '@app/modules/manage-stay/mocks/manage-stay-mock';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) {
  }
}

export class MockManageStayService {
  getRateCodeData(data) {
    return of(RATE_CODE_DETAILS);
  }
}

describe('RoomAndRateComponent', () => {
  let component: RoomAndRateComponent;
  let fixture: ComponentFixture<RoomAndRateComponent>;
  let guestData: ReservationDataModel;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ RoomAndRateComponent ],
      imports: [TranslateModule.forRoot(), NgbModule],
      providers: [TranslateService, { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService }, TitleCasePipe,
      {provide: ManageStayService, useClass: MockManageStayService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomAndRateComponent);
    component = fixture.componentInstance;
    component.guestData = GUEST_DATA;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show upgrade icon when reservation has room upgrade & room is assigned', () => {
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'))).toBeNull();
    component.guestData = {...component.guestData, freeUpgradeInd: true, assignedRoomNumber: '4543'};

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'))).not.toBeNull();
  });

  it('should display assigned room after assignment', () => {
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'))).toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="AssignedRoomNumber-SID"]'))).toBeNull();
    component.guestData = {
      ...component.guestData, freeUpgradeInd: true, assignedRoomNumber: '4543',
      assignedRoomStatus: {
        roomType: 'KEXG',
        roomNumber: '4554',
        roomStatus: null,
        houseKeepingStatus: null,
        frontOfficeStatus: null,
        features: [],
        reservationFeatures: []
      }
    };

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="AssignedRoomNumber-SID"]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomTypeCode-SID"]'))).not.toBeNull();
  });

  it('Check room categoery toopltip displayed on mouseenter', async(() => {
    component.ngOnInit();
    const roomcategoryInfo = fixture.debugElement.queryAll(By.css('.room-rate__details__item'));
    const event = new Event('mouseenter');
    const roomCat = roomcategoryInfo[1];
    roomCat.query(By.css('.info-dtl-roomcategory-infoicon')).nativeElement.dispatchEvent(event);
    fixture.detectChanges();
    const popover = fixture.debugElement.query(By.css('.popover'));
    expect(popover.nativeElement).not.toBe(null);
  }));

  it('Check room categoery toopltip displayed on mouseenter when reservation have assinged room number', async(() => {
    component.guestData.assignedRoomNumber = '4543';
    component.ngOnInit();
    fixture.detectChanges();
    const roomcategoryInfo = fixture.debugElement.queryAll(By.css('.room-rate__details__item'));
    const event = new Event('mouseenter');
    const roomCat = roomcategoryInfo[1];
    roomCat.query(By.css('.info-dtl-roomcategory-infoicon')).nativeElement.dispatchEvent(event);
    fixture.detectChanges();
    const popover = fixture.debugElement.query(By.css('.popover'));
    expect(popover.nativeElement).not.toBe(null);
  }));

  it('should hide upgrade icon in room rate based on the free room upgrade indicator', () => {
    component.guestData.assignedRoomNumber = '104';
    component.guestData.assignedRoomStatus = {
      roomNumber: '104', roomStatus: 'Clean', roomType: 'KDXG', houseKeepingStatus: 'Clean', frontOfficeStatus: 'Vacant',
      features: [], reservationFeatures: []
    };
    component.guestData.freeUpgradeInd = false;
    fixture.detectChanges();
    const upgradeIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'));
    expect(upgradeIcon).toBeNull();
  });

  it('should show tooltip on upgrade icon', () => {
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'))).toBeNull();
    component.guestData = {...component.guestData, freeUpgradeInd: true, assignedRoomNumber: '4543'};
    component.guestData.freeUpgradeInd = true;
    fixture.detectChanges();

    const upgradeIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradeIndicator-SID"]'));
    expect(upgradeIcon).not.toBeNull();

    const upgradeContent = fixture.debugElement.query(By.css('.category-tooltip'));
    expect(upgradeContent).not.toBeNull();

    upgradeContent.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();

    const tooltipElement = document.querySelector('ngb-tooltip-window');
    expect(tooltipElement).toBeDefined();
    expect(tooltipElement).not.toBeNull();
  });

  it('should check if pop up opens when clicked on rate code', () => {
    component.guestData = GUEST_DATA;
    spyOn(component, 'openDescriptionModal');
    component.getRateCodeDetails();
    expect(component.openDescriptionModal).toHaveBeenCalled();
  });


});
