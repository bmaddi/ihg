import { Component, Input, OnInit, HostListener, OnChanges, SimpleChanges } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ReservationDataModel } from '../../models/check-in.models';
import { UtilityService } from '@services/utility/utility.service';
import { CostEstimateComponent } from '../cost-estimate/cost-estimate.component';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { RateCategoryDataModel, RateCategoryModel } from '@app/modules/offers/models/offers.models';
import { RoomRateDescriptionModalComponent } from '@app/modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';
import { ManageStayService } from '@app/modules/manage-stay/services/manage-stay.service';

@Component({
  selector: 'app-room-and-rate',
  templateUrl: './room-and-rate.component.html',
  styleUrls: ['./room-and-rate.component.scss']
})
export class RoomAndRateComponent  extends AppErrorBaseComponent implements OnInit, OnChanges {

  public readonly roomDescCharLimit = 23;
  public readonly grpCharLimit = 10;
  public popupContentClass ;

  @Input() guestData: ReservationDataModel;
  isDesktop: boolean;
  isIpad: boolean;

  constructor(public utility: UtilityService,
    private modalService: NgbModal,
    private gaService: GoogleAnalyticsService,
    private titleCasePipe: TitleCasePipe,
    private manageStayService: ManageStayService) {
      super();
     }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.popupContentClass = "";
    if (window.outerWidth > 1024) {
      this.isDesktop = true;
    } else if(window.outerWidth <=834 && window.outerWidth >= 832){
      this.setPopupForIpad();
    }
    else{
      this.isDesktop = false;
      this.assignCSSClass();
    }
  }

  ngOnInit() {    
    this.assignCSSClass();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.assignCSSClass();
  }


  public assignCSSClass(){
    this.popupContentClass = '';    
    if(!this.guestData.assignedRoomNumber){
      this.popupContentClass ='popover-content';
    }else {
      this.guestData.assignedRoomNumber.length == 3 ?  this.popupContentClass ='popover-by-room-length':this.popupContentClass = '';
    } 

    if(window.outerWidth == 832){
      this.setPopupForIpad();
    }
  }

  private setPopupForIpad(){
    this.isIpad=true;
    if(this.guestData.assignedRoomNumber){
      this.popupContentClass ="popover-content-ipad";
    }
    else if(!this.guestData.assignedRoomNumber){
      this.popupContentClass ="popover-content-ipad-no-assigned";
    }
  }

  public getDisplayValue(value: string, charCount: number): string {
    return this.utility.truncateWithEllipsis(this.titleCasePipe.transform(value), charCount);
  }

  openCostEstimate() {
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN, CheckinAnalyticsConstants.GA_COST_ESTIMATE_ACTION,
      CheckinAnalyticsConstants.GA_COST_ESTIMATE_LABLE);
    const modal = this.modalService.open(CostEstimateComponent, {windowClass: 'cost-estimate'});
    const modalComponent = modal.componentInstance as CostEstimateComponent;
    modalComponent.currencyCode = this.guestData.ratesInfo.currencyCode;
    modalComponent.reservationNumber = this.guestData.reservationNumber;
  }

  public getRateCodeDetails() {
    const rateDetailsData = {
        'checkInDate': this.guestData.checkInDate,
        'checkOutDate': this.guestData.checkOutDate,
        'ihgRcNumber': this.guestData.ihgRcNumber,
        'roomTypeCode': this.guestData.roomTypeCode,
        'rateCategoryCode': this.guestData.rateCategoryCode
    };
    this.manageStayService.getRateCodeData(rateDetailsData).subscribe((response) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.openDescriptionModal(null, response);
      } else {
        this.openDescriptionModal(rateDetailsData, null, true);
      }
    }, (error) => {
      this.processHttpErrors(error);
      this.openDescriptionModal(rateDetailsData, null);
    });
  }

  public openDescriptionModal(serviceDetails: RateCategoryDataModel, data: RateCategoryModel, noData: boolean = false) {
    const modal = this.modalService.open(RoomRateDescriptionModalComponent,
      { windowClass: 'large-modal', backdrop: 'static', keyboard: false });
    if (data) {
      modal.componentInstance.rateName = data.name;
      modal.componentInstance.rateCode = data.code;
      modal.componentInstance.roomName = '';
      modal.componentInstance.roomCode = '';
      modal.componentInstance.roomDesc = '';
      modal.componentInstance.pointsEligible = data.pointsEligible;
      modal.componentInstance.rateInfo = data.rateInfo;
      modal.componentInstance.arrivalDate = '';
      modal.componentInstance.departureDate = '';
      modal.componentInstance.isOnlyRateInfo = true;
    } else {
      modal.componentInstance.noData = noData;
      modal.componentInstance.noRateDetailsErrorEvent = this.emitError;
      modal.componentInstance.rateCodeData = serviceDetails;
    }
    return modal;
  }
}
