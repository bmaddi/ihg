import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActOnContainerComponent } from './act-on-container.component';

xdescribe('ActOnContainerComponent', () => {
  let component: ActOnContainerComponent;
  let fixture: ComponentFixture<ActOnContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActOnContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActOnContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
