import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-act-on-container',
  templateUrl: './act-on-container.component.html',
  styleUrls: ['./act-on-container.component.scss']
})
export class ActOnContainerComponent implements OnInit {

  constructor() {
  }

  @Input() cardType: string; // ACT_ON_CARD_TYPES.success || ACT_ON_CARD_TYPES.warning

  ngOnInit() {
  }

}
