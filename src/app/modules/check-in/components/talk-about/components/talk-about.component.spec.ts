import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TitleCasePipe } from '@angular/common';
import { DebugElement } from '@angular/core';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

import { CardComponent, ConfirmationModalService, ErrorContainerComponent } from 'ihg-ng-common-components';
import { MenuService, UserService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { TalkAboutComponent } from './talk-about.component';
import { ServiceErrorComponent } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { SmallErrorCardComponent } from '@app-shared/components/small-error-card/small-error-card.component';
import { TalkAboutService } from '@modules/check-in/components/talk-about/services/talk-about.service';
import { GuestResponse } from '@modules/guests/models/guest-list.models';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { TalkAboutType } from '@modules/check-in/components/talk-about/models/talk-about.models';


export const parsedMockResponse = {
  'translationValues': {
    'birthDate': 'September 9th',
    'pointsToNextLevel': '154',
    'nextLoyaltyLevel': 'Gold'
  },
  'actions': [
    {
      'name': 'Birthday',
      'translationKey': 'LBL_TA_WISH_HAPPY_BDAY'
    },
    {
      'name': 'Book Direct',
      'translationKey': 'LBL_TA_BOOKING_DIRECT'
    },
    {
      'name': 'Reward Night',
      'translationKey': 'LBL_TA_THANK_REWARD_NIGHT'
    },
    {
      'name': 'Owners Association Rate',
      'translationKey': 'LBL_TA_WELCOME_IHG_OWNER_ACCOC'
    },
    {
      'name': 'Inner Circle Member',
      'translationKey': 'LBL_TA_WELCOME_INNER_CIRCLE'
    },
    {
      'name': 'New Member',
      'translationKey': 'LBL_TA_WELCOME_IHG_REW_CLUB'
    },
    {
      'name': 'New Ambassador Member',
      'translationKey': 'LBL_TA_THANK_AMBASSADOR_PRG'
    },
    {
      'name': 'New Royal Ambassador',
      'translationKey': 'LBL_TA_CONG_ROY_AMB'
    },
    {
      'name': 'New Inner Circle Member',
      'translationKey': 'LBL_TA_CONG_INNER_CIRCLE'
    },
    {
      'name': 'Ambassador',
      'translationKey': 'LBL_TA_WELCOME_AMBASSADOR'
    },
    {
      'name': 'New Status Spire',
      'translationKey': 'LBL_TA_CONG_SPIRE_STATUS'
    },
    {
      'name': 'New Status Platinum',
      'translationKey': 'LBL_TA_CONG_PLATINUM_STATUS'
    },
    {
      'name': 'New Status Gold',
      'translationKey': 'LBL_TA_CONG_GOLD_STATUS'
    },
    {
      'name': 'Close to Next Membership',
      'translationKey': 'LBL_TA_POINTS_TO_NEXT_LEVEL'
    },
    {
      'name': 'Welcome Back',
      'translationKey': 'LBL_TA_WELCOME_BACK'
    },
    {
      'name': 'Royal Ambassador',
      'translationKey': 'LBL_TA_WELCOME_AMBASSADOR'
    },
    {
      'name': 'Long Stay',
      'translationKey': 'LBL_TA_SUGGEST_ACTV_ATTRACT_LONG_STAY'
    },
    {
      'name': 'Ambassador Enrollment',
      'translationKey': 'LBL_TA_ASK_LEARN_AMBASSADOR_PRG'
    },
    {
      'name': 'IHG Employee',
      'translationKey': 'LBL_TA_WELCOME_IHG_EMP'
    }
  ]
};

export const mockGuestData = <GuestResponse>{
  'firstName': 'Saurav',
  'lastName': 'Agrawal',
  'groupName': 'Agrawal',
  'ihgRcNumber': '123456',
  'reservationNumber': '44002733',
  'badges': [
    'spire',
    'ambassador',
    'employee'
  ],
  'arrival': '',
  'arrivalTime': '',
  'nights': '1',
  'checkInDate': '2019-06-05',
  'deparatureDate': '2019-06-05',
  'ihgRcMembership': 'Spire Elite',
  'roomNumber': '204',
  'roomType': '',
  'paymentType': '',
  'roomCount': 1,
  'reservationStatus': 'INPROGRESS',
  'prepStatus': 'INPROGRESS',
  'loyaltyId': '',
};

xdescribe('TalkAboutComponent', () => {
  let component: TalkAboutComponent;
  let fixture: ComponentFixture<TalkAboutComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(ConfirmationModalService, {
        useValue: {}
      })
      .overrideProvider(ReportIssueService, {
        useValue: {}
      })
      .overrideProvider(MenuService, {
        useValue: {}
      })
      .configureTestingModule({
        imports: [
          TranslateModule.forChild(),
          HttpClientTestingModule
        ],
        declarations: [
          TalkAboutComponent,
          CardComponent,
          ServiceErrorComponent,
          ErrorContainerComponent,
          SmallErrorCardComponent],
        providers: [
          UserService,
          TitleCasePipe,
          ConfirmationModalService,
          ReportIssueService,
          TranslateStore,
          MenuService
        ]
      })

      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalkAboutComponent);
    component = fixture.componentInstance;
    component.guestData = mockGuestData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not show spinner on card when TalkAboutComponent is docked', () => {
    component.docked = true;
    const getTalkAboutDataSharedSpy = spyOn(TestBed.get(TalkAboutService), 'getTalkAboutDataShared')
      .and.returnValue(of(parsedMockResponse));

    component.ngOnInit();
    const element = fixture.debugElement.query(By.css('.service-error-container'));
    console.log(element.styles);
    fixture.detectChanges();
    expect(element.nativeElement.hasAttribute('hidden')).toEqual(true);
    expect(getTalkAboutDataSharedSpy).toHaveBeenCalled();
  });

  it('should show spinner on initial load and hide on finalize of Observable response', () => {
    const getTalkAboutDataSharedSpy = spyOn(TestBed.get(TalkAboutService), 'getTalkAboutDataShared')
      .and.returnValue(of(parsedMockResponse));

    component.ngOnInit();

    const element = fixture.debugElement.query(By.css('.uce-error-wrapper'));
    expect(element).not.toBeNull();

    fixture.detectChanges();
    expect(getTalkAboutDataSharedSpy).toHaveBeenCalled();


    fixture.whenRenderingDone().then(() => {
      expect(component.showSpinner).toBeFalsy();
      const spinnerElement = fixture.debugElement.query(By.css('.uce-error-wrapper'));
      expect(spinnerElement).toBeNull();
    });

  });


  it('should show spinner on Guest Enrollment Updates and hide on finalize of Observable response', () => {
    let spinnerElement: DebugElement;
    const getTalkAboutDataSharedSpy = spyOn(TestBed.get(TalkAboutService), 'getTalkAboutDataShared')
      .and.returnValue(of(parsedMockResponse));

    TestBed.get(CheckInService)['enrollmentSuccessSource'].next({
      membershipId: '1324123',
      firstName: '',
      lastName: '',
      type: '',
    });

    spinnerElement = fixture.debugElement.query(By.css('.uce-error-wrapper'));
    expect(spinnerElement).not.toBeNull();

    fixture.detectChanges();
    expect(getTalkAboutDataSharedSpy).toHaveBeenCalled();


    fixture.whenRenderingDone().then(() => {
      expect(component.showSpinner).toBeFalsy();
      spinnerElement = fixture.debugElement.query(By.css('.uce-error-wrapper'));
      expect(spinnerElement).toBeNull();
    });

  });

  it('should show spinner on Workflow Tab Event Change and hide on finalize of Observable response', () => {
    let spinnerEl: DebugElement;
    const getTalkAboutDataSharedSpy = spyOn(TestBed.get(TalkAboutService), 'getTalkAboutDataShared')
      .and.returnValue(of(parsedMockResponse));

    TestBed.get(CheckInService).emitWorkflowTabEvent('assignment');

    spinnerEl = fixture.debugElement.query(By.css('.uce-error-wrapper'));
    expect(spinnerEl).not.toBeNull();

    fixture.detectChanges();
    expect(getTalkAboutDataSharedSpy).toHaveBeenCalled();


    fixture.whenRenderingDone().then(() => {
      expect(component.showSpinner).toBeFalsy();
      expect(component.cardType).toEqual(TalkAboutType.Consider);
      spinnerEl = fixture.debugElement.query(By.css('.uce-error-wrapper'));
      expect(spinnerEl).toBeNull();
    });
  });
});
