import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { talkAboutTranslationMap, talkAboutDisplayConfig } from '../constants/talk-about-constants';
import { TalkAboutType, TalkAboutCardConfig, TalkAboutModel, TalkAboutAction } from '../models/talk-about.models';
import { TalkAboutService } from '@app/modules/check-in/components/talk-about/services/talk-about.service';
import { CheckInService } from '@app/modules/check-in/services/check-in.service';
import { CHECK_IN_TABS } from '@app/modules/check-in/components/check-in-workflow/constants/check-in-workflow-constants';
import { GuestResponse } from '@app/modules/guests/models/guest-list.models';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { ERROR_CARD_TYPE } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';

@Component({
  selector: 'app-talk-about',
  templateUrl: './talk-about.component.html',
  styleUrls: ['./talk-about.component.scss']
})
export class TalkAboutComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  @Input() guestData: GuestResponse;
  @Input() docked = false;
  @Output() dockedItemClick = new EventEmitter<void>();

  translations = talkAboutTranslationMap;
  cardProps: TalkAboutCardConfig;
  cardType: TalkAboutType;
  actions: TalkAboutAction[] = [];
  translationValues: object = {};
  maxEntries = 3;
  showSpinner = false;
  showAllEntries = false;
  errorConfig: ServiceErrorComponentConfig;
  errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  public initialized = false;
  public hasErrors = true;
  private subscriptions$: Subscription = new Subscription();
  private workflowTabs = CHECK_IN_TABS;
  private talkAboutDataCache: {[index: string]: TalkAboutModel} = {};

  constructor(
    private talkAboutService: TalkAboutService,
    private checkInService: CheckInService) {
      super();
    }

  ngOnInit() {
    this.cardType = TalkAboutType.TalkAbout;
    this.refreshTalkAboutData();
    this.subscribeTabChange();
    this.initErrorConfig();
    this.checkInService.enrollmentSuccess.subscribe((data: any) => {
      this.fetchTalkAboutData();
    });
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      timeout: { message: this.translations.error },
      general: { message: this.translations.error }
    };
  }

  private subscribeTabChange() {
    this.subscriptions$.add(this.checkInService.getWorkflowTabEvent().subscribe(newTabId => {
      switch (newTabId) {
        case this.workflowTabs[0].id:
          this.cardType = TalkAboutType.TalkAbout;
          break;
        case this.workflowTabs[1].id:
          this.cardType = TalkAboutType.Consider;
          break;
        case this.workflowTabs[2].id:
          this.cardType = TalkAboutType.Remind;
          break;
        default:
          this.cardType = null;
          break;
      }
      this.refreshTalkAboutData();
    }));
  }

  private refreshTalkAboutData() {
    this.showAllEntries = false;
    if (this.cardType) {
      this.showSpinner = true;
      this.cardProps = talkAboutDisplayConfig[this.cardType];
      if (this.talkAboutDataCache[this.cardType]) {
        this.setDataFromCache();
      } else {
        this.fetchTalkAboutData();
      }
    } else {
      this.cardProps = null;
      this.setDefaultData();
    }
  }

  private fetchTalkAboutData() {
    this.setDefaultData();
    const {reservationNumber, ihgRcNumber} = this.guestData;
    this.subscriptions$.add(
      this.talkAboutService.getTalkAboutDataShared(reservationNumber, ihgRcNumber, this.cardType, false)
        .pipe(finalize(() => this.showSpinner = false))
        .subscribe(data => {
            this.talkAboutDataCache[this.cardType] = data;
            this.setDataFromCache();
          },
          error => this.processHttpErrors(error)));
  }

  private setDataFromCache() {
    this.initialized = true;
    this.hasErrors = false;
    this.actions = this.talkAboutDataCache[this.cardType].actions;
    this.translationValues = this.talkAboutDataCache[this.cardType].translationValues;
    this.showSpinner = false;
  }

  private setDefaultData() {
    this.hasErrors = true;
    this.initialized = false;
    this.actions = [];
    this.translationValues = {};
  }

  public handleDockedItemClick(): void {
    this.dockedItemClick.emit();
  }

  ngOnDestroy() {
    this.subscriptions$.unsubscribe();
  }

}
