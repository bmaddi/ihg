import { TalkAboutType, TalkAboutCardConfig } from '@app/modules/check-in/components/talk-about/models/talk-about.models';



export const talkAboutTranslationMap = {
  [TalkAboutType.TalkAbout]: {
    'Birthday': 'LBL_TA_WISH_HAPPY_BDAY',
    'Book Direct': 'LBL_TA_BOOKING_DIRECT',
    'Reward Night': 'LBL_TA_THANK_REWARD_NIGHT',
    'Owners Association Rate': 'LBL_TA_WELCOME_IHG_OWNER_ACCOC',
    'Inner Circle Member': 'LBL_TA_WELCOME_INNER_CIRCLE',
    'New Member': 'LBL_TA_WELCOME_IHG_REW_CLUB',
    'New Ambassador Member': 'LBL_TA_THANK_AMBASSADOR_PRG',
    'New Royal Ambassador': 'LBL_TA_CONG_ROY_AMB',
    'New Inner Circle Member': 'LBL_TA_CONG_INNER_CIRCLE',
    'Ambassador': 'LBL_TA_WELCOME_AMBASSADOR',
    'New Status Spire': 'LBL_TA_CONG_SPIRE_STATUS',
    'New Status Platinum': 'LBL_TA_CONG_PLATINUM_STATUS',
    'New Status Gold': 'LBL_TA_CONG_GOLD_STATUS',
    'Close to Next Membership': 'LBL_TA_POINTS_TO_NEXT_LEVEL',
    'Welcome Back': 'LBL_TA_WELCOME_BACK',
    'Royal Ambassador': 'LBL_TA_WELCOME_AMBASSADOR',
    'Long Stay': 'LBL_TA_SUGGEST_ACTV_ATTRACT_LONG_STAY',
    'Ambassador Enrollment': 'LBL_TA_ASK_LEARN_AMBASSADOR_PRG',
    'IHG Employee': 'LBL_TA_WELCOME_IHG_EMP'
  },
  [TalkAboutType.Consider]: {
    'Owners Association Rate': 'LBL_TA_PROVIDE_WITH_IHG_BENEFITS',
    'New Ambassador Member': 'LBL_TA_PROVIDE_WITH_AMBASSADOR_BENEFITS',
    'New Royal Ambassador': 'LBL_TA_ROYAL_AMB_UPGRADE_IF_AV',
    'New Inner Circle Member': 'LBL_TA_INNER_CIRCLE_UPGRADE_IF_AV',
    'Ambassador': 'LBL_TA_PROVIDE_WITH_AMBASSADOR_BENEFITS',
    'New Status Spire': 'LBL_TA_SPIRE_ELITE_UPGRADE_IF_AV',
    'New Status Platinum': 'LBL_TA_PLATINUM_ELITE_UPGRADE_IF_AV',
    'New Status Gold': 'LBL_TA_GOLD_ELITE_UPGRADE_IF_AV'
  },
  [TalkAboutType.Remind]: {
  },
  showMore: 'LBL_GST_SHOW_MORE',
  showLess: 'LBL_GST_SHOW_LESS',
  error: 'LBL_TA_SOMETHING_WENT_WRONG'
};

export const talkAboutDisplayConfig: {[index: string]: TalkAboutCardConfig} = {
  [TalkAboutType.TalkAbout]: {
    title: 'LBL_TALK_ABT',
    icon: 'fas fa-comments',
    cardClass: 'talk-about-card',
    noneMessage: 'LBL_TA_NONE_TALK_ABOUT'
  },
  [TalkAboutType.Consider]: {
    title: 'LBL_CONSIDER',
    icon: 'fas fa-comment-dots',
    cardClass: 'consider-card',
    noneMessage: 'LBL_TA_NONE_CONSIDER'
  },
  [TalkAboutType.Remind]: {
    title: 'LBL_REMIND',
    icon: 'fas fa-bookmark',
    cardClass: 'remind-card',
    noneMessage: 'LBL_TA_NONE_REMIND'
  },
};
