export interface TalkAboutResponse {
  birthDate: string;
  pointsToNextLevel: number;
  nextLoyaltyLevel: string;
  actions: string[];
}

export interface TalkAboutModel {
  translationValues: object;
  actions: TalkAboutAction[];
}
export interface TalkAboutAction {
  name: string;
  translationKey: string;
}

export enum TalkAboutType {
  Remind = 'REMIND',
  TalkAbout = 'TALK_ABOUT',
  Consider = 'CONSIDER'
}

export interface TalkAboutCardConfig {
  title: string;
  icon: string;
  cardClass: string;
  noneMessage: string;
}
