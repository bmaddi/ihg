import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { throwError } from 'rxjs';

import { TalkAboutService } from './talk-about.service';
import { TalkAboutModel, TalkAboutType } from '@modules/check-in/components/talk-about/models/talk-about.models';

describe('TalkAboutService', () => {
  let service: TalkAboutService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;

  const mockResponse = <TalkAboutModel>{actions: []};
  const endPointController = 'checkIn/talkAbout';
  const checkAPICall = (req: HttpRequest<any>) => {
    return req.method === 'GET' && req.url.indexOf(endPointController) !== -1;
  };

  const fakeApiCall = (response: TalkAboutModel | any, {cardName, confirmationNumber, memberId}, error: boolean = false) => {
    request = httpMockController.expectOne(checkAPICall);
    expect(request.request.params.get('cardName')).toEqual(cardName);
    expect(request.request.params.get('confirmationNumber')).toEqual(confirmationNumber);
    expect(request.request.params.get('memberId')).toEqual(memberId);
    expect(request.request.method).toBe('GET');
    error ? request.error(response) : request.flush(response);
    httpMockController.verify();
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(TalkAboutService);
    httpMockController = TestBed.get(HttpTestingController);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send correct query parameters based on function inputs (Consider Scenario)', () => {
    const query = {confirmationNumber: '1234234123', memberId: '244534523', cardName: TalkAboutType.Consider};
    service.getTalkAboutData('1234234123', '244534523', TalkAboutType.Consider).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse, query);
  });

  it('should send correct query parameters based on function inputs (TalkAbout Scenario)', () => {
    const query = {confirmationNumber: '1234234123', memberId: '244534523', cardName: TalkAboutType.TalkAbout};
    service.getTalkAboutData('1234234123', '244534523', TalkAboutType.TalkAbout).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse, query);
  });

  it('should send correct query parameters based on function inputs (Remind Scenario)', () => {
    const query = {confirmationNumber: '1234234123', memberId: '244534523', cardName: TalkAboutType.Remind};
    service.getTalkAboutData('1234234123', '244534523', TalkAboutType.Remind).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse, query);
  });


  it('should store observable to avoid duplicate calls for same component', () => {
    const query = {confirmationNumber: '1234234123', memberId: '244534523', cardName: TalkAboutType.Remind};
    expect(service['initialTalkAboutDataLoad$'][TalkAboutType.Remind]).toBeNull();
    service.getTalkAboutDataShared('1234234123', '244534523', TalkAboutType.Remind).subscribe(data => {
      expect(data).toBeDefined();
    });
    expect(service['initialTalkAboutDataLoad$'][TalkAboutType.Remind]).not.toBeNull();
    fakeApiCall(mockResponse, query);
  });

  it('should clear shared observable on errors to allow new API calls', () => {
    const query = {confirmationNumber: '1234234123', memberId: '244534523', cardName: TalkAboutType.Remind};
    expect(service['initialTalkAboutDataLoad$'][TalkAboutType.Remind]).toBeNull();
    service.getTalkAboutDataShared('1234234123', '244534523', TalkAboutType.Remind).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(service['initialTalkAboutDataLoad$'][TalkAboutType.Remind]).toBeNull();
    });
    expect(service['initialTalkAboutDataLoad$'][TalkAboutType.Remind]).not.toBeNull();
    fakeApiCall(throwError({status: 500}), query, true);
  });
});
