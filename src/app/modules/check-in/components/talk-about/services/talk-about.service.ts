import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, share } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

import { environment } from '@env/environment';
import { TalkAboutType, TalkAboutResponse, TalkAboutModel } from '@app/modules/check-in/components/talk-about/models/talk-about.models';
import { talkAboutTranslationMap } from '@app/modules/check-in/components/talk-about/constants/talk-about-constants';
import { AppConstants } from '@app/constants/app-constants';

@Injectable({
  providedIn: 'root'
})
export class TalkAboutService {
  private readonly fdkApi = environment.fdkAPI;
  private initialTalkAboutDataLoad$: { [key: string]: Observable<TalkAboutModel> } = {
    [TalkAboutType.Remind]: null,
    [TalkAboutType.TalkAbout]: null,
    [TalkAboutType.Consider]: null
  };

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:max-line-length
  public getTalkAboutDataShared(reservationId: string, memberId: string, type: TalkAboutType, globalSpinner: boolean = false): Observable<TalkAboutModel> {
    if (this.initialTalkAboutDataLoad$[type] === null) {
      this.initialTalkAboutDataLoad$[type] = this.getTalkAboutData(reservationId, memberId, type, globalSpinner).pipe(share());
    }
    return this.initialTalkAboutDataLoad$[type];
  }

  // tslint:disable-next-line:max-line-length
  public getTalkAboutData(confirmationNumber: string, memberId: string, cardName: TalkAboutType, globalSpinner = false): Observable<TalkAboutModel> {
    const apiURl = `${this.fdkApi}checkIn/talkAbout`;
    return this.http.get(apiURl,
      {
        headers: new HttpHeaders().set('spinnerConfig', globalSpinner ? 'Y' : 'N'),
        params: {
          cardName,
          confirmationNumber,
          memberId: memberId || ''
        }
      })
      .pipe(
        map((response: TalkAboutResponse) => {
          return this.convertResponseToModel(cardName, response);
        }),
        catchError((error: HttpErrorResponse) => {
          this.initialTalkAboutDataLoad$[cardName] = null;
          return throwError(error);
        })
    );
  }

  private convertResponseToModel(type: TalkAboutType, response: TalkAboutResponse): TalkAboutModel {
    const names = response && response.actions ? response.actions : [];
    const translations = talkAboutTranslationMap[type];
    const actions =  names.map(name => {
      return {
        name,
        translationKey: translations[name]
      };
    });
    return {
      translationValues: this.getTranslationValues(response),
      actions
    };
  }

  private getTranslationValues(response: TalkAboutResponse) {
    if (response) {
      const {birthDate, pointsToNextLevel, nextLoyaltyLevel} = response;
      return {
        birthDate: moment(birthDate, AppConstants.DB_DT_FORMAT).format('MMMM Do'),
        pointsToNextLevel,
        nextLoyaltyLevel};
    } else {
      return {};
    }
  }

}
