import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule } from '@ngx-translate/core';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { AppSharedModule } from '@app-shared/app-shared.module';
import { TalkAboutService } from './services/talk-about.service';
import { TalkAboutComponent } from '@app/modules/check-in/components/talk-about/components/talk-about.component';
import { ActOnContainerComponent } from './components/act-on-container/act-on-container.component';

@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    TranslateModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    AppSharedModule
  ],
  declarations: [
    TalkAboutComponent,
    ActOnContainerComponent
  ],
  exports: [
    TalkAboutComponent,
    ActOnContainerComponent
  ],
  providers: [
    TalkAboutService
  ]
})
export class TalkAboutModule {
}
