export const BADGE_LABELS = {
  spire: 'LBL_SPIRE',
  platinum: 'LBL_PLTNM',
  gold: 'LBL_GOLD',
  club: 'LBL_CLUB',
  ambassador: 'LBL_FILTER_AMB',
  royalambassador: 'LBL_ROYAL_AMB',
  innerCircle: 'LBL_FILTER_IC',
  karma: 'LBL_FILTER_KAR',
  priorityEnrollment: 'LBL_PRIO_ENROL',
  potentialMember: 'LBL_POT_MEM',
  employee: 'LBL_FILTER_EMP'
};

export const PREP_STATUSES = {
  notStarted: 'NOTSTARTED',
  inProgress: 'INPROGRESS',
  ready: 'READY',
  checkedIn: 'CHECKEDIN'
};

export const STATE_DETAILS = {
  stateName : 'check-in'
};

export const CheckinAnalyticsConstants = {
  CATEGORY_CHECK_IN : 'Check In',
  CATEGORY_MANAGE_STAY : 'Manage Stay',
  ACTION_CHECKING_TIME_LAG_MSG : 'Page Load',
  ACTION_CHECKING_TIME_LAG_LBL:'PMS Reservation Out of Synch (60 sec or less)',
  ACTION_CHECKING_TIME_ERROR_MSG: 'Page Load',
  ACTION_CHECKING_TIME_ERROR_LBL: 'PMS Reservation Out of Synch and Check In Inaccessible'
}

export const GA = {
  ec: 'Loyalty Enrollment Modal',
  ea: 'Successful enrollment',
  copyToClipboard: 'Copy to Clipboard button clicked',
};
