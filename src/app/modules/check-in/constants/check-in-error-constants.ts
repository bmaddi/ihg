import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';

export const CHECK_IN_REPORT_ISSUE_AUTOFILL = {
  loadError: {
    function: 'Guests',
    topic: 'Check In - Reservation Info',
    subtopic: 'Error Message'
  },
  printRegCardError: {
    printErrorMsg: 'TOAST_ERR_PRINT_REG_CARD'
  },
  costEstimateError: {
    function: 'Guests',
    topic: 'Guest List - In House',
    subtopic: 'Error Message'
  }
};

export const FETCH_AUTH_ERROR_CONTENT: ServiceErrorComponentConfig = {
  timeout: {
    message: 'LBL_ERR_AUTH_DATA_LOAD_FLD',
    header: 'TOAST_ERROR',
    icon: 'fas fa-exclamation-triangle',
    showRefreshBtn: true,
    refreshActionText: 'COM_BTN_RFRSH'
  },
  general: {
    message: 'LBL_ERR_AUTH_DATA_LOAD_FLD',
    header: 'TOAST_ERROR',
    icon: 'fas fa-exclamation-triangle',
    showRefreshBtn: true,
    refreshActionText: 'COM_BTN_RFRSH'
  }
};
