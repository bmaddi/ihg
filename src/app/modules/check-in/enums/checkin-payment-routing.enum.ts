export enum RoutingType {
  singleUseCC = 'SUCC',
  guestPay = 'Guest Pay',
  extendCredit = 'Extend Credit'
}
