export const MOCK_COSTESTIMATE_RESPONSE = {
  costEstimateData: [{
    charges: [{
      ammount: "123",
      description: "Test Description"
    }],
    data: "Test",
    exCharge: "121",
    room: "111",
    roomRate: "123",
    tax: "123",
    totalAmmount: "1234",
    estimatedTotal: "2345"
  }]
};

export const MOCK_PAGECHANGE_EVENT = {
  skip: 10,
  take: 2
};