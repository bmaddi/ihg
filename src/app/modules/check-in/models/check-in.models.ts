import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { RoutingType } from '@modules/check-in/enums/checkin-payment-routing.enum';

export interface TasksModel {
  traceId: number;
  traceText: string;
  departmentCode: string;
  departmentName: string;
  traceTimestamp: string;
  isResolved: boolean;
  resolvedTimestamp: string;
  resolvedBy: string;
  error?: number;
}

export interface InfoItemsModel {
  type: string;
  detailText: string;
  detailCode: string;
  indicator: string;
  createTimestamp?: string;
}

export interface RoomFeatureModel {
  code: string;
  description: string;
  type?: string;
  matched?: boolean;
}

export interface RoomModel {
  roomNumber: string;
  roomStatus: string;
  roomType: string;
  houseKeepingStatus: string;
  frontOfficeStatus: string;
  features: RoomFeatureModel[];
  reservationFeatures: RoomFeatureModel[];
}

export interface HeartBeatActionsModel {
  myHotelActions: Array<string>;
  otherHotelActions: Array<string>;
}

export interface RateInfoModel {
  currencyCode: string;
  totalAmountBeforeTax: string;
  totalAmountAfterTax: string;
  totalTaxes: string;
}

export interface CardModel {
  code: string;
  expireDate: string;
  number: string;
}

export interface PaymentInfoModel {
  card: CardModel;
}

export interface PaymentModel {
  paymentInformation: PaymentInfoModel;
}

export interface CDSResUpdtdTS {
  timzone: string;
  timeFormat: string;
  time: string;
  operation: string;
  seconds: string;
}

export interface PayMethod {
  type: string;
  value: string;
}

export interface RoutingPayMethod {
  payMethod: PayMethod;
  last4Digits: string;
}

export interface PayRouting {
  routingInstruction?: string;
  owner: string;
  window: number;
  room: string;
  routingType?: string;
  payMethod?: RoutingPayMethod;
}

export interface ReservationDataModel {
  pmsGRSCompareFlag?: string;
  cdsResUpdtdTs?: CDSResUpdtdTS;
  amenityPoints?: string;
  amenityFlag?: boolean;
  isMasterReservation?: boolean;
  prefixName: string;
  firstName: string;
  lastName: string;
  suffixName: string;
  reservationNumber: string;
  pmsReservationNumber: string;
  pmsUniqueNumber: string;
  checkInDate: string;
  checkOutDate: string;
  doNotMoveRoom?: boolean;
  arrivalTime: string;
  ihgRcNumber: string;
  ihgRcMembership: string;
  ihgRcPoints: string;
  vipCode: string;
  keysCut: boolean;
  companyName: string;
  groupName: string;
  rateCategoryCode: string;
  roomTypeCode: string;
  roomTypeDescription: string;
  badges: string[];
  assignedRoomNumber: string;
  previousAssignedRoomNumber?: string;
  assignedRoomStatus: RoomModel;
  numberOfNights: number;
  numberOfRooms: number;
  numberOfAdults: number;
  numberOfChildren: number;
  preferences: string[];
  specialRequests: string[];
  infoItems: InfoItemsModel[];
  tasks: TasksModel[];
  prepStatus: string;
  pmsReservationStatus: string;
  heartBeatActions?: HeartBeatActionsModel;
  heartBeatComment: string;
  heartBeatImprovement: string;
  otaFlag: boolean;
  resVendorCode: string;
  inspected: boolean;
  ratesInfo: RateInfoModel;
  payment: PaymentModel;
  confirmationDate?: string;
  selectedAmenity?: string;
  pointsPosted?: boolean;
  authError?: boolean;
  isCheckedIn?: boolean;
  paymentType?: string;
  ineligibleReservation?: boolean;
  isCutkeyError?: boolean;
  noPost?: boolean;
  pmsAuthSaveInd?: string;
  freeUpgradeInd?: boolean;
  payRoutings?: PayRouting[];
  rateCode?: string;
  routingType?: RoutingType;
}

export interface GuestCheckInModel {
  payIntegratedFlag?: string;
  reservationData?: ReservationDataModel;
  errors?: ErrorsModel[];
  status: string;
}

export interface LoyaltyInfoModel {
  memberDetails: {
    firstName?: string;
    lastName?: string;
    prefixName?: string;
    ihgRcNumber: string;
    ihgRcMembership: string;
    ihgRcPoints: string;
    badges: string[];
  };
}

export interface Charges {
  ammount: string;
  description: string;
}

export interface CostEstimate {
  charges: Charges[];
  data: string;
  exCharge: string;
  room: string;
  roomRate: string;
  tax: string;
  totalAmmount: string;
  estimatedTotal: string;
}

export interface CostEstimateResponse {
  costEstimateData: CostEstimate[];
}

export const CHECK_IN_ERROR: { [key: string]: string } = {
  CheckinDate: 'LBL_ARR_DT',
  RoomQuantity: 'LBL_ERR_ROOM_QTY',
  CheckoutDate: 'LBL_DEP_DT',
  LoyaltyId: 'LBL_RW_NO',
  RoomType: 'LBL_ROOM_TYPE',
  RateCategoryCode: 'LBL_RATE_CAT',
  GuestName : 'LBL_GST_GUEST_NAME',
  AdultsCount: 'LBL_ADULTS'
}

export const PMS_NOT_IN_SYNC_SECONDRY: Partial<ErrorToastMessageTranslations> = {
  retryButton: 'LBL_VW_DTLS',
  reportAnIssueButton: '',
  generalError: {
    message: 'COM_TOAST_WARNING',
    detailMessage: 'LBL_SCND_PMS_INCONSIST'
  }
}

export interface PMSNotInSyncKey {
  key: string;
  label: string;
}
