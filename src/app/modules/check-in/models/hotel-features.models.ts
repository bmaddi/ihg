export interface OnSiteDining {
  breakfast: string;
  eveningReception: string;
  inRoomDining: string;
}

export interface Restaurant {
  name: string;
  cuisineType: string;
  hours: string;
  phone: string;
}

export interface Bar {
  name: string;
  hours: string;
  phone: string;
}

export interface DiningDataModel {
  onSiteDining: OnSiteDining;
  restaurants: Restaurant[];
  bars: Bar[];
}

export interface Pool {
  indoorPool: string;
  outdoorPool: string;
}

export interface WellnessDataModel {
  onSiteFitnessCenter: string;
  pool: Pool;
  amenities: string[];
}

