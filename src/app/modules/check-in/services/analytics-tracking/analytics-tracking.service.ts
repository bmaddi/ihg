import { Injectable } from '@angular/core';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import { PrepareForArrivalsGaConstants } from '@app/modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsTrackingService {

  constructor(private gaService: GoogleAnalyticsService) {
  }

  public trackClickToggleDining(): void {
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN,
      CheckinAnalyticsConstants.ACTION_TOGGLE_HOTEL_FEATURE,
      CheckinAnalyticsConstants.LBL_TOGGLE_HOTEL_FEATURE_DINING);
  }

  public trackClickToggleWellness(): void {
    this.gaService.trackEvent(CheckinAnalyticsConstants.CATEGORY_CHECK_IN,
      CheckinAnalyticsConstants.ACTION_TOGGLE_HOTEL_FEATURE,
      CheckinAnalyticsConstants.LBL_TOGGLE_HOTEL_FEATURE_WELLNESS);
  }

  public trackBulkNotifyWarning(): void {
    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY,
      PrepareForArrivalsGaConstants.ACTION_NOTIFY_GUEST_BULK_WAR,
      PrepareForArrivalsGaConstants.SELECTED);
  }
  public trackBulkNotifyNoWarning(): void {
    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY,
      PrepareForArrivalsGaConstants.ACTION_NOTIFY_GUEST_BULK_NO_WAR,
      PrepareForArrivalsGaConstants.SELECTED);
  }
}
