import { Injectable } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError, Subject, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { UserService, UserConfirmationModalConfig, Alert, AlertType } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { EnrollNewMemberModalComponent, GuestListModel, SingleModeEnrollmentModel, EnrollNewMemberService } from 'ihg-ng-common-pages';

import { environment } from '@env/environment';
import { GuestCheckInModel, ReservationDataModel, LoyaltyInfoModel, CostEstimateResponse } from '@modules/check-in/models/check-in.models';
import { AdditionalAlert } from '@app/modules/guest-info/models/guest-info.model';
import { get } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CheckInService {
  private readonly API_URL = environment.fdkAPI;
  private workflowTabChanged = new Subject<string>();
  private enrollmentSuccessSource: Subject<SingleModeEnrollmentModel> = new Subject();
  private refreshClickWatcher = new Subject<null>();
  private reservationNumber: string;
  private listnerCheckInNonIntegrated = new Subject<null>();
  enrollmentSuccess = this.enrollmentSuccessSource.asObservable();

  
  public ihgSyncMessageSource = new BehaviorSubject<Alert>(null);
  additioanlWarningMessage = this.ihgSyncMessageSource.asObservable();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private modalService: NgbModal,
    private titlePipe: TitleCasePipe,
    private confirmationModalService: ConfirmationModalService, private enrollmentService: EnrollNewMemberService) {
  }

  public triggerCheckInNonIntegrated(): void {
    this.listnerCheckInNonIntegrated.next();
  }

  public listenTriggerSaveAuthDetails(): Observable<null> {
    return this.listnerCheckInNonIntegrated.asObservable();
  }

  public getCheckInReservationData(reservationNumber: string): Observable<GuestCheckInModel> {
    const apiURl = `${this.API_URL}checkIn/fetchReservation/${this.getLocationId()}${this.getCheckInDataParams(reservationNumber)}`;
    return this.http.get(apiURl).pipe(map((response: GuestCheckInModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getCheckInDataParams(reservationNumber: string): string {
    return `?brandCode=${this.getBrandCode()}&reservationNumber=${reservationNumber}`;
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  private getBrandCode(): string {
    return this.userService.getCurrentLocation().brandCode;
  }

  public openEnrollmentModal(guestData: ReservationDataModel): void {  
    this.enrollmentService.setIsCheckInFlow(true);
    const modal = this.modalService.open(
      EnrollNewMemberModalComponent, { windowClass: 'enm__modal', backdrop: 'static', keyboard: false });
    modal.componentInstance.selectedGuest = {
      selected: <GuestListModel>{
        reservationNumber: guestData.reservationNumber,
        salutation: guestData.prefixName,
        firstName: guestData.firstName,
        lastName: guestData.lastName
      }
    };
  }

  emitWorkflowTabEvent(newTabId: string) {
    this.workflowTabChanged.next(newTabId);
  }

  getWorkflowTabEvent() {
    return this.workflowTabChanged;
  }

  public setRefreshPointSubject() {
    this.refreshClickWatcher.next();
  }

  public getRefreshPointSubject(): Observable<null> {
    return this.refreshClickWatcher.asObservable();
  }

  public getLoyaltyInfo(membershipId: string): Observable<LoyaltyInfoModel> {
    const apiURl = `${this.API_URL}loyalty/memberDetails/${membershipId}`;
    return this.http.get(apiURl).pipe(map((response: LoyaltyInfoModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public formatGuestName(guest: ReservationDataModel): string {
    const salutation = guest.prefixName ? (guest.prefixName.endsWith('.') ? guest.prefixName : `${guest.prefixName}.`) : '';
    return this.titlePipe.transform(`${salutation} ${guest.firstName} ${guest.lastName}`);
  }

  public costEstimate(reservationNumber: string) {
    const apiURl = `${this.API_URL}checkIn/costEstimate?currentLocationId=${this.getLocationId()}&reservationNumber=${reservationNumber}`;

    return this.http.get(apiURl).pipe(map((response: CostEstimateResponse) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public openCheckInWarning(message: string): Observable<void> {
    return this.confirmationModalService.openConfirmationModal(this.getConfirmModalConfig(message));
  }

  private getConfirmModalConfig(message: string): UserConfirmationModalConfig {
    return {
      modalHeader: 'LBL_WRN_CNOT_CHK_IN',
      defaultButtonLabel: 'COM_BTN_CLOSE',
      defaultButtonClass: 'btn-default',
      messageHeader: message,
      windowClass: 'hide-primary-btn modal-medium',
      dismissible: true
    };
  }

  public updateReservationNumber(resNumber: string): void {
    this.reservationNumber = resNumber;
  }

  public getReservationNumber(): string {
    return this.reservationNumber;
  }

 
}