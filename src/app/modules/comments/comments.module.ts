import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { AppSharedModule } from '../shared/app-shared.module';
import { CommentsComponent } from './components/comments.component';
import { CommentsService } from './services/comments.service';

@NgModule({
  imports: [
    CommonModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    AppSharedModule,
    TranslateModule
  ],
    declarations: [
        CommentsComponent
    ],
  exports: [
    CommentsComponent
  ],
  providers: [
    CommentsService
  ]
})
export class CommentsModule {
}
