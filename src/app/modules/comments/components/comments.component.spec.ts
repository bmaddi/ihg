import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { ReportIssueService } from 'ihg-ng-common-pages';
import { CardContainerModule } from 'ihg-ng-common-components';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { CommentsService } from '@modules/comments/services/comments.service';
import { CommentsComponent } from '@modules/comments/components/comments.component';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import {
  COMMENTS_LIST,
  COMMENTS_MOCK_RESERVATION,
  COMMENTS_RESPONSE,
  COMMENTS_WITH_LESS_CONTENT
} from '@modules/comments/mocks/comments-mock';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CommentsComponent', () => {
  let component: CommentsComponent;
  let fixture: ComponentFixture<CommentsComponent>;
  let commentsService: CommentsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, AppSharedModule, CardContainerModule, HttpClientTestingModule],
      declarations: [CommentsComponent],
      providers: [CommentsService, ReportIssueService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(CommentsComponent);
    component = fixture.componentInstance;
    component.guestData = COMMENTS_MOCK_RESERVATION;
    component.commentsList = COMMENTS_LIST;
    commentsService = TestBed.get(CommentsService);
    fixture.detectChanges();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display No comments message if there are no comments', fakeAsync(() => {
    const commentsSpy = spyOn(commentsService, 'getComments').and.returnValue(of({ comments: [] }));
    const errorElement = fixture.debugElement.query(By.css('[data-slnm-ihg="CommentsError-SID"]')).nativeElement;
    component['getCommentsData']();
    fixture.detectChanges();
    tick();
    expect(commentsSpy).toHaveBeenCalledWith(component.guestData.reservationNumber);
    expect(errorElement.textContent).not.toBeNull();
  }));

  it('should display comments', () => {
    const commentsSpy = spyOn(commentsService, 'getComments').and.returnValue(of(COMMENTS_RESPONSE));
    component['getCommentsData']();
    fixture.detectChanges();
    const commentListEl = fixture.debugElement.query(By.css('.text')).nativeElement;
    fixture.detectChanges();
    expect(commentsSpy).toHaveBeenCalled();
    expect(commentListEl.textContent).toEqual(COMMENTS_RESPONSE.comments[0].commentText);
  });

  it('should contain header as Comments', () => {
    const commentHeaderEl = fixture.debugElement.query(By.css('.comments uic-card.comments-card')).nativeElement;
    fixture.detectChanges();
    expect(commentHeaderEl).toBeDefined();
    expect(commentHeaderEl.textContent).toContain('Comments');
  });

  it('should display show more when content overflows', () => {
    const showMoreButton = fixture.debugElement.queryAll(By.css('[data-slnm-ihg="CommentsShowMore-SID"]'));
    fixture.detectChanges();
    expect(showMoreButton).toBeDefined();
  });

  it('should hide show more when content is less', () => {
    const showMoreButton = fixture.debugElement.query(By.css('.show-more-button'));
    component.commentsList = COMMENTS_WITH_LESS_CONTENT;
    component.maxHeight = 260;
    component.containerHeight = 246;
    fixture.detectChanges();
    expect(showMoreButton.nativeElement.hidden).toBeTruthy();
  });

  it('should display comments upto 4 when show more is clicked', () => {
    const showMoreButton = fixture.debugElement.query(By.css('[data-slnm-ihg="CommentsShowMore-SID"]'));
    fixture.detectChanges();
    expect(showMoreButton).toBeDefined();
    showMoreButton.triggerEventHandler('click', null);
    component.maxHeight = 260;
    component.containerHeight = 266;
    fixture.detectChanges();

    expect(component.commentsList.length).toBe(4);
  });

  it('should display show less', () => {
    const showLessButton = fixture.debugElement.queryAll(By.css('[data-slnm-ihg="ShowLess-SID"]'));
    component.showViewMore = true;
    fixture.detectChanges();
    expect(showLessButton).toBeDefined();
  });

});
