import {
  Component,
  OnDestroy,
  OnInit,
  Input,
  SimpleChanges,
  AfterViewInit,
  OnChanges,
  ElementRef,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { of, Subscription } from 'rxjs';
import { delay, finalize, tap } from 'rxjs/operators';

import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { CommentsService } from '@modules/comments/services/comments.service';
import { CommentsList, CommentsResponse } from '@modules/comments/interfaces/comments.interface';
import { COMMENTS_ERROR_CONTENT, commentsTranslation } from '@modules/comments/constants/comments-constants';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { UtilityService } from '@services/utility/utility.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent extends AppErrorBaseComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() guestData: ReservationDataModel;
  @ViewChild('commentsContainer') commentsContainer: ElementRef;
  maxEntries = 4;
  showViewMore = false;
  showSpinner = false;
  commentsList: CommentsList[];
  errorConfig = COMMENTS_ERROR_CONTENT;
  errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  translations = commentsTranslation;
  containerHeight: number;
  maxHeight = 260;

  private subscriptions$: Subscription = new Subscription();

  constructor(private commentsService: CommentsService, private utilityService: UtilityService,
              private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.getCommentsData();
  }

  ngAfterViewInit() {
    this.checkForElementOverflow();
    this.changeDetector.detectChanges();
  }

  private checkForElementOverflow() {
    this.subscriptions$.add(
      of(true)
        .pipe(
          delay(0),
          tap(() => this.checkIfShowViewMore())
        ).subscribe()
    );
  }

  private checkIfShowViewMore() {
    if (this.commentsContainer) {
      this.containerHeight = this.commentsContainer.nativeElement.scrollHeight;
    }
  }

  toggleValue() {
    this.showViewMore = !this.showViewMore;
  }

  private getCommentsData() {
    this.showSpinner = true;
    this.subscriptions$.add(this.commentsService.getComments(this.guestData.reservationNumber)
      .pipe(finalize(() => this.showSpinner = false))
      .subscribe((response: CommentsResponse) => {
        this.handleCommentsResponse(response);
      }, (error: HttpErrorResponse) => {
        super.processHttpErrors(error);
      }));
  }

  private handleCommentsResponse(response: CommentsResponse) {
    if (!super.checkIfAnyApiErrors(response, (data) => !(data.comments && data.comments.length))) {
      this.commentsList = response.comments;
      this.checkIfShowViewMore();
    }
  }

  ngOnDestroy() {
    this.subscriptions$.unsubscribe();
  }
}
