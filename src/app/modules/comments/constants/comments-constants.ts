export const COMMENTS_ERROR_CONTENT = {
  timeout: {
    message: 'LBL_ERR_DATA_LOAD',
    icon: '',
    showRefreshBtn: false
  },
  general: {
    message: 'LBL_ERR_DATA_LOAD',
    icon: '',
    showRefreshBtn: false
  },
  noData: {
    message: 'LBL_NO_CMT_AVBL'
  }
};

export const commentsTranslation = {
  showMore: 'LBL_GST_SHOW_MORE',
  showLess: 'LBL_GST_SHOW_LESS',
  error: 'LBL_TA_SOMETHING_WENT_WRONG'
};
