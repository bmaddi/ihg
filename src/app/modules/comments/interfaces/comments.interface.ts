export interface CommentsResponse {
  comments: CommentsList[];
}

export interface CommentsList {
  commentId: string;
  commentText: string;
  commentType: string;
  guestViewable: boolean;
}
