import { CommentsList } from '@modules/comments/interfaces/comments.interface';

export const COMMENTS_MOCK_RESERVATION = {
  'pmsGRSCompareFlag': 'Y',
  'prefixName': 'Mr',
  'firstName': 'Saurav',
  'lastName': 'Agrawal',
  'suffixName': '',
  'reservationNumber': '44002733',
  'pmsReservationNumber': '447469',
  'pmsReservationStatus': 'DUEIN',
  'pmsUniqueNumber': '466611',
  'arrivalTime': '',
  'checkInDate': '2019-06-05',
  'checkOutDate': '2019-06-07',
  'numberOfNights': 2,
  'ihgRcNumber': '123456',
  'ihgRcMembership': 'Spire Elite',
  'ihgRcPoints': '5000',
  'vipCode': '',
  'isKeyCut': false,
  'companyName': 'ARKANSAS HOSPICE INC',
  'groupName': '',
  'rateCategoryCode': 'IGBBB',
  'roomTypeCode': 'KEXG',
  'roomTypeDescription': '1 KING BED EXECUTIVE',
  'numberOfRooms': 1,
  'badges': [
    'spire',
    'ambassador',
    'employee'
  ],
  'assignedRoomNumber': '',
  'assignedRoomStatus': null,
  'numberOfAdults': 1,
  'numberOfChildren': 0,
  'preferences': [
    'HIGH FLOOR',
    'BALCONY',
    'ROOM NEAR ELEVATOR',
    'OCEAN VIEW',
    'NEAR FITNESS ROOM'
  ],
  'specialRequests': [
    'Need Extra Towels and Water Bottles'
  ],
  'infoItems': [],
  'tasks': [],
  'prepStatus': 'INPROGRESS',
  'heartBeatActions': {
    'myHotelActions': [
      'GuestProblem',
      'GuestComment',
      'SleepIssue'
    ],
    'otherHotelActions': [
      'GuestLove'
    ]
  },
  'heartBeatComment': 'Good Hospitality. Looking forward to revisit again',
  'heartBeatImprovement': 'Add more Indoor Events',
  'otaFlag': false,
  'resVendorCode': 'BKGTL',
  'ratesInfo': {
    'currencyCode': 'EUR',
    'totalAmountBeforeTax': '755.14',
    'totalAmountAfterTax': '808.00',
    'totalTaxes': '52.86'
  },
  'payment': {
    'paymentInformation': {
      'card': {
        'code': 'VS',
        'expireDate': '0420',
        'number': '7472471499080010'
      }
    }
  },
  'inspected': false,
  'paymentType': 'CARD',
  'isCheckedIn': false,
  'amenityPoints': '500',
  'keysCut': false
};

export const COMMENTS_RESPONSE = {
  'comments': [
    {
      'commentId': 45777,
      'commentText': 'XYZ has payment set up. Don\'t ask guest for card. Lorem Ipsum dolor sit amet consectetur.XYZ has payment set up. Don\'t ask guest for card.',
      'commentType': 'GENERAL',
      'guestViewable': false
    },
    {
      'commentId': 45778,
      'commentText': 'ABC has payment set up. Don\'t ask guest for card. Lorem Ipsum dolor sit amet consectetur.ABC has payment set up. ' +
        'Don\'t ask guest for card.',
      'commentType': 'RESERVATION',
      'guestViewable': false
    },
    {
      'commentId': 45781,
      'commentText': 'Reservation comments',
      'commentType': 'RESERVATION',
      'guestViewable': false
    },
    {
      'commentId': 45779,
      'commentText': 'All comments',
      'commentType': 'IN HOUSE',
      'guestViewable': false
    },
    {
      'commentId': 45782,
      'commentText': 'Inhouse Comments',
      'commentType': 'IN HOUSE',
      'guestViewable': false
    },
    {
      'commentId': 45780,
      'commentText': 'All comments',
      'commentType': 'CASHIER',
      'guestViewable': false
    },
    {
      'commentId': 45783,
      'commentText': 'Test cashier comments',
      'commentType': 'CASHIER',
      'guestViewable': false
    }
  ]
};

export const COMMENTS_LIST: CommentsList[] = [{
  commentId: '45777',
  commentText: 'XYZ has payment set up. Don\'t ask guest for card. Lorem Ipsum dolor sit amet consectetur.XYZ has payment set up. ' +
    'Don\'t ask guest for card.',
  commentType: 'GENERAL',
  guestViewable: false
}, {
  commentId: '45778',
  commentText: 'ABC has payment set up. Don\'t ask guest for card. Lorem Ipsum dolor sit amet consectetur.ABC has payment set up. ' +
    'Don\'t ask guest for card.',
  commentType: 'RESERVATION',
  guestViewable: false
},
  {
    commentId: '45779',
    commentText: 'ABC has payment set up. Don\'t ask guest for card. Lorem Ipsum dolor sit amet consectetur.ABC has payment set up. ' +
      'Don\'t ask guest for card.',
    commentType: 'IN HOUSE',
    guestViewable: false
  },
  {
    commentId: '45790',
    commentText: 'ABC has payment set up. Don\'t ask guest for card. Lorem Ipsum dolor sit amet consectetur.ABC has payment set up. ' +
      'Don\'t ask guest for card.',
    commentType: 'CASHIER',
    guestViewable: false
  }
];

export const COMMENTS_WITH_LESS_CONTENT = [{
    commentId: '123',
    commentText: 'ABC has payment set up.',
    commentType: 'GENERAL',
    guestViewable: false
  },
  {
    commentId: '345',
    commentText: 'DEF has vouchers.',
    commentType: 'RESERVATION',
    guestViewable: false
  }];
