import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { CommentsService } from '@modules/comments/services/comments.service';

describe('CommentsService', () => {
  let service: CommentsService;

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        imports: [CommonTestModule, HttpClientTestingModule]
      });
    service = TestBed.get(CommentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
