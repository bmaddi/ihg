import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';

import { environment } from '../../../../environments/environment';

import { UserService } from 'ihg-ng-common-core';
import { CommentsResponse } from '../interfaces/comments.interface';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  private readonly fdkApi = environment.fdkAPI;

  constructor(private http: HttpClient, private userService: UserService) {
  }

  public getComments(resNum: string, globalSpinner = false): Observable<CommentsResponse> {
    const apiURl = `${this.fdkApi}comments/fetchResComments/${this.getLocationId()}?grsReservationNumber=${resNum}`;
    return this.http.get(apiURl,
      {
        headers: new HttpHeaders().set('spinnerConfig', globalSpinner ? 'Y' : 'N')
      }).pipe(map((response: CommentsResponse) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

}
