import { CommonTestModule } from './common-test.module';

describe('CommonTestModule', () => {
  let commonTestModule: CommonTestModule;

  beforeEach(() => {
    commonTestModule = new CommonTestModule();
  });

  it('should create an instance', () => {
    expect(commonTestModule).toBeTruthy();
  });
});
