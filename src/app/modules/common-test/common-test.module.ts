import { NgModule } from '@angular/core';
import {CommonModule, TitleCasePipe} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {EnvironmentService, GoogleAnalyticsService, IhgNgCommonCoreModule} from 'ihg-ng-common-core';
import {IhgNgCommonKendoModule} from 'ihg-ng-common-kendo';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    TranslateModule.forRoot(),
    RouterTestingModule,
    IhgNgCommonCoreModule.forRoot(),
  ],
  declarations: [],
  exports: [
    CommonModule,
    NgbModule,
    TranslateModule,
    RouterTestingModule,
    IhgNgCommonCoreModule,
  ],
  providers: [
    TitleCasePipe,
  ]
})
export class CommonTestModule { }
