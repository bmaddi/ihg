import {environment} from '@env/environment.local-int';
import {EnvironmentService} from 'ihg-ng-common-core';
import {TestBed } from '@angular/core/testing';

export function setTestEnvironment(): void {
  const envService = TestBed.get(EnvironmentService);
  envService.setEnvironmentConstants(environment);
}

export function hasClass(element: HTMLElement, classMatcher: string): boolean {
  return element.className.split(' ').indexOf(classMatcher) !== -1;
}
