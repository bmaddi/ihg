import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRouteModule } from './demo.route.module';
import { DemoComponent } from './components/demo/demo.component';

// import { LazyModule } from 'ihg-ng-common-core';

@NgModule({
  imports: [
    CommonModule,
    DemoRouteModule,
    // LazyModule
  ],
  declarations: [DemoComponent]
})
export class DemoModule { }
