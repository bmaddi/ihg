import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DemoComponent } from './components/demo/demo.component';

const routes: Routes = [
  {
    path: '',
    component: DemoComponent,
    data: {
      crumb: [],
      pageTitle: 'Demo',
      noSubtitle: true,
      gaPageTitle: 'Concerto',
      stateName: 'dashboard',
      stateView: 'dashboard'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: []
})
export class DemoRouteModule {
}

