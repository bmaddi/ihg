import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DnmReasonTooltipContentComponent } from './dnm-reason-tooltip-content.component';
import { mockDnmReason } from '../../mocks/do-not-move.mocks';
import { CommentModel } from '../../models/do-not-move.models';
import { By } from '@angular/platform-browser';

describe('DnmReasonTooltipContentComponent', () => {
  let component: DnmReasonTooltipContentComponent;
  let fixture: ComponentFixture<DnmReasonTooltipContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnmReasonTooltipContentComponent]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnmReasonTooltipContentComponent);
    component = fixture.componentInstance;
    component.dnmReason = { ...mockDnmReason };
    fixture.detectChanges();
  });

  it('should properly display the reason', () => {
    const element: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg=DNMTooltipReason-SID]')).nativeElement;
    expect(element.innerText).toContain('Test Reason');
  });

  it('should properly display the user name', () => {
    const element: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg=DNMTooltipUserName-SID]')).nativeElement;
    expect(element.innerText).toEqual('John Doe');
  });

  it('should properly display the date and time', () => {
    const element: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg=DNMTooltipDate-SID]')).nativeElement;
    expect(element.innerText).toEqual('11DEC2019 07:45:30');
  });
});
