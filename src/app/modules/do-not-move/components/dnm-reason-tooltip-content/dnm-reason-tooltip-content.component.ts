import { Component, Input } from '@angular/core';
import { CommentModel } from '../../models/do-not-move.models';

@Component({
  selector: 'app-dnm-reason-tooltip-content',
  templateUrl: './dnm-reason-tooltip-content.component.html',
  styleUrls: ['./dnm-reason-tooltip-content.component.scss']
})
export class DnmReasonTooltipContentComponent {

  @Input() dnmReason: CommentModel;

  constructor() { }

}
