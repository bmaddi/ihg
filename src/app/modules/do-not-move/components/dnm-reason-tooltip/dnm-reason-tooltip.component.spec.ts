import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NgbModule, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { of, throwError } from 'rxjs';

import { DnmReasonTooltipComponent } from './dnm-reason-tooltip.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { DnmReasonTooltipContentComponent } from '../dnm-reason-tooltip-content/dnm-reason-tooltip-content.component';
import { DoNotMoveService } from '../../services/do-not-move.service';
import { mockDnmReason, mockAddReasonResponse, mockAddReasonErrorResponse } from '../../mocks/do-not-move.mocks';
import { SimpleChanges } from '@angular/core';
import { blankReason } from '../../constants/do-not-move.constants';


export class MockNgbPopover {
  open() { }
  close() { }
}

describe('DnmReasonTooltipComponent', () => {
  let component: DnmReasonTooltipComponent;
  let fixture: ComponentFixture<DnmReasonTooltipComponent>;
  let service: DoNotMoveService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, NgbModule.forRoot()],
      declarations: [DnmReasonTooltipComponent, DnmReasonTooltipContentComponent],
      providers: [DoNotMoveService, {
        provide: NgbPopover, useClass: MockNgbPopover
      }]
    })
  }));

  beforeEach(() => {
    service = TestBed.get(DoNotMoveService);
    fixture = TestBed.createComponent(DnmReasonTooltipComponent);
    component = fixture.componentInstance;
    component.dnmReason = null;
  });

  it('should call fetchComment on component initialization', () => {
    const fetchSpy = spyOn<any>(component, 'fetchComment');
    fixture.detectChanges();
    expect(fetchSpy).toHaveBeenCalled();
  });

  it('should display the tooltip momentarily when the reason is updated', () => {
    const tooltipSpy = spyOn<any>(component, 'showTooltipMomentarily');
    const changes: SimpleChanges = {
      dnmReason: {
        previousValue: null,
        currentValue: { ...mockDnmReason },
        firstChange: false,
        isFirstChange: () => false
      }
    };
    component.ngOnChanges(changes);
    expect(tooltipSpy).toHaveBeenCalled();
  });

  it('should display "None Provide" as a reason when the reason is empty', () => {
    component.dnmReason = { ...mockDnmReason };
    component.dnmReason.reason = '';
    component['updateDefaultReason']();
    expect(component.dnmReason.reason).toEqual(blankReason);
  });

  it('should check availability of comment / reason in the response', () => {
    const successResponse = { ...mockAddReasonResponse };
    const successReturnVal = component['isCommentUnavailable'](successResponse);
    expect(successReturnVal).toEqual(false);

    const errorResponse = { ...mockAddReasonErrorResponse };
    const errorReturnVal = component['isCommentUnavailable'](errorResponse);
    expect(errorReturnVal).toEqual(true);
  });

  it('should hide the tooltip in case of error to fetch comment', () => {
    const errorHandlerSpy = spyOn<any>(component, 'handleError').and.callThrough();
    spyOn(service, 'fetchComment').and.returnValue(throwError({}));
    component['fetchComment']();
    expect(errorHandlerSpy).toHaveBeenCalled();
    expect(component.dnmReason).toBeNull();
  });

  it('should call service method with proper argument and handle success', () => {
    const subscriptionAddSpy = spyOn(component['rootSubscription'], 'add');
    const fetchCommentSpy = spyOn(service, 'fetchComment').and.returnValue(of(mockAddReasonResponse));
    const responseHandlerSpy = spyOn<any>(component, 'handleResponse').and.callThrough();
    const successHandlerSpy = spyOn<any>(component, 'handleSuccess').and.callThrough();
    const defaultReasonSpy = spyOn<any>(component, 'updateDefaultReason').and.callThrough();
    component.reservationNumber = '123456';

    component['fetchComment']();
    fixture.detectChanges();
    expect(subscriptionAddSpy).toHaveBeenCalled();
    expect(fetchCommentSpy).toHaveBeenCalledWith('123456');
    expect(responseHandlerSpy).toHaveBeenCalledWith(mockAddReasonResponse);
    expect(successHandlerSpy).toHaveBeenCalledWith(mockAddReasonResponse);
    expect(component.dnmReason).toEqual(mockAddReasonResponse.doNotMoveRoomComments[0]);
    expect(defaultReasonSpy).toHaveBeenCalled();
  });

  it('should display the tooltip momentarily', fakeAsync(() => {
    const openSpy = spyOn(component.popover, 'open');
    const closeSpy = spyOn(component.popover, 'close');
    component['showTooltipMomentarily']();
    expect(openSpy).toHaveBeenCalled();
    expect(closeSpy).not.toHaveBeenCalled();
    tick(component['defaultDuration']);
    expect(closeSpy).toHaveBeenCalled();
  }));

});
