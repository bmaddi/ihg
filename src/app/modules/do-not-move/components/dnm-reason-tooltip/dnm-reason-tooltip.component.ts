import { Component, OnInit, Input, OnDestroy, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription, of } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';

import { CommentModel, DNMCommentsModel } from '../../models/do-not-move.models';
import { DoNotMoveService } from '../../services/do-not-move.service';
import { blankReason } from '../../constants/do-not-move.constants';

@Component({
  selector: 'app-dnm-reason-tooltip',
  templateUrl: './dnm-reason-tooltip.component.html',
  styleUrls: ['./dnm-reason-tooltip.component.scss']
})
export class DnmReasonTooltipComponent implements OnInit, OnChanges, OnDestroy {

  private rootSubscription = new Subscription();

  @Input() dnmReason: CommentModel;
  @Input() reservationNumber: string;
  @Input() defaultDuration = 3000;
  @Input() dnmInteraction: boolean;

  @ViewChild('popover') popover: NgbPopover;

  constructor(private doNotMoveService: DoNotMoveService) { }

  ngOnInit() {
    this.fetchComment();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.dnmReason && changes.dnmReason.currentValue) {
      this.showTooltipMomentarily();
    }
  }

  private fetchComment(): void {
    this.rootSubscription.add(
      this.doNotMoveService.fetchComment(this.reservationNumber)
        .subscribe(
          (response: DNMCommentsModel) => this.handleResponse(response),
          (error) => this.handleError())
    );
  }

  private handleResponse(response: DNMCommentsModel): void {
    if (this.isCommentUnavailable(response)) {
      this.handleError();
    } else {
      this.handleSuccess(response);
    }
  }

  private isCommentUnavailable(response: DNMCommentsModel): boolean {
    return !!(response && response.errors) || (response && !response.doNotMoveRoomComments.length);
  }

  private handleSuccess(response: DNMCommentsModel): void {
    this.dnmReason = response.doNotMoveRoomComments[0];
    this.updateDefaultReason();
    if (this.dnmInteraction) {
      this.showTooltipMomentarily();
    }
  }

  private updateDefaultReason(): void {
    if (!this.dnmReason.reason) {
      this.dnmReason.reason = blankReason;
    }
  }

  private showTooltipMomentarily(): void {
    this.popover.open();
    this.rootSubscription.add(of(true).pipe(
      delay(this.defaultDuration),
      tap(() => this.popover.close())
    ).subscribe());
  }

  private handleError(): void {
    this.dnmReason = null;
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
