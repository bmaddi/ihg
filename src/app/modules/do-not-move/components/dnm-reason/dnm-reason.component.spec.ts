import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, SimpleChanges, SimpleChange } from '@angular/core';
import { FormsModule, ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { DnmReasonComponent } from './dnm-reason.component';
import { DoNotMoveService } from '../../services/do-not-move.service';
import { mockDnmReason, mockAddReasonResponse, mockAddReasonErrorResponse } from '../../mocks/do-not-move.mocks';
import { of, BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';
import { ApiService, EnvironmentService, UserService, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { environmentServiceServiceStub } from '@modules/guest-relations/mocks/guest-relations.mock';

export class MockArrivalService {
  public dnmReasonUnsavedData = false;
  public dnmReasonData = '';
  public taskChangeSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public hasUnsavedTask = false;

  public discardUnsavedDoNotMoveReason() {}
  public clearUnsavedDnsReason() {}
  public setGenericUnsavedDataChange(value: boolean) {}
  getUnsavedTask(): boolean {
    return this.hasUnsavedTask;
  }
}

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) {
  }
}

describe('DnmReasonComponent', () => {
  let component: DnmReasonComponent;
  let fixture: ComponentFixture<DnmReasonComponent>;
  let service: DoNotMoveService;

  function getControl(controlName: string): AbstractControl {
    return component.dnmReasonFormGroup.get(controlName);
  }

  function getSaveBtn(): DebugElement {
    return fixture.debugElement.query(By.css('button[type=submit]'));
  }

  beforeEach(async(() => {
    TestBed
    .overrideProvider(EnvironmentService, {
      useValue: environmentServiceServiceStub
    }).configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, CommonTestModule],
      declarations: [DnmReasonComponent],
      providers: [
        DoNotMoveService,
        {
          provide: ArrivalsService,
          useClass: MockArrivalService
        }, {
            provide: GoogleAnalyticsService,
            useClass: MockGoogleAnalyticsService
        },
        ApiService, EnvironmentService, UserService
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.get(DoNotMoveService);
    fixture = TestBed.createComponent(DnmReasonComponent);
    component = fixture.componentInstance;
    component.dnmState = true;
    component.manualInteraction = true;
    fixture.detectChanges();
  });

  it('should NOT render the reason fields when DNM is turned OFF', () => {
    component.dnmState = false;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('.dnm-reason'));
    expect(el).toBeNull();
  });

  it('should render the reason fields when DNM is turned ON manually', () => {
    component.dnmState = true;
    component.manualInteraction = true;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('.dnm-reason'));
    expect(el).toBeDefined();
  });

  it('Save button should be disabled when the input field is empty', () => {
    getControl('dnmReasonInput').setValue('');
    component.dnmReasonFormGroup.updateValueAndValidity();
    fixture.detectChanges();
    expect(getSaveBtn().nativeElement.disabled).toEqual(true);
  });

  it('Save button should be disabled when the input field has only blank spaces', () => {
    getControl('dnmReasonInput').setValue('    ');
    component.dnmReasonFormGroup.updateValueAndValidity();
    fixture.detectChanges();
    expect(getSaveBtn().nativeElement.disabled).toEqual(true);
  });

  it('Save button should be enabled when the input field is NOT empty', () => {
    getControl('dnmReasonInput').setValue('Test reason');
    fixture.detectChanges();
    expect(getSaveBtn().nativeElement.disabled).toEqual(false);
  });

  it('the input field should allow max 150 characters', () => {
    const str = 'a'.repeat(200);
    const input: HTMLInputElement = fixture.debugElement.query(By.css('#dnmReasonInput')).nativeElement;
    getControl('dnmReasonInput').setValue(str);
    fixture.detectChanges();

    expect(input.maxLength).toEqual(150);
    expect(getControl('dnmReasonInput').valid).toEqual(false);
    expect(getSaveBtn().nativeElement.disabled).toEqual(true);
  });

  it('should NOT render the reason fields when DNM is ON but it is not turned on manually', () => {
    component.dnmState = true;
    component.manualInteraction = false;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('.dnm-reason'));
    expect(el).toBeNull();
  });

  it('should reset input field if DNM is turned off', () => {
    const changes: SimpleChanges = {
      dnmState: {
        currentValue: false,
        firstChange: false,
        isFirstChange: () => false,
        previousValue: null
      }
    };
    const resetSpy = spyOn(component.dnmReasonFormGroup, 'reset').and.callThrough();
    getControl('dnmReasonInput').setValue('Test Reason');
    component.ngOnChanges(changes);
    expect(resetSpy).toHaveBeenCalled();
  });

  it('should call service method with proper argument when the reason is submitted', () => {
    const subscriptionAddSpy = spyOn(component['rootSubscription'], 'add');
    const addReasonSpy = spyOn(service, 'addDoNotMoveReason').and.returnValue(of(mockAddReasonResponse));
    component.reservationNumber = '123456';
    getControl('dnmReasonInput').setValue('Test Reason');
    component.onSubmit();
    fixture.detectChanges();
    expect(subscriptionAddSpy).toHaveBeenCalled();
    expect(addReasonSpy).toHaveBeenCalledWith('123456', 'Test Reason');
  });

  it('should hide and reset reason section on successful save and should emit the reason', () => {
    const resetSpy = spyOn(component.dnmReasonFormGroup, 'reset').and.callThrough();
    const emitSpy = spyOn(component.reasonAdded, 'emit');
    spyOn(service, 'addDoNotMoveReason').and.returnValue(of(mockAddReasonResponse));
    component.onSubmit();
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('.dnm-reason'));
    expect(el).toBeNull();
    expect(resetSpy).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalledWith(mockDnmReason);
  });

  it('should destroy the registered subscription on component destroy', () => {
    const unsubscribeSpy = spyOn(component['rootSubscription'], 'unsubscribe').and.callThrough();
    component.ngOnDestroy();
    expect(unsubscribeSpy).toHaveBeenCalled();
  });

  it('should check availability of comment / reason in the response', () => {
    const successResponse = { ...mockAddReasonResponse };
    const successReturnVal = component['isCommentUnavailable'](successResponse);
    expect(successReturnVal).toEqual(false);

    const errorResponse = { ...mockAddReasonErrorResponse };
    const errorReturnVal = component['isCommentUnavailable'](errorResponse);
    expect(errorReturnVal).toEqual(true);
  });

  it('should check "rePopulateOldValue" method is called if dnm reason has unsaved changes', () => {
    const rePopulateOldValueSpy = spyOn(component, 'rePopulateOldValue');
    const arrivalServ = TestBed.get(ArrivalsService);
    arrivalServ.dnmReasonUnsavedData = true;
    component.ngOnInit();
    fixture.detectChanges();
    expect(rePopulateOldValueSpy).toHaveBeenCalled();
    expect(component.dnmState).toEqual(true);
    expect(component.manualInteraction).toEqual(true);
  });

  it('should check "rePopulateOldValue" is not called if dnm reason does not have unsaved changes', () => {
    const rePopulateOldValueSpy = spyOn(component, 'rePopulateOldValue');
    const arrivalServ = TestBed.get(ArrivalsService);
    arrivalServ.dnmReasonUnsavedData = false;
    component.ngOnInit();
    fixture.detectChanges();
    expect(rePopulateOldValueSpy).not.toHaveBeenCalled();
  });

  it('verify "onDnmReasonInputValueChange" when input value is not blank and user is visiting check in page', () => {
    const arrivalServ = TestBed.get(ArrivalsService);
    arrivalServ.ifCheckInPage = true;
    const setGenericUnsavedDataChangeSpy = spyOn(arrivalServ, 'setGenericUnsavedDataChange');
    const input = fixture.debugElement.query(By.css('#dnmReasonInput'));
    input.nativeElement.value = 'Test Reason';
    input.nativeElement.dispatchEvent(new Event('input'));
    expect(setGenericUnsavedDataChangeSpy).toHaveBeenCalledWith(true);
  });

  it('verify "onDnmReasonInputValueChange" when input value is not blank and user is visiting prepare tab', () => {
    const arrivalServ = TestBed.get(ArrivalsService);
    arrivalServ.ifCheckInPage = false;
    const input = fixture.debugElement.query(By.css('#dnmReasonInput'));
    input.nativeElement.value = 'Test Reason';
    input.nativeElement.dispatchEvent(new Event('input'));
    arrivalServ.taskChangeSubject.subscribe((changed: boolean) => {
      expect(changed).toEqual(true);
    });
  });

  it('verify "onDnmReasonInputValueChange" when input value is blank and user is visiting check in page', () => {
    const arrivalServ = TestBed.get(ArrivalsService);
    arrivalServ.ifCheckInPage = true;
    const discardUnsavedDoNotMoveReasonSpy = spyOn(arrivalServ, 'discardUnsavedDoNotMoveReason');
    const input = fixture.debugElement.query(By.css('#dnmReasonInput'));
    input.nativeElement.value = '';
    input.nativeElement.dispatchEvent(new Event('input'));
    expect(discardUnsavedDoNotMoveReasonSpy).toHaveBeenCalled();
  });

  it('verify "onDnmReasonInputValueChange" when input value is blank and user is visiting prepare tab', () => {
    const arrivalServ = TestBed.get(ArrivalsService);
    arrivalServ.ifCheckInPage = false;
    const input = fixture.debugElement.query(By.css('#dnmReasonInput'));
    input.nativeElement.value = '';
    input.nativeElement.dispatchEvent(new Event('input'));
    arrivalServ.taskChangeSubject.subscribe((changed: boolean) => {
      expect(changed).toEqual(false);
    });
  });

  it(' On failure reason save should emit the reason', () => {
    const emitSpy = spyOn(component.reasonAddError, 'emit');
    spyOn(service, 'addDoNotMoveReason').and.returnValue(throwError({error:"test"}));
    component.onSubmit();
    fixture.detectChanges();
    expect(emitSpy).toHaveBeenCalled();
  });

  it('On triggering retry action should call addDoNotReason', () => {
    const subscriptionAddSpy = spyOn(component['rootSubscription'], 'add');
    const addReasonSpy = spyOn(service, 'addDoNotMoveReason').and.returnValue(of(mockAddReasonResponse));
    component.reservationNumber = '123456';
    getControl('dnmReasonInput').setValue('Test Reason');
    service.retryAddReason.next(true);
    fixture.detectChanges();
    expect(subscriptionAddSpy).toHaveBeenCalled();
    expect(addReasonSpy).toHaveBeenCalledWith('123456', 'Test Reason');
  });

});
