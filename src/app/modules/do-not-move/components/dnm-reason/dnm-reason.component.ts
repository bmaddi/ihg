import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { FdkValidators } from '@app/modules/shared/validators/app.validators';
import { DoNotMoveService } from '../../services/do-not-move.service';
import { DNMCommentsModel, CommentModel } from '../../models/do-not-move.models';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { DoNotMoveReasonAnalytics } from '../../constants/do-not-move.constants';

@Component({
  selector: 'app-dnm-reason',
  templateUrl: './dnm-reason.component.html',
  styleUrls: ['./dnm-reason.component.scss']
})
export class DnmReasonComponent implements OnInit, OnChanges, OnDestroy {

  public hasUnsavedDnmReason = false;
  public dnmReasonFormGroup: FormGroup;
  private rootSubscription = new Subscription();

  @Input() dnmState = false;
  @Input() stateName = '';
  @Input() manualInteraction = false;
  @Input() reservationNumber: string;
  @Output() reasonAdded = new EventEmitter<CommentModel>();
  @Output() reasonAddError = new EventEmitter();


  constructor(private fb: FormBuilder, private doNotMoveService: DoNotMoveService, 
    private arrivalsService: ArrivalsService,
    private gaService: GoogleAnalyticsService) { }

  ngOnInit() {
    this.createForm();
    this.rootSubscription.add(this.doNotMoveService.retryAddReason.asObservable().subscribe(val => {
      if (val === true) {
        this.onSubmit();
      }
    }));
    this.resetDnmState();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.dnmState && !changes.dnmState.currentValue && !changes.dnmState.firstChange) {
      this.dnmReasonFormGroup.reset();
    }
  }

  resetDnmState() {
    this.rootSubscription.add(
      this.arrivalsService.taskChangeSubject.subscribe((changedVal: boolean) => {
        if(!changedVal && !this.arrivalsService.ifCheckInPage && this.arrivalsService.dnmReasonData) {
          this.dnmState = false;
        }
      })
    )
  }

  createForm(): void {
    this.dnmReasonFormGroup = this.fb.group({
      dnmReasonInput: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(150),
        FdkValidators.EmptySpaceValidation]
      ]
    });
    if(this.arrivalsService.dnmReasonUnsavedData) {
      this.rePopulateOldValue();
    }
    this.onDnmReasonInputValueChange();
  }

  rePopulateOldValue() {
    this.dnmState = true;
    this.manualInteraction = true;
    this.dnmReasonFormGroup.get('dnmReasonInput').patchValue(this.arrivalsService.dnmReasonData, { emitEvent: true });
  }

  onDnmReasonInputValueChange() {
    this.dnmReasonFormGroup.get('dnmReasonInput').valueChanges.subscribe((data) => {
      if (data && (this.dnmReasonFormGroup.dirty || this.dnmReasonFormGroup.touched)) {
        if(this.arrivalsService.ifCheckInPage) {
          this.arrivalsService.setGenericUnsavedDataChange(true);
        } else {
          this.arrivalsService.taskChangeSubject.next(true);
        }
        this.arrivalsService.dnmReasonUnsavedData = true;
        this.arrivalsService.dnmReasonData = data;
      } else {
        this.unsetDnmReason();
      }
    });
  }

  unsetDnmReason() {
    if(!this.arrivalsService.ifCheckInPage) {
      this.unsetTaskSubject();
      this.arrivalsService.clearUnsavedDnsReason();
    } else {
      this.arrivalsService.discardUnsavedDoNotMoveReason();
    }
  }

  unsetTaskSubject() {
    if(!this.arrivalsService.getUnsavedTask()) {
      this.arrivalsService.taskChangeSubject.next(false);
    }
  }

  onSubmit(): void {
    this.rootSubscription.add(
      this.doNotMoveService.addDoNotMoveReason(this.reservationNumber, this.dnmReasonFormGroup.get('dnmReasonInput').value)
        .subscribe(
          (response: DNMCommentsModel) => this.handleResponse(response),
          (error) => this.handleError())
    );
  }

  private handleResponse(response: DNMCommentsModel): void {
    if (this.isCommentUnavailable(response)) {
      this.handleError();
    } else {
      this.handleSuccess(response);
    }
  }

  private handleSuccess(response: DNMCommentsModel): void {
    this.dnmState = false;
    this.dnmReasonFormGroup.reset();
    this.reasonAdded.emit(response.doNotMoveRoomComments[0]);
    if(this.arrivalsService.ifCheckInPage) {
      this.arrivalsService.discardUnsavedDoNotMoveReason();
    } else {
      this.unsetTaskSubject();
      this.arrivalsService.clearUnsavedDnsReason();
    }    
     this.addGAAnalytics(DoNotMoveReasonAnalytics.AddReason.label)
  }

  private handleError(): void {
    this.reasonAddError.emit(this.arrivalsService.ifCheckInPage);
    this.addGAAnalytics(DoNotMoveReasonAnalytics.AddReason.labelError) 
  }

  private addGAAnalytics(label: string){
    this.gaService.trackEvent(
      this.stateName ==  DoNotMoveReasonAnalytics.checkInState? DoNotMoveReasonAnalytics.AddReason.checkInCategory: DoNotMoveReasonAnalytics.AddReason.category, 
      DoNotMoveReasonAnalytics.AddReason.action, label);
  }

  private isCommentUnavailable(response: DNMCommentsModel): boolean {
    return !!(response && response.errors) || (response && !response.doNotMoveRoomComments.length);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
