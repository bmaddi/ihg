import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { By } from '@angular/platform-browser';
import { HttpErrorResponse } from '@angular/common/http';
import { of, Observable, throwError } from 'rxjs';

import {
  ApiService,
  DetachedToastMessageService,
  EnvironmentService,
  UserService,
  ToastMessageOutletService,
  GoogleAnalyticsService,
  AlertButton,
  ApiErrorService
} from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { DoNotMoveComponent } from './do-not-move.component';
import { SERVICE_ERROR_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ToggleSwitchComponent } from '@app-shared/components/toggle-switch/toggle-switch.component';
import { mockEnvs, mockUser, googleAnalyticsStub } from '@app/constants/app-test-constants';
import { DoNotMoveService } from '../../services/do-not-move.service';
import { DoNotMoveAnalytics } from '@modules/do-not-move/constants/do-not-move.constants';
import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';
import { DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { Input, Component, Output, EventEmitter, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommentModel } from '../../models/do-not-move.models';
import { mockDnmReason } from '../../mocks/do-not-move.mocks';

@Component({
  selector: 'app-dnm-reason',
  template: '<div></div>'
})
export class MockDnmReasonComponent {
  @Input() manualInteraction: boolean;
  @Input() dnmState: boolean;
  @Input() reservationNumber: string;
  @Output() reasonAdded = new EventEmitter<CommentModel>();
}

describe('DoNotMoveComponent', () => {
  let component: DoNotMoveComponent;
  let fixture: ComponentFixture<DoNotMoveComponent>;
  let counterService: ApiErrorService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(DetachedToastMessageService, {
        useValue: {
          success: (s1, s2): void => {
          }
        }
      })
      .overrideProvider(ReportIssueService, {
        useValue: { openAutofilledReportIssueModal: (t): Observable<string> => of(t) }
      })
      .overrideProvider(ToastMessageOutletService, {
        useValue: {
          danger: (message?: string,
            detailMessage?: string,
            buttons?: AlertButton[],
            clearPreviousAlerts?: boolean,
            dismissible?: boolean,
            keepAfterRouteChange?: boolean): void => {
          },
          clear: (): void => {
          }
        }
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        declarations: [DoNotMoveComponent, ToggleSwitchComponent, MockDnmReasonComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          DoNotMoveService,
          TranslateStore,
          TranslateService,
          ApiService,
          ApiErrorService,
          EnvironmentService,
          UserService,
          DetachedToastMessageService,
          ToastMessageOutletService,
          ReportIssueService,
          GoogleAnalyticsService
        ],
        imports: [TranslateModule.forChild(), FormsModule, JWBootstrapSwitchModule, HttpClientTestingModule]
      })
      .compileComponents();
    const environmentService = TestBed.get(EnvironmentService);
    const userService = TestBed.get(UserService);
    environmentService.setEnvironmentConstants(mockEnvs);
    userService.setUser(mockUser);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoNotMoveComponent);
    component = fixture.componentInstance;
    counterService = fixture.componentRef.injector.get(ApiErrorService);
    fixture.detectChanges();
  });

  xit('should create with default inputs', () => {
    expect(component).toBeTruthy();
    expect(component.doNotMoveState).not.toBeDefined();
    expect(component.reservationNumber).not.toBeDefined();
    expect(component.pmsUniqueNumber).not.toBeDefined();
    expect(component.disabled).toBeFalsy();
    expect(component.showToggle).toBeFalsy();
    expect(component.label).toEqual('LBL_DO_NOT_MOVE');
    expect(component.seleniumTag).toEqual('DoNotMove');
  });

  it('should only show Do Not Move toggle if doNotMoveState boolean is not undefined or null & access role condition is met', () => {

    component.ngOnInit();
    fixture.detectChanges();
    expect(component.doNotMoveState).not.toBeDefined();

    component.doNotMoveState = false;
    const accessSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);
    component.ngOnInit();
    fixture.detectChanges();
    expect(accessSpy).toHaveBeenCalled();
    expect(component.showToggle).toBeTruthy();
  });

  it('should NOT show Do Not Move toggle if either doNotMoveState boolean is undefined/null or if access role condition is not met', () => {

    component.ngOnInit();
    fixture.detectChanges();
    expect(component.doNotMoveState).not.toBeDefined();

    component.ngOnInit();
    fixture.detectChanges();
    const toggleChangeSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);
    expect(component.showToggle).toBeFalsy();
  });

  it('should show toggle and update parent on toggle click', () => {
    const toggleChangeSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);
    const updateDoNotMoveSpy = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove').and.returnValue(of(true));
    component.doNotMoveState = false;
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.showToggle).toBeTruthy();
    const updateStateSpy = spyOn(component, 'updateState').and.callThrough();
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMove-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveToggle-SID"]'));

    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_DO_NOT_MOVE');
    expect(toggle).not.toBeNull();

    toggle.triggerEventHandler('click', null);

    fixture.detectChanges();
    expect(updateStateSpy).toBeDefined();
    expect(component.doNotMoveState).toBeTruthy();
  });

  it('should turn toggle OFF, update parent on click, invoke API successfully, track google analytics and display toast', () => {
    const accessSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);
    const updateDoNotMoveSpy = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove').and.returnValue(of(true));
    component.doNotMoveState = true;
    component.reservationNumber = '11111111';
    component.pmsUniqueNumber = '222222';
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.showToggle).toBeTruthy();
    expect(accessSpy).toHaveBeenCalled();
    const updateNoToggleStatusSpy = spyOn<any>(component, 'updateState').and.callThrough();
    const handleSuccessfulUpdateSpy = spyOn<any>(component, 'handleSuccessfulUpdate').and.callThrough();
    const trackGoogleAnalyticsSpy = spyOn<any>(component, 'trackGoogleAnalytics').and.callThrough();
    const googleAnalyticsSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent');
    const toastServiceSpy = spyOn(TestBed.get(DetachedToastMessageService), 'success');

    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMove-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveToggle-SID"]'));
    const stateName = 'check-in';
    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_DO_NOT_MOVE');
    expect(toggle).not.toBeNull();

    toggle.triggerEventHandler('click', null);
    component.updateState(false);
    fixture.detectChanges();

    expect(updateDoNotMoveSpy).toHaveBeenCalled();
    expect(updateNoToggleStatusSpy).toHaveBeenCalled();

    component['trackGoogleAnalytics'](DoNotMoveAnalytics.labelOff);
    fixture.detectChanges();
    expect(component.doNotMoveState).toBeFalsy();
    expect(toastServiceSpy).toHaveBeenCalledWith('LBL_NO_NOT_MOVE_OFF', 'LBL_NO_NOT_MOVE_OFF_MSG');
    expect(trackGoogleAnalyticsSpy).toHaveBeenCalledWith(DoNotMoveAnalytics.labelOff);
    expect(googleAnalyticsSpy).toHaveBeenCalledWith(DoNotMoveAnalytics.category[stateName],
      DoNotMoveAnalytics.action, DoNotMoveAnalytics.labelOff);
  });

  it('should turn toggle ON, update parent on click, invoke API successfully, track google analytics and display toast', () => {
    const accessSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);
    const updateDoNotMoveSpy = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove').and.returnValue(of(true));
    component.doNotMoveState = false;
    component.reservationNumber = '11111111';
    component.pmsUniqueNumber = '222222';
    component.stateName = 'guest-list-details';
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.showToggle).toBeTruthy();
    expect(accessSpy).toHaveBeenCalled();
    const updateNoToggleStatusSpy = spyOn<any>(component, 'updateState').and.callThrough();
    const handleSuccessfulUpdateSpy = spyOn<any>(component, 'handleSuccessfulUpdate').and.callThrough();
    const trackGoogleAnalyticsSpy = spyOn<any>(component, 'trackGoogleAnalytics').and.callThrough();
    const googleAnalyticsSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent');
    const toastServiceSpy = spyOn(TestBed.get(DetachedToastMessageService), 'success');

    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMove-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveToggle-SID"]'));
    const stateName = 'guest-list-details';
    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_DO_NOT_MOVE');
    expect(toggle).not.toBeNull();

    toggle.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(updateNoToggleStatusSpy).toHaveBeenCalled();
    expect(handleSuccessfulUpdateSpy).toHaveBeenCalledWith(true);

    component['trackGoogleAnalytics'](DoNotMoveAnalytics.labelOn);
    fixture.detectChanges();
    expect(toastServiceSpy).toHaveBeenCalledWith('LBL_NO_NOT_MOVE_ON', 'LBL_NO_NOT_MOVE_ON_MSG');
    expect(trackGoogleAnalyticsSpy).toHaveBeenCalledWith(DoNotMoveAnalytics.labelOn);
    expect(googleAnalyticsSpy).toHaveBeenCalledWith(DoNotMoveAnalytics.category[stateName],
      DoNotMoveAnalytics.action, DoNotMoveAnalytics.labelOn);
    expect(component.doNotMoveState).toBeTruthy();
  });

  xit('should show toggle and update parent on toggle click and make API call to update and reset toggle if API call fails', () => {
    const accessSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);
    component.doNotMoveState = true;
    component.reservationNumber = '11111111';
    component.pmsUniqueNumber = '222222';
    component.stateName = 'guest-list-details';
    component.ngOnInit();
    fixture.detectChanges();
    expect(accessSpy).toHaveBeenCalled();
    expect(component.showToggle).toBeTruthy();
    const updateStateSpy = spyOn(component, 'updateState').and.callThrough();
    const updateDoNotMoveStatusSpy = spyOn<any>(component, 'updateDoNotMoveStatus').and.callThrough();
    const updateDoNotMoveServiceSpy = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove')
      .and.returnValue(throwError({ status: 500, statusText: 'Failure' }));
    const handleUpdateFailureSpy = spyOn<any>(component, 'handleUpdateFailure').and.callThrough();
    const trackGoogleAnalyticsSpy = spyOn<any>(component, 'trackGoogleAnalytics').and.callThrough();
    const googleAnalyticsSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent');
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveToggle-SID"]'));
    const stateName = 'guest-list-details';
    expect(toggle).not.toBeNull();
    toggle.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(updateStateSpy).toHaveBeenCalled();

    fixture.detectChanges();
    expect(updateDoNotMoveStatusSpy).toHaveBeenCalled();
    expect(updateDoNotMoveServiceSpy).toHaveBeenCalled();
    expect(handleUpdateFailureSpy).toHaveBeenCalled();

    component['trackGoogleAnalytics'](DoNotMoveAnalytics.labelError);
    fixture.detectChanges();
    expect(trackGoogleAnalyticsSpy).toHaveBeenCalledWith(DoNotMoveAnalytics.labelError);
    expect(googleAnalyticsSpy).toHaveBeenCalledWith(DoNotMoveAnalytics.category[stateName],
      DoNotMoveAnalytics.action, DoNotMoveAnalytics.labelError);
  });

  it('should make API call with correct parameters including next toggle state not current', () => {
    const accessSpy = spyOn(TestBed.get(DoNotMoveService), 'hasDoNotMoveAccess').and.returnValue(true);

    const updateDoNotMoveServiceSpy = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove')
      .and.returnValue(of());

    component.doNotMoveState = true;
    component.reservationNumber = '11111111';
    component.pmsUniqueNumber = '222222';
    component.ngOnInit();
    fixture.detectChanges();
    expect(accessSpy).toHaveBeenCalled();
    expect(component.showToggle).toBeTruthy();
    component.updateState(false);
    fixture.detectChanges();
    expect(updateDoNotMoveServiceSpy).toHaveBeenCalledWith({ doNotMoveRoom: false, resNum: '11111111', pmsUniqueNumber: '222222' });
  });

    it('should set proper Report An Issue Autofill depending on state', () => {
    const timeoutErrorType = SERVICE_ERROR_TYPE['0'];
    const generalErrorType = SERVICE_ERROR_TYPE['2'];

    const checkInAutoFill = component['setProperAutoFill'](timeoutErrorType);
    const checkInAutoFillGeneral = component['setProperAutoFill'](generalErrorType);

    expect(checkInAutoFill).toEqual(DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL[timeoutErrorType]);
    expect(checkInAutoFillGeneral).toEqual(DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL[generalErrorType]);

    component.stateName = 'check-in';

    fixture.detectChanges();

    const stayAutoFill = component['setProperAutoFill'](timeoutErrorType);
    const stayAutoFillGeneral = component['setProperAutoFill'](generalErrorType);

    expect(stayAutoFill).toEqual(DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL[timeoutErrorType]);
    expect(stayAutoFillGeneral).toEqual(DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL[generalErrorType]);
  });

  it('should properly handle object path autofill null or undefined scenarios for stateName & errorType', () => {
    const timeoutErrorType = SERVICE_ERROR_TYPE['0'];
    const generalErrorType = SERVICE_ERROR_TYPE['2'];

    component.stateName = null;
    fixture.detectChanges();

    expect(component['setProperAutoFill'](timeoutErrorType)).toBeNull();
    expect(component['setProperAutoFill'](generalErrorType)).toBeNull();
    expect(component['setProperAutoFill'](null)).toBeNull();

    component.stateName = 'check-in';
    fixture.detectChanges();

    expect(component['setProperAutoFill'](null)).toBeNull();
  });

  it('should emit proper value when reason is saved', () => {
    const reason: CommentModel = { ...mockDnmReason };
    const reasonEmitSpy = spyOn(component.reasonAdded, 'emit');
    component.handleReasonAdded(reason);
    fixture.detectChanges();

    expect(reasonEmitSpy).toHaveBeenCalledWith(reason);
  });

  it('should emit when calls showErrorMessage', () => {
    const dnmSwitchSpy = spyOn(component.dnmSwitchError, 'emit');
    component['showErrorMessage'](new HttpErrorResponse({status: 400, statusText: 'Failure'}));
    fixture.detectChanges();
    expect(dnmSwitchSpy).toHaveBeenCalled();
  });

});
