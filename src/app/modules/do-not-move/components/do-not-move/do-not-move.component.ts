import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { get, isNil } from 'lodash';
import { Subscription } from 'rxjs';

import {
  AlertButtonType, DetachedToastMessageService, ApiErrorService, ToastMessageOutletService, AlertButton, GoogleAnalyticsService
} from 'ihg-ng-common-core';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { DoNotMoveService } from '../../services/do-not-move.service';
import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { HttpErrorResponse } from '@angular/common/http';
import { SERVICE_ERROR_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';

import { doNotMove, DoNotMoveAnalytics } from '../../constants/do-not-move.constants';
import { CommentModel } from '../../models/do-not-move.models';

@Component({
  selector: 'app-do-not-move',
  templateUrl: './do-not-move.component.html',
  styleUrls: ['./do-not-move.component.scss'],
  providers: [ApiErrorService]
})
export class DoNotMoveComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @Input() doNotMoveState: boolean;
  @Input() reservationNumber: string;
  @Input() pmsUniqueNumber: string;
  @Input() disabled = false;
  @Input() stateName = 'check-in';
  @Input() dnmReason: CommentModel;
  @Output() toggleChange: EventEmitter<boolean> = new EventEmitter();
  @Output() interaction = new EventEmitter<boolean>();
  @Output() reasonAdded = new EventEmitter<CommentModel>();
  @Output() reasonAddedError = new EventEmitter();
  @Output() dnmSwitchError = new EventEmitter();

  showToggle = true;
  label = 'LBL_DO_NOT_MOVE';
  seleniumTag = 'DoNotMove';
  manualInteraction = false;
  public serviceErrorType: string;

  private doNotMove$: Subscription = new Subscription();

  constructor(
    private apiErrorService: ApiErrorService,
    private appErrorsService: AppErrorsService,
    private doNotMoveService: DoNotMoveService,
    private toastMessageService: DetachedToastMessageService,
    private staticToastService: ToastMessageOutletService,
    private gaService: GoogleAnalyticsService
  ) {
    super();
  }

  ngOnInit() {
    this.showToggle = !isNil(this.doNotMoveState) && this.doNotMoveService.hasDoNotMoveAccess();
  }

  ngOnDestroy() {
    this.doNotMove$.unsubscribe();
  }

  updateState(state: boolean) {
    if (state) {
      this.manualInteraction = true;
      this.interaction.emit(this.manualInteraction);
    }
    this.updateDoNotMoveStatus(state);
  }

  private updateDoNotMoveStatus(state: boolean) {
    this.doNotMove$.add(
      this.doNotMoveService.updateDoNotMove({
        doNotMoveRoom: state,
        resNum: this.reservationNumber,
        pmsUniqueNumber: this.pmsUniqueNumber
      })
        .subscribe(
          () => this.handleSuccessfulUpdate(state),
          error => this.handleUpdateFailure(error))
    );
  }

  private handleSuccessfulUpdate(state: boolean) {
    this.doNotMoveState = state;
    this.toastMessageService.success(this.doNotMoveState ? 'LBL_NO_NOT_MOVE_ON' : 'LBL_NO_NOT_MOVE_OFF',
      this.doNotMoveState ? 'LBL_NO_NOT_MOVE_ON_MSG' : 'LBL_NO_NOT_MOVE_OFF_MSG');
    this.toggleChange.emit(this.doNotMoveState);
    this.trackGoogleAnalytics(this.doNotMoveState ? DoNotMoveAnalytics.labelOn : DoNotMoveAnalytics.labelOff);
  }

  private handleUpdateFailure(error: HttpErrorResponse) {    
    super.processHttpErrors(error);    
    const serviceError = this.appErrorsService.getErrorType(this.emitError.getValue());
    this.serviceErrorType = SERVICE_ERROR_TYPE[serviceError];
    this.showErrorMessage(error);
    this.trackGoogleAnalytics(DoNotMoveAnalytics.labelError);
  }

  private showErrorMessage(error: HttpErrorResponse) {
    let dnmSwitchError= { error : error, reportFillData:this.setProperAutoFill(this.serviceErrorType)}
    this.dnmSwitchError.emit(dnmSwitchError);
  }

  public  handleTryAgain() {
    this.updateState(!this.doNotMoveState);
  }

  private setProperAutoFill(errorType: string): ReportIssueAutoFillData {
    return get(doNotMove, [this.stateName, errorType], null);
  }

  private trackGoogleAnalytics(label: string): void {
    const category = DoNotMoveAnalytics.category[this.stateName];
    const action = DoNotMoveAnalytics.action;
    this.gaService.trackEvent(category, action, label);
  }

  handleReasonAdded(reason: CommentModel): void {
    this.reasonAdded.emit(reason);
  }

  handleReasonAddedError($event): void {    
    this.reasonAddedError.emit($event);
  }

  retrySaveReason(){
    this.doNotMoveService.retryAddReason.next(true);
  }

}
