import { RoleData } from 'ihg-ng-common-core';
import { find, some } from 'lodash';

export const accessRolesForDoNotMove: RoleData[] = [{
  id: 5004,
  name: 'FRONT_DESK_MANAGEMENT',
  description: 'FRONT_DESK_MANAGEMENT',
  functions: null
}];

// tslint:disable-next-line:max-line-length
export const hasDoNotPostAccessRoles = (roles: RoleData[]): boolean => some(accessRolesForDoNotMove, (role: RoleData) => find(roles, r => r.id === role.id || r.name === role.name));

import { DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL, DO_NOT_MOVE_PREPARE_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';

export const doNotMove = {
  ['check-in']: DO_NOT_MOVE_CHECK_IN_REPORT_ISSUE_AUTOFILL,
  ['guest-list-details']: DO_NOT_MOVE_PREPARE_REPORT_ISSUE_AUTOFILL
};

export const DoNotMoveAnalytics = {
  category: {
    'guest-list-details': 'Guest List - Prepare',
    'check-in': 'Check In'
  },
  action: 'Room Assignment Expanded',
  labelOn: 'Turn Do Not Move ON',
  labelOff: 'Turn Do Not Move OFF',
  labelError: 'Error Changing Do Not Move'
};

export const DoNotMoveReasonAnalytics = {
  checkInState:  'check-in',
  AddReason : {
    category:'Guest List - Prepare',
    action : 'Room Assignment Expanded',
    checkInCategory : 'Check In',
    label:'Do Not Move Reason Saved',
    labelError : 'Error Saving Do Not Move Reason'
  }
}
export const blankReason = 'None Provided';
