import { DoNotMoveModule } from './do-not-move.module';

describe('DoNotMoveModule', () => {
  let doNotMoveModule: DoNotMoveModule;

  beforeEach(() => {
    doNotMoveModule = new DoNotMoveModule();
  });

  it('should create an instance', () => {
    expect(doNotMoveModule).toBeTruthy();
  });
});
