import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppSharedModule } from '@app-shared/app-shared.module';
import { DoNotMoveComponent } from './components/do-not-move/do-not-move.component';
import { DnmReasonComponent } from './components/dnm-reason/dnm-reason.component';
import { DnmReasonTooltipComponent } from './components/dnm-reason-tooltip/dnm-reason-tooltip.component';
import { DnmReasonTooltipContentComponent } from './components/dnm-reason-tooltip-content/dnm-reason-tooltip-content.component';

@NgModule({
  imports: [
    CommonModule,
    AppSharedModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    NgbModule.forRoot()
  ],
  declarations: [DoNotMoveComponent, DnmReasonComponent, DnmReasonTooltipComponent, DnmReasonTooltipContentComponent],
  exports: [DoNotMoveComponent, DnmReasonTooltipComponent]
})
export class DoNotMoveModule {
}
