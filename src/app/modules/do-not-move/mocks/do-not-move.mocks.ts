import { CommentModel, DNMCommentsModel } from '../models/do-not-move.models';

export const mockDnmReason: CommentModel = {
  date: '11DEC2019 07:45:30',
  reason: 'Test Reason',
  userName: 'John Doe'
};

export const mockAddReasonResponse: DNMCommentsModel = {
  doNotMoveRoomComments: [mockDnmReason]
};

export const mockAddReasonErrorResponse: DNMCommentsModel = {
  doNotMoveRoomComments: [],
  errors: [{
    code: 'ERR-101',
    detailText: 'Reason cannot be saved'
  }]
}