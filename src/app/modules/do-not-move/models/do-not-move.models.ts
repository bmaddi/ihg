export interface CommentModel {
  userName: string;
  date?: string;
  reason: string;
};

export interface DNMCommentsModel {
  doNotMoveRoomComments: CommentModel[];
  errors?: [
    {
      code: string;
      detailText: string;
    }
  ];
};
