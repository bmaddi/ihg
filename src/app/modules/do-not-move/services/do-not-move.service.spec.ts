import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';

import { ApiService, EnvironmentService, RoleData, UserService } from 'ihg-ng-common-core';

import { DoNotMoveService } from './do-not-move.service';
import { mockEnvs, mockUser } from '@app/constants/app-test-constants';
import { hasDoNotPostAccessRoles } from '../constants/do-not-move.constants';

describe('DoNotMoveService', () => {
  let service: DoNotMoveService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        providers: [ApiService, EnvironmentService, UserService],
        imports: [HttpClientTestingModule]
      });

    const environmentService = TestBed.get(EnvironmentService);
    const userService = TestBed.get(UserService);
    environmentService.setEnvironmentConstants(mockEnvs);
    userService.setUser(mockUser);

    service = TestBed.get(DoNotMoveService);
    httpMockController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('hasDoNotMoveAccess should return False if access role is NOT assigned to the user', () => {
    expect(service.hasDoNotMoveAccess()).toBeFalsy();
  });

  it('hasDoNotMoveAccess should return True if access role is assigned to the user', () => {
    const getUserSpy = spyOn(TestBed.get(UserService), 'getUser').and.returnValue({
      'roles': [
        {
          id: 5004,
          name: 'FRONT_DESK_MANAGEMENT',
          description: 'FRONT_DESK_MANAGEMENT',
          functions: null
        }]
    });

    expect(service.hasDoNotMoveAccess()).toBeTruthy();
    expect(getUserSpy).toHaveBeenCalled();
  });

  it('hasDoNotMoveAccess should return False if access roles error occurs', () => {
    const getUserSpy = spyOn(TestBed.get(UserService), 'getUser').and.returnValue(undefined);

    expect(service.hasDoNotMoveAccess()).toBeFalsy();
    expect(getUserSpy).toHaveBeenCalled();
  });

  it('hasDoNotMoveAccessRoles should handle all scenarios properly', () => {
    expect(hasDoNotPostAccessRoles(null)).toBeFalsy();
    expect(hasDoNotPostAccessRoles([])).toBeFalsy();
    expect(hasDoNotPostAccessRoles(undefined)).toBeFalsy();
    expect(hasDoNotPostAccessRoles(<RoleData[]>[
      {
        id: 5004,
        name: 'FRONT_DESK_MANG',
        description: 'FRONT_DESK_MANG',
        functions: null
      }, {
        id: 500234,
        name: 'Test',
        description: 'Test',
      }])).toBeTruthy();
    expect(hasDoNotPostAccessRoles(<RoleData[]>[
      {
        id: 1002,
        name: 'FRONT_DESK_MANAGEMENT',
        description: 'FRONT_DESK_MANAGEMENT',
        functions: null
      }, {
        id: 500234,
        name: 'Test',
        description: 'Test',
      }])).toBeTruthy();
    expect(hasDoNotPostAccessRoles(<RoleData[]>[
      {
        id: 1002,
        name: 'FRONT_DESK_MANAGMENT',
        description: 'FRONT_DESK_MANAGMENT',
        functions: null
      }, {
        id: 500234,
        name: 'Test',
        description: 'Test',
      }])).toBeFalsy();
  });


  it('should return Observable void if successful', () => {
    service.updateDoNotMove({ doNotMoveRoom: true, resNum: '11111111', pmsUniqueNumber: '765528' })
      .subscribe(data => {
        expect(data).toBeDefined();
      }, () => {
        fail();
      });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'POST' && req.url.indexOf('booking/updateBookingInfo') !== -1;
    });

    expect(request.request.body['doNotMoveRoom']).toBeTruthy();
    expect(request.request.body['resNum']).toEqual('11111111');
    expect(request.request.body['pmsUniqueNumber']).toEqual('765528');
    expect(request.request.body['userName']).toEqual('John Doe');

    request.flush({});

    httpMockController.verify();
  });

  it('should catch error if any', () => {
    service.updateDoNotMove({ doNotMoveRoom: true, resNum: '11111111', pmsUniqueNumber: '765528' }).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toEqual(500);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'POST' && req.url.indexOf('booking/updateBookingInfo') !== -1;
    });

    expect(request.request.body['doNotMoveRoom']).toBeTruthy();
    expect(request.request.body['resNum']).toEqual('11111111');
    expect(request.request.body['pmsUniqueNumber']).toEqual('765528');
    expect(request.request.body['userName']).toEqual('John Doe');

    request.error(null, { status: 500, statusText: 'Bad Request' });

    httpMockController.verify();
  });

  it('#getUserName should return user name in "FirstName LastName" format', () => {
    const userName = service.getUserName();
    expect(userName).toEqual('John Doe');
  });

  it('#addDoNotMoveReason should call addComment endpoint with proper payload and handle success', () => {
    service.addDoNotMoveReason('123456', 'Test reason')
      .subscribe(data => {
        expect(data).toBeDefined();
      }, () => {
        fail();
      });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'POST' && req.url.indexOf('doNotMoveRoomComments/addComment') !== -1
        && req.url.indexOf('grsReservationNumber=123456') !== -1;
    });

    expect(request.request.body['doNotMoveRoomComments']).toEqual([
      { userName: 'John Doe', reason: 'Test reason' }]);

    request.flush({});
    httpMockController.verify();
  });

  it('#fetchComment should call fetchComments endpoint with proper query string and handle success', () => {
    service.fetchComment('123456')
      .subscribe(data => {
        expect(data).toBeDefined();
      }, () => {
        fail();
      });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'GET' && req.url.indexOf('doNotMoveRoomComments/fetchComments') !== -1
        && req.url.indexOf('grsReservationNumber=123456') !== -1;
    });

    request.flush({});
    httpMockController.verify();
  });

  it('#fetchComment should catch error if any', () => {
    service.fetchComment('123456')
      .subscribe(data => {
        fail();
      }, error => {
        expect(error).toBeDefined();
        expect(error.status).toEqual(500);
        expect(error.statusText).toEqual('Bad Request');
      });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'GET' && req.url.indexOf('doNotMoveRoomComments/fetchComments') !== -1
        && req.url.indexOf('grsReservationNumber=123456') !== -1;
    });

    request.error(null, { status: 500, statusText: 'Bad Request' });
    httpMockController.verify();
  });
});
