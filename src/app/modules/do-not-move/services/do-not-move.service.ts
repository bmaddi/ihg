import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { get } from 'lodash';

import { ApiService, ApiUrl, EnvironmentService, UserService } from 'ihg-ng-common-core';

import { hasDoNotPostAccessRoles } from '../constants/do-not-move.constants';
import { DNMCommentsModel } from '../models/do-not-move.models';

@Injectable({
  providedIn: 'root'
})
export class DoNotMoveService {
  
  public retryAddReason = new Subject<boolean>();
  constructor(private apiService: ApiService,
    private environmentService: EnvironmentService,
    private userService: UserService) {
  }

  updateDoNotMove(params: {
    doNotMoveRoom: boolean;
    resNum: string;
    pmsUniqueNumber: string;
  }): Observable<void> {
    const payload = { ...params, userName: this.getUserName() };
    return this.apiService.post(
      new ApiUrl(this.environmentService.getEnvironmentConstants()['fdkAPI'],
        `booking/updateBookingInfo/${this.userService.getCurrentLocationId()}`),
      payload,
      null,
      new HttpHeaders().set('spinnerConfig', 'Y')
    );
  }

  hasDoNotMoveAccess(): boolean {
    const roles = get(this.userService.getUser(), 'roles', []);
    return hasDoNotPostAccessRoles(roles);
  }

  addDoNotMoveReason(resNum: string, reason: string): Observable<DNMCommentsModel> {
    const payload: DNMCommentsModel = {
      doNotMoveRoomComments: [{ userName: this.getUserName(), reason: reason }]
    };
    return this.apiService.post(
      new ApiUrl(this.environmentService.getEnvironmentConstants()['fdkAPI'],
        `doNotMoveRoomComments/addComment/${this.userService.getCurrentLocationId()}?grsReservationNumber=${resNum}`),
      payload,
      null,
      new HttpHeaders().set('spinnerConfig', 'Y')
    );
  }

  fetchComment(resNum: string): Observable<DNMCommentsModel> {
    return this.apiService.get(
      new ApiUrl(this.environmentService.getEnvironmentConstants()['fdkAPI'],
        `doNotMoveRoomComments/fetchComments/${this.userService.getCurrentLocationId()}?grsReservationNumber=${resNum}`),      
      null
    );
  }

  getUserName(): string {
    return `${this.userService.getUser().firstName} ${this.userService.getUser().lastName}`;
  }
}
