import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EncoderSelectorComponent } from './encoder-selector.component';
import { CookieService } from 'ngx-cookie-service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { DropDownListComponent, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { MOCK_ENCODER_LIST } from '@modules/encoder/mocks/encoder-mock';
import { EncoderService } from '@modules/encoder/services/encoder.service';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';


describe('EncoderSelectorComponent', () => {
  let component: EncoderSelectorComponent;
  let fixture: ComponentFixture<EncoderSelectorComponent>;
  let cookieService: CookieService;
  let encoderService: EncoderService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EncoderSelectorComponent],
      imports: [CommonTestModule, DropDownsModule, FormsModule, HttpClientTestingModule],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(EncoderSelectorComponent);
    component = fixture.componentInstance;
    cookieService = TestBed.get(CookieService);
    encoderService = TestBed.get(EncoderService);
    fixture.detectChanges();
    component.encoderList = MOCK_ENCODER_LIST;
    cookieService.deleteAll();
    spyOn(encoderService, 'getEncoderList').and.returnValue(of(MOCK_ENCODER_LIST));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should obtain the correct encoder from the cookie service', () => {
    expect(component.findEncoderFromCookie()).toEqual(component.defaultEncoder);
    const encoderSelection = MOCK_ENCODER_LIST[1];

    cookieService.set(component.encoderCookieString, encoderSelection.encoderId);
    expect(component.findEncoderFromCookie()).toEqual(encoderSelection);

    cookieService.deleteAll();
    expect(component.findEncoderFromCookie()).toEqual(component.defaultEncoder);
  });

  it('should emit the selected encoder', () => {
    spyOn(component.encoder, 'emit');
    spyOn(cookieService, 'set');
    component.selectedEncoder = MOCK_ENCODER_LIST[2];

    component.encoderSelected();

    expect(component.encoder.emit).toHaveBeenCalledWith(component.selectedEncoder);
    expect(cookieService.set).toHaveBeenCalledWith(component.encoderCookieString, component.selectedEncoder.encoderId);
  });

  it('should check dropdown component is present', () => {
    const dropdownElement = fixture.debugElement.query(By.directive(DropDownListComponent));
    expect(dropdownElement).toBeDefined();
  });
});
