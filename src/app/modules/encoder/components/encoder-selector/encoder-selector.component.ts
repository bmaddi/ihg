import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {EncoderModel} from '@modules/encoder/models/encoder.model';
import {CookieService} from 'ngx-cookie-service';
import {TranslateService} from '@ngx-translate/core';
import {EncoderService} from '@modules/encoder/services/encoder.service';

@Component({
  selector: 'app-encoder-selector',
  templateUrl: './encoder-selector.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./encoder-selector.component.scss']
})
export class EncoderSelectorComponent implements OnInit {

  @Output() encoder = new EventEmitter();
  selectedEncoder: EncoderModel;
  encoderList: EncoderModel[];
  encoderCookieString = 'IHGEncoderID';
  defaultEncoder: EncoderModel = {
    encoderName: this.translateService.instant('LBL_SELECT_ENCODER'),
    encoderId: ''
  };

  constructor(private cookieService: CookieService,
              private translateService: TranslateService,
              private encoderService: EncoderService) { }

  ngOnInit() {
    this.encoderService.getEncoderList().subscribe((response: EncoderModel[]) => {
      this.encoderList = response;
      this.selectedEncoder = this.findEncoderFromCookie();
      if (this.selectedEncoder) {
        this.encoder.emit(this.selectedEncoder);
      }
    });
  }

  findEncoderFromCookie = (): EncoderModel =>
    this.cookieService.check(this.encoderCookieString) ?
      this.encoderList.filter(encoder => encoder.encoderId === this.cookieService.get(this.encoderCookieString))[0] : this.defaultEncoder;

  encoderSelected(): void {
    this.encoder.emit(this.selectedEncoder);
    this.cookieService.set(this.encoderCookieString, this.selectedEncoder.encoderId);
  }
}
