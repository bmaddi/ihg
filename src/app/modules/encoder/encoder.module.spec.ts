import { EncoderModule } from './encoder.module';

describe('EncoderSelectorModule', () => {
  let encoderSelectorModule: EncoderModule;

  beforeEach(() => {
    encoderSelectorModule = new EncoderModule();
  });

  it('should create an instance', () => {
    expect(encoderSelectorModule).toBeTruthy();
  });
});
