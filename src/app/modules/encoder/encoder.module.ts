import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EncoderSelectorComponent } from './components/encoder-selector/encoder-selector.component';
import {FormsModule} from '@angular/forms';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { TranslateModule } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DropDownsModule,
    TranslateModule,
  ],
  providers: [CookieService],
  declarations: [EncoderSelectorComponent],
  exports: [EncoderSelectorComponent]
})
export class EncoderModule { }
