import {EncoderModel} from '@modules/encoder/models/encoder.model';

export const MOCK_ENCODER_LIST: EncoderModel[] = [
  {
    encoderId: '362',
    encoderName: 'Encoder 1'
  },
  {
    encoderId: '34673467',
    encoderName: 'Encoder 2'
  },
  {
    encoderId: '842362354',
    encoderName: 'Encoder 3'
  }
];

export const MOCK_SELECTED_ENCODER = {
  encoderId: '362',
  encoderName: 'Encoder 1'
};
