export interface EncoderModel {
  encoderId: string;
  encoderName: string;
}
