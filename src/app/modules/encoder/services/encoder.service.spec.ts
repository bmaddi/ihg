import { TestBed } from '@angular/core/testing';
import { UserService } from 'ihg-ng-common-core';
import { EncoderService } from './encoder.service';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';
import {environment} from '@env/environment';
import {MOCK_ENCODER_LIST} from '@modules/encoder/mocks/encoder-mock';
import {HttpRequest} from '@angular/common/http';

describe('EncoderService', () => {
  let httpTestingController: HttpTestingController;
  let request: TestRequest;
  let service: EncoderService;
  let userService: UserService;
  let hotelCode: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(EncoderService);
    userService = TestBed.get(UserService);
    hotelCode = 'GRVGG';
    spyOn(userService, 'getCurrentLocationId').and.returnValue(hotelCode);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check getEncoderList method & check that returned Observable is valid', () => {
    service.getEncoderList().subscribe( data => {
      expect(JSON.stringify(data)).toEqual(JSON.stringify(MOCK_ENCODER_LIST));
    });

    const req = httpTestingController.
    expectOne(`${environment.fdkAPI}checkIn/keyEncoders?currentLocationId=${hotelCode}`);

    expect(req.request.method).toEqual('GET');
    req.flush(MOCK_ENCODER_LIST);
  });

  it('should catch error from getEncoderList method if http service is not successful', () => {
    service.getEncoderList().subscribe( data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toBeGreaterThan(204);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'GET' && req.url.indexOf('checkIn/keyEncoders') !== -1;
    });

    expect(request.request.method).toEqual('GET');
    expect(request.request.urlWithParams).toContain(hotelCode);

    request.error(null, {status: 500, statusText: 'Bad Request'});
    httpTestingController.verify();
  });
});
