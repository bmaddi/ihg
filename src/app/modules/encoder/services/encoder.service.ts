import { Injectable } from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {UserService} from 'ihg-ng-common-core';
import {Observable, throwError} from 'rxjs';
import {EncoderModel} from '@modules/encoder/models/encoder.model';

@Injectable({
  providedIn: 'root'
})
export class EncoderService {

  private readonly API_URL = environment.fdkAPI;

  constructor(private http: HttpClient,
              private userService: UserService) { }

  getEncoderList(): Observable<EncoderModel[]> {
    const apiURl = `${this.API_URL}checkIn/keyEncoders?currentLocationId=${this.userService.getCurrentLocationId()}`;
    return this.http.get(apiURl).pipe(map((response: EncoderModel[]) => {
      return response;
    }),
    catchError((error: HttpErrorResponse) => {
      return throwError(error);
    }));
  }
}
