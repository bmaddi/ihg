import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupArrivalsGridComponent } from './group-arrivals-grid.component';

xdescribe('GroupArrivalsGridComponent', () => {
  let component: GroupArrivalsGridComponent;
  let fixture: ComponentFixture<GroupArrivalsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupArrivalsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupArrivalsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
