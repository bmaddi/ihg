import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HostListener } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { GridComponent, GridDataResult } from '@progress/kendo-angular-grid';
import { SortDescriptor, State } from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { AppConstants } from '@app/constants/app-constants';
import { GuestGridModel, GuestResponse } from '@app/modules/guests/models/guest-list.models';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { ReservationStatus } from '@modules/guests/enums/guest-arrivals.enums';
import {
  ArrivalsDesktopColumns,
  ArrivalsTabLandscapeColumns,
  ArrivalsTabPortraitColumns
} from '@modules/group-block/constants/group-block.constants';
import {ARRIVALS_GRID_REPORT_ISSUE, PREPARE_GRID_ISSUE_AUTOFILL} from '@app/constants/error-autofill-constants';
import {ReportIssueAutoFillObject} from 'ihg-ng-common-pages';


@Component({
  selector: 'app-group-arrivals-grid',
  templateUrl: './group-arrivals-grid.component.html',
  styleUrls: ['./group-arrivals-grid.component.scss']
})
export class GroupArrivalsGridComponent implements OnInit, OnChanges {

  @Input() guestDataErrorEvent: Observable<EmitErrorModel>;
  @Input() guestData: GuestGridModel[] = [];
  @Input() localDate: Date;
  @Input() dateIndex: number;
  @Input() gridState: State;
  @Input() totalItems = 0;
  @Input() searchBy: string;
  @Input() topic: string;
  @Output() refreshAction = new EventEmitter();
  @Output() stateChange: EventEmitter<State> = new EventEmitter<State>();
  @ViewChild(GridComponent) grid: GridComponent;

  public guestNameCharLimit = 14;
  public characterCount = 16;
  public noDataSymbol = AppConstants.EMDASH;
  public reservationStatuses = ReservationStatus;
  public keyMap = GUEST_CONST.KEYMAP;
  public isDesktopView: boolean;
  public isTabletPortraitView: boolean;
  public gridView: GridDataResult;
  public gridColumns = ArrivalsDesktopColumns;
  public reportIssueConfig: ReportIssueAutoFillObject;

  private applicableItems: GuestGridModel[] = [];
  private ga = GUEST_CONST.GA;
  private gaCategory = this.ga.CATEGORY + this.ga.SUB_CATEGORY.ARRIVALSTAB;

  errorConfig: ServiceErrorComponentConfig;

  constructor(private router: Router,
              private titleCasePipe: TitleCasePipe,
              private guestListService: GuestListService,
              private gaService: GoogleAnalyticsService,
              private activeModal: NgbActiveModal,
              private translate: TranslateService) {
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    if (window.outerWidth < 769) {
      this.isTabletPortraitView = true;
      this.isDesktopView = false;
      this.gridColumns = ArrivalsTabPortraitColumns;
    } else if (window.outerWidth < 1025) {
      this.isTabletPortraitView = false;
      this.isDesktopView = false;
      this.gridColumns = ArrivalsTabLandscapeColumns;
    } else {
      this.isTabletPortraitView = false;
      this.isDesktopView = true;
      this.gridColumns = ArrivalsDesktopColumns;
    }
  }

  ngOnInit() {
    this.isDesktopView = (window.outerWidth < 1025);
    this.isTabletPortraitView = (window.outerWidth < 769);
    this.getScreenSize();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.guestData && changes.guestData.currentValue) {
      this.handleDataChange(changes.guestData.currentValue);
    }
  }

  public isToday(date: string): boolean {
    return moment(date).startOf('day').isSame(moment(this.localDate).startOf('day'));
  }

  public getDateValue(date: string): string {
    if (this.isToday(date)) {
      return this.translate.instant('LBL_TODAY');
    } else if (this.isTomorrow(date)) {
      return this.translate.instant('LBL_TMW');
    }
    return moment(date).format(AppConstants.VW_DT_FORMAT).toUpperCase();
  }

  private isTomorrow(date: string): boolean {
    return moment(date).startOf('day').isSame(moment(this.localDate).add(1, 'days').startOf('day'));
  }

  private handleDataChange = (newData: GuestGridModel[]) => {
    this.setApplicableItems(newData);
  }

  public setMemberProfile(loyaltyId: string) {
    this.gaService.trackEvent(this.gaCategory, this.ga.GUEST_PROFILE, this.ga.SELECTED);
    this.guestListService.setMemberProfile(+loyaltyId);
  }

  private setApplicableItems(items: GuestGridModel[]): void {
    // renew when current items are empty and total count > 0
    if (!items.length && this.totalItems) {
      this.renew();
    } else {
      this.applicableItems = items;
      this.loadGridData();
    }
  }

  private loadGridData(): void {
    this.gridView = {
      data: this.applicableItems,
      total: this.totalItems
    };
  }

  public onDataStateChange(event): void {
    this.stateChange.emit(event);
  }

  public onSortChange(sort: SortDescriptor[]): void {
    this.gaService.trackEvent(this.ga.CATEGORY, this.ga.SORT, this.ga[sort[0].field]);
  }

  public getReportIssueConfig(): void {
    if (this.topic === this.ga.SUB_CATEGORY.PREPARETAB) {
      this.reportIssueConfig = PREPARE_GRID_ISSUE_AUTOFILL;
    } else {
      this.reportIssueConfig = ARRIVALS_GRID_REPORT_ISSUE;
    }
  }

  public onRefresh(error) {
    switch (error) {
      case 0:
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.DATA_TIMEOUT, this.ga.TRY_AGAIN);
        break;
      case 2:
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.ERROR, this.ga.REFRESH);
        break;
    }
    this.getReportIssueConfig();
    this.refreshAction.emit();
  }

  public onReportIssueClicked() {
    this.gaService.trackEvent(this.ga.CATEGORY, this.ga.REPORT_ISSUE, this.ga.SELECTED);
    this.getReportIssueConfig();
  }

  public navigateToCheckInPage(dataItem: GuestResponse): void {
    this.gaService.trackEvent(this.gaCategory, this.ga.CHECK_IN, this.ga.SELECTED);
    this.guestListService.userDataSource.next(dataItem);
    this.activeModal.dismiss();
    this.router.navigate(['/check-in']);
  }

  public navigateToPreparePage(dataItem: GuestGridModel) {
    this.gaService.trackEvent(this.gaCategory, this.ga.PREPARE, this.ga.SELECTED);
    this.activeModal.dismiss();
    this.guestListService.emitGuestTabEvent({
      tabFlag: true,
      dateIndex: this.dateIndex,
      selectedData: dataItem,
      searchedDate: moment(dataItem.checkInDate).toDate()
    });
  }

  public transformData(dataParam: string): string {
    return this.titleCasePipe.transform(dataParam);
  }

  public displayToolTip(data: string): boolean {
    return data.length > this.characterCount;
  }

  public onGuestNameClick(dataItem: GuestGridModel): void {
    this.gaService.trackEvent(this.gaCategory, this.ga.GUEST_NAME, this.ga.SELECTED);
    if (this.isToday(dataItem.checkInDate)) {
      this.navigateToCheckInPage(dataItem);
    } else {
      this.navigateToPreparePage(dataItem);
    }
  }

  private renew() {
    this.gridState.skip = this.gridState.skip - this.gridState.take;
    this.stateChange.emit(this.gridState);
  }

}
