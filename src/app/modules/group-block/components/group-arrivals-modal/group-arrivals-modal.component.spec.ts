import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupArrivalsModalComponent } from './group-arrivals-modal.component';

xdescribe('GroupArrivalsModalComponent', () => {
  let component: GroupArrivalsModalComponent;
  let fixture: ComponentFixture<GroupArrivalsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupArrivalsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupArrivalsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
