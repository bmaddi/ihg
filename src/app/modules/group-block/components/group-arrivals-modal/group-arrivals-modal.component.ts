import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { State } from '@progress/kendo-data-query';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { TitleCasePipe } from '@angular/common';

import { AlertType, Alert } from 'ihg-ng-common-core';

import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { GuestGridModel, GuestListModel } from '@modules/guests/models/guest-list.models';
import { AppConstants } from '@app/constants/app-constants';

@Component({
  selector: 'app-group-arrivals-modal',
  templateUrl: './group-arrivals-modal.component.html',
  styleUrls: ['./group-arrivals-modal.component.scss']
})
export class GroupArrivalsModalComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public selectedGroup: string;
  public gridState: State = {
    take: 10,
    skip: 0,
    sort: [{ field: 'guestName', dir: 'asc' }]
  };
  public totalItems = 0;
  public guestData: GuestGridModel[] = [];
  public dateIndex: number;
  public localDate: Date;
  public alert: Alert;
  public topic: String;
  private groups$: Subscription = new Subscription();
  private maxDateRange = 7;

  constructor(private activeModal: NgbActiveModal, private guestListService: GuestListService, private titleCase: TitleCasePipe) {
    super();
  }

  ngOnInit() {
    this.fetchGuestList();
  }

  fetchGuestList(state?: State) {
    const endDate = moment(this.localDate).add(this.maxDateRange, 'days').toDate();
    this.groups$.add(this.guestListService.getGuestList(this.localDate, this.selectedGroup, this.gridState, 'group', endDate)
      .subscribe(this.handleDataChange,
        error => this.handleErrors(error)));
  }

  private handleDataChange = (newData: GuestListModel) => {
    const hasErrors = this.checkIfAnyApiErrors(newData, data => !data || !data.items || data.items.length === 0);
    this.guestData = hasErrors ? [] : newData.items;
    this.totalItems = hasErrors ? 0 : newData.totalItems;
    if (!hasErrors && this.guestData.length) {
      this.handleAlertMessage();
    }
  }

  private handleErrors = (error) => {
    this.guestData = [];
    this.totalItems = 0;
    this.processHttpErrors(error);
  }

  private handleAlertMessage(): void {
    this.alert = {
      type: AlertType.Info,
      message: '',
      detailMessage: 'LBL_NTC_ARR_DT_GST',
      translateValues: {
        detailMessage: { maxDate: this.getMaxDate(), groupName: this.titleCase.transform(this.selectedGroup) }
      },
      dismissible: false
    };
  }

  private getMaxDate(): string {
    return moment(this.localDate).add(this.maxDateRange, 'days').format(AppConstants.VW_DT_FORMAT).toUpperCase();
  }

  public onGuestListRefresh(): void {
    this.fetchGuestList();
  }

  public onStateChange(state: State): void {
    this.gridState = state;
    this.fetchGuestList(state);
  }

  public close() {
    this.activeModal.dismiss();
  }

  ngOnDestroy(): void {
    this.groups$.unsubscribe();
  }

}
