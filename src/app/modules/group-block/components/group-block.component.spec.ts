import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupBlockComponent } from './group-block.component';

xdescribe('GroupBlockComponent', () => {
  let component: GroupBlockComponent;
  let fixture: ComponentFixture<GroupBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
