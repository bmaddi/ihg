import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

import { GroupList, GroupListData, PreviewSearchData } from '@modules/group-block/models/group-block.model';
import { GroupBlockService } from '@modules/group-block/services/group-block.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { UtilityService } from '@services/utility/utility.service';

@Component({
  selector: 'app-group-block',
  templateUrl: './group-block.component.html',
  styleUrls: ['./group-block.component.scss']
})
export class GroupBlockComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {
  public gridState: State = { take: 5, skip: 0, sort: [{ field: 'groupName', dir: 'asc' }] };
  public groupList: GroupList[];
  public localDate: Date;
  public previewSearchValue: string;
  public isAllGroupSelected: boolean;
  private group$: Subscription = new Subscription();

  @Input() selectedDate: Date;
  @Input() searchQuery: string;
  @Input() subcategory: string;
  @Input() parentTabActive = false;
  @Output() emitSelectedGroup = new EventEmitter<string>();
  @Output() emitGroupList = new EventEmitter<object>();

  constructor(private groupService: GroupBlockService, private utility: UtilityService) {
    super();
  }

  ngOnInit(): void {
    this.refreshGroupsIfActiveTab();
    this.subscribePreviewSearch();
    this.subscribeShowGroupsClickEvent();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.utility.hasSimpleParamChanges(changes, 'searchQuery') || this.hasDateChanges(changes) ||
      this.utility.hasSimpleParamChanges(changes, 'parentTabActive')) {
      this.refreshGroupsIfActiveTab();
    }
  }

  private hasDateChanges(changes: SimpleChanges): boolean {
    return this.utility.hasSimpleDateChanges(changes, 'selectedDate') && changes.selectedDate.previousValue !== null;
  }

  private refreshGroupsIfActiveTab() {
    if (this.parentTabActive) {
      this.subscribeGroupList();
    }
  }

  public onGuestListRefresh(): void {
    this.subscribeGroupList(null, true, true);
  }

  private determineGridState = (state?: State) => {
    if (!state && this.gridState) {
      this.gridState.sort = null;
      this.gridState.skip = 0;
      return;
    }
    this.gridState = state;
  };

  private subscribeGroupList(state?: State, showSpinner = false, groupDetailsRequired = false): void {
    const date = this.selectedDate;
    const searchBy = this.searchQuery || this.previewSearchValue;
    this.determineGridState(state);
    this.group$.add(this.groupService.getGroupList(date, searchBy, this.gridState, showSpinner, groupDetailsRequired)
      .subscribe((response: GroupListData) => {
        this.handleGroupResponse(response);
      }, (error) => {
        this.emitGroupList.emit({data: null, refresh: true});
        this.groupList = null;
        super.processHttpErrors(error);
      }));
  }

  private handleGroupResponse(response: GroupListData): void {
    if (!super.checkIfAnyApiErrors(response)) {
      this.groupList = response.groupDataLst || [];
      this.localDate = response.localDate ? moment(response.localDate).toDate() : moment().toDate();
      this.handleRecordHighlight();
      this.emitGroupList.emit({ data: this.groupList, refresh: this.isAllGroupSelected });
    } else {
      this.emitGroupList.emit({ data: response ? null : [], refresh: this.isAllGroupSelected });
      this.groupList = null;
    }
  }

  private handleRecordHighlight(): void {
    if (this.groupList.length && this.previewSearchValue && !this.isAllGroupSelected) {
      this.groupList[0].selected = true;
    } else if (this.isAllGroupSelected) {
      this.clearPreviousSelection();
    }
  }

  public changeState(state: State): void {
    this.subscribeGroupList(state, true, true);
  }

  private subscribePreviewSearch(): void {
    this.group$.add(this.groupService.previewSearch.subscribe((data: PreviewSearchData) => {
      this.previewSearchValue = data.search;
      this.isAllGroupSelected = data.isAll;
      if (this.previewSearchValue) {
        this.subscribeGroupList(null, true, true);
      } else {
        this.clearPreviousSelection();
      }
    }));
  }

  private clearPreviousSelection(): void {
    const selectedItem = this.groupList.find((item: GroupList) => item.selected);
    if (selectedItem) {
      selectedItem.selected = false;
    }
  }

  private subscribeShowGroupsClickEvent(): void {
    this.groupService.showGroups.subscribe(() => {
      this.isAllGroupSelected = true;
      this.subscribeGroupList(null, true, true);
    });
  }

  ngOnDestroy(): void {
    this.group$.unsubscribe();
  }
}
