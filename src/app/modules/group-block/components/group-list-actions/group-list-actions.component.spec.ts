import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupListActionsComponent } from './group-list-actions.component';

xdescribe('GroupListActionsComponent', () => {
  let component: GroupListActionsComponent;
  let fixture: ComponentFixture<GroupListActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupListActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupListActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
