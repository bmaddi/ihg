import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { GroupArrivalsModalComponent } from '@modules/group-block/components/group-arrivals-modal/group-arrivals-modal.component';
import { GroupList } from '@modules/group-block/models/group-block.model';

@Component({
  selector: 'app-group-list-actions',
  templateUrl: './group-list-actions.component.html',
  styleUrls: ['./group-list-actions.component.scss']
})
export class GroupListActionsComponent implements OnInit {

  @Input() groupItem: GroupList;
  @Input() localDate: Date;
  @Input() topic: String;

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  public openGroupArrivalsModal(): void {
    const modal = this.modalService.open(GroupArrivalsModalComponent, { backdrop: 'static', windowClass: 'extra-large-modal' });
    modal.componentInstance.selectedGroup = this.groupItem.groupName;
    modal.componentInstance.localDate = this.localDate;
    modal.componentInstance.topic = this.topic;
  }

}
