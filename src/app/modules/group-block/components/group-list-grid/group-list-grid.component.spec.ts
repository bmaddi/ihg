import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupListGridComponent } from './group-list-grid.component';

xdescribe('GroupListGridComponent', () => {
  let component: GroupListGridComponent;
  let fixture: ComponentFixture<GroupListGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupListGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupListGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
