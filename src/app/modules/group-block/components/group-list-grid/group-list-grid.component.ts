import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { PageChangeEvent, RowClassArgs } from '@progress/kendo-angular-grid';
import { SortDescriptor, State } from '@progress/kendo-data-query';
import { Observable } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { GroupList } from '@modules/group-block/models/group-block.model';
import { ServiceErrorComponentConfig } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import {GROUP_LIST_GRID_AUTOFILL, GROUP_LIST_REPORT_AUTOFILL} from '@app/constants/error-autofill-constants';
import {ReportIssueAutoFillObject} from 'ihg-ng-common-pages';

@Component({
  selector: 'app-group-list-grid',
  templateUrl: './group-list-grid.component.html',
  styleUrls: ['./group-list-grid.component.scss']
})

export class GroupListGridComponent implements OnInit, OnChanges {
  public selectedGroup = null;
  public reportIssueConfig: ReportIssueAutoFillObject;

  @Input() prepareGuestDataErrorEvent: Observable<EmitErrorModel>;
  @Input() gridState: State;
  @Input() localDate: Date;
  @Input() items: GroupList[];
  @Input() searchQuery: string;
  @Input() displayBorder: boolean;
  @Input() topic: string;
  @Output() refreshAction = new EventEmitter();
  @Output() emitSelectedGroup = new EventEmitter<string>();
  @Output() stateChange: EventEmitter<State> = new EventEmitter<State>();

  private ga = GUEST_CONST.GA;

  errorConfig: ServiceErrorComponentConfig;

  constructor(private gaService: GoogleAnalyticsService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.items && changes.items.currentValue) {
      this.selectedGroup = null;
    }
  }

  onSortChange(sort: SortDescriptor[]): void {
    this.gridState.sort = sort;
    this.stateChange.emit(this.gridState);
  }

  onPageChange({ skip, take }: PageChangeEvent): void {
    this.gridState.skip = skip;
    this.gridState.take = take;
  }

   getReportIssueConfig(): void {
    if (this.topic === this.ga.SUB_CATEGORY.PREPARETAB) {
      this.reportIssueConfig = GROUP_LIST_GRID_AUTOFILL;
    } else {
      this.reportIssueConfig = GROUP_LIST_REPORT_AUTOFILL;
    }
  }

  public onRefresh(error) {
    switch (error) {
      case 0:
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.DATA_TIMEOUT, this.ga.TRY_AGAIN);
        break;
      case 2:
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.ERROR, this.ga.REFRESH);
        break;
    }
    this.getReportIssueConfig();
    this.refreshAction.emit();
  }

  public onReportIssueClicked() {
    this.gaService.trackEvent(this.ga.CATEGORY, this.ga.REPORT_ISSUE, this.ga.SELECTED);
    this.getReportIssueConfig();
  }

  public onGroupClick(dataItem: GroupList): void {
    this.clearPreviousSelection();
    dataItem.selected = true;
    this.selectedGroup = dataItem.groupName;
    this.emitSelectedGroup.emit(this.selectedGroup);
  }

  private clearPreviousSelection(): void {
    const selectedItem = this.items.find((item: GroupList) => item.selected);
    if (selectedItem) {
      selectedItem.selected = false;
    }
  }

  public getRowClass = (context: RowClassArgs) => {
    return { 'selected-group': context.dataItem.selected };
  }
}
