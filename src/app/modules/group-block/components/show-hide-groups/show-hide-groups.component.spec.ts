import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowHideGroupsComponent } from './show-hide-groups.component';

xdescribe('ShowHideGroupsComponent', () => {
  let component: ShowHideGroupsComponent;
  let fixture: ComponentFixture<ShowHideGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowHideGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowHideGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
