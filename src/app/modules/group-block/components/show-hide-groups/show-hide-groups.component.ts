import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';

@Component({
  selector: 'app-show-hide-groups',
  templateUrl: './show-hide-groups.component.html',
  styleUrls: ['./show-hide-groups.component.scss']
})
export class ShowHideGroupsComponent implements OnInit {

  @Input() showGroups = false;
  @Output() emitChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private arrivalsService: ArrivalsService,
    private appCommonService: AppCommonService) {
  }

  ngOnInit() {
  }

  public handleClick(): void {    
    this.arrivalsService.handleUnsavedTasks(() => {
      this.showGroups = !this.showGroups;
      //this.appCommonService.flushReservationNumber();
      this.emitChange.emit(this.showGroups);      
    });
  }

}
