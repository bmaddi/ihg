export const ArrivalsDesktopColumns = {
  guestNameWidth: 170,
  profileWidth: 35,
  loyaltyWidth: 105,
  groupWidth: 155,
  arrivalWidth: 90,
  nightsWidth: 70,
  departureWidth: 140,
  roomWidth: 100,
  statusWidth: 150,
  actionsWidth: 110
};

export const ArrivalsTabLandscapeColumns = {
  guestNameWidth: 190,
  profileWidth: 36,
  loyaltyWidth: 105,
  groupWidth: 173,
  arrivalWidth: 74,
  nightsWidth: 92,
  departureWidth: 98,
  roomWidth: 95,
  statusWidth: 68,
  actionsWidth: 106
};

export const ArrivalsTabPortraitColumns = {
  guestNameWidth: 168,
  profileWidth: 27,
  loyaltyWidth: 105,
  groupWidth: 128,
  arrivalWidth: 71,
  nightsWidth: 68,
  departureWidth: 0,
  roomWidth: 61,
  statusWidth: 63,
  actionsWidth: 92
};
