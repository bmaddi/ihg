import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridModule } from '@progress/kendo-angular-grid';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { ActionToggleDropdownModule } from 'ihg-ng-common-components';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { BadgeListModule } from '@modules/badge-list/badge-list.module';

import { GroupBlockComponent } from '@modules/group-block/components/group-block.component';
import { GroupListGridComponent } from './components/group-list-grid/group-list-grid.component';
import { ShowHideGroupsComponent } from './components/show-hide-groups/show-hide-groups.component';
import { GroupListActionsComponent } from '@modules/group-block/components/group-list-actions/group-list-actions.component';
import { GroupArrivalsModalComponent } from './components/group-arrivals-modal/group-arrivals-modal.component';
import { GroupArrivalsGridComponent } from './components/group-arrivals-grid/group-arrivals-grid.component';
import { ProgressStatusModule } from '@modules/progress-status/progress-status.module';

@NgModule({
  imports: [
    CommonModule,
    GridModule,
    TranslateModule,
    NgbModule,
    RouterModule,
    IhgNgCommonCoreModule,
    IhgNgCommonKendoModule,
    ActionToggleDropdownModule,
    AppSharedModule,
    BadgeListModule,
    ProgressStatusModule
  ],
  declarations: [
    GroupBlockComponent,
    GroupListGridComponent,
    GroupListActionsComponent,
    ShowHideGroupsComponent,
    GroupArrivalsModalComponent,
    GroupArrivalsGridComponent
  ],
  entryComponents: [
    GroupArrivalsModalComponent
  ],
  exports: [
    ShowHideGroupsComponent,
    GroupBlockComponent
  ]
})
export class GroupBlockModule {
}
