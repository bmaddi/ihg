import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export interface GroupListActions {
  selected?: boolean;
}

export interface GroupList extends GroupListActions {
  groupName: string;
  groupCode: string;
  arrivalDate: string;
  departureDate: string;
  noOfNights: number;
  releaseDate: string;
  roomsContracted: number;
  numberOfReservations: number;
}

export interface GroupListData {
  errors?: Array<ErrorsModel>;
  groupDataLst: Array<GroupList>;
  localDate: string;
}

export interface PreviewSearchData {
  search: string;
  isAll?: boolean;
}

