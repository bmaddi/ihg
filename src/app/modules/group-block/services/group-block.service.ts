import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { AppConstants } from '@app/constants/app-constants';
import { GroupListData, PreviewSearchData } from '@modules/group-block/models/group-block.model';
import { State } from '@progress/kendo-data-query';

@Injectable({
  providedIn: 'root'
})
export class GroupBlockService {
  private readonly API_URL = environment.fdkAPI;
  public previewSearch: Subject<PreviewSearchData> = new Subject();
  public showGroups: Subject<void> = new Subject();

  constructor(private http: HttpClient, private userService: UserService) {
  }

  public getGroupList(selectedDate: Date = null, searchQuery: string, state?: State, displaySpinner = false, groupDetailsRequired = false)
    : Observable<GroupListData> {
    const params = this.getServiceParams(selectedDate, searchQuery, state, groupDetailsRequired);
    const apiURl = `${this.API_URL}groupsData/${this.getLocationId()}?${params}`;
    const httpOptions = { headers: new HttpHeaders({ spinnerConfig: displaySpinner ? 'Y' : 'N' }) };
    return this.http.get(apiURl, httpOptions).pipe(map((response: GroupListData) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getServiceParams(selectedDate: Date, searchQuery: string, state?: State, groupDetailsRequired = false): string {
    const dateParam = selectedDate ? `date=${moment(selectedDate).format(AppConstants.DB_DT_FORMAT)}&` : '';
    const groupNameParam = searchQuery && (searchQuery.length > 1) ? `groupName=${searchQuery}&` : '';
    const sortParam = state && state.sort ? `sortBy=${state.sort[0].field === 'groupName' ? 'groupName&' : state.sort[0].field}&` : '';
    const directionParam = state && state.sort ? `sortDirection=${state.sort[0].dir}&` : '';
    const groupDetailsParam = `groupDetailsRequired=${groupDetailsRequired}`;
    return `${dateParam}${groupNameParam}${sortParam}${directionParam}${groupDetailsParam}`;
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }
}
