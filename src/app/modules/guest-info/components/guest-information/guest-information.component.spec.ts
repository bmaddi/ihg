import { async, ComponentFixture, TestBed, fakeAsync, inject } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { NgbModule, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppSharedModule } from '@modules/shared/app-shared.module';
import { RewardsNumberLookUpModule } from '@modules/rewards-number-look-up/rewards-number-look-up.module';
import { GuestInformationComponent } from './guest-information.component';
import { EnrollmentSummaryService } from 'ihg-ng-common-pages';

import {
  TransitionGuardService,
  GuardRegistrationParams,
  Alert,
  GoogleAnalyticsService,
  UserConfirmationModalConfig,
  AlertType
} from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GuestInfoService } from '../../services/guest-information.service';
import { UserService } from 'ihg-ng-common-core';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Subject, observable, of } from 'rxjs';
import { GuestInfo, IHGMemberDetail, Country, AdditionalAlert } from '../../models/guest-info.model';
import { LookUpRewardsMemberModalComponent } from '@modules/rewards-number-look-up/components/look-up-rewards-member-modal/look-up-rewards-member-modal.component';
import * as cloneDeep from 'lodash/cloneDeep';
import { RewardsNumberLookUpService } from '@app/modules/rewards-number-look-up/services/rewards-number-look-up.service';
import { MOCK_STAY_DATA } from '@modules/stay-info/constants/stay-information-constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { By } from '@angular/platform-browser';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) {
  }
}

xdescribe('GuestInformationComponent', () => {
  let component: GuestInformationComponent;
  let fixture: ComponentFixture<GuestInformationComponent>;
  let guestInfoService: GuestInfoService;

  const guestInfoData = {
    'reservation': {
      'reservationNumber': '34534534',
      'hotelCode': 'GRVAB',
      'loyaltyMembershipId': '',
      'confirmationNumber': '47593592',
      'roomCategoryCode': '',
      'corporateId': '',
      'checkInDate': null,
      'checkOutDate': '2019-10-19',
      'confirmationDate': null,
      'roomNumber': null,
      'iataNumber': null,
      'amenityPoints': null,
      'arrivalDate': '2019-10-18',
      'numberOfNights': 1,
      'numberOfRooms': 1,
      'adultQuantity': 1,
      'childQuantity': 0,
      'rateCategoryCode': 'IGCOR',
      'roomTypeCode': 'KDXG',
      'groupCode': '',
      'appUserId': '',
      'rateInfo': {
        'averageRate': '100.00',
        'currency': 'GBP',
        'totalTaxAmount': '16.67',
        'totalAmountAfterTax': '100.00',
        'totalExtraPersonAmount': '0.00',
        'totalServiceChargeAmount': '0.00',
        'dailyRates': [
          {
            'date': '2019-10-18',
            'amountBeforeTax': '83.33',
            'amountAfterTax': '100.00',
            'baseAmount': '100.00'
          }
        ],
        'rateRules': [
          {
            'rateRuleDesc': '',
            'noShowPolicyDesc': '',
            'extraPersonCharge': '0.00',
            'earlyDeparture': '',
            'guaranteePolicy': '',
            'checkinTime': '',
            'checkoutTime': '',
            'tax': '16.67',
            'serviceCharge': '0.00'
          }
        ],
        'serviceChargeIncluded': false,
        'taxesChargeIncluded': true,
        'extraPersonChargeIncluded': true
      },
      'guestInfo': {
        'firstName': 'debasmita',
        'lastName': 'pradhan',
        'middleName': '',
        'addressLine1': '',
        'addressLine2': '',
        'countryCode': '',
        'countryName': '',
        'zipCode': '',
        'stateProvince': '',
        'cityName': '',
        'phoneType': '',
        'phoneNumber': '12111',
        'email': '',
        'hotelCode': ''
      },
      'rateCategories': [
        'ADAVP',
        'AG000',
        'IBDNG',
        'IBGP2',
        'IBMTF'
      ],
      'doNotMoveRoom': false,
      'productIncludedInOffer': false,
      'serviceChargeIncluded': false,
      'taxesChargeIncluded': true,
      'confirmationDateString': '',
      'checkInDateString': '',
      'checkOutDateString': '2019-10-19'
    }
  };

  const rewardClubResponse = {
    'memberDetails': {
      'ihgRcNumber': '147096275',
      'ihgRcMembership': 'platinum',
      'ihgRcPoints': 66993,
      'badges': ['platinum', 'ambassador', 'employee'],
      'firstName': 'Test User D',
      'lastName': 'Homepage',
      'middleName': '',
      'emailId': '',
      'phone': '',
      'countryCode': ''
    }, 'errors': null, 'reservation': null
  };
  const rewardClubErrorResponse = {'errors': [], 'memberDetails': null};
  let router: Router;
  let modalService: NgbModal;
  let modalRef: NgbModalRef;

  class MockGuestInfoService {
    public hasUnsavedGuestInfo = false;
    public guestInfoChangeSubject: BehaviorSubject<any> = new BehaviorSubject(null);
    public resetGuestInfoForm: BehaviorSubject<any> = new BehaviorSubject(false);
    public guestInfomation: GuestInfo;
    public messageSource = new BehaviorSubject<Alert>(null);
    currentMessage = this.messageSource.asObservable();
    public hasDataChanges: any;

    public warningMessageSource = new BehaviorSubject<Alert>(null);
    additioanlWarningMessage = this.warningMessageSource.asObservable();
    public updatedIHGNo: BehaviorSubject<string> = new BehaviorSubject(null);
    public isInValidIHGNo: BehaviorSubject<boolean> = new BehaviorSubject(null);

    private ihgRcNumberValidationSource = new Subject<IHGMemberDetail>();
    public ihgRcNumberValidation = this.ihgRcNumberValidationSource.asObservable();

    private navigateToCheckInSubject = new Subject<null>();
    public navigate$ = this.navigateToCheckInSubject.asObservable();

    public getCountries(): Observable<Country[]> {
      const countries: Country[] = [];
      return of(countries);
    }

    getConfirmModalConfig(): UserConfirmationModalConfig {
      return {
        modalHeader: 'COM_SAVE_DISCARD_MOD_TITLE',
        primaryButtonLabel: 'COM_BTN_CONT',
        primaryButtonClass: 'btn-primary',
        defaultButtonLabel: 'COM_BTN_CANCEL',
        defaultButtonClass: 'btn-default',
        messageHeader: 'LBL_GST_UPDATE_TEXT',
        messageBody: '',
        windowClass: 'modal-medium',
        dismissible: true
      };
    }

    validateEmail() {
      return of({status: 'Ok', code: 200});
    }

    validateRewardClubNo() {
      return of({memberDetail: {}, error: null});
    }

    triggerCancelEditMode(): void {

    }

    getGuestInformation(): Observable<any> {
      return Observable.of({
        json: () => guestInfoData
      });
    }

    public showAlertMessage() {
      this.messageSource.next(null);
      this.warningMessageSource.next(this.setAlertMessage());
    }

    private setAlertMessage(): AdditionalAlert {
      return {
        type: AlertType.Warning,
        message: 'COM_TOAST_WARNING',
        detailMessage: 'LBL_RCN_NAME_CHANGE',
        dismissible: false,
        otherMessages: []
      };
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        IhgNgCommonComponentsModule,
        ReactiveFormsModule,
        TranslateModule,
        DatePickerModule,
        DropDownsModule,
        DateInputsModule,
        NgbModule,
        AppSharedModule,
        RewardsNumberLookUpModule,
        RouterTestingModule.withRoutes([])
      ],
      declarations: [GuestInformationComponent],
      providers: [GuestInfoService, TranslateStore, TransitionGuardService, GoogleAnalyticsService,
        NgbModal, EnrollmentSummaryService, RewardsNumberLookUpService, UserService,
        {provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService},
        {provide: GuestInfoService, useClass: MockGuestInfoService},
        {provide: ReportIssueService, useValue: {}}
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 200000;
    modalService = TestBed.get(NgbModal);
    fixture = TestBed.createComponent(GuestInformationComponent);
    component = fixture.componentInstance;
    guestInfoService = TestBed.get(GuestInfoService);
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', async(() => {
    fixture.whenStable().then(() => {
      expect(component).toBeTruthy();
    });
    component.ngOnInit();
  }), 10000);

  it('call on validateEmailAddress for empty', async(() => {
    fixture.whenStable().then(() => {
      component.guestInfoFromGroup.controls['email'].patchValue('');
      component.validateEmailAddress();
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['email'].errors).toBe(null);
    });
  }));

  it('call on validateEmailAddress for empty and isEmailUpdated True', async(() => {
    fixture.whenStable().then(() => {
      component.isEmailUpdated = true;
      component.guestInfoFromGroup.controls['email'].markAsDirty();
      component.guestInfoFromGroup.controls['email'].patchValue('');
      component.guestInformationData = guestInfoData;
      component.validateEmailAddress();
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['email'].errors).toBe(null);
    });
  }));

  it('call on validateEmailAddress for empty and guestinfo email is also empty', async(() => {
    fixture.whenStable().then(() => {
      component.isEmailUpdated = true;
      component.guestInfoFromGroup.controls['email'].markAsDirty();
      component.guestInfoFromGroup.controls['email'].patchValue('');
      const clonedObject = cloneDeep(guestInfoData);
      clonedObject.reservation.guestInfo.email = null;
      component.guestInformationData = clonedObject;
      component.validateEmailAddress();
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['email'].errors).toBe(null);
    });
  }));

  //  this.guestInformationData.reservation.guestInfo

  it('call on validateEmailAddress for valid email address', async(() => {
    fixture.whenStable().then(() => {
      component.guestInfoFromGroup.controls['email'].patchValue('bharatreddy99@gmail.com');
      component.isEmailUpdated = true;
      component.guestInfoFromGroup.controls['email'].markAsDirty();
      spyOn(guestInfoService, 'validateEmail').and.returnValue(Observable.of({reasonCode: 200, reasonDescription: 'Ok'}));
      component.validateEmailAddress();
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['email'].errors).toBe(null);
    });
  }));

  it('call on validateEmailAddress for invalid email address', async(() => {
    fixture.whenStable().then(() => {
      component.guestInfoFromGroup.controls['email'].patchValue('bharatreddy990000@gmail.com');
      component.isEmailUpdated = true;
      component.guestInfoFromGroup.controls['email'].markAsDirty();
      spyOn(guestInfoService, 'validateEmail').and.returnValue(Observable.of({reasonCode: 221, reasonDescription: 'Ok'}));
      component.validateEmailAddress();
      fixture.detectChanges();
      expect(component.emailValidationDescription).not.toEqual('');
    });
  }));

  it('call on onChangeClubNo for empty', async(() => {
    fixture.whenStable().then(() => {
      component.guestInformationData = guestInfoData;
      component.guestInfoFromGroup.controls['ihgRcNumber'].patchValue('');
      component.onChangeClubNo();
      expect(component.guestInfoFromGroup.controls['ihgRcNumber'].errors).toBe(null);
    });
  }));

  it('call on getGuestInformation', async(() => {
    fixture.whenStable().then(() => {
      component.reservationNumber = '435345345';
      component.guestInformationData = guestInfoData;
      spyOn(guestInfoService, 'getGuestInformation').and.returnValue(Observable.of(guestInfoData));
      component.ngOnInit();
      fixture.detectChanges();
      expect(component.guestInformationData).not.toBe(null);
    });
  }));


  it('call on getGuestInformation with loyalty member', async(() => {
    fixture.whenStable().then(() => {
      component.reservationNumber = '435345345';
      const clonedObject = cloneDeep(guestInfoData);
      clonedObject.reservation.loyaltyMembershipId = '456456456';
      component.guestInformationData = clonedObject;
      spyOn(guestInfoService, 'getGuestInformation').and.returnValue(Observable.of(clonedObject));
      component.ngOnInit();
      fixture.detectChanges();
      expect(component.isClubMember).toBeTruthy();
    });
  }));

  it('on change ClubNumber for different First Name and Last Name', async(() => {
    fixture.whenStable().then(() => {
      component.reservationNumber = '435345345';
      component.guestInformationData = guestInfoData;
      spyOn(guestInfoService, 'validateRewardClubNo').and.returnValue(Observable.of(rewardClubResponse));
      component.onChangeClubNo();
      fixture.detectChanges();
      expect(guestInfoService.warningMessageSource).not.toBe(null);
    });
  }));

  it('Error response on change ClubNumber ', async(() => {
    fixture.whenStable().then(() => {
      component.reservationNumber = '435345345';
      component.guestInformationData = guestInfoData;
      spyOn(guestInfoService, 'validateRewardClubNo').and.returnValue(Observable.of(rewardClubErrorResponse));
      component.onChangeClubNo();
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['ihgRcNumber'].errors).not.toBe(null);
    });
  }));

  it('should check cancel button is loaded and is enabled always', (() => {
    const cancel = fixture.debugElement.query(By.css('[data-slnm-ihg="Cancel-SID"]'));
    expect(cancel).toBeDefined();
    component.ngOnInit();
    expect(cancel.nativeElement.disabled).toBe(false);
  }));

  it('should check save button is loaded and disabled for invalid form', (() => {
    const save = fixture.debugElement.query(By.css('[data-slnm-ihg="Save-SID"]'));
    expect(save).toBeDefined();
    if (component.guestInfoFromGroup.invalid) {
      expect(save.nativeElement.disabled).toBe(true);
    }
  }));

  it('should check save button is loaded and enabled after form validation', (() => {
    const save = fixture.debugElement.query(By.css('[data-slnm-ihg="Save-SID"]'));
    expect(save).toBeDefined();
    if (component.guestInfoFromGroup.dirty && component.guestInfoFromGroup.valid) {
      expect(save.nativeElement.disabled).toBe(false);
    }
  }));

  it('On call reset form values ', async(() => {
    fixture.whenStable().then(() => {
      component.reservationNumber = '435345345';
      component.guestInformationData = guestInfoData;
      component.guestInfoFromGroup.controls['firstName'].patchValue('Bharath');
      guestInfoService.resetGuestInfoForm.next(true);
      component.resetFormValues();
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['firstName'].value).toBe('debasmita');
    });
  }));

  it('On updating IHG No ', async(() => {
    fixture.whenStable().then(() => {
      component.reservationNumber = '435345345';
      component.guestInformationData = guestInfoData;
      component.subscribeNecessaryValues();
      fixture.detectChanges();
      guestInfoService.updatedIHGNo.next('784542126');
      fixture.detectChanges();
      expect(component.guestInfoFromGroup.controls['ihgRcNumber'].value).toBe('784542126');
    });
  }));

  it('On emailUpdated when having reservation number ', async(() => {
    fixture.whenStable().then(() => {
      component.emailUpdated();
      fixture.detectChanges();
      expect(component.isEmailUpdated).toBeTruthy();
    });
  }));

  it('On cancelChanges when having reservation number ', fakeAsync(() => {
    fixture.whenStable().then(() => {
      const component1 = fixture.componentInstance;
      const navigateSpy = spyOn(router, 'navigate');
      component1.reservationNumber = '435345345';
      component1.guestInformationData = guestInfoData;
      component1.cancelChanges();
      expect(navigateSpy).toHaveBeenCalledWith(['check-in']);
    });
  }));

  it('On cancelChanges when there is no reservation number ', async(() => {
    fixture.whenStable().then(() => {
      const component1 = fixture.componentInstance;
      const navigateSpy = spyOn(router, 'navigate');
      component1.reservationNumber = '';
      component1.guestInformationData = guestInfoData;
      component1.cancelChanges();
      expect(navigateSpy).toHaveBeenCalledWith(['guest-list-details']);
    });
  }));


  it('On openRewardsMemberModal ', async(() => {
    fixture.whenStable().then(() => {
      inject([EnrollmentSummaryService], (injectService: EnrollmentSummaryService) => {
        modalRef = modalService.open(LookUpRewardsMemberModalComponent);
        spyOn(modalService, 'open').and.returnValue(modalRef);
        modalRef.componentInstance.ihgRcNumber.next('34534534');
        component.openRewardsMemberModal();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.guestInfoFromGroup.controls['ihgRcNumber'].value).toBe(34534534);
        });
      });

    });
  }));

  it('Should check if state is ManageStay and emit stayInformation event on Cancel Button click', () => {
    component.state = AppStateNames.manageStay;
    fixture.detectChanges();
    spyOn(TestBed.get(GuestInfoService), 'triggerCancelEditMode');

    component.cancelChanges();
    fixture.detectChanges();

    expect(TestBed.get(GuestInfoService).triggerCancelEditMode).toHaveBeenCalled();
  });
});

