import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription, Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ValidateEmailModel } from 'ihg-ng-common-pages';
import { TransitionGuardService, GuardRegistrationParams, Alert, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from 'ihg-ng-common-core';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GuestInformationModel, Country, Reservation, IHGMemberDetail, GuestInfo } from '../../models/guest-info.model';
import { GuestInfoService } from '../../services/guest-information.service';
import { DEFAULT_COUNTRY, PHONE_TYPE, errorReasonCodes } from '../../constants/guest-info.constant';
import { LookUpRewardsMemberModalComponent } from '@modules/rewards-number-look-up/components/look-up-rewards-member-modal/look-up-rewards-member-modal.component';
import { MANAGE_STAY_CONST } from '@app/modules/manage-stay/manage-stay.constants';
import { FdkValidators } from '@app/modules/shared/validators/app.validators';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { OffersService } from '@modules/offers/services/offers.service';

@Component({
  selector: 'app-guest-information',
  templateUrl: './guest-information.component.html',
  styleUrls: ['./guest-information.component.scss']
})
export class GuestInformationComponent extends AppErrorBaseComponent implements OnInit {

  public guestInfoFromGroup: FormGroup;
  public guestInformationData: GuestInformationModel;
  public countries: Country[];
  public phoneTypes: any = PHONE_TYPE;
  public isClubMember: boolean;
  public ihgInfomration: IHGMemberDetail;
  public ihgNumber: string;
  public emailValidationDescription: string;
  public originalCardNumber: string;
  public showSuccessMessage = false;
  public showErrorMessage = false;
  public hardReset = false;
  public confirmReservationToast: Alert;
  public isEnroll = false;

  private rootSubscription = new Subscription();
  private guestInfoPayload: GuestInfo;
  private actualEmail: string;
  isEmailUpdated: boolean;

  constructor(
    private fb: FormBuilder,
    private guestInfoService: GuestInfoService,
    private translate: TranslateService,
    private transitionGuardService: TransitionGuardService,
    private modalService: NgbModal,
    private gaService: GoogleAnalyticsService,
    private router: Router,
    private userService: UserService,
    private offersService: OffersService) {
    super();
  }

  @Input() reservationNumber: string;
  @Input() elementDisabled = false;
  @Input() state: string;
  @Output() emitGuestInfo: EventEmitter<GuestInfo> = new EventEmitter();
  @Output() successToastMsg: EventEmitter<boolean> = new EventEmitter();
  @Output() cancelEdit = new EventEmitter<void>();
  @ViewChild('ccInput') input;

  ngOnInit() {
    this.resetGuestServiceData();
    this.translatePhoneTypes();
    this.createForm();
    this.getCountries();
    this.resetFormValues();
    this.handleGuestData();
    this.subscribeNecessaryValues();
    this.disableLoyaltyEnrollmentForWalkin();
  }

  private handleGuestData() {
    if (this.reservationNumber) {
      this.getGuestInformation();
    } else {
      this.initCountryAndPhoneType();
      this.isEnroll = true;
    }
  }

  subscribeNecessaryValues() {
    this.registerInTransitionGuard();
    this.subscribeIHGNoChange();
    this.subscribeValueChanges();
  }

  private disableLoyaltyEnrollmentForWalkin() {
    if (!this.reservationNumber) {
      this.guestInfoFromGroup.controls['ihgRcNumber'].disable();
    }
  }

  private resetGuestServiceData() {
    this.guestInfoService.warningMessageSource.next(null);
    this.guestInfoService.messageSource.next(null);
  }

  subscribeValueChanges() {
    this.rootSubscription.add(this.guestInfoFromGroup.valueChanges.subscribe(val => {
      if (val && !this.hardReset && (this.guestInfoFromGroup.dirty || this.guestInfoFromGroup.touched)) {
        this.setDataChange(true);
        this.guestInfoPayload = {...this.guestInfoPayload, ...this.guestInfoFromGroup.getRawValue()};
      }
    }));
  }

  resetFormValues() {
    this.rootSubscription.add(this.guestInfoService.resetGuestInfoForm.subscribe(val => {
      if (val === true) {
        this.hardReset = true;
        this.ihgNumber = null;
        this.createForm();
        this.loadGuestFormGroup();
      }
    }));
  }

  private subscribeIHGNoChange() {
    this.rootSubscription.add(this.guestInfoService.updatedIHGNo
      .subscribe(response => {
        if (response != null) {
          this.guestInfoFromGroup.controls['ihgRcNumber'].patchValue(response);
          this.onChangeClubNo();
        }
        this.ihgNumber = response;
      }));
  }

  private getGuestInformation() {
    this.rootSubscription.add(this.guestInfoService.getGuestInformation(this.reservationNumber)
      .subscribe((response: GuestInformationModel) => {
        this.guestInformationData = response;
        this.loadGuestFormGroup();
      }));
  }

  getCountries() {
    this.rootSubscription.add(this.guestInfoService.getCountries()
      .subscribe((response: Country[]) => {
        response.unshift(DEFAULT_COUNTRY);
        this.countries = response;
      }));
  }

  validateEmailAddress() {
    const emailControl = this.guestInfoFromGroup.get('email') as FormControl;
    if (!emailControl.dirty || !this.isEmailUpdated) {
      return;
    }
    emailControl.setErrors(null);
    this.actualEmail = emailControl.value;
    if (emailControl.value !== '') {
      this.rootSubscription.add(this.guestInfoService.validateEmail(emailControl.value)
        .subscribe((response: ValidateEmailModel) => {
          if (response) {
            this.handleValidateEmailResponse(response, emailControl);
          }
        }));
    } else {
      this.handleEmptyEmail(emailControl);
    }
    this.maskEmail(emailControl);
  }

  handleValidateEmailResponse(response: ValidateEmailModel, emailControl: FormControl) {
    if (errorReasonCodes.indexOf(response.reasonCode) !== -1) {
      emailControl.setErrors({'notExists': true});
      this.emailValidationDescription = response.reasonDescription;
    } else {
      emailControl.setErrors(null);
    }
  }

  handleEmptyEmail(emailControl: FormControl) {
    const guestInfo = this.guestInformationData ? this.guestInformationData.reservation.guestInfo : null;
    if (guestInfo) {
      if (guestInfo.email != null && guestInfo.email != '') {
        emailControl.setErrors({'doNotDelete': true});
      } else {
        emailControl.setErrors(null);
      }
    }
  }

  maskEmail(emailControl: FormControl) {
    let email = emailControl.value;
    let hide = email.split('@')[0].length - 1;
    if (hide > 0) {
      var r = new RegExp('.{' + hide + '}@', 'g');
      email = email.replace(r, '*'.repeat(hide) + '@');
      emailControl.patchValue(email);
    }
    this.isEmailUpdated = false;
  }

  emailUpdated() {
    this.isEmailUpdated = true;
  }

  onChangeClubNo() {
    if (this.guestInformationData) {
      const reservation: Reservation = this.guestInformationData.reservation;
      const rewardsClubNumber = this.guestInfoFromGroup.get('ihgRcNumber') as FormControl;
      const guestInfo = this.guestInformationData.reservation.guestInfo;
      if (rewardsClubNumber.value === '') {
        this.isEnroll = true;
        this.guestInfoFromGroup.controls['firstName'].patchValue(guestInfo.firstName);
        this.guestInfoFromGroup.controls['lastName'].patchValue(guestInfo.lastName);
        rewardsClubNumber.setErrors(null);
        this.guestInfoService.updatedIHGNo.next(null);
        this.guestInfoService.isInValidIHGNo.next(null);
        this.guestInfoService.warningMessageSource.next(null);
        return;
      }
      this.validateRewardClubNumber(reservation, rewardsClubNumber);
    }
  }

  validateRewardClubNumber(reservation: Reservation, rewardsClubNumber: FormControl) {
    this.rootSubscription.add(this.guestInfoService.validateRewardClubNo(rewardsClubNumber.value,
      reservation.confirmationNumber)
      .subscribe((response: IHGMemberDetail) => {
        this.ihgInfomration = response;
        this.validateMemberDetails(reservation, rewardsClubNumber);
      }));
  }

  private validateMemberDetails(reservation: Reservation, rewardsClubNumber: FormControl) {
    const memberDetails = this.ihgInfomration.memberDetails;
    if (this.ihgInfomration.errors == null && memberDetails) {
      if (reservation.guestInfo.firstName.toLowerCase() !== memberDetails.firstName.toLowerCase()
         || reservation.guestInfo.lastName.toLowerCase() !== memberDetails.lastName.toLowerCase()) {
        const ihgUser = memberDetails.firstName + ` ` + memberDetails.lastName;
        const reservationUser = reservation.guestInfo.firstName + ` ` + reservation.guestInfo.lastName;
        this.guestInfoService.showAlertMessage(ihgUser, reservationUser);
        this.guestInfoFromGroup.controls['firstName'].patchValue(memberDetails.firstName);
        this.guestInfoFromGroup.controls['lastName'].patchValue(memberDetails.lastName);
        this.guestInfoFromGroup.controls['middleName'].patchValue(memberDetails.middleName);
        this.isEnroll = false;
      }
      rewardsClubNumber.setErrors(null);
      this.guestInfoService.isInValidIHGNo.next(null);
    } else {
      rewardsClubNumber.setErrors({'isValid': false});
      this.guestInfoService.isInValidIHGNo.next(true);
      this.guestInfoService.warningMessageSource.next(null);
      this.isEnroll = true;
    }
    this.makeGuestInfoTouched(rewardsClubNumber);
  }

  private makeGuestInfoTouched(rewardsClubNumber: FormControl) {
    rewardsClubNumber.markAsDirty();
    this.guestInfoFromGroup.markAsDirty();
    this.setDataChange(true);
  }

  private translatePhoneTypes() {
    this.phoneTypes.forEach(element => {
      element.text = this.translate.instant(element.text);
    });
  }

  public createForm(): void {
    this.guestInfoFromGroup = this.fb.group({
      ihgRcNumber: [],
      firstName: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(56),
        Validators.pattern('^[a-zA-Z_ ]*$'), FdkValidators.EmptySpaceValidation])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(57), Validators.pattern('^[a-zA-Z_ ]*$'), FdkValidators.EmptySpaceValidation])],
      middleName: ['', Validators.compose([Validators.pattern('^[a-zA-Z_]*$'), FdkValidators.EmptySpaceValidation])],
      addressLine1: [],
      addressLine2: [],
      countryCode: [],
      zipCode: [],
      stateProvince: [],
      cityName: [],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      phoneType: [],
      email: [''],
      cardNumber: ['', Validators.maxLength(16)],
      expirationDate: [],
      nameOnCard: [],
      confirmationNumber: []
    });
  }

  public validateCardNumber() {
    const element = this.input.nativeElement.value;
    const regex = /[^0-9]/gi;
    this.input.nativeElement.value = element.replace(regex, '');
  }

  loadGuestFormGroup() {
    this.handleEnrollmentControls();
    const guestInfo = this.guestInformationData ? this.guestInformationData.reservation.guestInfo : null;
    if (this.guestInfoFromGroup.controls['ihgRcNumber'].value) {
      const rewardsClubNumber = this.guestInfoFromGroup.controls['ihgRcNumber'] as FormControl;
      this.makeGuestInfoTouched(rewardsClubNumber);
    }
    this.guestInfoFromGroup.controls['ihgRcNumber'].patchValue(this.ihgNumber || (this.guestInformationData ? this.guestInformationData.reservation.loyaltyMembershipId : null));
    this.guestInfoFromGroup.controls['confirmationNumber'].patchValue(this.guestInformationData ? this.guestInformationData.reservation.confirmationNumber : null);
    if (guestInfo) {
      this.guestInfoFromGroup.controls['firstName'].patchValue(guestInfo.firstName);
      this.guestInfoFromGroup.controls['lastName'].patchValue(guestInfo.lastName);
      this.guestInfoFromGroup.controls['middleName'].patchValue(guestInfo.middleName);
      this.guestInfoFromGroup.controls['email'].patchValue(guestInfo.email);
      this.guestInfoFromGroup.controls['addressLine1'].patchValue(guestInfo.addressLine1);
      this.guestInfoFromGroup.controls['addressLine2'].patchValue(guestInfo.addressLine2);
      this.guestInfoFromGroup.controls['zipCode'].patchValue(guestInfo.zipCode);
      this.guestInfoFromGroup.controls['countryCode'].patchValue(guestInfo.countryCode && guestInfo.countryCode !== '' ? guestInfo.countryCode : '');
      this.guestInfoFromGroup.controls['cityName'].patchValue(guestInfo.cityName);
      this.guestInfoFromGroup.controls['stateProvince'].patchValue(guestInfo.stateProvince);
      this.guestInfoFromGroup.controls['phoneNumber'].patchValue(guestInfo.phoneNumber);
      this.guestInfoFromGroup.controls['phoneType'].patchValue(guestInfo.phoneType && guestInfo.phoneType !== '' ? guestInfo.phoneType : '1');
    }
    this.hardReset = false;

  }

  private handleEnrollmentControls(): void {
    if (this.guestInformationData && this.guestInformationData.reservation.loyaltyMembershipId != null &&
      this.guestInformationData.reservation.loyaltyMembershipId !== '') {
      this.isClubMember = true;
      this.isEnroll = false;
      this.disableControls();
    } else {
      this.isEnroll = true;
    }
  }

  public disableControls() {
    this.guestInfoFromGroup.controls['ihgRcNumber'].disable();
    this.guestInfoFromGroup.controls['firstName'].disable();
    this.guestInfoFromGroup.controls['lastName'].disable();
    this.guestInfoFromGroup.controls['middleName'].disable();
  }

  private initCountryAndPhoneType() {
    const countryCode = '';
    const phoneTypeValue = this.phoneTypes.value = '6';
    this.guestInfoFromGroup.controls['countryCode'].patchValue(countryCode);
    this.guestInfoFromGroup.controls['phoneType'].patchValue(phoneTypeValue);
  }

  public onSubmit() {
    this.gaService.trackEvent(MANAGE_STAY_CONST.GA.CHCKIN_MNG_STAY, MANAGE_STAY_CONST.GA.SAVE_ACTION, MANAGE_STAY_CONST.GA.SLCTD);
    this.guestInfoPayload.hotelCode = this.getLocationId();
    this.guestInfoPayload.brandCode = this.getBrandCode();
    if (this.actualEmail) {
      this.guestInfoPayload.email = this.actualEmail;
    }
    this.emitGuestInfo.emit(this.guestInfoPayload);
  }

  public openEnrollmentModal() {

  }

  public maskCardNumber() {
    this.getOriginalCardValue();
    const cardNumber = this.guestInfoFromGroup.get('cardNumber') as FormControl;
    const cardNumberValue = cardNumber.value;
    const maskedCardValueInput = this.maskCardNumberInput(cardNumberValue);
    this.guestInfoFromGroup.patchValue({cardNumber: maskedCardValueInput, options: {onlySelf: true}});
  }

  public unMaskCardNumber() {
    this.guestInfoFromGroup.patchValue({cardNumber: this.originalCardNumber, options: {onlySelf: true}});
  }

  private maskCardNumberInput(cardNumber): string {
    const visibleDigits = 4;
    const maskedSection = cardNumber.slice(0, -visibleDigits);
    const visibleSection = cardNumber.slice(-visibleDigits);
    return maskedSection.replace(/./g, 'X') + visibleSection;
  }

  public getOriginalCardValue() {
    const originalCardNumber = this.input.nativeElement.value;
    this.originalCardNumber = originalCardNumber;
  }

  public cancelChanges() {
    if (this.state === AppStateNames.manageStay) {
      this.guestInfoService.triggerCancelEditMode();
      this.cancelEdit.emit();
    } else {
      if (this.reservationNumber) {
        this.setDataChange(false);
        this.router.navigate(['check-in']);
      } else {
        this.router.navigate(['guest-list-details']);
      }
    }
  }

  private registerInTransitionGuard() {
    this.transitionGuardService.register(<GuardRegistrationParams>{
      dataChangesObserver: this.guestInfoService.guestInfoChangeSubject.asObservable(),
      useCancelSaveDiscardModal: false,
      userConfirmModalConfig: this.guestInfoService.getConfirmModalConfig(),
      noToastMessage: true,
      onSave: null,
      onDiscard: () => {
        return this.handleConfirmActions(true);
      },
      onCancel: () => {
        return this.handleConfirmActions(false);
      }
    });
  }

  private handleConfirmActions(shouldDiscard: boolean): Observable<boolean> {
    const subject = new Subject<boolean>();
    if (shouldDiscard) {
      this.loadGuestFormGroup();
    }
    setTimeout(() => {
      subject.next(shouldDiscard);
      this.setDataChange(!shouldDiscard);
    });
    return subject.asObservable();
  }

  private setDataChange(dataChanged: boolean): void {
    this.guestInfoService.guestInfoChangeSubject.next(dataChanged);
  }

  public openRewardsMemberModal(): void {
    const modalRef = this.modalService.open(LookUpRewardsMemberModalComponent, {windowClass: 'modal-lookup'});
    this.rootSubscription.add(modalRef.componentInstance.ihgRcNumber.subscribe((ihgRcNumber: string) => {
      this.guestInfoFromGroup.get('ihgRcNumber').patchValue(ihgRcNumber);
      this.onChangeClubNo();
    }));
  }

  private getBrandCode(): string {
    return this.userService.getCurrentLocation().brandCode;
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  // This method is only using to control maxlength of first Name and Last Name fields on Tab
  public checkMaxLength(control: any, length: any): void {
    const controlValue = control.value;
    if (controlValue.length > length) {
      control.patchValue(controlValue.toString().substring(0, length));
    }
  }
}
