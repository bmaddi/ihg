import { Country } from '../models/guest-info.model';
export const DEFAULT_COUNTRY : Country = {
    "countryName": "Select",
    "countryCode": "",
    "internationalCallingCode": "0"
  }; 

export const PHONE_TYPE:  any = [
    { "text": "LBL_HOME", "value": "6" },
    { "text": "LBL_PHONE_TYPE_MOBILE", "value": "5" },
    { "text": "LBL_BUSINESS", "value": "7" },
    { "text": "LBL_PHONE_TYPE_OTHER", "value": "1" },
  ];

export const errorReasonCodes: any = [221, 222, 223, 224, 225, 226, 301, 302, 303, 304, 305, 306, 307, 311];
