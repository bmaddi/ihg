import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TranslateModule } from '@ngx-translate/core';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { GuestInformationComponent } from './components/guest-information/guest-information.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppSharedModule } from '@modules/shared/app-shared.module';
import { RewardsNumberLookUpModule } from '@modules/rewards-number-look-up/rewards-number-look-up.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IhgNgCommonComponentsModule,
    ReactiveFormsModule,
    TranslateModule,
    DatePickerModule,
    DropDownsModule,
    DateInputsModule,
    NgbModule,
    AppSharedModule,
    RewardsNumberLookUpModule
  ],
  declarations: [
    GuestInformationComponent   
  ],
  exports: [
    GuestInformationComponent
  ]
})
export class GuestInfoModule { }
