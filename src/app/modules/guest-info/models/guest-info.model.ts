import { AlertType, AlertButton, Alert } from 'ihg-ng-common-core';

import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export interface GuestInformationModel {
  reservation: Reservation;
}

export interface Reservation {
    loyaltyMembershipId: string | number;
    corporateId: string | number;
    reservationNumber: string;
    hotelCode: string ;
    appUserId: string;
    roomCategoryCode: string;
    confirmationNumber: string;
    guestInfo: GuestInfo;
}

export interface Country {
  countryName: string;
  countryCode: string;
  internationalCallingCode: string;
}

export interface GuestInfo {
    firstName: string;
    lastName: string;
    middleName: string;
    addressLine1: string;
    addressLine2: string;
    countryCode: string | number;
    countryName: string;
    zipCode: string | number;
    stateProvince: string;
    cityName: string;
    phoneNumber: string;
    phoneType: string;
    email: string;
    cardNumber?: string;
    expirationDate?: string;
    nameOnCard?: string;
    confirmationNumber?: string;
    hotelCode?: string;
    brandCode?: string;
}


export interface AdditionalAlert {
  type: AlertType;
  message: string;
  retryButton?: string;
  detailMessage: string;
  dismissible: boolean;
  buttons?: AlertButton[];
  translateValues?: any;
  id?: number;
  index?: number;
  animation?: boolean;
  otherMessages?: Alert[];
}

export interface IHGMemberDetail {
  memberDetails: MemberDetails;
  errors?: Array<ErrorsModel>;
}

export interface MemberDetails {
  ihgRcNumber: string;
  ihgRcMembership: string;
  ihgRcPoints: number;
  badges: any;
  firstName: string;
  lastName: string;
  middleName: string;
}


