import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';

import { Alert, AlertButtonType, AlertType, UserConfirmationModalConfig, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { ValidateEmailModel } from 'ihg-ng-common-pages';
import { environment } from '@env/environment';
import { AdditionalAlert, Country, GuestInfo, IHGMemberDetail } from '../models/guest-info.model';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Injectable({
  providedIn: 'root'
})
export class GuestInfoService {

  public hasUnsavedGuestInfo = false;
  private guestInfoCancelSubject: BehaviorSubject<{ editMode: boolean, refreshRequired: boolean }> = new BehaviorSubject(null);
  public resetGuestInfoForm: BehaviorSubject<any> = new BehaviorSubject(false);
  public guestInfomation: GuestInfo;
  public messageSource = new BehaviorSubject<Alert>(null);
  currentMessage = this.messageSource.asObservable();
  public hasDataChanges: any;

  public warningMessageSource = new BehaviorSubject<Alert>(null);
  additioanlWarningMessage = this.warningMessageSource.asObservable();
  public updatedIHGNo: BehaviorSubject<string> = new BehaviorSubject(null);
  public isInValidIHGNo: BehaviorSubject<boolean> = new BehaviorSubject(null);

  private readonly API_URL = environment.fdkAPI;

  public guestInfoChangeSubject: Subject<boolean> = new Subject<boolean>();

  private ihgRcNumberValidationSource = new Subject<IHGMemberDetail>();
  public ihgRcNumberValidation = this.ihgRcNumberValidationSource.asObservable();

  private navigateToCheckInSubject = new Subject<null>();
  public navigate$ = this.navigateToCheckInSubject.asObservable();

  private navigateToGuestListSubject = new Subject<null>();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private modalService: ConfirmationModalService) { }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public getCountries(): Observable<Country[]> {
    const apiURl = `${environment.apiUrl}enrollmentSummary/countries`;
    return this.http.get(apiURl).pipe(map((response: Country[]) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  showSuccessMessage(state?:string) {
    this.warningMessageSource.next(null);
    this.messageSource.next(state === AppStateNames.manageStay ? this.getSaveSuccessToast() : this.getSuccessToast());
  }

  public validateEmail(email: string): Observable<ValidateEmailModel> {
    const apiURl = `${environment.apiUrl}enrollmentSummary/validateEmail?emailId=${email}`;
    return this.http.get(apiURl).pipe(map((response: ValidateEmailModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getGuestInformation(reservationNumber: string): Observable<any> {
    const apiURl = `${this.API_URL}reservation/fetchDetails/${this.getLocationId()}?reservationNumber=${reservationNumber}`;
    return this.http.get(apiURl).pipe(map((response: any) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public saveGuestInformation(guestInfo: any): Observable<any> {
    this.guestInfomation = guestInfo;
    const apiURl = `${this.API_URL}reservation/modify/primaryGuestDetails/${this.getLocationId()}`;
    return this.http.put(apiURl, guestInfo).pipe(map((response: any) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public validateRewardClubNo(reservationNumber: string, confirmationNumber: string): Observable<IHGMemberDetail> {
    const apiURl = `${this.API_URL}loyalty/memberDetails/reservation/${this.getLocationId()}?membershipId=${reservationNumber}&confirmationNumber=${confirmationNumber}`;
    return this.http.get(apiURl).pipe(map((response: IHGMemberDetail) => {
      this.ihgRcNumberValidationSource.next(response);
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        this.ihgRcNumberValidationSource.next(null);
        return throwError(error);
      })
    );
  }

  openUnsavedDataWarningModal() {
    return this.modalService.openConfirmationModal(this.getConfirmModalConfig());
  }

  getConfirmModalConfig(): UserConfirmationModalConfig {
    return {
      modalHeader: 'COM_SAVE_DISCARD_MOD_TITLE',
      primaryButtonLabel: 'COM_BTN_CONT',
      primaryButtonClass: 'btn-primary',
      defaultButtonLabel: 'COM_BTN_CANCEL',
      defaultButtonClass: 'btn-default',
      messageHeader: 'LBL_GST_UPDATE_TEXT',
      messageBody: '',
      windowClass: 'modal-medium',
      dismissible: true
    };
  }

  public getSuccessToast(): Alert {
    return {
      type: AlertType.Success,
      message: 'LBL_CONF_SUCCS',
      detailMessage: 'LBL_CONF_SUCCS_DESC',
      dismissible: false,
      buttons: [{
        label: 'LBL_BK_TO_CHK_IN',
        type: AlertButtonType.Success,
        onClick: () => {
          this.navigateToCheckInSubject.next();
        }
      }]
    };
  }

  public getSaveSuccessToast(): Alert {
    return {
      type: AlertType.Success,
      message: 'LBL_CONF_SUCCS',
      detailMessage: 'LBL_CONF_SUCCS_DESC',
      dismissible: false,
      buttons: [{
        label: 'LBL_BACK_TO_GUESTLIST',
        type: AlertButtonType.Success,
        onClick: () => {
          this.navigateToGuestListSubject.next();
        }
      }]
    };
  }

  public triggerCancelEditMode(cancelEvent = {editMode: false, refreshRequired: false}) {
    this.guestInfoCancelSubject.next(cancelEvent);
  }

  public getCancelEditObservable(): Observable<{ editMode: boolean, refreshRequired: boolean }> {
    return this.guestInfoCancelSubject.asObservable()
      .pipe(
        filter(data => data !== null)
      );
  }

  public getGuestListNavigationObservable(): Observable<boolean> {
    return this.navigateToGuestListSubject.asObservable()
      .pipe(
        filter(data => data !== null)
      );
  }

  public showAlertMessage(ihgUser: string, reservationUser: string) {
    this.messageSource.next(null);
    this.warningMessageSource.next(this.setAlertMessage(ihgUser, reservationUser));
  }

  private setAlertMessage(ihgUser: string, reservationUser: string): AdditionalAlert {
    return {
      type: AlertType.Warning,
      message: 'COM_TOAST_WARNING',
      detailMessage: 'LBL_RCN_NAME_CHANGE',
      dismissible: false,
      otherMessages: [
        {
          type: AlertType.Warning,
          message: 'LBL_CUR_R',
          detailMessage: reservationUser,
          dismissible: false
        },
        {
          type: AlertType.Warning,
          message: 'LBL_IHGRW_NO',
          detailMessage: ihgUser,
          dismissible: false
        },
      ]
    };
  }

}
