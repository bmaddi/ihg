import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement, TemplateRef, ViewChild } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, of, throwError } from 'rxjs';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';

import { EnvironmentService, GoogleAnalyticsService, PageAccessService, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalComponent, ConfirmationModalService } from 'ihg-ng-common-components';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { GuestComplaintsCountComponent } from './guest-complaints-count.component';
import { GuestRelationsService } from '../../services/guest-relations/guest-relations.service';
import { GuestRelationsAnalyticsService } from '../../services/guest-relations-analytics-service/guest-relations-analytics-service';
import {
  environmentServiceServiceStub,
  gcAdditonalMockComplaints,
  googleAnalyticsServiceStub,
  guestRelationsServiceMockComplaints
} from '../../mocks/guest-relations.mock';
import { GuestComplaintsModalComponent } from '../guest-complaints-modal/guest-complaints-modal.component';
import { ServiceErrorComponent } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { GUEST_COUNT_ERROR_CONTENT } from '@modules/prepare/constants/prepare-error-constants';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Component({
  template: `
    <ng-template #div1
                 let-count="count">
      <div class="mock__template">{{count}}</div>
    </ng-template>
  `,
})
class WrapperComponent {
  @ViewChild('div1') template: TemplateRef<any>;
}

describe('GuestComplaintsCountComponent', () => {
  let component: GuestComplaintsCountComponent;
  let fixture: ComponentFixture<GuestComplaintsCountComponent>;
  let spy: jasmine.Spy;
  let guestRelationsService: GuestRelationsService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: {openAutofilledReportIssueModal: (t): Observable<string> => of(t)}
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
      .configureTestingModule({
        imports: [
          HttpClientTestingModule,
          TranslateModule.forChild(),
          NgbModule
        ],
        declarations: [
          GuestComplaintsCountComponent,
          WrapperComponent,
          GuestComplaintsModalComponent,
          ConfirmationModalComponent,
          ServiceErrorComponent
        ],
        providers: [
          GuestRelationsService,
          EnvironmentService,
          UserService,
          ConfirmationModalService,
          PageAccessService,
          TranslateService,
          ReportIssueService,
          GuestRelationsAnalyticsService,
          GoogleAnalyticsService,
          TranslateStore
        ]
      })
      .overrideModule(BrowserDynamicTestingModule,
        {set: {entryComponents: [GuestComplaintsModalComponent, ConfirmationModalComponent]}})
      .overrideComponent(GuestComplaintsModalComponent, {
        set: {
          template: `<button class="btn btn-primary test__button" (click)="close()">GuestComplaintsModalComponent Close</button> `
        }
      })
      .overrideComponent(ConfirmationModalComponent, {
        set: {
          template: `<button class="btn btn-primary test__button" (click)="activeModal.dismiss()">ConfirmationModalComponent Close</button>`
        }
      })
      .overrideComponent(ServiceErrorComponent, {
        set: {
          providers: [],
          template: `   <div>
                <div class="error-body error-color" [translate]="message"></div>
                <span *ngIf="!hasReachedMaxError() && showRefreshBtn"
                      class="pt-2 btn-link cursor-pointer ref-btn"
                      [translate]="refreshActionText || 'COM_BTN_RFRSH'"
                      (click)="onRefreshClick()" >
                </span>
                <span *ngIf="showReportAnIssue && showRefreshBtn"
                class="pt-2 btn-link cursor-pointer ref-btn"
                translate="REPORT_AN_ISSUE_MENU"
                (click)="onReportIssue()" ></span>
            </div>`,
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    guestRelationsService = TestBed.get(GuestRelationsService);
    fixture = TestBed.createComponent(GuestComplaintsCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.guestComplaintsCount).toEqual(0);
    expect(component.seleniumTag).toEqual('GuestComplaints');
    expect(component.errorConfig).toEqual(GUEST_COUNT_ERROR_CONTENT);
    expect(component.seleniumTag).toBeDefined();
    expect(component.errorCardType).toEqual(ERROR_CARD_TYPE.LINE_ERROR);
  });

  it('should not display on load', () => {
    expect(component.guestComplaintsCount).toEqual(0);
    expect(component.seleniumTag).toEqual('GuestComplaints');
  });

  it('should validate Summary of Reason Categories is correctly generated', () => {
    expect(component.reasonCategories).toEqual([]);
    component['entryReasonCategories'](guestRelationsServiceMockComplaints());

    fixture.detectChanges();

    expect(component.reasonCategories).toBeTruthy();
    expect(component.reasonCategories).toBeDefined();
    expect(component.reasonCategories.some((rc) => {
      return rc.category === 'Billing Issues';
    })).toBeTruthy();
  });

  it('should display un-categorized Reason category if the value received as null', () => {
    expect(component.reasonCategories).toEqual([]);
    expect(component.reasonCategories).toBeDefined();

    component['entryReasonCategories'](guestRelationsServiceMockComplaints());
    fixture.detectChanges();

    expect(component.reasonCategories.some((rc) => {
      return rc.category === 'Uncategorized';
    })).toBeTruthy();
  });

  it('should validate sorting of category list based on count High to Low', () => {
    const mock = gcAdditonalMockComplaints();

    component['entryReasonCategories']([...mock, ...guestRelationsServiceMockComplaints()]);
    fixture.detectChanges();

    const expectedOutPut = [3, 2, 1];
    expectedOutPut.forEach((expectedCount, index) => {
      expect(component.reasonCategories[index].count).toEqual(expectedCount);
    });
  });

  it('should validate secondary sorting will be by Category (A-Z)', () => {
    expect(component.reasonCategories).toEqual([]);
    const mock = gcAdditonalMockComplaints();

    component['entryReasonCategories']([...mock, ...guestRelationsServiceMockComplaints()]);
    fixture.detectChanges();

    const expectedOutput = ['Billing Issues', 'RC Program', 'Uncategorized'];
    expectedOutput.forEach((expected, index) => {
      expect(component.reasonCategories[index].category).toEqual(expected);
    });
  });

  it('sorting with both count and (A-Z)', () => {
    expect(component.reasonCategories).toEqual([]);
    expect(component.reasonCategories).toBeDefined();

    component['entryReasonCategories'](guestRelationsServiceMockComplaints());
    fixture.detectChanges();

    const expectedOutput = [
      {
        category: 'RC Program',
        count: 2
      },

      {
        category: 'Billing Issues',
        count: 1
      },

      {
        category: 'Uncategorized',
        count: 1
      }
    ];

    expect(component.reasonCategories).toBeDefined();
    expect(component.reasonCategories).toEqual(expectedOutput);

  });

  it('should validate clicking number displays the modal (Prepare/CheckIn)', () => {
    const mockData = guestRelationsServiceMockComplaints();
    component.ihgRcNumber = '12321231';

    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(mockData));
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough();
    const openComplaintsGridSpy = spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const guestRelationsSpy = spyOn(TestBed.get(GuestRelationsService), 'hasGuestComplaintsAccess').and.returnValue(true);

    const element = fixture.debugElement.query(By.css('.guest-complaints__counts'));
    expect(element).not.toBeNull();
    expect(element.nativeElement.textContent).toEqual('(2)');
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestRelationsSpy).toHaveBeenCalled();
    expect(openComplaintsGridSpy).toHaveBeenCalled();
  });

  it('should validate on clicking count will display only those reason categories in the modal (Prepare/CheckIn)', () => {
    const mock = gcAdditonalMockComplaints();
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(mock));
    component.ihgRcNumber = '12321231';

    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough();
    const openComplaintsGridSpy = spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const guestRelationsSpy = spyOn(TestBed.get(GuestRelationsService), 'hasGuestComplaintsAccess').and.returnValue(true);

    const element = fixture.debugElement.query(By.css('.guest-complaints__counts'));
    expect(element).not.toBeNull();
    expect(element.nativeElement.textContent).toEqual('(2)');
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestRelationsSpy).toHaveBeenCalled();
    expect(openComplaintsGridSpy).toHaveBeenCalled();
  });

  it('should validate total records on the modal to be same as count clicked (Prepare/CheckIn)', () => {
    const mock = gcAdditonalMockComplaints();
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(mock));
    component.ihgRcNumber = '12321231';


    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough();
    const openComplaintsGridSpy = spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const guestRelationsSpy = spyOn(TestBed.get(GuestRelationsService), 'hasGuestComplaintsAccess').and.returnValue(true);

    const element = fixture.debugElement.query(By.css('.guest-complaints__counts'));
    expect(element).not.toBeNull();
    expect(element.nativeElement.textContent).toEqual('(2)');
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestRelationsSpy).toHaveBeenCalled();
    expect(openComplaintsGridSpy).toHaveBeenCalled();
    fixture.detectChanges();
    TestBed.get(NgbModal).dismissAll();

    component['entryReasonCategories'](guestRelationsServiceMockComplaints());
    fixture.detectChanges();
    expect(component.reasonCategories).toBeTruthy();
    expect(component.reasonCategories).toBeDefined();
  });

  it('should Validate clicking View All displays the modal (Prepare/CheckIn)', () => {
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));


    component.ihgRcNumber = '12321231';

    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough();
    const openComplaintsGridSpy = spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const guestRelationsSpy = spyOn(TestBed.get(GuestRelationsService), 'hasGuestComplaintsAccess').and.returnValue(true);

    const element = fixture.debugElement.query(By.css('.view-all'));
    expect(element).not.toBeNull();
    expect(element.nativeElement.textContent).toEqual('LBL_VIEW_ALL (4)');
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestRelationsSpy).toHaveBeenCalled();
    expect(openComplaintsGridSpy).toHaveBeenCalled();
  });

  it('Validate total records on the modal to be same as View All count clicked (Prepare/CheckIn)', () => {
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough();
    const openComplaintsGridSpy = spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const guestRelationsSpy = spyOn(TestBed.get(GuestRelationsService), 'hasGuestComplaintsAccess').and.returnValue(true);

    component.ihgRcNumber = '12321231';

    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();

    const element = fixture.debugElement.query(By.css('.view-all'));
    expect(element).not.toBeNull();
    expect(element.nativeElement.textContent).toEqual('LBL_VIEW_ALL (4)');
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestRelationsSpy).toHaveBeenCalled();
    expect(openComplaintsGridSpy).toHaveBeenCalled();
    fixture.detectChanges();

    component['entryReasonCategories'](guestRelationsServiceMockComplaints());
    fixture.detectChanges();
    expect(component.reasonCategories).toBeTruthy();
    expect(component.reasonCategories).toBeDefined();
  });

  it('should get and populate Guest Complaints count', () => {
    component.ihgRcNumber = '12321231';
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));
    component.ngOnInit();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(component.guestComplaintsCount).toBeGreaterThanOrEqual(1);
  });

  it('guestComplaints counts are not shown', () => {
    component.ihgRcNumber = '12321231';
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of([]));
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.guestComplaintsData).toEqual([]);
  });

  it('if no ihgRcNumber it should Not make guest complaints API call', () => {
    component.ihgRcNumber = null;
    spy = spyOn(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));

    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).not.toHaveBeenCalled();
  });

  it('if ihgRcNumber is present it should make guest complaints API call and show default template', () => {
    spy = spyOn(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));

    component.ihgRcNumber = '12321231';
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();

    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="GuestComplaintsContainer-SID"]'));
    expect(element).not.toBeNull();
  });

  it('if ihgRcNumber & Template Ref is provided display provided template', () => {
    const wrapper = TestBed.createComponent(WrapperComponent);
    const wrapperComponent = wrapper.debugElement.componentInstance;

    component.ihgRcNumber = '12321231';
    component.countDisplayTemplate = wrapperComponent.template;
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(component.guestComplaintsCount).toBeGreaterThan(0);
    const element = fixture.debugElement.queryAll(By.css('.mock__template'));
    expect(element).not.toBeNull();
  });

  // tslint:disable-next-line:max-line-length
  it('should check guest complaints access on guest-complaints__counts link click and show no access modal if user has no access to guest complaints', () => {
    spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));
    spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough();
    const guestAnalyticsSpy = spyOn(TestBed.get(GuestRelationsAnalyticsService), 'trackNoAccessToClick').and.callThrough();
    const accessDeniedWarningSpy = spyOn(guestRelationsService, 'accessDeniedWarning').and.callThrough();
    component.ihgRcNumber = '12321231';

    component.ngOnInit();
    fixture.detectChanges();

    expect(guestRelationsService.getGuestComplaintsData).toHaveBeenCalled();
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CountLink0-SID"]'));
    expect(element).not.toBeNull();
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestAnalyticsSpy).toHaveBeenCalled();
    expect(accessDeniedWarningSpy).toHaveBeenCalled();
  });

  // tslint:disable-next-line:max-line-length
  it('should check guest complaints access on guest-complaints__counts link click and open Complaints Grid modal if user has access to guest complaints', () => {
    spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(of(guestRelationsServiceMockComplaints()));
    spyOn<any>(guestRelationsService, 'openComplaintsGrid').and.returnValue(of(null));
    const openGuestComplaintsModalSpy = spyOn(component, 'openGuestComplaintsModal').and.callThrough(),
      openComplaintsGridSpy = spyOn<any>(component, 'openComplaintsGrid').and.callThrough(),
      guestRelationsSpy = spyOn(TestBed.get(GuestRelationsService), 'hasGuestComplaintsAccess').and.returnValue(true);
    component.ihgRcNumber = '12321231';
    component.stateName = AppStateNames.guestListDetails;


    component.ngOnInit();
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CountLink0-SID"]'));
    expect(element).not.toBeNull();
    element.triggerEventHandler('click', null);

    expect(openGuestComplaintsModalSpy).toHaveBeenCalled();
    expect(guestRelationsSpy).toHaveBeenCalled();
    expect(openComplaintsGridSpy).toHaveBeenCalled();
  });

  it('should call the correct function when there is a server error', () => {
    component.ihgRcNumber = '12321231';
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(throwError({
      status: 500
    }));

    component.ngOnInit();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(component.hasErrors).toBeTruthy();
  });

  it('should display the template correctly when there is a server error', () => {

    component.ihgRcNumber = '12321231';

    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(throwError({
      status: 500
    }));

    const processHttpErrorsSpy = spyOn<any>(component, 'handleResponseFailure').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    expect(processHttpErrorsSpy).toHaveBeenCalled();
    const debugEl: DebugElement = fixture.debugElement;
    const gcElement = debugEl.queryAll(By.css('.errorTemplate'));
    expect(gcElement).toBeDefined();
    expect(gcElement).toBeTruthy();
  });

  it('should click on Refresh button', () => {
    component.ihgRcNumber = '12321231';
    spy = spyOn<any>(guestRelationsService, 'getGuestComplaintsData').and.returnValue(throwError({
      status: 500
    }));

    const processHttpErrorsSpy = spyOn<any>(component, 'handleResponseFailure').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(processHttpErrorsSpy).toHaveBeenCalled();
  });

});
