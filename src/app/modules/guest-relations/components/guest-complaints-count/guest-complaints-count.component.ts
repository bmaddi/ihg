import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';
import { countBy, flatMap, includes, isEmpty, map, orderBy } from 'lodash';

import { AppConstants } from '@app/constants/app-constants';
import { GuestRelationsService } from '../../services/guest-relations/guest-relations.service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GUEST_COUNT_ERROR_CONTENT } from '@app/modules/prepare/constants/prepare-error-constants';
import { GUEST_COMPLAINTS_COUNT_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ERROR_CARD_TYPE, SERVICE_ERROR_TYPE } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { GuestRelationsAnalyticsService } from '../../services/guest-relations-analytics-service/guest-relations-analytics-service';
import { ServiceErrorComponent } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { GuestComplaint } from '../../interfaces/guest-complaints.interface';

@Component({
  selector: 'app-guest-complaints-count',
  templateUrl: './guest-complaints-count.component.html',
  styleUrls: ['./guest-complaints-count.component.scss']
})
export class GuestComplaintsCountComponent extends AppErrorBaseComponent implements OnInit {
  @ViewChild(ServiceErrorComponent) serviceErrorRef: ServiceErrorComponent;

  @Input() ihgRcNumber: string;
  @Input() countDisplayTemplate: TemplateRef<any>;
  @Input() stateName: string;

  guestComplaintsCount = 0;
  preValueTime = 36;
  guestComplaintsData: GuestComplaint[];
  seleniumTag = 'GuestComplaints';
  hasGuestComplaintsAccess = this.guestRelationsService.hasGuestComplaintsAccess();
  errorConfig = GUEST_COUNT_ERROR_CONTENT;
  reportIssueAutoFill;
  errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  reasonCategories = [];
  unCategorized = 'Uncategorized';

  constructor(
    private guestRelationsService: GuestRelationsService,
    private gaAnalyticsService: GuestRelationsAnalyticsService
  ) {
    super();
  }

  ngOnInit() {
    this.setReportingIssueAutoFill();
    this.getGuestComplaintsCount();
  }

  private setReportingIssueAutoFill(): void {
    if (this.stateName && this.stateName === 'guest-list-details') {
      this.reportIssueAutoFill = GUEST_COMPLAINTS_COUNT_AUTOFILL.prepare;
    } else {
      this.reportIssueAutoFill = GUEST_COMPLAINTS_COUNT_AUTOFILL.checkIn;
    }
  }

  openGuestComplaintsModal(rcData?: string) {
    if (this.guestRelationsService.hasGuestComplaintsAccess()) {
      this.gaAnalyticsService.trackAccessToClickGuestCount(this.stateName);
      let data;
      if (rcData) {
        data = this.getReasonCategoryData(rcData);
      } else {
        data = this.guestComplaintsData;
      }
      this.openComplaintsGrid(data);
    } else {
      this.gaAnalyticsService.trackNoAccessToClick(this.stateName);
      this.guestRelationsService.accessDeniedWarning('LBL_WRN_NO_ACCSS_FOR_GST_COMP').subscribe();
    }
  }

  private getGuestComplaintsCount() {
    if (this.ihgRcNumber) {
      const startDate = moment().subtract(this.preValueTime, 'months').format(AppConstants.DB_DT_FORMAT);
      const endDate = moment().format(AppConstants.DB_DT_FORMAT);
      this.guestRelationsService.getGuestComplaintsData(startDate, endDate, this.ihgRcNumber)
        .pipe(take(1))
        .subscribe(
          data => this.handleSuccessfulResponse(data),
          error => this.handleResponseFailure(error)
        );
    }
  }

  private handleSuccessfulResponse(data: GuestComplaint[]) {
    if (!super.checkIfAnyApiErrors(data)) {
      this.entryReasonCategories(data);
      this.guestComplaintsData = data;
      this.guestComplaintsCount = data.length;
    } else if (this.serviceErrorRef.errorType === SERVICE_ERROR_TYPE.NO_DATA) {
      this.guestComplaintsCount = null;
    } else {
      this.sendGaTrackEvents();
    }
  }

  private entryReasonCategories(data) {
    const entries = map(countBy(flatMap(data, 'reasonCategories')), (value, prop) => {
      return {category: prop, count: value};
    });

    const unCategorized = data.filter((gc) => !gc.reasonCategories.length);
    if (unCategorized.length) {
      entries.push({category: this.unCategorized, count: unCategorized.length});
    }
    this.reasonCategories = orderBy(entries, ['count', 'category'], ['desc', 'asc']);
  }

  private handleResponseFailure(error: HttpErrorResponse) {
    super.processHttpErrors(error);
    this.sendGaTrackEvents();
  }

  private sendGaTrackEvents() {
    if (this.serviceErrorRef.errorType === SERVICE_ERROR_TYPE.GENERAL) {
      this.gaAnalyticsService.trackGuestRelationsCountError(this.stateName);
    } else {
      this.gaAnalyticsService.trackGuestRelationsCountTimeoutError(this.stateName);
    }
  }

  getReasonCategoryData(rcData: string): GuestComplaint[] {
    this.gaAnalyticsService.trackReasonCategoryClick(this.stateName, rcData);
    const complaintsData = this.guestComplaintsData;
    return complaintsData.filter(r => {
      return rcData === this.unCategorized ?
        isEmpty(r.reasonCategories) : includes(r.reasonCategories, rcData);
    });
  }

  private openComplaintsGrid(data: GuestComplaint[]) {
    this.guestRelationsService.openComplaintsGrid(data, this.stateName).subscribe(
      () => {},
      () => {}
    );
  }
}
