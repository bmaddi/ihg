import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GridComponent, GridModule } from '@progress/kendo-angular-grid';
import { By } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { ApiService, EnvironmentService, GoogleAnalyticsService, PageAccessService, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { GuestComplaintsGridComponent } from './guest-complaints-grid.component';
import {
  environmentServiceServiceStub,
  googleAnalyticsServiceStub,
  guestComplaintDescription1,
  guestComplaintDescription3,
  guestRelationsServiceMockComplaints,
  mockHinAPIURL
} from '../../mocks/guest-relations.mock';
import { GuestRelationsAnalyticsService } from '../../services/guest-relations-analytics-service/guest-relations-analytics-service';

import {
  GUEST_ANALYTICS_CONST,
  GuestComplaintsDesktopColumns,
  GuestComplaintsLandscapeColumns,
  GuestComplaintsPortraitColumns
} from '../../constants/guest-relations.constants';
import { ServiceErrorComponent } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { GuestComplaint } from '../../interfaces/guest-complaints.interface';
import { MaxHeightViewMoreComponent } from '@app-shared/components/max-height-view-more/max-height-view-more.component';

describe('GuestComplaintsGridComponent', () => {
  let component: GuestComplaintsGridComponent;
  let fixture: ComponentFixture<GuestComplaintsGridComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: { openAutofilledReportIssueModal: (t): Observable<string> => of(t) }
      })
      .configureTestingModule({
        imports: [
          TranslateModule.forChild(),
          NgbModule,
          GridModule,
          IhgNgCommonKendoModule,
          HttpClientTestingModule
        ],
        declarations: [
          GuestComplaintsGridComponent,
          ServiceErrorComponent,
          MaxHeightViewMoreComponent
        ],
        providers: [
          ApiService,
          EnvironmentService,
          UserService,
          ConfirmationModalService,
          PageAccessService,
          TranslateStore,
          GuestRelationsAnalyticsService,
          GoogleAnalyticsService,
          ReportIssueService,
          TitleCasePipe
        ],
      })
      .overrideComponent(ServiceErrorComponent, {
        set: {
          providers: [],
          template: `<div class="error-body"></div>`
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestComplaintsGridComponent);
    component = fixture.componentInstance;
    component.gridState = {
      take: 3,
      skip: 0
    };
    component.ihgRcNumber = '123253223452';
    component.guestComplaints = guestRelationsServiceMockComplaints();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create and set default properties', () => {
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.isDesktopView).toBeDefined();
    expect(component.gridColumns).toBeDefined();
    expect(component.isTabletPortraitView).toBeDefined();
  });

  it('should display guest complaints in the grid with no pagination enabled if less grid state take', () => {
    component.guestComplaints = [{
      'caseNumber': 10422212463,
      'openTime': '26SEP2019',
      'hotelStatus': 'Open',
      // tslint:disable-next-line:max-line-length
      'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
      'reasonCategories': [],
      'actionCodes': []
    },
      {
        'caseNumber': 10422212464,
        'openTime': '26SEP2019',
        'hotelStatus': 'Open',
        // tslint:disable-next-line:max-line-length
        'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
        'reasonCategories': [],
        'actionCodes': []

      }];
    fixture.detectChanges();
    expect(component.gridState.take).toEqual(3);

    component.ngOnInit();
    fixture.detectChanges();

    expect(component.guestComplaints.length).toEqual(2);

    const gridComponent: GridComponent = fixture.debugElement.query(By.directive(GridComponent)).componentInstance;
    let items = 0;
    gridComponent.view.forEach(s => items++);
    expect(items).toBeLessThanOrEqual(3);

    const nextButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-next-buttons'));
    const previousButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-prev-buttons'));
    nextButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeDefined();
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });
    previousButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });
  });

  it('should display guest complaints data in the grid with pagination enabled if greater than take', () => {

    const mockData = [];

    for (let i = 0; i < 12; i++) {
      mockData.push({
        'caseNumber': 10422212463,
        'openTime': '26SEP2019',
        'hotelStatus': 'Open',
        // tslint:disable-next-line:max-line-length
        'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
        'reasonCategories': [],
        'actionCodes': []
      });
    }

    component.guestComplaints = mockData;
    fixture.detectChanges();

    component.ngOnInit();
    fixture.detectChanges();

    expect(component.guestComplaints.length).toEqual(12);

    const gridComponent: GridComponent = fixture.debugElement.query(By.directive(GridComponent)).componentInstance;
    expect(gridComponent.view.length).toEqual(3);
    expect(gridComponent.skip).toEqual(0);

    const nextButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-next-buttons'));
    const previousButtons: DebugElement = fixture.debugElement.query(By.css('.grid-pager-prev-buttons'));

    previousButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });

    nextButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeFalsy();
    });

    nextButtons.children[0].nativeElement.click();
    expect(gridComponent.skip).toEqual(3);

    fixture.detectChanges();

    previousButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeFalsy();
    });
    nextButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeFalsy();
    });

    nextButtons.children[1].nativeElement.click();
    expect(gridComponent.skip).toEqual(9);

    fixture.detectChanges();

    previousButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeFalsy();
    });
    nextButtons.children.forEach(c => {
      expect(c.classes['pager-link-disabled']).toBeTruthy();
    });
  });

  it('on screen resize view port properties should be set according to screen size conditions', () => {
    const setViewEnablersSpy = spyOn<any>(component, 'setViewEnablers').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    expect(setViewEnablersSpy).toHaveBeenCalled();
    expect(component.isDesktopView).toBeDefined();
    expect(component.gridColumns).toBeDefined();
    expect(component.isTabletPortraitView).toBeDefined();

    component['setViewEnablers'](750);
    fixture.detectChanges();
    expect(component.isDesktopView).toBeFalsy();
    expect(component.isTabletPortraitView).toBeTruthy();
    expect(component.gridColumns).toEqual(GuestComplaintsPortraitColumns);

    component['setViewEnablers'](820);
    fixture.detectChanges();
    expect(component.isDesktopView).toBeFalsy();
    expect(component.isTabletPortraitView).toBeFalsy();
    expect(component.gridColumns).toEqual(GuestComplaintsLandscapeColumns);

    component['setViewEnablers'](2000);
    fixture.detectChanges();
    expect(component.isDesktopView).toBeTruthy();
    expect(component.isTabletPortraitView).toBeFalsy();
    expect(component.gridColumns).toEqual(GuestComplaintsDesktopColumns);
  });

  it('should display the grid with proper column headers on creation', () => {
    const gridColumnHeaders = fixture.debugElement.queryAll(By.css('.k-header'));
    expect(gridColumnHeaders).toBeDefined();

    const columnsInSpecifiedOrder = ['LBL_DATE', 'LBL_REASON', 'COM_LBL_DESCRIPTION', 'LBL_ACTION_TAKEN', 'LBL_STATUS'];
    gridColumnHeaders.forEach((g, i) => {
      const header: DebugElement = g.children.find(v => v.nativeElement.getAttribute(
        'data-slnm-ihg'));

      expect(header).toBeDefined();
      expect(header.nativeElement.textContent).toEqual(columnsInSpecifiedOrder[i]);
    });
  });

  it('should display viewmore in description', () => {
    component.guestComplaints = [{
      'caseNumber': 10422212463,
      'openTime': '26SEP2019',
      'hotelStatus': 'Open',
      // tslint:disable-next-line:max-line-length
      'note': guestComplaintDescription1 + guestComplaintDescription3,
      'reasonCategories': [],
      'actionCodes': []
    }];

    component.ngOnInit();
    fixture.detectChanges();
    const gridColumnContent = fixture.debugElement.query(By.css('[data-slnm-ihg="DescriptionViewMore1-SID"]'));

    expect(component.guestComplaints.length).toEqual(1);
    expect(gridColumnContent).toBeDefined();
  });

  it('it should call navigate method on click of Date', () => {
    component.guestComplaints = [{
      'caseNumber': 10422212463,
      'openTime': '26SEP2019',
      'hotelStatus': 'Open',
      // tslint:disable-next-line:max-line-length
      'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
      'reasonCategories': [],
      'actionCodes': []
    }];
    component.ngOnInit();
    fixture.detectChanges();

    const navigateSpy = spyOn(component, 'navigateToCaseDetails').and.callThrough();
    const expectedEndPoint = `${mockHinAPIURL}guestcomplaintdetails/10400041191`;

    component.guestComplaints.forEach((t, i) => {
      const dateComponent = fixture.debugElement.query(By.css('[data-slnm-ihg="Date-SID"]'));
      dateComponent.triggerEventHandler('click', null);

      expect(navigateSpy).toBeDefined();
      expect(navigateSpy).toHaveBeenCalledTimes(1);
    });
  });

  it('should test window open event', function () {
    spyOn(window, 'open').and.callFake(function () {
      return true;
    });
    expect(window.open).toBeDefined();
  });

  it('should test analytics service is called', function () {
    const trackEventSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    const navigateSpy = spyOn(component, 'navigateToCaseDetails').and.callThrough();
    component.navigateToCaseDetails(<GuestComplaint>{
      'caseNumber': 10422212463,
      'openTime': '26SEP2019',
      'hotelStatus': 'Open',
      'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
      'reasonCategories': [],
      'actionCodes': []
    });
    fixture.detectChanges();
    expect(navigateSpy).toHaveBeenCalled();
    const category = GUEST_ANALYTICS_CONST.CheckCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ViewCase;
    expect(trackEventSpy).toHaveBeenCalledWith(category, action, label);
  });
});
