import { Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

import { EnvironmentService } from 'ihg-ng-common-core';
// tslint:disable-next-line:max-line-length
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

import { GuestRelationsService } from '../../services/guest-relations/guest-relations.service';
import { GuestComplaint } from '../../interfaces/guest-complaints.interface';
import {
  ComplaintsPagerTranslations,
  GuestComplaintsDesktopColumns,
  GuestComplaintsLandscapeColumns,
  GuestComplaintsPortraitColumns
} from '../../constants/guest-relations.constants';
import { GuestRelationsAnalyticsService } from '../../services/guest-relations-analytics-service/guest-relations-analytics-service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ServiceErrorComponent } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { AppConstants } from '@app/constants/app-constants';

@Component({
  selector: 'app-guest-complaints-grid',
  templateUrl: './guest-complaints-grid.component.html',
  styleUrls: ['./guest-complaints-grid.component.scss']
})
export class GuestComplaintsGridComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @ViewChild('description') description: ElementRef;

  @Input() ihgRcNumber: string;
  @Input() guestComplaints: GuestComplaint[] = [];
  @Input() gridState: State;
  @Input() stateName: string;

  isDesktopView: boolean;
  isTabletPortraitView: boolean;
  gridColumns = GuestComplaintsDesktopColumns;
  pagerTranslations = ComplaintsPagerTranslations;
  reportIssueAutoFill: ReportIssueAutoFillObject;
  maxHeight = 110;
  appConstants = AppConstants;

  private root$: Subscription = new Subscription();

  constructor(private guestRelationsService: GuestRelationsService,
              private environmentService: EnvironmentService,
              private gaAnalyticsService: GuestRelationsAnalyticsService) {
    super();
  }

  @ViewChild(ServiceErrorComponent) serviceErrorRef: ServiceErrorComponent;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.setViewEnablers(window.outerWidth);
  }

  ngOnInit() {
    this.isDesktopView = (window.outerWidth < 1025);
    this.isTabletPortraitView = (window.outerWidth < 769);
    this.getScreenSize();
    this.formatOpenDate();
  }

  private formatOpenDate() {
    this.guestComplaints.forEach((guestComplaints) => {
      guestComplaints.openTime = moment(guestComplaints.openTime).format(AppConstants.VW_DT_FORMAT);
      return this.guestComplaints;
    });
  }

  private setViewEnablers(screenWidth: number) {
    if (screenWidth < 769) {
      this.isTabletPortraitView = true;
      this.isDesktopView = false;
      this.gridColumns = GuestComplaintsPortraitColumns;
    } else if (screenWidth < 1025) {
      this.isTabletPortraitView = false;
      this.isDesktopView = false;
      this.gridColumns = GuestComplaintsLandscapeColumns;
    } else {
      this.isTabletPortraitView = false;
      this.isDesktopView = true;
      this.gridColumns = GuestComplaintsDesktopColumns;
    }
  }

  navigateToCaseDetails(data: GuestComplaint) {
    this.gaAnalyticsService.trackModalClick(this.stateName);
    const url = this.environmentService.getAppLinks()['hin'] + 'guestcomplaintdetails/' + data.caseNumber;
    window.open(url, '_blank');
  }

  ngOnDestroy(): void {
    this.root$.unsubscribe();
  }
}
