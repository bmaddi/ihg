import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';

import { EnvironmentService, GoogleAnalyticsService } from 'ihg-ng-common-core';

import { GuestComplaintsModalComponent } from './guest-complaints-modal.component';
import { GuestComplaintsGridComponent } from '../guest-complaints-grid/guest-complaints-grid.component';
import { GuestRelationsService } from '../../services/guest-relations/guest-relations.service';
import {
  environmentServiceServiceStub,
  googleAnalyticsServiceStub,
  guestRelationsServiceMockComplaints,
  guestRelationsServiceStub
} from '../../mocks/guest-relations.mock';


describe('GuestComplaintsModalComponent', () => {
  let component: GuestComplaintsModalComponent;
  let fixture: ComponentFixture<GuestComplaintsModalComponent>;

  beforeEach(async(() => {

    TestBed
      .overrideProvider(GuestRelationsService,
        {useValue: guestRelationsServiceStub})
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
      .configureTestingModule({
        imports: [
          TranslateModule.forChild(),
          NgbModule
        ],
        declarations: [
          GuestComplaintsModalComponent,
          GuestComplaintsGridComponent],
        providers: [
          NgbActiveModal,
          TranslateStore,
          EnvironmentService,
          GoogleAnalyticsService
        ]
      })
      .overrideComponent(GuestComplaintsGridComponent, {
        set: {
          template: 'Guest Complaints Grid'
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestComplaintsModalComponent);
    component = fixture.componentInstance;
    component.guestComplaintsData = guestRelationsServiceMockComplaints();
    component.complaintsCount = guestRelationsServiceMockComplaints().length;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component.gridState).toEqual({
      take: 3,
      skip: 0
    });
    expect(component).toBeTruthy();
  });

  it('should close the modal on x click', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.close')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('modal title should have Guest Complaints translation key', () => {
    const element = fixture.debugElement.query(By.css('.modal-title')).nativeElement;
    expect(element).not.toBeNull();
    // Todo implement actual translation for additional test coverage
    expect(element.textContent.indexOf('COM_LBL_GUEST_COMPLAINTS') > -1).toBeTruthy();
  });

  it('should close the modal on close button click', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.btn-primary')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

});
