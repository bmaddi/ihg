import { Component, Input, OnInit } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GuestComplaint } from '../../interfaces/guest-complaints.interface';

@Component({
  selector: 'app-guest-complaints-modal',
  templateUrl: './guest-complaints-modal.component.html',
  styleUrls: ['./guest-complaints-modal.component.scss']
})
export class GuestComplaintsModalComponent extends AppErrorBaseComponent implements OnInit {
  public gridState: State = {
    take: 3,
    skip: 0
  };

  @Input() complaintsCount: number;
  @Input() ihgRcNumber: string;
  @Input() stateName: string;
  @Input() guestComplaintsData: GuestComplaint[];

  constructor(
    private activeModal: NgbActiveModal
  ) {
    super();
  }

  ngOnInit() {
  }

  public close() {
    this.activeModal.dismiss();
  }
}
