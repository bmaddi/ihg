export const GuestComplaintsDesktopColumns = {
  reasonWidth: 150,
  descriptionWidth: 240,
  actionTakenWidth: 90,
  statusWidth: 90,
  dateWidth: 80
};

export const GuestComplaintsLandscapeColumns = {
  reasonWidth: 150,
  descriptionWidth: 210,
  actionTakenWidth: 95,
  statusWidth: 90,
  dateWidth: 80
};

export const GuestComplaintsPortraitColumns = {
  reasonWidth: 110,
  descriptionWidth: 168,
  actionTakenWidth: 61,
  statusWidth: 70,
  dateWidth: 71
};

export const ComplaintsPagerTranslations = {
  gridPagerInfo: {
    itemsText: 'LBL_COMPLAINTS',
    noItemsText: 'LBL_NO_COMP_DISP'
  }
};

export const guestRelationsRoleIds = [5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009];
export const guestRelationsRoleDescriptions = [
  'HOTEL_MANAGEMENT',
  'SALES',
  'ROOMS_DIVISION_MANAGEMENT',
  'REVENUE_MANAGEMENT',
  'FRONT_DESK_MANAGEMENT',
  'RESERVATIONS',
  'GUEST_COMMUNICATIONS',
  'VIEW_ONLY',
  'FRONT_DESK',
  'ACCOUNTING'
];

export const GUEST_ANALYTICS_CONST = {
  PrepareState: 'guest-list-details',
  PrepareCategory: 'Guest List - Prepare',
  CheckCategory: 'Guest List - Check-In',
  Action: 'Guest Relations',
  ViewLabel: 'Guest Complaints - View All records',
  NoAccessLabel: 'Guest Complaints - No Access',
  ViewCase: 'Guest Complaints - View Case details',
  ERROR_RETRIEVING_COUNTS: 'Guest Complaints - Error Retrieving Guest Complaints (Count)',
  ERROR_TIMEOUT_RETRIEVING_COUNTS: 'Guest Complaints - Data Timeout Retrieving Guest Complaints (Count)',
  ERROR_RETRIEVING_LIST: 'Guest Complaints - Error Retrieving Guest Complaints (List)',
  ERROR_RETRIEVING_TIMEOUT_LIST: 'Guest Complaints - Data Timeout Retrieving Guest Complaints (List)',
  REASON_CATEGORY: 'Guest Complaints - Select Billing Issues'
};


export const GUEST_COMPLAINT_CONST = {
  KEYMAP: {
    GC: {
      CATEGORY: 'Guest List - ',
      SUB_CATEGORY: {
        CHECKINTAB: 'Check-In',
        PREPARETAB: 'Prepare'
      },
      ACTION: 'Guest Relations',
      LABEL: 'Guest Complaints - ',
      SUB_LABEL: {
        VIEW: 'View All records',
        NO_ACCESS: 'No Access',
        VIEW_CASE: 'View Case details',
        ERROR_RETRIEVING_COUNTS: 'Error Retrieving Guest Complaints (Count)',
        ERROR_TIMEOUT_RETRIEVING_COUNTS: 'Data Timeout Retrieving Guest Complaints (Count)',
        ERROR_RETRIEVING_LIST: 'Error Retrieving Guest Complaints (List)',
        ERROR_RETRIEVING_TIMEOUT_LIST: 'Data Timeout Retrieving Guest Complaints (List)',
        REASON_CATEGORY: 'Select '
      }
    }
  }
};


