import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';

import { GuestComplaintsCountComponent } from './components/guest-complaints-count/guest-complaints-count.component';
import { GuestComplaintsModalComponent } from './components/guest-complaints-modal/guest-complaints-modal.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { GuestComplaintsGridComponent } from './components/guest-complaints-grid/guest-complaints-grid.component';
import { AppSharedModule } from '@app-shared/app-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    NgbTooltipModule,
    GridModule,
    IhgNgCommonKendoModule,
    AppSharedModule
  ],
  entryComponents: [ GuestComplaintsModalComponent ],
  declarations: [GuestComplaintsCountComponent, GuestComplaintsModalComponent, GuestComplaintsGridComponent],
  exports: [GuestComplaintsCountComponent, GuestComplaintsModalComponent]
})
export class GuestRelationsModule {
}
