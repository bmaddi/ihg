export interface GuestComplaint {
  caseNumber: number;
  openTime: string;
  hotelStatus: string;
  note: string;
  reasonCategories: string[];
  actionCodes: string[];
}
