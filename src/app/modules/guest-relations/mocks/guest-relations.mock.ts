import { Observable, of } from 'rxjs';
import { cloneDeep } from 'lodash';
import { GuestComplaint } from '../interfaces/guest-complaints.interface';

export const mockAPIURL = 'https://concerto.mock.test/fdk/concertoservices/v1/';
export const mockHinAPIURL = 'https://concertoint.ihgext.global/hin/#/';
export const mockApplcd = '';
export const environmentServiceServiceStub = {
  getApiUrl: (): string => mockAPIURL,
  getAppLinks: (): string => mockHinAPIURL,
  getEnvironmentConstants: (): { [key: string]: string } => {
    return { fdkAPI: mockAPIURL };
  },
  getApplCd: (): string => mockApplcd
};

export const googleAnalyticsServiceStub = {
  trackEvent: (category, action, label): void => {
  }
};

export const guestRelationsServiceMockComplaints = () => cloneDeep([
  {
    'caseNumber': 10422212463,
    'openTime': '02OCT2019',
    'hotelStatus': 'Action Required',
    'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
    'reasonCategories': ['Billing Issues'],
    'actionCodes': ['action codes'],
  },
  {
    'caseNumber': 10422212464,
    'openTime': '02OCT2019',
    'hotelStatus': 'Action Required',
    'note': '',
    'reasonCategories': ['RC Program'],
    'actionCodes': ['action codes'],
  },
  {
    'caseNumber': 10422212465,
    'openTime': '02OCT2019',
    'hotelStatus': 'Action Required',
    'note': '',
    'reasonCategories': [],
    'actionCodes': ['action codes'],
  },
  {
    'caseNumber': 10422212466,
    'openTime': '02OCT2019',
    'hotelStatus': 'Action Required',
    'note': '',
    'reasonCategories': ['RC Program'],
    'actionCodes': ['action codes'],
  }
]);
export const gcAdditonalMockComplaints = () => cloneDeep([
  {
    'caseNumber': 20422212464,
    'openTime': '02OCT2019',
    'hotelStatus': 'Action Required',
    'note': '',
    'reasonCategories': ['Billing Issues'],
    'actionCodes': ['action codes'],
  },
  {
    'caseNumber': 50422212464,
    'openTime': '02OCT2019',
    'hotelStatus': 'Action Required',
    'note': '',
    'reasonCategories': ['Billing Issues'],
    'actionCodes': ['action codes'],
  }
]);

export const guestRelationsServiceStub = {
  getGuestComplaintsData: (): Observable<GuestComplaint> => of(<any>{
    guestRelationsServiceMockComplaints
  })
};

export const rolesMock = {
  'roles':
    [
      {
        'id': 5000,
        'name': 'HOTEL_MANAGEMENT',
        'description': 'HOTEL_MANAGEMENT',
        'functions': null
      },
      {
        'id': 5002,
        'name': 'ROOMS_DIVISION_MANAGEMENT',
        'description': 'ROOMS_DIVISION_MANAGEMENT',
        'functions': null
      },
      {
        'id': 5009,
        'name': 'ACCOUNTING',
        'description': 'ACCOUNTING',
        'functions': null
      }]
};

// tslint:disable-next-line:max-line-length
export const guestComplaintDescription1 = 'Mr.Test called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.';
// tslint:disable-next-line:max-line-length
export const guestComplaintDescription3 = 'Mr.Ryan complained of non availability of 24/7 hot water service. We provided user with loyalty points for inconvenience caused.';


