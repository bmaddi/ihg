import { TestBed } from '@angular/core/testing';
import { GuestRelationsAnalyticsService } from './guest-relations-analytics-service';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { googleAnalyticsServiceStub } from '@modules/guest-relations/mocks/guest-relations.mock';
import { GUEST_ANALYTICS_CONST } from '@modules/guest-relations/constants/guest-relations.constants';

describe('GuestRelationsAnalyticsService', () => {

  let gaGuestRelationService: GuestRelationsAnalyticsService;

  beforeEach(() => {
    TestBed.overrideProvider(GoogleAnalyticsService, {
      useValue: googleAnalyticsServiceStub
    });
    TestBed.configureTestingModule({
      providers: [GoogleAnalyticsService]
    });
    gaGuestRelationService = TestBed.get(GuestRelationsAnalyticsService);
  });

  it('should call trackAccessToClickGuestCount', () => {
    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ViewLabel;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    gaGuestRelationService.trackAccessToClickGuestCount(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should call trackNoAccessToClick', () => {
    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.NoAccessLabel;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    gaGuestRelationService.trackNoAccessToClick(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should call trackClickInModal', () => {
    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ViewCase;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    gaGuestRelationService.trackModalClick(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should call trackGuestRelationsCountError', () => {

    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ERROR_RETRIEVING_COUNTS;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    gaGuestRelationService.trackGuestRelationsCountError(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should call trackGuestRelationsCountTimeoutError', () => {

    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ERROR_TIMEOUT_RETRIEVING_COUNTS;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    gaGuestRelationService.trackGuestRelationsCountTimeoutError(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should call trackGuestRelationsListError', () => {

    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ERROR_RETRIEVING_LIST;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    gaGuestRelationService.trackGuestRelationsListError(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should call trackGuestRelationsListTimeout', () => {

    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.ERROR_RETRIEVING_TIMEOUT_LIST;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    gaGuestRelationService.trackGuestRelationsListTimeout(stateName);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('Validate GA is tracked for Prepare tab for the reason category clicked', () => {

    const rcData = 'Billing Issues';

    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.REASON_CATEGORY;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    gaGuestRelationService.trackReasonCategoryClick(stateName, rcData);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('Validate GA is tracked for Check in for teh reason category clicked', () => {

    const rcData = 'Billing Issues';

    const stateName = GUEST_ANALYTICS_CONST.PrepareState;
    const category = GUEST_ANALYTICS_CONST.PrepareCategory;
    const action = GUEST_ANALYTICS_CONST.Action;
    const label = GUEST_ANALYTICS_CONST.REASON_CATEGORY;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    gaGuestRelationService.trackReasonCategoryClick(stateName, rcData);

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

});
