import { Injectable } from '@angular/core';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { GUEST_COMPLAINT_CONST } from '@modules/guest-relations/constants/guest-relations.constants';

@Injectable({
  providedIn: 'root'
})
export class GuestRelationsAnalyticsService {

  private gc = GUEST_COMPLAINT_CONST.KEYMAP.GC;
  private gcCategoryPrepare = this.gc.CATEGORY + this.gc.SUB_CATEGORY.PREPARETAB;
  private gcCategoryCheckIn = this.gc.CATEGORY + this.gc.SUB_CATEGORY.CHECKINTAB;
  private gcLabelView = this.gc.LABEL + this.gc.SUB_LABEL.VIEW;
  private gcLabelNoAccess = this.gc.LABEL + this.gc.SUB_LABEL.NO_ACCESS;
  private gcLabelViewCase = this.gc.LABEL + this.gc.SUB_LABEL.VIEW_CASE;
  private gcLabelCountsError = this.gc.LABEL + this.gc.SUB_LABEL.ERROR_RETRIEVING_COUNTS;
  private gcLabelCountsTimeout = this.gc.LABEL + this.gc.SUB_LABEL.ERROR_TIMEOUT_RETRIEVING_COUNTS;
  private gcLabelGuestRelationListError = this.gc.LABEL + this.gc.SUB_LABEL.ERROR_RETRIEVING_LIST;
  private gcLabelGuestRelationTimeout = this.gc.LABEL + this.gc.SUB_LABEL.ERROR_RETRIEVING_TIMEOUT_LIST;
  private gcLabelReasonCategory = this.gc.LABEL + this.gc.SUB_LABEL.REASON_CATEGORY;

  constructor(private gaService: GoogleAnalyticsService) {
  }

  public trackAccessToClickGuestCount(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelView);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelView);
    }
  }

  public trackNoAccessToClick(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelNoAccess);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelNoAccess);
    }
  }

  public trackModalClick(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelViewCase);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelViewCase);
    }
  }

  public trackGuestRelationsCountError(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelCountsError);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelCountsError);
    }
  }

  public trackGuestRelationsCountTimeoutError(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelCountsTimeout);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelCountsTimeout);
    }
  }

  public trackGuestRelationsListError(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelGuestRelationListError);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelGuestRelationListError);
    }
  }

  public trackGuestRelationsListTimeout(stateName: string): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelGuestRelationTimeout);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelGuestRelationTimeout);
    }
  }

  public trackReasonCategoryClick(stateName: string, rcData): void {
    if (stateName && stateName === 'guest-list-details') {
      this.gaService.trackEvent(this.gcCategoryPrepare, this.gc.ACTION, this.gcLabelReasonCategory + rcData);
    } else {
      this.gaService.trackEvent(this.gcCategoryCheckIn, this.gc.ACTION, this.gcLabelReasonCategory + rcData);
    }
  }
}
