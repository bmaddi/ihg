import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { ApiService, EnvironmentService, PageAccessService, UserConfirmationModalConfig, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalComponent, ConfirmationModalService, } from 'ihg-ng-common-components';

import { GuestRelationsService } from './guest-relations.service';
import { GuestComplaint } from '../../interfaces/guest-complaints.interface';
import { environmentServiceServiceStub, mockAPIURL, rolesMock } from '../../mocks/guest-relations.mock';

describe('GuestRelationsService', () => {
  const rewardClubNumber = '1111111';
  const qStartDate = '2016-11-11';
  const qEndDate = '2019-11-11';
  const endPoint = `${mockAPIURL}guestComplaints/`;
  const expectedEndPointCounts = `${endPoint}retrieveCasesByRcNumber/`;
  const checkAPICall = (req: HttpRequest<any>) => {
    return req.method === 'GET' && req.url.indexOf(expectedEndPointCounts) !== -1;
  };
  const fakeApiCall = (mockResponse: GuestComplaint | any, error: boolean = false) => {
    request = httpMockController.expectOne(checkAPICall);
    expect(request.request.params.get('qStartDate')).toEqual(qStartDate);
    expect(request.request.params.get('qEndDate')).toEqual(qEndDate);
    expect(request.request.params.get('rewardsClubNumber')).toEqual(rewardClubNumber);
    expect(request.request.method).toBe('GET');
    error ? request.error(mockResponse) : request.flush(mockResponse);
    httpMockController.verify();
  };

  let service: GuestRelationsService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;

  beforeEach(() => {

    TestBed.overrideProvider(EnvironmentService, {
      useValue: environmentServiceServiceStub
    });

    TestBed
      .configureTestingModule({
        declarations: [ConfirmationModalComponent],
        providers: [ApiService, EnvironmentService, UserService, ConfirmationModalService, PageAccessService, TranslateStore],
        imports: [HttpClientTestingModule, TranslateModule.forChild(), NgbModule]
      })
      .overrideModule(BrowserDynamicTestingModule,
        {set: {entryComponents: [ConfirmationModalComponent]}});

    service = TestBed.get(GuestRelationsService);
    httpMockController = TestBed.get(HttpTestingController);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return Observable GuestComplaintsInterface', () => {

    const mockResponse = <GuestComplaint>{};

    service.getGuestComplaintsData(qStartDate, qEndDate, rewardClubNumber).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse);
  });

  it('should return Observable GuestComplaintsCountResponse', () => {
    const mockResponse = <GuestComplaint>{
      'caseNumber': 10422212463,
      'openTime': '26SEP2019',
      'hotelStatus': 'Open',
      // tslint:disable-next-line:max-line-length
      'note': 'Mr.John called downstairs to our restaurant requesting room service. Our charge is $20.00 for room service. We advised guest of charge. Guest could have placed a to go order, we could not provide room service to the guest.',
      'reasonCategories': [],
      'actionCodes': []
    };

    service.getGuestComplaintsData(qStartDate, qEndDate, rewardClubNumber).subscribe(data => {
      expect(data).toBeDefined();
    });
    fakeApiCall(mockResponse);
  });

  it('should return and gracefully handle Observable HttpErrorResponse', () => {
    const errorResponse: any = {status: 500, statusText: 'Bad Request'};

    service.getGuestComplaintsData(qStartDate, qEndDate, rewardClubNumber).subscribe(data => {
      },
      error => {
        expect(error).toBeDefined();
        expect(error.error.status).toBeGreaterThan(204);
        expect(error.error.statusText).toEqual('Bad Request');
      });
    fakeApiCall(errorResponse, true);
  });

  it('should return and gracefully handle Observable HttpErrorResponse for Guest Complaints Count API', () => {
    const errorResponse: any = {status: 500, statusText: 'Bad Request'};

    service.getGuestComplaintsData(qStartDate, qEndDate, rewardClubNumber).subscribe(data => {
      },
      error => {
        expect(error).toBeDefined();
        expect(error.error.status).toBeGreaterThan(204);
        expect(error.error.statusText).toEqual('Bad Request');
      });

    fakeApiCall(errorResponse, true);
  });

  it('should call hasGuestComplaintsAccess and by default be false', () => {
    expect(service.hasGuestComplaintsAccess()).toBeFalsy();
  });

  it('should call hasGuestComplaintsAccess and be true should the menu service flag be true', () => {
    const pageAccess = TestBed.get(PageAccessService);
    const userService = TestBed.get(UserService);

    const pageAccessSpy = spyOn(pageAccess, 'getGuestComplaintsAccess').and.returnValue(true);
    const userServiceSpy = spyOn(userService, 'getUser').and.returnValue(rolesMock);


    expect(service.hasGuestComplaintsAccess()).toBeTruthy();
    expect(pageAccessSpy).not.toHaveBeenCalled();
    expect(userServiceSpy).toHaveBeenCalled();
  });

  it('getConfirmModalConfig should return type of UserConfirmationModalConfig with message inserted with other defaults', () => {
    const testMessage = 'My Test Message';
    const userConfig: UserConfirmationModalConfig = service['getConfirmModalConfig'](testMessage);
    expect(userConfig).toBeDefined();
    expect(userConfig).toEqual(<UserConfirmationModalConfig>{
      modalHeader: 'LBL_WRN_NO_ACCS',
      defaultButtonLabel: 'COM_BTN_CLOSE',
      defaultButtonClass: 'btn-default',
      messageHeader: testMessage,
      windowClass: 'hide-primary-btn modal-medium',
      dismissible: true
    });
  });

  it('should call accessDeniedWarning function', () => {
    const configModalSpy = spyOn<any>(service, 'getConfirmModalConfig').and.callThrough();
    const message = 'This feature is only available to Concerto users with access to Guest Complaints. Please obtain access to continue.';
    const modal = service.accessDeniedWarning(message);
    expect(modal).toBeDefined();
    expect(modal).toBeTruthy();
    expect(configModalSpy).toHaveBeenCalledTimes(1);
  });
});


