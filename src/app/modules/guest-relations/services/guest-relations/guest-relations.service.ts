import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { get, includes, some } from 'lodash';

import { ConfirmationModalService } from 'ihg-ng-common-components';
import { ApiService, ApiUrl, PageAccessService, UserConfirmationModalConfig, UserService } from 'ihg-ng-common-core';

import { GuestComplaint } from '../../interfaces/guest-complaints.interface';
import { guestRelationsRoleDescriptions, guestRelationsRoleIds } from '../../constants/guest-relations.constants';
import { GuestComplaintsModalComponent } from '@modules/guest-relations/components/guest-complaints-modal/guest-complaints-modal.component';

@Injectable({
  providedIn: 'root'
})
export class GuestRelationsService {

  constructor(private apiService: ApiService,
              private confirmationModalService: ConfirmationModalService,
              private pageAccess: PageAccessService,
              private userService: UserService,
              private modalService: NgbModal) {
  }

  getGuestComplaintsData(qStartDate: string,
                         qEndDate: string,
                         rewardsClubNumber: string,
                         accessCheck: boolean = false,
                         showSpinner: 'N' | 'Y' = 'N'): Observable<GuestComplaint[]> {
    return this.apiService.get(
      new ApiUrl(this.apiService.getApiUrl(), 'guestComplaints/retrieveCasesByRcNumber/'),
      {qStartDate, qEndDate, accessCheck, ...{rewardsClubNumber}} as any,
      new HttpHeaders().set('spinnerConfig', showSpinner)
    );
  }

  accessDeniedWarning(message: string): Observable<void> {
    return this.confirmationModalService.openConfirmationModal(this.getConfirmModalConfig(message));
  }

  openComplaintsGrid(data: GuestComplaint[], stateName: string): Observable<string> {
    const activeModal = this.modalService.open(GuestComplaintsModalComponent, {backdrop: 'static', windowClass: 'large-modal'});
    activeModal.componentInstance.complaintsCount = data.length;
    activeModal.componentInstance.stateName = stateName;
    activeModal.componentInstance.guestComplaintsData = data;
    return from(activeModal.result);
  }

  hasGuestComplaintsAccess(): boolean {
    return this.hasGuestComplaintsRoles();
  }

  private getConfirmModalConfig(message: string): UserConfirmationModalConfig {
    return {
      modalHeader: 'LBL_WRN_NO_ACCS',
      defaultButtonLabel: 'COM_BTN_CLOSE',
      defaultButtonClass: 'btn-default',
      messageHeader: message,
      windowClass: 'hide-primary-btn modal-medium',
      dismissible: true
    };
  }

  private hasGuestComplaintsRoles(): boolean {
    // Todo: Ideally check should come from backend but this is put in place as a temporary solution
    //  until getGuestComplaintsAccess is properly updated
    const roles = get(this.userService.getUser(), 'roles', []);
    return some(roles, role => includes(guestRelationsRoleIds, role.id) ||
      includes(guestRelationsRoleDescriptions, role.description));
  }
}
