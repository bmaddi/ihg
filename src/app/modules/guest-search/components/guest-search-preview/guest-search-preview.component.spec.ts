import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestSearchPreviewComponent } from './guest-search-preview.component';
import { MOCK_ASSIGNED_ROOM_1, MOCK_ASSIGNED_ROOM_2, MOCK_GUEST_LIST } from '@modules/guest-search/mocks/guest-search-mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { FormControl, FormGroup } from '@angular/forms';

xdescribe('GuestSearchPreviewComponent', () => {
  let component: GuestSearchPreviewComponent;
  let fixture: ComponentFixture<GuestSearchPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestSearchPreviewComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonModule, CommonTestModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(GuestSearchPreviewComponent);
    component = fixture.componentInstance;
    component.guestsListData = MOCK_GUEST_LIST;
    component.formData = new FormGroup({
      search: new FormControl('')
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the guest list', () => {
    expect(component.guestsListData[0].roomNumber).toEqual('238');

    component.updateGuestList(MOCK_ASSIGNED_ROOM_1);
    expect(component.guestsListData[0].roomNumber).toEqual('245');

    component.updateGuestList(MOCK_ASSIGNED_ROOM_2);
    expect(component.guestsListData[0].roomNumber).toEqual('238');
  });
});
