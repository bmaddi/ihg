import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { filterBy } from '@progress/kendo-data-query';
import * as moment from 'moment';
import { cloneDeep } from 'lodash';

import { GuestSearchPreviewService } from '../../services/guest-search-preview.service';
import { DataDisplayProperty, PreviewGuestItem } from '../../interfaces/search-preview.interface';
import { GroupList } from '@modules/group-block/models/group-block.model';
import { AppConstants } from '@app/constants/app-constants';
import { PreviewAPIParameter, PreviewSection, PreviewTextPipe } from '../../enums/preview-section.enum';
import { UtilityService } from '@services/utility/utility.service';
import {RoomChangeModel} from '@modules/prepare/models/arrivals-checklist.model';

@Component({
  selector: 'app-guest-search-preview',
  templateUrl: './guest-search-preview.component.html',
  styleUrls: ['./guest-search-preview.component.scss']
})
export class GuestSearchPreviewComponent implements OnInit, OnDestroy, OnChanges {
  @Input() anchor: ElementRef;
  @Input() formData: FormGroup;
  @Input() groupList: GroupList[];
  @Input() input: HTMLInputElement;
  @Input() searchDate: Date;
  @Input() previewTabApiParameter: PreviewAPIParameter;
  @Input() roomChangeData?: RoomChangeModel;
  @Output() emitGuest: EventEmitter<string> = new EventEmitter();
  @Output() emitGroup: EventEmitter<string> = new EventEmitter();
  @Output() emitAllGroup: EventEmitter<string> = new EventEmitter();

  show = false;
  guestListFiltered: PreviewGuestItem[] = [];
  roomListFiltered: PreviewGuestItem[] = [];
  groupListFiltered: GroupList[] = [];
  guestsListPropsToDisplay: DataDisplayProperty[] = [{
    property: 'lastName',
    propertyPipe: PreviewTextPipe.titleCase,
    separator: ','
  }, {property: 'firstName', propertyPipe: PreviewTextPipe.titleCase}];
  groupsListPropsToDisplay: DataDisplayProperty[] = [{property: 'groupName', propertyPipe: PreviewTextPipe.titleCase}];
  roomsListPropsToDisplay: DataDisplayProperty[] = [
    {
      property: 'roomNumber',
      propertyPipe: PreviewTextPipe.titleCase,
      separator: ' - '
    },
    {
      property: 'lastName',
      propertyPipe: PreviewTextPipe.titleCase,
      separator: ','
    },
    {
      property: 'firstName',
      propertyPipe: PreviewTextPipe.titleCase
    }
  ];
  previewSections = PreviewSection;
  guestsListData: PreviewGuestItem[];

  private dateChanged = true;
  private groupListData: GroupList[] = [];
  private searchValueUpdate = new Subject<string>();
  private userSearchInputs = this.searchValueUpdate.asObservable().pipe(debounceTime(250));
  private subscriptions$ = new Subscription();

  get selectedDate(): string {
    return this.searchDate ? moment(this.searchDate).format(AppConstants.DB_DT_FORMAT) : '';
  }

  get currentInputValue(): string {
    return cloneDeep(this.formData.get('search').value);
  }

  constructor(private guestSearchPreviewService: GuestSearchPreviewService,
    private utilityService: UtilityService) {
  }

  ngOnInit() {
    this.addInputChangesDebounceSubscription();
    this.refreshGuestList();
    this.addInputChangesSubscription();
    this.addInputListeners();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.utilityService.hasSimpleDateChanges(changes, 'searchDate')) {
      this.refreshGuestList();
      this.dateChanged = true;
    }
    if (changes.roomChangeData) {
      this.updateGuestList(this.roomChangeData);
    }
    if (changes.groupList && !changes.groupList.firstChange && this.dateChanged) {
      this.groupListData = cloneDeep(this.groupList);
      if (this.formData.valid) {
        this.filterGroupsList(this.currentInputValue);
      }
      this.dateChanged = false;
    }
  }

  ngOnDestroy() {
    this.subscriptions$.unsubscribe();
  }

  onSelection<T>(data: T) {
    this.input.blur();
    this.handleEmitSelection(data);
  }

  private handleEmitSelection<T>(data: T) {
    if (data['section'] === this.previewSections.guestsPreview) {
      this.handleEmitGuest(data['value']);
    } else if (data['section'] === this.previewSections.roomsPreview) {
      this.handleEmitGuest(data['value']);
    } else {
      this.handleEmitGroup(data['value']);
    }
  }

  private handleEmitGuest(value: string) {
    this.emitGuest.emit(value.match(/ALL/i) ? this.currentInputValue : value);
  }

  private handleEmitGroup(value: string) {
    if (value === 'All') {
      this.emitAllGroup.emit(this.currentInputValue);
    } else {
      this.emitGroup.emit(value);
    }
  }

  private refreshGuestList() {
    this.subscriptions$
      .add(
        this.guestSearchPreviewService.getPreviewResults(this.previewTabApiParameter, this.selectedDate)
          .subscribe(data => {
            this.guestsListData = data;
            if (this.formData.valid) {
              this.filterGuestList(this.currentInputValue);
              this.filterRoomList(this.currentInputValue);
            }
          })
      );
  }

  private filterData(search: string) {
    this.filterGuestList(search);
    this.filterGroupsList(search);
    this.filterRoomList(search);
  }

  private filterGuestList(search: string) {
    if (this.guestsListData) {
      this.guestListFiltered = filterBy(this.guestsListData, {
        logic: 'or',
        filters: [
          { field: 'firstName', operator: 'contains', value: search, ignoreCase: true },
          { field: 'lastName', operator: 'contains', value: search, ignoreCase: true },
          { field: 'name', operator: 'contains', value: search, ignoreCase: true },
        ]
      });
    }
  }

  private filterGroupsList(search: string) {
    if (this.groupListData) {
      this.groupListFiltered = filterBy(this.groupListData, {
        logic: 'or',
        filters: [
          { field: 'groupName', operator: 'contains', value: search, ignoreCase: true },
          { field: 'groupCode', operator: 'contains', value: search, ignoreCase: true },
        ]
      });
    }
  }

  private filterRoomList(search: string) {
    if (this.guestsListData) {
      this.roomListFiltered = filterBy(this.guestsListData, {
        logic: 'or',
        filters: [
          { field: 'roomNumber', operator: 'contains', value: search, ignoreCase: true }
        ]
      });
    }
  }

  private addInputListeners() {
    if (this.input) {
      this.input.addEventListener('focus', () => {
        this.show = true;
      });
      this.input.addEventListener('blur', () => {
        this.show = false;
      });
    }
  }

  private addInputChangesDebounceSubscription() {
    this.subscriptions$
      .add(
        this.userSearchInputs.subscribe((data) => this.filterData(data))
      );
  }

  private addInputChangesSubscription() {
    if (this.formData) {
      this.subscriptions$.add(
        this.formData.valueChanges.subscribe(data => {
          if (this.formData.valid) {
            this.searchValueUpdate.next(data.search);
          }
        })
      );
    }
  }

  @HostListener('mousedown', ['$event'])
  onClick(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
  }

  updateGuestList(data: RoomChangeModel) {
    if (this.guestsListData) {
      const index = this.guestsListData.findIndex(reservation =>
        reservation.roomNumber === data.previousRoomNumber && reservation.confirmationNumber === data.reservationNumber);
      if (index !== -1) {
        this.guestsListData[index].roomNumber = data.newRoomNumber;
        if (this.currentInputValue) {
          this.filterData(this.currentInputValue);
        }
      }
    }
  }
}
