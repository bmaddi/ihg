import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuestSearchComponent } from './guest-search.component';
import { NO_ERRORS_SCHEMA, Component, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GoogleAnalyticsService, UserService, SessionStorageService } from 'ihg-ng-common-core';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { UtilityService } from '@services/utility/utility.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';
import { DeviceDetectorService } from 'ngx-device-detector';
import { googleAnalyticsServiceStub } from '@modules/guest-relations/mocks/guest-relations.mock';
import { GUEST_CONST } from '@modules/guests/guests.constants';

@Component({
  selector: 'app-guest-search-preview',
  template: ''
})

export class ChildStubComponent {
  @Input() anchor;
  @Input() formData;
  @Input() groupList;
  @Input() input;
  @Input() searchDate;
  @Input() previewTabApiParameter;
  @Output() emitGuest;
  @Output() emitGroup;
  @Output() emitAllGroup;
}

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}
export class MockArrivalsService {
  handleUnsavedTasks(callBackAction: Function, cancelCallBackFn?: Function, dataItem?): void {
    callBackAction();
  }
}

export class MockUtilityService {
  public checkNonWhiteSpaceCharacterWithMinLength(searchText: string, minCharLength: number): boolean {
    return !(searchText && minCharLength !== undefined && searchText.trim().length >= minCharLength);
  }
}

export class MockSessionStorageService {
  public setSessionStorage(key, value) { }
}

describe('GuestSearchComponent', () => {
  let component: GuestSearchComponent;
  let fixture: ComponentFixture<GuestSearchComponent>;
  let translateService: TranslateService;
  const searchBoxHeight = 34;
  const searchBoxWidth = 294;

  beforeEach(async(() => {
    TestBed.overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsServiceStub
      })
    .configureTestingModule({
      declarations: [GuestSearchComponent, ChildStubComponent],
      imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserService, TranslateService, DeviceDetectorService,
        { provide: GoogleAnalyticsService },
        { provide: ArrivalsService, useClass: MockArrivalsService },
        { provide: UtilityService, useClass: MockUtilityService },
        { provide: SessionStorageService, useClass: MockSessionStorageService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestSearchComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check the placeholder value in the search box', () => {
    const searchBox = fixture.debugElement.query(By.css('.form-control'));
    expect(searchBox.nativeElement.placeholder).toEqual('LBL_ENTR_GUEST_NAME_OR_RESERVATION_NUMBER_OR_GROUP_NAME');
  });

  it('Search box label should be same as "Search"', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="SearchGuestLabel-SID"]')).nativeElement;

    expect(element).not.toBeNull();
    expect(element.textContent).toEqual('LBL_SRCH_GST_GRP');
  });

  it('Search check box height and width should match with wireframe', () => {
    const element = fixture.debugElement.query(By.css('.form-control')).nativeElement;

    expect(element).not.toBeNull();
    expect(element.offsetWidth).toEqual(searchBoxWidth);
    expect(element.offsetHeight).toEqual(searchBoxHeight);
  });

  it('should check handleEnterKey method', () => {
    const search = component.searchFormData.controls['search'];
    search.setValue('Test');
    const event = new KeyboardEvent('keypress', {
      'key': 'Enter'
    });
    spyOn(component, 'performSearch');
    component.handleEnterKey(event);
    expect(component.performSearch).toHaveBeenCalled();
  });

  it('should check validateForm method ', () => {
    component.validateForm();
    const search = component.searchFormData.controls['search'];
    search.setValue('Guest');
    expect(search.valid).toBeTruthy();
  });

  it('should check form invalid state', () => {
    component.validateForm();
    const search = component.searchFormData.controls['search'];
    search.setValue('$');
    fixture.detectChanges();
    expect(search.valid).toBeFalsy();
  });

  it('should test resetSearch method', () => {
    component.isSearched = false;
    const resetSearchFieldSpy = spyOn(component, 'resetSearchField');
    const event = new KeyboardEvent('keypress', {
      'key': 'Enter'
    });
    component.resetSearch(event);
    expect(resetSearchFieldSpy).toHaveBeenCalled();
  });

  it('should test resetSearch method when isSearched is true', () => {
    component.isSearched = true;
    const event = new KeyboardEvent('keypress', {
      'key': 'Enter'
    });
    component.resetSearch(event);
    expect(component.isSearched).toBeFalsy();
  });

  it('should check resetSearchField method', () => {
    const search = component.searchFormData.controls['search'];
    search.setValue('Test');
    component.resetSearchField();
    expect(component.searchFormData.pristine).toBeTruthy();
    expect(component.searchFormData.get('search').value).toBeNull();
  });

  it('should check handleGuestSelection method', () => {
    const emitspy = spyOn(component.emitGuestSelection, 'emit');
    component.handleGuestSelection('Guest');
    expect(emitspy).toHaveBeenCalled();
    expect(component.isSearched).toBeTruthy();
  });

  it('should check handleGroupSelection method', () => {
    const emitspy = spyOn(component.emitGroupSelection, 'emit');
    component.handleGroupSelection('Guest');
    expect(emitspy).toHaveBeenCalled();
    expect(component.isSearched).toBeTruthy();
  });

  it('should check handleAllGroupSelection method', () => {
    const emitspy = spyOn(component.emitAllGroupSelection, 'emit');
    component.handleAllGroupSelection('Guest');
    expect(emitspy).toHaveBeenCalled();
    expect(component.isSearched).toBeTruthy();
  });

  it('should check if form is valid if 2 or more characters are entered in input', () => {
    let errors = {};
    component.searchFormData.patchValue({ 'search': 'group' });
    const search = component.searchFormData.controls['search'];
    errors = search.errors;
    fixture.detectChanges();
    expect(component.searchFormData.valid).toBeTruthy();
    expect(errors).toBeNull();
  });

  it('should check if search button is enabled if less than 2 characters are entered in input', () => {
    let errors = {};
    const search = component.searchFormData.controls['search'];
    errors = search.errors || {};
    expect(errors['emptySpaces']).toBeTruthy();
    expect(component.searchFormData.valid).toBeFalsy();
  });

  it('Should track the search from All Guest tab', () => {
    component.source = 'ALLGUESTTAB';
    const ga = GUEST_CONST.GA;
    const category = ga.CATEGORY + ga.SUB_CATEGORY.ALLGUESTTAB;
    const action = ga.SEARCH_BUTTON;
    const label = ga.SELECTED;
    const trackSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component.trackSearch();

    expect(trackSpy).toBeDefined();
    expect(trackSpy).toHaveBeenCalledWith(category, action, label);
  });

});
