import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

import { GoogleAnalyticsService, SessionStorageService } from 'ihg-ng-common-core';

import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { UtilityService } from '@services/utility/utility.service';
import { FdkValidators } from '@app-shared/validators/app.validators';
import { searchValidRegex } from '@app/constants/app-constants';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { GroupList } from '@modules/group-block/models/group-block.model';
import { PreviewAPIParameter } from '../../enums/preview-section.enum';
import {DeviceDetectorService} from 'ngx-device-detector';
import {RoomChangeModel} from '@modules/prepare/models/arrivals-checklist.model';

@Component({
  selector: 'app-guest-search',
  templateUrl: './guest-search.component.html',
  styleUrls: ['./guest-search.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GuestSearchComponent implements OnInit {
  @ViewChild('searchInput') searchInput;

  @Input() searchQuery: string;
  @Input() source: string;
  @Input() groupList: GroupList[];
  @Input() previewEnabled = false;
  @Input() searchDate: Date;
  @Input() roomChangeData?: RoomChangeModel;
  @Output() search: EventEmitter<string> = new EventEmitter();
  @Output() emitGuestSelection: EventEmitter<string> = new EventEmitter();
  @Output() emitGroupSelection: EventEmitter<string> = new EventEmitter();
  @Output() emitAllGroupSelection: EventEmitter<string> = new EventEmitter();
  @Output() emitIsReset: EventEmitter<boolean> = new EventEmitter();
  @Output() emptyReset: EventEmitter<boolean> = new EventEmitter();

  isSearched = false;
  previewAPIParameter = PreviewAPIParameter.PREPARETAB;
  searchFormData: FormGroup;
  ga = GUEST_CONST.GA;
  isDesktop: boolean;

  private minSearchCharLimit = 2;
  private inHouseCheckCondition = () => this.source === 'INHOUSETAB';

  constructor(
    private gaService: GoogleAnalyticsService,
    private arrivalsService: ArrivalsService,
    private utilityService: UtilityService,
    private storageService: SessionStorageService,
    private deviceService: DeviceDetectorService) {
  }

  get searchControl(): AbstractControl {
    return this.searchFormData.get('search');
  }

  ngOnInit() {
    this.validateForm();
    this.setPreviewAPIParam();
    this.isDesktop = this.deviceService.isDesktop();
  }

  validateForm() {
    this.searchFormData = new FormGroup({
      search: new FormControl(this.searchQuery, Validators.compose([
        FdkValidators.validateMinWithTrimmedSpaces(this.minSearchCharLimit, this.utilityService.checkNonWhiteSpaceCharacterWithMinLength),
        Validators.pattern(searchValidRegex)
      ]))
    });
  }

  performSearch(event): void {
    this.saveToSessionBasedOnTab(this.inHouseCheckCondition, 'searchValue', this.searchFormData.get('search').value);
    const callbackFn = this.doSearch.bind(this, event);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  private doSearch(event): void {
    this.isSearched = true;
    this.searchQuery = this.searchControl.value;
    this.focusSearchInput(event);
    this.emitSearch();
    this.trackSearch();
    this.emitIsReset.emit(false);
  }

  handleEnterKey(event: KeyboardEvent): void {
    if (this.searchFormData.valid) {
      this.performSearch(event);
    }
  }

  resetSearch(event): void {
    if (this.isSearched || this.searchQuery) {
      const callbackFn = this.doReset.bind(this, event);
      this.arrivalsService.handleUnsavedTasks(callbackFn);
    } else {
      this.resetSearchField();
    }
    this.isSearched = false;
  }

  private doReset(event): void {
    this.resetSearchField();
    this.focusSearchInput(event);
    this.emitSearch();
    this.trackReset();
    this.emitIsReset.emit(true);
  }

  private focusSearchInput(event: KeyboardEvent): void {
    if (event.which === 13) {
      this.searchInput.nativeElement.focus();
    }
  }

  private emitSearch(): void {
    this.search.emit(this.searchQuery);
  }

  private setPreviewAPIParam() {
    if (this.source) {
      this.previewAPIParameter = PreviewAPIParameter[this.source];
    }
  }

  trackSearch(): void {
    this.gaService.trackEvent(
      this.ga.CATEGORY + this.ga.SUB_CATEGORY[this.source],
      this.ga.SEARCH_BUTTON,
      this.ga.SELECTED);
  }

  trackReset(): void {
    this.gaService.trackEvent(
      this.ga.CATEGORY + this.ga.SUB_CATEGORY[this.source],
      this.ga.RESET,
      this.ga.SELECTED);
  }

  resetSearchField(): void {
    this.searchQuery = '';
    this.searchFormData.reset();
    this.searchFormData.markAsPristine();
    this.emptyReset.emit(true);
    this.saveToSessionBasedOnTab(this.inHouseCheckCondition);
  }

  handleGuestSelection(value: string): void {
    this.arrivalsService.handleUnsavedTasks(() => {
      this.isSearched = true;
      this.emitGuestSelection.emit(value);
      this.saveToSessionBasedOnTab(this.inHouseCheckCondition, 'searchValue', this.searchFormData.get('search').value);
    });
  }

  handleGroupSelection(value: string): void {
    this.arrivalsService.handleUnsavedTasks(() => {
      this.isSearched = true;
      this.saveToSessionBasedOnTab(this.inHouseCheckCondition);
      this.emitGroupSelection.emit(value);
      this.emitIsReset.emit(false);
    });
  }

  handleAllGroupSelection(value: string): void {
    this.arrivalsService.handleUnsavedTasks(() => {
      this.isSearched = true;
      this.saveToSessionBasedOnTab(this.inHouseCheckCondition);
      this.emitAllGroupSelection.emit(value);
      this.emitIsReset.emit(false);
    });
  }

  saveToSessionBasedOnTab(conditionFunc: Function, sessionVarName: string = 'searchValue', value: string = '') {
    if (conditionFunc()) {
      this.storageService.setSessionStorage(sessionVarName, value);
    }
  }
}
