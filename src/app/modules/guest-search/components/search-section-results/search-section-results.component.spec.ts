import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSectionResultsComponent } from './search-section-results.component';
import { TranslateModule } from '@ngx-translate/core';

xdescribe('SearchSectionResultsComponent', () => {
  let component: SearchSectionResultsComponent<any>;
  let fixture: ComponentFixture<SearchSectionResultsComponent<any>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchSectionResultsComponent],
      imports: [TranslateModule.forRoot()]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSectionResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
