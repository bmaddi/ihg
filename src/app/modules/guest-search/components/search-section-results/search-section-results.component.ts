import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { PreviewSection, PreviewTextPipe } from '../../enums/preview-section.enum';
import { DataDisplayProperty, PreviewSearchClickEvent } from '../../interfaces/search-preview.interface';

@Component({
  selector: 'app-search-section-results',
  templateUrl: './search-section-results.component.html',
  styleUrls: ['./search-section-results.component.scss']
})
export class SearchSectionResultsComponent<T> implements OnInit {
  @Input() totalsLabel: string;
  @Input() sectionIdentifier: PreviewSection;
  @Input() resultsLimit = 4;
  @Input() filterData: T[] = [];
  @Input() dataProps: DataDisplayProperty[] = [];
  @Input() seleniumPrefix: string;
  @Output() selectionMade: EventEmitter<T | PreviewSearchClickEvent> = new EventEmitter();

  showMore = false;
  previewPipes = PreviewTextPipe;
  listHeaderTag: string;
  sectionHeaderTag: string;

  constructor() {
  }

  ngOnInit() {
    this.listHeaderTag = this.seleniumPrefix + 'SearchDropdown-SID';
    this.sectionHeaderTag = this.seleniumPrefix + 'SearchHeader-SID';
  }

  totalsClick() {
      this.selectionMade.emit(<PreviewSearchClickEvent>{ section: this.sectionIdentifier, value: 'All' });
  }

  selectionClick(item: T) {
    this.selectionMade.emit(<PreviewSearchClickEvent>{
      section: this.sectionIdentifier,
      value: item['groupName'] || item['confirmationNumber']
    });
  }

  showMoreClick() {
    this.totalsClick();
  }

  getRowTag(index: number): string {
    return this.seleniumPrefix + 'Row' + (index + 1) + '-SID';
  }
}
