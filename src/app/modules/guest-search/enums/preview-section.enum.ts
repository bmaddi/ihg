export enum PreviewSection {
  guestsPreview = 'GuestsListPreview',
  groupsPreview = 'GroupsListPreview',
  roomsPreview = 'RoomsListPreview',
}

export enum PreviewAPIParameter {
  ARRIVALSTAB = 'ARRIVAL',
  PREPARETAB = 'PREPARE',
  INHOUSETAB = 'INHOUSE',
  ALLGUESTTAB = 'ALLGUEST',
}

export enum PreviewTextPipe {
  titleCase = 'titleCase'
}

