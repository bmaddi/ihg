import { GuestSearchModule } from './guest-search.module';

describe('GuestSearchModule', () => {
  let guestSearchModule: GuestSearchModule;

  beforeEach(() => {
    guestSearchModule = new GuestSearchModule();
  });

  it('should create an instance', () => {
    expect(guestSearchModule).toBeTruthy();
  });
});
