import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PopupModule } from '@progress/kendo-angular-popup';

import { GuestSearchComponent } from './components/guest-search/guest-search.component';
import { GuestSearchPreviewComponent } from '@modules/guest-search/components/guest-search-preview/guest-search-preview.component';
import { SearchSectionResultsComponent } from '@modules/guest-search/components/search-section-results/search-section-results.component';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    PopupModule,
    NgbTooltipModule
  ],
  declarations: [
    GuestSearchComponent,
    GuestSearchPreviewComponent,
    SearchSectionResultsComponent
  ],
  exports: [
    GuestSearchComponent,
  ]
})
export class GuestSearchModule { }
