import { PreviewAPIParameter, PreviewSection, PreviewTextPipe } from '../enums/preview-section.enum';

export interface PreviewSearchQueryParams {
  searchBy?: string;
  day?: string;
  tab?: PreviewAPIParameter;
}

export interface PreviewGuestItem {
  firstName: string;
  lastName: string;
  name?: string;
  roomNumber?: string;
  confirmationNumber: string;
}

export interface PreviewSearchClickEvent {
  section: PreviewSection;
  value?: any;
}

export interface DataDisplayProperty {
  property: string;
  propertyPipe?: PreviewTextPipe;
  separator?: string;
}
