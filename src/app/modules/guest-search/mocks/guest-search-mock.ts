import {RoomChangeModel} from '@modules/prepare/models/arrivals-checklist.model';
import {PreviewGuestItem} from '@modules/guest-search/interfaces/search-preview.interface';

export const MOCK_ASSIGNED_ROOM_1: RoomChangeModel = {
  previousRoomNumber: '238',
  newRoomNumber: '245',
  reservationNumber: '45458888'
};

export const MOCK_ASSIGNED_ROOM_2: RoomChangeModel = {
  previousRoomNumber: '245',
  newRoomNumber: '238',
  reservationNumber: '45458888'
};

export const MOCK_GUEST_LIST: PreviewGuestItem[] = [
  {
    'firstName': 'VIJAY',
    'lastName': 'DAS',
    'confirmationNumber': '45458888',
    'roomNumber': '238',
  },
  {
    'firstName': 'Testinhouse',
    'lastName': 'Twentynov',
    'confirmationNumber': '45539044',
    'roomNumber': '106',
  },
  {
    'firstName': 'Test User B',
    'lastName': 'Rising Phoenix',
    'confirmationNumber': '48254134',
    'roomNumber': '101',
  },
  {
    'firstName': 'Adam',
    'lastName': 'Sandler',
    'confirmationNumber': '27724630',
    'roomNumber': '1310',
  }
];
