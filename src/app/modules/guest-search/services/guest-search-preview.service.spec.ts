import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';

import { ApiService, EnvironmentService, UserService } from 'ihg-ng-common-core';

import { GuestSearchPreviewService } from './guest-search-preview.service';
import { PreviewAPIParameter } from '@modules/guest-search/enums/preview-section.enum';
import { PreviewGuestItem } from '@modules/guest-search/interfaces/search-preview.interface';

describe('GuestSearchPreviewService', () => {
  const mockAPIURL = 'https://concerto.mock.test/fdk/concertoservices/v1/';
  const environmentServiceServiceStub = {
    getEnvironmentConstants: (): { [key: string]: string } => {
      return {fdkAPI: mockAPIURL};
    }
  };
  const expectedEndPoint = mockAPIURL + 'guest/preview';
  const userServiceStub = {};
  const mockPreviewList = [
    {
      'firstName': 'Test User A',
      'lastName': '##Rising Phoenix',
      'confirmationNumber': '28492449',
      'errors': null
    },
    {
      'firstName': 'Raghav',
      'lastName': 'A',
      'confirmationNumber': '41547609',
      'errors': null
    },
    {
      'firstName': 'Owenss',
      'lastName': 'Amelias',
      'confirmationNumber': '48502823',
      'errors': null
    },
    {
      'firstName': 'Raghavendra',
      'lastName': 'Amilineni',
      'confirmationNumber': '25060126',
      'errors': null
    }];

  let service: GuestSearchPreviewService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;


  beforeEach(() => {
    TestBed.overrideProvider(EnvironmentService, {
      useValue: environmentServiceServiceStub
    });

    TestBed.overrideProvider(UserService, {
      useValue: userServiceStub
    });

    TestBed.configureTestingModule({
      providers: [ApiService, EnvironmentService, UserService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(GuestSearchPreviewService);
    httpMockController = TestBed.get(HttpTestingController);
  });


  it('should be created', () => {
    service = TestBed.get(GuestSearchPreviewService);
    expect(service).toBeTruthy();
  });

  it('should send proper query parameters if present', () => {


    service.getPreviewResults(PreviewAPIParameter.ARRIVALSTAB, '2019-10-20', 'Test').subscribe(data => {
      expect(data).toBeDefined();
    });

    request = httpMockController.expectOne(req => req.url === expectedEndPoint);

    expect(request.request.params.get('tab')).toEqual(PreviewAPIParameter.ARRIVALSTAB);
    expect(request.request.params.get('day')).toEqual('2019-10-20');
    expect(request.request.params.get('searchBy')).toEqual('Test');
    expect(request.request.headers.get('spinnerConfig')).toEqual('N');

    expect(request.request.method).toBe('GET');

    request.flush(mockPreviewList);

    httpMockController.verify();
  });

  it('should send only required parameters by default', () => {

    service.getPreviewResults().subscribe(data => {
      expect(data).toBeDefined();
    });

    request = httpMockController.expectOne(req => req.url === expectedEndPoint);

    expect(request.request.params.get('tab')).toEqual(PreviewAPIParameter.PREPARETAB);
    expect(request.request.params.get('day')).toBeNull();
    expect(request.request.params.get('searchBy')).toBeNull();
    expect(request.request.headers.get('spinnerConfig')).toEqual('N');

    expect(request.request.method).toBe('GET');

    request.flush(mockPreviewList);

    httpMockController.verify();
  });

  it('should send only optional parameters if truthy', () => {
    service.getPreviewResults(PreviewAPIParameter.ARRIVALSTAB, '', null).subscribe(data => {
      expect(data).toBeDefined();
    });

    request = httpMockController.expectOne(req => req.url === expectedEndPoint);

    expect(request.request.params.get('tab')).toEqual(PreviewAPIParameter.ARRIVALSTAB);
    expect(request.request.params.get('day')).toBeNull();
    expect(request.request.params.get('searchBy')).toBeNull();
    expect(request.request.headers.get('spinnerConfig')).toEqual('N');

    expect(request.request.method).toBe('GET');

    request.flush(mockPreviewList);

    httpMockController.verify();
  });

  it('should properly map preview guest data to concat first and last name to property name and return it as such', () => {
    service.getPreviewResults().subscribe((data: PreviewGuestItem[]) => {
      expect(data).toBeDefined();
      data.forEach(guest => {
          expect(guest.name).toBeDefined();
          expect(guest.name.indexOf(guest.firstName)).toBeGreaterThanOrEqual(0);
          expect(guest.name.indexOf(guest.lastName)).toBeGreaterThanOrEqual(0);
        }
      );
    });

    request = httpMockController.expectOne(req => req.url === expectedEndPoint);

    expect(request.request.params.get('tab')).toEqual(PreviewAPIParameter.PREPARETAB);

    expect(request.request.method).toBe('GET');

    request.flush(mockPreviewList);

    httpMockController.verify();
  });

});
