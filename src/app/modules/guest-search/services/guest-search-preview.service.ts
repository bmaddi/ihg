import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { omitBy } from 'lodash';

import { ApiService, ApiUrl, EnvironmentService } from 'ihg-ng-common-core';

import { PreviewGuestItem, PreviewSearchQueryParams } from '../interfaces/search-preview.interface';
import { PreviewAPIParameter } from '../enums/preview-section.enum';

@Injectable({
  providedIn: 'root'
})
export class GuestSearchPreviewService {

  constructor(private apiService: ApiService,
              private environmentService: EnvironmentService) {
  }

  getPreviewResults(tab: PreviewAPIParameter = PreviewAPIParameter.PREPARETAB,
                    day?: string,
                    searchBy?: string): Observable<PreviewGuestItem[]> {
    const queryParams: PreviewSearchQueryParams = omitBy({day, tab, searchBy}, value => !value);
    const headers = new HttpHeaders().set('spinnerConfig', 'N');

    return this.apiService.get(
      new ApiUrl(this.environmentService.getEnvironmentConstants()['fdkAPI'], 'guest/preview'),
      queryParams as { [param: string]: string },
      headers
    ).pipe(
      map((data: PreviewGuestItem[]) => {
        data.forEach(g => g.name = `${g.firstName} ${g.lastName}`);
        return data;
      }));
  }
}
