import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddObsCancelModalComponent } from './add-obs-cancel-modal.component';

xdescribe('AddObsCancelModalComponent', () => {
  let component: AddObsCancelModalComponent;
  let fixture: ComponentFixture<AddObsCancelModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddObsCancelModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddObsCancelModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
