import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
export interface AddObsDismissModalOptions {
  titleKey: string;
  textKey: string;
  cancelKey: string;
  continueKey: string;
}
@Component({
  selector: 'app-add-obs-cancel-modal',
  templateUrl: './add-obs-cancel-modal.component.html',
  styleUrls: ['./add-obs-cancel-modal.component.scss']
})
export class AddObsCancelModalComponent implements OnInit {
  @Input() titleKey = 'TITLE';
  @Input() textKey = 'TEXT';
  @Input() cancelKey = 'COM_BTN_CANCEL';
  @Input() continueKey = 'COM_BTN_CNTNU';
  resolve: AddObsDismissModalOptions;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  closeModal() {
    this.activeModal.dismiss();
  }

  continue() {
    this.activeModal.close(true);
  }
}
