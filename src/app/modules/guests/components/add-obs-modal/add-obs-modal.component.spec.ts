import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddObservationModalComponent } from './add-obs-modal.component';

xdescribe('AddObservationModalComponent', () => {
  let component: AddObservationModalComponent;
  let fixture: ComponentFixture<AddObservationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddObservationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddObservationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
