import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GUEST_CONST, observationReportIssueConfig } from '@app/modules/guests/guests.constants';
import { TranslateService } from '@ngx-translate/core';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { NewGuestObservation, GuestProfileModel, ValueLabelModel } from '@app/modules/guests/models/guest-profile.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddObsCancelModalComponent } from '../add-obs-cancel-modal/add-obs-cancel-modal.component';
import { NgForm } from '@angular/forms';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';

@Component({
  selector: 'app-add-obs-modal',
  templateUrl: './add-obs-modal.component.html',
  styleUrls: ['./add-obs-modal.component.scss']
})
export class AddObservationModalComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  guest: GuestProfileModel;
  formIsChangedStatus: boolean;
  formChangesSubscription$: any;
  keyMap = GUEST_CONST.KEYMAP;
  maxCommentChars = 140;
  charsLeftCount: number;
  @ViewChild('form') ngForm: NgForm;
  formData: {
    comment: string;
    visibility: string;
    category: string;
    ack: boolean;
  };
  private defaltVisibility: string;
  private defaultCategoryLabel: string;
  visibilityData: ValueLabelModel[];
  categoryData: ValueLabelModel[];
  toastTranslationConfig: Partial<ErrorToastMessageTranslations>;
  reportIssueAutoFill = observationReportIssueConfig;

  constructor(
    private activeModal: NgbActiveModal,
    private translate: TranslateService,
    private guestService: GuestListService,
    private modalService: NgbModal
    ) {
      super();
    }

  ngOnInit() {
   this.formChangesSubscription$ = this.ngForm.form.valueChanges.subscribe(x => {
     this.isFormChanged(x);
    });
    this.initformData();
    this.initToastErrorConfig();
    super.init();
  }

  private initformData() {
    this.charsLeftCount = this.maxCommentChars;
    this.defaltVisibility = this.translate.instant(this.keyMap.obsModalSelectVisibility);
    this.defaultCategoryLabel = this.translate.instant(this.keyMap.obsModalSelectCategory);
    this.formData = {
      comment: '',
      visibility: null,
      category: null,
      ack: false
    };
    this.categoryData = [
      {
        value: null,
        label: this.defaultCategoryLabel
      }
    ].concat(this.guestService.getCategories());
    this.visibilityData = [
      {
        value: null,
        label: this.defaltVisibility
      }
    ].concat(this.guestService.getVisibilities());
  }

  private initToastErrorConfig() {
    this.toastTranslationConfig = {
      timeoutError: {
        message: this.keyMap.timeoutToast,
        detailMessage: this.keyMap.unableToAddObsTryAgainToast,
        detailMessageMoreErrors: this.keyMap.unableToAddObsToast
      },
      generalError: {
        message: this.keyMap.errorToast,
        detailMessage: this.keyMap.unableToAddObsTryAgainToast,
        detailMessageMoreErrors: this.keyMap.unableToAddObsToast
      }
    };
  }

  onRetry() {
    this.sendAddObservationRequest();
  }

  onReportAnIssue() {
    this.dismissModal(true);
  }

  onAddObservation() {
    this.sendAddObservationRequest();
  }

  sendAddObservationRequest() {
    const newObservation: NewGuestObservation = {
      category: this.formData.category,
      text: this.formData.comment,
      visibleToAll: this.formData.visibility === this.guestService.visibilityValues.allHotels,
      memberId: this.guest.guestLoyalty.loyaltyID
    };
    this.guestService.createObservation(newObservation).subscribe((data) => {
      if (!this.checkIfAnyApiErrors(data)) {
        this.activeModal.close();
      }
    }, error => {
      this.processHttpErrors(error);
  });
  }
  isInvalid() {
    const {comment, visibility, category, ack} = this.formData;
    const isValid = (comment.length > 0 && visibility !== null && category !== null && ack);
    return !isValid;
  }

  onClose() {
    this.dismissModal(false);
  }

  onCancel() {
    this.dismissModal(false);
  }
  ngOnDestroy() {
    this.formChangesSubscription$.unsubscribe();
  }

  private dismissModal(ignoreFormChanges: boolean) {
    if (this.formIsChangedStatus && !ignoreFormChanges) {
      const activeModal = this.modalService.open(AddObsCancelModalComponent, {
        windowClass: 'guest-new-observation-modal',
        backdrop: 'static'});
      activeModal.componentInstance.resolve = {
        titleKey: this.keyMap.dismissModalHead,
        textKey: this.keyMap.dismissModalText,
        cancelKey: this.keyMap.dismissModalContinue,
        continueKey: this.keyMap.dismissModalDiscard
      };
      activeModal.result
        .then(() => {
          this.activeModal.dismiss();
        }, () => {
        });
    } else {
      this.activeModal.dismiss();
      this.formIsChangedStatus = false;
    }
  }
  isFormChanged(res: any) {
      const categoryChangeStatus = res.category === null ? false : true;
      const visibilityChangeStatus = res.visibility === null ? false : true;
      const commentChangeStatus = res.comment === '' ? false : true;
       this.formIsChangedStatus = ( commentChangeStatus || res.ack || visibilityChangeStatus || categoryChangeStatus ) ? true : false;
    }
}
