import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupProfileEditPrefCheckboxComponent } from './group-profile-edit-pref-checkbox.component';

xdescribe('GroupProfileEditPrefCheckboxComponent', () => {
  let component: GroupProfileEditPrefCheckboxComponent;
  let fixture: ComponentFixture<GroupProfileEditPrefCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupProfileEditPrefCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupProfileEditPrefCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
