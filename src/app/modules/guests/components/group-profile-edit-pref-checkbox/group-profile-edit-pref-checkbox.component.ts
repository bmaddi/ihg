import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { KeyDesModel } from '@app/modules/guests/models/guest-profile.model';

@Component({
  selector: 'app-group-profile-edit-pref-checkbox',
  templateUrl: './group-profile-edit-pref-checkbox.component.html',
  styleUrls: ['./group-profile-edit-pref-checkbox.component.scss']
})
export class GroupProfileEditPrefCheckboxComponent implements OnInit {

  constructor() { }

  @Input() titleLabel: string;
  @Input() subTopic: string;
  @Input() form: FormGroup;
  @Input() checkBoxData: KeyDesModel[];
  @Input() checkboxType: string;
  @Input() classType: string = 'col-4';

  ngOnInit() {
    
  }


}
