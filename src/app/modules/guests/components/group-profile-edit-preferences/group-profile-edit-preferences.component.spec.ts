import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupProfileEditPreferencesComponent } from './group-profile-edit-preferences.component';

xdescribe('GroupProfileEditPreferencesComponent', () => {
  let component: GroupProfileEditPreferencesComponent;
  let fixture: ComponentFixture<GroupProfileEditPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupProfileEditPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupProfileEditPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
