import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { differenceWith, isEqual } from 'lodash';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { GuestInterestsResponse, GuestInterestSectionResponse, ValuePreferenceResponse, PreferenceUpdate, OptionPreferencesResponse } from '@app/modules/guests/models/guest-interest.models';
import { KeyDesModel, PreferenceData, PreferenceResponseData } from '@app/modules/guests/models/guest-profile.model';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { TranslateService } from '@ngx-translate/core';
import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { GUEST_PROFILE_PREFERENCES_OPTIONS } from '@app/modules/guests/guest-profile-preferences-options';
import { UtilityService } from '@app/services/utility/utility.service';

@Component({
  selector: 'app-group-profile-edit-preferences',
  templateUrl: './group-profile-edit-preferences.component.html',
  styleUrls: ['./group-profile-edit-preferences.component.scss']
})
export class GroupProfileEditPreferencesComponent extends AppErrorBaseComponent implements OnInit {

  public disableSave = true;
  public initialized = false;
  public hasErrors = true;
  keyMap = GUEST_CONST.KEYMAP;
  elevator: KeyDesModel[];
  floor: KeyDesModel[];
  smoking: KeyDesModel[];
  prefData: PreferenceData;
  preferenceForm: FormGroup;
  guestPreferenceData: PreferenceResponseData;
  guestInterests: GuestInterestsResponse;
  interest = [];
  exercise = [];
  relaxation = [];
  destinations = [];
  favorite_team: string = "";
  pet_type: string = "";
  pet_name: string = "";
  textFields = [];
  private sectionNames = GUEST_CONST.GUEST_INTERESTS.sectionNames;
  private guestInterestsFields = GUEST_CONST.GUEST_INTERESTS;
  public savePreferences: PreferenceUpdate[] = [];
  public formBeforeDirty: PreferenceUpdate[] = [];
  public formAfterDirty: PreferenceUpdate[] = [];
  public preferencesCheckboxs = GUEST_CONST.GUEST_PREFERENCES_CHECKBOX;
  public preferencesFields = GUEST_CONST.GUEST_PREFERENCES_FIELDS;
  toastTranslationConfig: Partial<ErrorToastMessageTranslations>;
  reportIssueConfig: ReportIssueAutoFillData = {
    function: 'Guests',
    topic: 'Guest Profile - Room Preferences/Guest Interests',
    subtopic: 'Error Message'
  };
  private ga = GUEST_CONST.GA;
  private action;
  private label;
  constructor(private activeModal: NgbActiveModal, private fb: FormBuilder,
    private translate: TranslateService,
    private guestListService: GuestListService,
    private utilityService: UtilityService,
    private gaService: GoogleAnalyticsService) 
  { super(); }

  ngOnInit() {
    this.prefData = {
      elevator: null,
      floor: null,
      smoking: null,
      dietary: null
    }
    this.getPreferences();
    this.initToastErrorConfig();
    this.action = this.utilityService.getGAString(this.ga.ROOM_PREFERENCE, this.ga.GUEST_INTERESTS, ' / ');
  }

  formInit() {
    this.updatePreferenceData();
    this.getFields();
    this.updateSelectedValues(GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.destinations, this.guestPreferenceData.destinations as ValuePreferenceResponse[]);
    this.preferenceForm = this.fb.group({
      elevator: new FormControl(this.prefData.elevator),
      floor: new FormControl(this.prefData.floor),
      smoking: new FormControl(this.prefData.smoking),
      dietary: new FormControl(this.prefData.dietary),
      favorite_team: new FormControl(this.favorite_team),
      pet_name: new FormControl(this.pet_name),
      pet_type: new FormControl(this.pet_type),
      interest: new FormArray(this.interest.map(control => new FormControl(eval(control.selectedValue)))),
      exercise: new FormArray(this.exercise.map(control => new FormControl(eval(control.selectedValue)))),
      relaxation: new FormArray(this.relaxation.map(control => new FormControl(eval(control.selectedValue)))),
      destinations: new FormArray(this.destinations.map(control => new FormControl(eval(control.selectedValue)))),
    });
    this.updateCheckboxChanges();
  }

  updateSelectedValues(value: string, section: ValuePreferenceResponse[]) {
    GUEST_PROFILE_PREFERENCES_OPTIONS[value].forEach((parent: KeyDesModel) => {
      section.forEach((child) => {
        parent.selectedValue = parent.key === child.key ? child.selectedValue : parent.selectedValue;
      })
    });
    GUEST_PROFILE_PREFERENCES_OPTIONS[value] = GUEST_PROFILE_PREFERENCES_OPTIONS[value].filter(pSearch => section
      .find((cSearch: ValuePreferenceResponse) => pSearch.key === cSearch.key));
    section.filter(pSearch => !GUEST_PROFILE_PREFERENCES_OPTIONS[value]
    .find((cSearch: ValuePreferenceResponse) => pSearch.key === cSearch.key))
    .map(data => GUEST_PROFILE_PREFERENCES_OPTIONS[value].push(data));
    this[value] = GUEST_PROFILE_PREFERENCES_OPTIONS[value];
  }

  updatePreferenceData() {
    this.guestInterests.interests.forEach((data: GuestInterestSectionResponse) => {
      if(data.description === this.sectionNames.guestInterests) {
        this.updateSelectedValues(GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.interest, data.preferences as ValuePreferenceResponse[]);
      }else if(data.description === this.sectionNames.exercisePrefs) {
        this.updateSelectedValues(GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.exercise, data.preferences as ValuePreferenceResponse[]);
      }else if(data.description === this.sectionNames.relaxationPrefs) {
        this.updateSelectedValues(GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.relaxation, data.preferences as ValuePreferenceResponse[]);
      }else if(data.description === this.sectionNames.dietaryPrefs) {
        this.prefData[data.key] = data.preferences[0]["selectedValue"];
        data.preferences[0]["options"].filter((pSearch: ValuePreferenceResponse) => !GUEST_PROFILE_PREFERENCES_OPTIONS.dietary
        .find((cSearch: ValuePreferenceResponse) => pSearch.key === cSearch.key))
        .map(data => GUEST_PROFILE_PREFERENCES_OPTIONS.dietary.push(data));
        GUEST_PROFILE_PREFERENCES_OPTIONS.dietary.map(model => {
          model.description = this.translate.instant(model.description)
        })
        this[data.key] = GUEST_PROFILE_PREFERENCES_OPTIONS.dietary;
      }else if(data.description === this.sectionNames.guestInsights) {
        this.textFields.push(data.preferences);
      }else if(data.description === this.sectionNames.travelingWithPet) {
        this.textFields.push(data.preferences);
      }
    })
  }

  getFields(){
    this.textFields.forEach((data: ValuePreferenceResponse[]) => {
      data.forEach((field) => {
        if(field.key === this.guestInterestsFields.favoriteTeamKey){
          this.favorite_team = field.selectedValue;
        }else if(field.key === this.guestInterestsFields.petNameKey){
          this.pet_name = field.selectedValue;
        }else if(field.key === this.guestInterestsFields.petTypeKey){
          this.pet_type = field.selectedValue;
        }
      })
    })
  }

  getPreferences() {
    this.handleDataChange(this.guestListService.getPreferenceResponseData(), this.guestListService.getGuestInterest());
  }

  updateCheckboxChanges() {
    const formUpdates = !this.preferenceForm.dirty ? 'formBeforeDirty' : 'formAfterDirty';
    this.preferencesCheckboxs.forEach((element: string) => {
      this.preferenceForm.value[element]
        .map((checked:boolean, index:number) => {
          if(checked){
            this[formUpdates].push({
                "key":this[element][index].key,
                "value":"true"
            });
          }else {
            this[formUpdates].push({
              "key":this[element][index].key,
              "value":"false"
          });
          } 
        });
    });
    this.preferencesFields.forEach((element: string) => {
      this[formUpdates].push({
        "key": element,
        "value": this.preferenceForm.value[element]
      })
    })
  }

  private initToastErrorConfig() {
    this.toastTranslationConfig = {
      timeoutError: {
        message: this.keyMap.timeoutToast,
        detailMessage: this.keyMap.unabletoSaveEditPreferences
      },
      generalError: {
        message: this.keyMap.errorToast,
        detailMessage: this.keyMap.unabletoSaveEditPreferences
      }
    };
  }

  private handleDataChange = (preference:PreferenceResponseData, interests: GuestInterestsResponse) => {
    this.initialized = true;
    this.hasErrors = false;
    if(preference !== null && interests !== null) {
      this.guestPreferenceData = preference;
      this.guestInterests = interests;
      const getPrefData = preference.roomBedPreferences;
      getPrefData.forEach((dataItem) => {
        this.prefData[dataItem.key] = dataItem.selectedValue;
        this[dataItem.key] = dataItem.options;
      });
      this.formInit();
    }
  }

  onClose() {
    this.activeModal.close();
  }

  onCancel() {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.CANCEL_BUTTON);
    this.activeModal.dismiss();
  }

  onSave() {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.SAVE_BUTTON);
    this.updateCheckboxChanges();
    this.savePreferences = differenceWith(this.formAfterDirty, this.formBeforeDirty, isEqual);
    this.guestListService.savePreferences(this.savePreferences)
      .subscribe(() => {
        this.guestListService.refreshGridEvent.next();
        this.activeModal.close();
      }, error => {
        this.formAfterDirty = [];
        this.processHttpErrors(error);
    });
  }

  onRetry() {
    this.label = this.utilityService.getGAString( this.ga.SAVE_FAILED , this.ga.RETRY , ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);
    this.onSave();
  }

  onReportAnIssue() {
    this.label = this.utilityService.getGAString( this.ga.SAVE_FAILED , this.ga.REPORT_ISSUE , ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);
    this.activeModal.dismiss();
  }
}
