import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestArrivalsComponent } from './guest-arrivals.component';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA, SimpleChanges, SimpleChange } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService, ToastMessageOutletService, DetachedToastMessageService, SessionStorageService, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { RouterTestingModule } from '@angular/router/testing';
import { TitleCasePipe } from '@angular/common';
import { GuestListService } from '../../services/guest-list.service';
import { of, Subject, Observable } from 'rxjs';
import { ArrivalGuestList, GROUP_LIST } from '../../mocks/arrival-guest-list-mock';
import { PreviewSearchData } from '@app/modules/group-block/models/group-block.model';
import { GroupBlockService } from '@app/modules/group-block/services/group-block.service';
import { cloneDeep } from 'lodash';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import moment = require('moment');
import { AppConstants } from '@app/constants/app-constants';


export class MockSessionStorageService {
  getSessionStorage() {
    return {};
  }
  public setSessionStorage(key, value) { }
}

export class MockGroupBlockService {
  public previewSearch: Subject<PreviewSearchData> = new Subject();
  public showGroups: Subject<void> = new Subject();
}

export class MockGuestListService {
  public retryArrivalPrint = new Subject<void>();
  public printArrivalError = new Subject<EmitErrorModel>();
  getGuestList() {
    return of({ ArrivalGuestList });
  }

}

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) {
  }
}

describe('GuestArrivalsComponent', () => {
  let component: GuestArrivalsComponent;
  let fixture: ComponentFixture<GuestArrivalsComponent>;
  let showWalkin: boolean = false;
  let guestListService: GuestListService;
  let groupBlockService: GroupBlockService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [TranslateModule.forRoot(), NgbModule, HttpClientTestingModule, TranslateModule.forRoot(),
      RouterTestingModule.withRoutes([
      ])],
      declarations: [GuestArrivalsComponent],
      providers: [UserService, ToastMessageOutletService, DetachedToastMessageService, TitleCasePipe,
        { provide: GuestListService, useClass: MockGuestListService },
        { provide: SessionStorageService, useClass: MockSessionStorageService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: GroupBlockService, useClass: MockGroupBlockService }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestArrivalsComponent);
    component = fixture.componentInstance;
    guestListService = TestBed.get(GuestListService);
    groupBlockService = TestBed.get(GroupBlockService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Fetch guest list without any search param ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.fetchGuestList();
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy()
  });

  it('Fetch guest list when there is date selected ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.selector = 2;
    component.localCurrentDate =moment(new Date(), AppConstants.DB_DT_FORMAT).toDate();
    component.fetchGuestList();
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy()
  });

  it('Fetch guest list with search param ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.onGuestSearch("hema");
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy()
  });

  it('Fetch guest list when search by grop name ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.handleGroupSelection("Rising Phoneix");
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy();
  });


  it('Check on Guest list state change ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.onStateChange({ "group": [], "skip": 10, "sort": [{ "field": "guestName", "dir": "asc" }], "take": 10 });
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy();
  });


  it('Check on Guest list populating on onGuestListRefresh ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.onGuestListRefresh();
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy();
  });


  it('Handle when guest list request throws http error ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of({ error: "test" }));
    component.onGuestSearch("hema");
    component['handleErrors']({});
    fixture.detectChanges();
    expect(component.guestData.length).toBe(0);
  });

  it('Guest list request throws http error when handlePrintAll called', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of({ error: "test" }));
    component.onGuestSearch("hema");
    component['handlePrintAll']();
    fixture.detectChanges();
    expect(component.guestData).toBe(undefined);
  });
  it('check guest list called for print when retry print ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of({ error: "test" }));
    guestListService.retryArrivalPrint.next();
    component.onGuestSearch("hema");
    component['subscribeToArrivalPrintRetry']();
    fixture.detectChanges();
    expect(component.printableRecords).not.toBeNull();
  });
  //this.guestListService.retryArrivalPrint

  it('Handle when guest list request retuns no guests ', () => {
    let arrivalList = cloneDeep(ArrivalGuestList);
    arrivalList.items = [];
    spyOn(guestListService, 'getGuestList').and.returnValue(of(arrivalList));
    component.onGuestSearch("hema");
    fixture.detectChanges();
    expect(component.guestData.length).toBe(0);
  });

  it('Handle guest list without any grid state hange ', () => {
    let arrivalList = cloneDeep(ArrivalGuestList);
    component.gridState = { skip: 1, sort: null };
    spyOn(guestListService, 'getGuestList').and.returnValue(of(arrivalList));
    component.onGuestSearch("hema");
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy();
  });

  it('Handle when  handlePreviewGroupSelection', () => {
    spyOn(groupBlockService, 'previewSearch').and.returnValue(of({ search: "", isAll: false }));
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.handlePreviewGroupSelection("", false);
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy();
  });

  it('Chek groups displaying when clicking show groups', () => {
    spyOn(groupBlockService, 'previewSearch').and.returnValue(of({ search: "", isAll: false }));
    spyOn(groupBlockService, 'showGroups');
    component.showArrivals = false;
    component.handleShowHideGroup(true);
    fixture.detectChanges();
    expect(component.showArrivals).toBeFalsy();
  });

  it('Chek groups displaying when clicking hide groups', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    spyOn(groupBlockService, 'previewSearch').and.returnValue(of({ search: "", isAll: false }));
    component.showArrivals = false;
    component.handleShowHideGroup(false);
    fixture.detectChanges();
    expect(component.showArrivals).toBeTruthy();
  });


  it('On change search selection', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.onSelectorChange(1);
    fixture.detectChanges();
    expect(guestListService.getGuestList).toHaveBeenCalled();
  });

  it('Check guest list populating on selecting guest from search box', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.handleGuestSelection('roy');
    fixture.detectChanges();
    expect(guestListService.getGuestList).toHaveBeenCalled();
  });


  it('Check group list populating when pass group list data to handleGroupList method', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.handleGroupList(GROUP_LIST);
    fixture.detectChanges();
    expect(component.groupList.length > 0).toBeTruthy();
  });


  it('print current guests', () => {
    component.handlePrint({ printAction: 'currentDetailed', items: [] });
    fixture.detectChanges();
    expect(component.printableRecords).not.toBeNull();
  });

  it('print current guests with action current compact', () => {
    component.handlePrint({ printAction: 'currentCompact', items: [] });
    fixture.detectChanges();
    expect(component.printableRecords).not.toBeNull();
  });


  it('print all guests', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.handlePrint({ printAction: 'allDetailed', items: [] });
    fixture.detectChanges();
    expect(component.printableRecords).not.toBeNull();
  });



  it('print all guests when print action all compact', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.handlePrint({ printAction: 'allCompact', items: [] });
    fixture.detectChanges();
    expect(component.printableRecords).not.toBeNull();
  });


  it('Check guest list populating on value changes ', () => {
    spyOn(guestListService, 'getGuestList').and.returnValue(of(ArrivalGuestList));
    component.isActiveTab = true;
    const changesObj: SimpleChanges = {
      isActiveTab: new SimpleChange(false, true, false)
    };
    component.ngOnChanges(changesObj);
    fixture.detectChanges();
    expect(component.guestData.length > 0).toBeTruthy()
  });

  // it('should check CreateWalkinButton is hidden ', () =>{
  //   expect(component).toBeTruthy();
  //   const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CreateWalkinReservation-SID"]')).nativeElement;
  //   expect(element).toBeNull();
  // });

  // it('should check CreateWalkinButton is enabled ', () =>{
  //   expect(component).toBeTruthy();
  //   const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CreateWalkinReservation-SID"]')).nativeElement;
  //   expect(element).not.toBeNull();
  // });
});
