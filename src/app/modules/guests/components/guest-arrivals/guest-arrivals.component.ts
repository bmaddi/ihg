import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { State } from '@progress/kendo-data-query';
import { Subject, Subscription } from 'rxjs';
import { cloneDeep } from 'lodash';

import { GoogleAnalyticsService, ToastMessageOutletService, SessionStorageService } from 'ihg-ng-common-core';

import { GuestGridModel, GuestListModel } from '@modules/guests/models/guest-list.models';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { UtilityService } from '@app/services/utility/utility.service';
import { FdkValidators } from '@app-shared/validators/app.validators';
import { searchValidRegex } from '@app/constants/app-constants';
import { GroupList } from '@modules/group-block/models/group-block.model';
import { GroupBlockService } from '@modules/group-block/services/group-block.service';
import { printTypes } from '@modules/print-record/constants/print-grid.constant';

@Component({
  selector: 'app-guest-arrivals',
  templateUrl: './guest-arrivals.component.html',
  styleUrls: ['./guest-arrivals.component.scss'],
  providers: [GroupBlockService]
})
export class GuestArrivalsComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {
  guestData: GuestGridModel[] = [];
  localCurrentDate: Date;
  keyMap = GUEST_CONST.KEYMAP;
  selector = 0;
  searchQuery = '';
  shouldNavigate = false;
  searchDate: Date;
  dateIndex: number;
  totalItems = 0;
  gridState: State;
  ga = GUEST_CONST.GA;
  activeTab: string;
  arrivalsSearchFormData: FormGroup;
  showGroups = null;
  showArrivals = true;
  selectedGroup = null;
  groupList: GroupList[] = [];
  detailedPrint: boolean;
  enableWalkIn: boolean = false;
  printableRecords = new Subject<GuestGridModel[]>();
  private minSearchCharLimit = 2;
  private previewSearchValue: string;
  private rootSubscription = new Subscription();

  @Input() isActiveTab: boolean;
  @ViewChild('searchInput') searchInput;
  @Output() search: EventEmitter<string> = new EventEmitter();

  constructor(
    private guestListService: GuestListService,
    private toastOutletService: ToastMessageOutletService,
    private gaService: GoogleAnalyticsService,
    private storageService: SessionStorageService,
    private utilityService: UtilityService,
    private groupBlockService: GroupBlockService) {
    super();
  }

  ngOnInit(): void {
    super.init();
    this.activeTab = this.ga.CATEGORY + this.ga.SUB_CATEGORY.ARRIVALSTAB;
    this.validateArrivalsSearch();
    this.subscribeToArrivalPrintRetry();
  }

  private subscribeToArrivalPrintRetry(): void {
    this.rootSubscription.add(this.guestListService.retryArrivalPrint.subscribe(() => this.handlePrintAll()));
  }

  validateArrivalsSearch() {
    this.arrivalsSearchFormData = new FormGroup({
      search: new FormControl('', Validators.compose([
        FdkValidators.validateMinWithTrimmedSpaces(this.minSearchCharLimit, this.utilityService.checkNonWhiteSpaceCharacterWithMinLength),
        Validators.pattern(searchValidRegex)
      ]))
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isActiveTab && changes.isActiveTab.currentValue) {
      this.selectedGroup = null;
      this.fetchGuestList();
    }
    super.resetSpecificError(this.guestListService.printArrivalError);
  }

  onStateChange(state: State): void {
    this.fetchGuestList(state);
  }

  fetchGuestList(state?: State) {
    const day = this.getDate();
    const searchColumn = this.selectedGroup ? 'group' : null;
    const searchBy = this.previewSearchValue || this.selectedGroup || this.searchQuery;
    this.searchDate = day;
    this.determineGridState(state);
    this.guestListService.getGuestList(day, searchBy, this.gridState, searchColumn).subscribe(
      this.handleDataChange,
      error => this.handleErrors(error));
  }

  private handleDataChange = (newData: GuestListModel) => {
    const hasErrors = this.checkIfAnyApiErrors(newData, data => !data || !data.items || data.items.length === 0);
    if (hasErrors) {
      this.guestData = [];
    }
    this.guestData = newData.items;
    this.localCurrentDate = newData.localCurrentDateParsed;
    this.totalItems = newData.totalItems;
    this.addHotelDateToSession();
    this.updateGroupDisplay();
  }

  private updateGroupDisplay(): void {
    this.updateShowGroupsShowArrivals(this.selectedGroup || (!this.groupList || this.groupList.length ? false : null), true);
  }

  private handleErrors = (error) => {
    this.guestData = [];
    this.totalItems = 0;
    this.updateGroupDisplay();
    this.processHttpErrors(error);
  }

  onGuestListRefresh(): void {
    this.fetchGuestList();
  }

  onSelectorChange(selector: number): void {
    this.gaService.trackEvent(this.activeTab, this.ga.DATE, this.ga.SELECTED);
    this.selector = selector > 0 ? selector : 0;
    this.shouldNavigate = false;
    this.dateIndex = selector;
    this.previewSearchValue = null;
    this.selectedGroup = null;
    this.fetchGuestList();
  }

  onGuestSearch(searchString: string): void {
    this.determineGridState();
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.searchQuery = searchString;
    this.selectedGroup = null;
    this.previewSearchValue = null;
    this.fetchGuestList();
  }

  private addHotelDateToSession(): void {
    this.storageService.setSessionStorage('localHotelTime', this.localCurrentDate);
  }

  private determineGridState = (state?: State) => {
    if (!state && this.gridState) {
      this.gridState.sort = null;
      this.gridState.skip = 0;
      return;
    }
    this.gridState = state;
  }

  public handleShowHideGroup(value): void {
    super.resetSpecificError(this.emitError);
    this.updateShowGroupsShowArrivals(value, !value);
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    if (this.showArrivals) {
      this.selectedGroup = null;
      this.fetchGuestList();
    } else {
      this.groupBlockService.showGroups.next();
    }
  }

  private updateShowGroupsShowArrivals(showGroups: boolean, showArrivals: boolean): void {
    this.showGroups = showGroups;
    this.showArrivals = showArrivals;
  }

  public handleGroupSelection(groupName: string): void {
    this.selectedGroup = groupName;
    this.previewSearchValue = null;
    this.fetchGuestList();
  }

  public handleGroupList(groupList: object): void {
    this.groupList = groupList['data'];
    if (!groupList['refresh']) {
      this.updateGroupDisplay();
    }
  }

  public handleGuestSelection(searchValue: string): void {
    this.selectedGroup = '';
    this.previewSearchValue = searchValue;
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.fetchGuestList();
  }

  public handlePreviewGroupSelection(searchValue: string, isAll = false): void {
    this.groupBlockService.previewSearch.next({ search: searchValue, isAll: isAll });
    this.updateShowGroupsShowArrivals(true, false);
    if (!isAll) {
      this.handleGroupSelection(searchValue);
    }
  }

  public handlePrint(data: { printAction: string, items: GuestGridModel[] }): void {
    this.detailedPrint = data.printAction === printTypes.CURR_DTLD || data.printAction === printTypes.ALL_DTLD;

    switch (data.printAction) {
      case printTypes.CURR_CMPCT:
      case printTypes.CURR_DTLD:
        this.printableRecords.next(data.items);
        break;
      case printTypes.ALL_CMPCT:
      case printTypes.ALL_DTLD:
        this.handlePrintAll();
        break;
    }
  }

  private handlePrintAll(): void {
    const gridState = cloneDeep(this.gridState);
    this.guestListService.getGuestList(this.getDate(), this.searchQuery, { ...gridState, take: 10000, skip: 0 })
      .subscribe((data: GuestListModel) => {
        if (!this.checkIfAnyApiErrors(data, null, this.guestListService.printArrivalError)) {
          this.printableRecords.next(data.items);
        }
      },
        error => {
          super.processHttpErrors(error, this.guestListService.printArrivalError);
        }
      );
  }

  private getDate(): Date {
    return this.localCurrentDate ? moment(this.localCurrentDate).add(this.selector, 'day').toDate() : null;
  }

  ngOnDestroy(): void {
    this.toastOutletService.setPageErrorDisabled(false);
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.rootSubscription.unsubscribe();
  }
}
