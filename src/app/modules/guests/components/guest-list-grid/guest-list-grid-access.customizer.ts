import { TemplateRef, ViewContainerRef } from '@angular/core';
import { AccessControlPermission, AccessControlPermissionType } from 'ihg-ng-common-core';

export class GuestListAccessCustomizer {

    static customize(templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        accessControlItem: AccessControlPermission,
        alternateRef: TemplateRef<any>) {
        if (!accessControlItem) { viewContainer.createEmbeddedView(templateRef); return; }

        if (accessControlItem.access === AccessControlPermissionType.ReadWrite ||
            accessControlItem.access === AccessControlPermissionType.ReadOnly) {
            viewContainer.createEmbeddedView(templateRef);
        } else if (accessControlItem.access === AccessControlPermissionType.NoAccess) {
            if (alternateRef) {
                viewContainer.createEmbeddedView(alternateRef);
            } else {
                viewContainer.clear();
            }
        }
    }
}
