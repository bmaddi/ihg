import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';
import { GoogleAnalyticsService, AccessControlModule, AccessControlService, AccessControlPermission } from 'ihg-ng-common-core';
import { GuestListGridComponent } from './guest-list-grid.component';
import { NO_ERRORS_SCHEMA, Component, Input, Output, Pipe, PipeTransform, EventEmitter } from '@angular/core';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TitleCasePipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { GridModule } from '@progress/kendo-angular-grid';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { GUEST_LIST_MOCK, MOCK_GUEST_DETAILS } from '@modules/guests/mocks/guestList-mock';
import { ARRIVALS_GROUP_REPORT_ISSUE, ARRIVALS_REPORT_ISSUE } from '@app/constants/error-autofill-constants';
import { TruncateTextPipe } from '@app/modules/shared/pipes/shorten-text.pipe';

@Pipe({ name: 'textHighlight' })
class TexthighlightPipeMock implements PipeTransform {
  transform(param) {
    return param;
  }
}

@Component({
  selector: 'app-service-error',
  template: ''
})

export class StubServiceErrorComponent {
  @Input() errorEvent;
  @Input() cardType;
  @Input() reportIssueAutoFill;
  @Output() refreshAction;
}
@Component({
  selector: 'app-prepare-arrivals-grid-pager',
  template: ''
})

export class StubPrepArrivalGridPagerComponent {
  @Input() gridView;
  @Input() settings;
  @Input() printMenuItems: { label: string, value: string }[];
  @Output() selectPageSize: EventEmitter<any> = new EventEmitter();
  @Output() print = new EventEmitter<string>();
}

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

describe('GuestListGridComponent', () => {
  let component: GuestListGridComponent;
  let fixture: ComponentFixture<GuestListGridComponent>;
  let translateService: TranslateService;
  let mockAccessControlService;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

  beforeEach(async(() => {
    mockAccessControlService = jasmine.createSpyObj(['getAccessControlPermissions']);
    mockAccessControlService.accessControlPermissions = [];
    TestBed.configureTestingModule({
      declarations: [GuestListGridComponent, StubServiceErrorComponent, StubPrepArrivalGridPagerComponent,
        TexthighlightPipeMock, TruncateTextPipe],
      imports: [IhgNgCommonKendoModule, HttpClientTestingModule, TranslateModule.forRoot(),
        IhgNgCommonComponentsModule, RouterTestingModule, GridModule, AccessControlModule, NgbTooltipModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [TranslateService, TitleCasePipe, GuestListService,
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: AccessControlService, useValue: mockAccessControlService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestListGridComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    component.gridView = GUEST_LIST_MOCK;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test formatDate', () => {
    const dateValue = component.formatDate('Mon Oct 21 2019');
    expect(dateValue).toEqual('21OCT2019');
  });

  it('should test dateComparision method', () => {
    component.searchDate = undefined;
    component.checkIn = undefined;
    component.dateComparision();
    expect(component.searchDate).toBeDefined();
    expect(component.checkIn).toBeDefined();
  });

  it('should test pageSizeUpdate method', () => {
    component.gridState = {
      sort: null,
      skip: 1
    };
    component.pageSizeUpdate(2);
    expect(component.gridState.skip).toEqual(0);
  });

  it('should test onDataStateChange method', () => {
    spyOn(component.stateChange, 'emit');
    const ev = new Event('keypress');
    component.onDataStateChange(ev);
    expect(component.stateChange.emit).toHaveBeenCalled();
  });

  it('should test fetchErrorConfig method arrivals group', () => {
    component.selectedGroup = 'ARRIVALS_GROUP';
    component.reportIssueConfig = undefined;
    component.fetchErrorConfig();
    expect(component.reportIssueConfig).toBeDefined();
    expect(component.reportIssueConfig).toEqual(ARRIVALS_GROUP_REPORT_ISSUE);
  });

  it('should test fetchErrorConfig method arrivals', () => {
    component.selectedGroup = undefined;
    component.reportIssueConfig = undefined;
    component.fetchErrorConfig();
    expect(component.reportIssueConfig).toBeDefined();
    expect(component.reportIssueConfig).toEqual(ARRIVALS_REPORT_ISSUE);
  });

  it('should test onRefresh method', () => {
    spyOn(component, 'fetchErrorConfig');
    spyOn(component.refreshAction, 'emit');
    component.onRefresh(0);
    component.onRefresh(2);
    expect(component.fetchErrorConfig).toHaveBeenCalled();
    expect(component.refreshAction.emit).toHaveBeenCalled();
  });

  it('should test onReportIssueClicked method', () => {
    spyOn(component, 'fetchErrorConfig');
    component.onReportIssueClicked();
    expect(component.fetchErrorConfig).toHaveBeenCalled();
  });

  it('should test transformData method', () => {
    const transformedString = component.transformData('testing title case');
    expect(transformedString).toEqual('Testing Title Case');
  });

  it('should test displayToolTip method', () => {
    component.characterCount = 1;
    const transformedString = component.displayToolTip('testing');
    expect(transformedString).toBeTruthy();
  });

  it('should test displayToolTip method if characterCount is greater', () => {
    component.characterCount = 2;
    const transformedString = component.displayToolTip('');
    expect(transformedString).toBeFalsy();
  });

  it('should test onGuestNameClick method checkin true case', () => {
    component.checkIn = true;
    spyOn(component, 'navigateToCheckInPage');
    spyOn(component, 'navigateToPreparePage');
    component.onGuestNameClick(MOCK_GUEST_DETAILS);
    expect(component.navigateToCheckInPage).toHaveBeenCalled();
    expect(component.navigateToPreparePage).not.toHaveBeenCalled();
  });

  it('should test onGuestNameClick method checkin false case', () => {
    component.checkIn = false;
    spyOn(component, 'navigateToPreparePage');
    spyOn(component, 'navigateToCheckInPage');
    component.onGuestNameClick(MOCK_GUEST_DETAILS);
    expect(component.navigateToPreparePage).toHaveBeenCalled();
    expect(component.navigateToCheckInPage).not.toHaveBeenCalled();
  });

  /*** TC205476 (F16494/US103120) ***/
  it('Should show \'Check In\' link in enabled mode for users with "Read & Write" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'W' }]);

    const checkLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.check-in[data-slnm-ihg^="ActionsCheckin"]'));

    expect(checkLinks.length).toEqual(2);
    checkLinks.forEach(element => {
      expect(element).toBeTruthy();
    });
  });

  /*** TC205477 (F16494/US103120) ***/
  it('Should show \'Guest Name\' link in enabled mode for users with "Read & Write" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'W' }]);

    const guestNameLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.guest-name[data-slnm-ihg^="GuestName"]'));

    expect(guestNameLinks.length).toEqual(2);
    guestNameLinks.forEach(element => {
      expect(element).toBeTruthy();
    });
  });

  /*** TC208183 (F16494/US103121) ***/
  it('Should show \'Check In\' link in enabled mode for users with "Read Only" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'R' }]);

    const checkLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.check-in[data-slnm-ihg^="ActionsCheckin"]'));

    expect(checkLinks.length).toEqual(2);
    checkLinks.forEach(element => {
      expect(element).toBeTruthy();
    });
  });

  /*** TC208184 (F16494/US103121) ***/
  it('Should show \'Guest Name\' link in enabled mode for users having "Read Only" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'R' }]);

    const guestNameLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.guest-name[data-slnm-ihg^="GuestName"]'));

    expect(guestNameLinks.length).toEqual(2);
    guestNameLinks.forEach(element => {
      expect(element).toBeTruthy();
    });
  });

  /*** TC208286 (F16494/US103122) ***/
  it('Should show \'Guest Name\' link in disabled mode for users having "No Access" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'X' }]);

    const guestNameLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.guest-name.noaccess[data-slnm-ihg^="GuestName"]'));

    expect(guestNameLinks.length).toEqual(2);
    guestNameLinks.forEach(element => {
      expect(element).toBeTruthy();
    });
  });

    /*** TC208287 (F16494/US103122) ***/
  it('Should not navigate to check-in page when user clicks the disabled \'Guest Name\' link, who is having "No Access" permission for the Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'X' }]);
    spyOn(component, 'onGuestNameClick');

    const guestNameLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.guest-name.noaccess[data-slnm-ihg^="GuestName"]'));

    guestNameLinks.forEach(item => {
      item.triggerEventHandler('click', null);
      expect(component.onGuestNameClick).not.toHaveBeenCalled();
    });
  });

    /*** TC208288 (F16494/US103122) ***/
  it('Should show tooltip when user mouse-over \'Guest Name\' link, who is having "No Access" permission for the Check In sub-capability and name of the guest is truncated', () => {
    GUEST_LIST_MOCK.data[0].guestName = 'Antoinette Cunningham';
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'X' }]);
    spyOn(component, 'onGuestNameClick');

    const guestNameLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.guest-name.noaccess[data-slnm-ihg^="GuestName"]'));
    guestNameLinks[0].triggerEventHandler('mouseenter', null);
    fixture.detectChanges();

    const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
    expect(tooltip).not.toBeNull();
    expect(tooltip.nativeElement.innerText).toEqual('Antoinette Cunningham');
  });

    /*** TC208289 (F16494/US103122) ***/
  it('Should not show any actions link for the users whose is having "No Access" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'X' }]);

    const checkLinks = fixture.debugElement.queryAll(By.css('kendo-grid-list table span.check-in[data-slnm-ihg^="ActionsCheckin"]'));

    expect(checkLinks.length).toEqual(0);
  });

    /*** TC208290 (F16494/US103122) ***/
  it('Should not show \'Actions\' column header for users whose is having "No Access" permission to Check In sub-capability', () => {
    recreate([{ name: 'Check In', tagName: 'check_in', type: 'C', access: 'X' }]);

    const actionHeader = fixture.debugElement.query(By.css('kendo-grid table thead tr th:last-child'));

    expect(actionHeader).toBeTruthy();
    expect(actionHeader.nativeElement.innerText).toEqual('');
  });

  function recreate(accessControlPermissions: AccessControlPermission[]) {
    fixture.destroy();
    translateService = TestBed.get(TranslateService);
    mockAccessControlService = TestBed.get(AccessControlService);
    mockAccessControlService.accessControlPermissions = accessControlPermissions;
    fixture = TestBed.createComponent(GuestListGridComponent);
    component = fixture.componentInstance;
    component.gridView = GUEST_LIST_MOCK;
    component.checkIn = true;
    fixture.detectChanges();
  }

  it('should track GA events on click of Check in', () => {
    const trackEvents = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component['getDisplayCheckIn'](MOCK_GUEST_DETAILS);
    fixture.detectChanges();
    expect(trackEvents).toBeDefined();
    expect(trackEvents).toHaveBeenCalledWith('Guest List - Arrivals', 'View', 'Selected');
  });

  it('should track GA events on click of View', () => {
    MOCK_GUEST_DETAILS.ineligibleReservation = false;
    const trackEvents = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    component['getDisplayCheckIn'](MOCK_GUEST_DETAILS);
    fixture.detectChanges();
    expect(trackEvents).toBeDefined();
    expect(trackEvents).toHaveBeenCalledWith( 'Guest List - Arrivals', 'Check In', 'Selected');
  });

  it('getDate should correctly format the date', () => {
    const testDate = component.getDateValue('2020-03-12');
    expect(testDate).toBe('12MAR2020');
  });

});
