import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HostListener } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { GridComponent, GridDataResult } from '@progress/kendo-angular-grid';
import { SortDescriptor, State } from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

import { GridSettings } from 'ihg-ng-common-kendo';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { AppConstants } from '@app/constants/app-constants';
import { GuestGridModel, GuestResponse } from '@app/modules/guests/models/guest-list.models';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { TOOL_TIP_DATA } from '@modules/prepare/constants/arrivals-constants';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { UtilityService } from '@services/utility/utility.service';
import { ReservationStatus } from '../../enums/guest-arrivals.enums';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';
import { ARRIVALS_GROUP_REPORT_ISSUE, ARRIVALS_REPORT_ISSUE } from '@app/constants/error-autofill-constants';
import { printGridItems } from '@app/modules/print-record/constants/print-grid.constant';
import { GuestListAccessCustomizer } from './guest-list-grid-access.customizer';

@Component({
  selector: 'app-guest-list-grid',
  templateUrl: './guest-list-grid.component.html',
  styleUrls: ['./guest-list-grid.component.scss']
})
export class GuestListGridComponent implements OnInit, OnChanges {

  @Input() isSearchResult: boolean;
  @Input() guestDataErrorEvent: Observable<EmitErrorModel>;
  @Input() guestData: GuestGridModel[] = [];
  @Input() searchDate: Date;
  @Input() localCurrentDate = moment().toDate();
  @Input() dateIndex: number;
  @Input() gridState: State;
  @Input() totalItems = 0;
  @Input() searchBy: string;
  @Input() selectedGroup: string;
  @Output() refreshAction = new EventEmitter();
  @Output() stateChange: EventEmitter<State> = new EventEmitter<State>();
  @Output() print = new EventEmitter<{ printAction: string, items: GuestGridModel[] }>();
  @ViewChild(GridComponent) grid: GridComponent;

  public guestNameCharLimit = 14;
  public toolTipList = TOOL_TIP_DATA.KEYMAP;
  public characterCount = 16;
  public noDataSymbol = '—';
  public reservationStatuses = ReservationStatus;
  public printMenuItems = printGridItems.slice(0);
  private applicableItems: GuestGridModel[] = [];

  checkIn: boolean;
  keyMap = GUEST_CONST.KEYMAP;
  isDesktopView: boolean;
  isTabletPortraitView: boolean;
  gridView: GridDataResult;
  gridSettings: GridSettings = null;
  activeTab: string;
  accessControlKeyMap = GUEST_CONST.ACCESS_CONTROL;
  accessCutomizer = GuestListAccessCustomizer;

  /** Table cell width section **/
  guestNameWidth: number;
  profileWidth: number;
  loyaltyWidth = 105;
  groupWidth: number;
  arrivalWidth: number;
  timeWidth: number;
  nightsWidth: number;
  departureWidth: number;
  roomWidth: number;
  statusWidth: number;
  actionsWidth: number;
  /** End width section **/

  gridPagerTranslations = {
    gridPagerInfo: {
      itemsText: this.keyMap.guests,
      noItemsText: this.keyMap.noGuestsToDisplay
    }
  };
  errorConfig: ServiceErrorComponentConfig = {
    noData: {
      header: this.keyMap.noGuestArrivals,
      message: this.keyMap.noGuestsArrivingForSelectedDate
    }
  };
  searchErrorConfig: ServiceErrorComponentConfig = {
    noData: {
      header: this.keyMap.noSearchResultsTitle,
      message: this.keyMap.noSearchResultsText
    }
  };
  reportIssueConfig: ReportIssueAutoFillObject;
  ga = GUEST_CONST.GA;

  constructor(
    public utility: UtilityService,
    private router: Router,
    private titleCasePipe: TitleCasePipe,
    private translate: TranslateService,
    private guestListService: GuestListService,
    private gaService: GoogleAnalyticsService
  ) {
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    if (window.outerWidth < 769) {
      this.isTabletPortraitView = true;
      this.isDesktopView = false;
      this.guestNameWidth = 168;
      this.profileWidth = 27;
      this.groupWidth = 128;
      this.arrivalWidth = 66;
      this.nightsWidth = 68;
      this.departureWidth = 0;
      this.roomWidth = 61;
      this.statusWidth = 63;
      this.actionsWidth = 92;
      this.timeWidth = 55;
    } else if (window.outerWidth < 1025) {
      this.isTabletPortraitView = false;
      this.isDesktopView = false;
      this.guestNameWidth = 190;
      this.profileWidth = 36;
      this.groupWidth = 173;
      this.arrivalWidth = 69;
      this.nightsWidth = 92;
      this.departureWidth = 98;
      this.roomWidth = 95;
      this.statusWidth = 68;
      this.actionsWidth = 106;
      this.timeWidth = 55;
    } else {
      this.isTabletPortraitView = false;
      this.isDesktopView = true;
      this.guestNameWidth = 170;
      this.profileWidth = 35;
      this.groupWidth = 155;
      this.arrivalWidth = 65;
      this.nightsWidth = 70;
      this.departureWidth = 140;
      this.roomWidth = 100;
      this.statusWidth = 150;
      this.actionsWidth = 110;
      this.timeWidth = 75;
    }
  }

  ngOnInit() {
    this.activeTab = this.ga.CATEGORY + this.ga.SUB_CATEGORY.ARRIVALSTAB;
    this.isDesktopView = (window.outerWidth < 1025);
    this.isTabletPortraitView = (window.outerWidth < 769);
    this.setGridSettings();
    this.getScreenSize();
  }

  dateComparision() {
    this.searchDate = this.searchDate || this.localCurrentDate;
    this.checkIn = moment(this.searchDate).isSame(this.localCurrentDate);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.guestData && changes.guestData.currentValue) {
      this.handleDataChange(changes.guestData.currentValue);
    }
  }

  private handleDataChange = (newData: GuestGridModel[]) => {
    this.dateComparision();
    this.setApplicableItems(newData);
  }

  setMemberProfile(loyaltyId: string) {
    this.gaService.trackEvent(this.activeTab, this.ga.GUEST_PROFILE, this.ga.SELECTED);
    this.guestListService.setMemberProfile(+loyaltyId);
  }

  private setApplicableItems(items: GuestGridModel[]): void {
    // renew when current items are empty and total count > 0
    if (!items.length && this.totalItems) {
      this.renew();
    } else {
      this.applicableItems = items;
      this.loadGridData();
    }
  }

  private loadGridData(): void {
    this.gridView = {
      data: this.applicableItems,
      total: this.totalItems
    };
  }

  private setGridSettings(): void {
    this.gridSettings = <GridSettings>{
      rows: {
        allOption: true,
        title: 'COM_LBL_ROWS',
        values: [10, 25, 50, 100],
        default: 10
      }
    };
  }

  pageSizeUpdate(val) {
    this.gaService.trackEvent(this.ga.CATEGORY, this.ga.GEAR_SELECTED, this.ga[`ROWS_PER_PAGE_${val}`]);
    // Reset page no and sorting while page size changes
    if (this.gridState) {
      this.gridState.sort = null;
      this.gridState.skip = 0;
    }
  }

  onDataStateChange(event): void {
    this.stateChange.emit(event);
  }

  onSortChange(sort: SortDescriptor[]): void {
    const fieldSortBy = this.translate.instant('LBL_GST_SORT_BY') + ' ' + this.ga[sort[0].field];
    this.gaService.trackEvent(this.ga.CATEGORY, this.ga.SORT, fieldSortBy);
  }

  formatDate(date: string) {
    return moment(date).format('DDMMMYYYY').toUpperCase();
  }

  fetchErrorConfig(): void {
    if (this.selectedGroup) {
      this.reportIssueConfig = ARRIVALS_GROUP_REPORT_ISSUE;
    } else {
      this.reportIssueConfig = ARRIVALS_REPORT_ISSUE;
    }
  }

  onRefresh(error) {
    switch (error) {
      case 0:
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.DATA_TIMEOUT, this.ga.TRY_AGAIN);
        break;
      case 2:
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.ERROR, this.ga.REFRESH);
        break;
    }
    this.fetchErrorConfig();
    this.refreshAction.emit();
  }

  onReportIssueClicked() {
    this.gaService.trackEvent(this.ga.CATEGORY, this.ga.REPORT_ISSUE, this.ga.SELECTED);
    this.fetchErrorConfig();
  }

  navigateToCheckInPage(dataItem: GuestResponse): void {
    this.getDisplayCheckIn(dataItem);
    this.guestListService.userDataSource.next(dataItem);
    this.router.navigate(['/check-in']);
  }

  getDisplayCheckIn(dataItem) {
    if(dataItem.ineligibleReservation ||
      dataItem.prepStatus === this.reservationStatuses.checkedIn ||
      dataItem.prepStatus === this.reservationStatuses.inHouse) {

      this.trackGAEvents(this.activeTab, this.ga.VIEW, this.ga.SELECTED);
    } else {
      this.trackGAEvents(this.activeTab, this.ga.CHECK_IN, this.ga.SELECTED);
    }
  }

  trackGAEvents(activeTab, action, label): void {
    this.gaService.trackEvent(activeTab, action, label);
  }

  navigateToPreparePage(dataItem: GuestGridModel) {
    this.gaService.trackEvent(this.activeTab, this.ga.PREPARE, this.ga.SELECTED);
    this.guestListService.emitGuestTabEvent({
      tabFlag: true,
      dateIndex: this.dateIndex,
      selectedData: dataItem,
      searchedDate: this.searchDate
    });
  }

  public transformData(dataParam: string): string {
    return this.titleCasePipe.transform(dataParam);
  }

  public displayToolTip(data: string): boolean {
    return data.length > this.characterCount;
  }

  onGuestNameClick(dataItem: GuestGridModel): void {
    this.gaService.trackEvent(this.activeTab, this.ga.GUEST_NAME, this.ga.SELECTED);
    if (this.checkIn) {
      this.navigateToCheckInPage(dataItem);
    } else {
      this.navigateToPreparePage(dataItem);
    }
  }

  private renew() {
    this.gridState.skip = this.gridState.skip - this.gridState.take;
    this.stateChange.emit(this.gridState);
  }

  public getDateValue(date: string): string {
    return moment(date).format(AppConstants.VW_DT_FORMAT).toUpperCase();
  }

  public handlePrint(printAction: string): void {
    this.print.emit({
      printAction,
      items: this.gridView.data
    });
  }
}
