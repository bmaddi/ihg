import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';

import { ConfirmationModalService } from 'ihg-ng-common-components';
import { EnvironmentService } from 'ihg-ng-common-core';
import { ConcertoGridSettingsService } from 'ihg-ng-common-kendo';

import { GuestListComponent } from './guest-list.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { mockEnvs } from '@app/constants/app-test-constants';

describe('GuestListComponent', () => {
  let component: GuestListComponent;
  let fixture: ComponentFixture<GuestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CommonTestModule],
      declarations: [GuestListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [ConfirmationModalService, ConcertoGridSettingsService, EnvironmentService]
    })
      .compileComponents();
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the All Guests tab as the 4th tab and proper child content', () => {
    const tabs: DebugElement = fixture.debugElement.query(By.css('[role="tablist"]'));
    expect(tabs).not.toBeNull();
    const expectedTab: HTMLElement = tabs.childNodes[4].nativeNode;
    expect(expectedTab.children[0].id).toEqual('allGuests');
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="AllGuestsView-SID"]'))).not.toBeNull();
  });
});
