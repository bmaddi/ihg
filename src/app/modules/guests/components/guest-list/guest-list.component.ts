import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';

import { SessionStorageService, GoogleAnalyticsService, useDefault } from 'ihg-ng-common-core';

import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { GuestArrivalParams, GuestInHouseParams } from '@app/modules/guests/models/guest-list.models';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { HelpService } from '@modules/shared/components/help/help.service';
import { ConcertoGridSettingsService } from 'ihg-ng-common-kendo';
import { PRINT_ERROR_CONTENT } from '@modules/prepare/constants/prepare-error-constants';
import { PREPARE_REPORT_ISSUE_AUTOFILL, ARRIVAL_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';

@Component({
  selector: 'app-guest-list',
  templateUrl: './guest-list.component.html',
  styleUrls: ['./guest-list.component.scss']
})

export class GuestListComponent implements OnInit, OnDestroy {
  private guestArrival$: Subscription = new Subscription();

  keyMap = GUEST_CONST.KEYMAP;
  selectedTab: string;
  arrivalParams: GuestArrivalParams;
  inHouseParams: GuestInHouseParams;
  tabs = GUEST_CONST.GUEST_LIST_TABS;
  ga = GUEST_CONST.GA;
  printError = PRINT_ERROR_CONTENT;
  prepareReportIssue = PREPARE_REPORT_ISSUE_AUTOFILL.genericError;
  arrivalReportIssue = ARRIVAL_REPORT_ISSUE_AUTOFILL.genericError;

  constructor(public prepareArrivalsService: ArrivalsService,
              private storageService: SessionStorageService,
              public guestListService: GuestListService,
              private helpService: HelpService,
              private settingsService: ConcertoGridSettingsService,
              private gaService: GoogleAnalyticsService,
             private appCommonService: AppCommonService) {
  }

  ngOnInit() {
    this.storeArrivalParams();
    this.storeInHouseParams();
    this.initSelectedTab();
    this.guestTabChange();
  }

  guestTabChange() {
    this.guestArrival$.add(this.guestListService.getGuestTabEvent().subscribe((data) => {
      if (data.tabFlag) {
        this.selectedTab = this.tabs[0].tabName;
      }
    }));
  }

  handleTabChange(event: NgbTabChangeEvent) {
    event.preventDefault();
    this.checkUnsavedTasks(this.changeTab.bind(this, event));
  }

  private checkUnsavedTasks(callbackFn: Function): void {
    this.prepareArrivalsService.handleUnsavedTasks(callbackFn);
  }

  private changeTab(event: NgbTabChangeEvent): void {
    this.selectedTab = event.nextId;
    this.gaService.trackEvent(this.ga.CATEGORY + this.ga.SUB_CATEGORY[this.selectedTab.toUpperCase()],
      this.ga.TAB_SELECTED, this.ga.SELECTED);
    this.triggerHelpTagUpdate(event.nextId);
    this.settingsService.setIsSettingsPopoverOpen(false);
    this.appCommonService.flushReservationNumber();
  }

  private triggerHelpTagUpdate(tabId: string): void {
    const tagName = this.tabs.find(tab => tab.tabName === tabId).helpTagName;
    this.helpService.setTabStateChange(tagName);
  }

  private storeArrivalParams(): void {
    this.arrivalParams = this.storageService.getSessionStorage('guestArrivalParams');
    this.storageService.setSessionStorage('guestArrivalParams', null);
  }

  private storeInHouseParams(): void {
    this.inHouseParams = useDefault(this.storageService.getSessionStorage('guestInHouseParams'), {});
    this.storageService.setSessionStorage('guestInHouseParams', null);
  }

  private isInHouseParamsValid() {
    return Object.keys(this.inHouseParams).length > 0;
  }

  private initSelectedTab(): void {
    this.selectedTab = this.arrivalParams ? this.tabs[0].tabName : this.isInHouseParamsValid() ? this.tabs[2].tabName : this.tabs[1].tabName;
    this.triggerHelpTagUpdate(this.selectedTab);
  }

  ngOnDestroy() {
    this.storageService.setSessionStorage('guestArrivalParams', null);
    this.storageService.setSessionStorage('guestInHouseParams', null);
    this.guestArrival$.unsubscribe();
  }
}
