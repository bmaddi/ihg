import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileFlagObservationComponent } from './guest-profile-flag-observation.component';

xdescribe('GuestProfileFlagObservationComponent', () => {
  let component: GuestProfileFlagObservationComponent;
  let fixture: ComponentFixture<GuestProfileFlagObservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileFlagObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileFlagObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
