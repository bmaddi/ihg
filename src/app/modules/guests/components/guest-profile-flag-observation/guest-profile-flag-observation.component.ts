import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GUEST_CONST, observationReportIssueConfig } from '@app/modules/guests/guests.constants';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';

@Component({
  selector: 'app-guest-profile-flag-observation',
  templateUrl: './guest-profile-flag-observation.component.html',
  styleUrls: ['./guest-profile-flag-observation.component.scss']
})
export class GuestProfileFlagObservationComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  @Input() public commentId: string;
  @Input() public memberId: string;
  @Input() public firstName: string
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  keyMap = GUEST_CONST.KEYMAP;
  private flagObsSubscription$: Subscription;
  toastTranslationConfig: Partial<ErrorToastMessageTranslations>;
  reportIssueAutoFill = observationReportIssueConfig;

  constructor( private guestService: GuestListService,
    private activeModal: NgbActiveModal
  ) { super(); }

  ngOnInit() {
    this.initToastErrorConfig();
  }

  onClose() {
    this.activeModal.close();
  }

  onCancel() {
    this.activeModal.dismiss();
  }

  onSave() {
    this.flagObsSubscription$ = this.guestService.flagObservation(this.memberId, this.commentId)
      .subscribe(() => {
        this.activeModal.close();
        this.passEntry.emit();
      }, error => {
        this.processHttpErrors(error);
    });
  }

  onRetry() {
    this.onSave();
  }

  onReportAnIssue() {
    this.activeModal.dismiss();
  }

  private initToastErrorConfig() {
    this.toastTranslationConfig = {
      timeoutError: {
        message: this.keyMap.timeoutToast,
        detailMessage: this.keyMap.unabletoAddFlagTryAgain,
        detailMessageMoreErrors: this.keyMap.unabletoAddFlagForTryAgain
      },
      generalError: {
        message: this.keyMap.errorToast,
        detailMessage: this.keyMap.unabletoAddFlagTryAgain,
        detailMessageMoreErrors: this.keyMap.unabletoAddFlagForTryAgain
      }
    };
  }

  ngOnDestroy() {
    if (this.flagObsSubscription$) {
      this.flagObsSubscription$.unsubscribe();
    }
  }

}
