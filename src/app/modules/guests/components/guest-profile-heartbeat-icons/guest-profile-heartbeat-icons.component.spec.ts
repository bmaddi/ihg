import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileHeartbeatIconsComponent } from './guest-profile-heartbeat-icons.component';

xdescribe('GuestProfileHeartbeatIconsComponent', () => {
  let component: GuestProfileHeartbeatIconsComponent;
  let fixture: ComponentFixture<GuestProfileHeartbeatIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileHeartbeatIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileHeartbeatIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
