import { Component, Input, OnInit } from '@angular/core';

import { GuestHeartBeatSurvey } from '../../models/guest-profile-heartbeat.model';
import { GUEST_CONST } from '../../guests.constants';
import { HeartBeatCommentsService } from '../../../heart-beat-comments/services/heart-beat-comments.service';
import { shouldNotShowHeartBeat } from '../../constants/guest-profile-heartbeat.constants';
import { AppConstants } from '@app/constants/app-constants';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

@Component({
  selector: 'app-guest-profile-heartbeat-icons',
  templateUrl: './guest-profile-heartbeat-icons.component.html',
  styleUrls: ['./guest-profile-heartbeat-icons.component.scss']
})
export class GuestProfileHeartbeatIconsComponent implements OnInit {
  @Input() heartBeatSurvey: GuestHeartBeatSurvey;
  datePipeDisplayFormat = AppConstants.PIPE_DD_MM_FORMAT;
  keyMap = GUEST_CONST.KEYMAP;

  constructor(private heartBeatCommentsService: HeartBeatCommentsService, private gaService: GoogleAnalyticsService) {
  }

  get validHeartBeatData(): boolean {
    return !shouldNotShowHeartBeat(this.heartBeatSurvey);
  }

  ngOnInit() {
  }

  isValidLowGuestLoveScore(val: string): boolean {
    return (val === 'R' || val === 'R*');
  }

  showIcon(val: string, extraVal?: string): boolean {
    return !!(val || extraVal);
  }

  openCommentsModal() {
    this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.HEARTBEAT_CARD, GUEST_CONST.GA.HB_PAST_COMMENTS);
    this.heartBeatCommentsService.openHeartBeatCommentsModal(
      this.heartBeatSurvey.likeComment,
      this.heartBeatSurvey.makeStayBetterComment,
      this.heartBeatSurvey.surveyDate);
  }
}
