import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileHeartbeatComponent } from './guest-profile-heartbeat.component';

xdescribe('GuestProfileHeartbeatComponent', () => {
  let component: GuestProfileHeartbeatComponent;
  let fixture: ComponentFixture<GuestProfileHeartbeatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileHeartbeatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileHeartbeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
