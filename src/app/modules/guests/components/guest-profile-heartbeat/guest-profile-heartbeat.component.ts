import { Component, OnInit } from '@angular/core';
import { isNil } from 'lodash';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import {GoogleAnalyticsService} from 'ihg-ng-common-core'

import { GUEST_CONST } from '../../guests.constants';
import { GuestHeartbeatService } from '../../services/guest-heartbeat.service';
import { GuestListService } from '../../services/guest-list.service';
import { GuestHeartBeat, GuestHeartBeatSurvey } from '../../models/guest-profile-heartbeat.model';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ServiceErrorComponentConfig } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { ERROR_CARD_TYPE } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { shouldNotShowHeartBeat } from '../../constants/guest-profile-heartbeat.constants';
import { GUEST_PROFILE_HEARTBEAT_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

@Component({
  selector: 'app-guest-profile-heartbeat',
  templateUrl: './guest-profile-heartbeat.component.html',
  styleUrls: ['./guest-profile-heartbeat.component.scss']
})
export class GuestProfileHeartbeatComponent extends AppErrorBaseComponent implements OnInit {
  guestHeartBeatRes: GuestHeartBeat;
  keyMap = GUEST_CONST.KEYMAP;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  hbErrorConfig: ServiceErrorComponentConfig = {
    noData: {
      header: 'LBL_ERR_NO_DATA_AVAILABLE',
      message: 'LBL_HB_NO_FEEDBACK',
      showHorizontalLine: true
    }
  };
  public reportIssueConfig: ReportIssueAutoFillObject;

  constructor(private guestHeartBeat: GuestHeartbeatService,
              private guestService: GuestListService,
              private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.reportIssueConfig = GUEST_PROFILE_HEARTBEAT_AUTOFILL;
    this.getHeartBeatForProfile();
  }

  private getHeartBeatForProfile() {
    const memberId = this.guestService.getCurrentMemberId().toString();
    this.guestHeartBeat.getGuestProfileHeartbeat({memberId})
      .subscribe(
        res => this.handleHeartBeatResponse(res),
        err => this.handleHeartBeatError(err)
      );
  }

  private handleHeartBeatResponse(res: GuestHeartBeat) {
    this.guestHeartBeatRes = res;
    this.checkIfAnyApiErrors(res, this.hasNoHeartBeatData);
  }

  private handleHeartBeatError(err) {
    this.processHttpErrors(err);
  }

  private hasNoHeartBeatData(res: GuestHeartBeat): boolean {
    return isNil(res.heartBeatMyHotel) && isNil(res.heartBeatOtherHotel)
      ? true : shouldNotShowHeartBeat(res.heartBeatMyHotel) && shouldNotShowHeartBeat(res.heartBeatOtherHotel);
  }

  isValidHeartBeatOtherHotel(res: GuestHeartBeatSurvey): boolean {
    return !shouldNotShowHeartBeat(res);
  }

  refreshHb(action: any) {
    switch (action) {
      case 0: {
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.HEARTBEAT_CARD, GUEST_CONST.GA.TRY_AGAIN);
        break;
      }
      case 2: {
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.HEARTBEAT_CARD, GUEST_CONST.GA.REFRESH);
        break;
      }
    }
    this.getHeartBeatForProfile();
  }

  reportIssueClicked() {
    this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.HEARTBEAT_CARD, GUEST_CONST.GA.REPORT_ISSUE);
  }

}
