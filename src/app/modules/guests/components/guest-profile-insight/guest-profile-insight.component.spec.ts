import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileInsightComponent } from './guest-profile-insight.component';

xdescribe('GuestProfileInsightComponent', () => {
  let component: GuestProfileInsightComponent;
  let fixture: ComponentFixture<GuestProfileInsightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileInsightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileInsightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
