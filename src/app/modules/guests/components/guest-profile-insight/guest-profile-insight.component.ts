import {Component, Input} from '@angular/core';
import {GuestInsightModel} from '@modules/guests/models/guest-insight.models';
import {GUEST_CONST} from '@modules/guests/guests.constants';

@Component({
  selector: 'app-guest-profile-insight',
  templateUrl: './guest-profile-insight.component.html',
  styleUrls: ['./guest-profile-insight.component.scss']
})
export class GuestProfileInsightComponent {
  keyMap = GUEST_CONST.KEYMAP;
  @Input() insight: GuestInsightModel;
}
