import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileInsightsComponent } from './guest-profile-insights.component';

xdescribe('GuestProfileInsightsComponent', () => {
  let component: GuestProfileInsightsComponent;
  let fixture: ComponentFixture<GuestProfileInsightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileInsightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileInsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
