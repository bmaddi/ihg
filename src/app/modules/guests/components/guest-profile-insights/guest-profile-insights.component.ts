import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import {GUEST_CONST} from '@modules/guests/guests.constants';
import {GuestInsightModel} from '@modules/guests/models/guest-insight.models';
import {GuestInsightsService} from '@modules/guests/services/guest-insights.service';
import { GuestProfileModel } from '@app/modules/guests/models/guest-profile.model';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ERROR_CARD_TYPE } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { GUEST_INSIGHT } from '@modules/guests/guests.constants';
import { GUEST_PROFILE_INSITE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

@Component({
  selector: 'app-guest-profile-insights',
  templateUrl: './guest-profile-insights.component.html',
  styleUrls: ['./guest-profile-insights.component.scss']
})
export class GuestProfileInsightsComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @Input() guest: GuestProfileModel;

  keyMap = GUEST_CONST.KEYMAP;
  insights: GuestInsightModel[] = [];
  activeInsightIndex = 0;
  errorConfig: ServiceErrorComponentConfig;
  initialized = false;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  private insightsSubscription$: Subscription;
  public reportIssueConfig: ReportIssueAutoFillObject;

  constructor(private insightService: GuestInsightsService,
    private translate: TranslateService, private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.reportIssueConfig = GUEST_PROFILE_INSITE_AUTOFILL;
    this.fetchInightsData();
    this.initErrorConfig();
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      noData: {
        header: this.keyMap.noDataAvailable,
        message: this.keyMap.noInsightsToDisplay,
        showHorizontalLine: true }
    };
  }

  private fetchInightsData() {
    this.insightsSubscription$ = this.insightService.getInsights(this.guest)
      .subscribe(this.handleInsights,
    error => this.processHttpErrors(error));
  }

  public refresh(action: any) {
    switch (action) {
      case 0: {
        this.gaService.trackEvent(GUEST_INSIGHT.GA.CATEGORY, GUEST_INSIGHT.GA.ACTION, GUEST_INSIGHT.GA.TRY_AGAIN);
        break;
      }
      case 2: {
        this.gaService.trackEvent(GUEST_INSIGHT.GA.CATEGORY, GUEST_INSIGHT.GA.ACTION, GUEST_INSIGHT.GA.REFRESH);
        break;
      }
    }
    this.fetchInightsData();
  }

  private handleInsights = (insights: GuestInsightModel[]) => {
    this.checkIfAnyApiErrors(insights, insightsData => insightsData.length === 0);
    this.initialized = true;
    this.insights = insights;
  }

  reportIssueClicked(event: any) {
    this.gaService.trackEvent(GUEST_INSIGHT.GA.CATEGORY, GUEST_INSIGHT.GA.ACTION, GUEST_INSIGHT.GA.REPORT_ISSUE);
  }

  goToInsight(insightIndex: number) {
    this.gaService.trackEvent(GUEST_INSIGHT.GA.CATEGORY, GUEST_INSIGHT.GA.ACTION, GUEST_INSIGHT.GA.LABEL_VIEW_CAROUSEL);
    this.activeInsightIndex = insightIndex;
  }

  previousInsight() {
    this.gaService.trackEvent(GUEST_INSIGHT.GA.CATEGORY, GUEST_INSIGHT.GA.ACTION, GUEST_INSIGHT.GA.LABEL_VIEW_CARET);
    if (this.activeInsightIndex === 0) {
      this.activeInsightIndex = this.insights.length - 1;
    } else {
      this.activeInsightIndex--;
    }
  }

  nextInsight() {
    this.gaService.trackEvent(GUEST_INSIGHT.GA.CATEGORY, GUEST_INSIGHT.GA.ACTION, GUEST_INSIGHT.GA.LABEL_VIEW_CARET);
    if (this.activeInsightIndex === (this.insights.length - 1)) {
      this.activeInsightIndex = 0;
    } else {
      this.activeInsightIndex++;
    }
  }

  ngOnDestroy() {
    if (this.insightsSubscription$) {
      this.insightsSubscription$.unsubscribe();
    }
  }

}
