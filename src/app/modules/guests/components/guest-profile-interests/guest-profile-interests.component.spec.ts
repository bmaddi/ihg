import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GuestProfileInterestsComponent } from './guest-profile-interests.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CommonTestModule} from '@modules/common-test/common-test.module';
import {setTestEnvironment} from '@modules/common-test/functions/common-test.function';

describe('GuestProfileInterestsComponent', () => {
  let component: GuestProfileInterestsComponent;
  let fixture: ComponentFixture<GuestProfileInterestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuestProfileInterestsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonTestModule],
      providers: [TranslateService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(GuestProfileInterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
