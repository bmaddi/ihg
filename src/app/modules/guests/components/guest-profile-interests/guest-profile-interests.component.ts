import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GUEST_CONST } from '../../guests.constants';
import { GuestInterestsService } from '@app/modules/guests/services/guest-interests.service';
import { GuestInterestsModel, ValuePreferenceResponse } from '@app/modules/guests/models/guest-interest.models';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { GUEST_PROFILE_PREFERENCES_OPTIONS } from '../../guest-profile-preferences-options';
import { UtilityService } from '@app/services/utility/utility.service';

@Component({
  selector: 'app-guest-profile-interests',
  templateUrl: './guest-profile-interests.component.html',
  styleUrls: ['./guest-profile-interests.component.scss']
})
export class GuestProfileInterestsComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  constructor(private translate: TranslateService,
    private interestsService: GuestInterestsService,
    private guestListService: GuestListService,
    private utilityService: UtilityService,
    private gaService: GoogleAnalyticsService
  ) { super(); }

  public initialized = false;
  public hasErrors = true;
  public interests: string[] = [];
  private guestInterests$: Subscription = new Subscription();
  keyMap = GUEST_CONST.KEYMAP;
  maxInterests = 3;
  showAllInterests = false;
  errorConfig: ServiceErrorComponentConfig;
  errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  private ga = GUEST_CONST.GA;
  private action ;    
  private label ; 
  ngOnInit() {
    this.initErrorConfig();
    this.reloadService();
   this.action = this.utilityService.getGAString(this.ga.ROOM_PREFERENCE, this.ga.GUEST_INTERESTS, ' / ');
  }

  private getInterestsData() {
    const guestMemberId = this.guestListService.getCurrentMemberId();
    this.interestsService.getInterests(guestMemberId + '').subscribe(
      this.handleInterests,
      error => this.handleErrors(error));
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      timeout: { message: this.keyMap.errorRetData, showRefreshBtn: true },
      general: { message: this.keyMap.errorRetData, showRefreshBtn: true },
      noData: { message: this.keyMap.none }
    };
  }

  private handleInterests = (interests: GuestInterestsModel[]) => {
    this.initialized = true;
    this.hasErrors = false;
    this.replaceLabel(interests);
    this.interests = interests.map(model => model.displayValue);
    if(this.guestListService.getPreferenceResponseData() !== null) {
      const destinations = this.guestListService.getPreferenceResponseData().destinations;
      this.interestsService.updateSections(destinations as ValuePreferenceResponse[], GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.destinations)
      .map((model) => {
        model.selectedValue === "true" ? this.interests.push(model.description) : null
      })
    }
  }

  private replaceLabel(interests: GuestInterestsModel[]) {
    interests.map((model) => {
      if(GUEST_PROFILE_PREFERENCES_OPTIONS.replaceLabel[model.displayValue] !== undefined) {
        model.displayValue = GUEST_PROFILE_PREFERENCES_OPTIONS.replaceLabel[model.displayValue]['replace']
      }
    })
  }

  private handleErrors = (error) => {
    this.guestListService.guestInterestsFlag.next(false);
    this.processHttpErrors(error);
  }

  public refresh() {
    this.label = this.utilityService.getGAString(this.ga.GUEST_INTERESTS, this.ga.REFRESH, ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);
    this.getInterestsData();
  }

  public reloadService() {
    this.guestInterests$.add(this.guestListService.refreshInterest.subscribe(() => {
      this.getInterestsData();
    }))
  }

  public enableShowAllInterest(){
    this.label = this.utilityService.getGAString(this.ga.GUEST_INTERESTS, this.showAllInterests ? this.ga.SHOW_LESS : this.ga.SHOW_MORE, ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);
    this.showAllInterests = !this.showAllInterests;
  }

  onReportIssueClicked() {
    this.label = this.utilityService.getGAString(this.ga.GUEST_INTERESTS, this.ga.REPORT_ISSUE, ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);
  }

  ngOnDestroy() {
    this.guestInterests$.unsubscribe();
  }
}
