import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileLoyaltyComponent } from './guest-profile-loyalty.component';

xdescribe('GuestProfileLoyaltyComponent', () => {
  let component: GuestProfileLoyaltyComponent;
  let fixture: ComponentFixture<GuestProfileLoyaltyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileLoyaltyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileLoyaltyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
