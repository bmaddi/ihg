import { Component, OnInit, Input } from '@angular/core';
import { GuestProfileModel } from '@app/modules/guests/models/guest-profile.model';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { BadgeIconNames } from '@app/constants/badge-icon-names';

@Component({
  selector: 'app-guest-profile-loyalty',
  templateUrl: './guest-profile-loyalty.component.html',
  styleUrls: ['./guest-profile-loyalty.component.scss']
})
export class GuestProfileLoyaltyComponent implements OnInit {

  @Input() guest: GuestProfileModel;
  keyMap = GUEST_CONST.KEYMAP;
  badgeIconNames = BadgeIconNames;

  constructor(
  ) { }

  ngOnInit() {
  }

}
