import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileObservationListComponent } from './guest-profile-observation-list.component';

xdescribe('GuestProfileObservationListComponent', () => {
  let component: GuestProfileObservationListComponent;
  let fixture: ComponentFixture<GuestProfileObservationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileObservationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileObservationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
