import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import {
  GuestProfileModel, GuestProfileObservationModel,
  ObservationListType, ObservationSortType, ValueLabelModel
} from '@app/modules/guests/models/guest-profile.model';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { AddObservationModalComponent } from '@app/modules/guests/components/add-obs-modal/add-obs-modal.component';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { GUEST_PROFILE_OBSERVATIONS_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';
import { UtilityService } from '@app/services/utility/utility.service';

@Component({
  selector: 'app-guest-profile-observation-list',
  templateUrl: './guest-profile-observation-list.component.html',
  styleUrls: ['./guest-profile-observation-list.component.scss']
})
export class GuestProfileObservationListComponent extends AppErrorBaseComponent implements OnInit {

  @Input() guest: GuestProfileModel;
  keyMap = GUEST_CONST.KEYMAP;
  listDropdownLabel: string;
  sortDropdownLabel: string;
  filterByDropdownLabel: string;
  observationsPerLoad = 5;
  observationsShown: number;
  observationsList: GuestProfileObservationModel[];
  observationsCount = 0;
  private observationListType: ObservationListType;
  private sortType: ObservationSortType;
  private filterByCategory: string;
  filterByOptions: ValueLabelModel[];
  errorConfig: ServiceErrorComponentConfig;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  hasError = false;
  public reportIssueConfig: ReportIssueAutoFillObject;
  private ga = GUEST_CONST.GA;
  private label;
  private action;
  constructor(
    private guestService: GuestListService,
    private modalService: NgbModal,
    private translate: TranslateService,
    private utilityService: UtilityService,
    private gaService: GoogleAnalyticsService
  ) {
    super();
  }

  ngOnInit() {
    this.reportIssueConfig = GUEST_PROFILE_OBSERVATIONS_AUTOFILL;
    this.filterByOptions = [
      {
        value: GuestListService.allCategoriesFilterValue,
        label: this.translate.instant(this.keyMap.observationFilterByAll)
      },
      ...this.guestService.getCategories()
    ];
    this.setHotelsList('ALL', 'MOST_RECENT', 'ALL');
    super.init();
    this.initErrorConfig();
    this.action = this.ga.OBSERVATION_CARD;
  }

  setHotelsList(newObservationListType: ObservationListType, newSortType: ObservationSortType, filterByCategory: string) {
    this.observationListType = newObservationListType;
    this.sortType = newSortType;
    switch (newObservationListType) {
      case 'ALL': this.listDropdownLabel = this.keyMap.observationAllHotels; break;
      case 'OTHER': this.listDropdownLabel = this.keyMap.observationOtherHotels; break;
      case 'MY': this.listDropdownLabel = this.keyMap.observationMyHotel; break;
    }
    switch (newSortType) {
      case 'MOST_RECENT': this.sortDropdownLabel = this.keyMap.observationMostRecent; break;
      case 'MOST_HELPFUL': this.sortDropdownLabel = this.keyMap.observationMostHelpful; break;
      case 'HOTEL_CODE': this.sortDropdownLabel = this.keyMap.observationHotelCode; break;
    }
    this.filterByCategory = filterByCategory;
    this.filterByDropdownLabel = this.getFilterByDropdownLabel(filterByCategory);
    this.setObservationsList([]);
    this.refreshObservations();
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      noData: {
        header: this.keyMap.noDataAvailable,
        message: this.keyMap.noObservationsToDisplay,
        showHorizontalLine: true
      }
    };
  }

  private getFilterByDropdownLabel(filterByCategory: string) {
    const idx = this.filterByOptions.findIndex(item => item.value === filterByCategory);
    return idx === -1 ? this.translate.instant(this.keyMap.observationFilterByAll) : this.filterByOptions[idx].label;
  }

  setList(newObservationListType: ObservationListType) {
    const finalSortType = newObservationListType === 'MY' && this.sortType === 'HOTEL_CODE' ? 'MOST_RECENT' : this.sortType;
    this.setHotelsList(newObservationListType, finalSortType, this.filterByCategory);
    this.label = this.utilityService.getGAString(this.ga.VIEW, this.translate.instant(this.listDropdownLabel), ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action, this.label);

  }

  setSortBy(newSortType: ObservationSortType) {
    this.setHotelsList(this.observationListType, newSortType, this.filterByCategory);
    this.label = this.utilityService.getGAString(this.ga.SORT_BY, this.translate.instant(this.sortDropdownLabel), ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action, this.label);
  }

  setFilterBy(newFilterBy: string) {
    this.setHotelsList(this.observationListType, this.sortType, newFilterBy);
    this.label = this.utilityService.getGAString(this.ga.FILTER_BY, this.translate.instant(this.filterByDropdownLabel), ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action, this.label);
  }

  private refreshObservations() {
    const subject = new Subject();
    this.guestService.getObservations(this.guest.guestLoyalty.loyaltyID, this.observationListType, this.sortType, this.filterByCategory)
      .subscribe(newObservationList => {
        if (!this.checkIfAnyApiErrors(newObservationList, newList => !newList || newList.length === 0)) {
          this.setObservationsList(newObservationList);
          subject.next();
        }
        this.hasError = false;
      }, error => {
        this.hasError = true;
        this.processHttpErrors(error);
      }
      );
    return subject;
  }

  onAddObservation() {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action, this.ga.ADD_OBSERVATION);
    const modal = this.modalService.open(AddObservationModalComponent,
      {
        windowClass: 'guest-new-observation-modal',
        backdrop: 'static',
      });
    const modalComponent = modal.componentInstance as AddObservationModalComponent;
    modalComponent.guest = this.guest;
    modal.result.then( /*succeded*/() => {
      this.refreshObservations()
        .subscribe(() => { this.guestService.showCreateObservationSuccess(this.guest.firstName); });
    }, /* dismissed */() => { });
  }

  private setObservationsList = (newList: GuestProfileObservationModel[]) => {
    this.observationsList = newList ? newList : [];
    this.observationsShown = this.observationsPerLoad;
    this.observationsCount = this.observationsList.length ? this.observationsList[0].observationsCount : 0;
  }

  loadMore() {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.LOAD_MORE);
    this.observationsShown = this.observationsShown + this.observationsPerLoad;
  }

  onReportIssueClicked() {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.REPORT_ISSUE);
  }

  refresh(error) {
    switch (error) {
      case 0:
        this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action, this.ga.TRY_AGAIN);
        break;
      case 2:
        this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action, this.ga.REFRESH);
        break;
    }
    this.refreshObservations();
  }
}
