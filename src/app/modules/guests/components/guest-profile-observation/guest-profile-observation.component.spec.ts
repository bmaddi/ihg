import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileObservationComponent } from './guest-profile-observation.component';

xdescribe('GuestProfileObservationComponent', () => {
  let component: GuestProfileObservationComponent;
  let fixture: ComponentFixture<GuestProfileObservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
