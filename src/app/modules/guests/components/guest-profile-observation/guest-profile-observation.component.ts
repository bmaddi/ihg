import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { GuestProfileModel, GuestProfileObservationModel } from '@app/modules/guests/models/guest-profile.model';
import { GUEST_CONST, observationReportIssueConfig } from '@app/modules/guests/guests.constants';
import { BadgeIconNames } from '@app/constants/badge-icon-names';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GuestProfileFlagObservationComponent } from '@app/modules/guests/components/guest-profile-flag-observation/guest-profile-flag-observation.component';

@Component({
  selector: 'app-guest-profile-observation',
  templateUrl: './guest-profile-observation.component.html',
  styleUrls: ['./guest-profile-observation.component.scss']
})
export class GuestProfileObservationComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  @Input() observation: GuestProfileObservationModel;
  @Input() memberId: string;
  @Input() showHotelName: boolean;
  @Input() firstName: string;
  private errorServiceSubscription$: Subscription;
  keyMap = GUEST_CONST.KEYMAP;
  category: string;
  toastTranslationConfig: Partial<ErrorToastMessageTranslations>;
  markHelpfulDisabled = false;
  flagDisabled: boolean;
  flagTooltip: string = "";
  reportIssueAutoFill = observationReportIssueConfig;
  private ga = GUEST_CONST.GA;
  private action;
  constructor(
    private guestService: GuestListService,
    private translate: TranslateService,
    private modalService: NgbModal,
    private gaService: GoogleAnalyticsService
  ) {
    super();
  }

  ngOnInit() {
    this.getCategory();
    this.initToastErrorConfig();
    super.init();
    this.action = this.ga.OBSERVATION_CARD;
  }

  onMarkHelpful(commentId: string) {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.MARKED_AS_HELPFUL);
    this.errorServiceSubscription$ = this.guestService.markObservationHelpful(this.memberId, commentId)
      .subscribe((data) => {
        if (!this.checkIfAnyApiErrors(data)) {
          this.markHelpfulDisabled = true;
          this.observation.mostHelpfulCount = (this.observation.mostHelpfulCount || 0) + 1;
        }
      }, error => {
        this.processHttpErrors(error);
      });
  }
  getCategory() {
    this.category = this.observation.category === 'General Info' ? this.translate.instant(this.keyMap.categoryGeneralInfo)
      : this.observation.category === 'Food and Beverage' ? this.translate.instant(this.keyMap.categoryFoodAndBeverage)
        : this.observation.category === 'Hotel Amenities' ? this.translate.instant(this.keyMap.categoryHotelAmenities)
          : this.observation.category === 'Room and Bed' ? this.translate.instant(this.keyMap.categoryRoomAndBed)
            : '';
    this.flagDisabled = this.observation.observationFlag;
  }

  private initToastErrorConfig() {
    this.toastTranslationConfig = {
      timeoutError: {
        message: this.keyMap.timeoutToastColon,
        detailMessage: this.keyMap.unableToMarkAsHelpful,
        detailMessageMoreErrors: this.keyMap.unableToAddObsToast
      },
      generalError: {
        message: this.keyMap.errorToastColon,
        detailMessage: this.translate.instant(this.keyMap.unableToMarkAsHelpful),
        detailMessageMoreErrors: this.keyMap.unableToMarkAsHelpful
      }
    };
  }
  onRetry() {
    this.onMarkHelpful(this.observation.commentId);
  }

  flagged(commentId: string) {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.FLAGGED_FOR_REVIEW);
    const flagModal = this.modalService.open(GuestProfileFlagObservationComponent, {windowClass: 'guest-profile-flag'});
    flagModal.componentInstance.commentId = commentId;
    flagModal.componentInstance.memberId = this.memberId;
    flagModal.componentInstance.firstName = this.firstName;
    flagModal.componentInstance.passEntry.subscribe(() => {
      this.flagDisabled = true;
      this.flagTooltip = this.translate.instant(this.keyMap.obsModalFlagTooltip);
    })
  }

  ngOnDestroy() {
    if (this.errorServiceSubscription$) {
      this.errorServiceSubscription$.unsubscribe();
    }
  }
}
