import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfilePreferencesComponent } from './guest-profile-preferences.component';

xdescribe('GuestProfilePreferencesComponent', () => {
  let component: GuestProfilePreferencesComponent;
  let fixture: ComponentFixture<GuestProfilePreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfilePreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfilePreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
