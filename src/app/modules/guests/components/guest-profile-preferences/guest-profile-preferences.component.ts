import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { cloneDeep } from 'lodash';

import { remove, find, result } from 'lodash';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { GroupProfileEditPreferencesComponent } from '@app/modules/guests/components/group-profile-edit-preferences/group-profile-edit-preferences.component';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GuestListService } from '../../services/guest-list.service';
import { PreferenceResponseData } from '../../models/guest-profile.model';
import { GUEST_CONST } from '../../guests.constants';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { GuestInterestsService } from '@app/modules/guests/services/guest-interests.service';
import { OptionPreferencesResponse, OptionPreferenceResponse } from '@app/modules/guests/models/guest-interest.models';
import { GUEST_PROFILE_PREFERENCES_OPTIONS } from '@app/modules/guests/guest-profile-preferences-options';
import { GUEST_PROFILE_PREFERENCE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';
import { UtilityService } from '@app/services/utility/utility.service';

@Component({
  selector: 'app-guest-profile-preferences',
  templateUrl: './guest-profile-preferences.component.html',
  styleUrls: ['./guest-profile-preferences.component.scss']
})
export class GuestProfilePreferencesComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  constructor(private translate: TranslateService,
    private guestService: GuestListService,
    private interestsService: GuestInterestsService,
    private modalService: NgbModal,
    private utilityService: UtilityService,
    private gaService: GoogleAnalyticsService,
  ) { super(); }

  private roomPreferences$: Subscription = new Subscription();
  public initialized = false;
  public roomPreferenceCheck = false;
  public guestInterestsCheck = false;
  public hasErrors = true;
  public roomPreferences: string[] = [];
  keyMap = GUEST_CONST.KEYMAP;
  maxPreferences = 3;
  showAllPreferences = false;
  errorConfig: ServiceErrorComponentConfig;
  errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  public reportIssueConfig: ReportIssueAutoFillObject;

  private ga = GUEST_CONST.GA;
  private action ;
  private label ;
  ngOnInit() {
    this.reportIssueConfig = GUEST_PROFILE_PREFERENCE_AUTOFILL;
    this.getPreferenceData();
    this.initErrorConfig();
    this.reloadService();
    this.action = this.utilityService.getGAString(this.ga.ROOM_PREFERENCE, this.ga.GUEST_INTERESTS, ' / ');
  }

  private getPreferenceData() {
    this.guestService.getPreference().subscribe(
      this.handlePreference,
      error => this.handleErrors(error));
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      timeout: { message: this.keyMap.errorRetData, showRefreshBtn: true },
      general: { message: this.keyMap.errorRetData, showRefreshBtn: true },
      noData: { message: this.keyMap.none }
    };
  }

  public updateSectionOptions(section:OptionPreferencesResponse, type: string) {
    section.options.forEach((parent) => {
      GUEST_PROFILE_PREFERENCES_OPTIONS[type].forEach((child: OptionPreferenceResponse) => {
        parent.description = parent.key === child.key ? this.translate.instant(child.description) : this.translate.instant(parent.description);
      })
    })
    return section;
  };

  private handlePreference = (pref: PreferenceResponseData) => {
    this.initialized = true;
    this.hasErrors = false;
    this.roomPreferences = [];
    if(pref !== null) {
      this.roomPreferenceCheck = true;
      pref.roomBedPreferences.forEach(option => this.updateSectionOptions(option as OptionPreferencesResponse, option.key));
      this.guestService.setPreferenceResponseData(cloneDeep(pref));
      const getPrefData = pref.roomBedPreferences;
      remove(getPrefData, { selectedValue: 'nopref'});
      getPrefData.forEach((dataItem) => {
      this.roomPreferences.push(result(find(dataItem.options,(option) => {
          return option.key === dataItem.selectedValue;
      }), 'description'));
      });
    }
  }

  private handleErrors = (error) => {
    this.roomPreferenceCheck = false;
    this.processHttpErrors(error);
  }

  public refresh() {
    this.label = this.utilityService.getGAString(this.ga.ROOM_PREFERENCE, this.ga.REFRESH, ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);
    this.getPreferenceData();
  }

  public editPreferenceEnable() {
    return this.roomPreferenceCheck && this.guestInterestsCheck;
  }

  public reloadService() {
    this.roomPreferences$.add(this.guestService.guestInterestsFlag.subscribe((flag) => {
      this.guestInterestsCheck = flag;
    }))
    this.roomPreferences$.add(this.guestService.refreshGridEvent.subscribe(() => {
      this.getPreferenceData();
    }))
  }

  public editPreferences() {
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.ga.EDIT_ICON);
    const activeModal = this.modalService.open(GroupProfileEditPreferencesComponent, {windowClass: 'edit-preferences'});
  }

  onReportIssueClicked() {
    this.label = this.utilityService.getGAString(this.ga.ROOM_PREFERENCE, this.ga.REPORT_ISSUE, ' - ');
    this.gaService.trackEvent(this.ga.GUEST_PROFILE_LBL, this.action , this.label);

  }

  ngOnDestroy() {
    this.roomPreferences$.unsubscribe();
  }
}
