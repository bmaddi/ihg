import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfilePreferredLanguageComponent } from './guest-profile-preferred-language.component';

xdescribe('GuestProfilePreferredLanguageComponent', () => {
  let component: GuestProfilePreferredLanguageComponent;
  let fixture: ComponentFixture<GuestProfilePreferredLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfilePreferredLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfilePreferredLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
