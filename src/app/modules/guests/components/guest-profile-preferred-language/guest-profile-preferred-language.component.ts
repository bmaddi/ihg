import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { GuestListService } from '../../services/guest-list.service';
import { GUEST_CONST } from '../../guests.constants';
import { GuestProfileLangResponse } from '../../models/guest-profile.model';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';

@Component({
  selector: 'app-guest-profile-preferred-language',
  templateUrl: './guest-profile-preferred-language.component.html',
  styleUrls: ['./guest-profile-preferred-language.component.scss']
})
export class GuestProfilePreferredLanguageComponent extends AppErrorBaseComponent implements OnInit {

  constructor(private translate: TranslateService,
    private guestService: GuestListService
  ) { super(); }

  languageData: GuestProfileLangResponse;
  keyMap = GUEST_CONST.KEYMAP;
  public initialized = false;
  public hasErrors = true;
  errorConfig: ServiceErrorComponentConfig;
  errorCardType = ERROR_CARD_TYPE.LINE_ERROR;

  ngOnInit() {
    this.getData();
    this.initErrorConfig();
  }

  getData() {
      this.guestService.getPreferredLanguage().subscribe(
      this.handleGuestFetched,
      error => this.handleErrors(error));
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      timeout: { message: this.keyMap.errorRetData, showRefreshBtn: true },
      general: { message: this.keyMap.errorRetData, showRefreshBtn: true },
      noData: { message: this.keyMap.none }
    };
  }

  private handleGuestFetched = (data: GuestProfileLangResponse) => {
    this.initialized = true;
    this.hasErrors = false;
    this.languageData = data;
  }
  
  private handleErrors = (error) => {
    this.processHttpErrors(error);
  }

  public refresh() {
    this.getData();
  }
}
