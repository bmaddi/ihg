import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileSpendingBreakdownComponent } from './guest-profile-spending-breakdown.component';

xdescribe('GuestProfileSpendingBreakdownComponent', () => {
  let component: GuestProfileSpendingBreakdownComponent;
  let fixture: ComponentFixture<GuestProfileSpendingBreakdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileSpendingBreakdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileSpendingBreakdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
