import { Component, OnInit, Input } from '@angular/core';

import { GUEST_CONST } from '@modules/guests/guests.constants';
import { PastYear, LastStay } from '@modules/guests/models/guest-spending.model';

@Component({
  selector: 'app-guest-profile-spending-breakdown',
  templateUrl: './guest-profile-spending-breakdown.component.html',
  styleUrls: ['./guest-profile-spending-breakdown.component.scss']
})
export class GuestProfileSpendingBreakdownComponent implements OnInit {
  @Input() spent: LastStay | PastYear;
  keyMap = GUEST_CONST.KEYMAP;
  constructor() { }

  ngOnInit() {
  }

}
