import { Component, OnInit, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { LastStay, PastYear } from '../../models/guest-spending.model';

@Component({
  selector: 'app-guest-profile-spending-chart',
  templateUrl: './guest-profile-spending-chart.component.html',
  styleUrls: ['./guest-profile-spending-chart.component.scss'],
  providers: [TranslatePipe]
})
export class GuestProfileSpendingChartComponent implements OnInit, OnDestroy {
  chart: Chart;
  @Input() spent: LastStay | PastYear;
  private languageChangeEvents$: Subscription;
  // Chart configuration
  chartOptions: Highcharts.Options = {
    chart: { type: 'pie', spacingTop: 0, width: 170, height: 100, margin: [0, 0, 0, 0], animation: false },
    title: { text: '', style: { display: 'none' } },
    subtitle: { text: '', style: { display: 'none' } },
    tooltip: {
      borderColor: '#fff',
      style: { fontFamily: 'Open Sans, sans-serif', fontWeight: 'bold' },
      padding: 0,
      useHTML: true,
      headerFormat: '',
      pointFormat: '<div style="padding: 3px;"><span>{point.name}:</span><span style="padding-left: 3px;">{point.percentage:.0f}%</span></div>'
    },
    credits: { enabled: false },
    colors: ['#385e9d', '#ffc600'],
    plotOptions: {
      pie: { allowPointSelect: false, dataLabels: { enabled: false } },
      series: { states: { hover: { enabled: false } } }
    }
  };

  constructor(private translateService: TranslateService,
              private translatePipe: TranslatePipe) { }

  ngOnInit() {
    this.initializeChart();
    this.subscribeToLanguageChange();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.spent && changes.spent.currentValue) {
      this.initializeChart();
    }
  }
  
  private subscribeToLanguageChange() {
    this.languageChangeEvents$ = this.translateService.onLangChange.subscribe(() => {
      this.initializeChart();
    });
  }

  initializeChart() {
    if (!this.spent) return;
    
    const series = [];
    series[0] = {
      type: 'pie', innerSize: 45, animation: false, data: [
        [this.translatePipe.transform('LBL_SPENDING_ACCOMMODATION'), this.spent.accomPct],
        [this.translatePipe.transform('LBL_SPENDING_FOODANDBEVERAGE'), this.spent.fnbPct]
      ]
    }
    this.chartOptions.series = series;
    this.chart = new Chart(this.chartOptions);
  }
  ngOnDestroy() {
    this.languageChangeEvents$.unsubscribe();
  }
}