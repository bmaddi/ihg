import { Component, OnInit } from '@angular/core';

import { GoogleAnalyticsService } from 'ihg-ng-common-core'
import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';

import { ServiceErrorComponentConfig } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { GuestSpendingService } from '@modules/guests/services/guest-spending.service';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { GuestSpendingModel, LastStay, PastYear } from '@modules/guests/models/guest-spending.model';

@Component({
  selector: 'app-guest-profile-spending',
  templateUrl: './guest-profile-spending.component.html',
  styleUrls: ['./guest-profile-spending.component.scss']
})
export class GuestProfileSpendingComponent extends AppErrorBaseComponent implements OnInit {

  keyMap = GUEST_CONST.KEYMAP;
  initialized = false;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  errorConfig: ServiceErrorComponentConfig;
  guestSpendingRes: GuestSpendingModel;
  spent: LastStay | PastYear;
  isLastStayEnabled: boolean = true;
  isFailed: boolean = false;

  reportIssueConfig: ReportIssueAutoFillData = {
    function: 'Guests',
    topic: 'Guest Profile - Spending',
    subtopic: 'Error Message'
  };
  constructor(private guestSpendingService: GuestSpendingService, private guestService: GuestListService, private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.initErrorConfig();
    this.fetchStayData();
  }

  private refresh(action: any) {
    switch (action) {
      case 0: {
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.GUEST_SPENDING, GUEST_CONST.GA.TRY_AGAIN);
        break;
      }
      case 2: {
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.GUEST_SPENDING, GUEST_CONST.GA.REFRESH);
        break;
      }
    }
    this.fetchStayData();
  }

  private fetchStayData() {
    const memberId = this.guestService.getCurrentMemberId();
    this.guestSpendingService.getSpending(memberId).subscribe(spendingData => {
      this.initialized = true;
      this.isFailed = false;
      this.guestSpendingRes = spendingData;
      this.setSpendings();
    },
      err => {
        this.initialized = true;
        this.isFailed = true;
        this.processHttpErrors(err);
      }
    );
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      noData: {
        header: this.keyMap.noDataAvailable,
        message: this.keyMap.noSpendingToDisplay,
        showHorizontalLine: true
      }
    };
  }

  reportIssueClicked() {
    this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.GUEST_SPENDING, GUEST_CONST.GA.REPORT_ISSUE);
  }

  handleTabChange(event: any) {
    const tabClicked = event.nextId;
    switch (tabClicked) {
      case 'lastStay': {
        this.handleLastStayTab();
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.GUEST_SPENDING, GUEST_CONST.GA.LAST_STAY);
        break;
      }
      case 'pastYear': {
        this.handlePastYearTab();
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.GUEST_SPENDING, GUEST_CONST.GA.PAST_YEAR);
        break;
      }
    }
  }

  private handleLastStayTab(): void {
    this.isLastStayEnabled = true;
    this.setSpendings();
  }

  private handlePastYearTab(): void {
    this.isLastStayEnabled = false;
    this.setSpendings();
  }

  private setSpendings(): void {
    // Set data based on the selected tab
    const validator = this.isLastStayEnabled ? data => !data.lastStay : data => !data.pastYear;
    if (!this.isFailed && !this.checkIfAnyApiErrors(this.guestSpendingRes, validator))
      this.spent = this.isLastStayEnabled ? this.guestSpendingRes.lastStay : this.guestSpendingRes.pastYear;
  }
}