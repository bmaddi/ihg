import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileStayComponent } from './guest-profile-stay.component';

xdescribe('GuestProfileStayComponent', () => {
  let component: GuestProfileStayComponent;
  let fixture: ComponentFixture<GuestProfileStayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileStayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileStayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
