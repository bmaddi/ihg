import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { AppConstants } from '@app/constants/app-constants';
import { UtilityService } from '@app/services/utility/utility.service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { ERROR_CARD_TYPE } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { GuestStayModel, GuestLastStayModel } from '../../models/guest-stay.models';
import { GuestListService } from '../../services/guest-list.service';
import { GuestStayService } from '../../services/guest-stay.service';
import { GUEST_CONST } from '../../guests.constants';
import { GUEST_PROFILE_STAY_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

@Component({
  selector: 'app-guest-profile-stay',
  templateUrl: './guest-profile-stay.component.html',
  styleUrls: ['./guest-profile-stay.component.scss']
})
export class GuestProfileStayComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  stayData: GuestStayModel;
  keyMap = GUEST_CONST.KEYMAP;
  dateFormat =  AppConstants.PIPE_DD_MM_FORMAT;
  initialized = false;
  errorConfig: ServiceErrorComponentConfig;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  public reportIssueConfig: ReportIssueAutoFillObject;
  private subscriptions$: Subscription = new Subscription();
  private hotelNameCharLimit:number = 60;
  constructor(
    private translate: TranslateService,
    private guestService: GuestListService,
    private guestStayService: GuestStayService,
    private utilityService: UtilityService,
    private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.reportIssueConfig = GUEST_PROFILE_STAY_AUTOFILL;
    this.initErrorConfig();
    this.fetchStayData();
  }

  ngOnDestroy() {
    this.subscriptions$.unsubscribe();
  }

  refresh(action: any) {
    switch (action) {
      case 0: {
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.STAYS_CARD, GUEST_CONST.GA.TRY_AGAIN);
        break;
      }
      case 2: {
        this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.STAYS_CARD, GUEST_CONST.GA.REFRESH);
        break;
      }
    }
    this.fetchStayData();
  }

  reportIssueClicked() {
    this.gaService.trackEvent(GUEST_CONST.GA.GUEST_PROFILE_GA, GUEST_CONST.GA.STAYS_CARD, GUEST_CONST.GA.REPORT_ISSUE);
  }

  private fetchStayData() {
    const memberId = this.guestService.getCurrentMemberId();
    this.subscriptions$.add(this.guestStayService.getStays(memberId).subscribe(stayData => {
        this.initialized = true;
        if (!this.checkIfAnyApiErrors(stayData, data => data.lastStays.length === 0)) {
          this.stayData = stayData;
        }
      },
      err => {
        this.initialized = true;
        this.processHttpErrors(err);
      }
    ));
  }

  private initErrorConfig() {
    this.errorConfig = <Partial<ServiceErrorComponentConfig>>{
      noData: {
        header: this.keyMap.noDataAvailable,
        message: this.keyMap.staysNoData,
        showHorizontalLine: true }
    };
  }

  private getConcatenatedHotelBrandAndName(lastStay: GuestLastStayModel):string {
    return lastStay.hotelBrand &&  lastStay.hotelBrand.concat(' ',  lastStay.hotelName);
  }

  public displayHotelBrandAndName(lastStay: GuestLastStayModel):string {
    return this.utilityService.truncateWithEllipsis(this.getConcatenatedHotelBrandAndName(lastStay), this.hotelNameCharLimit);
  }

  public displayHotelBrandAndNameToolTip(lastStay: GuestLastStayModel):string {
    return this.getConcatenatedHotelBrandAndName(lastStay).length > this.hotelNameCharLimit ? this.getConcatenatedHotelBrandAndName(lastStay) : '';
  }
}
