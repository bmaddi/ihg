import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileComponent } from './guest-profile.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';

xdescribe('GuestProfileComponent', () => {
  let component: GuestProfileComponent;
  let fixture: ComponentFixture<GuestProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuestProfileComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonTestModule],
      providers: [ReportIssueService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(GuestProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
