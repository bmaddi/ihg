import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';

import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { GuestListService } from '../../services/guest-list.service';
import { GuestProfileModel } from '../../models/guest-profile.model';
import { GUEST_CONST } from '../../guests.constants';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { UtilityService } from '@services/utility/utility.service';
import {HelpService} from '@app-shared/components/help/help.service';

@Component({
  selector: 'app-guest-profile',
  templateUrl: './guest-profile.component.html',
  styleUrls: ['./guest-profile.component.scss']
})
export class GuestProfileComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  constructor(
    public utility: UtilityService,
    private router: Router,
    private guestService: GuestListService,
    private appErrorsService: AppErrorsService,
    private titleService: Title,
    private helpService: HelpService
  ) { super(); }

  @Input() reportIssueAutoFill: ReportIssueAutoFillData;

  guest: GuestProfileModel;
  guestDisplayName: string;
  keyMap = GUEST_CONST.KEYMAP;
  guestErr = false;
  guestNameCharLimit = 57;
  previousTitle: string;

  ngOnInit() {
    this.getData();
    this.helpService.setTabStateChange('GUEST_PROFILE');
  }

  private tryInitTitle() {
    if (!this.previousTitle && this.guest) {
      this.previousTitle = this.titleService.getTitle();
      this.titleService.setTitle(`Concerto™ - ${this.guest.lastName}, ${this.guest.firstName}`);
    }
  }

  getData() {
    const id = this.guestService.getCurrentMemberId();
    if (id) {
      this.guestService.getGuestProfile(id).subscribe(
        this.handleGuestFetched,
        error => this.handleErrors(error));
    } else {
      this.router.navigate([`guest-list-details/`]);
    }
  }

  getGuestAddress(guest) {
    return _(guest.address).values().compact().value().join(', ');
  }

  private handleGuestFetched = (guest: GuestProfileModel) => {
    this.guestErr = false;
    this.hasErrors = false;
    this.guest = guest;
    const salutation = this.formatSalutation(guest.salutation);
    this.guestDisplayName = `${salutation}${guest.firstName} ${guest.lastName} ${guest.suffix}`;
    this.tryInitTitle();
  }

  private formatSalutation(guestSalutation: string): string {
    if (guestSalutation) {
      guestSalutation = guestSalutation.replace(/[^\w\s]/gi, '').trim();
      return guestSalutation + '. ';
    } else {
      return '';
    }
  }

  private handleErrors = (error) => {
    this.guestErr = true;
    this.processHttpErrors(error);
  }

  public refresh() {
    this.getData();
  }

  onReportIssue() {
    this.appErrorsService.openReportIssueModal(this.reportIssueAutoFill);
  }

  ngOnDestroy() {
    if (this.previousTitle) {
      this.titleService.setTitle(this.previousTitle);
    }
  }
}
