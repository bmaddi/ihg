import { includes, isEmpty } from 'lodash';

import { GuestHeartBeatSurvey } from '../models/guest-profile-heartbeat.model';

export const lowGuestLoveValues: string[] = ['R', 'R*'];

export const shouldNotShowHeartBeat = (survey: GuestHeartBeatSurvey) => {
  return survey ? isEmpty(survey.sleepIssue) && isEmpty(survey.likeComment)
    && isEmpty(survey.makeStayBetterComment)
    && !includes(lowGuestLoveValues, survey.guestLove) : true;
};
