export enum PaymentType {
  creditCard = 'CARD',
  other = 'OTHER'
}

export enum ReservationStatus {
  checkedIn = 'CHECKEDIN',
  dueIn = 'DUEIN',
  inHouse = 'INHOUSE'
}
