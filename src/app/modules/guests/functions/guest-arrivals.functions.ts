import { isNil } from 'lodash';

import { PaymentType } from '../enums/guest-arrivals.enums';


export const checkForIneligibleReservation = (roomCount: number, paymentType: string, enabled: boolean = true) => {
  if (!enabled || (isNil(roomCount) && isNil(paymentType))) {
    return false;
  }
  return roomCount > 1 || paymentType !== PaymentType.creditCard;
};
