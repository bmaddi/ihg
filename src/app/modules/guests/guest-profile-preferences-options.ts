export const GUEST_PROFILE_PREFERENCES_OPTIONS = {
    checkBoxType: {
        interest: 'interest',
        exercise: 'exercise',
        relaxation: 'relaxation',
        destinations: 'destinations',
        dietary: 'dietary'
    },
    destinations: [
        {
            key: 'dt_beach',
            description: 'LBL_BEACH',
            selectedValue: "false"
        },
        {
            key: 'dt_mountain',
            description: 'LBL_MOUNTAIN',
            selectedValue: "false"
        },
        {
            key: 'dt_city',
            description: 'LBL_CITY',
            selectedValue: "false"
        },
        {
            key: 'dt_resort',
            description: 'LBL_RESORT',
            selectedValue: "false"
        },
        {
            key: 'dt_attractions',
            description: 'LBL_ATTRACTIONS',
            selectedValue: "false"
        },
        {
            key: 'dt_park',
            description: 'LBL_PARK',
            selectedValue: "false"
        }
    ],

    interest: [
        {
            key: 'culture',
            description: 'LBL_ART_CULTURA',
            selectedValue: "false"
        },
        {
            key: 'cooking',
            description: 'LBL_COOKING',
            selectedValue: "false"
        },
        {
            key: 'fitness',
            description: 'LBL_HEALTH_FITNESS',
            selectedValue: "false"
        },
        {
            key: 'music',
            description: 'LBL_MUSIC_CONCERTS',
            selectedValue: "false"
        },
        {
            key: 'outdoors',
            description: 'LBL_OUTDOORS',
            selectedValue: "false"
        },
        {
            key: 'theater',
            description: 'LBL_THEATER',
            selectedValue: "false"
        },
        {
            key: 'travel',
            description: 'LBL_TRAVEL',
            selectedValue: "false"
        },
        {
            key: 'volunteering',
            description: 'LBL_VOLUNTEERING',
            selectedValue: "false"
        },
        {
            key: 'shopping',
            description: 'LBL_SHOPPING',
            selectedValue: "false"
        },
        {
            key: 'sports',
            description: 'LBL_SPORTS',
            selectedValue: "false"
        },
        {
            key: 'women',
            description: 'LBL_WOMENS_TOPICS',
            selectedValue: "false"
        },
        {
            key: 'lgbt',
            description: 'LBL_LGBT',
            selectedValue: "false"
        },
        {
            key: 'food',
            description: 'LBL_FOOD_WINE',
            selectedValue: "false"
        }
    ],
    exercise: [
        {
            key: 'core_muscles',
            description: 'LBL_FOCUS_MUSCLES',
            selectedValue: "false"
        },
        {
            key: 'gym',
            description: 'LBL_GYM_STRENGTH',
            selectedValue: "false"
        },
        {
            key: 'group_fitness',
            description: 'LBL_GROUP_FITNESS',
            selectedValue: "false"
        },
        {
            key: 'outdoor_activities',
            description: 'LBL_OUTDOOR_ACTIVITIES',
            selectedValue: "false"
        },
        {
            key: 'swimming',
            description: 'LBL_SWIMMING',
            selectedValue: "false"
        },
        {
            key: 'yoga',
            description: 'LBL_YOGA',
            selectedValue: "false"
        },
        {
            key: 'on_my_own',
            description: 'LBL_GREAT_WORKOUT',
            selectedValue: "false"
        },
        {
            key: 'running',
            description: 'LBL_RUNNING',
            selectedValue: "false"
        }
    ],
    relaxation: [
        {
            key: 'comfortable_room',
            description: 'LBL_CONFT_BED_PILLOW_LIGHTING',
            selectedValue: "false"
        },
        {
            key: 'alcoholic_drink',
            description: 'LBL_BEER_WINE_COCKTAIL',
            selectedValue: "false"
        },
        {
            key: 'non_alcoholic_drink',
            description: 'LBL_HOT_TEA_UNWIND',
            selectedValue: "false"
        },
        {
            key: 'book',
            description: 'LBL_GOOD_BOOK',
            selectedValue: "false"
        },
        {
            key: 'meditation',
            description: 'LBL_MEDITATING',
            selectedValue: "false"
        }
    ],
    dietary: [
        {
            key: 'nopref',
            description: 'LBL_NO_PREFERENCE'
        },
        {
            key: 'gluten_free',
            description: 'LBL_GLUTEN_FREE'
        },
        {
            key: 'low_carb',
            description: 'LBL_LOW_CARB'
        },
        {
            key: 'low_fat',
            description: 'LBL_LOW_FAT'
        },
        {
            key: 'vegetarian',
            description: 'LBL_VEGETARIAN'
        },
        {
            key: 'vegan',
            description: 'LBL_VEGAN'
        },
        {
            key: 'tasty',
            description: 'LBL_TASTY'
        }
    ],
    elevator: [
        {
            key: 'nopref',
            description: 'LBL_NO_PREFERENCE'
        },
        {
            key: 'near',
            description: 'LBL_NEAR_ELEVATOR'
        },
        {
            key: 'away',
            description: 'LBL_AWAY_FROM_ELEVATOR'
        }
    ],
    smoking: [
        {
            key: 'nopref',
            description: 'LBL_NO_PREFERENCE'
        },
        {
            key: 'smoking',
            description: 'LBL_SMOKING'
        },
        {
            key: 'non_smoking',
            description: 'LBL_NON_SMOKING'
        }
    ],
    floor: [
        {
            key: 'nopref',
            description: 'LBL_NO_PREFERENCE'
        },
        {
            key: 'lowfloor',
            description: 'LBL_LOW_FLOOR'
        },
        {
            key: 'hifloor',
            description: 'LBL_HIGH_FLOOR'
        },
        {
            key: 'ground',
            description: 'LBL_GROUND_FLOOR'
        }
    ],
    replaceLabel: {
        LBL_ART_CULTURA: {
            original: 'LBL_ART_CULTURA',
            replace: 'LBL_ART_AND_CULTURA'
        },
        LBL_HEALTH_FITNESS: {
            original: 'LBL_HEALTH_FITNESS',
            replace: 'LBL_HEALTH_AND_FITNESS'
        },
        LBL_FOOD_WINE: {
            original: 'LBL_FOOD_WINE',
            replace: 'LBL_FOOD_AND_WINE'
        },
        LBL_GYM_STRENGTH: {
            original: 'LBL_GYM_STRENGTH',
            replace: 'LBL_GYM_AND_STRENGTH'
        }
    }

}
