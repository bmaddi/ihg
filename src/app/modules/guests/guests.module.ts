import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ChartModule } from 'angular-highcharts';

import { IhgNgCommonCoreModule, AccessControlResolver } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';

import { GuestListRoutingModule } from './guests.routes';
import { GuestListComponent } from './components/guest-list/guest-list.component';
import { GuestProfileComponent } from './components/guest-profile/guest-profile.component';
import { GuestListService } from './services/guest-list.service';
import { GuestListGridComponent } from './components/guest-list-grid/guest-list-grid.component';
import { GuestProfileLoyaltyComponent } from './components/guest-profile-loyalty/guest-profile-loyalty.component';
import { GuestProfileObservationListComponent } from './components/guest-profile-observation-list/guest-profile-observation-list.component';
import { GuestProfileObservationComponent } from './components/guest-profile-observation/guest-profile-observation.component';
import { GuestProfileStayComponent } from './components/guest-profile-stay/guest-profile-stay.component';
import { AppSharedModule } from '@app/modules/shared/app-shared.module';
import { PrepareModule } from '@modules/prepare/prepare.module';
import { GuestArrivalsComponent } from './components/guest-arrivals/guest-arrivals.component';
import { AddObsCancelModalComponent } from './components/add-obs-cancel-modal/add-obs-cancel-modal.component';
import { AddObservationModalComponent } from '@app/modules/guests/components/add-obs-modal';
import { GroupProfileEditPreferencesComponent } from './components/group-profile-edit-preferences/group-profile-edit-preferences.component';
import { GuestProfileInsightsComponent } from '@modules/guests/components/guest-profile-insights/guest-profile-insights.component';
import { GuestProfileInsightComponent } from './components/guest-profile-insight/guest-profile-insight.component';
import { GuestProfilePreferencesComponent } from './components/guest-profile-preferences/guest-profile-preferences.component';
import { GuestProfilePreferredLanguageComponent } from './components/guest-profile-preferred-language/guest-profile-preferred-language.component';
import { GuestProfileFlagObservationComponent } from './components/guest-profile-flag-observation/guest-profile-flag-observation.component';
import { GuestProfileInterestsComponent } from '@app/modules/guests/components/guest-profile-interests/guest-profile-interests.component';
import { GuestProfileHeartbeatComponent } from './components/guest-profile-heartbeat/guest-profile-heartbeat.component';
import { GuestProfileHeartbeatIconsComponent } from './components/guest-profile-heartbeat-icons/guest-profile-heartbeat-icons.component';
import { GuestProfileSpendingComponent } from '@modules/guests/components/guest-profile-spending/guest-profile-spending.component';
import { GuestProfileSpendingChartComponent } from '@modules/guests/components/guest-profile-spending-chart/guest-profile-spending-chart.component';
import { GroupProfileEditPrefCheckboxComponent } from './components/group-profile-edit-pref-checkbox/group-profile-edit-pref-checkbox.component';
import { GroupBlockModule } from '@modules/group-block/group-block.module';
import { BadgeListModule } from '@app/modules/badge-list/badge-list.module';
import { InHouseModule } from '@modules/in-house/in-house.module';
import { ProgressStatusModule } from '@modules/progress-status/progress-status.module';
import { GuestSearchModule } from '@modules/guest-search/guest-search.module';
import { GuestProfileSpendingBreakdownComponent } from '@modules/guests/components/guest-profile-spending-breakdown/guest-profile-spending-breakdown.component';
import { PrintRecordModule } from '@app/modules/print-record/print-record.module';
import { AllGuestsModule } from '@modules/all-guests/all-guests.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDropdownModule.forRoot(),
    NgbModule.forRoot(),
    TranslateModule,
    GridModule,
    ChartModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    GuestListRoutingModule,
    DropDownsModule,
    AppSharedModule,
    PrepareModule,
    InHouseModule,
    ReactiveFormsModule,
    GroupBlockModule,
    BadgeListModule,
    ProgressStatusModule,
    GuestSearchModule,
    PrintRecordModule,
    AllGuestsModule
  ],
  declarations: [
    GuestListComponent,
    GuestProfileComponent,
    GuestListGridComponent,
    GuestProfileLoyaltyComponent,
    GuestProfileObservationListComponent,
    GuestProfileObservationComponent,
    GuestProfileStayComponent,
    GuestProfileSpendingComponent,
    GuestProfileInsightsComponent,
    AddObservationModalComponent,
    GuestArrivalsComponent,
    AddObsCancelModalComponent,
    GroupProfileEditPreferencesComponent,
    GuestProfileInsightComponent,
    GuestProfilePreferencesComponent,
    GuestProfilePreferredLanguageComponent,
    GuestProfileFlagObservationComponent,
    GuestProfileInterestsComponent,
    GuestProfileHeartbeatComponent,
    GuestProfileHeartbeatIconsComponent,
    GroupProfileEditPrefCheckboxComponent,
    GuestProfileSpendingChartComponent,
    GuestProfileSpendingBreakdownComponent
  ],
  providers: [
    GuestListService,
    AccessControlResolver
  ],
  entryComponents: [
    AddObservationModalComponent,
    AddObsCancelModalComponent,
    GroupProfileEditPreferencesComponent,
    GuestProfileFlagObservationComponent
  ]
})
export class GuestsModule {
}
