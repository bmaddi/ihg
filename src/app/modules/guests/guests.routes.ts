import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TransitionGuardService, AccessControlResolver } from 'ihg-ng-common-core';

import { GuestListComponent } from './components/guest-list/guest-list.component';
import { GuestProfileComponent } from './components/guest-profile/guest-profile.component';

const routes: Routes = [
  {
    path: 'guest-list-details',
    resolve: { AccessControlResolver },
    component: GuestListComponent,
    canDeactivate: [
      TransitionGuardService
    ],
    data: {
      crumb: ['dashboard', 'guest'],
      pageTitle: 'LBL_GST_LST',
      stateName: 'guest-list-details',
      slnmId: 'GuestList-SID',
      maxWidth: true,
      noSubtitle: true,
      showHelp: true,
      accessControlTag: 'guest_list'
    }
  },
  {
    path: 'guest-profile',
    component: GuestProfileComponent,
    data: {
      stateName: 'guest-profile',
      slnmId: 'GuestProfile-SID',
      maxWidth: true,
      noSubtitle: true,
      showHelp: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class GuestListRoutingModule {
}
