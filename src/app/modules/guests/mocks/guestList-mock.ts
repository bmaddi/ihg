export const GUEST_LIST_MOCK = {
    data: [
        {
            arrival: 'Early',
            arrivalTime: '',
            badgeIconPaths: [],
            badges: [],
            deparatureDate: '2019-09-18',
            firstName: 'Hema',
            groupName: 'Rising Phoenix',
            guestName: 'Balaji, Hema',
            ineligibleReservation: true,
            lastName: 'Balaji',
            loyaltyId: '',
            nights: 1,
            paymentType: '',
            preferences: ['City View City View City View City View City View City View', 'Near Elevator Near Elevator Near Elevator Near Elevator', 'Balcony View Balcony View Balcony View Balcony View', 'High Floor High Floor High Floor High Floor High Floor', 'Garden View Garden View Garden View Garden View Garden View', 'City View City View City View City View City View City View', 'Near Elevator Near Elevator Near Elevator Near Elevator', 'Balcony View Balcony View Balcony View Balcony View', 'High Floor High Floor High Floor High Floor High Floor', 'Garden View Garden View Garden View Garden View Garden View'],
            prepStatus: 'READY',
            reservationNumber: '27453651',
            reservationStatus: 'DUEIN',
            roomCount: 1,
            roomNumber: '',
            roomType: '',
            specialRequests: ['High Floor Near Elevator High Floor Near Elevator High Floor Near Elevator']
        }, {
            arrival: 'Late',
            arrivalTime: '',
            badgeIconPaths: [],
            badges: [],
            deparatureDate: '2019-09-16',
            firstName: 'Sharon',
            groupName: 'Rising Phoenix',
            guestName: 'Butler, Sharon',
            ineligibleReservation: true,
            lastName: 'Butler',
            loyaltyId: '',
            nights: 1,
            paymentType: 'CARD',
            prepStatus: 'INPROGRESS',
            reservationNumber: '22020368',
            reservationStatus: 'DUEIN',
            roomCount: 2,
            roomNumber: '',
            roomType: '',
            specialRequests: ['High Floor Near Elevator High Floor Near Elevator High Floor Near Elevator']
        }],
    total: 2
};



export const MOCK_GUEST_DETAILS = {
    arrival: 'Late',
    arrivalTime: '',
    badgeIconPaths: [],
    badges: [],
    ihgRcNumber: '',
    deparatureDate: '2019-09-16',
    firstName: 'Sharon',
    groupName: 'Rising Phoenix',
    guestName: 'Butler, Sharon',
    ineligibleReservation: true,
    lastName: 'Butler',
    loyaltyId: '',
    nights: '1',
    paymentType: 'CARD',
    prepStatus: 'INPROGRESS',
    reservationNumber: '22020368',
    reservationStatus: 'DUEIN',
    roomCount: 2,
    roomNumber: '',
    roomType: '',
    checkInDate: ''
};
