export interface GuestInsightModel {
  icons: String[];
  badgeIconPaths: string[];
  primaryText: string;
  secondaryText: boolean;
  enrollmentDate?: string;
  firstName?: string;
  pointsToAchieve?: string;
  nextLoyaltyLevel?: string;
}

export interface GuestInsightsResponse {
  asOfDate: string;
  actions: GuestInsightAction[];
}

export interface GuestInsightAction {
  enrollmentDate?: string;
  action: string;
  pointsToAchieve?: string;
  nextLoyaltyLevel?: string;
}
