export interface ValuePreferenceResponse {
  key: string;
  description: string;
  selectedValue: string;
}

export interface OptionPreferenceResponse {
  key: string;
  description: string;
}

export interface OptionPreferencesResponse {
  selectedValue: string;
  options: OptionPreferenceResponse[];
}

export interface GuestInterestSectionResponse {
  description: string;
  preferences: ValuePreferenceResponse[] | OptionPreferencesResponse[];
  key?: string;
}

export interface GuestInterestsResponse {
  interests: GuestInterestSectionResponse[];
}

export interface GuestInterestsModel {
  key: string;
  description: string;
  selectedValue: string;
  displayValue: string;
}

export interface PreferenceUpdate {
  key: string;
  value: string;
}