export interface GuestListResponse {
  items: GuestResponse[];
  localCurrentDate: string;
  totalItems: number;
}

export interface GuestListModel extends GuestListResponse {
  items: GuestGridModel[];
  localCurrentDateParsed: Date;
}

export interface GuestResponse {
  firstName: string;
  lastName: string;
  groupName: string;
  ihgRcNumber: string;
  reservationNumber: string;
  badges: string[];
  arrival: string;
  arrivalTime: string;
  nights: string;
  checkInDate: string;
  deparatureDate: string;
  roomNumber: string;
  roomType: string;
  paymentType: string;
  roomCount: number;
  reservationStatus: string;
  prepStatus: string;
  loyaltyId: string;
}

export interface GuestGridModel extends GuestResponse {
  badgeIconPaths: string[];
  guestName: string;
  ineligibleReservation?: boolean;
}

export interface GuestArrivalParams {
  selectedDate: string;
  memberLevel?: string;
  reservationStatus?: string;
  localDate?: string;
  reservationNumber?: string;
  expanded?: boolean;
}

export interface GuestInHouseParams {
  searchCriteria?: string;
  rowCount?: number | string;
  showGroups?: boolean;
  showInHouse?: boolean;
  groupSelection?: string;
  guestSelection?: string;
  roomSelection?: string;
  hideToggle?: boolean;
}

export interface AllGuestParams {
  searchCriteria?: string;
  rowCount?: number | string;
  showGroups?: boolean;
  showInHouse?: boolean;
  groupSelection?: string;
  guestSelection?: string;
  roomSelection?: string;
  hideToggle?: boolean;
}

export interface ManageStayParams {
  reservationNumber: string;
  inHouseParams: GuestInHouseParams;
  pmsNumber: string;
}

export interface GuestTabFlag {
  tabFlag: boolean;
  dateIndex: number;
  selectedData: GuestGridModel;
  searchedDate: Date;
}
