export interface GuestHeartBeat {
  memberId?: number;
  heartBeatMyHotel: GuestHeartBeatSurvey;
  heartBeatOtherHotel: GuestHeartBeatSurvey;
}

export interface GuestHeartBeatSurvey {
  hotelCode?: string;
  surveyDate: string;
  sleepIssue: string;
  guestLove: string;
  likeComment?: string;
  makeStayBetterComment?: string;
}
