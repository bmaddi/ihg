import * as moment from 'moment';

export interface GuestProfileResponse {
  salutation: string;
  firstName: string;
  lastName: string;
  suffix: string;
  priorityClubBadge: string;
  ambassadorBadge: string;
  guestLoyalty: GuestProfileLoyaltyResponse;
  roomPreferences: string[];
  guestInterests: string[];
  preferredLanguage: GuestProfileLangResponse;
  hotelCurrentDate: string;
  phoneNumber: string;
  email: string;
  address: GuestAddress;
  myHotelObservations: GuestProfileObservationResponse[];
}

export interface GuestProfileLangResponse {
  preferredLanguageName: string;
  preferredLanguageCode: string;
}

export interface GuestProfileLangModel extends GuestProfileLangResponse{
  iconPath: string;
}

export interface GuestProfileModel extends GuestProfileResponse {
  badgeIconPaths: string[];
  guestLoyalty: GuestProfileLoyaltyModel;
  myHotelObservations: GuestProfileObservationModel[];
  otherHotelObservations?: GuestProfileObservationModel[];
  allHotelObservations?: GuestProfileObservationModel[];
  preferredLanguage: GuestProfileLangModel;
}

export interface GuestProfileLoyaltyResponse {
  priorityClubLevel: string;
  loyaltyID: string;
  enrollmentDate: string;
  pointsBalance: number;
  ambassadorExpirationDate: string;
  ambassadorClubLevel: string;
  innerCircleClubLevel: string;
  karmaClubLevel: string;
  employeeClubLevel: string;
}

export interface GuestProfileLoyaltyModel extends GuestProfileLoyaltyResponse {
  enrollmentDateFormatted: string;
  pointsBalanceFormatted: string;
  ambassadorExpDateFormatted: string;
  isExpiringWithin30Days: boolean;
  circleOrKarmaClubLevel: string;
}

export interface GuestProfileObservationResponse {
  brandLogo: string;
  hotelChainCode: string;
  hotelName: string;
  hotelCode: string;
  createdTimestamp: string;
  comment: string;
  mostHelpfulCount: number;
  createdByUserId: string;
  observationsCount: number;
  commentId: string;
  category: string;
  observationFlag: boolean;
}

export interface GuestProfileObservationModel extends GuestProfileObservationResponse {
  hotelLogoPath: string;
  postDateFormatted: string;
}

export interface GuestAddress {
  line1: string;
  line2: string;
  line3: string;
  line4: string;
  line5: string;
  locality1: string;
  locality2: string;
  postalCode: string;
  region1: string;
  region2: string;
  countryCode: string;
  companyName: string;
}

export interface NewGuestObservation {
  category: string;
  text: string;
  visibleToAll: boolean;
  memberId: string;
}

export interface ValueLabelModel {
  value: string;
  label: string;
}

export interface KeyDesModel {
  key: string;
  description: string;
  selectedValue?: string;
}

export interface PreferenceData {
  elevator: string;
  floor: string;
  smoking: string;
  dietary: string;
}

export interface RoomBedPreferences {
  key: string;
  selectedValue: string;
  options: KeyDesModel[];
}

export interface PreferenceResponseData {
  destinations: KeyDesModel[];
  roomBedPreferences: RoomBedPreferences[];
}

export type ObservationListType = 'ALL' | 'OTHER' | 'MY';

export type ObservationSortType = 'HOTEL_CODE' | 'MOST_RECENT' | 'MOST_HELPFUL';
