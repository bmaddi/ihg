export interface LastStay {
  accomPct: number;
  accomTotal: number;
  currencyCode: string;
  fnbPct: number;
  fnbTotal: number;
  totalAmount: number;
}

export interface PastYear {
  accomPct: number;
  accomTotal: number;
  currencyCode: string;
  fnbPct: number;
  fnbTotal: number;
  totalAmount: number;
}

export interface GuestSpendingModel {
  lastStay: LastStay;
  pastYear: PastYear;
}
