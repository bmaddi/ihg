export interface GuestStayResponse {
  lastStays: GuestLastStayResponse[];
  numberOfStaysMyHotel: number;
  totalStaysIhgHotels: number;
  percentOfMostVisits: string;
  brandMostVisited: string;
}

export interface GuestLastStayResponse {
  hotelName: string;
  hotelBrand:string;
  checkInDate: string;
  numberOfNights: number;
  accruedPoints?: number | string;
}

export interface GuestStayModel extends GuestStayResponse {
  lastStays: GuestLastStayModel[];
  showAccruedPoints?: boolean;
}

export interface GuestLastStayModel extends GuestLastStayResponse {
  checkInDateParsed: Date;
}
