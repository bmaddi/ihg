import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { GuestHeartbeatService } from './guest-heartbeat.service';

describe('GuestHeartbeatService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: GuestHeartbeatService = TestBed.get(GuestHeartbeatService);
    expect(service).toBeTruthy();
  });
});
