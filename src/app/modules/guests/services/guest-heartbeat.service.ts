import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';
import { GuestHeartBeat } from '../models/guest-profile-heartbeat.model';

@Injectable({
  providedIn: 'root'
})
export class GuestHeartbeatService {
  private readonly API_URL = environment.fdkAPI;

  constructor(private http: HttpClient) {
  }

  getGuestProfileHeartbeat(params: { [param: string]: string }, displaySpinner = true): Observable<GuestHeartBeat> {
    const apiUrl = `${this.API_URL}guestProfile/heartbeat`;
    const httpOptions = { headers: new HttpHeaders({ spinnerConfig: displaySpinner ? 'Y' : 'N' }), params: params };
    return this.http.get(apiUrl, httpOptions)
      .pipe(map((response: GuestHeartBeat) => response));
  }
}
