import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

import {
  GuestInsightAction, GuestInsightModel,
  GuestInsightsResponse
} from '../models/guest-insight.models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { GUEST_INSIGHT} from '@app/modules/guests/guests.constants';
import { AppConstants } from '@app/constants/app-constants';
import { environment } from '@env/environment';
import { GuestProfileModel } from '@app/modules/guests/models/guest-profile.model';
import { BadgeIconPaths } from '@app/constants/badge-icon-paths';


@Injectable({
  providedIn: 'root'
})
export class GuestInsightsService {

  private insightTypes = GUEST_CONST.INSIGHT_TYPES;
  private keyMap = GUEST_CONST.KEYMAP;
  private readonly API_URL = environment.fdkAPI;
  private badgeIconPaths = BadgeIconPaths;

  constructor(private http: HttpClient,
              private translate: TranslateService) { }

  public getInsights(guest: GuestProfileModel): Observable<GuestInsightModel[]> {
    const guestMemberId = guest.guestLoyalty.loyaltyID;
    const apiUrl = `${this.API_URL}guestProfile/insight?memberId=${guestMemberId}`;
    return this.http.get(apiUrl, { observe: 'response' }).pipe(
      map((response: HttpResponse<GuestInsightAction[]>) => this.convertInsightsResponseToInsightsModels(response && response.body ? response.body : [], guest))
    );
  }

  private convertInsightsResponseToInsightsModels(response: GuestInsightAction[], guest: GuestProfileModel): GuestInsightModel[] {
    const actions: GuestInsightModel[] = [];
    for (const insight of response) {
      switch (insight.action) {
        case this.insightTypes.NEW_MEMBER: actions.push(this.createMemberInsight('NEW_MEMBER', insight.enrollmentDate, guest)); break;
        case this.insightTypes.NEW_SPIRE: actions.push(this.createMemberInsight('NEW_SPIRE', insight.enrollmentDate, guest)); break;
        case this.insightTypes.NEW_PLATINUM: actions.push(this.createMemberInsight('NEW_PLATINUM', insight.enrollmentDate, guest)); break;
        case this.insightTypes.NEW_GOLD: actions.push(this.createMemberInsight('NEW_GOLD', insight.enrollmentDate, guest)); break;
        case this.insightTypes.NEW_AMBASSADOR: actions.push(this.createMemberInsight('AMBASSADOR', insight.enrollmentDate, guest)); break;
        case this.insightTypes.NEW_ROYAL_AMBASSADOR: actions.push(this.createMemberInsight('ROYAL_AMBASSADOR', insight.enrollmentDate, guest)); break;
        case this.insightTypes.NEW_INNER_CIRCLE: actions.push(this.createMemberInsight('NEW_INNER_CIRCLE', insight.enrollmentDate, guest)); break;
        case this.insightTypes.OWNERS_ASSOCIATION: actions.push(this.createMemberInsight('OWNERS_ASSOCIATION', insight.enrollmentDate, guest)); break;
        case this.insightTypes.CLOSE_NEXT_MEMBERSHIP: actions.push(this.createMemberCloseNextInsight('CLOSE_NEXT_MEMBERSHIP', insight, guest)); break;
      }
    }
    return actions;
  }

  private createMemberInsight(action, enrollmentDate, guest: GuestProfileModel): GuestInsightModel {
    if (enrollmentDate) {
      return {
        icons: GUEST_INSIGHT.INSIGHT_ACTIONS[action].icons,
        badgeIconPaths: GUEST_INSIGHT.INSIGHT_ACTIONS[action].badgeIconPaths,
        primaryText: GUEST_INSIGHT.INSIGHT_ACTIONS[action].primaryText,
        secondaryText: GUEST_INSIGHT.INSIGHT_ACTIONS[action].secondaryText,
        enrollmentDate: moment(enrollmentDate, AppConstants.DB_DT_FORMAT).format('MMMM Do'),
        firstName: guest.firstName
      };
    } else {
      return {
        icons: GUEST_INSIGHT.INSIGHT_ACTIONS[action].icons,
        badgeIconPaths: GUEST_INSIGHT.INSIGHT_ACTIONS[action].badgeIconPaths,
        primaryText: GUEST_INSIGHT.INSIGHT_ACTIONS[action].primaryText,
        secondaryText: null,
        enrollmentDate: null,
        firstName: guest.firstName
      };
    }
  }

  private createMemberCloseNextInsight(action: string, insight: GuestInsightAction , guest: GuestProfileModel): GuestInsightModel {
    return {
        icons: GUEST_INSIGHT.INSIGHT_ACTIONS[action].icons,
        badgeIconPaths: [this.badgeIconPaths[insight.nextLoyaltyLevel]],
        primaryText: GUEST_INSIGHT.INSIGHT_ACTIONS[action].primaryText,
        secondaryText: null,
        enrollmentDate: null,
        firstName: guest.firstName,
        pointsToAchieve: insight.pointsToAchieve,
        nextLoyaltyLevel: this.capitalizeString(insight.nextLoyaltyLevel)
    }
  }

  private capitalizeString(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}
