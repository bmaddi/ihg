import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';
import {
  GuestInterestsResponse,
  OptionPreferencesResponse,
  OptionPreferenceResponse,
  GuestInterestsModel,
  ValuePreferenceResponse,
  GuestInterestSectionResponse
} from '@app/modules/guests/models/guest-interest.models';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { GUEST_PROFILE_PREFERENCES_OPTIONS } from '@app/modules/guests/guest-profile-preferences-options';


@Injectable({
  providedIn: 'root'
})
export class GuestInterestsService {

  private readonly API_URL = environment.fdkAPI;
  private sectionNames = GUEST_CONST.GUEST_INTERESTS.sectionNames;
  private guestInterests = GUEST_CONST.GUEST_INTERESTS;

  constructor(private http: HttpClient, private guestListService: GuestListService) { }

  public getInterests(guestMemberId: string): Observable<GuestInterestsModel[]> {
    const apiUrl = `${this.API_URL}guestProfile/interests?memberId=${guestMemberId}`;
    return this.http.get(apiUrl, { observe: 'response' }).pipe(
      map((response: HttpResponse<GuestInterestsResponse>) => this.convertInterestsResponseToInterestModels(response.body))
    );
  }

  public convertInterestsResponseToInterestModels(response: GuestInterestsResponse): GuestInterestsModel[] {
    this.guestListService.setGuestInterest(response);
    if (response && response.interests) {
      this.guestListService.guestInterestsFlag.next(true);
      return this.getAndConvertSection(this.sectionNames.guestInterests, response.interests)
        .concat(this.getAndConvertSection(this.sectionNames.dietaryPrefs, response.interests))
        .concat(this.getAndConvertSection(this.sectionNames.exercisePrefs, response.interests))
        .concat(this.getAndConvertSection(this.sectionNames.relaxationPrefs, response.interests))
        .concat(this.getAndConvertSection(this.sectionNames.guestInsights, response.interests))
        .concat(this.getAndConvertSection(this.sectionNames.travelingWithPet, response.interests));
    }
    return [];
  }

  private flatMap(modelsArr: GuestInterestsModel[][]): GuestInterestsModel[] {
    return modelsArr.reduce((acc, arr) => acc.concat(arr), []);
  }

  private getAndConvertSection(sectionName: string, sections: GuestInterestSectionResponse[]): GuestInterestsModel[] {
    return this.flatMap(sections
      .filter(section => section.description === sectionName)
      .map(section => this.convertInterestSectionToInterestModels(section))
    );
  }

  public updateSections(section:ValuePreferenceResponse[], type: string) {
    section.forEach((parent) => {
      GUEST_PROFILE_PREFERENCES_OPTIONS[type].forEach((child: ValuePreferenceResponse) => {
        parent.description = parent.key === child.key ? child.description : parent.description
      })
    })
    return section;
  };

  public updateSectionOptions(section:OptionPreferencesResponse, type: string) {
    section.options.forEach((parent) => {
      GUEST_PROFILE_PREFERENCES_OPTIONS[type].forEach((child: OptionPreferenceResponse) => {
        parent.description = parent.key === child.key ? child.description : parent.description
      })
    })
    return section;
  };

  private convertInterestSectionToInterestModels(section: GuestInterestSectionResponse): GuestInterestsModel[] {
    switch (section.description) {
      case this.sectionNames.dietaryPrefs: {
        return this.convertInterestsOptionPrefArr(section.preferences as OptionPreferencesResponse[]);
      }
      case this.sectionNames.exercisePrefs: {
        this.updateSections(section.preferences as ValuePreferenceResponse[], GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.exercise);
        return this.convertInterestsBooleanPref(section.preferences as ValuePreferenceResponse[]);
      } 
      case this.sectionNames.relaxationPrefs:{
        this.updateSections(section.preferences as ValuePreferenceResponse[], GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.relaxation);
        return this.convertInterestsBooleanPref(section.preferences as ValuePreferenceResponse[]);
      } 
      case this.sectionNames.guestInterests: {
        this.updateSections(section.preferences as ValuePreferenceResponse[], GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.interest);
        return this.convertInterestsBooleanPref(section.preferences as ValuePreferenceResponse[]);
      } 
      case this.sectionNames.guestInsights:
        return this.convertInterestsNonEmptyPref(section.preferences as ValuePreferenceResponse[], this.guestInterests.favoriteTeamKey);
      case this.sectionNames.travelingWithPet:
        return this.convertInterestsNonEmptyPref(section.preferences as ValuePreferenceResponse[], this.guestInterests.petNameKey)
          .concat(this.convertInterestsNonEmptyPref(section.preferences as ValuePreferenceResponse[], this.guestInterests.petTypeKey));
    }
    return [];
  }

  private convertInterestsOptionPrefArr(prefsArr: OptionPreferencesResponse[]): GuestInterestsModel[] {
    if (prefsArr) {
      return prefsArr
        .filter(prefs => prefs.selectedValue !== this.guestInterests.noPrefValue)
        .map(prefs => this.convertInterestsOptionPrefs(prefs))
        .reduce((acc, modelArr) => acc.concat(modelArr), []);
    }
    return [];
  }

  private convertInterestsOptionPrefs(prefs: OptionPreferencesResponse): GuestInterestsModel[] {
    this.updateSectionOptions(prefs, GUEST_PROFILE_PREFERENCES_OPTIONS.checkBoxType.dietary)
    return prefs.options
      .filter(option => option.key === prefs.selectedValue)
      .map(pref => {
        return {
          key: pref.key,
          description: pref.description,
          selectedValue: prefs.selectedValue,
          displayValue: pref.description
        };
      });
  }

  private convertInterestsBooleanPref(prefs: ValuePreferenceResponse[]): GuestInterestsModel[] {
    if (prefs) {
      return prefs
        .filter(pref => pref.selectedValue === this.guestInterests.trueValue)
        .map(pref => {
          return {
            key: pref.key,
            description: pref.description,
            selectedValue: pref.selectedValue,
            displayValue: pref.description
          };
        });
    }
    return [];
  }

  private convertInterestsNonEmptyPref(prefs: ValuePreferenceResponse[], key: string): GuestInterestsModel[] {
    if (prefs) {
      return prefs
        .filter(pref => pref.selectedValue && pref.key === key)
        .map(pref => {
          return {
            key: pref.key,
            description: pref.description,
            selectedValue: pref.selectedValue,
            displayValue: pref.selectedValue
          };
        });
    }
    return [];
  }
}
