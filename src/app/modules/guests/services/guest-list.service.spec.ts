import { TestBed } from '@angular/core/testing';

import { GuestListService } from './guest-list.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';

describe('GuestListService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [CommonTestModule]
  }));

  it('should be created', () => {
    const service: GuestListService = TestBed.get(GuestListService);
    expect(service).toBeTruthy();
  });
});
