import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { State } from '@progress/kendo-data-query';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TranslateService } from '@ngx-translate/core';

import { GuestInterestsResponse, PreferenceUpdate } from '@app/modules/guests/models/guest-interest.models';
import { DetachedToastMessageService, SessionStorageService, ToastMessageOutletService, UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { AppConstants } from '@app/constants/app-constants';
import { GuestGridModel, GuestListModel, GuestListResponse, GuestResponse, GuestTabFlag } from './../models/guest-list.models';
import {
  GuestProfileLangModel,
  GuestProfileLangResponse,
  GuestProfileLoyaltyModel,
  GuestProfileLoyaltyResponse,
  GuestProfileModel,
  GuestProfileObservationModel,
  GuestProfileObservationResponse,
  GuestProfileResponse,
  NewGuestObservation,
  ObservationListType,
  ObservationSortType,
  PreferenceResponseData,
  ValueLabelModel
} from './../models/guest-profile.model';
import { BadgeIconPaths } from '@app/constants/badge-icon-paths';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { checkForIneligibleReservation } from '../functions/guest-arrivals.functions';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

@Injectable({
  providedIn: 'root'
})
export class GuestListService {
  public static readonly allCategoriesFilterValue = 'ALL';
  public refreshInterest = new Subject<void>();
  private guestTab: Subject<GuestTabFlag> = new Subject<GuestTabFlag>();
  private readonly API_URL = environment.fdkAPI;
  private readonly guestProfileEndpoint = this.API_URL + 'guestProfile/loyaltyAndMemberInfo';
  private readonly memberIdStorageKey = 'guestProfileMemberId';
  private readonly guestProfileObservationsEndpoint = this.API_URL + 'guest/observations';
  private readonly guestProfileObservationMarkHelpfulEndpoint = this.API_URL + 'guest/observations/markHelpful';
  private readonly guestProfileFlagObservation = this.API_URL + 'guest/observations/flag';
  private keyMap = GUEST_CONST.KEYMAP;
  public refreshGridEvent = new Subject<void>();
  public guestInterestsFlag = new Subject<boolean>();
  public readonly visibilityValues = {
    myHotelOnly: 'My Hotel Only',
    allHotels: 'All Hotels'
  };
  public enableExpandView = false;
  public preferenceAndInterests: PreferenceResponseData = null;
  public guestInterest: GuestInterestsResponse = null;
  public printArrivalError = new Subject<EmitErrorModel>();
  public retryArrivalPrint = new Subject<void>();

  constructor(
    private http: HttpClient,
    private toastOutlet: ToastMessageOutletService,
    private detachedToastMessageService: DetachedToastMessageService,
    private translate: TranslateService,
    private router: Router,
    private storageService: SessionStorageService,
    public userService: UserService
  ) { }

  public userDataSource = new BehaviorSubject(null);
  public userDataSource$ = this.userDataSource.asObservable();

  public getGuestList(day?: Date, searchBy?: string, state?: State, searchColumn?: string, endDate?: Date): Observable<GuestListModel> {
    const dayParam = day ? `?day=${moment(day).format(AppConstants.DB_DT_FORMAT)}` : '';
    const searchParam = searchBy ? (searchColumn ? `&searchBy=${searchBy}&searchColumn=${searchColumn}` : `&searchBy=${searchBy}`) : '';
    const sortParam = state && state.sort ? `&sortBy=${state.sort[0].field === 'guestName' ? 'lastName,firstName' : state.sort[0].field}` : '';
    const directionParam = state && state.sort ? `&sortDirection=${state.sort[0].dir}` : '';
    const pageSizeParam = state ? `&pageSize=${state.take}` : '';
    const pageNumberParam = state ? `&page=${state.skip && state.take ? state.skip / state.take + 1 : 1}` : '';
    const endDateParam = endDate ? `&endDate=${moment(endDate).format(AppConstants.DB_DT_FORMAT)}` : '';
    const apiURl = `${this.API_URL}guest/guestList${dayParam}${searchParam}${pageSizeParam}${pageNumberParam}${sortParam}${directionParam}${endDateParam}`;

    return this.http.get(apiURl).pipe(map((response: GuestListResponse | GuestResponse[]) => {
      return this.mapToGuestListModel(response);
    }));
  }

  getGuestProfile(id: number): Observable<GuestProfileModel> {
    const apiUrl = `${this.guestProfileEndpoint}?memberId=${id}&currentLocationId=${this.userService.getCurrentLocationId()}`;
    return this.http.get(apiUrl, { observe: 'response' }).pipe(
      map((res: HttpResponse<GuestProfileResponse>) => this.mapToGuestProfileModel(res.body))
    );
  }

  getObservations(memberId: string, hotelType: ObservationListType, sortType: ObservationSortType, filterByCategory: string)
    : Observable<GuestProfileObservationModel[]> {
    const sortParam = this.getObservationSortParam(sortType);
    const params = `memberId=${memberId}&hotelType=${hotelType}${sortParam}&filterBy=${filterByCategory}`;
    const apiUrl = `${this.guestProfileObservationsEndpoint}?${params}`;
    return this.http.get(apiUrl, { observe: 'response' }).pipe(
      map((res: HttpResponse<GuestProfileObservationResponse[]>) => {
        const coalesced = res && res.body ? res.body : [];
        return coalesced.map(this.mapToGuestProfileObservationToModel);
      })
    );
  }

  getPreference() {
    const params = `memberId=${this.getCurrentMemberId()}&currentLocationId=${this.userService.getCurrentLocationId()}`;
    const apiUrl = `${this.API_URL}guestProfile/preferenceAndInterest?${params}`;
    return this.http.get(apiUrl, { observe: 'response' }).pipe(map((res: HttpResponse<PreferenceResponseData>) => {
      return (res.status === 200) ? res.body : null;
    }));
  }

  getPreferredLanguage() {
    const params = `memberId=${this.getCurrentMemberId()}&currentLocationId=${this.userService.getCurrentLocationId()}`;
    const apiUrl = `${this.API_URL}guestProfile/languagePreferences?${params}`;
    return this.http.get(apiUrl, { observe: 'response' }).pipe(map((res: HttpResponse<GuestProfileLangResponse>) => {
      return (res.status === 200) ? this.mapToGuestProfileLangModel(res.body) : null;
    }));
  }

  private getObservationSortParam(sortType: ObservationSortType) {
    switch (sortType) {
      case 'HOTEL_CODE': return '&sortByHotelCode=true&sortByHelpFullCount=false';
      case 'MOST_HELPFUL': return '&sortByHotelCode=false&sortByHelpFullCount=true';
      case 'MOST_RECENT': return '';
    }
    return '';
  }

  createObservation(newObservation: NewGuestObservation): Observable<any> {
    const apiUrl = `${this.guestProfileObservationsEndpoint}`;
    return this.http.post(apiUrl, newObservation, { observe: 'response' }
    );
  }

  showCreateObservationSuccess(guestFirstName: string): void {
    this.toastOutlet.clear();
    this.detachedToastMessageService.success(this.keyMap.successToast, this.keyMap.obsModalSuccess, null, false);
  }

  markObservationHelpful(memberId: string, commentId: string): Observable<any> {
    const apiUrl = `${this.guestProfileObservationMarkHelpfulEndpoint}?commentId=${commentId}&memberId=${memberId}`;
    return this.http.post(apiUrl, {}, { observe: 'response' }
    ).pipe(
      tap(() => {
        this.toastOutlet.clear();
        const translatedMessage = this.translate.instant(this.keyMap.obsMarkHelpfulSuccess);
        this.detachedToastMessageService.success(this.keyMap.successToast, translatedMessage, null, false);
      })
    ).pipe(
      catchError((err) => {
        this.toastOutlet.clear();
        return throwError(err);
      })
    );
  }

  flagObservation(memberId: string, commentId: string): Observable<any> {
    const apiUrl = `${this.guestProfileFlagObservation}?commentId=${commentId}&memberId=${memberId}&currentLocationId=${this.userService.getCurrentLocationId()}&APP_USER=${this.userService.getUserName()}&reviewReasonCode=ID`;
    return this.http.put(apiUrl, {}, { observe: 'response' }
    ).pipe(
      tap(() => {
        this.toastOutlet.clear();
        const translatedMessage = this.translate.instant(this.keyMap.obsModalObsFlagSuccess);
        this.detachedToastMessageService.success(this.keyMap.successToast, translatedMessage, null, false);
      })
    ).pipe(
      catchError((err) => {
        this.toastOutlet.clear();
        return throwError(err);
      })
    );
  }

  getCategories(): ValueLabelModel[] {
    return [
      {
        value: 'Food and Beverage',
        label: this.translate.instant(this.keyMap.categoryFoodAndBeverage)
      }, {
        value: 'General Info',
        label: this.translate.instant(this.keyMap.categoryGeneralInfo)
      }, {
        value: 'Hotel Amenities',
        label: this.translate.instant(this.keyMap.categoryHotelAmenities)
      }, {
        value: 'Room and Bed',
        label: this.translate.instant(this.keyMap.categoryRoomAndBed)
      },
    ];
  }

  getVisibilities(): ValueLabelModel[] {
    return [
      {
        value: this.visibilityValues.myHotelOnly,
        label: this.translate.instant(this.keyMap.visibilityMyHotelOnly)
      }, {
        value: this.visibilityValues.allHotels,
        label: this.translate.instant(this.keyMap.visibilityAllHotels)
      }
    ];
  }

  getBadgeIconPaths(badges: string[]) {
    return Object.keys(BadgeIconPaths).filter(key => {
      return badges && badges.findIndex(badge => badge === key) !== -1;
    }).map(key => BadgeIconPaths[key]);
  }

  private mapToGuestListModel(response?: GuestListResponse | GuestResponse[]): GuestListModel {
    if (response && !(response instanceof Array)) {
      const { items, localCurrentDate, totalItems } = response;
      return {
        items: items ? items.map(item => this.mapToGuestGridModel(item)) : [],
        localCurrentDate,
        localCurrentDateParsed: moment(localCurrentDate, AppConstants.DB_DT_FORMAT).toDate(),
        totalItems: totalItems
      };
    } else {
      const localDate = moment().toDate();
      return {
        items: [],
        localCurrentDateParsed: localDate,
        localCurrentDate: moment(localDate).format(AppConstants.DB_DT_FORMAT),
        totalItems: 0
      };
    }
  }

  private mapToGuestGridModel(guest: GuestResponse): GuestGridModel {
    return {
      ...guest,
      badges: this.sortBadges(guest.badges),
      badgeIconPaths: this.getBadgeIconPaths(this.sortBadges(guest.badges)),
      guestName: `${guest.lastName}, ${guest.firstName}`,
      ineligibleReservation: checkForIneligibleReservation(guest.roomCount, guest.paymentType)
    };
  }

  private sortBadges(badges: string[]): string[] {
    if (!badges) {
      return badges;
    }
    const sortResult = Object.keys(BadgeIconPaths).reduce((acc, badgeCode) => {
      if (badges.indexOf(badgeCode) !== -1) {
        acc.push(badgeCode);
      }
      return acc;
    }, []);
    return sortResult;
  }

  private mapToGuestProfileModel(guest: GuestProfileResponse): GuestProfileModel {
    if (guest !== null) {
      const guestLoyaltyModel = guest.guestLoyalty ? this.mapToGuestProfileLoyaltyModel(guest.guestLoyalty, guest.hotelCurrentDate) : null;
      const ambassadorBadges = guest.ambassadorBadge ? [guest.ambassadorBadge] : [];
      const priorityBadges = guest.priorityClubBadge ? [guest.priorityClubBadge] : [];
      const circleBadges = guestLoyaltyModel && guestLoyaltyModel.circleOrKarmaClubLevel ? [guestLoyaltyModel.circleOrKarmaClubLevel] : [];
      const employeeBadges = guestLoyaltyModel && guestLoyaltyModel.employeeClubLevel ? [guestLoyaltyModel.employeeClubLevel] : [];
      const badges = [
        ...priorityBadges,
        ...ambassadorBadges,
        ...circleBadges,
        ...employeeBadges
      ];

      return {
        ...guest,
        badgeIconPaths: this.getBadgeIconPaths(badges),
        guestLoyalty: guestLoyaltyModel,
        myHotelObservations: guest.myHotelObservations ?
          guest.myHotelObservations.map(this.mapToGuestProfileObservationToModel) : [],
        preferredLanguage: this.mapToGuestProfileLangModel(guest.preferredLanguage)
      };
    }
  }

  mapToGuestProfileLangModel(guestProfileLang: GuestProfileLangResponse): GuestProfileLangModel {
    if (!guestProfileLang) {
      return null;
    }
    const langCode = guestProfileLang.preferredLanguageCode;
    return {
      ...guestProfileLang,
      iconPath: langCode ? `assets/images/flags/${langCode.toLowerCase()}.svg` : null
    };
  }

  private mapToGuestProfileObservationToModel = (observation: GuestProfileObservationResponse): GuestProfileObservationModel => {
    const { createdTimestamp } = observation;
    const postDateParsed = createdTimestamp ? moment(createdTimestamp, 'YYYY-MM-DDTHH:mm:ss.SSS') : null;
    return {
      ...observation,
      hotelLogoPath: observation.brandLogo ? `assets/images/Logos_for_Global_Header/${observation.brandLogo}` : '',
      postDateFormatted: postDateParsed.format(AppConstants.VW_DT_FORMAT)
    };
  }

  private mapToGuestProfileLoyaltyModel(guestLoyalty: GuestProfileLoyaltyResponse, hotelCurrentDate: string): GuestProfileLoyaltyModel {
    const { enrollmentDate, pointsBalance, ambassadorExpirationDate, innerCircleClubLevel, karmaClubLevel } = guestLoyalty;
    const ambassadorExpDateParsed = ambassadorExpirationDate ? moment(ambassadorExpirationDate, AppConstants.DB_DT_FORMAT) : null;
    const hotelCurDateParsed = hotelCurrentDate ? moment(hotelCurrentDate, AppConstants.DB_DT_FORMAT) : null;
    return {
      ...guestLoyalty,
      enrollmentDateFormatted: enrollmentDate ? moment(enrollmentDate, AppConstants.DB_DT_FORMAT).format('DD MMM YYYY') : '',
      pointsBalanceFormatted: this.formatWithDigitSeparator(pointsBalance || 0),
      ambassadorExpDateFormatted: ambassadorExpDateParsed ? ambassadorExpDateParsed.format('DD MMM YYYY') : '',
      isExpiringWithin30Days: this.isExpiringWithin30Days(ambassadorExpDateParsed, hotelCurDateParsed),
      circleOrKarmaClubLevel: innerCircleClubLevel || karmaClubLevel
    };
  }

  private isExpiringWithin30Days(ambExpDate: moment.Moment, hotelCurDate: moment.Moment) {
    return ambExpDate && hotelCurDate && (ambExpDate.diff(hotelCurDate, 'days') <= 30 || ambExpDate.isBefore(hotelCurDate));
  }

  private formatWithDigitSeparator(num: number) {
    const num_parts = num.toString().split('.');
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return num_parts.join('.');
  }

  savePreferences(preferences: PreferenceUpdate[]): Observable<any> {
    const params = `memberId=${this.getCurrentMemberId()}&currentLocationId=${this.userService.getCurrentLocationId()}`;
    const apiUrl = `${this.API_URL}guestProfile/preferenceAndInterest?${params}`;
    return this.http.put(apiUrl, preferences, { observe: 'response' }
    ).pipe(
      tap(() => {
        this.toastOutlet.clear();
        const translatedMessage = this.translate.instant(this.keyMap.successfullyPreferencesSaved);
        this.detachedToastMessageService.success(this.keyMap.successToast, translatedMessage, null, false);
      })
    ).pipe(
      catchError((err) => {
        this.toastOutlet.clear();
        return throwError(err);
      })
    );
  }

  setMemberProfile(memberId: number) {
    this.storageService.setSessionStorage(this.memberIdStorageKey, memberId);
  }

  getCurrentMemberId(): number {
    return Number.parseInt(this.storageService.getSessionStorage(this.memberIdStorageKey));
  }

  emitGuestTabEvent(newData: GuestTabFlag) {
    this.guestTab.next(newData);
  }

  getGuestTabEvent() {
    return this.guestTab;
  }

  setExpandView(flag: boolean) {
    this.enableExpandView = flag;
  }

  getExpandView() {
    return this.enableExpandView;
  }

  setPreferenceResponseData(data: PreferenceResponseData) {
    this.preferenceAndInterests = data;
    this.refreshInterest.next();
  }

  getPreferenceResponseData() {
    return this.preferenceAndInterests;
  }

  setGuestInterest(data: GuestInterestsResponse) {
    this.guestInterest = data;
  }

  getGuestInterest() {
    return this.guestInterest;
  }
}
