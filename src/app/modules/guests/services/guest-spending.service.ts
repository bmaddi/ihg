import { Injectable } from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GuestHeartBeat} from '@modules/guests/models/guest-profile-heartbeat.model';
import {map} from 'rxjs/operators';
import {GuestSpendingModel} from '@modules/guests/models/guest-spending.model';

@Injectable({
  providedIn: 'root'
})
export class GuestSpendingService {

  private readonly API_URL = environment.fdkAPI;

  constructor(private http: HttpClient) {
  }

  getSpending(guestMemberId: number) {
    const apiURL = `${this.API_URL}guestProfile/spending?memberId=${guestMemberId}`;
    return this.http.get(apiURL, {observe: 'response'}).pipe(
      map((res: HttpResponse<GuestSpendingModel>) => {
        return (res.status === 200) ? res.body : null;
      })
    );
  }
}
