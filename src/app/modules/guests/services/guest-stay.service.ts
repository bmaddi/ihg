import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { AppConstants, getHotelLogoPath } from '@app/constants/app-constants';
import { environment } from '@env/environment';
import { GuestLastStayModel, GuestLastStayResponse, GuestStayModel, GuestStayResponse } from '../models/guest-stay.models';

@Injectable({
  providedIn: 'root'
})
export class GuestStayService {

  private readonly API_URL = environment.fdkAPI;

  constructor(private http: HttpClient) { }

  public getStays(guestMemberId: number): Observable<GuestStayModel> {
    const apiUrl = `${this.API_URL}guestProfile/stayHistory?memberId=${guestMemberId}`;
    return this.http.get(apiUrl).pipe(
      map((response: GuestStayResponse) => this.convertResponseToModel(response))
    );
  }

  private convertResponseToModel(response: GuestStayResponse): GuestStayModel {
    const lastStaysResponse = response && response.lastStays ? response.lastStays : [];
    return <GuestStayModel>{
      ...response,
      brandMostVisited: getHotelLogoPath(response.brandMostVisited),
      lastStays: lastStaysResponse.map(this.convertLastStayToModel),
      showAccruedPoints: this.checkValidAccruedPoints(lastStaysResponse)
    };
  }

  private checkValidAccruedPoints(stays: GuestLastStayResponse[]): boolean {
    return stays.some(e => !!e.accruedPoints);
  }

  private convertLastStayToModel = (lastStay: GuestLastStayResponse): GuestLastStayModel => {
    return {
      ...lastStay,
      checkInDateParsed: moment(lastStay.checkInDate, AppConstants.DB_DT_FORMAT).toDate()
    };
  }
}
