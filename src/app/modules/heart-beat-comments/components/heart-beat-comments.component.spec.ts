import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeartBeatCommentsComponent } from './heart-beat-comments.component';

xdescribe('HeartBeatCommentsComponent', () => {
  let component: HeartBeatCommentsComponent;
  let fixture: ComponentFixture<HeartBeatCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeartBeatCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeartBeatCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
