import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-heart-beat-comments',
  templateUrl: './heart-beat-comments.component.html'
})
export class HeartBeatCommentsComponent implements OnInit {
  public maxCommentsLimit = 999;
  public heartBeatComment: string;
  public heartBeatImprovement: string;
  public heartBeatDate: string;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.dismiss();
  }
}
