import { HeartBeatCommentsModule } from './heart-beat-comments.module';

describe('HeartBeatCommentsModule', () => {
  let heartBeatCommentsModule: HeartBeatCommentsModule;

  beforeEach(() => {
    heartBeatCommentsModule = new HeartBeatCommentsModule();
  });

  it('should create an instance', () => {
    expect(heartBeatCommentsModule).toBeTruthy();
  });
});
