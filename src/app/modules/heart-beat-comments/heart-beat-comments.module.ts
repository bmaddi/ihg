import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { HeartBeatCommentsComponent } from '@modules/heart-beat-comments/components/heart-beat-comments.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    TranslateModule,
    IhgNgCommonComponentsModule
  ],
  declarations: [
    HeartBeatCommentsComponent
  ],
  entryComponents: [
    HeartBeatCommentsComponent
  ],
  exports: [
    HeartBeatCommentsComponent
  ]
})
export class HeartBeatCommentsModule {
}
