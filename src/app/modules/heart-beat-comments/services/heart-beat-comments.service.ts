import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { HeartBeatCommentsComponent } from '@modules/heart-beat-comments/components/heart-beat-comments.component';

@Injectable({
  providedIn: 'root'
})
export class HeartBeatCommentsService {

  constructor(private modalService: NgbModal) {
  }

  public openHeartBeatCommentsModal(comment: string, improvement: string, date?: string): void {
    const activeModal = this.modalService.open(HeartBeatCommentsComponent, {
      backdrop: 'static',
      windowClass: 'large-modal'
    });
    const modalComponent = activeModal.componentInstance as HeartBeatCommentsComponent;
    modalComponent.heartBeatComment = comment;
    modalComponent.heartBeatImprovement = improvement;
    modalComponent.heartBeatDate = date;
  }
}
