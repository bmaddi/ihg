import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService, LanguageSelectorService } from 'ihg-ng-common-core';
import { DatepickerComponent } from './datepicker.component';
import {MOCK_FORMGROUP, MOCK_INPUT_OBJECT, MOCK_INVALID_DATE, MOCK_VALID_DATE} from '@modules/ihg-date-range/mocks/mock-inputs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonTestModule} from '@modules/common-test/common-test.module';
import {setTestEnvironment} from '@modules/common-test/functions/common-test.function';
import * as moment from 'moment';
import {CalendarModule} from 'ihg-ng-datepicker-common';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;
  let userService: UserService;
  let languageService: LanguageSelectorService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatepickerComponent],
      imports: [CommonTestModule, ReactiveFormsModule, FormsModule, CalendarModule, NoopAnimationsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [LanguageSelectorService, UserService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    userService = TestBed.get(UserService);
    languageService = TestBed.get(LanguageSelectorService);
    spyOn(languageService, 'getSelectedLanguage').and.returnValue('en');

    fixture = TestBed.createComponent(DatepickerComponent);
    component = fixture.componentInstance;
    component.parentForm = MOCK_FORMGROUP;
    component.inputConfigObj = MOCK_INPUT_OBJECT;
    fixture.detectChanges();
  });

  it('should initialize properly', () => {
    expect(component).toBeTruthy();
    expect(moment(component.todayDate).format(component.calendarFormat)).toEqual('2002-02-02');
    expect(moment(component.firstDay).format(component.calendarFormat)).toEqual('2002-01-02');
    expect(moment(component.lastDay).format(component.calendarFormat)).toEqual('2002-03-05');
  });

  it ('should switch focus only from arrival field', () => {
    spyOn(component, 'onFocus');

    component.focusField = null;
    component.switchFocus();
    expect(component.onFocus).not.toHaveBeenCalled();

    component.focusField = 'arrival';
    component.switchFocus();
    expect(component.onFocus).toHaveBeenCalledTimes(1);
    expect(component.onFocus).toHaveBeenCalledWith('departure');

    component.focusField = 'departure';
    component.switchFocus();
    expect(component.onFocus).toHaveBeenCalledTimes(1);
  });

  // calendarDateSelect
  it('should focus when clicked', () => {
    const mockElement = document.createElement('div');
    mockElement.focus = () => {};

    spyOn(mockElement, 'focus');
    spyOn(document, 'getElementById').and.returnValue(mockElement);

    component.onCalendarClick('elementString');
    expect(mockElement.focus).toHaveBeenCalled();
  });

  // calendarDateSelect
  it('should do the correct stuff when a calendar date is selected but there is no focus', () => {
    spyOn(component, 'switchFocus');
    spyOn(component.parentForm.get('arrivalDate'), 'setValue');
    spyOn(component.parentForm.get('departureDate'), 'setValue');
    spyOn(component, 'isValidArrivalDate');
    spyOn(component, 'isValidDepartureDate');

    component.focusField = null;

    const d =  moment(MOCK_VALID_DATE, component.calendarFormat);

    component.calendarDateSelect(d);

    expect(component.isValidArrivalDate).not.toHaveBeenCalled();
    expect(component.isValidDepartureDate).not.toHaveBeenCalled();
    expect(component.parentForm.get('arrivalDate').setValue).not.toHaveBeenCalled();
    expect(component.parentForm.get('departureDate').setValue).not.toHaveBeenCalled();
  });

  it('Should properly set the arrival field with a valid date', () => {
    spyOn(component, 'switchFocus');
    spyOn(component.parentForm.get('arrivalDate'), 'setValue');
    spyOn(component, 'isValidArrivalDate').and.returnValue(true);
    spyOn(component, 'isValidDepartureDate');

    component.focusField = 'arrival';

    const d =  moment(MOCK_VALID_DATE, component.calendarFormat);

    component.calendarDateSelect(d);

    expect(component.isValidArrivalDate).toHaveBeenCalledWith(d);
    expect(component.isValidDepartureDate).not.toHaveBeenCalled();
    expect(component.parentForm.get('arrivalDate').setValue).toHaveBeenCalledWith('07FEB2002', {emitEvent: false});
  });

  it('Should not set the arrival field with an invalid date', () => {
    spyOn(component, 'switchFocus');
    spyOn(component.parentForm.get('arrivalDate'), 'setValue');
    spyOn(component, 'isValidArrivalDate');
    spyOn(component, 'isValidDepartureDate');

    component.focusField = 'arrival';

    const d =  moment(MOCK_INVALID_DATE, component.calendarFormat);

    component.calendarDateSelect(d);

    expect(component.isValidArrivalDate).toHaveBeenCalledWith(d);
    expect(component.isValidDepartureDate).not.toHaveBeenCalled();
    expect(component.parentForm.get('arrivalDate').setValue).not.toHaveBeenCalled();
  });

  it('Should retain focus when departure date is set and closeOnDepartureClick is false', () => {
    spyOn(component, 'switchFocus');
    spyOn(component.parentForm.get('departureDate'), 'setValue');
    spyOn(component, 'isValidArrivalDate');
    spyOn(component, 'isValidDepartureDate');

    component.focusField = 'departure';
    component.hasFocus = true;
    component.inputConfigObj.closeOnDepartureClick = false;

    const d =  moment(MOCK_VALID_DATE, component.calendarFormat);

    component.calendarDateSelect(d);

    expect(component.hasFocus).toBe(true);
    expect(component.isValidDepartureDate).toHaveBeenCalledWith(d);
    expect(component.isValidArrivalDate).not.toHaveBeenCalled();
    expect(component.parentForm.get('departureDate').setValue).not.toHaveBeenCalled();
  });

  it('Should not retain focus when departure date is set and closeOnDepartureClick is true', () => {
    spyOn(component, 'switchFocus');
    spyOn(component.parentForm.get('departureDate'), 'setValue');
    spyOn(component, 'isValidArrivalDate');
    spyOn(component, 'isValidDepartureDate').and.returnValue(true);

    component.focusField = 'departure';
    component.hasFocus = true;
    component.inputConfigObj.closeOnDepartureClick = true;

    const d =  moment(MOCK_VALID_DATE, component.calendarFormat);

    component.calendarDateSelect(d);

    expect(component.hasFocus).toBe(false);
    expect(component.isValidDepartureDate).toHaveBeenCalledWith(d);
    expect(component.isValidArrivalDate).not.toHaveBeenCalled();
    expect(component.parentForm.get('departureDate').setValue).toHaveBeenCalledWith('07FEB2002', {emitEvent: false});
  });

  it('Should not set the departure field with an invalid date', () => {
    spyOn(component, 'switchFocus');
    spyOn(component.parentForm.get('departureDate'), 'setValue');
    spyOn(component, 'isValidArrivalDate');
    spyOn(component, 'isValidDepartureDate');

    component.focusField = 'departure';

    const d =  moment(MOCK_INVALID_DATE, component.calendarFormat);

    component.calendarDateSelect(d);

    expect(component.isValidDepartureDate).toHaveBeenCalledWith(d);
    expect(component.isValidArrivalDate).not.toHaveBeenCalled();
    expect(component.parentForm.get('departureDate').setValue).not.toHaveBeenCalled();
  });

  it('should know if a date is disabled for arrival', () => {
    component.focusField = 'arrival';
    const d1 = moment(MOCK_VALID_DATE, component.calendarFormat);
    const d2 = moment(MOCK_INVALID_DATE, component.calendarFormat);
    const d3 = {year: d1.format('YYYY'), month: d1.format('MM'), day: d1.format('DD')};
    const d4 = {year: d2.format('YYYY'), month: d2.format('MM'), day: d2.format('DD')};
    expect(component.isDisabled(d3)).toBe(false);
    expect(component.isDisabled(d4)).toBe(true);
  });

  it('should know if a date is disabled for departure', () => {
    component.focusField = 'departure';
    const d1 = moment(MOCK_VALID_DATE, component.calendarFormat);
    const d2 = moment(MOCK_INVALID_DATE, component.calendarFormat);
    const d3 = {year: d1.format('YYYY'), month: d1.format('MM'), day: d1.format('DD')};
    const d4 = {year: d2.format('YYYY'), month: d2.format('MM'), day: d2.format('DD')};
    expect(component.isDisabled(d3)).toBe(false);
    expect(component.isDisabled(d4)).toBe(true);
  });

  it('should update the monthShown when the month is changed', () => {
    spyOn(component, 'updateMonthShown').and.callThrough();
    expect(component.firstShownCalendar).toEqual({month: 4, year: 2002});

    component.onMonthChange({month: 8, year: 1999});
    expect(component.updateMonthShown).toHaveBeenCalledWith(8, 1999);
    expect(component.firstShownCalendar).toEqual({month: 8, year: 1999});
  });

  it('should know if it has hit the minimum displayable month', () => {
    component.firstShownCalendar = {year: 1, month: 2};
    component.minimumCalendar = {year: 2, month: 4};
    expect(component.isMinimumMonth()).toBe(true);

    component.firstShownCalendar = {year: 2, month: 2};
    expect(component.isMinimumMonth()).toBe(true);

    component.firstShownCalendar = {year: 2, month: 4};
    expect(component.isMinimumMonth()).toBe(true);

    component.firstShownCalendar = {year: 2, month: 5};
    expect(component.isMinimumMonth()).toBe(false);

    component.firstShownCalendar = {year: 3, month: 5};
    expect(component.isMinimumMonth()).toBe(false);
  });

  it('should know if it has hit the maximum displayable month', () => {
    component.firstShownCalendar = {year: 3, month: 2};
    component.maximumCalendar = {year: 2, month: 4};
    expect(component.isMaximumMonth()).toBe(false);

    component.firstShownCalendar = {year: 2, month: 5};
    expect(component.isMaximumMonth()).toBe(false);

    component.firstShownCalendar = {year: 2, month: 4};
    expect(component.isMaximumMonth()).toBe(true);

    component.firstShownCalendar = {year: 2, month: 2};
    expect(component.isMaximumMonth()).toBe(true);

    component.firstShownCalendar = {year: 1, month: 2};
    expect(component.isMaximumMonth()).toBe(true);
  });
});
