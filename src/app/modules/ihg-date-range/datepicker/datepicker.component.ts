import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {ConfigDates} from '@modules/ihg-date-range/models/datepicker-config.model';
import {calendarLocale} from '@modules/ihg-date-range/constants/calendar-locale.constant';
import { UserService, LanguageSelectorService } from 'ihg-ng-common-core';
import { Moment } from 'moment';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DatepickerComponent implements OnInit {
  @Input() parentForm: FormGroup;
  @Input() inputConfigObj: ConfigDates;
  @Output() focused = new EventEmitter<boolean>();
  @Output() changeDatePickerValue = new EventEmitter<any>();
  @Output() onCloseCalendar = new EventEmitter<boolean>();

  calendarFormat = 'YYYY-MM-DD';
  hasFocus = false;
  focusField = null;
  locale;
  calendarExclusionArray = ['ui-ihg-datepicker-clickable-date', 'ui-ihg-datepicker-next', 'ui-ihg-datepicker-prev', 'arrivalDate', 'departureDate'];
  numberOfMonths: number; // Number of months shown at a time
  firstShownCalendar;
  minimumCalendar;
  maximumCalendar;
  todayDate;
  firstDay;
  lastDay;
  defaultCalendarDate: Date;
  clickedDate;

  constructor(private languageService: LanguageSelectorService,
              private userService: UserService) { }

  ngOnInit() {
    this.numberOfMonths = this.inputConfigObj.numberOfMonths || 2;
    this.todayDate = this.getToday();
    this.lastDay = moment(this.todayDate).startOf('day').add(this.inputConfigObj.maxDays, 'day');
    this.firstDay = moment(this.todayDate).startOf('day').subtract(this.inputConfigObj.minDays, 'day');

    this.inputConfigObj.closeOnDepartureClick = true;
    this.numberOfMonths = this.inputConfigObj.numberOfMonths || this.numberOfMonths;

    // localization options which can be improved
    this.locale = calendarLocale[this.languageService.getSelectedLanguage()];
    this.parentForm.get('dateFormat').setValue(this.locale.angularDateFormat, {emitEvent: false});

    const maxCalDate = moment(this.lastDay).startOf('day').format(this.calendarFormat);
    const minCalDate = moment(this.firstDay).startOf('day').format(this.calendarFormat);

    this.maximumCalendar = {month: moment(maxCalDate).add(1, 'month').startOf('day').format('MM'),
                            year: moment(maxCalDate).startOf('day').year()};
    this.minimumCalendar = {month: moment(minCalDate).startOf('day').format('MM'), year: moment(minCalDate).startOf('day').year()};

    // Current implementation of the packaged calendar object uses Date
    this.defaultCalendarDate = new Date(moment(this.firstDay).startOf('day').format(this.calendarFormat));
    this.initFirstShownMonth();
  }

  initFirstShownMonth() {
    const first = this.maximumCalendar.month === parseInt(this.todayDate.format('M'), 10) ? this.minimumCalendar : this.maximumCalendar;
    this.updateMonthShown(first.month, first.year);
  }

  getToday(): Moment {
    const date = this.inputConfigObj.hotelDate || this.userService.getCurrentLocation().hotelCurrentDate;
    return date ? moment(date).startOf('day') : moment();
  }

  onOutsideClick(e) {
    if (e && e.target && e.target.className && typeof e.target.className === 'string') {
      if (!e.relatedTarget) {
        if (!!this.calendarExclusionArray.filter(c => c === e.target.className)) {
          this.hasFocus = false;
          this.focusField = null;
        }
      }
    }
  }

  switchFocus() {
    if (this.focusField === 'arrival') {
      this.onFocus('departure');
    } else if (this.focusField === 'departure') {
      if (this.parentForm.get('arrivalDate').value) {
        this.changeDatePickerValue.emit();
      }
    }
  }

  onCalendarClick(elementString) { // the calendar icon
    document.getElementById(elementString).focus();
  }

  // event from the calendar
  calendarDateSelect(date) {
    if (this.focusField === 'arrival') {
      if (this.isValidArrivalDate(date)) {
        this.parentForm.get('arrivalDate').setValue(moment(date).format(this.locale.angularDateFormat).toUpperCase(), {emitEvent: false});
        this.switchFocus();
      }
    } else if (this.focusField === 'departure') {
      if (this.isValidDepartureDate(date)) {
        this.parentForm.get('departureDate').setValue(moment(date).format(this.locale.angularDateFormat).toUpperCase(), {emitEvent: false});
        this.switchFocus();
        if (this.inputConfigObj.closeOnDepartureClick) {
          this.hasFocus = false;
        }
      }
    }
  }

  isValidArrivalDate = (date): boolean => !moment(date).isBefore(this.firstDay) && !moment(date).isAfter(this.lastDay);

  isValidDepartureDate = (date): boolean => this.getArrivalDate() && !moment(date).isBefore(this.getArrivalMoment()) &&
    !moment(date).isBefore(this.firstDay) && !moment(date).isAfter(this.lastDay);

  onFocus(focusType) {
    if (focusType === 'arrival') {
      this.focusField = 'arrival';
      this.hasFocus = true;
      this.focused.emit(this.hasFocus);
    } else if (focusType === 'departure' && this.getArrivalDate()) {
      this.focusField = 'departure';
      this.hasFocus = true;
      this.focused.emit(this.hasFocus);
    }
  }

  onMonthChange(e) {
    this.updateMonthShown(e.month, e.year);
  }

  // currently only updates the data attribute
  updateMonthShown(month, year) {
    this.firstShownCalendar = {month: parseInt(month, 10), year: parseInt(year, 10)};
  }

  getArrivalDate = (): string => this.parentForm.get('arrivalDate').value;

  getArrivalMoment = (): Moment => moment(this.getArrivalDate(), this.locale.angularDateFormat);

  getDepartureDate = (): string => this.parentForm.get('departureDate').value;

  getDepartureMoment = (): Moment => moment(this.getDepartureDate(), this.locale.angularDateFormat);

  isMinimumMonth = (): boolean => this.isMinMaxMonth(this.minimumCalendar, this.firstShownCalendar);

  isMaximumMonth = (): boolean => this.isMinMaxMonth(this.maximumCalendar, this.firstShownCalendar);

  isMinMaxMonth = (m1, m2): boolean => m1.year > m2.year || m1.year === m2.year ? m1.month >= m2.month : false;

  isArrival = (date): boolean => this.getArrivalDate() ? date.isSame(this.getArrivalMoment()) : false;

  isDeparture = (date): boolean => this.getDepartureDate() ? date.isSame(this.getDepartureMoment()) : false;

  isWithinSelectedRange = (date): boolean => date.isBefore(this.getDepartureMoment()) && date.isAfter(this.getArrivalMoment());

  isOutsideConfigRange = (date): boolean => this.toMoment(date).isBefore(this.firstDay) || this.toMoment(date).isAfter(this.lastDay);

  isDisabled = (date): boolean => this.focusField === 'arrival' ? this.isOutsideConfigRange(date) : this.isOutsideConfigRange(date) ||
    (!!this.parentForm.get('arrivalDate').value && this.toMoment(date).isBefore(this.getArrivalMoment()));

  isSelected = (d): boolean => {
    const date = moment(d).startOf('day');
    const x1 = this.isArrival(date);
    const x2 = this.isDeparture(date);
    const x3 = this.getArrivalDate() && this.getDepartureDate() ? this.isWithinSelectedRange(date) : false;

    return x1 || x2 || x3;
  };

  echoDateString = (date): string => date.year + '-' + (date.month + 1) + '-' + date.day;

  toMoment = (date): Moment => moment(date.year + '-' + (date.month + 1) + '-' + date.day, this.calendarFormat).startOf('day');

  isToday = (date): boolean => this.toMoment(date) === this.todayDate;
}
