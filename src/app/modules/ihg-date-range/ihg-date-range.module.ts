import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CalendarModule} from 'ihg-ng-datepicker-common';
import {TranslateModule} from '@ngx-translate/core';
import {DatepickerComponent} from '@modules/ihg-date-range/datepicker/datepicker.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CalendarModule,
    FormsModule,
    TranslateModule,
  ],
  declarations: [
    DatepickerComponent],
  exports: [
    DatepickerComponent
  ]
})
export class IhgDateRangeModule {

}
