export const MOCK = {
  'pageNumber': 1,
  'pageSize': 10,
  'totalPages': 1,
  'totalItems': 9,
  'startItem': 1,
  'endItem': 9,
  'items': [
    {
      'firstName': 'WONDER',
      'lastName': 'BREAD',
      'badges': [],
      'loyaltyId': '',
      'deparatureDate': '2019-12-20',
      'groupName': '',
      'roomType': '',
      'checkInDate': '2019-12-19',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': '',
      'roomNumber': '',
      'companyName': '',
      'roomStatus': '',
      'paymentType': 'OTHER',
      'roomCount': 1,
      'reservationNumber': '43530617'
    },
    {
      'firstName': 'WONDER',
      'lastName': 'BREAD',
      'badges': [],
      'loyaltyId': '',
      'deparatureDate': '2019-12-20',
      'groupName': '',
      'roomType': '',
      'checkInDate': '2019-12-19',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': '',
      'roomNumber': '',
      'companyName': '',
      'roomStatus': '',
      'paymentType': 'OTHER',
      'roomCount': 1,
      'reservationNumber': '25715051'
    }
  ],
  'localCurrentDate': '2019-12-18'
};
