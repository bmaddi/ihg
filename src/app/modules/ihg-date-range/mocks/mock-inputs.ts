import {ConfigDates} from '@modules/ihg-date-range/models/datepicker-config.model';
import {FormControl, FormGroup} from '@angular/forms';

export const MOCK_INPUT_OBJECT: ConfigDates = {
  hotelDate: '2002-02-02',
  minDays: 31,
  maxDays: 31,
  numberOfMonths: 2,
  closeOnDepartureClick: true
};

export const MOCK_FORMGROUP = new FormGroup({
  arrivalDate: new FormControl(),
  departureDate: new FormControl(),
  dateFormat: new FormControl(),
});

export const MOCK_VALID_DATE = '2002-02-07';

export const MOCK_INVALID_DATE = '2012-11-23';
