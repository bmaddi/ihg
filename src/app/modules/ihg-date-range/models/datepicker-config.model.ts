export interface ConfigDates {
  hotelDate: string;
  disabledWeekdays?: Array<number>; // array of weekdays [0,1,2,3,4,5,6]
  minDays?: number;
  maxDays?: number;
  numberOfMonths?: number;
  closeOnDepartureClick: boolean;
}
