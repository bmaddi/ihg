import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { InHouseGridPagerComponent } from './in-house-grid-pager.component';
import { NO_ERRORS_SCHEMA, Component, Input, Output, EventEmitter, Pipe, PipeTransform } from '@angular/core';
import { MOCK_IN_HOUSE_RESPONSE } from '../../mocks/in-house-mock';
import { LocalizationService } from '@progress/kendo-angular-l10n';
import { PagerContextService, GridComponent } from '@progress/kendo-angular-grid';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { CommonTestModule } from '@app/modules/common-test/common-test.module';
import { IhgNgCommonCoreModule, EnvironmentService } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { environmentServiceServiceStub } from '@modules/guest-relations/mocks/guest-relations.mock';

@Component({
  selector: 'app-print-grid',
  template: '',
})
class MockPrintComponent {
  @Input() menuItems;
  @Input() disabled;
  @Output() print: EventEmitter<any> = new EventEmitter();
}

@Pipe({ name: 'translate' })
export class TranslatePipe implements PipeTransform {
  transform(value: string): string {
    return value;
  }
}

xdescribe('InHouseGridPagerComponent', () => {
  let component: InHouseGridPagerComponent;
  let fixture: ComponentFixture<InHouseGridPagerComponent>;
  let translateService: TranslateService;
  const localizationServiceStub = {};
  const PagerContextServiceStub = {};
  const GridComponentServiceStub = {};

  beforeEach(async(() => {
    TestBed.overrideProvider(LocalizationService, {
      useValue: localizationServiceStub
    });
    TestBed.overrideProvider(PagerContextService, {
      useValue: PagerContextServiceStub
    });
    TestBed.overrideProvider(GridComponent, {
      useValue: GridComponentServiceStub
    });

    TestBed.overrideProvider(EnvironmentService, {
      useValue: environmentServiceServiceStub
    });

    TestBed.configureTestingModule({
      declarations: [InHouseGridPagerComponent,
        MockPrintComponent, TranslatePipe],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [IhgNgCommonKendoModule, RouterTestingModule, CommonTestModule, IhgNgCommonCoreModule,
        IhgNgCommonComponentsModule],
      providers: [LocalizationService, PagerContextService, GridComponent]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InHouseGridPagerComponent);
    component = fixture.componentInstance;
    component.gridView = MOCK_IN_HOUSE_RESPONSE;
    component.settings = MOCK_IN_HOUSE_RESPONSE;
    translateService = TestBed.get(TranslateService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
