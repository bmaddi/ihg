import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PagerInfoComponent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-in-house-grid-pager',
  templateUrl: './in-house-grid-pager.component.html',
  styleUrls: ['./in-house-grid-pager.component.scss']
})
export class InHouseGridPagerComponent extends PagerInfoComponent implements OnInit {

  @Input() gridView;
  @Input() settings;
  @Input() printMenuItems: { label: string, value: string }[];

  @Output() selectPageSize: EventEmitter<any> = new EventEmitter();
  @Output() print = new EventEmitter<string>();

  selectedPageSize(value: any): void {
    this.selectPageSize.emit(value);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public handlePrint(printAction: string): void {
    this.print.emit(printAction);
  }
}
