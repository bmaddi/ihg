import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InHouseListComponent } from './in-house-list.component';
import {NO_ERRORS_SCHEMA, DebugElement} from '@angular/core';
import {CommonTestModule} from '@modules/common-test/common-test.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ConfirmationModalService} from 'ihg-ng-common-components';
import {EnvironmentService, GoogleAnalyticsService} from 'ihg-ng-common-core';
import {GridDataResult, GridModule} from '@progress/kendo-angular-grid';
import {IhgNgCommonKendoModule} from 'ihg-ng-common-kendo';
import {
  MOCK_IN_HOUSE_RESPONSE, MOCK_IN_HOUSE_ARRAY, MOCK_IN_HOUSE_PARAMS,
  MOCK_IN_HOUSE_PARAMS_2, DEFAULT_GUEST_NAME
} from '@modules/in-house/mocks/in-house-mock';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {setTestEnvironment} from '@modules/common-test/functions/common-test.function';
import {By} from '@angular/platform-browser';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

describe('InHouseListComponent', () => {
  let component: InHouseListComponent;
  let fixture: ComponentFixture<InHouseListComponent>;
  let translateService: TranslateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InHouseListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonModule, NgbModule, CommonTestModule, GridModule, IhgNgCommonKendoModule],
      providers: [TranslateService, EnvironmentService, ConfirmationModalService,
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(InHouseListComponent);
    component = fixture.componentInstance;
    component.guestData = MOCK_IN_HOUSE_RESPONSE;
    translateService = TestBed.get(TranslateService);
    fixture.detectChanges();
  });

  it('should init successfully', () => {
    expect(component).toBeTruthy();
    expect(component.isTabletPortraitView).toBe(false);
    expect(component.isDesktopView).toBe(true);
    expect(component.guestNameWidth).toBe(204);
    expect(component.profileWidth).toBe(36);
    expect(component.groupWidth).toBe(210);
    expect(component.nightsWidth).toBe(90);
    expect(component.departureWidth).toBe(120);
    expect(component.roomWidth).toBe(120);
    expect(component.statusWidth).toBe(150);
    expect(component.actionsWidth).toBe(125);
    expect(component.activeTab).toBe('Guest List - In House');
    expect(component.rowAmount).toBe(10);
    expect(component.inHouseParams).toEqual({
      searchCriteria: '',
      rowCount: 10
    });
    expect(component.gridSettings).toEqual({
      rows: {
        allOption: true,
        title: 'COM_LBL_ROWS',
        values: [10, 25, 50, 100],
        default: 10
      }
    });
  });

  it('should load the grid data', () => {
    expect(component.gridView).toBe(undefined);

    component.gridView = {} as GridDataResult;
    component.applicableItems = MOCK_IN_HOUSE_ARRAY;
    component.totalItems = 11;
    component.loadGridData();

    expect(component.gridView).not.toBe(undefined);
    expect(component.gridView.data).toEqual(MOCK_IN_HOUSE_ARRAY);
    expect(component.gridView.total).toEqual(11);
  });

  it('should successfully get the inHouseParams', () => {
    expect(component.rowAmount).toEqual(10);

    component.inHouseParams = MOCK_IN_HOUSE_PARAMS;
    component.checkInHouseSessionStorage();
    expect(component.rowAmount).toEqual(3);

    component.inHouseParams = MOCK_IN_HOUSE_PARAMS_2;
    component.checkInHouseSessionStorage();
    expect(component.rowAmount).toEqual(10);

    component.inHouseParams = null;
    component.checkInHouseSessionStorage();
    expect(component.rowAmount).toEqual(10);
  });

  it('should return the full guest name', () => {
    expect(component.getGuestName(DEFAULT_GUEST_NAME)).toEqual('Ever, Greatest');
    expect(component.getGuestName(null)).toEqual(null);
    expect(component.getGuestName('adfgdfgsfg')).toEqual(null);
    expect(component.getGuestName(346465456)).toEqual(null);
  });

  it('should check if page shows correct action on clicking dropdown for guest or room', () => {
    const actionVal = fixture.debugElement.query(By.css('.manage-stay-link'));
    expect(actionVal.nativeElement.textContent).toContain('LBL_MANAGE_STAY');
  });

  it('should check if page size is set when pageSizeUpdate is called ', () => {
    component.ga = GUEST_CONST.GA;
    component.pageSizeUpdate(2);
    expect(component.inHouseParams.rowCount).toBe(2);
  });
});
