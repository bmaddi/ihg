import {
  Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { TitleCasePipe } from '@angular/common';
import { Observable, Subscription } from 'rxjs';
import * as moment from 'moment';
import { SortDescriptor, State } from '@progress/kendo-data-query';
import { GridComponent, GridDataResult } from '@progress/kendo-angular-grid';

import { GridSettings } from 'ihg-ng-common-kendo';
import { GoogleAnalyticsService, SessionStorageService, useDefault } from 'ihg-ng-common-core';
import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';

import { GUEST_CONST } from '@modules/guests/guests.constants';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { GuestInHouseParams, GuestGridModel } from '@modules/guests/models/guest-list.models';
import { UtilityService } from '@services/utility/utility.service';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ServiceErrorComponentConfig } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { InHouseGuestModel, InHouseResponse } from '@modules/in-house/models/in-house-guest.model';
import { InHouseService } from '@modules/in-house/services/in-house.service';
import { TranslateService } from '@ngx-translate/core';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';
import { IN_HOUSE_GUEST_LIST_AUTOFILL } from '@app/constants/error-autofill-constants';
import { printGridItems } from '@app/modules/print-record/constants/print-grid.constant';

@Component({
  selector: 'app-in-house-list',
  templateUrl: './in-house-list.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./in-house-list.component.scss']
})

export class InHouseListComponent implements OnInit, OnChanges {

  @Input() gridState: State;
  @Input() guestDataErrorEvent: Observable<EmitErrorModel>;
  @Input() isSearchResult: boolean;
  @Input() dateIndex;
  @Input() totalItems;
  @Input() guestData: InHouseResponse;
  @Input() inHouseParams: GuestInHouseParams;

  @ViewChild(GridComponent) grid: GridComponent;
  @Output() refreshAction = new EventEmitter();
  @Output() stateChange: EventEmitter<State> = new EventEmitter<State>();
  @Output() print = new EventEmitter<{ printAction: string, items: GuestGridModel[] }>();

  /** Table cell width section **/
  guestNameWidth: number;
  profileWidth: number;
  loyaltyWidth = 105;
  groupWidth: number;
  nightsWidth: number;
  departureWidth: number;
  roomWidth: number;
  statusWidth: number;
  actionsWidth: number;
  /** End width section **/

  keyMap = GUEST_CONST.KEYMAP;
  gridSettings: GridSettings = null;
  gridView: GridDataResult;
  isDesktopView: boolean;
  isTabletPortraitView: boolean;
  activeTab: string;
  guestNameCharLimit = 21;
  groupCharacterLimit = 23;
  noDataSymbol = '—';
  rootSubscription = new Subscription();
  selectedGuests = [];
  applicableItems: InHouseGuestModel[] = [];
  rowAmount: number | string;
  public printMenuItems = printGridItems.slice(0);

  gridPagerTranslations = {
    gridPagerInfo: {
      itemsText: this.keyMap.guests,
      noItemsText: this.keyMap.noGuestsToDisplay
    }
  };

  reportIssueConfig: ReportIssueAutoFillObject = IN_HOUSE_GUEST_LIST_AUTOFILL;

  errorConfig: ServiceErrorComponentConfig = {
    noData: {
      header: this.keyMap.noGuestsInHouse,
      message: this.keyMap.noGuestsInToday
    }
  };
  searchErrorConfig: ServiceErrorComponentConfig = {
    noData: {
      header: this.keyMap.noSearchResultsTitle,
      message: this.keyMap.noSearchResultsText
    }
  };

  ga = GUEST_CONST.GA;

  constructor(private router: Router,
    public utility: UtilityService,
    private titleCasePipe: TitleCasePipe,
    private gaService: GoogleAnalyticsService,
    private guestListService: GuestListService,
    private inHouseService: InHouseService,
    private storageService: SessionStorageService,
    private translate: TranslateService,
  ) { }


  /* @HostListener('window:resize', ['$event'])
   getScreenSize(event?) {
     if (window.outerWidth < 769) {
       this.isTabletPortraitView = true;
       this.isDesktopView = false;
       this.guestNameWidth = 168;
       this.groupWidth = 128;
       this.nightsWidth = 68;
       this.departureWidth = 0;
       this.roomWidth = 61;
       this.statusWidth = 63;
       this.actionsWidth = 92;
     } else if (window.outerWidth < 1025) {
       this.isTabletPortraitView = false;
       this.isDesktopView = false;
       this.guestNameWidth = 190;
       this.groupWidth = 173;
       this.nightsWidth = 92;
       this.departureWidth = 98;
       this.roomWidth = 95;
       this.statusWidth = 68;
       this.actionsWidth = 106;
     } else {
       this.isTabletPortraitView = false;
       this.isDesktopView = true;
       this.guestNameWidth = 170;
       this.groupWidth = 155;
       this.nightsWidth = 70;
       this.departureWidth = 140;
       this.roomWidth = 100;
       this.statusWidth = 150;
       this.actionsWidth = 110;
     }
   }*/

  ngOnInit() {
    this.getScreenSize();
    this.activeTab = this.ga.CATEGORY + this.ga.SUB_CATEGORY.INHOUSETAB;
    this.checkInHouseSessionStorage();
    this.setGridSettings();
  }

  checkInHouseSessionStorage() {
    if (this.inHouseParams) {
      this.rowAmount = useDefault(this.inHouseParams.rowCount, 10);
    } else {
      this.rowAmount = 10;
      this.inHouseParams = {
        searchCriteria: '',
        rowCount: this.rowAmount
      };
    }
  }

  getScreenSize() {
    this.isTabletPortraitView = false;
    this.isDesktopView = true;
    this.guestNameWidth = 204;
    this.profileWidth = 36;
    this.groupWidth = 210;
    this.nightsWidth = 90;
    this.departureWidth = 120;
    this.roomWidth = 120;
    this.statusWidth = 150;
    this.actionsWidth = 125;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.removeKendoInlineStyles();
    if (changes.guestData && changes.guestData.currentValue) {
      this.setApplicableItems(changes.guestData.currentValue);
    }
  }

  removeKendoInlineStyles() {
    const element = document.querySelector('.in-house-list .k-grid-header');
    if (!!element) {
      element.removeAttribute('style');
    }
  }

  setApplicableItems(items: InHouseGuestModel[]): void {
    // renew when current items are empty and total count > 0
    if (!items.length && this.totalItems) {
      this.renew();
    } else {
      this.applicableItems = items;
      this.loadGridData();
    }
  }

  loadGridData(): void {
    this.gridView = {
      data: this.applicableItems,
      total: this.totalItems
    };
  }

  pageSizeUpdate(val) {
    this.inHouseParams.rowCount = val;
    this.storageService.setSessionStorage('guestInHouseParams', this.inHouseParams);
    this.gaService.trackEvent(this.activeTab, this.ga.GEAR_SELECTED, this.ga[`ROWS_PER_PAGE_${val}`]);
    // Reset page no and sorting while page size changes
    if (this.gridState) {
      this.gridState.sort = null;
      this.gridState.skip = 0;
    }
  }

  navigateToStayManagementPage(data: InHouseGuestModel): void {
    this.gaService.trackEvent(this.activeTab, this.ga.MANAGE_STAY, this.ga.SELECTED);
    this.navaigateToManageStay(data);
  }

  navaigateToManageStay(data: InHouseGuestModel) {
    this.inHouseParams.rowCount = this.gridSettings.rows.default;
    this.storageService.setSessionStorage('manageStayParams',
      {
        reservationNumber: data.holidexNumber,
        pmsNumber: data.pmsNumber,
        inHouseParams: this.inHouseParams
      });
    this.router.navigate(['/manage-stay']);
  }

  setMemberProfile(loyaltyId: string) {
    this.gaService.trackEvent(this.activeTab, this.ga.GUEST_PROFILE, this.ga.SELECTED);
    this.guestListService.setMemberProfile(+loyaltyId);
  }

  getGuestName(guestData) {
    return (guestData && guestData.lastName && guestData.firstName) ? guestData.lastName + ', ' + guestData.firstName : null;
  }

  public transformData(dataParam: string): string {
    return this.titleCasePipe.transform(dataParam);
  }

  public displayToolTip(data: string): boolean {
    return data.length > this.groupCharacterLimit;
  }

  formatDate(date: string) {
    return moment(date).format('DDMMMYYYY').toUpperCase();
  }

  onGuestNameClick(data: InHouseGuestModel): void {
    this.gaService.trackEvent(this.activeTab, this.ga.GUEST_NAME, this.ga.SELECTED);
    this.navaigateToManageStay(data);
  }

  onDataStateChange(event): void {
    this.stateChange.emit(event);
  }

  onSortChange(sort: SortDescriptor[]): void {
    const fieldSortBy = this.translate.instant('LBL_GST_SORT_BY') + ' ' + this.ga[sort[0].field];
    this.gaService.trackEvent(this.activeTab, this.ga.SORT, fieldSortBy);
  }

  onReportIssueClicked() {
    this.gaService.trackEvent(this.activeTab, this.ga.REPORT_ISSUE, this.ga.SELECTED);
  }

  onRefresh(error) {
    switch (error) {
      case 0:
        this.gaService.trackEvent(this.activeTab, this.ga.DATA_TIMEOUT, this.ga.TRY_AGAIN);
        break;
      case 2:
        this.gaService.trackEvent(this.activeTab, this.ga.ERROR, this.ga.REFRESH);
        break;
    }
    this.refreshAction.emit();
  }

  setGridSettings(): void {
    this.gridSettings = <GridSettings>{
      rows: {
        allOption: true,
        title: 'COM_LBL_ROWS',
        values: [10, 25, 50, 100],
        default: this.rowAmount
      }
    };
  }

  renew() {
    this.gridState.skip = this.gridState.skip - this.gridState.take;
    this.stateChange.emit(this.gridState);
  }

  public handlePrint(printAction: string): void {
    this.print.emit({
      printAction,
      items: this.gridView.data
    });
  }
}
