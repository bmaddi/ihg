import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InHouseViewComponent } from './in-house-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SessionStorageService} from 'ihg-ng-common-core';
import {ConfirmationModalService} from 'ihg-ng-common-components';
import {MOCK_IN_HOUSE_RESPONSE_2, MOCK_GROUP_LIST, MOCK_GROUP_LIST_BLANK, MOCK_IN_HOUSE_RESPONSE} from '@modules/in-house/mocks/in-house-mock';
import {InHouseGuestModel} from '@modules/in-house/models/in-house-guest.model';
import {CommonTestModule} from '@modules/common-test/common-test.module';
import {GroupBlockService} from '@app/modules/group-block/services/group-block.service';
import { By } from '@angular/platform-browser';

describe('InHouseViewComponent', () => {
  let component: InHouseViewComponent;
  let fixture: ComponentFixture<InHouseViewComponent>;
  let sessionService: SessionStorageService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InHouseViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonTestModule],
      providers: [ConfirmationModalService, GroupBlockService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    sessionService = TestBed.get(SessionStorageService);
    fixture = TestBed.createComponent(InHouseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create InHouseViewComponent', () => {
    expect(component).toBeTruthy();
    expect(component.totalItems).toEqual(0);
    expect(component.activeTab).toEqual('Guest List - In House');
    expect(component.searchQuery).toEqual('');
    expect(component.searchDone).toBeFalsy();
    const showHideButton = fixture.debugElement.query(By.css('app-show-hide-groups'));
    expect(showHideButton).toBeFalsy();
  });

  it('should handle a guest search', () => {
    component.gridState.skip = 8;
    expect(component.gridState.skip).toEqual(8);
    expect(component.inHouseParams).toEqual({});
    spyOn(sessionService, 'setSessionStorage');
    spyOn(component, 'handleClearFilters');
    const MOCK_DATA_SEARCH = {
      groupSelection: null,
      showGroups: false,
      searchCriteria: 'Test',
      hideToggle: false,
    };
    const MOCK_NO_DATA_SEARCH = {
      groupSelection: null,
      showGroups: false,
      searchCriteria: '',
      hideToggle: false,
    };

    component.onGuestSearch('');
    expect(sessionService.setSessionStorage).toHaveBeenCalledWith('guestInHouseParams', MOCK_NO_DATA_SEARCH);
    expect(component.gridState.skip).toEqual(0);
    expect(component.inHouseParams.searchCriteria).toEqual('');

    component.onGuestSearch('Test');
    expect(sessionService.setSessionStorage).toHaveBeenCalledWith('guestInHouseParams', MOCK_DATA_SEARCH);
    expect(component.gridState.skip).toEqual(0);
    expect(component.inHouseParams.searchCriteria).toEqual('Test');
    expect(component.handleClearFilters).toHaveBeenCalledTimes(2);
  });

  it('should properly clear the filters', () => {
    component.selectedFilters = {};
    component.filterApplied = true;
    component.filterCount = 9;

    expect(component.selectedFilters).toEqual({});
    expect(component.filterApplied).toEqual(true);
    expect(component.filterCount).toEqual(9);
    component.clearFilters();

    expect(component.selectedFilters).toEqual({ orangeFilter: ['total'], blueFilter: [] });
    expect(component.filterApplied).toEqual(false);
    expect(component.filterCount).toEqual(0);
  });

  xit('should upgrade the filter state', () => {

  });

  xit('should fetch the guest list', () => {
    spyOn(component, 'fetchGuestList').and.returnValue(MOCK_IN_HOUSE_RESPONSE_2);
    spyOn(component, 'handleDataChange');
    expect(component.guestData.data).toEqual([] as InHouseGuestModel[]);
    expect(component.totalItems).toEqual(0);
    expect(component.guestData.total).toEqual(0);

    component.fetchGuestList();

    expect(component.handleDataChange).toHaveBeenCalledTimes(1);
    expect(component.guestData.data).toEqual({} as InHouseGuestModel[]);
    expect(component.totalItems).toEqual(17);
    expect(component.guestData.total).toEqual(17);
  });

  it('should properly determine the search params', () => {
    expect(component.determineSearchParams()).toEqual('');
    expect(component.determineSearchParams('apple')).toEqual('apple');

    component.inHouseParams = { searchCriteria: 'ringo' };
    expect(component.determineSearchParams()).toEqual('ringo');
  });

  xit('should properly determine the grid state', () => {
    expect(component.gridState).toEqual({});

  });

  it('should check for the existing row count', () => {
    expect(component.gridState.take).toEqual(10);
    component.checkForExistingRowCount();
    expect(component.gridState.take).toEqual(10);

    component.inHouseParams = { rowCount: 78 };
    component.checkForExistingRowCount();
    expect(component.gridState.take).toEqual(78);
  });

  it('should test handleGuestSelection method', () => {
    spyOn(component, 'invokeClearFilter');
    component.handleGuestSelection('1234');
    expect(component.invokeClearFilter).toHaveBeenCalledWith('1234');
  });

  it('should not show Show / Hide Group Button when land into Inhouse Tab', () => {
    const changes = {
      'isActiveTab': {
        'currentValue': true,
        'previousValue': false,
        'firstChange': true
      }
    };
    component.ngOnChanges(changes);
    expect(component.searchDone).toBeFalsy();
    const showHideButton = fixture.debugElement.query(By.css('.show-hide-group-btn'));
    expect(showHideButton).toBeFalsy();
  });

  it('should hide "Show Groups" button when clear the filter and show Guest List but not Group List', () => {
    component.clearFilters();
    expect(component.showGroups).toBeFalsy();
    const guestList = fixture.debugElement.query(By.css('app-in-house-list')).nativeElement;
    expect(guestList).toBeTruthy();
  });

  it('should hide "Show Groups" button when click the reset button and show Guest List', () => {
    const isReset = true;
    component.handleResetEvent(isReset);
    expect(component.searchDone).toBeFalsy();
    const guestList = fixture.debugElement.query(By.css('app-in-house-list')).nativeElement;
    expect(guestList).toBeTruthy();
  });

  it('should show Group List when click on Show Group Button and Group list exist', () => {
    component.showGroups = true;
    component.handleGroupList(MOCK_GROUP_LIST);
    expect(component.isGroupList).toBeTruthy();
    const guestList = fixture.debugElement.query(By.css('app-group-block')).nativeElement;
    expect(guestList).toBeTruthy();
  });

  it('should show Guest List when click on Hide Group Button and Group list exist', () => {
    component.showGroups = false;
    component.handleGroupList(MOCK_GROUP_LIST);
    expect(component.isGroupList).toBeTruthy();
    const guestList = fixture.debugElement.query(By.css('app-in-house-list')).nativeElement;
    expect(guestList).toBeTruthy();
  });

  it('should show "Show Group" button if search result has Group info', () => {
    component.showGroups = true;
    component.searchDone = true;
    component.handleGroupList(MOCK_GROUP_LIST);
    expect(component.isGroupList).toBeTruthy();
    const guestList = fixture.debugElement.query(By.css('app-show-hide-groups'));
    expect(guestList).toBeDefined();
  });

  it('should hide "Show Group" button if search result dont have Group info', () => {
    component.showGroups = true;
    component.searchDone = true;
    component.handleGroupList(MOCK_GROUP_LIST_BLANK);
    expect(component.isGroupList).toBeFalsy();
    const guestList = fixture.debugElement.query(By.css('app-show-hide-groups'));
    expect(guestList).toBeNull();
  });

  it('should check if guest list grid shows up when clicked on group name', () => {
    spyOn(component, 'fetchGuestList');
    component.selectedGroup = undefined;
    const MOCK_GROUP_NAME = 'Test Group';
    component.handleGroupSelection(MOCK_GROUP_NAME);
    expect(component.showInHouse).toBeFalsy();
    expect(component.selectedGroup).toBe(MOCK_GROUP_NAME);
    expect(component.fetchGuestList).toHaveBeenCalled();
  });

  it('should check if guest list grid is populated with correct data on group search', () => {
    const MOCK_GROUP_NAME = 'Test Group';
    spyOn(component, 'updateGridFilterState');
    spyOn(component, 'fetchGuestList');
    component.handleGroupSelection(MOCK_GROUP_NAME);
    expect(component.inHouseParams.searchCriteria).toBe(MOCK_GROUP_NAME);
    component.handleGuestSelection(MOCK_GROUP_NAME);
    expect(component.updateGridFilterState).toHaveBeenCalledWith(MOCK_GROUP_NAME);
    expect(component.fetchGuestList).toHaveBeenCalled();
  });

  it('should check print functionality', () => {
    component.detailedPrint = undefined;
    spyOn(component.printableRecords, 'next');
    component.handlePrint({ printAction: 'currentDetailed', items: { data: MOCK_IN_HOUSE_RESPONSE } });
    expect(component.detailedPrint).toBeTruthy();
    expect(component.printableRecords.next).toHaveBeenCalled();
    component.detailedPrint = undefined;
    component.handlePrint({ printAction: 'currentCompact', items: { data: MOCK_IN_HOUSE_RESPONSE } });
    expect(component.printableRecords.next).toHaveBeenCalled();
  });

  it('should check show all functionality for groups when selected from dropdown', () => {
    spyOn(component, 'handleGroupSelection');
    component.handlePreviewGroupSelection('test', true);
    const groupsList = fixture.debugElement.query(By.css('app-group-block'));
    expect(groupsList).toBeDefined();
    expect(component.handleGroupSelection).not.toHaveBeenCalled();
  });

});
