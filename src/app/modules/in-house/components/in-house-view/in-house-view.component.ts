import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {State} from '@progress/kendo-data-query';
import {AppErrorBaseComponent} from '@app-shared/components/app-service-errors/app-error-base-component';
import * as moment from 'moment';
import {SessionStorageService, ToastMessageOutletService, useDefault} from 'ihg-ng-common-core';
import {GUEST_CONST} from '@modules/guests/guests.constants';
import {InHouseService} from '@modules/in-house/services/in-house.service';
import {InHouseResponseModel} from '@modules/in-house/models/in-house-response.model';
import {InHouseResponse} from '@modules/in-house/models/in-house-guest.model';
import {cloneDeep} from 'lodash';
import {GuestInHouseParams, GuestGridModel, GuestListModel} from '@modules/guests/models/guest-list.models';
import {CompositeFilterDescriptor} from '@progress/kendo-data-query/dist/npm/filtering/filter-descriptor.interface';
import {GroupBlockService} from '@app/modules/group-block/services/group-block.service';
import {GroupList} from '@app/modules/group-block/models/group-block.model';
import {printTypes} from '@modules/print-record/constants/print-grid.constant';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-in-house-view',
  templateUrl: './in-house-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./in-house-view.component.scss']
})
export class InHouseViewComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy  {

  @Input() isActiveTab: boolean;
  @Input() inHouseParams: GuestInHouseParams;

  defaultFilter: CompositeFilterDescriptor = { logic: 'and', filters: [] };
  gridState: State = {
    take: 10,
    skip: 0,
    filter: this.defaultFilter,
    sort: []
  };
  guestData: InHouseResponse = { data: [], total: 0 };
  dateIndex: number;
  totalItems: number;
  searchQuery: string;
  selector = 0;
  activeTab: string;
  ga = GUEST_CONST.GA;
  totalStr = 'total';
  readonly filterTypes = { loyalty: 'orangeFilter', misc: 'blueFilter' };
  defaultFilterState = { [this.filterTypes.loyalty]: [this.totalStr], [this.filterTypes.misc]: [] };
  selectedFilters = cloneDeep(this.defaultFilterState);
  filterApplied = false;
  filterCount = 0;
  searchDone = false;
  isGroupList = false;
  public showGroups = false;
  public showInHouse = true;
  public selectedGroup = null;
  groupList: GroupList[] = [];
  suppressGroupsBtn = false;
  checkPreview = false;
  detailedPrint: boolean;
  printableRecords = new Subject<GuestGridModel[]>();

  constructor(private inHouseService: InHouseService,
              private toastOutletService: ToastMessageOutletService,
              private storageService: SessionStorageService,
              private groupBlockService: GroupBlockService
  ) {
    super();
  }

  ngOnInit() {
    this.totalItems = 0;
    this.searchDone = false;
    this.searchQuery = this.determineSearchParams();
    super.init();
    this.activeTab = this.ga.CATEGORY + this.ga.SUB_CATEGORY.INHOUSETAB;
    this.checkInHouseParams();
  }

  saveSessionParams(): void {
    this.storageService.setSessionStorage('guestInHouseParams', this.inHouseParams);
    this.storageService.setSessionStorage('normalSearch', this.checkPreview);
  }

  checkInHouseParams() {
    if (!this.inHouseParams) {
      this.inHouseParams = {};
      this.suppressGroupsBtn = false;
    } else if (this.inHouseParams.showGroups) {
      this.showGroups = this.inHouseParams.showGroups;
      this.selectedGroup = this.inHouseParams.groupSelection || this.searchQuery;
      this.showInHouse = this.inHouseParams.showInHouse;
      this.suppressGroupsBtn = this.inHouseParams.hideToggle ? this.inHouseParams.hideToggle : false;
    }
  }

  onGuestSearch(searchString: string): void {
    this.searchDone = true;
    this.suppressGroupsBtn = true;
    this.gridState.skip = 0;
    this.inHouseParams.groupSelection = this.selectedGroup = null;
    this.inHouseParams.showGroups = this.showGroups = false;
    this.inHouseParams.searchCriteria = this.searchQuery = searchString;
    this.inHouseParams.hideToggle = false;
    this.checkPreview = true;
    this.saveSessionParams();
    this.handleClearFilters();
  }

  handleClearFilters(): void {
    const callbackFn = this.invokeClearFilter.bind(this);
    this.inHouseService.handleUnsavedTasks(callbackFn);
  }

  invokeClearFilter(searchBy?: string): void {
    this.clearFilters();
    this.updateGridFilterState(searchBy);
    this.fetchGuestList();
  }

  clearFilters(): void {
    this.selectedFilters = cloneDeep(this.defaultFilterState);
    this.filterApplied = false;
    this.filterCount = 0;
    this.showGroups = false;
  }

  updateGridFilterState(searchValue = null): void {
    const appliedLoyaltyFilters = this.selectedFilters[this.filterTypes.loyalty];
    const appliedMiscFilters = this.selectedFilters[this.filterTypes.misc];
    const allAppliedFilters = [];

    if (this.selectedGroup || this.searchQuery.length || searchValue) {
      const searchBy = this.selectedGroup || searchValue || this.searchQuery;
      allAppliedFilters.push({ field: 'search', operator: 'eq', value: searchBy.replace(/['<>]/g, ' ') });
    }

    if (appliedLoyaltyFilters.length && appliedLoyaltyFilters[0] !== this.totalStr) {
      allAppliedFilters.push({ field: this.filterTypes.loyalty, operator: 'eq', value: appliedLoyaltyFilters.join(',') });
    }

    if (appliedMiscFilters.length) {
      allAppliedFilters.push({ field: this.filterTypes.misc, operator: 'eq', value: appliedMiscFilters.join(',') });
    }

    this.gridState.filter.filters = allAppliedFilters;
  }

  onGuestListRefresh(): void {
    this.fetchGuestList();
  }

  onStateChange(state: State): void {
    this.fetchGuestList(state);
  }

  handleErrors = (error) => {
    this.guestData.data = [];
    this.processHttpErrors(error);
  }

  handleDataChange = (response: InHouseResponseModel) => {
    const hasErrors = this.checkIfAnyApiErrors(response, data => !data || !data.items || data.items.length === 0);

    this.guestData.data = hasErrors ? [] : response.items;
    this.totalItems = hasErrors ? 0 : response.totalItems;
    this.guestData.total = this.totalItems;
    const search = this.storageService.getSessionStorage('searchValue');
    this.searchQuery = search ? search : this.searchQuery;
    this.updateGroupDisplay();
  }

  ngOnChanges(changes: any) {
    if (changes.isActiveTab && changes.isActiveTab.currentValue) {
      this.suppressGroupsBtn = false;
      const search = this.storageService.getSessionStorage('searchValue');
      const preview = this.storageService.getSessionStorage('normalSearch');
      if (search && preview) {
        this.onGuestSearch(search);
      } else {
        this.fetchGuestList();
      }
    } else {
      this.inHouseParams = {};
      this.searchQuery = '';
      this.selectedGroup = null;
      this.searchDone = false;
      this.saveSessionParams();
    }
  }

  fetchGuestList(state?: State, searchValue?: string) {
    const searchBy = searchValue || (this.selectedGroup) ? this.selectedGroup : this.determineSearchParams(this.searchQuery);
    const searchColumn = this.selectedGroup ? 'group' : null;
    this.determineGridState(state);
    this.inHouseService.getInHouseList(moment().format('YYYY-MM-DD'), searchBy, this.gridState, searchColumn).subscribe(
      this.handleDataChange,
      error => this.handleErrors(error));
  }

  determineSearchParams(searchQuery?): string {
    return (this.inHouseParams && this.inHouseParams.searchCriteria) ? this.inHouseParams.searchCriteria : useDefault(searchQuery, '');
  }

  determineGridState = (state: State) => {
    if (!!state) {
      this.gridState = state;
    } else {
      this.gridState.sort = null;
      this.gridState.skip = 0;
    }
    if (!this.gridState.filter) {
      this.gridState.filter = this.defaultFilter;
    }
    this.checkForExistingRowCount();
  }

  checkForExistingRowCount() {
    if (this.inHouseParams.rowCount) {
      this.gridState.take = useDefault(this.inHouseParams.rowCount as number, 10);
    } else {
      this.inHouseParams.rowCount = this.gridState.take;
    }
  }

  showGroupToggle(): boolean {
    return !!this.groupList && this.groupList.length > 0;
  }

  public handleShowHideGroup(value): void {
    super.resetSpecificError(this.emitError);
    this.inHouseParams.showGroups = this.showGroups = !this.showGroups;
    this.checkPreview = false;
    this.updateShowGroupsShowInHouse(value, !value);
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    if (this.showInHouse) {
      this.selectedGroup = null;
      this.inHouseParams.searchCriteria = null;
      this.invokeClearFilter('');
    } else {
      this.groupBlockService.showGroups.next();
    }
    this.saveSessionParams();
  }

  private updateGroupDisplay(): void {
    this.updateShowGroupsShowInHouse(this.selectedGroup || (!this.groupList || this.groupList.length ? false : null), true);
  }

  private updateShowGroupsShowInHouse(showGroups: boolean, showInHouse: boolean): void {
    this.inHouseParams.showGroups = this.showGroups = showGroups;
    this.inHouseParams.showInHouse = this.showInHouse = showInHouse;
    this.saveSessionParams();
  }

  public handleGroupList(groupList: object): void {
    if (groupList) {
      this.groupList = groupList['data'] || null;
      this.isGroupList = (!!this.inHouseParams && !!this.inHouseParams.groupSelection) || (this.showGroupToggle());
      if (!groupList['refresh']) {
        this.updateGroupDisplay();
      }
    }
  }

  public handleGroupSelection(groupName: string): void {
    this.gridState.skip = 0;
    this.inHouseParams.groupSelection = this.selectedGroup = groupName;
    this.updateShowGroupsShowInHouse(true, false);
    this.saveSessionParams();
    this.invokeClearFilter();
    this.inHouseParams.searchCriteria = groupName;
  }

  public handleGuestSelection(searchValue: string): void {
    this.selectedGroup = '';
    this.suppressGroupsBtn = true;
    this.inHouseParams.groupSelection = this.selectedGroup = null;
    this.inHouseParams.showGroups = this.showGroups = false;
    this.inHouseParams.searchCriteria = searchValue;
    this.inHouseParams.hideToggle = false;
    this.checkPreview = false;
    this.saveSessionParams();
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.invokeClearFilter(searchValue);
  }

  public handlePreviewGroupSelection(searchValue: string, isAll = false): void {
    this.groupBlockService.previewSearch.next({ search: searchValue, isAll: isAll });
    this.updateShowGroupsShowInHouse(true, false);
    if (!isAll) {
      this.handleGroupSelection(searchValue);
    }
  }

  public handleResetEvent(isReset: boolean) {
    this.searchDone = !isReset;
    this.inHouseParams = {};
    this.inHouseParams.showGroups = this.isGroupList = false;
    this.suppressGroupsBtn = false;
    this.saveSessionParams();
  }

  public handlePrint(data: { printAction: string, items }): void {
    this.detailedPrint = data.printAction === printTypes.CURR_DTLD || data.printAction === printTypes.ALL_DTLD;
    switch (data.printAction) {
      case printTypes.CURR_CMPCT:
      case printTypes.CURR_DTLD:
        this.printableRecords.next(data.items['data']);
        break;
      case printTypes.ALL_CMPCT:
      case printTypes.ALL_DTLD:
        this.handlePrintAll();
        break;
    }
  }

  private handlePrintAll(): void {
    const gridState = cloneDeep(this.gridState);
    const searchVal = this.selectedGroup ? this.selectedGroup : this.searchQuery;
    const searchCol = this.selectedGroup ? 'group' : null;
    this.inHouseService.getInHouseList(moment().format('YYYY-MM-DD'), searchVal, { ...gridState, take: 10000, skip: 0 }, searchCol)
      .subscribe((data: GuestListModel) => {
        if (!this.checkIfAnyApiErrors(data, null, this.inHouseService.printInHouseError)) {
          this.printableRecords.next(data.items);
        }
      },
        error => {
          super.processHttpErrors(error, this.inHouseService.printInHouseError);
        }
      );
  }

  ngOnDestroy(): void {
    this.toastOutletService.setPageErrorDisabled(false);
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
  }
}
