import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'
import { GridModule } from '@progress/kendo-angular-grid';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';

import { InHouseListComponent } from './components/in-house-list/in-house-list.component';
import { ManageStayModule } from '@modules/manage-stay/manage-stay.module';
import { BadgeListModule } from '@modules/badge-list/badge-list.module';
import { InHouseViewComponent } from './components/in-house-view/in-house-view.component';
import { GuestSearchModule } from '@modules/guest-search/guest-search.module';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { ProgressStatusModule } from '@modules/progress-status/progress-status.module';
import { GroupBlockModule } from '../group-block/group-block.module';
import { InHouseGridPagerComponent } from './components/in-house-grid-pager/in-house-grid-pager.component';
import { PrintRecordModule } from '@app/modules/print-record/print-record.module';

@NgModule({
  imports: [
    CommonModule,
    GuestSearchModule,
    ManageStayModule,
    BadgeListModule,
    TranslateModule,
    GridModule,
    NgbModule,
    RouterModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    GroupBlockModule,
    AppSharedModule,
    ProgressStatusModule,
    PrintRecordModule,
  ],
  declarations: [
    InHouseListComponent,
    InHouseViewComponent,
    InHouseGridPagerComponent,
  ],
  exports: [
    InHouseListComponent,
    InHouseViewComponent,
  ]
})
export class InHouseModule { }
