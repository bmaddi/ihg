import {InHouseGuestModel, InHouseResponse} from '@modules/in-house/models/in-house-guest.model';
import {Room} from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import {GuestInHouseParams} from '@modules/guests/models/guest-list.models';

export const MOCK_IN_HOUSE_RESPONSE: InHouseResponse = {
  data: [
    {
      firstName: 'one',
      lastName: 'two',
      holidexNumber: 'three',
      pmsNumber: 'four',
      badges: [
        '123',
        '321'
      ],
      loyaltyId: 'five',
      groupName: 'six',
      nights: 7,
      room: undefined,
      checkInDate: 'eight',
      checkOutDate: 'nine',
      hotelInspectedFlag: 'ten'
    }
  ],
  total: 1
};

export const MOCK_IN_HOUSE_ARRAY: InHouseGuestModel[] = [
  {
    firstName: 'Mike',
    lastName: 'Jones',
    holidexNumber: '1',
    pmsNumber: '2',
    badges: [],
    loyaltyId: '2813308004',
    groupName: 'Ice Age',
    nights: 2,
    room: null,
    checkInDate: '',
    checkOutDate: '',
    hotelInspectedFlag: 'nah'
  }
];

export const MOCK_IN_HOUSE_PARAMS: GuestInHouseParams = {
  searchCriteria: 'chocolate',
  rowCount: 3
};

export const MOCK_IN_HOUSE_PARAMS_2: GuestInHouseParams = {
  searchCriteria: 'raspberry'
};

export const DEFAULT_GUEST_NAME = {
  firstName: 'Greatest',
  lastName: 'Ever'
};

export const MOCK_IN_HOUSE_RESPONSE_2 = {
  pageNumber: 1,
  pageSize: 10,
  totalPages: 2,
  totalItems: 17,
  startItem: 1,
  endItem: 10,
  items: [
    {
      firstName: 'Phineas',
      lastName: 'Fogg',
      holidexNumber: '48745123',
      pmsNumber: '19444',
      loyaltyId: '',
      badges: [

      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-22',
      checkOutDate: '2019-10-23',
      roomNumber: '228',
      roomType: 'CSTN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Test User C',
      lastName: 'Homepage',
      holidexNumber: '22904217',
      pmsNumber: '19400',
      loyaltyId: '147096233',
      badges: [
        'karma',
        'platinum'
      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-21',
      checkOutDate: '2019-10-22',
      roomNumber: '111',
      roomType: 'KEXS',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Anuroop',
      lastName: 'Kumar',
      holidexNumber: '49656806',
      pmsNumber: '19100',
      loyaltyId: '149829166',
      badges: [
        'club'
      ],
      groupName: '',
      nights: 4,
      checkInDate: '2019-10-18',
      checkOutDate: '2019-10-22',
      roomNumber: '127',
      roomType: 'CSTN',
      roomStatus: 'DI',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Sruthi',
      lastName: 'Kuruvilla',
      holidexNumber: '41955592',
      pmsNumber: '19098',
      loyaltyId: '150765771',
      badges: [
        'gold'
      ],
      groupName: '',
      nights: 11,
      checkInDate: '2019-10-18',
      checkOutDate: '2019-10-29',
      roomNumber: '108',
      roomType: 'KNGN',
      roomStatus: 'DI',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Giridhar',
      lastName: 'Maniyendra',
      holidexNumber: '47127134',
      pmsNumber: '19414',
      loyaltyId: '146154253',
      badges: [
        'club',
        'employee'
      ],
      groupName: '',
      nights: 2,
      checkInDate: '2019-10-21',
      checkOutDate: '2019-10-23',
      roomNumber: '210',
      roomType: 'KNGN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Mickey',
      lastName: 'Mouse',
      holidexNumber: '46341203',
      pmsNumber: '19440',
      loyaltyId: '',
      badges: [

      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-22',
      checkOutDate: '2019-10-23',
      roomNumber: '110',
      roomType: 'KNGN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Mickey',
      lastName: 'Mouse',
      holidexNumber: '44694140',
      pmsNumber: '19442',
      loyaltyId: '',
      badges: [

      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-22',
      checkOutDate: '2019-10-23',
      roomNumber: '209',
      roomType: 'KNGN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Minnie',
      lastName: 'Mouse',
      holidexNumber: '43744471',
      pmsNumber: '19435',
      loyaltyId: '',
      badges: [

      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-22',
      checkOutDate: '2019-10-23',
      roomNumber: '109',
      roomType: 'KNGN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Uday',
      lastName: 'Neethipudi',
      holidexNumber: '22635793',
      pmsNumber: '19415',
      loyaltyId: '148807231',
      badges: [
        'club'
      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-21',
      checkOutDate: '2019-10-22',
      roomNumber: '106',
      roomType: 'KNGN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    },
    {
      firstName: 'Test User A',
      lastName: 'Rising Phoenix',
      holidexNumber: '46632481',
      pmsNumber: '19076',
      loyaltyId: '147096190',
      badges: [
        'spire',
        'ambassador',
        'employee'
      ],
      groupName: '',
      nights: 1,
      checkInDate: '2019-10-17',
      checkOutDate: '2019-10-18',
      roomNumber: '427',
      roomType: 'CSTN',
      roomStatus: 'IP',
      hotelInspectedStatus: 'Y'
    }
  ]
};

export const MOCK_GROUP_LIST = {
  data: [
    {
      groupName: 'RISING PHOENIX GROUP',
      groupCode: 'PHX',
      arrivalDate: '2019-07-08',
      departureDate: '2020-06-22',
      noOfNights: 350,
      releaseDate: '',
      roomsContracted: 100,
      numberOfReservations: 20
    },
    {
      groupName: 'RISINGPHOENIXGROUP',
      groupCode: 'GP9',
      arrivalDate: '2019-07-22',
      departureDate: '2020-06-30',
      noOfNights: 344,
      releaseDate: '2019-07-22',
      roomsContracted: 0,
      numberOfReservations: 100
    }
  ],
  localDate: '2019-08-29'
};

export const MOCK_GROUP_LIST_BLANK = {
  data: [],
  localDate: '2019-08-29'
};
