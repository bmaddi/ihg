import {Room} from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';

export interface InHouseGuestModel {
  firstName: string;
  lastName: string;
  holidexNumber: string;
  pmsNumber: string;
  badges: Array<string>;
  loyaltyId: string;
  groupName: string;
  nights: number;
  room: Room;
  checkInDate: string;
  checkOutDate: string;
  hotelInspectedFlag: string;
}

export interface  InHouseResponse {
  data: InHouseGuestModel[];
  total: number;
}
