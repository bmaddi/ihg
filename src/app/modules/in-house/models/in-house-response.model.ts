import {InHouseGuestModel} from '@modules/in-house/models/in-house-guest.model';

export interface InHouseResponseModel {
  pageNumber: number;
  pageSize: number;
  totalPages: number;
  totalItems: number;
  startItem: number;
  endItem: number;
  items: InHouseGuestModel[];
}
