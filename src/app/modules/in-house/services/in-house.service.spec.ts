import { TestBed } from '@angular/core/testing';
import { InHouseService } from './in-house.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { environment } from '@env/environment';
import { IN_HOUSE_GUEST_MOCK } from '../mocks/in-house-service-mock';
import { componentFactoryName } from '@angular/compiler';

describe('InHouseService', () => {
    let modalServiceSpy: { get: jasmine.Spy };
    let service: InHouseService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [UserService, ConfirmationModalService, InHouseService]
        });
        modalServiceSpy = jasmine.createSpyObj('ConfirmationModalService', ['openConfirmationModal']);
        httpTestingController = TestBed.get(HttpTestingController);
        service = TestBed.get(InHouseService);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should check getInHouseList method & check that returned Observable should match the right data', () => {
        const day = '2019-10-22';
        service.getInHouseList(day).subscribe(
            guestList => expect(guestList).toEqual(IN_HOUSE_GUEST_MOCK, 'expected Guest List'),
            fail
        );
        const req = httpTestingController.
            expectOne(`${environment.fdkAPI}inHouse/inHouseList?day=${day}`);
        expect(req.request.method).toEqual('GET');

        req.flush(IN_HOUSE_GUEST_MOCK);
    });

    it('should check getUnsavedTask method', () => {
        service.hasUnsavedTask = true;
        const res = service.getUnsavedTask();
        expect(res).toBeTruthy();
    });

    it('should check setUnsavedTask method', () => {
        service.hasUnsavedTask = false;
        service.setUnsavedTask(true);
        expect(service.hasUnsavedTask).toBeTruthy();
    });

});
