import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {State} from '@progress/kendo-data-query';
import {environment} from '@env/environment';
import {UserConfirmationModalConfig, UserService} from 'ihg-ng-common-core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ConfirmationModalService} from 'ihg-ng-common-components';
import {TasksModel} from '@modules/prepare/models/arrivals-checklist.model';
import {Subject} from 'rxjs';
import {EmitErrorModel} from '@app-shared/models/app-service-errors/app-service-errors-models';

@Injectable({
  providedIn: 'root'
})

export class InHouseService {

  private readonly API_URL = environment.fdkAPI;
  public hasUnsavedTask = false;
  public taskChangeSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public printInHouseError = new Subject<EmitErrorModel>();

  constructor(
    private http: HttpClient,
    public userService: UserService,
    private modalService: ConfirmationModalService,
  ) { }

  public getInHouseList(day: string, searchby?: string, state?: State, searchCol?: string) {
    const searchparam = searchby ? `&searchBy=${searchby}` : '';
    const pageSizeParam = state ? `&pageSize=${state.take}` : '';
    const searchColParam = searchCol ? `&searchColumn=${searchCol}` : '';
    const pageNumberParam = state ? `&page=${state.skip && state.take ? state.skip / state.take + 1 : 1}` : '';
    const sortByParam = state && state.sort ? `&sortBy=${state.sort[0].field === 'guestName' ? 'lastName,firstName' : state.sort[0].field}` : '';
    const sortDirectionParam = state && state.sort ? `&sortDirection=${state.sort[0].dir}` : '';

    const apiURl = `${this.API_URL}inHouse/inHouseList?day=${day}${searchparam}${pageSizeParam}${pageNumberParam}${sortByParam}${sortDirectionParam}${searchColParam}`;

    return this.http.get(apiURl).pipe(map((response) => {
      return response;
    }));
  }

  getUnsavedTask(): boolean {
    return this.hasUnsavedTask;
  }

  getConfirmModalConfig(): UserConfirmationModalConfig {
    return {
      modalHeader: 'LBL_MOD_TTL_UNSNT_TSK',
      primaryButtonLabel: 'COM_BTN_CONT',
      primaryButtonClass: 'btn-primary',
      defaultButtonLabel: 'COM_BTN_CANCEL',
      defaultButtonClass: 'btn-default',
      messageHeader: 'LBL_MOD_TXT_UNSNT_TSK',
      messageBody: '',
      windowClass: 'modal-medium',
      dismissible: true
    };
  }

  openUnsentTaskWarningModal() {
    return this.modalService.openConfirmationModal(this.getConfirmModalConfig());
  }

  discardUnsentTasks(dataItem): void {
    if (dataItem && dataItem.tasks) {
      dataItem.tasks = dataItem.tasks.filter((task: TasksModel) => task.isResolved !== null);
    }
  }

  setUnsavedTask(unsavedTask: boolean): void {
    this.hasUnsavedTask = unsavedTask;
  }

  executeCallBack(callBackAction: Function): void {
    if (callBackAction) {
      callBackAction();
    }
  }

  handleUnsavedTasks(callBackAction: Function, cancelCallBackFn?: Function, dataItem?): void {
    if (this.getUnsavedTask()) {
      this.openUnsentTaskWarningModal().subscribe(
        (proceed) => {
          this.taskChangeSubject.next(false);
          this.discardUnsentTasks(dataItem);
          this.setUnsavedTask(false);
          this.executeCallBack(callBackAction);
        },
        (cancel) => {
          this.executeCallBack(cancelCallBackFn);
          return false;
        });
    } else {
      this.executeCallBack(callBackAction);
    }
  }
}
