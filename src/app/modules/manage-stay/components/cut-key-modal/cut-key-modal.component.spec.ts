import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { UserService, GoogleAnalyticsService} from 'ihg-ng-common-core';
import { CutKeyModalComponent } from './cut-key-modal.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

import { RoomDetails } from '@modules/manage-stay/models/manage-stay.models';
import { MockGoogleAnalyticsService } from '@modules/manage-stay/components/manage-cut-key/manage-cut-key.component.spec';
import { OfferByProductsGridComponent } from '@modules/offers/components/offer-by-products-grid/offer-by-products-grid.component';
import { EncoderSelectorComponent } from '@modules/encoder/components/encoder-selector/encoder-selector.component';
import { MOCK_ENCODER_LIST, MOCK_SELECTED_ENCODER } from '@modules/encoder/mocks/encoder-mock';
import { ErrorToastMessageComponent } from '@app-shared/components/error-toast-message/error-toast-message.component';
import { error } from 'util';
import { CUT_KEY_ERROR } from '@modules/manage-stay/constants/manage-stay-constants';
import { Test } from 'tslint';
import { OffersService } from '@modules/offers/services/offers.service';

describe('CutKeyModalComponent', () => {
  let component: CutKeyModalComponent;
  let fixture: ComponentFixture<CutKeyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CutKeyModalComponent, ErrorToastMessageComponent ],
      imports: [ NgbModule.forRoot(), HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [ TranslateService, NgbActiveModal, UserService, { provide: ReportIssueService, useValue: {} },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService }, { provide: ActivatedRoute, useValue: {} }],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CutKeyModalComponent);
    component = fixture.componentInstance;
    component.manageStayData =  {
      roomNumber: '140',
      roomType: 'CTSN',
      roomStatus: 'Clean'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the modal on clicking x', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.close')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('modal title should have offers not available', () => {
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('[translate="LBL_ROOM_ASSIGNMENT_SUCCESS"]')).nativeElement;
    expect(element).not.toBeNull();
    expect(element.textContent.indexOf('LBL_ROOM_ASSIGNMENT_SUCCESS') > -1).toBeTruthy();
  });

  it('should close the modal on clicking close', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.btn-default')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('should validate room details', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CutKeyMsg-SID"]')).nativeElement;
    expect(element).not.toBeNull();
    expect(element.textContent.indexOf('LBL_ROOM_CUTKEYS') > -1).toBeTruthy();
    const roomNum = fixture.debugElement.query(By.css('span.room-number')).nativeElement;
    expect(roomNum).not.toBeNull();
    expect(roomNum.textContent).toBe('140');
    const roomType = fixture.debugElement.query(By.css('span.room-type')).nativeElement;
    expect(roomType).not.toBeNull();
    expect(roomType.textContent).toBe('CTSN');
  });

  it('should check cut key is present', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CutKeys-SID"]')).nativeElement;
    expect(element).not.toBeNull();
  });

  it('should check key encoder present in cut key', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="KeysLabel-SID"]')).nativeElement;
    expect(element).not.toBeNull();
    expect(element.textContent).toEqual('LBL_KEYS');

    const encoderElement = fixture.debugElement.query(By.directive(EncoderSelectorComponent));
    expect(encoderElement).toBeDefined();
  });

  it('should populate the encoder value in manage room component', () => {
    expect(component.selectedEncoder).toBeUndefined();
    component.reservationNumber = '1234';
    component.ngOnInit();
    expect(component.selectedEncoder).toEqual(null);
  });

  it('validate error content', () => {
    const errorSpy = spyOn(component.cutKeyAssignmentError, 'next');
    component.isCutKeyError({isError: true});
    fixture.detectChanges();
    expect(errorSpy).toHaveBeenCalled();
    expect(component.errorContent).toBe(CUT_KEY_ERROR);
  });

  it('validate report issue is prepopulated', () => {
    component.reservationNumber = '1234';
    spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    component['setAutoFill']();
    fixture.detectChanges();
    expect(component.reportIssueAutoFill.genericError.subject).toBe('Error cutting keys for room 140 for reservation 1234');
  });

  it('validate cut key button is disabled when encoder not selected', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CutKeys-SID"]')).nativeElement;
    expect(element).not.toBeNull();
    fixture.detectChanges();
    expect(element.disabled).toBeTruthy();
  });

  it('validate cut key button is enabled when encoder is selected', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="CutKeys-SID"]')).nativeElement;
    expect(element).not.toBeNull();

    component.encoderSelected(MOCK_SELECTED_ENCODER);
    fixture.detectChanges();

    expect(element.disabled).toBeFalsy();
  });

});
