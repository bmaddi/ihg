import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';

import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

import { ManageStayModel } from '@modules/manage-stay/models/manage-stay.models';
import { ManageStayService } from '@modules/manage-stay/services/manage-stay.service';
import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { ManageCutKeyComponent } from '@modules/manage-stay/components/manage-cut-key/manage-cut-key.component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { EncoderModel } from '@modules/encoder/models/encoder.model';
import { CUT_KEY_SAVE_ERROR } from '@app/constants/error-autofill-constants';
import { OffersService } from '@modules/offers/services/offers.service';
import { CUT_KEY_ERROR } from '@modules/manage-stay/constants/manage-stay-constants';

@Component({
  selector: 'app-cut-key-modal',
  templateUrl: './cut-key-modal.component.html',
  styleUrls: ['./cut-key-modal.component.scss']
})
export class CutKeyModalComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  @Input() pmsNumber: string;
  @Input() reservationNumber: string;
  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: StayInfoSearchModel;
  @ViewChild(ManageCutKeyComponent) cutKeyComp: ManageCutKeyComponent;

  public manageStayData: ManageStayModel;
  public errorContent = CUT_KEY_ERROR;
  public cutKeyAssignmentError = new Subject<EmitErrorModel>();
  public defaultEncoder: EncoderModel = {
    encoderName: '',
    encoderId: ''
  };
  isEncoderSelected: boolean;
  selectedEncoder: EncoderModel;
  reportIssueAutoFill: ReportIssueAutoFillObject;
  private rootSubscription = new Subscription();

  constructor(private activeModal: NgbActiveModal,
              private manageStayService: ManageStayService,
              private offersService: OffersService) {
    super();
  }

  ngOnInit() {
    if (this.reservationNumber) {
      this.getInHouseGuestData(this.reservationNumber);
      this.selectedEncoder = null;
    }
  }

  dismissModal() {
    this.activeModal.dismiss();
  }

  getInHouseGuestData(reservationNumber) {
    this.manageStayService.getManageStayData(reservationNumber).subscribe((response: ManageStayModel) => {
      this.manageStayData = response;
      this.setAutoFill();
    }, (error) => {
      this.manageStayData = {};
    });
  }

  isCutKeyError(error): void {
    if (error.isError === true) {
      super.processHttpErrors(error.error, this.cutKeyAssignmentError);
    } else {
      this.activeModal.close();
      super.resetSpecificError(this.cutKeyAssignmentError);
    }
  }

  handleCutKey() {
      this.cutKeyComp.onCutKey();
  }

  encoderSelected(encoder) {
    this.selectedEncoder = encoder as EncoderModel;
    this.isEncoderSelected = true;
  }

  shouldEnableButton(): boolean {
    return this.isEncoderSelected && !!this.selectedEncoder && !!this.selectedEncoder.encoderId;
  }

  private setAutoFill() {
    if (this.offersService.checkForManageStayEdit()) {
      this.reportIssueAutoFill = CUT_KEY_SAVE_ERROR(this.reservationNumber, this.manageStayData.roomNumber);
    }
  }

  ngOnDestroy(): void {
    this.rootSubscription.unsubscribe();
  }
}
