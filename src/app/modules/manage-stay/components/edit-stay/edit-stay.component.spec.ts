import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { TitleCasePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { EnrollNewMemberService, ReportIssueService } from 'ihg-ng-common-pages';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { EditStayComponent } from './edit-stay.component';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { ActivatedRoute } from '@angular/router';
import { By } from '@angular/platform-browser';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { OffersService } from '@modules/offers/services/offers.service';
import { GUEST_SAVE_AUTOFILL, IN_HOUSE_SAVE_GUEST_INFO } from '@app/constants/error-autofill-constants';
import { ChangeStayService } from '@modules/offers/services/change-stay/change-stay.service';
import { MOCK_ORIGINAL_RESERVATION_DATA, MOCK_SEARCH_CRITERIA } from '@modules/offers/mocks/warning-modal.mock';
import { offersByRateCategoryMock, offersByRateMockResponse } from '@modules/offers/mocks/offers-by-rates.mock';

describe('EditStayComponent', () => {
  let component: EditStayComponent;
  let fixture: ComponentFixture<EditStayComponent>;
  let offerService: OffersService;
  let changeStayService: ChangeStayService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: { openAutofilledReportIssueModal: (t): Observable<string> => of(t) }
      })
      .configureTestingModule({
        imports: [
          TranslateModule.forChild(),
          RouterTestingModule,
          HttpClientTestingModule],
        declarations: [EditStayComponent],
        providers: [
          UserService,
          GoogleAnalyticsService,
          ReportIssueService,
          TitleCasePipe,
          ConfirmationModalService,
          EnrollNewMemberService,
          TranslateStore,
          ChangeStayService],
        schemas: [NO_ERRORS_SCHEMA]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    offerService = TestBed.get(OffersService);
    changeStayService = TestBed.get(ChangeStayService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should default to the edit stay edit view', () => {
    const routeSubFunc = spyOn<any>(TestBed.get(ActivatedRoute).params, 'subscribe').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    const tabs = fixture.debugElement.query(By.css('[data-slnm-ihg="ManageStayTabs-SID"]'));

    expect(component.isEditView).toBeTruthy();
    expect(component.manageStayEdit).toBeFalsy();
    expect(routeSubFunc).toHaveBeenCalled();
    expect(tabs).toBeDefined();
  });

  it('should hide edit view and show content projection for view only', () => {
    const routeSubFunc = spyOn<any>(TestBed.get(ActivatedRoute).params, 'subscribe').and.callThrough();
    component.isEditView = false;
    component.reservationNumber = '1111111';
    component.state = AppStateNames.manageStay;

    component.ngOnInit();
    fixture.detectChanges();
    const tabs = fixture.debugElement.query(By.css('[data-slnm-ihg="ManageStayTabs-SID"]'));

    expect(component.isEditView).toBeFalsy();
    expect(component.manageStayEdit).toBeTruthy();
    expect(routeSubFunc).not.toHaveBeenCalled();
    expect(tabs).toBeNull();
  });

  it('should cover error on search/timeout offer failure', () => {
    const getOffersErrorAlertSpy = spyOn(TestBed.get(OffersService), 'getOffersErrorAlert').and.callThrough();
    expect(component.offersErrorToast).not.toBeDefined();

    component.showConfirmationCard = false;
    component['subscribeToSearchOffersError']();
    fixture.detectChanges();
    offerService['updateOffersErrorCount'](offerService['errorKeys'].rates);
    offerService.updateOffersErrorTrigger(true);

    expect(getOffersErrorAlertSpy).toHaveBeenCalled();
    expect(component.offersErrorToast).toBeDefined();
  });

  it('should set correct Autofills for service/timeout Report an Issue for In House - Edit Stay', () => {
    expect(component.saveErrorAutoFill).toEqual(GUEST_SAVE_AUTOFILL);
    const checkForManageStayEdit = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);

    component.ngOnInit();
    fixture.detectChanges();

    expect(component.saveErrorAutoFill).toEqual(IN_HOUSE_SAVE_GUEST_INFO as any);
    expect(checkForManageStayEdit).toHaveBeenCalled();
  });

  it('should set min and max alerts after departure and rate change', fakeAsync(() => {
    const getOffersByRateSpy = spyOn(changeStayService, 'getOffersByRate').and.returnValue(of(offersByRateMockResponse));
    const getMinMaxStayAlertSpy = spyOn(changeStayService, 'getMinMaxStayAlert').and.callThrough();

    component.originalReservationData = MOCK_ORIGINAL_RESERVATION_DATA;
    changeStayService['offersByRate'] = offersByRateCategoryMock;
    changeStayService.departureRateChange.next(MOCK_SEARCH_CRITERIA);
    component['getOffersByRate'](MOCK_SEARCH_CRITERIA, false);
    tick();

    expect(getOffersByRateSpy).toHaveBeenCalledWith(MOCK_SEARCH_CRITERIA, false);
    expect(component.offersByRate).toEqual(offersByRateCategoryMock);
    expect(getMinMaxStayAlertSpy).toHaveBeenCalled();

    const alert = changeStayService.getMinMaxStayAlert('2019-10-19', MOCK_ORIGINAL_RESERVATION_DATA);
    expect(component.minMaxStayAlert).toEqual(alert);
  }));
});
