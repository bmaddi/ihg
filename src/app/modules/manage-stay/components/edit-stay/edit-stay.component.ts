import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';

import { Alert, AlertType, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ErrorToastMessageTranslations } from '@app-shared/components/error-toast-message/error-toast-message.component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { StayInfoSearchModel, StayInformationModel } from '@modules/stay-info/models/stay-information.models';
import { OffersService } from '../../../offers/services/offers.service';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { GuestInfoService } from '@app/modules/guest-info/services/guest-information.service';
import { SAVE_GUESTINFO_ERR, CHECKIN_GA_CONSTANTS } from '../../constants/manage-stay-constants';
import { GuestInfo } from '@app/modules/guest-info/models/guest-info.model';
import { GUEST_SAVE_AUTOFILL, IN_HOUSE_SAVE_GUEST_INFO } from '@app/constants/error-autofill-constants';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { ModalHandlerService } from '@modules/offers/services/modal-handler/modal-handler.service';
import { OfferByRateCategoryModel, OffersByRateResponseModel } from '@modules/offers/models/offers.models';
import { ChangeStayService } from '@modules/offers/services/change-stay/change-stay.service';

import { GuestCheckInModel } from '@app/modules/offers/models/offers.models';
@Component({
  selector: 'app-edit-stay',
  templateUrl: './edit-stay.component.html',
  styleUrls: ['./edit-stay.component.scss']
})
export class EditStayComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @Input() state: string;
  @Input() reservationNumber: string;
  @Input() isEditView = true;

  public showAllOffers = false;
  public searchCriteria: StayInfoSearchModel;
  public originalReservationData: StayInformationModel;
  public estimatedTotal: string;
  public confirmReservationToast: Alert;
  public showConfirmationCard = false;
  public ihgRcWarning: Alert;
  public selectedTab: string;
  public saveGuestInfoErrorContent: ErrorToastMessageTranslations = SAVE_GUESTINFO_ERR;
  public guestInfoSaveError = new Subject<EmitErrorModel>();
  public searchOffersByProductError: Subject<EmitErrorModel>;
  public searchOffersByRateError: Subject<EmitErrorModel>;
  public guestInfo: GuestInfo;
  public offersErrorToast: Alert;
  public saveErrorAutoFill = GUEST_SAVE_AUTOFILL;
  private rootSubscription = new Subscription();
  public stateNames = AppStateNames;
  public isInHouseStay: boolean;
  public offersByRate: OfferByRateCategoryModel[] = [];
  public minMaxStayAlert: Alert;

  get manageStayEdit(): boolean {
    return (this.reservationNumber && this.state === this.stateNames.manageStay);
  }

  constructor(
    private route: ActivatedRoute,
    private offersService: OffersService,
    private router: Router,
    private checkInService: CheckInService,
    private guestInfoService: GuestInfoService,
    private gaService: GoogleAnalyticsService,
    private appCommonService: AppCommonService,
    private modalHandler: ModalHandlerService,
    private changeStayService: ChangeStayService
  ) {
    super();
  }

  ngOnInit() {
    this.resetServiceData();
    this.subscribeToRouteParam();
    this.subscribeToConfirmCardVisibility();
    this.subscribeToConfirmReservationToast();
    this.subscribeToShopOffers();
    this.subscribeToAdditionalWarning();
    this.subscribeToSearchOffersError();
    this.subscribeToNavigateClickHandler();
    this.subscribeDepartureRateChange();
    this.checkForInHouseEditStay();
  }

  private subscribeToNavigateClickHandler(): void {
    this.rootSubscription.add(this.guestInfoService.navigate$.subscribe(() => {
      this.navigateToCheckIn();
      this.gaService.trackEvent(CHECKIN_GA_CONSTANTS.category, CHECKIN_GA_CONSTANTS.action, CHECKIN_GA_CONSTANTS.label);
    }));
  }

  private subscribeToRouteParam(): void {
    if (!this.manageStayEdit) {
      this.rootSubscription.add(this.route.params.subscribe(params => {
        this.reservationNumber = params['reservationNum'];
      }));
    }
  }

  private checkForInHouseEditStay() {
    this.isInHouseStay = this.offersService.checkForManageStayEdit();
    if (this.isInHouseStay) {
      this.rootSubscription.add(this.offersService.getCutKeyTriggerObservable().subscribe(
        data => {
          this.modalHandler.openCutKeysModal(data).subscribe(
            () => this.handleManageStayNavigation(true),
            () => this.handleManageStayNavigation(true));
        }));
      this.saveErrorAutoFill = IN_HOUSE_SAVE_GUEST_INFO as any;
    }
  }

  private subscribeToConfirmCardVisibility(): void {
    this.rootSubscription.add(this.offersService.currentMessage.subscribe((data: boolean) => {
      if (this.state === AppStateNames.manageStay) {
        this.confirmReservationToast = this.guestInfoService.getSaveSuccessToast();
      } else {
        this.confirmReservationToast = this.guestInfoService.getSuccessToast();
      }
      this.setUpdatedTotalEstimate();
      this.showConfirmationCard = data;
      if (this.showConfirmationCard) {
        this.resetServiceData();
      }
    }));
  }
  
  private setUpdatedTotalEstimate(){
    this.offersService.updatedReservationData.subscribe((data: GuestCheckInModel) => {
      if(data){
        this.setEstimatedTotal(data.reservationData.ratesInfo.totalAmountAfterTax);
      }        
    });
  }

  private subscribeToConfirmReservationToast(): void {
    this.rootSubscription.add(this.guestInfoService.currentMessage.subscribe((data: Alert) => {
      if (data) {
        this.confirmReservationToast = data;
        this.showConfirmationCard = this.confirmReservationToast.type === AlertType.Success;
        if (this.showConfirmationCard) {
          this.resetServiceData();
        }
      }
    }));
  }

  private subscribeToShopOffers(): void {
    this.rootSubscription.add(this.offersService.isShowOffers.subscribe((showOffers: boolean) => {
      this.showAllOffers = showOffers;
      if (showOffers) {
        this.resetOffersError();
      }
    }));
  }

  private subscribeToAdditionalWarning(): void {
    this.rootSubscription.add(this.guestInfoService.additioanlWarningMessage.subscribe((data) => {
      this.ihgRcWarning = data ? data : null;
      if (this.ihgRcWarning != null) {
        this.guestInfoSaveError.next(null);
      }
    }));
  }

  private subscribeToSearchOffersError(): void {
    this.rootSubscription.add(this.offersService.searchOffersError$.subscribe((alert: Alert) => {
      this.offersErrorToast = alert;
    }));
  }

  private subscribeDepartureRateChange(): void {
    this.rootSubscription.add(this.changeStayService.departureRateChange.subscribe(searchCriteria => {
      this.getOffersByRate(searchCriteria);
    }));
  }

  public handleStayInfoSearch(searchCriteria: StayInfoSearchModel): void {
    if (this.changeStayService.isReducedStayApplicable(searchCriteria.departureDate, this.originalReservationData)) {
      this.changeStayService.openConfirmationModal(searchCriteria, this.originalReservationData);
      this.setEstimatedTotal(this.offersByRate[0].products[0].rateInfo.totalAmountAfterTax);
    } else {
      this.searchCriteria = searchCriteria;
    }
  }

  public handleReservationData(reservationData: StayInformationModel): void {
    this.originalReservationData = reservationData;
  }

  private getOffersByRate(searchCriteria: StayInfoSearchModel, displaySpinner?: boolean): void {
    this.rootSubscription.add(this.changeStayService.getOffersByRate(searchCriteria, displaySpinner)
      .subscribe((response: OffersByRateResponseModel) => {
        if (response && response.offersByRateCategory && response.offersByRateCategory[0]) {
          this.offersByRate = response ? response.offersByRateCategory : [];
          this.minMaxStayAlert = this.changeStayService.getMinMaxStayAlert(searchCriteria.departureDate, this.originalReservationData);
        }
      }));
  }

  public setEstimatedTotal(estTotal: string): void {
    this.estimatedTotal = estTotal;
  }

  private navigateToCheckIn(): void {
    this.checkInService.updateReservationNumber(this.reservationNumber);
    this.guestInfoService.guestInfoChangeSubject.next(false);
    this.offersService.showAllOffers(false);
    this.router.navigate(['/check-in']);
  }

  public handleTabChange(tabId: string): void {
    this.selectedTab = tabId;
    if (tabId === 'stayInfo') {
      this.offersService.showAllOffers(false);
      this.showAllOffers = false;
      super.resetSpecificError(this.guestInfoSaveError);
    } else if (tabId === 'guestInfo') {
      this.resetOffersError();
    }
  }

  public saveGuestInfo(guestInfo: GuestInfo) {
    this.guestInfo = guestInfo;
    this.guestInfoService.saveGuestInformation(this.guestInfo)
      .subscribe((response: any) => {
        if (!super.checkIfAnyApiErrors(response, null, this.guestInfoSaveError)) {
          this.guestInfoService.triggerCancelEditMode({ editMode: false, refreshRequired: true });
          this.guestInfoService.showSuccessMessage(this.state);
          this.rootSubscription.add(this.offersService.setReservationData(response));
          this.resetServiceData();
        } else {
          this.guestInfoService.guestInfoChangeSubject.next(true);
          super.processHttpErrors(null, this.guestInfoSaveError);
        }
      }, error => {
        this.guestInfoService.guestInfoChangeSubject.next(true);
        super.processHttpErrors(error, this.guestInfoSaveError);
      });
    this.guestInfoService.warningMessageSource.next(null);
  }

  public handleManageStayNavigation(refreshRequired = false) {
    this.offersService.changeMessage(true);
    window.scrollTo(0, 0);
    this.guestInfoService.triggerCancelEditMode({ editMode: false, refreshRequired });
  }

  resetServiceData() {
    this.guestInfoService.updatedIHGNo.next(null);
    this.guestInfoService.guestInfoChangeSubject.next(false);
    this.guestInfoService.warningMessageSource.next(null);
    this.guestInfoService.messageSource.next(null);
  }

  private resetOffersError(): void {
    this.offersService.resetOffersError();
    this.offersErrorToast = null;
  }

  ngOnDestroy() {
    this.resetServiceData();
    this.offersService.changeMessage(false);
    this.offersService.resetOffersError();
    this.rootSubscription.unsubscribe();
    this.appCommonService.flushReservationNumber();
  }
}
