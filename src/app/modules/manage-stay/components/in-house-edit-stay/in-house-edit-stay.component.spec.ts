import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClipboardModule } from 'ngx-clipboard';

import { InHouseEditStayComponent } from './in-house-edit-stay.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { InHouseEditStayService } from '@modules/manage-stay/services/in-house-edit-stay-service/in-house-edit-stay.service';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';

describe('InHouseEditStayComponent', () => {
  let component: InHouseEditStayComponent;
  let fixture: ComponentFixture<InHouseEditStayComponent>;
  let guestInfoService: GuestInfoService;
  let inHouseEditService: InHouseEditStayService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: { openAutofilledReportIssueModal: (t): Observable<string> => of(t) }
      })
      .configureTestingModule({
        imports: [TranslateModule.forRoot(), HttpClientTestingModule, RouterTestingModule, ClipboardModule],
        declarations: [InHouseEditStayComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [GoogleAnalyticsService, UserService, ConfirmationModalService, ReportIssueService, InHouseEditStayService,
          GuestInfoService]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InHouseEditStayComponent);
    component = fixture.componentInstance;
    guestInfoService = TestBed.get(GuestInfoService);
    inHouseEditService = TestBed.get(InHouseEditStayService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the edit stay component', async(() => {
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('app-edit-stay')).not.toBe(null);
  }));

  it('should have Stay component', async(() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-stay-information')).toBeDefined();
  }));

  it('should have Guest component', async(() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-guest-information')).toBeDefined();
  }));

  it('should display warning toast message', () => {
    const getUnassignedGuestsSpy = spyOn(TestBed.get(InHouseEditStayService), 'getUnassignedGuestWarning').and.callThrough();
    component.unassignedGuestsData = ['23456789 LASTNAME, FIRSTNAME 01NOV2019'];
    expect(component.unassignedGuestsToast).toBeNull();

    guestInfoService.showSuccessMessage();
    fixture.detectChanges();
    expect(getUnassignedGuestsSpy).toHaveBeenCalledWith(component.unassignedGuestsData);
    expect(component.unassignedGuestsToast).not.toBeNull();
  });

  it('should create clipboard multiple records', () => {
    const createClipboardDataSpy = spyOn(TestBed.get(InHouseEditStayService), 'createClipboardData').and.callThrough();
    const conflict = {
      'firstName': 'AYUSH',
      'lastName': 'TEST',
      'reservationNumber': '22401014',
      'badges': [],
      'checkOutDate': null,
      'roomNumber': '100',
      'roomType': 'KSTG',
      'checkInDate': '2019-12-10',
      'reservationStatus': '',
      'pmsConfirmationNumber': '483095',
      'doNotMoveRoom': true
    };
    component.unassignedGuestsData = ['23456789 LASTNAME, FIRSTNAME 01NOV2019'];

    inHouseEditService.setUnassignedGuestEvent(conflict);
    fixture.detectChanges();
    expect(createClipboardDataSpy).toHaveBeenCalledWith(conflict);
    expect(component.unassignedGuestsData).toEqual(['23456789 LASTNAME, FIRSTNAME 01NOV2019', '22401014 TEST, AYUSH 10DEC2019']);
  });
});
