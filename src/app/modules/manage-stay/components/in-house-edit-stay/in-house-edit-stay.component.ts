import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { Alert } from 'ihg-ng-common-core';

import { GuestInfo } from '@modules/guest-info/models/guest-info.model';
import { ReservationInfoModel } from '@modules/manage-stay/models/reservation-info.model';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';
import { EditStayComponent } from '@modules/manage-stay/components/edit-stay/edit-stay.component';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { OffersService } from '@modules/offers/services/offers.service';
import { InHouseEditStayService } from '@modules/manage-stay/services/in-house-edit-stay-service/in-house-edit-stay.service';
import { RoomConflict } from '@modules/offers/models/room-conflict.interface';

@Component({
  selector: 'app-in-house-edit-stay',
  templateUrl: './in-house-edit-stay.component.html',
  styleUrls: ['./in-house-edit-stay.component.scss']
})
export class InHouseEditStayComponent implements OnInit, OnChanges, OnDestroy {
  public unassignedGuestsData = [];
  public unassignedGuestsToast: Alert;

  @ViewChild(EditStayComponent) editSay: EditStayComponent;
  @Input() guestInformation: ReservationInfoModel;
  @Input() reservationData: ReservationDataModel;
  @Input() reservationNumber: string;
  @Input() state: string;
  @Input() isEditView = false;
  @Output() editStayChange: EventEmitter<{ editMode: boolean, refreshRequired: boolean }> = new EventEmitter();
  @Output() saveGuestInformation: EventEmitter<GuestInfo> = new EventEmitter();

  private inHouseEditSubscriptions$ = new Subscription();

  constructor(private guestInfo: GuestInfoService,
              private offersService: OffersService,
              private router: Router,
              private inHouseEditService: InHouseEditStayService) {
  }

  ngOnInit() {
    this.addCancelSubscription();
    this.addBackToGuestListSubscription();
    this.subscribeUnassignedGuestsEvent();
    this.subscribeToConfirmReservation();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.reservationData && !changes.reservationData.isFirstChange()) {
      this.offersService.setInHouseReservationData(this.reservationData);
      this.unassignedGuestsData = [];
    }
  }

  handleEditToggle(editMode: boolean) {
    this.offersService.changeMessage(false);
    this.editStayChange.emit({ editMode, refreshRequired: false });
  }

  ngOnDestroy() {
    this.inHouseEditSubscriptions$.unsubscribe();
  }


  private addCancelSubscription() {
    this.inHouseEditSubscriptions$.add(
      this.guestInfo.getCancelEditObservable().subscribe(cancel => {
        this.editSay.resetServiceData();
        this.editStayChange.emit(cancel);
        this.editSay.searchCriteria = null;
      })
    );
  }

  private addBackToGuestListSubscription() {
    this.inHouseEditSubscriptions$.add(
      this.guestInfo.getGuestListNavigationObservable().subscribe(cancel => {
        this.navigateToGuestList();
      })
    );
  }

  private navigateToGuestList() {
    this.router.navigate(['/guest-list-details']).then();
  }

  private subscribeUnassignedGuestsEvent() {
    this.inHouseEditSubscriptions$.add(this.inHouseEditService.unassignedGuestEvent.subscribe((conflict: RoomConflict) => {
      this.unassignedGuestsData.push(this.inHouseEditService.createClipboardData(conflict));
    }));
  }

  private subscribeToConfirmReservation(): void {
    this.inHouseEditSubscriptions$.add(this.guestInfo.currentMessage.subscribe((data: Alert) => {
      if (this.unassignedGuestsData.length) {
        this.unassignedGuestsToast = this.inHouseEditService.getUnassignedGuestWarning(this.unassignedGuestsData);
      } else {
        this.unassignedGuestsToast = null;
      }
    }));
  }
}
