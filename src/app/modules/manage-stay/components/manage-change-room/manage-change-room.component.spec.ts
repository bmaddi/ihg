import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { cloneDeep } from 'lodash';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

import { ManageChangeRoomComponent } from './manage-change-room.component';
import { MOCK_RESERVATION_DATA_RESPONSE } from '@modules/manage-stay/mocks/manage-stay-mock';
import { TitleCasePipe } from '@angular/common';

describe('ManageChangeRoomComponent', () => {
  let component: ManageChangeRoomComponent;
  let fixture: ComponentFixture<ManageChangeRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
      providers: [TitleCasePipe],
      declarations: [ManageChangeRoomComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageChangeRoomComponent);
    component = fixture.componentInstance;
    component.reservationData = MOCK_RESERVATION_DATA_RESPONSE.reservationData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update suggestedRoomCount to 3 for Desktop View', () => {
    const updateSuggestedRoomCountSpy = spyOn<any>(component, 'updateSuggestedRoomCount').and.callThrough();
    window.dispatchEvent(new Event('resize'));
    component['updateSuggestedRoomCount'](1025);
    fixture.detectChanges();
    expect(updateSuggestedRoomCountSpy).toHaveBeenCalledWith(1025);
    expect(component.suggestedRoomCount).toBe(3);
  });

  it('should update suggestedRoomCount to 2 for Tablet Landscape View', () => {
    const updateSuggestedRoomCountSpy = spyOn<any>(component, 'updateSuggestedRoomCount').and.callThrough();
    window.dispatchEvent(new Event('resize'));
    component['updateSuggestedRoomCount'](1024);
    fixture.detectChanges();
    expect(updateSuggestedRoomCountSpy).toHaveBeenCalledWith(1024);
    expect(component.suggestedRoomCount).toBe(2);
  });

  it('should display current assigned room when edit icon for Room Assignment is clicked', () => {
    const modifyReservationDataSpy = spyOn<any>(component, 'modifyReservationData').and.callThrough();
    const mockUpdated = cloneDeep(MOCK_RESERVATION_DATA_RESPONSE.reservationData);
    mockUpdated.doNotMoveRoom = true;
    component.reservationData = mockUpdated;

    fixture.detectChanges();
    component.ngOnInit();

    expect(modifyReservationDataSpy).toHaveBeenCalled();
    expect(component.reservationData.assignedRoomNumber).toEqual(MOCK_RESERVATION_DATA_RESPONSE.reservationData.assignedRoomNumber);
    expect(component.reservationData.assignedRoomStatus).toEqual(MOCK_RESERVATION_DATA_RESPONSE.reservationData.assignedRoomStatus);
    expect(component.reservationData.doNotMoveRoom).toEqual(true);
  });

  it('should set do not move status as false and modify reservation to handle turning do not move off', () => {
    const doNotMove = false;
    const handleDoNotMoveDeactivatedSpy = spyOn<any>(component, 'handleDoNotMoveDeactivated').and.callThrough();
    component.reservationData = cloneDeep(MOCK_RESERVATION_DATA_RESPONSE.reservationData);
    component.handleRoomSettingUpdate(doNotMove);

    expect(handleDoNotMoveDeactivatedSpy).toHaveBeenCalled();
    expect(component.reservationData.doNotMoveRoom).toBeFalsy();
    expect(component.modifiedData.assignedRoomNumber).toBeNull();
    expect(component.modifiedData.assignedRoomStatus).toBeNull();
  });
});
