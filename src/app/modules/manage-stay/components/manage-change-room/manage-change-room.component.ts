import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { cloneDeep } from 'lodash';

import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { UtilityService } from '@services/utility/utility.service';

@Component({
  selector: 'app-manage-change-room',
  templateUrl: './manage-change-room.component.html',
  styleUrls: ['./manage-change-room.component.scss']
})
export class ManageChangeRoomComponent implements OnInit {
  public suggestedRoomCount = 3;
  public roomAnimDelay: string;
  public modifiedData: ReservationDataModel;
  public roomSelectorOption = {selector: 'div.change-room-assign', arbitrarySpaceValue: 50};
  private readonly animationDelay = {standard: '10ms', roomAssigned: '2000ms'};
  private readonly tabletWidth = 1024;

  @Input() reservationData: ReservationDataModel;
  @Output() roomAssignmentModified: EventEmitter<Room> = new EventEmitter();

  @HostListener('window:resize', ['$event']) getScreenSize() {
    this.updateSuggestedRoomCount(window.outerWidth);
  }

  constructor(private utilityService: UtilityService) {
  }

  ngOnInit() {
    this.setStandardAnimationDelay();
    this.updateSuggestedRoomCount(window.outerWidth);
    this.modifyReservationData();
  }

  handleRoomSettingUpdate(doNotMove: boolean) {
    if (!doNotMove) {
      this.handleDoNotMoveDeactivated();
    }
  }

  private setStandardAnimationDelay(): void {
    this.roomAnimDelay = this.animationDelay.standard;
  }

  private setRoomAssignedAnimationDelay(): void {
    this.roomAnimDelay = this.animationDelay.roomAssigned;
  }

  private modifyReservationData(): void {
    if (this.reservationData) {
      this.modifiedData = cloneDeep(this.reservationData);
      if (!this.modifiedData.doNotMoveRoom) {
        this.modifiedData.assignedRoomNumber = null;
        this.modifiedData.assignedRoomStatus = null;
        this.modifiedData.doNotMoveRoom = null;
      }
      this.modifiedData.previousAssignedRoomNumber = this.reservationData.assignedRoomNumber;
    }
  }

  private handleDoNotMoveDeactivated() {
    this.reservationData.doNotMoveRoom = false;
    this.utilityService.notifyUtilitySubjectUpdate({doNotMoveRoom: false});
    this.modifyReservationData();
  }

  private updateSuggestedRoomCount(width: number): void {
    this.suggestedRoomCount = width > this.tabletWidth ? 3 : 2;
  }

}
