import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ManageCutKeyComponent } from './manage-cut-key.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GoogleAnalyticsService, UserService, DetachedToastMessageService } from 'ihg-ng-common-core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { of, throwError } from 'rxjs';
import { MOCK_ENCODER_LIST } from '@modules/encoder/mocks/encoder-mock';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

export class MockDetachedToastMessageService {
}

describe('ManageCutKeyComponent', () => {
  let component: ManageCutKeyComponent;
  let fixture: ComponentFixture<ManageCutKeyComponent>;
  let translateService: TranslateService;
  const cutKeyResponse = true;
  let generateResponse;
  let checkResponse = true;

  class MockCheckInWorkflowService {
    public generateKey(keyCount, pmsNumber, selectedCutType) {
      if (checkResponse) {
        return of(generateResponse);
      } else {
        return throwError('error');
      }
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCutKeyComponent ],
      imports: [FormsModule, ReactiveFormsModule, NgbModule.forRoot(), HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [UserService, TranslateService,
        { provide: DetachedToastMessageService, useClass: MockDetachedToastMessageService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: CheckInWorkflowService, useClass: MockCheckInWorkflowService},
        { provide: ConfirmationModalService, useValue: {}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCutKeyComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    component.encoder = MOCK_ENCODER_LIST[1];
    fixture.detectChanges();
  });

  it('should create ManageCutKeyComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should not allow other than number through numberOnly method', () => {
    const event = new KeyboardEvent('keypress');
    Object.defineProperties(event, {
      keyCode: {value: 82}
    });
    const isTrue = component.numberOnly(event);
    expect(isTrue).toBeFalsy();
  });

  it('should pass only number through numberOnly method', () => {
    const event = new KeyboardEvent('keypress');
    Object.defineProperties(event, {
      keyCode: {value: 50}
    });
    const isTrue = component.numberOnly(event);
    expect(isTrue).toBeTruthy();
  });

  it('should not pass number that is less than 1 through validateCutKeyCount method', () => {
    const event = new KeyboardEvent('keypress', {
      'key': '0'
    });
    const count = 2;
    expect(component.validateCutKeyCount(event, count)).toBeFalsy();
  });

  it('should not pass number that is greater than 4 through validateCutKeyCount method', () => {
    const event = new KeyboardEvent('keypress', {
      'key': '7'
    });
    const count = 2;
    expect(component.validateCutKeyCount(event, count)).toBeFalsy();
  });

  it('should pass only number between 1 to 4 through validateCutKeyCount method', () => {
    const event = new KeyboardEvent('keypress', {
      'key': '2'
    });
    const count = 1;
    expect(component.validateCutKeyCount(event, count)).toBeTruthy();
  });

  it('should set keyCount as 1 when value is null on click outside through onBlurEvent method', () => {
    component.keyCount = null;
    component.onBlurEvent();
    expect(component.keyCount).toEqual(1);
  });

  it('should set keyCount as 4 when value is more than 4 on click outside through onBlurEvent method', () => {
    component.maxKeyCount = 4;
    component.keyCount = 7;
    component.onBlurEvent();
    expect(component.keyCount).toEqual(4);
  });

  it('should set keyCount as 1 when value is less than 1 on click outside through onBlurEvent method', () => {
    component.minKeyCount = 1;
    component.keyCount = 0;
    component.onBlurEvent();
    expect(component.keyCount).toEqual(1);
  });

  it('should increase keyCount if it is less than maxKeyCount through increaseValue method', () => {
    component.maxKeyCount = 4;
    component.keyCount = 2;
    component.increaseValue();
    expect(component.keyCount).toEqual(3);
  });

  it('should decrease keyCount if it is more than minKeyCount through decreaseValue method', () => {
    component.minKeyCount = 1;
    component.keyCount = 3;
    component.decreaseValue();
    expect(component.keyCount).toEqual(2);
  });

  it('generateKey service should call properly through onCutKey method and success response true should handle', () => {
    generateResponse = true;
    checkResponse = true;
    spyOn(component.isCutKeyError, 'emit');
    spyOn(component, 'handleCutKeySuccess');
    component.onCutKey();
    expect(component.isCutKeyError.emit).toHaveBeenCalled();
    expect(component.handleCutKeySuccess).toHaveBeenCalled();
  });

  it('generateKey service should call properly through onCutKey method and success response false should handle', () => {
    generateResponse = false;
    checkResponse = true;
    spyOn(component, 'handleCutKeyError');
    component.onCutKey();
    expect(component.handleCutKeyError).toHaveBeenCalled();
  });

  it('generateKey service should call properly through onCutKey method and service error should handle', () => {
    checkResponse = false;
    spyOn(component, 'handleCutKeyError');
    component.onCutKey();
    expect(component.handleCutKeyError).toHaveBeenCalled();
  });

  it('Should handle the error for generateKey service through handleCutKeyError method', () => {
    component.manageStayData = {};
    const error = throwError('error');
    spyOn(component.isCutKeyError, 'emit');
    component.handleCutKeyError(error);
    expect(component.isCutKeyError.emit).toHaveBeenCalled();
  });

  xit('should disable the cut key button if no encoder is selected', () => {
    expect(component.shouldEnableButton()).toEqual(true);
    component.encoder = null;
    expect(component.shouldEnableButton()).toEqual(false);
    component.encoder = MOCK_ENCODER_LIST[0];
    expect(component.shouldEnableButton()).toEqual(true);
  });

  it('Should disable the cut key button', () => {
    component.disableCutKey = true;
    expect(component.disableCutKey).toBeTruthy();
    const cutKey = fixture.nativeElement.querySelector('.cut-key-btn');
    expect(cutKey).toBeDefined();
    fixture.detectChanges();
    expect(cutKey.disabled).toBeTruthy();
  });

  it('Should disable key count selector', () => {
    component.disableCutKey = true;
    expect(component.disableCutKey).toBeTruthy();
    const keyCount = fixture.nativeElement.querySelector('input[data-slnm-ihg="KeysCount-SID"]');
    expect(keyCount).toBeDefined();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(keyCount.disabled).toBe(true);
    });
  });

  xit('Should check new is pre-selected', () => {
    component.modal = true;
    component.ngOnInit();
   expect(component.selectedCutType).toBe('new');
  });
});
