import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import { GoogleAnalyticsService, Alert, AlertType } from 'ihg-ng-common-core';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { CheckInWorkflowService } from '@modules/check-in/components/check-in-workflow/services/check-in-workflow.service';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import { ManageStayModel } from '@modules/manage-stay/models/manage-stay.models';
import { ManageStayEnumConstants } from '@app/modules/manage-stay/enums/manage-stay-constants.enum';
import { DetachedToastMessageService } from 'ihg-ng-common-core';
import { MANAGE_STAY_CONST } from '@modules/manage-stay/constants/manage-stay-constants';
import { TranslateService } from '@ngx-translate/core';
import {EncoderModel} from '@modules/encoder/models/encoder.model';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';
import { Subscription } from 'rxjs';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Component({
  selector: 'app-manage-cut-key',
  templateUrl: './manage-cut-key.component.html',
  styleUrls: ['./manage-cut-key.component.scss']
})
export class ManageCutKeyComponent extends AppErrorBaseComponent implements OnInit {

  @Input() manageStayData: ManageStayModel;
  @Input() pmsNumber: string;
  @Input() encoder: EncoderModel;
  @Input() disableCutKey: boolean;
  @Input() modal: boolean;
  @Output() isCutKeyError: EventEmitter<Object> = new EventEmitter();
  public keyCount = 1;
  public maxKeyCount = 4;
  public minKeyCount = 1;
  public selectedCutType: string;
  public manageStayConst = MANAGE_STAY_CONST;

  constructor(private checkInWorkflowService: CheckInWorkflowService,
              private gaService: GoogleAnalyticsService,
              private toastMessageService: DetachedToastMessageService,
              private translate: TranslateService,
              private guestInfoService: GuestInfoService) {
    super();
  }

  ngOnInit() {
    if (this.modal) {
      this.selectedCutType = 'new';
    } else {
      this.selectedCutType = 'duplicate';
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  validateCutKeyCount(event, count): boolean {
    if (this.numberOnly(event)) {
      this.keyCount = null;
      if (Number(event.key) > this.maxKeyCount) {
        this.keyCount = this.maxKeyCount;
        return false;
      } else if (Number(event.key) < this.minKeyCount) {
        this.keyCount = this.minKeyCount;
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  onBlurEvent() {
    if (this.keyCount === null) {
      this.keyCount = 1;
    } else if (this.keyCount > this.maxKeyCount) {
      this.keyCount = this.maxKeyCount;
    } else if (this.keyCount < this.minKeyCount) {
      this.keyCount = this.minKeyCount;
    }
  }

  increaseValue() {
    if (Number(this.keyCount) < this.maxKeyCount) {
      this.keyCount = this.keyCount + 1;
    }
  }

  decreaseValue() {
    if (Number(this.keyCount) > this.minKeyCount) {
      this.keyCount = this.keyCount - 1;
    }
  }

  onCutKey(): void {
    this.checkInWorkflowService.generateKey(this.keyCount, this.pmsNumber, this.selectedCutType === 'new', this.encoder.encoderId).subscribe((data: Object) => {
      if (data) {
        this.isCutKeyError.emit({
          'isError': false,
          'error': {}
        });
        if (this.modal) {
          this.handleCutKeyModalSuccess();
        } else {
          this.handleCutKeySuccess();
        }
      } else {
        this.handleCutKeyError({});
      }
    }, (error) => {
      this.handleCutKeyError(error);
    });
  }

  handleCutKeySuccess() {
    let cutKetSuccessMsg = '';
    const count = this.keyCount;
    const roomNumber = this.manageStayData.roomNumber;

    if (this.selectedCutType === MANAGE_STAY_CONST.new) {
      cutKetSuccessMsg = this.translate.instant('TOAST_CUT_KEY_SUCCESS_NEW', {'keyCount': count, 'roomNumber': roomNumber});
      this.gaService.trackEvent(ManageStayEnumConstants.CATEGORY_IN_HOUSE,
        ManageStayEnumConstants.ACTION_MANAGE_STAY,
        ManageStayEnumConstants.GA_CUT_KEY_NEW + ' (' + this.keyCount + ')');
    } else {
      cutKetSuccessMsg = this.translate.instant('TOAST_CUT_KEY_SUCCESS_DUPLICATE', {'keyCount': count, 'roomNumber': roomNumber});
      this.gaService.trackEvent(ManageStayEnumConstants.CATEGORY_IN_HOUSE,
        ManageStayEnumConstants.ACTION_MANAGE_STAY,
        ManageStayEnumConstants.GA_CUT_KEY_DUPLICATE + ' (' + this.keyCount + ')');
    }
    this.toastMessageService.success(cutKetSuccessMsg, '');
  }

  handleCutKeyError(error) {
    this.gaService.trackEvent(ManageStayEnumConstants.CATEGORY_IN_HOUSE,
      ManageStayEnumConstants.ACTION_MANAGE_STAY,
      ManageStayEnumConstants.GA_CUT_KEY_ERROR);
    this.manageStayData.isCutKeyError = true;
    this.isCutKeyError.emit({
      'isError': true,
      'error': {}
    });
  }

  shouldEnableButton = (): boolean => !this.disableCutKey && !!this.encoder && !!this.encoder.encoderId;

  private handleCutKeyModalSuccess() {
    this.isCutKeyError.emit({
      'isError': false,
      'error': {}
    });
  }
}
