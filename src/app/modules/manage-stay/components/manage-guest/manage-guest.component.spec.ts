import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {GoogleAnalyticsService} from 'ihg-ng-common-core';
import { ManageGuestComponent } from './manage-guest.component';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { RoomRateDescriptionModalComponent } from '@modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';
import {TitleCasePipe} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MAPPED_GUEST_DATA, MOCK_RESERVATION_DATA_RESPONSE, RATE_CODE_DETAILS, RATE_CODE_MODAL_PARAMS} from '@modules/manage-stay/mocks/manage-stay-mock';
import {GuestListService} from '@modules/guests/services/guest-list.service';
import {CommonTestModule} from '@modules/common-test/common-test.module';
import {setTestEnvironment} from '@modules/common-test/functions/common-test.function';
import { By } from '@angular/platform-browser';
import {GuestInfoModel} from '@modules/manage-stay/models/reservation-info.model';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { find } from 'tslint/lib/utils';
import { ManageStayService } from '../../services/manage-stay.service';
import { of, throwError } from 'rxjs';
import { CostEstimateComponent } from '@app/modules/check-in/components/cost-estimate/cost-estimate.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';

export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve('x'));
  componentInstance = RATE_CODE_MODAL_PARAMS;
}

export class MockCheckInService {
  public openEnrollmentModal() {
  }
}
describe('ManageGuestComponent', () => {
  let component: ManageGuestComponent;
  let fixture: ComponentFixture<ManageGuestComponent>;
  let guestService: GuestListService;
  let gaServ: GoogleAnalyticsService;
  let checkInService;

  const guestInfo = MAPPED_GUEST_DATA.guestInfo;
  const showLists = false;
  let check_api: boolean;
  let modalService: NgbModal;
  let modalRef: NgbModalRef;
  const mockModalRef: MockNgbModalRef = new MockNgbModalRef();

  class MockManageStayService {
    public getRateCodeData(reservationNumber) {
      return check_api ? of(RATE_CODE_DETAILS) : throwError('error');
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageGuestComponent, CostEstimateComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [NgbModule, CommonTestModule, NgbTooltipModule],
      providers:  [TitleCasePipe,
                  { provide: CheckInService, useClass: MockCheckInService },
                  {provide: ManageStayService, useClass: MockManageStayService}]
    })
    .overrideModule(BrowserDynamicTestingModule,
      {set: {entryComponents: [CostEstimateComponent]}})
      .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    gaServ = TestBed.get(GoogleAnalyticsService);
    fixture = TestBed.createComponent(ManageGuestComponent);
    component = fixture.componentInstance;
    component.guestInfo = MAPPED_GUEST_DATA.guestInfo;
    component.reservationData = {"amenityFlag":true,"prefixName":"Mr","firstName":"Saurav","lastName":"Agrawal","suffixName":"","reservationNumber":"44002733","pmsReservationNumber":"447469","pmsReservationStatus":"DUEIN","pmsUniqueNumber":"466611","arrivalTime":"","checkInDate":"2019-06-05","checkOutDate":"2019-06-07","numberOfNights":2,"ihgRcNumber":"123456","ihgRcMembership":"Spire Elite","ihgRcPoints":"5000","vipCode":"","keysCut":false,"companyName":"ARKANSAS HOSPICE INC","groupName":"IHG Concerto","rateCategoryCode":"IGBBB","roomTypeCode":"KEXG","roomTypeDescription":"1 KING BED EXECUTIVE","numberOfRooms":1,"badges":["spire","ambassador","employee"],"assignedRoomNumber":"","assignedRoomStatus":null,"numberOfAdults":1,"numberOfChildren":0,"preferences":["HIGH FLOOR","BALCONY","ROOM NEAR ELEVATOR","OCEAN VIEW","NEAR FITNESS ROOM"],"specialRequests":["Need Extra Towels and Water Bottles"],"infoItems":[],"tasks":[{"traceId":405806,"traceText":"Need Extra Towels and Water Bottles","departmentCode":"ZIF","departmentName":"CRS Information/Comments","traceTimestamp":"2019-06-06T00:00:00.0000000-04:00","isResolved":true,"resolvedTimestamp":"2019-06-04","resolvedBy":"SUPERVISOR"},{"traceId":405809,"traceText":"Need Shampoo","departmentCode":"HSK","departmentName":"House Keeping","traceTimestamp":"2019-06-04T10:43:00.0000000-04:00","isResolved":true,"resolvedTimestamp":"2019-06-04","resolvedBy":"SUPERVISOR"}],"prepStatus":"INPROGRESS","heartBeatActions":{"myHotelActions":["GuestProblem","GuestComment","SleepIssue"],"otherHotelActions":["GuestLove"]},"heartBeatComment":"Good Hospitality. Looking forward to revisit again","heartBeatImprovement":"Add more Indoor Events","otaFlag":false,"resVendorCode":"BKGTL","ratesInfo":{"currencyCode":"EUR","totalAmountBeforeTax":"755.14","totalAmountAfterTax":"808.00","totalTaxes":"52.86"},"payment":{"paymentInformation":{"card":{"code":"VS","expireDate":"0420","number":"7472471499080010"}}},"inspected":false,"paymentType":"CARD"};
    component.ga = GUEST_CONST.GA;
    guestService = TestBed.get(GuestListService);
    checkInService = TestBed.get(CheckInService);
    modalService = TestBed.get(NgbModal);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.groupCharacterLimit).toEqual(4);
    expect(component.guestNameCharLimit).toEqual(21);
  });

  it('should properly format the date', () => {
    expect(component.formatDate(guestInfo.checkInDate)).toEqual('21OCT2019');
    expect(component.formatDate(guestInfo.checkOutDate)).toEqual('22OCT2019');
  });

  it('should set the member profile', () => {
    spyOn(guestService, 'setMemberProfile');
    spyOn(gaServ, 'trackEvent');
    component.setMemberProfile(guestInfo.rcNumber);
    expect(guestService.setMemberProfile).toHaveBeenCalledTimes(1);
    expect(gaServ.trackEvent).toHaveBeenCalledWith('Manage Stay', 'Guest Profile', 'Selected');
  });

  it('should properly transform the data', () => {
    expect(component.transformData(guestInfo.groupName)).toEqual('');
    expect(component.transformData('clemson tigers')).toEqual('Clemson Tigers');
    expect(component.transformData(null)).toEqual(null);
  });

  it('should display the tooltip under correct situations', () => {
    expect(component.displayToolTip(guestInfo.groupName)).toEqual(false);
    expect(component.displayToolTip('123')).toEqual(false);
    expect(component.displayToolTip('1234')).toEqual(false);
    expect(component.displayToolTip('12345')).toEqual(true);
  });

  xit('Validate edit displays tooltip on hover ', () => {
      const editComponent = fixture.debugElement.query(By.css('.edit-room-icon'));
      editComponent.nativeElement.toBeDefined();
      editComponent.triggerEventHandler('hover', null);

      fixture.detectChanges();

      // @ts-ignore
    const tooltipAttribute = find(editComponent.nativeElement.getAttributeNames(), e => e.indexOf('ngb-tooltip') > -1);
      expect(editComponent).not.toBeNull();
      expect(editComponent.nativeElement.getAttribute(tooltipAttribute)).toEqual('LBL_EDT_STY');
  });

  it('should show Loyalty Enrollment when IhgRCNumber is blank', () => {
    component.guestInfo.rcNumber = '';
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.enrollment-container'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('.rewards-number-container'))).toBeNull();
  });

  it('should hide Loyalty Enrollment when IhgRCNumber is not blank', () => {
    component.guestInfo.rcNumber = '1234';
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.enrollment-container'))).toBeNull();
    expect(fixture.debugElement.query(By.css('.rewards-number-container'))).toBeTruthy();
  });

  it('should call checkInService openEnrollmentModal function from openEnrollmentModal function', () => {
    spyOn(checkInService, 'openEnrollmentModal');
    spyOn(gaServ, 'trackEvent');
    component.openEnrollmentModal();
    expect(checkInService.openEnrollmentModal).toHaveBeenCalled();
    expect(gaServ.trackEvent).toHaveBeenCalledWith('Manage Stay', 'Enroll', 'Selected');
  });

  it('should check EditReservationIcon is hidden ', () => {
    expect(component).toBeTruthy();
    const element = fixture.debugElement.query(By.css('far fa-edit edit-room-icon'));
    expect(element).toBeNull();
  });
  xit('Validate edit displays tooltip on hover ', () => {
    const editComponent = fixture.debugElement.query(By.css('.edit-room-icon'));
    editComponent.nativeElement.toBeDefined();
    editComponent.triggerEventHandler('hover', null);

    fixture.detectChanges();

    // @ts-ignore
    const tooltipAttribute = find(editComponent.nativeElement.getAttributeNames(), e => e.indexOf('ngb-tooltip') > -1);
      expect(editComponent).not.toBeNull();
      expect(editComponent.nativeElement.getAttribute(tooltipAttribute)).toEqual('LBL_EDT_STY');
  });

  it('Should fetch Rate code data from getRateCodeDetails and Call openDescriptionModal function', () => {
    check_api = true;
    component.reservationData = MOCK_RESERVATION_DATA_RESPONSE.reservationData;
    spyOn(component, 'openDescriptionModal');
    spyOn(gaServ, 'trackEvent');
    component.getRateCodeDetails();
    expect(component.openDescriptionModal).toHaveBeenCalledWith(null, RATE_CODE_DETAILS);
    expect(gaServ.trackEvent).toHaveBeenCalledWith('Manage Stay', 'Rate Category', 'Selected');
  });

  it('Should open the Rate code modal from openDescriptionModal function', () => {
    this.data = RATE_CODE_DETAILS;
    component.reservationData = MOCK_RESERVATION_DATA_RESPONSE.reservationData;
    spyOn(modalService, 'open').and.returnValue(mockModalRef);
    component.openDescriptionModal(null, RATE_CODE_DETAILS);
    expect(modalService.open).toHaveBeenCalledWith(RoomRateDescriptionModalComponent,
                                { windowClass: 'large-modal', backdrop: 'static', keyboard: false });
  });

  it('should track GA on selecting cost estimate', () => {    
    spyOn(gaServ, 'trackEvent');
    component.openCostEstimate();    
    expect(gaServ.trackEvent).toHaveBeenCalledWith('Guest List - In House', 'Manage Stay', 'Cost Estimate');
  });

  it('On open cost estimate modal ', async(() => {
    fixture.whenStable().then(() => {      
        modalRef = modalService.open(CostEstimateComponent);
        spyOn(gaServ, 'trackEvent');
        spyOn(modalService, 'open').and.returnValue(modalRef);        
        component.openCostEstimate();
        fixture.detectChanges();
        expect(gaServ.trackEvent).toHaveBeenCalledWith('Guest List - In House', 'Manage Stay', 'Cost Estimate');
        fixture.whenStable().then(() => {
          expect(modalRef).not.toBe(null)
        });
    });
  }));

});
