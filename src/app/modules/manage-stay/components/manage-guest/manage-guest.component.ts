import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { GuestInfoModel } from '@modules/manage-stay/models/reservation-info.model';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { TitleCasePipe } from '@angular/common';
import * as moment from 'moment';
import { UtilityService } from '@services/utility/utility.service';
import { ManageStayService } from '../../services/manage-stay.service';
import { RoomRateDescriptionModalComponent } from '@modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';
import { RateModalInputModel, WarningModalInputModel, RateCategoryModel, RateCategoryDataModel } from '@modules/offers/models/offers.models';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { CostEstimateComponent } from '@app/modules/check-in/components/cost-estimate/cost-estimate.component';

@Component({
  selector: 'app-manage-guest',
  templateUrl: './manage-guest.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./manage-guest.component.scss']
})
export class ManageGuestComponent extends AppErrorBaseComponent implements OnInit {

  @Input() guestInfo: GuestInfoModel;
  @Input() reservationData: ReservationDataModel;
  @Output() tabSelect = new EventEmitter<string>();
  @Output() emitChange: EventEmitter<boolean> = new EventEmitter();

  ga = GUEST_CONST.GA;
  groupCharacterLimit = 4;
  guestNameCharLimit = 21;
  showLists = false;

  constructor(private gaService: GoogleAnalyticsService,
    private guestListService: GuestListService,
    public utility: UtilityService,
    private titleCasePipe: TitleCasePipe,
    private checkInService: CheckInService,
    private modalService: NgbModal,
    private manageStayService: ManageStayService) {
    super();
  }

  displayToolTip(data: string): boolean {
    return data.length > this.groupCharacterLimit;
  }

  ngOnInit() {
  }

  formatDate = (date: string) => moment(date).format('DDMMMYYYY').toUpperCase();

  transformData(dataParam: string): string {
    return this.titleCasePipe.transform(dataParam);
  }

  setMemberProfile(loyaltyId: string) {
    this.gaService.trackEvent(this.ga.MANAGE_STAY, this.ga.GUEST_PROFILE, this.ga.SELECTED);
    this.guestListService.setMemberProfile(+loyaltyId);
  }

  public openEnrollmentModal(): void {
    this.checkInService.openEnrollmentModal(this.reservationData);
    this.gaService.trackEvent(this.ga.MANAGE_STAY,
      this.ga.ENROLL,
      this.ga.SELECTED);
  }

  public handleEditStay() {
    this.showLists = true;
    this.emitChange.emit(this.showLists);
  }


  handleEditReservation() {
    this.showLists = true;
    this.emitChange.emit(this.showLists);
  }
  openCostEstimate() {
    this.gaService.trackEvent(this.ga.GA_GUEST_LISIT_CAT, this.ga.GA_MANAGE_STAY_ACTION,
      this.ga.GA_COST_ESTIMATE_LBL);
    const modal = this.modalService.open(CostEstimateComponent, {windowClass: 'cost-estimate'});
    const modalComponent = modal.componentInstance as CostEstimateComponent;
    modalComponent.currencyCode = this.reservationData.ratesInfo.currencyCode;
    modalComponent.reservationNumber = this.reservationData.reservationNumber;
    modalComponent.reportIssueTopicVal = 'Manage Stay';
  }
  public getRateCodeDetails() {
    this.gaService.trackEvent(this.ga.MANAGE_STAY, this.ga.RATE_CATEGORY, this.ga.SELECTED);
    const rateDetailsData = {
      'checkInDate': this.reservationData.checkInDate,
      'checkOutDate': this.reservationData.checkOutDate,
      'ihgRcNumber': this.reservationData.ihgRcNumber,
      'roomTypeCode': this.reservationData.roomTypeCode,
      'rateCategoryCode': this.reservationData.rateCategoryCode
    };
    this.manageStayService.getRateCodeData(rateDetailsData).subscribe((response) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.openDescriptionModal(null, response);
      } else {
        this.openDescriptionModal(rateDetailsData, null, true);
      }
    }, (error) => {
      this.processHttpErrors(error);
      this.openDescriptionModal(rateDetailsData, null);
    });
  }

  public openDescriptionModal(serviceDetails: RateCategoryDataModel, data: RateCategoryModel, noData: boolean = false) {
    const modal = this.modalService.open(RoomRateDescriptionModalComponent,
      { windowClass: 'large-modal', backdrop: 'static', keyboard: false });
    modal.componentInstance.topic = 'Guest List - In House';
    modal.componentInstance.roomCode = this.reservationData.roomTypeCode;
    modal.componentInstance.roomDesc = this.reservationData.roomTypeDescription;
    modal.componentInstance.arrivalDate = this.reservationData.checkInDate;
    modal.componentInstance.departureDate = this.reservationData.checkOutDate;
    modal.componentInstance.isOnlyRateInfo = true;
    modal.componentInstance.roomName = '';

    if (data) {
      modal.componentInstance.rateName = data.name;
      modal.componentInstance.rateCode = data.code;
      modal.componentInstance.pointsEligible = data.pointsEligible;
      modal.componentInstance.rateInfo = data.rateInfo;
    } else {
      modal.componentInstance.noData = noData;
      modal.componentInstance.noRateDetailsErrorEvent = this.emitError;
      modal.componentInstance.rateCodeData = serviceDetails;
    }
    return modal;
  }
}
