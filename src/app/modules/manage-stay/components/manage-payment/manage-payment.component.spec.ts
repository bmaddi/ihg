import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ManagePaymentComponent } from './manage-payment.component';
import { ManageStayService } from '../../services/manage-stay.service';
import { GET_PAYMENT_INFO } from '../../mocks/manage-payment-mock';
import { of, throwError } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { CommonTestModule } from '@app/modules/common-test/common-test.module';
import { Component, Output, Input, NO_ERRORS_SCHEMA } from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';
import { NoPostModule } from '@modules/no-post/no-post.module';

@Component({
  selector: 'app-service-error',
  template: ''
})

export class StubServiceErrorComponent {
  @Input() errorEvent;
  @Input() cardType;
  @Input() reportIssueAutoFill;
  @Output() refreshAction;
}

describe('ManagePaymentComponent', () => {
  let component: ManagePaymentComponent;
  let fixture: ComponentFixture<ManagePaymentComponent>;
  let check_api: boolean;

  class MockManageStayService {
    public getPaymentDetails(reservationNumber) {
      if (check_api) {
        return of(GET_PAYMENT_INFO);
      } else {
        return throwError('error');
      }
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagePaymentComponent, StubServiceErrorComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonModule, NgbModule, HttpClientTestingModule, IhgNgCommonKendoModule, CommonTestModule, NoPostModule],
      providers: [
        {provide: ManageStayService, useClass: MockManageStayService},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePaymentComponent);
    component = fixture.componentInstance;
    check_api = true;
    fixture.detectChanges();
  });

  it('should create ManagePaymentComponent component and no edit button available.', () => {
    expect(component).toBeTruthy();
    const editPayment = fixture.debugElement.query(By.css('pay-edit'));
    expect(editPayment).toBeNull();
  });

  it('should test refresh method', () => {
    spyOn(component, 'getPayment');
    component.refresh();
    expect(component.getPayment).toHaveBeenCalled();
  });

  it('should test getPayment method success case', () => {
    component.paymentDetails = undefined;
    component.otherType = undefined;
    component.getPayment();
    expect(component.paymentDetails).toBe(GET_PAYMENT_INFO);
    expect(component.otherType).toBeFalsy();
    expect(component.cardType).toBeDefined();
  });

  it('should test getPayment method backend error case', async () => {
    check_api = false;
    spyOn(component, 'processHttpErrors');
    component.getPayment();
    expect(component.processHttpErrors).toHaveBeenCalled();
  });

  it('should test doPayment method', () => {
    component.otherType = undefined;
    spyOn(component.paymentProcessing, 'emit');
    component.doPayment();
    expect(component.paymentProcessing.emit).toHaveBeenCalled();
  });

  it('should test doPost component present', () => {
    component.ngOnInit();
    fixture.detectChanges();
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="ManagePayment-SID"]'));
    expect(container).not.toBeNull();
    const noPostElement = fixture.debugElement.query(By.css('no-Post'));
    expect(noPostElement).toBeDefined();
  });

  it('should test doPost component present for inhouse other payment type', () => {
    component.ngOnInit();
    component.otherType = true;
    fixture.detectChanges();
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="ManagePayment-SID"]'));
    expect(container).not.toBeNull();
    const noPostElement = fixture.debugElement.query(By.css('no-Post'));
    expect(noPostElement).toBeDefined();
  });

  it('should disable the payment edit button', () => {
    component.disablePayment = true;
    component.enableEditPayment = true;
    fixture.detectChanges();
    const editPayment = fixture.nativeElement.querySelector('[data-slnm-ihg="PaymentEdit-SID"]');
    expect(editPayment).toBeDefined();
    expect(editPayment.disabled).toBeTruthy();
  });

  it('should hide the edit icon for payment card', () => {
    component.enableEditPayment = false;
    fixture.detectChanges();

    const editIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="PaymentEdit-SID"]'));
    expect(editIcon).toBeNull();
  });
});
