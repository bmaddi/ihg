import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';

import { ManageStayService } from '../../services/manage-stay.service';
import { PaymentDetails, CardType } from '@modules/manage-stay/models/manage-stay.models';
import { MANAGE_STAY_CONST } from '@modules/manage-stay/manage-stay.constants';
import { CARD_CODE_ICON_MASK_MAP } from '@modules/check-in/components/check-in-workflow/components/check-in-payment/constants/check-in-payment-constants';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { MANAGE_PAYMENT_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';

@Component({
  selector: 'app-manage-payment',
  templateUrl: './manage-payment.component.html',
  styleUrls: ['./manage-payment.component.scss']
})
export class ManagePaymentComponent extends AppErrorBaseComponent implements OnInit {

  @Input() focusedCard: boolean;
  @Input() reservationNumber: string;
  @Input() reservationData: ReservationDataModel;
  @Input() disablePayment: boolean;
  @Output() paymentProcessing = new EventEmitter<boolean>();

  enableEditPayment = false;
  paymentActivated: boolean;
  otherType = false;
  paymentDetails: PaymentDetails;
  cardType: CardType;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  reportIssueAutoFill = MANAGE_PAYMENT_AUTOFILL;
  public cardCodeIconMaskMap = CARD_CODE_ICON_MASK_MAP;

  constructor(private manageStayService: ManageStayService) {
    super();
  }

  ngOnInit() {
    this.getPayment();
  }

  getPayment() {
    this.manageStayService.getPaymentDetails(this.reservationNumber).subscribe((response: PaymentDetails) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.paymentDetails = response;
        this.otherType = response.paymentType === 'Other';
        this.cardType  = this.cardCodeIconMaskMap[response.cardType];
      }
    }, (error) => {
      this.processHttpErrors(error);
    });
  }

  doPayment() {
    this.paymentActivated = true;
    this.paymentProcessing.emit(this.paymentActivated);
    console.log('Payment Processing');

    this.paymentActivated = false;
    this.paymentProcessing.emit(this.paymentActivated);
    console.log('Payment Processed');
  }

  refresh() {
    this.getPayment();
  }
}
