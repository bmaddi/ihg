import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';
import { of, throwError } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ClipboardModule } from 'ngx-clipboard';

import { DetachedToastMessageService, GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { ManageRoomAssignmentComponent } from './manage-room-assignment.component';
import { ManageStayService } from '../../services/manage-stay.service';
import { ROOM_ASSIGNMENT_DETAILS_RESPONSE } from '../../mocks/manage-tasks-mock';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { MOCK_ENCODER_LIST } from '@modules/encoder/mocks/encoder-mock';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { ManageStayEnumConstants } from '@modules/manage-stay/enums/manage-stay-constants.enum';
import { ManageCutKeyComponent } from '@modules/manage-stay/components/manage-cut-key/manage-cut-key.component';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) {
  }
}

export class MockUtilityService {
  public checkNonWhiteSpaceCharacterWithMinLength(searchText: string, minCharLength: number): boolean {
    return !(searchText && minCharLength !== undefined && searchText.trim().length >= minCharLength);
  }
}

export class MockDetachedToastMessageService {
}

describe('ManageRoomAssignmentComponent', () => {
  let component: ManageRoomAssignmentComponent;
  let fixture: ComponentFixture<ManageRoomAssignmentComponent>;
  let checkResponse: boolean;
  let roomsService: RoomsService;

  class MockManageStayService {
    public getManageStayData(keyCount, pmsNumber, selectedCutType) {
      if (checkResponse) {
        return of(ROOM_ASSIGNMENT_DETAILS_RESPONSE);
      } else {
        return throwError('error');
      }
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageRoomAssignmentComponent, ManageCutKeyComponent],
      imports: [FormsModule, ReactiveFormsModule, NgbModule.forRoot(), HttpClientTestingModule, TranslateModule.forRoot(), ClipboardModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserService, TranslateService, ConfirmationModalService, RoomsService,
        { provide: DetachedToastMessageService, useClass: MockDetachedToastMessageService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: ManageStayService, useClass: MockManageStayService }
      ]
    })
      .overrideComponent(ManageCutKeyComponent, {
        set: {
          template: `<div>ManageCutKeyComponent</div>`
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRoomAssignmentComponent);
    component = fixture.componentInstance;
    component['selectedHKStatus'] = '';
    roomsService = TestBed.get(RoomsService);
    fixture.detectChanges();
  });

  it('should create ManageRoomAssignmentComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should call getInHouseGuestData from ngOnInit', () => {
    spyOn(component, 'getInHouseGuestData');
    component.ngOnInit();
    expect(component.changeRoomView).toBeFalsy();
    expect(component.getInHouseGuestData).toHaveBeenCalled();
  });

  it('should fetch room assignment details through getInHouseGuestData and handle transaction successfull', () => {
    checkResponse = true;
    component.hasErrorRoomData = false;
    spyOn(component, 'getInHouseGuestData');
    component.getInHouseGuestData('123456');
    expect(component.manageStayData).toBeDefined();
    expect(component.hasErrorRoomData).toBeFalsy();
  });

  it('should fetch room assignment details through getInHouseGuestData and handle transaction error', () => {
    checkResponse = false;
    spyOn(component, 'handleErrors');
    component.getInHouseGuestData('123456');
    expect(component.manageStayData).toEqual({});
    expect(component.handleErrors).toHaveBeenCalled();
  });

  it('should handle error through handleErrors function', () => {
    spyOn(component, 'processHttpErrors');
    const error = throwError('error');
    component.handleErrors(error);
    expect(component.processHttpErrors).toHaveBeenCalled();
    expect(component.hasErrorRoomData).toBeTruthy();
  });

  it('should call getInHouseGuestData function from refresh function', () => {
    spyOn(component, 'getInHouseGuestData');
    component.refresh();
    expect(component.getInHouseGuestData).toHaveBeenCalled();
  });

  it('should handle openRoomChange function', () => {
    component.focusedCard = false;
    spyOn(component.changeRoom, 'emit');
    component.openRoomChange();
    expect(component.changeRoom.emit).toHaveBeenCalled();
    expect(component.changeRoomView).toBeTruthy();
  });

  it('should handle closeRoomChange function', () => {
    spyOn(component.changeRoom, 'emit');
    component.closeRoomChange();
    expect(component.changeRoom.emit).toHaveBeenCalled();
    expect(component.changeRoomView).toBeFalsy();
  });

  it('should handle isCutKeyError function when isError is true', () => {
    spyOn(Object.getPrototypeOf(Object.getPrototypeOf(component)), 'processHttpErrors');
    const errorObj = throwError('error');
    const error = { 'isError': true, 'error': errorObj };
    component.isCutKeyError(error);
    expect(Object.getPrototypeOf(component).processHttpErrors).toHaveBeenCalled();
  });

  it('should handle isCutKeyError function when isError is false', () => {
    spyOn(Object.getPrototypeOf(Object.getPrototypeOf(component)), 'resetSpecificError');
    const errorObj = throwError('error');
    const error = { 'isError': false, 'error': errorObj };
    component.isCutKeyError(error);
    expect(Object.getPrototypeOf(component).resetSpecificError).toHaveBeenCalled();
  });

  it('should populate the encoder value in manage room component', () => {
    expect(component.selectedEncoder).toBe(null);

    component.encoderSelected(MOCK_ENCODER_LIST[2]);
    expect(component.selectedEncoder).toEqual(MOCK_ENCODER_LIST[2]);

    component.encoderSelected(MOCK_ENCODER_LIST[0]);
    expect(component.selectedEncoder).toEqual(MOCK_ENCODER_LIST[0]);

    component.encoderSelected(MOCK_ENCODER_LIST[1]);
    expect(component.selectedEncoder).toEqual(MOCK_ENCODER_LIST[1]);
  });

  it('should display tooltip for change room', () => {
    const changeRoomIcon = fixture.debugElement.query(By.css('.edit-room-assignment'));
    expect(changeRoomIcon.nativeElement).toBeDefined();

    changeRoomIcon.triggerEventHandler('mouseenter', null);

    fixture.detectChanges();
    const tooltipElement = document.querySelector('ngb-tooltip-window');
    expect(tooltipElement).toBeDefined();
    expect(tooltipElement.textContent.trim()).toBe('LBL_CHG_ROOM');
  });

  it('should display change Room Assignment panel', () => {
    const changeRoomIcon = fixture.debugElement.query(By.css('.edit-room-assignment'));
    expect(changeRoomIcon.nativeElement).toBeDefined();
    changeRoomIcon.triggerEventHandler('click', null);

    fixture.detectChanges();
    const changeRoomView = fixture.debugElement.query(By.css('.room-assign-header'));
    expect(changeRoomView.nativeElement).toBeDefined();
    expect(changeRoomView.nativeElement.textContent).toContain('LBL_RM_ASGMT');
  });

  it('should update manageStayData on successful room assignment', () => {
    const refresh = spyOn(component, 'refresh').and.callThrough();
    const closeRoomEvent = spyOn<any>(component, 'closeRoomChange').and.callThrough();
    const saveRoomStatusSpy = spyOn<any>(roomsService, 'saveRoomStatus').and.callThrough();
    checkResponse = true;
    component.changeRoomView = false;
    component.ngOnInit();
    fixture.detectChanges();

    const cutKeyElement = fixture.debugElement.query(By.css('app-manage-cut-key'));
    expect(cutKeyElement).toBeTruthy();
    const cutKeyComponent = cutKeyElement.componentInstance;

    expect(component.manageStayData).toBeTruthy();
    component.roomAssignmentChange(<Room>{
      roomNumber: '2025',
      roomType: 'WHDA'
    });
    fixture.detectChanges();
    expect(cutKeyComponent).toBeDefined();
    expect(saveRoomStatusSpy).toHaveBeenCalled();
  });

  it('Should disable the room edit button', () => {
    const changeRoom = false;
    if (!changeRoom) {
      component.disableRoom = true;
      const editRoom = fixture.nativeElement.querySelector('[data-slnm-ihg="ChangeRoom-SID"]');
      expect(editRoom).toBeDefined();
      fixture.detectChanges();
      expect(editRoom.disabled).toBeTruthy();
    }
  });

  it('should check google analytics for unassigned room in-house tab', function () {
    component['room'] = {roomNumber: '1234', roomStatus: 'Dirty', roomType: 'KDXG'};
    component['selectedRoomStatus'] = 'Dirty';

    const category = ManageStayEnumConstants.CATEGORY_IN_HOUSE;
    const action = PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT;
    const label = PrepareForArrivalsGaConstants.RM_HK_STATUS_UPDATE.concat(component['selectedRoomStatus']);
    const trackEventSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    const roomsSpy = spyOn(TestBed.get(RoomsService), 'saveRoomStatus').and.returnValue(of('Success'));

    component.roomAssignmentChange(component['room']);
    fixture.detectChanges();

    expect(roomsSpy).toHaveBeenCalled();
    expect(trackEventSpy).toHaveBeenCalledWith(category, action, label);
  });
});
