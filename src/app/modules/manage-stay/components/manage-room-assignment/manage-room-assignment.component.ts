import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { of, Subject, Subscription } from 'rxjs';
import { delay, finalize, take } from 'rxjs/operators';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ManageRoomAssignmentEvent, ManageStayModel } from '../../models/manage-stay.models';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ServiceErrorComponentConfig } from '@app/modules/shared/components/app-service-errors/service-error/service-error.component';
import { EmitErrorModel, ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import {
  CUT_KEY_ERROR_CONTENT,
  MANAGE_STAY_ROOM_ASSIGNMENT_ERROR_AUTOFILL
} from '@modules/check-in/components/check-in-workflow/constants/check-in-workflow-error-constants';
import { ManageCutKeyComponent } from '../../components/manage-cut-key/manage-cut-key.component';
import { ManageStayService } from '../../services/manage-stay.service';
import { MANAGE_CUT_KEY_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { EncoderModel } from '@modules/encoder/models/encoder.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { ManageStayEnumConstants } from '@modules/manage-stay/enums/manage-stay-constants.enum';

@Component({
  selector: 'app-manage-room-assignment',
  templateUrl: './manage-room-assignment.component.html',
  styleUrls: ['./manage-room-assignment.component.scss']
})
export class ManageRoomAssignmentComponent extends AppErrorBaseComponent implements OnInit {

  @Input() focusedCard: boolean;
  @Input() reservationNumber: string;
  @Input() pmsNumber: string;
  @Input() reservationData: ReservationDataModel;
  @Input() disableRoom: boolean;
  @Input() modal: boolean;
  @Output() changeRoom = new EventEmitter<ManageRoomAssignmentEvent>();
  @ViewChild(ManageCutKeyComponent) cutKey: ManageCutKeyComponent;

  public cutKeyAssignmentError = new Subject<EmitErrorModel>();
  public cutKeyErrorContent = CUT_KEY_ERROR_CONTENT;
  public reportIssueAutoFill = MANAGE_CUT_KEY_AUTOFILL;
  public status: string;
  public roomsDetails: Object;

  public manageStayData: ManageStayModel;
  public errorConfig: ServiceErrorComponentConfig;
  public errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  public reportIssueConfig = MANAGE_STAY_ROOM_ASSIGNMENT_ERROR_AUTOFILL.roomAssignmentError;
  public hasErrorRoomData = false;
  public changeRoomView: boolean;
  selectedEncoder: EncoderModel;

  private cutKeyError: boolean;
  private selectedRoomStatus: string;

  private inHouseSubscriptions$ = new Subscription();

  constructor(private manageStayService: ManageStayService,
              private roomsService: RoomsService,
              private googleAnalyticsService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.changeRoomView = false;
    this.getInHouseGuestData(this.reservationNumber);
    this.selectedEncoder = null;
    this.subscribeToHKStatus();
  }

  getInHouseGuestData(reservationNumber) {
    this.manageStayService.getManageStayData(reservationNumber).subscribe((response: ManageStayModel) => {
      this.hasErrorRoomData = this.checkIfAnyApiErrors(response);
      this.manageStayData = response;
    }, (error) => {
      this.manageStayData = {};
      this.handleErrors(error);
    });
  }

  handleErrors = (error) => {
    this.processHttpErrors(error);
    this.hasErrorRoomData = true;
  }

  public refresh() {
    this.getInHouseGuestData(this.reservationNumber);
  }

  openRoomChange(): void {
    if (!this.focusedCard) {
      this.changeRoomView = true;
      this.changeRoom.emit({expandedState: this.changeRoomView});
    }
  }

  closeRoomChange(room?: Room): void {
    this.changeRoomView = false;
    this.changeRoom.emit({expandedState: this.changeRoomView, room});
  }

  isCutKeyError(error): void {
    if (error.isError === true) {
      this.cutKeyError = true;
      super.processHttpErrors(error.error, this.cutKeyAssignmentError);
    } else {
      this.cutKeyError = false;
      super.resetSpecificError(this.cutKeyAssignmentError);
    }
  }

  onCutKeyRetry() {
    this.cutKey.onCutKey();
  }

  encoderSelected(encoder) {
    this.selectedEncoder = encoder as EncoderModel;
  }

  subscribeToHKStatus() {
    this.inHouseSubscriptions$.add(this.roomsService.houseKeepingStatusEvent.subscribe((status: string) => {
      this.selectedRoomStatus = status;
    }));
  }

  changeRoomStatus(room: Room) {
    this.roomsService.saveRoomStatus(room.roomNumber, this.selectedRoomStatus)
      .pipe(finalize(() => {
          this.trackGoogleAnalytics();
          this.closeRoomChange(room);
          this.refresh();
        }
      ))
      .subscribe((response: string) => {
      }, error => super.processHttpErrors(error));
  }

  roomAssignmentChange(room: Room) {
    if (room) {
      this.manageStayData = {
        ...this.manageStayData,
        ...<ManageStayModel>{roomNumber: room.roomNumber, roomType: room.roomType}
      };
      this.changeRoomStatus(room);
      setTimeout(() => this.cutKey.selectedCutType = 'new');
    }
  }

  trackGoogleAnalytics(): void {
    const category = ManageStayEnumConstants.CATEGORY_IN_HOUSE;
    this.googleAnalyticsService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.RM_HK_STATUS_UPDATE.concat(this.selectedRoomStatus));
  }
}
