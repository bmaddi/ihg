import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageStayTabsComponent } from './manage-stay-tabs.component';

import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserService, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { InHouseEditStayComponent } from '@modules/manage-stay/components/in-house-edit-stay/in-house-edit-stay.component';

describe('ManageStayTabsComponent', () => {
  let component: ManageStayTabsComponent;
  let fixture: ComponentFixture<ManageStayTabsComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        imports: [ TranslateModule.forRoot(), HttpClientTestingModule ],
        declarations: [ ManageStayTabsComponent ],
        schemas: [ NO_ERRORS_SCHEMA ],
        providers: [ GoogleAnalyticsService, UserService, ConfirmationModalService ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageStayTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have Stay component', async(() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-stay-information')).toBeDefined();
  }));

  it('should have Guest component', async(() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-guest-information')).toBeDefined();
  }));
});
