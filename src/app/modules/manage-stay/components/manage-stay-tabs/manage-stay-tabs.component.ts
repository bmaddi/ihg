import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgbTabChangeEvent, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Observable, Subject } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { StayInfoSearchModel, StayInformationModel } from '@modules/stay-info/models/stay-information.models';
import { MANAGE_STAY_CONST } from '../../manage-stay.constants';
import { GuestInfoService } from '@app/modules/guest-info/services/guest-information.service';
import { GuestInformationComponent } from '@app/modules/guest-info/components/guest-information/guest-information.component';
import { StayInformationComponent } from '@app/modules/stay-info/components/stay-information/stay-information.component';


@Component({
  selector: 'app-manage-stay-tabs',
  templateUrl: './manage-stay-tabs.component.html',
  styleUrls: ['./manage-stay-tabs.component.scss']
})

export class ManageStayTabsComponent implements OnInit {
  public hasDataChanges: boolean;
  private rootSubscription = new Subscription();
  private readonly tabNames = {
    stayInfo: 'stayInfo',
    guestInfo: 'guestInfo'
  };

  @Input() reservationNumber: string;
  @Input() state: string;
  @Output() stayInfoSearch = new EventEmitter<StayInfoSearchModel>();
  @Output() saveGuestInfo = new EventEmitter<any>();
  @Output() originalReservationData = new EventEmitter<StayInformationModel>();
  @Output() tabChange = new EventEmitter<string>();
  @Output() cancelEdit = new EventEmitter<void>();

  @ViewChild('manageStay') manageStay: NgbTabset;
  @ViewChild('guestInfo') guestInfo: GuestInformationComponent;
  @ViewChild('stayInfo') stayInfo: StayInformationComponent;

  constructor(private ga: GoogleAnalyticsService, private guestInfoService: GuestInfoService) { }

  ngOnInit() {
    this.subscribeDataChange();
    this.tabChange.emit(this.tabNames.stayInfo);
  }

  public subscribeDataChange() {
    this.rootSubscription.add(this.guestInfoService.guestInfoChangeSubject
      .subscribe(response => {
        this.hasDataChanges = response;
      }));
  }

  public handleStayInfoSearch(searchCriteria: StayInfoSearchModel): void {
    this.stayInfoSearch.emit(searchCriteria);
  }

  public saveGuestInformation(guestInfo: any): void {
    this.saveGuestInfo.emit(guestInfo);
  }

  handleReservationData(reservationData) {
    this.originalReservationData.emit(reservationData);
    if (this.stayInfo) {
      this.stayInfo.subscribeNecessaryValues();
    }
  }

  handleTabChange(event: NgbTabChangeEvent): void {
    if (event.nextId === 'guestInfo') {
      this.guestInfo.subscribeNecessaryValues();
    } else {
      this.stayInfo.subscribeNecessaryValues();
    }
    if (this.hasDataChanges) {
       event.preventDefault();
       this.openUnsavedDataWarning(event.nextId);
    } else {
      const action = event.nextId === this.tabNames.stayInfo ? MANAGE_STAY_CONST.GA.STAY_INFO : MANAGE_STAY_CONST.GA.GUEST_INFO;
      this.ga.trackEvent(MANAGE_STAY_CONST.GA.CHCKIN_MNG_STAY, action, MANAGE_STAY_CONST.GA.SLCTD);
      this.tabChange.emit(event.nextId);
    }
  }

  openUnsavedDataWarning(nextId: string) {
    this.guestInfoService.openUnsavedDataWarningModal().subscribe(
      (proceed) => {
        this.resetServiceData();

        const action = nextId === this.tabNames.stayInfo ? MANAGE_STAY_CONST.GA.STAY_INFO : MANAGE_STAY_CONST.GA.GUEST_INFO;
        this.ga.trackEvent(MANAGE_STAY_CONST.GA.CHCKIN_MNG_STAY, action, MANAGE_STAY_CONST.GA.SLCTD);
        this.manageStay.select(nextId);
        this.tabChange.emit(nextId);
      },
      (cancel) => {
        return false;
      });
  }

  resetServiceData() {
    this.hasDataChanges = false;
    this.guestInfoService.updatedIHGNo.next(null);
    this.guestInfoService.guestInfoChangeSubject.next(false);
    this.guestInfoService.warningMessageSource.next(null);
    this.guestInfoService.resetGuestInfoForm.next(true);
    this.guestInfoService.messageSource.next(null);
  }
}
