import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { cloneDeep } from 'lodash';
import {
  EnvironmentService,
  DetachedToastMessageService,
  SessionStorageService,
  IhgNgCommonCoreModule,
  BreadcrumbsService,
  ToastMessageOutletService
} from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { UserService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { ManageStayComponent } from './manage-stay.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';

import { CommonModule, TitleCasePipe} from '@angular/common';
import { environment } from '@env/environment.local-int';
import { ManageStayParams } from '@modules/guests/models/guest-list.models';
import { SingleModeEnrollmentModel, EnrollNewMemberService } from 'ihg-ng-common-pages';
import {
  MAPPED_GUEST_DATA, MOCK_MAPPED_NAME, MOCK_RESERVATION_DATA_RESPONSE, ENROLLMENT_SUCCESS,
  LOYALTY_INFO, MockSaveGuestInfo
} from '@modules/manage-stay/mocks/manage-stay-mock';
import { ClipboardService } from 'ngx-clipboard';
import { ReservationDataModel, LoyaltyInfoModel } from '@modules/check-in/models/check-in.models';
import { Observable, BehaviorSubject, Subject, of } from 'rxjs';
import { By } from '@angular/platform-browser';
import {CheckInService} from '@app/modules/check-in/services/check-in.service';
import { HttpResponse } from '@angular/common/http';
import { CHECK_IN_MOCK } from '@modules/manage-stay/mocks/check-in.mock';

export class MockCheckInService {
  public getCheckInReservationData(reservationNumber: string) {
    return of(CHECK_IN_MOCK);
  }
}
import { UtilityService } from '@services/utility/utility.service';

export class MockBreadcrumbsService {
  public setLabel(stateName: string, label: string) { }
}

export class MockEnrollNewMemberService {
  private enrollmentSuccessSource: Subject<SingleModeEnrollmentModel> = new Subject();
  public successToast = this.enrollmentSuccessSource.asObservable();
  public toastErrorResponse = this.enrollmentSuccessSource.asObservable();
}
export class MockClipboardService {}

describe('ManageStayComponent', () => {
  let component: ManageStayComponent;
  let fixture: ComponentFixture<ManageStayComponent>;
  let envService: EnvironmentService;
  let sessionService: SessionStorageService;
  let breadCrumbService: BreadcrumbsService;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ManageStayComponent],
        schemas: [NO_ERRORS_SCHEMA],
        imports: [CommonModule, HttpClientTestingModule, TranslateModule.forRoot(), IhgNgCommonCoreModule.forRoot(), RouterTestingModule
        ],
        providers: [UserService, ConfirmationModalService, TitleCasePipe, DetachedToastMessageService, SessionStorageService,
          { provide: EnrollNewMemberService, useClass: MockEnrollNewMemberService},
          { provide: ClipboardService, useClass: MockClipboardService},
          { provide: CheckInService, useClass: MockCheckInService },
          { provide: BreadcrumbsService, useClass: MockBreadcrumbsService },
          { provide: ReportIssueService, useValue: {}}]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    sessionService = TestBed.get(SessionStorageService);
    breadCrumbService = TestBed.get(BreadcrumbsService);
    spyOn(sessionService, 'getSessionStorage').and.returnValue({});
    envService = TestBed.get(EnvironmentService);
    envService.setEnvironmentConstants(environment);
    fixture = TestBed.createComponent(ManageStayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.manageRoomExpanded).toBe(false);
    expect(component.tasksExpanded).toBe(false);
    expect(component.focusedCard).toBe(false);
    expect(component.manageStayParams).toEqual({} as ManageStayParams);
    expect(component.reservationNumber).toEqual('');
    expect(component.pmsNumber).toEqual('');
  });

  it('should toggle tasks', () => {
    spyOn(component, 'setFocus');
    expect(component.tasksExpanded).toBe(false);
    component.toggleTasks(true);
    expect(component.tasksExpanded).toBe(true);
    component.toggleTasks(false);
    expect(component.tasksExpanded).toBe(false);
    expect(component.setFocus).toHaveBeenCalledTimes(2);
  });

  it('should toggle room', () => {
    spyOn(component, 'setFocus');
    expect(component.manageRoomExpanded).toBe(false);
    component.changeRoom({expandedState: true});
    expect(component.manageRoomExpanded).toBe(true);
    component.changeRoom({expandedState: false});
    expect(component.manageRoomExpanded).toBe(false);
    expect(component.setFocus).toHaveBeenCalledTimes(2);
  });

  it('should process payment', () => {
    spyOn(component, 'setFocus');
    expect(component.processingPayment).toBe(false);
    component.paymentProcessing(true);
    expect(component.processingPayment).toBe(true);
    component.paymentProcessing(false);
    expect(component.processingPayment).toBe(false);
    expect(component.setFocus).toHaveBeenCalledTimes(2);
  });

  xit('should map the guest data', () => {
    component.guestData = component.mapGuestData(MOCK_RESERVATION_DATA_RESPONSE.reservationData);
    component.guestData.guestInfo.estimatedTotal = component.guestData.guestInfo.estimatedTotal as number;
    component.guestData.guestInfo.rcPoints = component.guestData.guestInfo.rcPoints as number;
    expect(component.guestData).toEqual(MAPPED_GUEST_DATA);
  });

  it('should properly map the guest name', () => {
    expect(component.mapName(MOCK_RESERVATION_DATA_RESPONSE.reservationData)).toEqual(MOCK_MAPPED_NAME);
  });

  it('should set focus on card', () => {
    component.focusedCard = undefined;
    component.tasksExpanded = true;
    component.setFocus();
    expect(component.focusedCard).toBeTruthy();
  });

  it('should set breadcrumb label', () => {
    spyOn(breadCrumbService, 'setLabel');
    component.setBreadCrumbLabel();
    expect(breadCrumbService.setLabel).toHaveBeenCalledWith('guest-list-details', 'LBL_GST_LST_IN_HOUSE');
  });

  it('should handle the enrollment success and fetch Loyalty enrollment ', () => {
    const success = ENROLLMENT_SUCCESS;
    spyOn(component, 'getLoyaltyInformation');
    component.handleSuccessToast(success);

    expect(component.hasSuccessDisplay).toBe(true);
    expect(component.successDataToast).toEqual(success);
    expect(component.copyMembershipId).toEqual(success.membershipId);
    expect(component.getLoyaltyInformation).toHaveBeenCalledWith(success.membershipId);
  });

  it('should update guest data info after loyalty enrollment success', () => {
    component.reservationData = MOCK_RESERVATION_DATA_RESPONSE.reservationData;
    const loyaltyInfo = LOYALTY_INFO;
    component.updateGuestData(loyaltyInfo);

    expect(component.reservationData.ihgRcNumber).toEqual(loyaltyInfo.memberDetails.ihgRcNumber);
    //  expect(component.reservationData.ihgRcPoints).toEqual(loyaltyInfo.memberDetails.ihgRcPoints || 0);
    expect(component.reservationData.badges).toEqual(loyaltyInfo.memberDetails.badges &&
    loyaltyInfo.memberDetails.badges.length ? loyaltyInfo.memberDetails.badges : ['club']);
    expect(component.reservationData.firstName).toEqual(loyaltyInfo.memberDetails.firstName);
    expect(component.reservationData.lastName).toEqual(loyaltyInfo.memberDetails.lastName);
    expect(component.reservationData.prefixName).toEqual(loyaltyInfo.memberDetails.prefixName);
  });

  it('Validate edit stay loaded', () => {
    expect(component.editStayToggled).toBeFalsy();
    component.editStayToggled = true;
    if (component.editStayToggled) {
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('div.in-house.edit')).nativeElement;
      expect(element).toBeDefined();
    }
  });

  it('should validate edit stay loaded and toggle is true to disable other cards', () => {
    expect(component.editStayToggled).toBeFalsy();

    component.editStayToggled = true;
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('div.in-house.edit')).nativeElement;

    expect(element).toBeDefined();
  });

  it('should display alert if DNM is turned on for in house manage stay', () => {
    const alertMessageSpy = spyOn(TestBed.get(ToastMessageOutletService), 'info').and.callFake(() => {
    });
    const mockData = cloneDeep(MOCK_RESERVATION_DATA_RESPONSE.reservationData);
    mockData.doNotMoveRoom = true;

    component['showDoNotMoveTurnedOnMessage'](mockData);
    fixture.detectChanges();

    expect(alertMessageSpy.calls.mostRecent().args[0]).toEqual('LBL_NOTE_PMS_UNAVAILABLE');
    expect(alertMessageSpy.calls.mostRecent().args[1]).toEqual('LBL_DO_NOT_MOVE_SET_FOR_RSV');
  });

  it('should not display alert if DNM is turned on for in house manage stay', () => {
    const alertMessageSpy = spyOn(TestBed.get(ToastMessageOutletService), 'info').and.callFake(() => {
    });
    const mockData = cloneDeep(MOCK_RESERVATION_DATA_RESPONSE.reservationData);
    mockData.doNotMoveRoom = true;

    component['showDoNotMoveTurnedOnMessage'](mockData);
    fixture.detectChanges();

    expect(alertMessageSpy.calls.mostRecent().args[0]).toEqual('LBL_NOTE_PMS_UNAVAILABLE');
    expect(alertMessageSpy.calls.mostRecent().args[1]).toEqual('LBL_DO_NOT_MOVE_SET_FOR_RSV');
    expect(alertMessageSpy.calls.mostRecent().args[2][0].label).toEqual('LBL_ROOM_ASGN');
  });

  it('should remove alert if DNM is turned off for in house manage stay', () => {
    const alertMessageSpy = spyOn(TestBed.get(ToastMessageOutletService), 'clear').and.callFake(() => {
    });
    const mockData = cloneDeep(MOCK_RESERVATION_DATA_RESPONSE.reservationData);
    mockData.doNotMoveRoom = true;

    component['showDoNotMoveTurnedOnMessage'](mockData);
    TestBed.get(UtilityService).notifyUtilitySubjectUpdate({doNotMoveRoom: false});
    fixture.detectChanges();

    expect(alertMessageSpy).toHaveBeenCalled();
  });
});
