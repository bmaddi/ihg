import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';
import { get } from 'lodash';
import { take } from 'rxjs/operators';

import {
  Alert,
  AlertButton,
  AlertButtonType,
  BreadcrumbsService,
  SessionStorageService,
  ToastMessageOutletService
} from 'ihg-ng-common-core';

import { InHouseService } from '@modules/in-house/services/in-house.service';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ManageStayParams } from '@modules/guests/models/guest-list.models';

import { ReservationInfoModel } from '@modules/manage-stay/models/reservation-info.model';
import { LoyaltyInfoModel, ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { EnrollNewMemberService } from 'ihg-ng-common-pages';
import { TruncatedTooltipService } from '@app/modules/shared/components/truncated-tooltip/truncated-tooltip.service';

import { ManageRoomAssignmentEvent } from '../../models/manage-stay.models';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { OffersService } from '@modules/offers/services/offers.service';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ManageRoomAssignmentComponent } from '@modules/manage-stay/components/manage-room-assignment/manage-room-assignment.component';
import { GenericUpdateEvent, UtilityService } from '@services/utility/utility.service';

@Component({
  selector: 'app-manage-stay',
  templateUrl: './manage-stay.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./manage-stay.component.scss']
})
export class ManageStayComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @ViewChild(ManageRoomAssignmentComponent) roomAssignmentComponent: ManageRoomAssignmentComponent;

  manageRoomExpanded: boolean;
  tasksExpanded: boolean;
  processingPayment: boolean;
  focusedCard: boolean;
  manageStayParams: ManageStayParams;
  guestData: ReservationInfoModel;
  reservationData: ReservationDataModel;

  hasErrorBarDisplay: boolean;
  hasSuccessDisplay: boolean;
  successDataToast: any;
  dataErrorToast: any;
  copyMembershipId: any;

  confirmSaveSuccessToast: Alert;
  showSuccessCard = false;

  public reservationNumber: string;
  public pmsNumber: string;
  public editStayToggled = false;
  public stateNames = AppStateNames;
  private rootSubscription = new Subscription();

  constructor(
    private inHouseService: InHouseService,
    private checkInService: CheckInService,
    private breadcrumbs: BreadcrumbsService,
    private storageService: SessionStorageService,
    private enrollmentService: EnrollNewMemberService,
    private clipboardService: ClipboardService,
    private truncatedTooltipService: TruncatedTooltipService,
    private utilityService: UtilityService,
    private offersService: OffersService,
    private guestInfoService: GuestInfoService,
    private router: Router,
    private appCommonService: AppCommonService,
    private staticToastService: ToastMessageOutletService,
  ) {
    super();
    this.enrollmentService.successToast.subscribe(successToast => {
      this.handleSuccessToast(successToast);
    });
    this.enrollmentService.toastErrorResponse.subscribe(error => {
      this.hasErrorBarDisplay = true;
      if (!this.dataErrorToast) {
        this.dataErrorToast = error;
      }
    });
  }

  ngOnInit() {
    super.init();
    this.setBreadCrumbLabel();
    this.manageRoomExpanded = false;
    this.tasksExpanded = false;
    this.processingPayment = false;
    this.focusedCard = false;
    this.manageStayParams = this.storageService.getSessionStorage('manageStayParams');
    this.reservationNumber = this.manageStayParams.reservationNumber || '';
    this.pmsNumber = this.manageStayParams.pmsNumber || '';
    this.getCheckedInData();
  }

  addSpaceAsNeeded = (item, back: boolean): string => item ? back ? item + ' ' : ' ' + item : '';
  mapName = (data): string => this.addSpaceAsNeeded(data.prefixName, true)
    + data.firstName + ' ' + data.lastName + this.addSpaceAsNeeded(data.suffixName, false)

  mapGuestData = (data): ReservationInfoModel => ({
    guestInfo: {
      guestName: this.mapName(data),
      resNumber: data.reservationNumber,
      rcNumber: data.ihgRcNumber,
      rcPoints: data.ihgRcPoints,
      checkInDate: data.checkInDate,
      checkOutDate: data.checkOutDate,
      nights: data.numberOfNights,
      adults: data.numberOfAdults,
      children: data.numberOfChildren,
      rooms: data.numberOfRooms,
      groupName: data.groupName,
      companyName: data.companyName,
      rateCode: data.rateCategoryCode,
      estimatedTotal: data.ratesInfo.totalAmountAfterTax,
      currencyCode: data.ratesInfo.currencyCode,
      badges: data.badges
    },
    payInfo: data.payment ? data.payment.paymentInformation.card : null
  })

  changeRoom(e: ManageRoomAssignmentEvent) {
    this.manageRoomExpanded = e.expandedState;
    this.setFocus();
    this.triggerRoomAssignmentDataRefresh(e);
  }

  handleSuccessToast(success) {
    this.hasSuccessDisplay = true;
    if (success && success.show && success.membershipId) {
      this.successDataToast = { ...success };
      this.copyMembershipId = success.membershipId;
      this.getLoyaltyInformation(success.membershipId);
    }
  }

  copyToClipBoard() {
    this.clipboardService.copyFromContent(this.copyMembershipId);
  }

  getLoyaltyInformation(membershipId: string) {
    this.checkInService.getLoyaltyInfo(membershipId).subscribe((loyaltyInfo: LoyaltyInfoModel) => {
      this.updateGuestData(loyaltyInfo);
      this.updateMembershipId(membershipId);
    }, () => this.updateMembershipId(membershipId));
  }


  updateGuestData(loyaltyInfo: LoyaltyInfoModel): void {
    const info = loyaltyInfo.memberDetails;
    if (this.reservationData) {
      this.reservationData.ihgRcNumber = info.ihgRcNumber;
      this.reservationData.ihgRcPoints = info.ihgRcPoints || '0';
      this.reservationData.badges = info.badges && info.badges.length ? info.badges : ['club'];
      this.reservationData.firstName = info.firstName;
      this.reservationData.lastName = info.lastName;
      this.reservationData.prefixName = info.prefixName;
    }
    this.guestData = this.mapGuestData(this.reservationData);
    this.truncatedTooltipService.updateContent(this.utilityService.concatNameWithPrefix(
      this.reservationData.prefixName, this.reservationData.firstName, this.reservationData.lastName));
  }

  updateMembershipId(membershipId): void {
    if (this.reservationData) {
      this.reservationData.ihgRcNumber = membershipId;
    }
  }

  toggleTasks(toggleValue: boolean) {
    this.tasksExpanded = toggleValue;
    this.setFocus();
  }

  paymentProcessing(status: boolean) {
    this.processingPayment = status;
    this.setFocus();
  }

  setFocus() {
    // Use this commented code when Payment card is ready.
    // this.focusedCard = this.tasksExpanded || this.manageRoomExpanded || this.processingPayment;
    this.focusedCard = this.tasksExpanded || this.manageRoomExpanded;
  }

  setBreadCrumbLabel(): void {
    this.breadcrumbs.setLabel('guest-list-details', 'LBL_GST_LST_IN_HOUSE');
  }

  handleEditStayChange(cancelEvent: { editMode: boolean, refreshRequired: boolean }) {
    this.editStayToggled = cancelEvent.editMode;
    if (cancelEvent.refreshRequired) {
      this.getCheckedInData();
      this.triggerInHouseEditRefresh();
    }
  }

  ngOnDestroy() {
    const fromTab = get(this.manageStayParams, ['fromTab'], null);
    this.storageService.setSessionStorage('guestInHouseParams', !fromTab ? this.manageStayParams.inHouseParams : null);
    this.storageService.setSessionStorage('manageStayParams', null);
    this.appCommonService.flushReservationNumber();
    this.rootSubscription.unsubscribe();
  }

  private triggerRoomAssignmentDataRefresh(e: ManageRoomAssignmentEvent) {
    if (e.room) {
      this.getCheckedInData();
    }
  }

  private getCheckedInData() {
    this.appCommonService.setReservationNumber(this.reservationNumber);
    this.checkInService.getCheckInReservationData(this.reservationNumber).subscribe(
      response => {
        if (!this.checkIfAnyApiErrors(response.reservationData)) {
          this.reservationData = response.reservationData;
          this.guestData = this.mapGuestData(response.reservationData);
          this.showDoNotMoveTurnedOnMessage(response.reservationData);
        }
      },
      error => this.processHttpErrors(error));
  }

  private triggerInHouseEditRefresh() {
    this.roomAssignmentComponent.getInHouseGuestData(this.reservationNumber);
  }

  private showDoNotMoveTurnedOnMessage({doNotMoveRoom}: ReservationDataModel) {
    if (doNotMoveRoom) {
      this.staticToastService.info(
        'LBL_NOTE_PMS_UNAVAILABLE',
        'LBL_DO_NOT_MOVE_SET_FOR_RSV',
        [<AlertButton>{
          label: 'LBL_ROOM_ASGN',
          type: AlertButtonType.Info,
          onClick: () => this.editRoomAssignment()
        }]);
      this.subscribeToDoNotMoveScenario();
    } else {
      this.staticToastService.clear();
    }
  }

  private editRoomAssignment() {
    if (this.roomAssignmentComponent) {
      this.roomAssignmentComponent.openRoomChange();
    }
  }

  private subscribeToDoNotMoveScenario() {
    this.rootSubscription.add(
      this.utilityService.getUtilitySubjectUpdate('doNotMoveRoom')
        .pipe(take(1))
        .subscribe(
          (update: GenericUpdateEvent) => {
            if (update.doNotMoveRoom === false) {
              this.staticToastService.clear();
            }
          }
        )
    );
  }
}
