import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { ManageTasksComponent } from './manage-tasks.component';
import { ManageStayService } from '../../services/manage-stay.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { PrepareArrivalsDetailsTasksService } from '@app/modules/prepare/components/prepare-arrivals-details/services/details-tasks/prepare-arrivals-details-tasks.service';
import { ManageStayTasksDetails } from '../../models/manage-stay.models';
import { MOCK_FETCH_TASKS_RESPONSE, MOCK_GET_TASKS_RESPONSE, MOCK_GET_DEPARTMENTS } from '../../mocks/manage-tasks-mock';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

@Pipe({ name: 'limitTo' })
class LimitToPipeMock implements PipeTransform {
  transform(param) {
    return param;
  }
}

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

export class MockPrepareArrivalsDetailsTasksService {
  public getDepartmentsList(val: boolean) {
    return of(MOCK_GET_DEPARTMENTS);
  }
}

describe('ManageTasksComponent', () => {
  let component: ManageTasksComponent;
  let fixture: ComponentFixture<ManageTasksComponent>;
  let translateService: TranslateService;
  let check_api: boolean;

  class MockManageStayService {
    public manageAddTask(taskInfo, reservationNumber) {
      if (check_api) {
        return of({});
      } else {
        return throwError('error');
      }
    }

    public fetchAddTask(reservationNumber) {
      if (check_api) {
        return of(MOCK_FETCH_TASKS_RESPONSE);
      } else {
        return throwError('error');
      }
    }

    public getManageStayTasks(reservationNumber) {
      if (check_api) {
        return of(MOCK_GET_TASKS_RESPONSE);
      } else {
        return throwError('error');
      }
    }

    public addTaskMap(response: ManageStayTasksDetails) {
      response.tasks.forEach(item => {
        item.taskName = item.traceText;
        item.deptName = item.departmentCode;
      });
      return response;
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageTasksComponent, LimitToPipeMock],
      imports: [HttpClientTestingModule, IhgNgCommonKendoModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [PrepareArrivalsDetailsTasksService, TranslateService,
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: ManageStayService, useClass: MockManageStayService },
        { provide: PrepareArrivalsDetailsTasksService, useClass: MockPrepareArrivalsDetailsTasksService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTasksComponent);
    component = fixture.componentInstance;
    check_api = true;
    translateService = TestBed.get(TranslateService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test formInit method', () => {
    component.addTaskForm = undefined;
    component.disableDepart = false;
    component.noData = false;
    component.formInit();
    expect(component.addTaskForm).toBeDefined();
    expect(component.disableDepart).toBeTruthy();
    expect(component.noData).toBeTruthy();
  });

  it('should test openTasks method', () => {
    component.focusedCard = false;
    component.tasksExpanded = false;
    component.noData = true;
    component.enableForm = false;
    const emitspy = spyOn(component.toggleTasksCard, 'emit');
    component.openTasks();
    expect(emitspy).toHaveBeenCalled();
    expect(component.tasksExpanded).toBeTruthy();
    expect(component.noData).toBeFalsy();
    expect(component.enableForm).toBeTruthy();
  });

  it('should test getTasksCount method', () => {
    const val: number = component.getTasksCount();
    component.getTasksCount();
    expect(val).toEqual(4);
  });

  it('should test deleteTrace method', () => {
    component.tasksExpanded = component.hasErrors = component.addTaskFail = true;
    const emitspy = spyOn(component.toggleTasksCard, 'emit');
    component.deleteTrace();
    expect(emitspy).toHaveBeenCalled();
    expect(component.tasksExpanded).toBeFalsy();
    expect(component.hasErrors).toBeFalsy();
    expect(component.addTaskFail).toBeFalsy();
  });

  it('should test handleTraceChange method', () => {
    component.addTaskForm.traceText = 'test';
    component.handleTraceChange();
    expect(component.disableDepart).toBeFalsy();
  });

  it('should test disableTrace method', () => {
    component.disableDepart = false;
    component.addTaskForm.departmentCode = 'TEST';
    component.keyMap.selectDept = 'XXXX';
    const val = component.disableTrace();
    expect(val).toBeFalsy();
  });

  it('should test sendTrace method', () => {
    spyOn(component, 'addTask');
    component.sendTrace();
    expect(component.addTask).toHaveBeenCalled();
  });

  it('should test refresh method', () => {
    spyOn(component, 'getInHouseGuestTasks');
    component.refresh();
    expect(component.getInHouseGuestTasks).toHaveBeenCalled();
  });

  it('should test onRetry method', () => {
    spyOn(component, 'addTask');
    component.onRetry();
    expect(component.addTask).toHaveBeenCalled();
  });

  it('should test getInHouseGuestTasks method success case', () => {
    component.taskDetails = undefined;
    component.getInHouseGuestTasks('1234');
    expect(component.taskDetails).toBeDefined();
  });

  it('should test getInHouseGuestTasks method backend error case', () => {
    check_api = false;
    spyOn(component, 'processHttpErrors');
    component.getInHouseGuestTasks('1234');
    expect(component.processHttpErrors).toHaveBeenCalled();
  });

  it('should test addTask method success case', () => {
    spyOn(component, 'addTaskInformation');
    component.addTask('1234');
    expect(component.addTaskInformation).toHaveBeenCalled();
  });

  it('should test addTask method backend error case', () => {
    check_api = false;
    spyOn(component, 'addTaskInformation');
    spyOn(component, 'processHttpErrors');
    component.addTaskFail = false;
    component.addTask('1234');
    expect(component.addTaskInformation).toHaveBeenCalled();
    expect(component.processHttpErrors).toHaveBeenCalled();
    expect(component.addTaskFail).toBeTruthy();
  });

  it('should test fetchTasks backend error case', () => {
    check_api = false;
    spyOn(component, 'processHttpErrors');
    component.addTaskFail = false;
    component.taskDetails = undefined;
    component.fetchTasks('1234');
    expect(component.processHttpErrors).toHaveBeenCalled();
    expect(component.addTaskFail).toBeTruthy();
  });

  it('should test fetchTasks method success case', () => {
    spyOn(component, 'addTaskInformation');
    component.addTaskFail = true;
    component.taskDetails = undefined;
    component.fetchTasks('1234');
    expect(component.taskDetails).toBeDefined();
    expect(component.addTaskFail).toBeFalsy();
  });

  it('should test addTaskInformation method', () => {
    component.departmentsList = [{ name: 'TEST', code: 'TESTING' }, { name: 'TESTCASE', code: 'XXXX' }];
    component.addTaskForm.departmentCode = 'TESTING';
    component.todaysDate = undefined;
    component.addTaskInfo = undefined;
    component.addTaskInformation();
    expect(component.todaysDate).toBeDefined();
    expect(component.addTaskInfo).toBeDefined();
    expect(component.addTaskInfo.departmentName).toEqual('TEST');
  });

  xit('Should disable the Add tasks button', () => {
    const hasErrors = false;
    const tasksExpanded = false;
    if (!hasErrors && !tasksExpanded) {
      const addTask = fixture.nativeElement.querySelector('[data-slnm-ihg="AddTask-SID"]');
      expect(addTask).toBeDefined();
      const disableTasks = true;
      fixture.detectChanges();
      fixture.whenRenderingDone();
      {
          expect(fixture.debugElement.query(By.css('disable-tasks'))).toBeTruthy();
      }
    }
  });
});
