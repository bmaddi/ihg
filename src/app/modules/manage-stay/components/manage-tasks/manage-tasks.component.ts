import {Component, EventEmitter, OnInit, Output, Input, OnDestroy, ViewEncapsulation} from '@angular/core';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { AppConstants } from '@app/constants/app-constants';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { AppErrorBaseComponent } from '@app/modules/shared/components/app-service-errors/app-error-base-component';
import { PrepareArrivalsDetailsTasksService } from '@modules/prepare/components/prepare-arrivals-details/services/details-tasks/prepare-arrivals-details-tasks.service';
import { DepartmentsListModel, DepartmentsModel } from '@modules/prepare/components/prepare-arrivals-details/models/arrivals-tasks.model';
import { AddTaskForm } from '@modules/manage-stay/models/stay-information.models';
import { ManageStayService } from '../../services/manage-stay.service';
import { ManageStayTasks, ManageStayTasksDetails, AddTaskData } from '@modules/manage-stay/models/manage-stay.models';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { MANAGE_STAY_CONST } from '@modules/manage-stay/manage-stay.constants';
import { MANAGE_TASKS_ADD_AUTOFILL, MANAGE_TASKS_SYSTEM_AUTOFILL} from '@app/constants/error-autofill-constants';

@Component({
  selector: 'app-manage-tasks',
  templateUrl: './manage-tasks.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./manage-tasks.component.scss']
})
export class ManageTasksComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  @Input() focusedCard: boolean;
  @Input() reservationNumber: string;
  @Input() pmsReservationNumber: string;
  @Input() disableTasks: boolean;
  @Output() toggleTasksCard = new EventEmitter<boolean>();

  private detailsTasks$: Subscription = new Subscription();
  public departmentsList: DepartmentsModel[];
  toastTranslationConfig: Partial<ErrorToastMessageTranslations>;
  errorCardType = ERROR_CARD_TYPE.CARD_ERROR;
  tasksExpanded: boolean;
  taskDetails: ManageStayTasks[];
  enableForm: boolean;
  disableDepart: boolean;
  addTaskForm: AddTaskForm;
  noData: boolean;
  addTaskFail = false;
  addTaskInfo: AddTaskData;
  todaysDate: string;
  departmentName: DepartmentsModel[];
  keyMap = MANAGE_STAY_CONST.KEYMAP;
  addIssueAutoFill = MANAGE_TASKS_ADD_AUTOFILL;
  dataIssueAutoFill = MANAGE_TASKS_SYSTEM_AUTOFILL;
  ga = MANAGE_STAY_CONST.GA;

  constructor( private tasksService: PrepareArrivalsDetailsTasksService,
    private manageStayService: ManageStayService,
    private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.taskDetails = [];
    this.getInHouseGuestTasks(this.reservationNumber);
    this.getDepartmentsList();
    this.formInit();
    this.initToastErrorConfig();
  }

  formInit() {
    this.addTaskForm = {
      traceText: '',
      departmentCode: this.keyMap.selectDept
    };
    this.disableDepart = true;
    this.noData = true;
  }

  openTasks() {
    if (!this.focusedCard) {
      this.tasksExpanded = true;
      this.toggleTasksCard.emit(this.tasksExpanded);
    }
    this.noData = false;
    this.enableForm = true;
  }

  getTasksCount() {
    return this.taskDetails.length;
  }

  deleteTrace() {
    this.tasksExpanded = this.hasErrors = this.addTaskFail =  false;
    this.toggleTasksCard.emit(this.tasksExpanded);
    this.formInit();
  }

  private getDepartmentsList(): void {
    if (!this.departmentsList) {
      this.detailsTasks$.add(this.tasksService.getDepartmentsList(true).subscribe((response: DepartmentsListModel) => {
        this.departmentsList = response.departments;
      }));
    }
  }

  public handleTraceChange() {
    this.disableDepart = !this.addTaskForm.traceText;
  }

  public disableTrace() {
    return this.disableDepart || this.addTaskForm.departmentCode === this.keyMap.selectDept || this.addTaskForm.departmentCode === '';
  }

  public sendTrace() {
    this.addTask(this.pmsReservationNumber);
  }

  public refresh() {
    this.getInHouseGuestTasks(this.reservationNumber);
  }

  public onRetry() {
    this.addTask(this.pmsReservationNumber);
  }

  public getInHouseGuestTasks(reservationNumber: string) {
    this.manageStayService.getManageStayTasks(reservationNumber).subscribe((response: ManageStayTasksDetails) => {
      this.taskDetails = response.taskDetails;
    }, (error) => {
      this.processHttpErrors(error);
    });
  }

  public addTask(reservationNumber: string) {
    this.addTaskInformation();
    this.manageStayService.manageAddTask(this.addTaskInfo, reservationNumber).subscribe((response: ManageStayTasksDetails) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.fetchTasks(reservationNumber);
      } else {
        this.addTaskFail = true;
      }
    }, (error) => {
      this.addTaskFail = true;
      this.processHttpErrors(error);
    });
  }

  public fetchTasks(reservationNumber: string) {
    this.manageStayService.fetchAddTask(reservationNumber).subscribe((response: ManageStayTasksDetails) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.manageStayService.addTaskMap(response);
        this.addTaskFail = false;
        this.taskDetails = response.tasks;
        this.hasErrors = false;
        this.formInit();
        this.gaService.trackEvent(this.ga.CATEGORY, this.ga.MANAGE_STAY, this.ga.ADD_NEW_TASK);
      } else {
        this.addTaskFail = true;
      }
    }, (error) => {
      this.addTaskFail = true;
      this.processHttpErrors(error);
    });
  }

  public addTaskInformation() {
    this.departmentName = this.departmentsList.filter(item => item.code === this.addTaskForm.departmentCode);
    this.todaysDate = moment(new Date()).format(AppConstants.DB_DT_FORMAT);
    this.addTaskInfo = {
      departmentName : this.departmentName[0].name,
      departmentCode : this.addTaskForm.departmentCode,
      traceText : this.addTaskForm.traceText,
      resolvedBy : '',
      isResolved : false,
      traceId : 0,
      resolvedTimestamp : this.todaysDate,
      traceTimestamp : this.todaysDate
    };
  }

  private initToastErrorConfig() {
    this.toastTranslationConfig = MANAGE_STAY_CONST.MANAGE_STAY_TASK_ERROR;
  }

  ngOnDestroy(): void {
    this.detailsTasks$.unsubscribe();
  }
}
