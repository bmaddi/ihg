export const MANAGE_STAY_CONST = {
  new: 'new',
  duplicate: 'duplicate'
};

export const SAVE_GUESTINFO_ERR = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_SAVE_FAIL_DESC'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_SAVE_FAIL_DESC'
  }
};

export const CHECKIN_GA_CONSTANTS = {
  category: 'Check In - Manage Stay',
  action: 'Back to Check In',
  label: 'Selected'
};

export const CUT_KEY_ERROR = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_TOAST_CUT_KEY'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_TOAST_CUT_KEY'
  }
};

