export enum ManageStayEnumConstants {
  CATEGORY_IN_HOUSE = 'Guest List - In House',
  ACTION_MANAGE_STAY = 'Manage Stay',
  GA_CUT_KEY_DUPLICATE = 'Cut Keys - Duplicate',
  GA_CUT_KEY_NEW = 'Cut Keys - New',
  GA_CUT_KEY_ERROR = 'Cut Keys - Error'
}
