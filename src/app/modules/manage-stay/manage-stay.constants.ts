export const MANAGE_STAY_CONST = {
  KEYMAP: {
    selectDept: 'Select Department'
  },
  MANAGE_STAY_TASK_ERROR: {
    retryButton: 'LBL_ERR_TRYAGAIN',
    reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
    timeoutError: {
      message: 'TOAST_TIMEOUT',
      detailMessage: 'LBL_ERR_SND_TSK',
      detailMessageMoreErrors: 'LBL_ERR_SND_TSK'
    },
    generalError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_SND_TSK',
      detailMessageMoreErrors: 'LBL_ERR_SND_TSK'
    }
  },
  ADDTASK_REPORT_AN_ISSUE: {
    function: 'Guests',
    topic: 'Manage Stay - Tasks',
    subtopic: 'Error Message'
  },
  PAYMENT_REPORT_AN_ISSUE: {
    function: 'Guests',
    topic: 'Manage Stay - Room Assignment',
    subtopic: 'Error Message'
  },
  GA: {
    CATEGORY: 'Guest List - In House',
    MANAGE_STAY: 'Manage Stay',
    ADD_NEW_TASK: 'Add New Task',
    CHCKIN_MNG_STAY: 'Check In - Manage Stay',
    STAY_INFO: 'Stay Information',
    GUEST_INFO: 'Guest Information',
    SLCTD: 'Selected',
    SAVE_ACTION: 'Save',
    CORPORATE_ID_LOOKUP: 'Corporate ID Lookup',
    REWARD_MEMBER_LOOKUP: 'Rewards Member Lookup'
  }
};
