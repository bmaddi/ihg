import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ProgressStatusModule } from '@modules/progress-status/progress-status.module';
import { AppSharedModule } from '@app-shared/app-shared.module';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { IhgNgCommonPagesModule } from 'ihg-ng-common-pages';

import { ManageGuestComponent } from '@modules/manage-stay/components/manage-guest/manage-guest.component';
import { ManageRoomAssignmentComponent } from '@modules/manage-stay/components/manage-room-assignment/manage-room-assignment.component';
import { ManagePaymentComponent } from '@modules/manage-stay/components/manage-payment/manage-payment.component';
import { ManageTasksComponent } from '@modules/manage-stay/components/manage-tasks/manage-tasks.component';
import { ManageStayComponent } from '@modules/manage-stay/components/manage-stay/manage-stay.component';
import { ManageCutKeyComponent } from './components/manage-cut-key/manage-cut-key.component';
import { ManagaeStayRoutingModule } from './manage-stay.route';
import { EditStayComponent } from './components/edit-stay/edit-stay.component';
import { ManageStayTabsComponent } from './components/manage-stay-tabs/manage-stay-tabs.component';
import { StayInfoModule } from '@modules/stay-info/stay-info.module';
import { GuestInfoModule } from '@modules/guest-info/guest-info.module';
import { OffersModule } from '@modules/offers/offers.module';
import { BadgeListModule } from '@modules/badge-list/badge-list.module';
import { NoPostModule } from '@modules/no-post/no-post.module';
import { GuestInfoService } from '../guest-info/services/guest-information.service';
import { EncoderModule } from '@modules/encoder/encoder.module';
import { ManageChangeRoomComponent } from './components/manage-change-room/manage-change-room.component';
import { PrepareArrivalsDetailsModule } from '@modules/prepare/components/prepare-arrivals-details/prepare-arrivals-details.module';
import { InHouseEditStayComponent } from './components/in-house-edit-stay/in-house-edit-stay.component';
import { CutKeyModalComponent } from './components/cut-key-modal/cut-key-modal.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    TranslateModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    IhgNgCommonPagesModule,
    FormsModule,
    ReactiveFormsModule,
    DropDownsModule,
    ProgressStatusModule,
    AppSharedModule,
    ManagaeStayRoutingModule,
    StayInfoModule,
    GuestInfoModule,
    OffersModule,
    BadgeListModule,
    NoPostModule,
    EncoderModule,
    RouterModule,
    PrepareArrivalsDetailsModule
  ],
  declarations: [
    ManagePaymentComponent,
    ManageRoomAssignmentComponent,
    ManageTasksComponent,
    ManageGuestComponent,
    ManageStayComponent,
    ManageCutKeyComponent,
    EditStayComponent,
    ManageStayTabsComponent,
    ManageChangeRoomComponent,
    InHouseEditStayComponent,
    CutKeyModalComponent
  ],
  exports: [
    ManagePaymentComponent,
    ManageRoomAssignmentComponent,
    ManageTasksComponent,
    ManageGuestComponent,
    ManageStayComponent,
  ],
  providers : [
    GuestInfoService
  ]
})
export class ManageStayModule { }
