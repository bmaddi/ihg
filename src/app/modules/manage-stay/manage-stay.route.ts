import { RouterModule, Routes } from '@angular/router';
import { TransitionGuardService } from 'ihg-ng-common-core';
import { NgModule } from '@angular/core';
import { ManageStayComponent } from './components/manage-stay/manage-stay.component';
import { EditStayComponent } from './components/edit-stay/edit-stay.component';

const routes: Routes = [
  {
    path: 'manage-stay',
    component: ManageStayComponent,
    canDeactivate: [
      TransitionGuardService
    ],
    data: {
      crumb: ['dashboard', 'guest', 'guest-list-details'],
      pageTitle: 'LBL_MANAGE_STAY',
      stateName: 'manage-stay',
      slnmId: 'ManageStay-SID',
      maxWidth: true,
      noSubtitle: true,
      showHelp: true
    }
  },
  {
    path: 'edit-stay/:reservationNum',
    component: EditStayComponent,
    canDeactivate: [
      TransitionGuardService
    ],
    data: {
      crumb: ['dashboard', 'guest', 'guest-list-details', 'check-in'],
      pageTitle: 'LBL_MANAGE_STAY',
      stateName: 'manage-stay',
      slnmId: 'ManageStay-SID',
      maxWidth: true,
      noSubtitle: true,
      showHelp: true
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class ManagaeStayRoutingModule {}
