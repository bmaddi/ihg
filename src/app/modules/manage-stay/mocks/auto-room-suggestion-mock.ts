export const AutoSuggestionMockResponse = {
  'roomStatusList': [
    {
      'roomStatus': 'Clean',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Clean',
      'roomNumber': '0100',
      'roomType': 'KNGN',
      'features': [
        {
          'code': 'BL',
          'description': 'Balcony'
        },
        {
          'code': 'BR',
          'description': 'Business Room'
        },
        {
          'code': 'CR',
          'description': 'Corner Room'
        },
        {
          'code': 'HF',
          'description': 'High Floor Near Elevator'
        },
        {
          'code': 'CV',
          'description': 'City View'
        },
        {
          'code': 'GV',
          'description': 'Garden View'
        },
        {
          'code': 'OV',
          'description': 'Ocean View'
        }
      ],
      'reservationFeatures': [
        {
          'code': 'BL',
          'type': 'R',
          'description': 'Balcony',
          'matched': false
        },
        {
          'code': 'CV',
          'type': 'R',
          'description': 'City View',
          'matched': true
        },
        {
          'code': 'GV',
          'type': 'R',
          'description': 'Garden View',
          'matched': true
        },
        {
          'code': 'HF',
          'type': 'R',
          'description': 'High Floor Near Elevator',
          'matched': true
        },
        {
          'code': 'OV',
          'type': 'R',
          'description': 'Ocean View',
          'matched': false
        }
      ]
    },
    {
      'roomStatus': 'PickUp',
      'frontOfficeStatus': 'Occupied',
      'houseKeepingStatus': 'PickUp',
      'roomNumber': '0101',
      'roomType': 'KNGN',
      'features': [
        {
          'code': 'BL',
          'description': 'Balcony'
        },
        {
          'code': 'OV',
          'description': 'Ocean View'
        },
        {
          'code': 'NE',
          'description': 'Near Elevator'
        }
      ],
      'reservationFeatures': []
    },
    {
      'roomStatus': 'Clean',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Clean',
      'roomNumber': '0102',
      'roomType': 'KNGN',
      'features': [
        {
          'code': 'KB',
          'description': 'King Bed'
        }
      ],
      'reservationFeatures': []
    }
  ]
};
