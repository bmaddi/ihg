
export const CHECK_IN_MOCK = {
    'payIntegratedFlag': 'N',
    'reservationData': {
        'pmsGRSCompareFlag': 'Y',
        'cdsResUpdtdTs': {
            'seconds': '60',
            'time': 'Nov 17, 2019 20:27:01.00000756',
            'timeFormat': 'dd-MMM-yyyy HH:mm:ss',
            'timezone': 'America/New_York'
        },
        'pmsAuthSaveInd': 'N',
        'prefixName': 'Mr',
        'firstName': 'Saurav',
        'lastName': 'Agrawal',
        'suffixName': '',
        'reservationNumber': '44002733',
        'pmsReservationNumber': '447469',
        'pmsReservationStatus': 'DUEIN',
        'pmsUniqueNumber': '466611',
        'arrivalTime': '',
        'checkInDate': '2019-06-05',
        'checkOutDate': '2019-06-07',
        'numberOfNights': 2,
        'ihgRcNumber': '123456',
        'ihgRcMembership': 'Spire Elite',
        'ihgRcPoints': 5000,
        'vipCode': '',
        'isKeyCut': false,
        'companyName': 'ARKANSAS HOSPICE INC',
        'groupName': 'IHG Concerto',
        'rateCategoryCode': 'IGBBB',
        'roomTypeCode': 'KEXG',
        'roomTypeDescription': '1 KING BED EXECUTIVE',
        'numberOfRooms': 1,
        'badges': [
            'spire',
            'ambassador',
            'employee'
        ],
        'assignedRoomNumber': '',
        'assignedRoomStatus': null,
        'numberOfAdults': 1,
        'numberOfChildren': 0,
        'preferences': [
            'HIGH FLOOR',
            'BALCONY',
            'ROOM NEAR ELEVATOR',
            'OCEAN VIEW',
            'NEAR FITNESS ROOM'
        ],
        'specialRequests': [
            'Need Extra Towels and Water Bottles'
        ],
        'infoItems': [],
        'tasks': [{
            'traceId': 405806,
            'traceText': 'Need Extra Towels and Water Bottles',
            'departmentCode': 'ZIF',
            'departmentName': 'CRS Information/Comments',
            'traceTimestamp': '2019-06-06T00:00:00.0000000-04:00',
            'isResolved': true,
            'resolvedTimestamp': '2019-06-04',
            'resolvedBy': 'SUPERVISOR'
        },
        {
            'traceId': 405809,
            'traceText': 'Need Shampoo',
            'departmentCode': 'HSK',
            'departmentName': 'House Keeping',
            'traceTimestamp': '2019-06-04T10:43:00.0000000-04:00',
            'isResolved': true,
            'resolvedTimestamp': '2019-06-04',
            'resolvedBy': 'SUPERVISOR'
        },
        {
            'traceId': 405811,
            'traceText': 'Need Chocolates',
            'departmentCode': 'MGT',
            'departmentName': 'Management',
            'traceTimestamp': '2019-06-04T13:24:00.0000000-04:00',
            'isResolved': false
        }
        ],
        'prepStatus': 'INPROGRESS',
        'heartBeatActions': {
            'myHotelActions': [
                'GuestProblem',
                'GuestComment',
                'SleepIssue'
            ],
            'otherHotelActions': [
                'GuestLove'
            ]
        },
        'heartBeatComment': 'Good Hospitality. Looking forward to revisit again',
        'heartBeatImprovement': 'Add more Indoor Events',
        'otaFlag': false,
        'resVendorCode': 'BKGTL',
        'ratesInfo': {
            'currencyCode': 'EUR',
            'totalAmountBeforeTax': '755.14',
            'totalAmountAfterTax': '808.00',
            'totalTaxes': '52.86'
        },
        'payment': {
            'paymentInformation': {
                'card': {
                    'code': 'VS',
                    'expireDate': '0420',
                    'number': '7472471499080010'
                }
            }
        },
        'inspected': false,
        'paymentType': 'CARD',
        'noPost': false,
        'doNotMoveRoom': true
    }
};