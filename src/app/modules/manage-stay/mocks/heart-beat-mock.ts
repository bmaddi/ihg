export const HeatBeatServiceMockResponse = {
  'memberId': 135641665,
  'heartBeatMyHotel': {
    'surveyDate': '2019-03-24',
    'sleepIssue': 'Sleep issues exists',
    'guestLove': 'R',
    'likeComment': 'Very friendly employees which feel my like at home',
    // tslint:disable-next-line:max-line-length
    'makeStayBetterComment': 'Maybe check the Wifi connection part. It would be very helpful if the login information would be kept permanent for such recurring users as we from Canon are (Dave Hamer and Roger Erne). '
  },
  'heartBeatOtherHotel': {
    'hotelCode': 'CODE1',
    'surveyDate': '2019-02-12',
    'sleepIssue': null,
    'guestLove': 'G'
  }
};
