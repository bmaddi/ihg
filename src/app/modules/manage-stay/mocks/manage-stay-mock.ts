import { ReservationInfoModel, GuestInfoModel } from '@modules/manage-stay/models/reservation-info.model';
import { LoyaltyInfoModel } from '@modules/check-in/models/check-in.models';
import { GuestInformationModel, GuestInfo } from '@modules/guest-info/models/guest-info.model';
import { RateCategoryModel } from '@modules/offers/models/offers.models';
import { GridDataResult } from '@progress/kendo-angular-grid';

export const MOCK_RESERVATION_DATA_RESPONSE = {
  reservationData: {
    amenityFlag: true,
    prefixName: 'Mr.',
    firstName: 'Ben',
    lastName: 'Dingle',
    suffixName: '',
    reservationNumber: '22904217',
    pmsReservationNumber: '19400',
    pmsReservationStatus: 'INHOUSE',
    pmsUniqueNumber: '19150',
    inspected: true,
    arrivalTime: '',
    checkInDate: '2019-10-21',
    checkOutDate: '2019-10-22',
    numberOfNights: 1,
    ihgRcNumber: '147096233',
    ihgRcMembership: 'pltnm',
    ihgRcPoints: '1500',
    vipCode: 'Y',
    companyName: '',
    groupName: '',
    rateCategoryCode: 'IGCOR',
    roomTypeCode: 'KNGN',
    roomTypeDescription: '1 KING BED NONSMOKING',
    numberOfRooms: 1,
    badges: [
      'platinum'
    ],
    assignedRoomNumber: '206',
    assignedRoomType: 'KNGN',
    assignedRoomStatus: {
      roomStatus: 'Inspected',
      frontOfficeStatus: 'Occupied',
      houseKeepingStatus: 'Inspected',
      roomNumber: '206',
      roomType: 'KNGN',
      features: [],
      reservationFeatures: []
    },
    numberOfAdults: 1,
    numberOfChildren: 0,
    preferences: [],
    specialRequests: [],
    infoItems: [],
    tasks: [
      {
        traceId: 8501,
        traceText: 'Invalid Channel Code: MEMHQ, using the Default: OT *WT:OXI',
        departmentCode: 'ZWN',
        departmentName: 'CRS Warning Trace',
        traceTimestamp: '2019-10-21 09:32:08.032615',
        isResolved: false,
        resolvedTimestamp: '',
        resolvedBy: ''
      }
    ],
    prepStatus: 'CHECKEDIN',
    heartBeatActions: null,
    heartBeatComment: '',
    heartBeatImprovement: '',
    heartBeatActionsInd: '',
    amenityPoints: null,
    otaFlag: false,
    resVendorCode: '',
    ratesInfo: {
      currencyCode: 'USD',
      totalAmountBeforeTax: '',
      totalAmountAfterTax: '122.34',
      totalTaxes: '',
      dailyRates: [],
      rateRules: []
    },
    payment: {
      paymentInformation: {
        card: {
          code: 'MC',
          expireDate: '1219',
          number: '7541766605734111'
        }
      },
      guaranteeType: ''
    },
    confirmationDate: '2019-10-21',
    corporateId: '',
    iataNumber: null,
    selectedAmenity: 'Points',
    paymentType: 'CARD',
    pmsAuthSaveInd: 'N',
    pmsGRSCompareFlag: 'Y',
    doNotMoveRoom: false,
    noPost: false,
    keysCut: false
  },
  payIntegratedFlag: 'Y'
};

export const MOCK_MAPPED_NAME = 'Mr. Ben Dingle';

export const MAPPED_GUEST_DATA: ReservationInfoModel = {
  guestInfo: {
    guestName: MOCK_MAPPED_NAME,
    resNumber: '22904217',
    rcNumber: '147096233',
    rcPoints: '1500',
    checkInDate: '2019-10-21',
    checkOutDate: '2019-10-22',
    nights: 1,
    adults: 1,
    children: 0,
    rooms: 1,
    groupName: '',
    companyName: '',
    rateCode: 'IGCOR',
    estimatedTotal: '122.34',
    currencyCode: 'USD',
    badges: ['platinum']
  },
  payInfo: {
    code: 'MC',
    expireDate: '1219',
    number: '7541766605734111'
  }
};

export const ENROLLMENT_SUCCESS = {
  show: true,
  type: 'success',
  dismissible: false,
  showAlertAction: true,
  alertActionName: ' Copy to Clipboard',
  caption: 'Well done!',
  message: 'Test Test has been enrolled in IHG<sup>®</sup> Rewards Club. Member profile number is <b>146830661</b>.',
  membershipId: '146830661'
};

export const LOYALTY_INFO: LoyaltyInfoModel = {
  memberDetails: {
    ihgRcNumber: '12345',
    ihgRcMembership: 'gold',
    ihgRcPoints: '22100',
    badges: ['gold', 'ambassador', 'employee'],
    firstName: 'reddy',
    lastName: 'Rising Phoenix',
    prefixName: ''
  }
};

export const MockSaveGuestInfo: GuestInfo = {
  firstName: 'ABC',
  lastName: 'DEF',
  middleName: '',
  addressLine1: 'Street drive1',
  addressLine2: 'Street 2',
  countryName: 'USA',
  countryCode: '55',
  zipCode: 54000,
  stateProvince: '',
  cityName: 'Atlanta',
  phoneNumber: '5555555',
  phoneType: '',
  email: 'abc.def@ihg.com',
  cardNumber: '4111111111111111',
  expirationDate: '03/2021',
  nameOnCard: 'ABC',
  confirmationNumber: '4567778',
  hotelCode: 'ATLCP',
  brandCode: 'HICV'
};

export const RATE_CODE_DETAILS: RateCategoryModel = {
  'name': 'BEST FLEXIBLE RATE',
  'code': 'IGCOR',
  'averageNightlyRate': '125.00',
  'rateInfo': {
    'averageRate': '125.00',
    'currency': 'GBP',
    'totalTaxAmount': '41.67',
    'totalAmountAfterTax': '250.00',
    'totalExtraPersonAmount': '0.00',
    'totalServiceChargeAmount': '0.00',
    'dailyRates': [
      {
        'date': '2019-10-29',
        'amountBeforeTax': '104.17',
        'amountAfterTax': '125.00',
        'baseAmount': '125.00'
      },
      {
        'date': '2019-10-30',
        'amountBeforeTax': '104.17',
        'amountAfterTax': '125.00',
        'baseAmount': '125.00'
      }
    ],
    'rateRules': [
      {
        'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY. ihg rc test content.',
        'noShowPolicyDesc':'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
        'extraPersonCharge': '2220.00',
        'earlyDeparture': '50.00',
        'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
        'checkinTime': '14:00:00',
        'checkoutTime': '11:00:00',
        'tax': '41.67',
        'serviceCharge': '45.00',
        'refundable': false
      }
    ],
    'serviceChargeIncluded': false,
    'taxesChargeIncluded': true,
    'extraPersonChargeIncluded': true
  },
  'breakfastIncluded': true,
  'pointsEligible': true
};

export const RATE_CODE_MODAL_PARAMS = {
  rateName: '',
  rateCode: '',
  roomName: '',
  roomCode: '',
  roomDesc: '',
  pointsEligible: '',
  rateInfo: '',
  arrivalDate: '',
  departureDate: '',
  isOnlyRateInfo: true
};

export const ALL_GUEST_DATA: GridDataResult = {
  data: [
    {
      'firstName': 'Sruthisdfgsdfgdgf',
      'lastName': 'Kuruvilladfgsdfgsdfg',
      'badges': [
        'gold',
        'karma',
        'spire'
      ],
      'loyaltyId': '150765771',
      'deparatureDate': '2019-12-04',
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '',
      'roomType': '',
      'checkInDate': '2019-12-03',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': 'RESERVED',
      'paymentType': 'CARD',
      'roomCount': 1,
      'reservationNumber': 44002733
    },
    {
      'firstName': 'Sruthisdfgsdfgdgf',
      'lastName': 'Kuruvilladfgsdfgsdfg',
      'badges': [
        'gold',
        'karma',
        'spire',
        'ambassador'
      ],
      'loyaltyId': '150765771',
      'deparatureDate': '2019-12-04',
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '',
      'roomType': 'KNGN',
      'checkInDate': '2019-12-03',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': 'NOSHOW',
      'paymentType': 'OTHER',
      'roomCount': 1,
      'reservationNumber': 44002733
    },
    {
      'firstName': 'Sruthisdfgsdfgdgf',
      'lastName': 'Kuruvilladfgsdfgsdfg',
      'badges': [
        'gold',
        'karma',
        'spire'
      ],
      'loyaltyId': '150765771',
      'deparatureDate': '2019-12-03',
      'groupName': 'Test Group',
      'companyName': 'Some really cool company',
      'roomNumber': '1210',
      'roomType': 'KNGN',
      'checkInDate': '2019-12-04',
      'rateCategoryCode': 'IGCOR',
      'reservationStatus': 'DUEIN',
      'paymentType': 'OTHER',
      'roomCount': 1,
      'reservationNumber': 44002733
    }
  ],
  total: 3
};
