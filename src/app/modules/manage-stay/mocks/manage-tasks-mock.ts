export const MOCK_FETCH_TASKS_RESPONSE = {
    'tasks': [
        {
            'traceId': 443500,
            'traceText': 'Trying to chg dept',
            'departmentCode': 'XYZ',
            'departmentName': '',
            'traceTimestamp': '2019-02-28T10:16:00.0000000-05:00',
            'isResolved': false
        },
        {
            'traceId': 443502,
            'traceText': 'Trace Creation with invalid dept',
            'departmentCode': 'RPnx',
            'departmentName': '',
            'traceTimestamp': '2019-02-28T15:04:00.0000000-05:00',
            'isResolved': false
        },
        {
            'traceId': 443513,
            'traceText': 'comment on 28th ( free text 500 chars) After mod in',
            'departmentCode': 'ZIF',
            'departmentName': '',
            'traceTimestamp': '2019-03-05T15:11:41.0000000-05:00',
            'isResolved': false
        },
        {
            'traceId': 443514,
            'traceText': 'PMS More comments   Again more comments',
            'departmentCode': 'ZIF',
            'departmentName': '',
            'traceTimestamp': '2019-03-05T15:11:41.0000000-05:00',
            'isResolved': false
        }
    ]
};

export const MOCK_GET_TASKS_RESPONSE = {
    'taskDetails': [{
        'taskName': 'Non smoking',
        'deptName': 'ZIF',
        'taskStatus': 'N'
    }, {
        'taskName': 'GDS RECORD LOCATOR -BK -H2-91683495',
        'deptName': 'ZIF',
        'taskStatus': 'N'
    }, {
        'taskName': 'GDS HOTEL -GRVGG',
        'deptName': 'ZIF',
        'taskStatus': 'N'
    }, {
        'taskName': 'Invalid Channel Code: IDCBK, using the Default: OT *WT:OXI',
        'deptName': 'ZWN',
        'taskStatus': 'N'
    }]
};

export const MOCK_GET_DEPARTMENTS = {
    'departments': [{
        'code': 'ACC',
        'name': 'Accounting'
    }, {
        'code': 'CON',
        'name': 'Concierge / Bell Stand'
    }, {
        'code': 'GS',
        'name': 'Guest Services'
    }, {
        'code': 'HSK',
        'name': 'House Keeping'
    }, {
        'code': 'MGT',
        'name': 'Management'
    }, {
        'code': 'MOB',
        'name': 'Mobile Check-in'
    }, {
        'code': 'NA',
        'name': 'Night Audit'
    }, {
        'code': '*NOR1',
        'name': 'NOR1 Interface Traces'
    }, {
        'code': 'PMS',
        'name': 'Default Department'
    }, {
        'code': 'RES',
        'name': 'Reservations'
    }, {
        'code': 'SAL',
        'name': 'Sales'
    }, {
        'code': 'ZIF',
        'name': 'CRS Information/Comments'
    }, {
        'code': 'ZSV',
        'name': 'CRS Service Requests'
    }, {
        'code': 'ZWN',
        'name': 'CRS Warning Trace'
    }]
};

export const ROOM_ASSIGNMENT_DETAILS_RESPONSE = {
    'roomNumber': 140,
    'roomType': 1234,
    'roomStatus': 'PU',
    'inspectedFlag': 'Y',
    'isCutKeyError': true,
    'roomDetails': [
        {
        'description': 'High Floor'
        },
        {
        'description': 'Away From Elevator'
        },
        {
        'description': 'Ocean view'
        },
        {
        'description': 'No Smoking'
        }
    ]
};

export const MOCK_TASKS_RESPONSE = {
    'taskDetails': [],
    'tasks': [
        {
            'deptName': 'ZIF',
            'taskName': 'Non smoking',
            'taskStatus': 'N',
            'traceText': 'comment on 28th ( free text 500 chars) After mod in',
            'departmentCode': 'ZIF',
            'isResolved': false
        }
    ]
};

export const MOCK_ADD_TASK_DATA = {
    'departmentCode': 'ZIF',
    'departmentName': 'ZIF',
    'isResolved': true,
    'resolvedBy': 'John',
    'resolvedTimestamp': '2019-03-05T15:11:41.0000000-05:00',
    'traceId': 1234,
    'traceText': 'Issue',
    'traceTimestamp': '2019-03-05T15:11:30.0000000-05:00'
};

