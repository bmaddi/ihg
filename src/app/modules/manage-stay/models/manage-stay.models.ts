import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';

export interface ManageStayModel {
  roomNumber?: string;
  roomType?: string;
  roomStatus?: string;
  inspectedFlag?: string;
  roomDetails?: [RoomDetails];
  isCutKeyError?: boolean;
}

export interface RoomDetails {
  description: string;
}

export interface ManageStayTasks {
  deptName: string;
  taskName: string;
  taskStatus: string;
  traceText: string;
  departmentCode: string;
  isResolved: boolean;
}

export interface ManageStayTasksDetails {
  taskDetails?: ManageStayTasks[];
  tasks?: ManageStayTasks[];
}

export interface AddTaskData {
  departmentCode: string;
  departmentName: string;
  isResolved: boolean;
  resolvedBy: string;
  resolvedTimestamp: string;
  traceId: number;
  traceText: string;
  traceTimestamp: string;
}

export interface PaymentDetails {
  paymentType: string;
  cardType: string;
  cardNumber: string;
  amount: string;
  expirationDate: string;
}

export interface CardType {
  icon: string;
  pattern: string;
}

export interface ManageRoomAssignmentEvent {
  expandedState: boolean;
  room?: Room;
}
