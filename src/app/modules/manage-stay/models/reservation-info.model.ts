import {CardModel} from '@modules/check-in/models/check-in.models';

export interface ReservationInfoModel {
  guestInfo: GuestInfoModel;
  payInfo?: CardModel;
}

export interface GuestInfoModel {
  guestName: string;
  resNumber: string;
  rcNumber: string;
  rcPoints: number | string;
  checkInDate: string;
  checkOutDate: string;
  nights: number;
  adults: number;
  children: number;
  rooms: number;
  groupName: string;
  companyName: string;
  rateCode: string;
  estimatedTotal: number | string;
  currencyCode: string;
  badges: string[];
}
