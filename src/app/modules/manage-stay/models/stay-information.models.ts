import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export interface StayInformationModel {
  adultQuantity: number;
  childQuantity: number;
  corporateID: string;
  groupCode: string;
  checkOutDate: string;
  rateCategoryCode: string;
  roomTypeCode: string;
  arrivalDate: string;
  numberOfRooms: number;
  numberOfNights: number;
}

export interface StayInformationReservationModel {
  reservation: StayInformationModel;
}

export interface AddTaskForm {
  traceText: string;
  departmentCode: string
}


