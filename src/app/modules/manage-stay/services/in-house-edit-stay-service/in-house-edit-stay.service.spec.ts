import { TestBed } from '@angular/core/testing';
import { ClipboardModule, ClipboardService } from 'ngx-clipboard';

import { InHouseEditStayService } from './in-house-edit-stay.service';

describe('InHouseEditStayService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ClipboardModule],
    providers: [ClipboardService]
  }));

  it('should be created', () => {
    const service: InHouseEditStayService = TestBed.get(InHouseEditStayService);
    expect(service).toBeTruthy();
  });

  it('should display', () => {
    const service: InHouseEditStayService = TestBed.get(InHouseEditStayService);
    expect(service).toBeTruthy();
  });
});
