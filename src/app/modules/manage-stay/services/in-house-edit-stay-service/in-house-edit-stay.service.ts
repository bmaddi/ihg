import { Injectable } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';
import * as moment from 'moment';

import { Alert, AlertButton, AlertButtonType, AlertType } from 'ihg-ng-common-core';

import { RoomConflict } from '@modules/offers/models/room-conflict.interface';
import { Subject } from 'rxjs';
import { AppConstants } from '@app/constants/app-constants';

@Injectable({
  providedIn: 'root'
})
export class InHouseEditStayService {

  public unassignedGuestEvent = new Subject<RoomConflict>();

  constructor(private clipboardService: ClipboardService) {
  }

  public setUnassignedGuestEvent(conflict: RoomConflict): void {
    this.unassignedGuestEvent.next(conflict);
  }

  public createClipboardData(conflict: RoomConflict): string {
    const checkInDate = moment(conflict.checkInDate).format(AppConstants.VW_DT_FORMAT).toUpperCase();
    return `${conflict.reservationNumber} ${conflict.lastName}, ${conflict.firstName} ${checkInDate}`;
  }

  public getUnassignedGuestWarning(unassignedGuestData: Array<string>): Alert {
    return {
      type: AlertType.Warning,
      message: 'COM_TOAST_WARNING',
      detailMessage: 'LBL_WARN_GST_UNASSGND',
      translateValues: { detailMessage: { guestCount: unassignedGuestData.length } },
      dismissible: false,
      buttons: [this.getCopyToClipboardButton(unassignedGuestData)]
    };
  }

  private getCopyToClipboardButton(unassignedGuestData: Array<string>): AlertButton {
    return <AlertButton>{
      label: 'LBL_COPY',
      type: AlertButtonType.Warning,
      onClick: () => {
        this.clipboardService.copyFromContent(unassignedGuestData.join('\n'));
      }
    };
  }
}
