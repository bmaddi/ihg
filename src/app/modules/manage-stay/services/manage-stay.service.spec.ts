import { TestBed } from '@angular/core/testing';
import { ManageStayService } from './manage-stay.service';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { environment } from '@env/environment';
import { ROOM_ASSIGNMENT_DETAILS_RESPONSE,
        MOCK_GET_TASKS_RESPONSE,
        MOCK_FETCH_TASKS_RESPONSE,
        MOCK_TASKS_RESPONSE,
        MOCK_ADD_TASK_DATA } from '../mocks/manage-tasks-mock';
import { componentFactoryName } from '@angular/compiler';
import { ManageStayModel, ManageStayTasksDetails, AddTaskData, PaymentDetails } from '@modules/manage-stay/models/manage-stay.models';
import { HttpRequest } from '@angular/common/http';

describe('ManageStayService', () => {
  let modalServiceSpy: { get: jasmine.Spy };
  let service: ManageStayService;
  let httpTestingController: HttpTestingController;
  let request: TestRequest;

  beforeEach(() => {
      TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
          providers: [UserService, ConfirmationModalService, ManageStayService]
      });
      modalServiceSpy = jasmine.createSpyObj('ConfirmationModalService', ['openConfirmationModal']);
      httpTestingController = TestBed.get(HttpTestingController);
      service = TestBed.get(ManageStayService);
  });
  afterEach(() => {
      httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check getManageStayData method & check that returned Observable is valid', () => {
      const pmsReservationNumber = '123456';
      service.getManageStayData(pmsReservationNumber).subscribe( data => {
        expect(JSON.stringify(data)).toEqual(JSON.stringify(ROOM_ASSIGNMENT_DETAILS_RESPONSE));
      });

      const req = httpTestingController.
          expectOne(`${environment.fdkAPI}inHouse/getInHouseRoomDetails?reservationNumber=${pmsReservationNumber}`);

      expect(req.request.method).toEqual('GET');
      req.flush(ROOM_ASSIGNMENT_DETAILS_RESPONSE);
  });

  it('should catch error from getManageStayData method if http service is not successful', () => {
    const pmsReservationNumber = '123456';
    service.getManageStayData(pmsReservationNumber).subscribe( data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toBeGreaterThan(204);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
        return req.method === 'GET' && req.url.indexOf('inHouse/getInHouseRoomDetails') !== -1;
    });

    expect(request.request.method).toEqual('GET');
    expect(request.request.urlWithParams).toContain('123456');

    request.error(null, {status: 500, statusText: 'Bad Request'});
    httpTestingController.verify();
  });

  it('should check getManageStayTasks method & check that returned Observable is valid', () => {
      const pmsReservationNumber = '123456';
      service.getManageStayTasks(pmsReservationNumber).subscribe( data => {
        expect(JSON.stringify(data)).toEqual(JSON.stringify(MOCK_GET_TASKS_RESPONSE));
      });

      const req = httpTestingController.
          expectOne(`${environment.fdkAPI}inHouse/manageStayTaskDetails?reservationNumber=${pmsReservationNumber}`);

      expect(req.request.method).toEqual('GET');
      req.flush(MOCK_GET_TASKS_RESPONSE);
  });

  it('should catch error from getManageStayTasks method if http service is not successful', () => {
    const pmsReservationNumber = '123456';
    service.getManageStayTasks(pmsReservationNumber).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toBeGreaterThan(204);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
        return req.method === 'GET' && req.url.indexOf('inHouse/manageStayTaskDetails') !== -1;
    });

    expect(request.request.method).toEqual('GET');
    request.error(null, {status: 500, statusText: 'Bad Request'});
    httpTestingController.verify();
  });

  it('should check manageAddTask method & check that returned Observable is valid', () => {
    const pmsReservationNumber = '123456';
    const addTaskData = MOCK_ADD_TASK_DATA;
    service.manageAddTask(addTaskData, pmsReservationNumber).subscribe( data => {
        expect(data).toBeDefined();
    });
    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
        return req.method === 'PUT' && req.url.indexOf('traces/add/') !== -1;
    });

    expect(request.request.method).toEqual('PUT');
    expect(request.request.body['departmentCode']).toEqual('ZIF');
    expect(request.request.body['departmentName']).toEqual('ZIF');

    request.flush({});

    httpTestingController.verify();
  });

  it('should catch error from manageAddTask method if http service is not successful', () => {
    const pmsReservationNumber = '123456';
    const addTaskData = MOCK_ADD_TASK_DATA;
    service.manageAddTask(addTaskData, pmsReservationNumber).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toBeGreaterThan(204);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
        return req.method === 'PUT' && req.url.indexOf('traces/add/') !== -1;
    });

    expect(request.request.method).toEqual('PUT');
    expect(request.request.body['departmentCode']).toEqual('ZIF');
    expect(request.request.body['departmentName']).toEqual('ZIF');

    request.error(null, {status: 500, statusText: 'Bad Request'});
    httpTestingController.verify();
  });

  it('should check fetchAddTask method & check that returned Observable is valid', () => {
        const pmsReservationNumber = '123456';
        service.fetchAddTask(pmsReservationNumber).subscribe( data => {
            expect(data).toBeDefined();
        });
        request = httpTestingController.expectOne((req: HttpRequest<any>) => {
            return req.method === 'GET' && req.url.indexOf('traces/fetch') !== -1;
        });

        expect(request.request.method).toEqual('GET');
        expect(request.request.urlWithParams).toContain('123456');

        request.flush({});
        httpTestingController.verify();
  });

  it('should catch error from fetchAddTask method if http service is not successful', () => {
    const pmsReservationNumber = '123456';
    service.fetchAddTask(pmsReservationNumber).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toBeGreaterThan(204);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
        return req.method === 'GET' && req.url.indexOf('traces/fetch') !== -1;
    });

    expect(request.request.method).toEqual('GET');
    request.error(null, {status: 500, statusText: 'Bad Request'});

    httpTestingController.verify();
  });

  it('should check getPaymentDetails method & check that returned Observable is valid', () => {
        const pmsReservationNumber = '123456';
        service.getPaymentDetails(pmsReservationNumber).subscribe( data => {
            expect(data).toBeDefined();
        });
        request = httpTestingController.expectOne((req: HttpRequest<any>) => {
            return req.method === 'GET' && req.url.indexOf('inHouse/paymentDetails') !== -1;
        });

        expect(request.request.method).toEqual('GET');
        expect(request.request.urlWithParams).toContain('123456');

        request.flush({});
        httpTestingController.verify();
  });

  it('should catch error from getPaymentDetails method if http service is not successful', () => {
    const pmsReservationNumber = '123456';
    service.getPaymentDetails(pmsReservationNumber).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toBeGreaterThan(204);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpTestingController.expectOne((req: HttpRequest<any>) => {
        return req.method === 'GET' && req.url.indexOf('inHouse/paymentDetails') !== -1;
    });

    expect(request.request.method).toEqual('GET');
    request.error(null, {status: 500, statusText: 'Bad Request'});
    httpTestingController.verify();
  });

  it('should return updated response through  addTaskMap method', () => {
    const response = MOCK_TASKS_RESPONSE;
    service.addTaskMap(response);
    expect(service.addTaskMap(response).tasks[0].taskName).toEqual(service.addTaskMap(response).tasks[0].traceText);
    expect(service.addTaskMap(response).tasks[0].deptName).toEqual(service.addTaskMap(response).tasks[0].departmentCode);
  });

  it('should check getRateCodeData method & check that returned Observable is valid', () => {
        const rateCodeParam = {
            'checkInDate': '2020-01-15',
            'checkOutDate': '2020-01-11',
            'ihgRcNumber': '123456',
            'roomTypeCode': 'KEXG',
            'rateCategoryCode': 'IGBBB'
        };
        service.getRateCodeData(rateCodeParam).subscribe( data => {
            expect(data).toBeDefined();
        });
        request = httpTestingController.expectOne((req: HttpRequest<any>) => {
            return req.method === 'GET' && req.url.indexOf('offers/rateCategoryDetails') !== -1;
        });

        expect(request.request.method).toEqual('GET');
        expect(request.request.urlWithParams).toContain('123456');

        request.flush({});
        httpTestingController.verify();
  });
});
