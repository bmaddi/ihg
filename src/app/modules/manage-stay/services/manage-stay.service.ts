import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { ManageStayModel, ManageStayTasksDetails, AddTaskData, PaymentDetails } from '@modules/manage-stay/models/manage-stay.models';
import { RateCategoryModel } from '@modules/offers/models/offers.models';

@Injectable({
  providedIn: 'root'
})
export class ManageStayService {

  private readonly API_URL = environment.fdkAPI;

  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public getManageStayData(reservationNumber: string): Observable<ManageStayModel> {
    const apiURl = `${this.API_URL}inHouse/getInHouseRoomDetails?reservationNumber=${reservationNumber}`;
    return this.http.get(apiURl).pipe(map((response: ManageStayModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getManageStayTasks(reservationNumber: string): Observable<ManageStayTasksDetails> {
    const apiURl = `${this.API_URL}inHouse/manageStayTaskDetails?reservationNumber=${reservationNumber}`;
    return this.http.get(apiURl).pipe(map((response: ManageStayTasksDetails) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public manageAddTask(task: AddTaskData, pmsReservationNumber: string): Observable<ManageStayTasksDetails> {
    const apiURl = `${this.API_URL}traces/add/${this.getLocationId()}?pmsReservationNumber=${pmsReservationNumber}`;
    return this.http.put(apiURl, task).pipe(map((response: ManageStayTasksDetails) => {
      return response;
    })).pipe(
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  public fetchAddTask(pmsReservationNumber: string): Observable<ManageStayTasksDetails> {
    const apiURl = `${this.API_URL}traces/fetch/${this.getLocationId()}?pmsReservationNumber=${pmsReservationNumber}`;
    return this.http.get(apiURl).pipe(map((response: ManageStayTasksDetails) => {
      return response;
    })).pipe(
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  public getPaymentDetails(reservationNumber: string): Observable<PaymentDetails> {
    const apiURl = `${this.API_URL}inHouse/paymentDetails?reservationNumber=${reservationNumber}`;
    return this.http.get(apiURl).pipe(map((response: PaymentDetails) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getRateCodeData(rateDetailsData: any): Observable<RateCategoryModel> {
    const loyaltyparam = rateDetailsData.ihgRcNumber ? `&ihgRcNumber=${rateDetailsData.ihgRcNumber}` : '';
    const apiURl = `${this.API_URL}offers/rateCategoryDetails?checkInDate=${rateDetailsData.checkInDate}&checkOutDate=${rateDetailsData.checkOutDate}${loyaltyparam}&roomTypeCode=${rateDetailsData.roomTypeCode}&rateCategoryCode=${rateDetailsData.rateCategoryCode}`;
    return this.http.get(apiURl).pipe(map((response: RateCategoryModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public addTaskMap(response: ManageStayTasksDetails) {
    response.tasks.forEach(item => {
      item.taskName = item.traceText;
      item.deptName = item.departmentCode;
    });
    return response;
  }
}
