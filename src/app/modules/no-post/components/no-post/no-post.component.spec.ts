import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { HttpErrorResponse } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { of, throwError } from 'rxjs';

import {
  AlertButton,
  ApiErrorService,
  ApiService,
  DetachedToastMessageService,
  EnvironmentService,
  GoogleAnalyticsService,
  ToastMessageOutletService,
  UserService
} from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { NoPostComponent } from './no-post.component';
import { ToggleSwitchComponent } from '@app-shared/components/toggle-switch/toggle-switch.component';
import { NoPostService } from '@modules/no-post/services/no-post.service';
import { googleAnalyticsStub, mockEnvs, mockUser } from '@app/constants/app-test-constants';
import { AppErrorsService } from '@app-shared/services/app-service-errors/app-errors.service';
import { SERVICE_ERROR_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { NO_POST_CONSTANTS } from '@modules/no-post/constants/no-post.constants';
import { NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL, NO_POST_MANAGE_STAY_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

describe('NoPostComponent', () => {
  let component: NoPostComponent;
  let fixture: ComponentFixture<NoPostComponent>;
  let counterService: ApiErrorService;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(DetachedToastMessageService, {
        useValue: {
          success: (s1, s2): void => {
          }
        }
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ToastMessageOutletService, {
        useValue: {
          danger: (message?: string,
                   detailMessage?: string,
                   buttons?: AlertButton[],
                   clearPreviousAlerts?: boolean,
                   dismissible?: boolean,
                   keepAfterRouteChange?: boolean): void => {
          },
          clear: (): void => {
          }
        }
      })
      .overrideProvider(ReportIssueService, {
        useValue: {
          openAutofilledReportIssueModal: (c: any, reportIssueAutoFill: any): void => {
          },
          openReportIssueModal: (): void => {
          }
        }
      })
      .configureTestingModule({
        imports: [
          TranslateModule.forChild(),
          FormsModule,
          JWBootstrapSwitchModule,
          HttpClientTestingModule
        ],
        declarations: [
          NoPostComponent,
          ToggleSwitchComponent
        ],
        providers: [
          NoPostService,
          TranslateStore,
          TranslateService,
          ApiService,
          EnvironmentService,
          UserService,
          DetachedToastMessageService,
          ReportIssueService,
          ToastMessageOutletService,
          ApiErrorService,
          AppErrorsService,
          GoogleAnalyticsService
        ]
      })
      .compileComponents();

    const environmentService = TestBed.get(EnvironmentService);
    const userService = TestBed.get(UserService);
    environmentService.setEnvironmentConstants(mockEnvs);
    userService.setUser(mockUser);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoPostComponent);
    component = fixture.componentInstance;
    counterService = fixture.componentRef.injector.get(ApiErrorService);
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
    expect(component.noPostState).toBeFalsy();
    expect(component.togglePresent).toBeFalsy();
    expect(component.reservationNumber).toBeFalsy();
    expect(component.disabled).toBeFalsy();
    expect(component.stateName).toEqual(AppStateNames.checkIn);
  });

  it('should only show No Post toggle if boolean is not undefined nor null', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.togglePresent).toBeFalsy();

    component.noPostState = false;
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.togglePresent).toBeTruthy();
  });

  it('should show toggle and update parent component on toggle click', () => {
    component.noPostState = false;
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.togglePresent).toBeTruthy();
    const toggleChangeSpy = spyOn(component, 'toggleChange').and.callThrough();
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPost-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostToggle-SID"]'));
    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_POST');
    expect(toggle).not.toBeNull();

    toggle.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(toggleChangeSpy).toHaveBeenCalledWith(true);
  });

  it('should make API call on toggle switch click and only update noPostState after successful update #handleSuccessfulUpdate', () => {
    const updateNoPostSpy = spyOn(TestBed.get(NoPostService), 'updateNoPost').and.returnValue(of({}));
    const updateNoToggleStatusSpy = spyOn<any>(component, 'updateNoToggleStatus').and.callThrough();
    const handleSuccessfulUpdateSpy = spyOn<any>(component, 'handleSuccessfulUpdate').and.callThrough();

    component.noPostState = false;
    component.reservationNumber = '11111111';
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.togglePresent).toBeTruthy();
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPost-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostToggle-SID"]'));
    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_POST');
    expect(toggle).not.toBeNull();

    toggle.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(updateNoPostSpy).toHaveBeenCalled();
    expect(updateNoToggleStatusSpy).toHaveBeenCalledWith(true);
    expect(handleSuccessfulUpdateSpy).toHaveBeenCalledWith(true);
    expect(component.noPostState).toBeTruthy();
  });

  it('should make API call on toggle switch click and not update noPostState after update failure #handleUpdateFailure ', () => {
    component.noPostState = true;
    component.reservationNumber = '11111111';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.togglePresent).toBeTruthy();
    const updateNoPostSpy = spyOn(TestBed.get(NoPostService), 'updateNoPost')
      .and.returnValue(throwError({
        status: 500
      }));
    const handleUpdateFailureSpy = spyOn<any>(component, 'handleUpdateFailure').and.callThrough();

    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostToggle-SID"]'));
    expect(toggle).not.toBeNull();
    toggle.nativeElement.click();

    fixture.detectChanges();
    expect(component.noPostState).toBeTruthy();
    expect(updateNoPostSpy).toHaveBeenCalled();
    expect(handleUpdateFailureSpy).toHaveBeenCalled();

  });

  it('should show correct toast messages depending on new toggle state', () => {
    const toggleChangeSpy = spyOn(TestBed.get(DetachedToastMessageService), 'success').and.callThrough();

    component.noPostState = false;
    component['handleSuccessfulUpdate'](true);

    expect(toggleChangeSpy).toHaveBeenCalled();
    expect(toggleChangeSpy).toHaveBeenCalledWith('LBL_NO_POST_ON', 'LBL_NO_INCIDENTAL_CHARGES');

    component.noPostState = false;
    fixture.detectChanges();

    component['handleSuccessfulUpdate'](false);
    expect(toggleChangeSpy).toHaveBeenCalledWith('LBL_NO_POST_OFF', 'LBL_INCIDENTAL_CHARGES_CAN_CHARGE');
  });

  it('should call Google Analytics #trackEvent on toggle switch click upon update failure #check-in', () => {
    const toggleChangeSpy = spyOn(component, 'toggleChange').and.callThrough();
    const updateNoPostSpy = spyOn(TestBed.get(NoPostService), 'updateNoPost')
      .and.returnValue(throwError({status: 500}));
    const googleAnalyticsSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent');
    const handleUpdateFailureSpy = spyOn<any>(component, 'handleUpdateFailure').and.callThrough();
    component.noPostState = true;
    component.reservationNumber = '11111111';
    component.stateName = AppStateNames.checkIn;
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.togglePresent).toBeTruthy();
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostToggle-SID"]'));
    expect(toggle).not.toBeNull();
    toggle.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(toggleChangeSpy).toHaveBeenCalled();
    expect(updateNoPostSpy).toHaveBeenCalled();
    expect(handleUpdateFailureSpy).toHaveBeenCalled();
    expect(googleAnalyticsSpy).toHaveBeenCalledWith(NO_POST_CONSTANTS.categoryMap['check-in'],
      NO_POST_CONSTANTS.Action_OFF + 'Failure', NO_POST_CONSTANTS.LABEL);
    expect(component.noPostState).toBeTruthy();
  });

  it('should call Google Analytics #trackEvent on toggle switch click upon successful update #check-in', () => {
    const toggleChangeSpy = spyOn(component, 'toggleChange').and.callThrough();
    const updateNoPostSpy = spyOn(TestBed.get(NoPostService), 'updateNoPost').and.returnValue(of({}));
    const updateNoToggleStatusSpy = spyOn<any>(component, 'updateNoToggleStatus').and.callThrough();
    const handleSuccessfulUpdateSpy = spyOn<any>(component, 'handleSuccessfulUpdate').and.callThrough();
    const googleAnalyticsSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();

    component.noPostState = false;
    component.reservationNumber = '11111111';
    component.stateName = AppStateNames.checkIn;
    fixture.detectChanges();

    component.ngOnInit();
    fixture.detectChanges();

    expect(component.togglePresent).toBeTruthy();
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPost-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostToggle-SID"]'));
    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_POST');
    expect(toggle).not.toBeNull();
    toggle.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(toggleChangeSpy).toHaveBeenCalled();
    expect(updateNoPostSpy).toHaveBeenCalled();
    expect(updateNoToggleStatusSpy).toHaveBeenCalled();
    expect(handleSuccessfulUpdateSpy).toHaveBeenCalled();
    expect(googleAnalyticsSpy).toHaveBeenCalledWith(NO_POST_CONSTANTS.categoryMap['check-in'],
      NO_POST_CONSTANTS.Action_ON + 'Success', NO_POST_CONSTANTS.LABEL);
    expect(component.noPostState).toBeTruthy();
  });

  it('should show error toast message if error is thrown and increment error counter', () => {
    component.noPostState = false;
    expect(counterService['errorCount']).toEqual(0);
    const dangerMessageSpy = spyOn(TestBed.get(ToastMessageOutletService), 'danger').and.callThrough();

    component['handleUpdateFailure'](new HttpErrorResponse({status: 500}));
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(1);

    component['handleUpdateFailure'](new HttpErrorResponse({status: 500}));
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(2);

    component['handleUpdateFailure'](new HttpErrorResponse({status: 500}));
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(3);
    expect(dangerMessageSpy).toHaveBeenCalledTimes(3);
  });

  it('should show error toast message with Report an Issue on third failure and have correct autofill provided for General Error', () => {
    component.noPostState = false;
    component.stateName = AppStateNames.checkIn;
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(0);
    const dangerMessageSpy = spyOn(TestBed.get(ToastMessageOutletService), 'danger').and.callThrough();
    const openReportIssueModalSpy = spyOn(TestBed.get(AppErrorsService), 'openReportIssueModal').and.callThrough();

    component['handleUpdateFailure'](new HttpErrorResponse({status: 500}));

    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(1);

    component['handleUpdateFailure'](new HttpErrorResponse({status: 500}));

    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(2);

    component['handleUpdateFailure'](new HttpErrorResponse({status: 500}));

    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(3);
    expect(counterService.maxErrorsReached).toBeTruthy();
    expect(dangerMessageSpy).toHaveBeenCalledTimes(3);

    const serviceErrorType = TestBed.get(AppErrorsService).getErrorType(component.emitError.getValue());

    expect(serviceErrorType).toEqual(2);

    component['handleTryAgain'](SERVICE_ERROR_TYPE[serviceErrorType]);

    expect(openReportIssueModalSpy).toHaveBeenCalledWith(NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL.GENERAL);

  });

  it('should show error toast message with Report an Issue on third failure and have correct autofill provided for Timeout Error', () => {
    const dangerMessageSpy = spyOn(TestBed.get(ToastMessageOutletService), 'danger').and.callThrough();
    const openReportIssueModalSpy = spyOn(TestBed.get(AppErrorsService), 'openReportIssueModal').and.callThrough();

    component.noPostState = false;
    component.stateName = AppStateNames.checkIn;
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(0);


    component['handleUpdateFailure'](new HttpErrorResponse({status: 504}));
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(1);
    component['handleUpdateFailure'](new HttpErrorResponse({status: 504}));
    fixture.detectChanges();
    expect(counterService['errorCount']).toEqual(2);

    component['handleUpdateFailure'](new HttpErrorResponse({status: 504}));
    fixture.detectChanges();

    expect(counterService['errorCount']).toEqual(3);
    expect(counterService.maxErrorsReached).toBeTruthy();
    expect(dangerMessageSpy).toHaveBeenCalledTimes(3);

    const serviceErrorType = TestBed.get(AppErrorsService).getErrorType(component.emitError.getValue());
    expect(serviceErrorType).toEqual(0);
    component['handleTryAgain'](SERVICE_ERROR_TYPE[serviceErrorType]);

    expect(openReportIssueModalSpy).toHaveBeenCalledWith(NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL.TIME_OUT);
  });

  it('should set proper Report An Issue Autofill depending on state', () => {
    const timeoutErrorType = SERVICE_ERROR_TYPE['0'];
    const generalErrorType = SERVICE_ERROR_TYPE['2'];

    component.stateName = AppStateNames.checkIn;
    fixture.detectChanges();
    const checkInAutoFill = component['setProperAutoFill'](timeoutErrorType);
    const checkInAutoFillGeneral = component['setProperAutoFill'](generalErrorType);

    expect(checkInAutoFill).toEqual(NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL[timeoutErrorType]);
    expect(checkInAutoFillGeneral).toEqual(NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL[generalErrorType]);

    component.stateName = AppStateNames.manageStay;
    fixture.detectChanges();

    const mgStayAutoFill = component['setProperAutoFill'](timeoutErrorType);
    const mgStayAutoFillGeneral = component['setProperAutoFill'](generalErrorType);

    expect(mgStayAutoFill).toEqual(NO_POST_MANAGE_STAY_REPORT_ISSUE_AUTOFILL[timeoutErrorType]);
    expect(mgStayAutoFillGeneral).toEqual(NO_POST_MANAGE_STAY_REPORT_ISSUE_AUTOFILL[generalErrorType]);
  });

  it('should properly handle object path autofill null or undefined scenarios for stateName & errorType', () => {
    const timeoutErrorType = SERVICE_ERROR_TYPE['0'];
    const generalErrorType = SERVICE_ERROR_TYPE['2'];

    component.stateName = null;
    fixture.detectChanges();

    expect(component['setProperAutoFill'](timeoutErrorType)).toBeNull();
    expect(component['setProperAutoFill'](generalErrorType)).toBeNull();
    expect(component['setProperAutoFill'](null)).toBeNull();

    component.stateName = AppStateNames.manageStay;
    fixture.detectChanges();

    expect(component['setProperAutoFill'](null)).toBeNull();
  });
});
