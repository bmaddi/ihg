import { Component, Input, OnInit } from '@angular/core';
import { get, isNil } from 'lodash';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import {
  AlertButton,
  AlertButtonType,
  ApiErrorService,
  DetachedToastMessageService,
  ToastMessageOutletService,
  GoogleAnalyticsService
} from 'ihg-ng-common-core';

import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { AppErrorsService } from '@app-shared/services/app-service-errors/app-errors.service';
import { NoPostService } from '../../services/no-post.service';
import { SERVICE_ERROR_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { NO_POST_CONSTANTS, noPostErrors } from '../../constants/no-post.constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Component({
  selector: 'app-no-post',
  templateUrl: './no-post.component.html',
  styleUrls: ['./no-post.component.scss'],
  providers: [ApiErrorService]
})
export class NoPostComponent extends AppErrorBaseComponent implements OnInit {
  @Input() noPostState: boolean;
  @Input() reservationNumber: string;
  @Input() pmsUniqueNumber: string;
  @Input() disabled = false;
  @Input() stateName = AppStateNames.checkIn;

  togglePresent: boolean;
  SUCCESS_FLAG = 'Success';
  FAILURE_FLAG = 'Failure';
  private noPost$: Subscription = new Subscription();

  constructor(
    private apiErrorCounter: ApiErrorService,
    private appErrorsService: AppErrorsService,
    private detachedToastService: DetachedToastMessageService,
    private noPostService: NoPostService,
    private staticToastService: ToastMessageOutletService,
    private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.togglePresent = !isNil(this.noPostState);
  }

  toggleChange(toggleState: boolean) {
    if (this.reservationNumber) {
      this.updateNoToggleStatus(toggleState);
    }
  }

  private updateNoToggleStatus(toggleState: boolean) {
    this.noPost$.add(
      this.noPostService.updateNoPost({
        noPost: toggleState,
        resNum: this.reservationNumber,
        pmsUniqueNumber: this.pmsUniqueNumber
      })
        .subscribe(
          () => this.handleSuccessfulUpdate(toggleState),
          error => this.handleUpdateFailure(error))
    );
  }

  private handleSuccessfulUpdate(toggleState: boolean) {
    this.noPostState = toggleState;
    this.apiErrorCounter.resetCounter();
    this.staticToastService.clear();
    this.detachedToastService.success(this.noPostState ? 'LBL_NO_POST_ON' : 'LBL_NO_POST_OFF',
      this.noPostState ? 'LBL_NO_INCIDENTAL_CHARGES' : 'LBL_INCIDENTAL_CHARGES_CAN_CHARGE');
    this.trackClickNoPostSuccess(this.stateName, this.noPostState);
  }

  private handleTryAgain(serviceErrorType: string) {
    this.apiErrorCounter.maxErrorsReached()
      ? this.appErrorsService.openReportIssueModal(this.setProperAutoFill(serviceErrorType))
      : this.toggleChange(!this.noPostState);
  }

  private setProperAutoFill(errorType: string): ReportIssueAutoFillData {
    return get(noPostErrors, [this.stateName, errorType], null);
  }

  private handleUpdateFailure(error: HttpErrorResponse) {
    this.processHttpErrors(error);
    this.apiErrorCounter.countError();
    const serviceErrorType = this.appErrorsService.getErrorType(this.emitError.getValue());
    this.showErrorMessage(SERVICE_ERROR_TYPE[serviceErrorType]);
    this.trackClickNoPostFailure(this.stateName, this.noPostState);
  }

  private showErrorMessage(serviceErrorType: string) {
    const tryAgainButton: AlertButton[] = [{
      label: this.apiErrorCounter.maxErrorsReached() ? 'REPORT_AN_ISSUE_MENU' : 'LBL_ERR_TRYAGAIN',
      type: AlertButtonType.Danger,
      onClick: () => this.handleTryAgain(serviceErrorType)
    }];
    this.staticToastService.danger('COM_TOAST_ERR', 'LBL_NO_POST_CHANGES_FAIL', tryAgainButton);
  }

  private getCategory(stateName: string): string {
    return NO_POST_CONSTANTS.categoryMap[stateName];
  }

  private getAction(noPostFlag: boolean, flagValue: string): string {
    if (noPostFlag) {
      return NO_POST_CONSTANTS.Action_ON + flagValue;
    } else {
      return NO_POST_CONSTANTS.Action_OFF + flagValue;
    }
  }

  private trackClickNoPostSuccess(stateName: string, noPostFlag: boolean) {
    const categoryVal = this.getCategory(stateName);
    const actionVal = this.getAction(noPostFlag, this.SUCCESS_FLAG);
    this.gaService.trackEvent(categoryVal, actionVal, NO_POST_CONSTANTS.LABEL);
  }

  private trackClickNoPostFailure(stateName: string, noPostFlag: boolean) {
    const categoryVal = this.getCategory(stateName);
    const actionVal = this.getAction(!noPostFlag, this.FAILURE_FLAG);
    this.gaService.trackEvent(categoryVal, actionVal, NO_POST_CONSTANTS.LABEL);
  }
}
