import { NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL, NO_POST_MANAGE_STAY_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';

export const noPostErrors = {
  ['check-in']: NO_POST_CHECK_IN_REPORT_ISSUE_AUTOFILL,
  ['manage-stay']: NO_POST_MANAGE_STAY_REPORT_ISSUE_AUTOFILL
};

export const NO_POST_CONSTANTS = {
  LABEL: 'Selected',
  categoryMap: {
    'manage-stay': 'Guest List - In House',
    'check-in': 'Check In'
  },
  Action_ON: 'Turn No Post ON - ',
  Action_OFF: 'Turn No Post OFF - ',
}
