export interface ModifyBookingPayload {
  doNotMoveRoom?: boolean;
  noPost?: boolean;
  resNum: string;
  pmsUniqueNumber: string;
}
