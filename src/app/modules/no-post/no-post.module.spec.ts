import { NoPostModule } from './no-post.module';

describe('NoPostModule', () => {
  let noPostModule: NoPostModule;

  beforeEach(() => {
    noPostModule = new NoPostModule();
  });

  it('should create an instance', () => {
    expect(noPostModule).toBeTruthy();
  });
});
