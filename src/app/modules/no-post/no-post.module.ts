import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoPostComponent } from './components/no-post/no-post.component';
import { AppSharedModule } from '../shared/app-shared.module';

@NgModule({
  imports: [
    CommonModule,
    AppSharedModule
  ],
  exports: [
    NoPostComponent
  ],
  declarations: [
    NoPostComponent
  ]
})
export class NoPostModule {
}
