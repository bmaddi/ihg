import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';

import { ApiService, EnvironmentService, UserService } from 'ihg-ng-common-core';

import { NoPostService } from './no-post.service';
import { mockEnvs, mockUser } from '@app/constants/app-test-constants';
import { ModifyBookingPayload } from '../interfaces/modify-booking-payload.interface';

describe('NoPostService', () => {
  let service: NoPostService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        providers: [ApiService, EnvironmentService, UserService],
        imports: [HttpClientTestingModule]
      });

    const environmentService = TestBed.get(EnvironmentService);
    const userService = TestBed.get(UserService);
    environmentService.setEnvironmentConstants(mockEnvs);
    userService.setUser(mockUser);

    service = TestBed.get(NoPostService);
    httpMockController = TestBed.get(HttpTestingController);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return Observable void if successful', () => {
    service.updateNoPost(<ModifyBookingPayload>{noPost: true, resNum: '11111111', pmsUniqueNumber: '222222'})
      .subscribe(data => {
        expect(data).toBeDefined();
      }, () => {
        fail();
      });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'POST' && req.url.indexOf('booking/updateBookingInfo') !== -1;
    });

    expect(request.request.body['noPost']).toBeTruthy();
    expect(request.request.body['resNum']).toEqual('11111111');
    expect(request.request.body['pmsUniqueNumber']).toEqual('222222');

    request.flush({});

    httpMockController.verify();
  });

  it('should catch error if any', () => {
    service.updateNoPost(<ModifyBookingPayload>{noPost: true, resNum: '11111111', pmsUniqueNumber: '222222'}).subscribe(data => {
      fail();
    }, error => {
      expect(error).toBeDefined();
      expect(error.status).toEqual(500);
      expect(error.statusText).toEqual('Bad Request');
    });

    request = httpMockController.expectOne((req: HttpRequest<any>) => {
      return req.method === 'POST' && req.url.indexOf('booking/updateBookingInfo') !== -1;
    });

    expect(request.request.body['noPost']).toBeTruthy();
    expect(request.request.body['resNum']).toEqual('11111111');
    expect(request.request.body['pmsUniqueNumber']).toEqual('222222');

    request.error(null, {status: 500, statusText: 'Bad Request'});

    httpMockController.verify();
  });


});
