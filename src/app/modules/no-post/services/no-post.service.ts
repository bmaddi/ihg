import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

import { ApiService, ApiUrl, EnvironmentService, UserService } from 'ihg-ng-common-core';

import { ModifyBookingPayload } from '../interfaces/modify-booking-payload.interface';

@Injectable({
  providedIn: 'root'
})
export class NoPostService {

  constructor(private apiService: ApiService,
              private environmentService: EnvironmentService,
              private userService: UserService) {
  }

  updateNoPost(payload: ModifyBookingPayload): Observable<void> {
    return this.apiService.post(
      new ApiUrl(this.environmentService.getEnvironmentConstants()['fdkAPI'],
        `booking/updateBookingInfo/${this.userService.getCurrentLocationId()}`),
      payload,
      null,
      new HttpHeaders().set('spinnerConfig', 'Y')
    );
  }
}
