import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, SimpleChange, DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateStore } from '@ngx-translate/core';
import { of } from 'rxjs';

import { ReportIssueService } from 'ihg-ng-common-pages';
import { EnvironmentService } from 'ihg-ng-common-core';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { AverageRateComponent } from './avg-rate.component';
import { mockEnvs } from '@app/constants/app-test-constants';
import { MOCK_ORIGINAL_RESERVATION_DATA, MOCK_RATEINFO_AFTER } from '@app/modules/offers/mocks/warning-modal.mock';
import { By } from '@angular/platform-browser';
import { NgxMaskModule } from 'ngx-mask';
import * as cloneDeep from 'lodash/cloneDeep';


describe('AverageRateComponent', () => {
  let component: AverageRateComponent;
  let fixture: ComponentFixture<AverageRateComponent>;
  let input: DebugElement;

  function setUpEditableAvgNightlyRate() {
    component.hasRateOverride = true;
    component.editableMode = true;
    component.ngOnInit();
    fixture.detectChanges();
  }

  function dispatchEventSetUp(input: DebugElement,fieldVal: string) {    
    input.nativeElement.value = fieldVal;
    input.nativeElement.dispatchEvent(new Event('input'));
    input.nativeElement.dispatchEvent(new Event('blur'));
  }

  function setUpExtraPersonCharge() {
    let tempRateInfoAfter  = cloneDeep(MOCK_RATEINFO_AFTER);
    tempRateInfoAfter['rateInfo']['totalExtraPersonAmount'] = "120.00";
    component.rateInfoAfter = tempRateInfoAfter;
    fixture.detectChanges();
  }

  function setUpTotalServiceCharge() {
    let tempRateInfoAfter  = cloneDeep(MOCK_RATEINFO_AFTER);
    tempRateInfoAfter['rateInfo']['totalServiceChargeAmount'] = "120.00";
    component.rateInfoAfter = tempRateInfoAfter;
    fixture.detectChanges();
  }

  function checkPendingStateOnFormDirty(elm: string) {
    component.avgNightlyRateForm.markAsDirty();
    fixture.detectChanges();
    const elmToCheck = fixture.debugElement.query(By.css(elm));
    expect(elmToCheck.nativeElement).toBeDefined();
    expect(elmToCheck.nativeElement.textContent.trim()).toBe('LBL_PENDING');
  }

  function checkNoPendingStateOnFormPristine(elm: string) {
    component.avgNightlyRateForm.markAsPristine();
    fixture.detectChanges();
    const elmToCheck = fixture.debugElement.query(By.css(elm));
    expect(elmToCheck).toBeNull();
  }

  function checkIfCellHighlighted(elm: string) {
    component.avgNightlyRateForm.markAsDirty();
    fixture.detectChanges();
    const elmToCheck = fixture.debugElement.query(By.css(elm)).nativeElement;    
    const elmClassList = elmToCheck.attributes.class.ownerElement.classList;
    expect(elmClassList.contains('highlight-cell')).toBeFalsy();
  }

  function checkHasHighlight(elm: string) {
    component.avgNightlyRateForm.markAsPristine();
    fixture.detectChanges();
    const elmToCheck = fixture.debugElement.query(By.css(elm)).nativeElement;    
    const elmClassList = elmToCheck.attributes.class.ownerElement.classList;
    expect(elmClassList.contains('highlight-cell')).toBeTruthy();
  }

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
  })

  beforeEach(async(() => {
    
    TestBed
      .configureTestingModule({
        declarations: [AverageRateComponent],
        imports: [CommonTestModule, HttpClientTestingModule, NgxMaskModule.forRoot()],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [NgbActiveModal, TranslateStore, EnvironmentService, FormBuilder,
          {provide: ReportIssueService, useValue: {}},
          {provide: GoogleAnalyticsService, useValue: googleAnalyticsStub}]
      })
      .compileComponents();
    TestBed.get(EnvironmentService).setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AverageRateComponent);
    component = fixture.componentInstance;
    component.rateInfoBefore = MOCK_ORIGINAL_RESERVATION_DATA;
    component.rateInfoAfter = MOCK_RATEINFO_AFTER;
    component.editableMode = false;
    component.hasRateOverride = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check if createForm method is called when hasRateOverride is true', () => {
    const spyCreateForm = spyOn<any>(component, 'createForm').and.callThrough();
    setUpEditableAvgNightlyRate();
    expect(spyCreateForm).toHaveBeenCalled();
  });

  it('check if addAvgNighlyRate method is called when hasRateOverride is true', () => {
    const spyAddAvgNighlyRate = spyOn<any>(component, 'addAvgNighlyRate').and.callThrough();
    setUpEditableAvgNightlyRate();
    expect(spyAddAvgNighlyRate).toHaveBeenCalled();
  });

  it('check if createAvgNighlyRate method is called when hasRateOverride is true', () => {
    const spyCreateAvgNighlyRate = spyOn<any>(component, 'createAvgNighlyRate').and.callThrough();
    setUpEditableAvgNightlyRate();
    expect(spyCreateAvgNighlyRate).toHaveBeenCalled();
  });

  it('check if text inside formcontrol is right aligned', () => {
    setUpEditableAvgNightlyRate();
    const elm = fixture.debugElement.query(By.css('.form-control'));
    const elmClassList = elm.nativeElement.attributes.class.ownerElement.classList;
    expect(elmClassList.contains('text-right')).toBeTruthy();
  });

  it('check if input field is allowing only two numbers after decimal', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      input = fixture.debugElement.query(By.css('.form-control'));
      dispatchEventSetUp(input,"123.345");
      expect(input.nativeElement.value).toBe("123.34");
    });
  }));

  it('check if transformToTwoDigitsAfterDecimal method is called on input field blur event', async(() => {
      setUpEditableAvgNightlyRate();
      fixture.whenStable().then(() => {
        input = fixture.debugElement.query(By.css('.form-control'));
        const transformToTwoDigitsAfterDecimalSpy = spyOn(component, 'transformToTwoDigitsAfterDecimal');
        dispatchEventSetUp(input,"123.345");
        expect(transformToTwoDigitsAfterDecimalSpy).toHaveBeenCalled();
      });
  }));

  xit('check if input field value is fixed to 2 decimal places', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      input = fixture.debugElement.query(By.css('.form-control'));
      dispatchEventSetUp(input,"123.2");
      expect(input.nativeElement.value).toBe("123.20");
    });
  }));

  it('check if input field value has thousand separator', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      input = fixture.debugElement.query(By.css('.form-control'));
      dispatchEventSetUp(input,"120000.34");
      expect(input.nativeElement.value).toBe("120,000.34");
    });
  }));

  it('check if TotalExtraPersonChanged column is showing pending text if avgNightlyRateForm is dirty', async(() => {
    setUpExtraPersonCharge();
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkPendingStateOnFormDirty('[data-slnm-ihg="TotalExtraPersonChanged-SID"] span');
    });    
  }));

  it('check if TotalExtraPersonChanged column is not showing pending text if avgNightlyRateForm is pristine', async(() => {
    setUpExtraPersonCharge();
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkNoPendingStateOnFormPristine('[data-slnm-ihg="TotalExtraPersonChanged-SID"] span');
    });    
  }));

  it('check if TotalExtraPersonChanged column is not highlighted if avgNightlyRateForm is dirty', async(() => {
    setUpExtraPersonCharge();
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkIfCellHighlighted('[data-slnm-ihg="TotalExtraPersonChanged-SID"]');
    });    
  }));

  it('check if TotalExtraPersonChanged column is highlighted if avgNightlyRateForm is pristine', async(() => {
    setUpExtraPersonCharge();
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkHasHighlight('[data-slnm-ihg="TotalExtraPersonChanged-SID"]');
    });    
  }));

  it('check if TotalServiceChargeChanged column is showing pending text if avgNightlyRateForm is dirty', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkPendingStateOnFormDirty('[data-slnm-ihg="TotalServiceChargeChanged-SID"] span');
    });    
  }));

  it('check if TotalServiceChargeChanged column is not highlighted if avgNightlyRateForm is dirty', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkIfCellHighlighted('[data-slnm-ihg="TotalServiceChargeChanged-SID"]');
    });    
  }));

  it('check if TotalServiceChargeChanged column is highlighted if avgNightlyRateForm is pristine', async(() => {
    setUpTotalServiceCharge();
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkHasHighlight('[data-slnm-ihg="TotalServiceChargeChanged-SID"]');
    });    
  }));

  it('check if TotalServiceChargeChanged column is not showing pending text if avgNightlyRateForm is pristine', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkNoPendingStateOnFormPristine('[data-slnm-ihg="TotalServiceChargeChanged-SID"] span');
    });    
  }));

  it('check if TotalTaxAmountChanged column is showing pending text if avgNightlyRateForm is dirty', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkPendingStateOnFormDirty('[data-slnm-ihg="TotalTaxAmountChanged-SID"] span');
    });    
  }));

  it('check if TotalTaxAmountChanged column is not showing pending text if avgNightlyRateForm is pristine', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkNoPendingStateOnFormPristine('[data-slnm-ihg="TotalTaxAmountChanged-SID"] span');
    });    
  }));

  it('check if TotalTaxAmountChanged column is not highlighted if avgNightlyRateForm is dirty', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkIfCellHighlighted('[data-slnm-ihg="TotalTaxAmountChanged-SID"]');
    });    
  }));

  it('check if TotalTaxAmountChanged column is not highlighted if avgNightlyRateForm is pristine', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      checkHasHighlight('[data-slnm-ihg="TotalTaxAmountChanged-SID"]');
    });    
  }));

  it('check if changes are cancelled when cancelChanges method is called', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      component.cancelChanges();
      fixture.detectChanges();
      const fieldVal = component.nightlyRates.at(Number(0)).get('indvNightlyRate').value;
      expect(fieldVal).toEqual(component.formattedData[0].rateAfter);
    });    
  }));

  it('check if changes are detected when average nightly rate field value is changed', async(() => {
    setUpEditableAvgNightlyRate();
    fixture.whenStable().then(() => {
      component.nightlyRates.at(Number(0)).get('indvNightlyRate').patchValue("1123.20");
      fixture.detectChanges();
      const hasChanges = component.checkAndUpdateTotals();
      fixture.detectChanges();
      const changedRateInfo = component.rateInfoAfter.rateInfo.dailyRates[0].baseAmount;
      expect(changedRateInfo).toEqual("1123.20");
      expect(hasChanges).toBeTruthy();
    });    
  }));

});
