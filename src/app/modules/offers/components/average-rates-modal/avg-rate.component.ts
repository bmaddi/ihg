import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { DecimalPipe } from '@angular/common';


import { RateInfoModel, RateCategoryModel } from '../../models/offers.models';
import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { outputWhenEditIsFalse } from '@app/modules/check-in/components/check-in-workflow/components/check-in-payment/mock/check-in-payment-mock';

@Component({
  selector: 'app-average-rate',
  templateUrl: './avg-rate.component.html',
  styleUrls: ['./avg-rate.component.scss']
})
export class AverageRateComponent implements OnInit {

  public formattedData: any;
  public extraPersonChargeBefore: number;
  public extraPersonChargeAfter: number;
  public serviceChargeBefore: string;
  public serviceChargeAfter: string;
  public totalTaxBefore: string;
  public totalTaxAfter: string;
  public avgNightlyRateForm: FormGroup;
  public nightlyRates: FormArray;
  public beforeUpdateChanges = [];

  @Input() rateInfoBefore: StayInfoSearchModel;
  @Input() rateInfoAfter: RateCategoryModel;
  @Input() editableMode: boolean;
  @Input() hasRateOverride: boolean;
  @Input() isFromOfferUnavailable: boolean;

  constructor(private formBuilder: FormBuilder, private decimalPipe: DecimalPipe) { }

  ngOnInit() {
    if (this.hasRateOverride) {
      this.createForm();
    }
    this.transformDataToGrid();
  }

  public checkAndUpdateTotals(): boolean {
    let hasChanges = false;
    this.beforeUpdateChanges = [];
    for (let i = 0; i < this.formattedData.length; i++) {
      let valueReq = this.nightlyRates.at(Number(i)).get('indvNightlyRate').value;
      valueReq = this.removeCommas(valueReq);
      let originalRateInfoAfterBaseAmt = this.rateInfoAfter.rateInfo.dailyRates[i].baseAmount;
      originalRateInfoAfterBaseAmt = this.removeCommas(originalRateInfoAfterBaseAmt);
      if (valueReq !== originalRateInfoAfterBaseAmt) {
        this.storePrevData(i, this.formattedData[i].rateAfter, this.rateInfoAfter.rateInfo.dailyRates[i].baseAmount)
        this.formattedData[i].rateAfter = valueReq;
        this.rateInfoAfter.rateInfo.dailyRates[i].baseAmount = valueReq;
        hasChanges = true;
      }
    }
    return hasChanges;
  }

  public storePrevData(index: number, formattedDataBeforeUpdateVal: string, rateInfoAfterBeforeUpdateVal: string): void {
    const tempChanges = {
      index,
      formattedDataBeforeUpdateVal,
      rateInfoAfterBeforeUpdateVal 
    }
    this.beforeUpdateChanges.push(tempChanges);
  }

  public restorePrevData(): void {
    this.beforeUpdateChanges.forEach((elm) => {
      const index = elm['index'];
      this.formattedData[index].rateAfter = elm['formattedDataBeforeUpdateVal'];
      this.rateInfoAfter.rateInfo.dailyRates[index].baseAmount = elm['rateInfoAfterBeforeUpdateVal'];
    });
  }

  public cancelChanges(): void {
    this.formattedData.forEach((elm,index) => {
      const val = this.formattedData[index].rateAfter;
      this.nightlyRates.at(Number(index)).get('indvNightlyRate').patchValue(val, { emitEvent: false });
    })
  }

  public removeCommas(val): string {
    return val.toString().replace(/,/g, '');
  }

  public createForm(): void {
    this.avgNightlyRateForm = this.formBuilder.group({
      nightlyRates: this.formBuilder.array([])
    });
  }

  public addAvgNighlyRate(tempVal): void {
    this.nightlyRates = this.avgNightlyRateForm.get('nightlyRates') as FormArray;
    this.nightlyRates.push(this.createAvgNighlyRate(tempVal));
  }

  public createAvgNighlyRate(tempVal): FormGroup {
    return this.formBuilder.group({
      indvNightlyRate: [this.getTwoDecimalPlacesValue(tempVal)]
    });
  }

  private getTwoDecimalPlacesValue(val: string): string {
    return this.decimalPipe.transform(val, '.2');
  }

  public get nightlyRatesArray(): FormArray {
    return this.avgNightlyRateForm.get('nightlyRates') as FormArray;
  }

  transformToTwoDigitsAfterDecimal(evt: Event, index) {
    if (this.avgNightlyRateForm.dirty) {
      const inputElm = (<HTMLInputElement>evt.target);
      let val = inputElm.value;
      val = val.toString().replace(/,/g, '');
      if(val === '') {
        val = "0";
      }
      val = this.getTwoDecimalPlacesValue(val);
      inputElm.value = val;
      this.nightlyRates.at(Number(index)).get('indvNightlyRate').patchValue(val, { emitEvent: false });
    }
  }

  checkValBlank(val) {
    return (val && val != '' && (Number(val) > 0)) ? val : '-';
  }

  convertBlankValToZero(val) {
    return Number(val);
  }

  transformDataToGrid() {
    this.formattedData = [];
    const { dailyRates } = this.rateInfoBefore.rateInfo;
    const { rateInfo } = this.rateInfoAfter;
    const rateInfoAfter = rateInfo.dailyRates;
    const biggerDateRange = dailyRates.length > rateInfoAfter.length ? dailyRates : rateInfoAfter;
    for (let i = 0; i < biggerDateRange.length; i++) {
      const temp = {
        date: biggerDateRange[i].date,
        rateBefore: dailyRates[i] && dailyRates[i].baseAmount && (dailyRates[i].baseAmount != '') ? this.getTwoDecimalPlacesValue(dailyRates[i].baseAmount) : '-',
        rateAfter: rateInfoAfter[i] && rateInfoAfter[i].baseAmount && (rateInfoAfter[i].baseAmount != '') ? rateInfoAfter[i].baseAmount : '-',
      };
      const rateAfterFormValue = rateInfoAfter[i] && rateInfoAfter[i].baseAmount && (rateInfoAfter[i].baseAmount != '') ? rateInfoAfter[i].baseAmount : 0;
      this.formattedData.push(temp);
      if (this.hasRateOverride) {
        this.addAvgNighlyRate(rateAfterFormValue);
      }
    }
    this.extraPersonChargeBefore = this.convertBlankValToZero(this.rateInfoBefore.rateInfo.totalExtraPersonAmount);
    this.extraPersonChargeAfter = this.convertBlankValToZero(this.rateInfoAfter.rateInfo.totalExtraPersonAmount);
    this.serviceChargeBefore = this.checkValBlank(this.rateInfoBefore.rateInfo.totalServiceChargeAmount);
    this.serviceChargeAfter = this.checkValBlank(this.rateInfoAfter.rateInfo.totalServiceChargeAmount);
    this.totalTaxBefore = this.checkValBlank(this.rateInfoBefore.rateInfo.totalTaxAmount);
    this.totalTaxAfter = this.checkValBlank(this.rateInfoAfter.rateInfo.totalTaxAmount);
  }

}
