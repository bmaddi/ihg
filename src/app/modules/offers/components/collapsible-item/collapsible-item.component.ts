import { Component, OnInit, Input, ContentChild, TemplateRef } from '@angular/core';

import { OffersService } from '@modules/offers/services/offers.service';

import { manageStayGaConstants } from '@modules/offers/constants/offer-types.constant';

@Component({
  selector: 'app-collapsible-item',
  templateUrl: './collapsible-item.component.html',
  styleUrls: ['./collapsible-item.component.scss']
})
export class CollapsibleItemComponent implements OnInit {

  @Input() panelTitle: string;
  @Input() panelInfoLabel: string;
  @Input() panelInfoParam: { [key: string]: string | number };
  @Input() collapsed = false;

  @ContentChild(TemplateRef) template: TemplateRef<any>;

  constructor(private offersService: OffersService) { }

  ngOnInit() {
  }

  public togglePanel(): void {
    this.offersService.trackClickManageStay(this.collapsed ? manageStayGaConstants.ACTION_EXPAND : manageStayGaConstants.ACTION_COLLAPSE, manageStayGaConstants.LBL_SELECTED);
    this.collapsed = !this.collapsed;
  }

}
