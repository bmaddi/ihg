import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { StayInfoSearchModel } from '@app/modules/stay-info/models/stay-information.models';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { TitleCasePipe } from '@angular/common';
import { Subscription } from 'rxjs';

import { GuestListService } from '@modules/guests/services/guest-list.service';
import { OffersService } from '../../services/offers.service';
import { UtilityService } from '@services/utility/utility.service';

import { BADGE_LABELS } from '@modules/check-in/constants/check-in-constants';
import { GuestCheckInModel, GuestWalkInModel, ReservationDataModel } from '../../models/offers.models';


@Component({
  selector: 'app-confirm-changes',
  templateUrl: './confirm-changes.component.html',
  styleUrls: ['./confirm-changes.component.scss']
})

export class ConfirmChangesComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {

  @Input() searchCritera: StayInfoSearchModel;
  @Input() estimatedTotal: string;
  @Input() guestData: ReservationDataModel;

  public badgeLabels = BADGE_LABELS;
  guestName = '';
  groupName = '';
  companyName = '';
  guestNameCharLimit = 7;
  private rootSubscription = new Subscription();

  constructor(
    private titlePipe: TitleCasePipe,
    private guestListService: GuestListService,
    private offersService: OffersService,
    private utility: UtilityService
  ) {
    super();
  }

  ngOnInit() {
    if (this.guestData) {
      this.formatGuestName(this.guestData);
    } else {
      this.getGuestData();
    }
  }

  getGuestData() {
    this.rootSubscription.add(this.offersService.updatedReservationData.subscribe((data: GuestCheckInModel) => {
      if (!this.checkIfAnyApiErrors(data)) {
        this.handleGuestDataResponse(data);
      }
    }, (error) => {
      this.processHttpErrors(error);
    }));
  }

  handleGuestDataResponse(response: GuestCheckInModel): void {
    this.guestData = response.reservationData;   
    this.formatGuestName(this.guestData);
    this.resetAnyErrors();
  }

  formatGuestName(guest: ReservationDataModel) {
    this.guestName = this.utility.concatNameWithPrefix(this.guestData.prefixName, this.guestData.firstName, this.guestData.lastName);
    this.groupName = guest.groupName ? guest.groupName : 'N/A';
    this.companyName = guest.companyName ? guest.companyName : 'N/A';
  }
  
  public setMemberProfile(loyaltyId: string) {
    this.rootSubscription.add(this.guestListService.setMemberProfile(+loyaltyId));
  }

  public navigateToManageStay(): void {
    this.rootSubscription.add(this.offersService.showAllOffers(false));
    this.rootSubscription.add(this.offersService.changeMessage(false));
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
