import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GridModule } from '@progress/kendo-angular-grid';
import { cloneDeep, find, includes } from 'lodash';
import { ClipboardModule } from 'ngx-clipboard';
import * as moment from 'moment';

import { EnvironmentService, GoogleAnalyticsService, SessionStorageService, UserService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { ConflictsGridComponent } from './conflicts-grid.component';
import { googleAnalyticsStub, mockEnvs } from '@app/constants/app-test-constants';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { OffersService } from '@modules/offers/services/offers.service';
import { of } from 'rxjs';
import { mockRoomConflicts } from '@modules/offers/mocks/room-conflicts.mock';
import { DoNotMoveService } from '@modules/do-not-move/services/do-not-move.service';

describe('ConflictsGridComponent', () => {
  let component: ConflictsGridComponent;
  let fixture: ComponentFixture<ConflictsGridComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(SessionStorageService, {
        useValue: {
          setSessionStorage(key: string, value: any): void {
          }
        }
      })
      .overrideProvider(ReportIssueService, {
        useValue: {
          openAutofilledReportIssueModal: (value: object): void => {
          }
        }
      })
      .configureTestingModule({
        imports: [
          GridModule,
          TranslateModule.forChild(),
          HttpClientTestingModule,
          RouterTestingModule,
          ClipboardModule,
          NgbTooltipModule
        ],
        declarations: [ConflictsGridComponent],
        providers: [
          NgbActiveModal,
          TranslateStore,
          UserService,
          GoogleAnalyticsService,
          ReportIssueService,
          EnvironmentService,
          SessionStorageService
        ],
        schemas: [NO_ERRORS_SCHEMA]
      })
      .compileComponents();
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConflictsGridComponent);
    component = fixture.componentInstance;
    component.conflicts = cloneDeep(mockRoomConflicts);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the correct grid column headers', () => {
    const expectedHeader = [
      'LBL_GST_GUEST_NAME',
      'LBL_GST_LOYALTY', 'LBL_GST_ARRIVAL',
      'LBL_GST_DEPARTURE', 'LBL_STATUS', 'COM_LBL_ACTIONS'];

    const slnmIds = ['GuestNameLabel', 'LoyaltyLabel',
      'ArrivalLabel', 'DepartureLabel', 'StatusLabel', 'ActionsLabel'];

    slnmIds.forEach((s, i) => {
      const headerSpan: DebugElement = fixture.debugElement.query(By.css(`[data-slnm-ihg="${s}-SID"]`));
      expect(headerSpan).not.toBeNull();
      expect(headerSpan.nativeElement.textContent).toEqual(expectedHeader[i]);
    });
  });

  it('should show all rooms as conflicts by default', () => {
    const elements = fixture.debugElement.queryAll(By.css('[translate="LBL_GST_CONFLICT"]'));
    expect(elements.length).toEqual(component.conflicts.length);
  });

  it('should show row action as "View" if user does not have Front Desk Management Access Role', () => {
    component.hasDoNotMoveAccess = false;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('room-unassign-btn'));
    const viewActions = fixture.debugElement.queryAll(By.css('[translate="LBL_GST_VIEW"]'));
    const conflictsWithDoNotMove = component.conflicts.filter(item => item.doNotMoveRoom).length;

    expect(elements.length).toEqual(0);
    expect(viewActions.length).toEqual(conflictsWithDoNotMove);
    viewActions.forEach(d => expect(d.nativeElement.textContent).toEqual('LBL_GST_VIEW'));
  });

  it('should show row action as "Unassign" if user has Front Desk Management Access Role', () => {
    component.hasDoNotMoveAccess = true;

    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('.room-unassign-btn'));
    const viewActions = fixture.debugElement.queryAll(By.css('[translate="LBL_GST_VIEW"]'));

    expect(elements.length).toEqual(component.conflicts.length);
    expect(viewActions.length).toEqual(0);
    elements.forEach(d => expect(d.nativeElement.textContent.trim()).toEqual('LBL_GST_UNASSIGN'));
  });

  // tslint:disable-next-line:max-line-length
  it('should toggle #releaseRoom action Unassign #releaseRoomConflict if user has Front Desk Management Access Role and clicks Unassign button', () => {
    component.hasDoNotMoveAccess = true;
    const releaseRoomSpy = spyOn(component, 'releaseRoom').and.callThrough();
    const getRoomReleasedSpy = spyOn(TestBed.get(OffersService), 'getRoomReleased').and.returnValue(of({
      'assignRoomTO': {
        'roomNumber': '11111',
        'roomType': 'string'
      },
    }));
    const updateDoNotMove = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove').and.returnValue(of({}));
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.room-unassign-btn'));
    expect(elements).not.toBeNull();
    expect(elements.length).toEqual(component.conflicts.length);

    elements[1].triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(releaseRoomSpy).toHaveBeenCalled();
    expect(getRoomReleasedSpy).toHaveBeenCalled();
    expect(updateDoNotMove).not.toHaveBeenCalled();
    expect(component.conflicts[1].doNotMoveRoom).toBeFalsy();

    const noAction = fixture.debugElement.queryAll(By.css('[data-slnm-ihg="ActionsLabel1-SID"]'));
    expect(noAction).not.toBeNull();
  });

  it('should toggle #releaseRoom action Unassign if user has access role and change status to clear on successful release', () => {
    component.hasDoNotMoveAccess = true;
    const releaseRoomSpy = spyOn(component, 'releaseRoom').and.callThrough();
    const getRoomReleasedSpy = spyOn(TestBed.get(OffersService), 'getRoomReleased').and.returnValue(of({
      'assignRoomTO': {
        'roomNumber': '11111',
        'roomType': 'string'
      },
    }));
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.room-unassign-btn'));
    expect(elements).not.toBeNull();
    expect(elements.length).toEqual(component.conflicts.length);

    elements[1].triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(releaseRoomSpy).toHaveBeenCalled();
    expect(getRoomReleasedSpy).toHaveBeenCalled();
    expect(component.conflicts[1].doNotMoveRoom).toBeFalsy();

    const clear = fixture.debugElement.queryAll(By.css('[translate="GC_CLEAR"]'));
    expect(clear.length).toEqual(1);
  });

  it('should update actions on clearing the conflicts', () => {
    component.hasDoNotMoveAccess = true;
    const releaseRoomSpy = spyOn(component, 'releaseRoom').and.callThrough();
    const getRoomReleasedSpy = spyOn(TestBed.get(OffersService), 'getRoomReleased').and.returnValue(of({
      'assignRoomTO': {
        'roomNumber': '11111',
        'roomType': 'string'
      },
    }));
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.room-unassign-btn'));
    expect(elements).not.toBeNull();
    expect(elements.length).toEqual(component.conflicts.length);

    elements[1].triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(releaseRoomSpy).toHaveBeenCalled();
    expect(getRoomReleasedSpy).toHaveBeenCalled();
    expect(component.conflicts[1].doNotMoveRoom).toBeFalsy();
    expect(component.conflicts[1].isConflictActive).toBeFalsy();
  });

  // tslint:disable-next-line:max-line-length
  it('should remove doNotMoveIndicator #removeDoNotMoveFromRoomConflict from conflicts with DoNotMove turned on and then release conflict room on successful response', () => {
    component.hasDoNotMoveAccess = true;
    const releaseRoomSpy = spyOn(component, 'releaseRoom').and.callThrough();
    const getRoomReleasedSpy = spyOn(TestBed.get(OffersService), 'getRoomReleased').and.returnValue(of({
      'assignRoomTO': {
        'roomNumber': '11111',
        'roomType': 'string'
      },
    }));
    const updateDoNotMove = spyOn(TestBed.get(DoNotMoveService), 'updateDoNotMove').and.returnValue(of({}));
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.room-unassign-btn'));
    expect(elements).not.toBeNull();
    expect(elements.length).toEqual(component.conflicts.length);

    elements[0].triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(releaseRoomSpy).toHaveBeenCalled();
    expect(getRoomReleasedSpy).toHaveBeenCalled();
    expect(updateDoNotMove).toHaveBeenCalled();
    expect(component.conflicts[0].doNotMoveRoom).toBeFalsy();
    expect(component.conflicts[0].isConflictActive).toBeFalsy();
  });

  // tslint:disable-next-line:max-line-length
  it('should check existing conflicts #checkIfAnyConflictsExists and update parent with proper status upon successful release of a Room Conflict', () => {
    const releaseRoomSpy = spyOn(component, 'releaseRoom').and.callThrough();
    const conflictsResolvedEmitter = spyOn(component.conflictsResolved, 'emit').and.callThrough();
    const conflictsResolved = spyOn<any>(component, 'checkIfAnyConflictsExists').and.callThrough();
    const getRoomReleasedSpy = spyOn(TestBed.get(OffersService), 'getRoomReleased').and.returnValue(of({
      'assignRoomTO': {
        'roomNumber': '11111',
        'roomType': 'string'
      },
    }));
    component.hasDoNotMoveAccess = true;
    component.conflicts[0].doNotMoveRoom = false;
    component.conflicts[0].isConflictActive = false;
    component.conflicts[1].doNotMoveRoom = false;
    component.conflicts[1].isConflictActive = false;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.room-unassign-btn'));
    expect(elements).not.toBeNull();
    expect(elements.length).toEqual(1);
    elements[0].triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(releaseRoomSpy).toHaveBeenCalled();
    expect(conflictsResolved).toHaveBeenCalled();
    expect(getRoomReleasedSpy).toHaveBeenCalled();
    expect(conflictsResolvedEmitter).toHaveBeenCalledWith(true);
  });

  it('should show Do No Move Indicator Icon and have tooltip with Do Not Move Reason when available', () => {
    component.hasDoNotMoveAccess = true;
    const reason = component.conflicts[0].doNotMoveRoomReason;
    fixture.detectChanges();

    const doNotMoveIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="DoNotMoveIndicator1-SID"]'));
    const tooltipAttribute = find(doNotMoveIcon.nativeElement.getAttributeNames(), e => e.indexOf('ngb-tooltip') > -1);

    expect(doNotMoveIcon).not.toBeNull();
    expect(tooltipAttribute).not.toBeNull();
    expect(includes(reason, doNotMoveIcon.nativeElement.getAttribute(tooltipAttribute))).toBeTruthy();
  });

  it('should show the Do No Move Indicator Icon correctly depending on Do Not Move Flag', () => {
    component.hasDoNotMoveAccess = true;
    const expectedDNM = component.conflicts.map(c => c.doNotMoveRoom);
    fixture.detectChanges();

    expectedDNM.forEach((s, i) => {
      expect(component.conflicts[i].doNotMoveRoom).toEqual(s);
      const attrTag = `[data-slnm-ihg="DoNotMoveIndicator${i + 1}-SID"]`;
      s ? expect(fixture.debugElement.query(By.css(attrTag))).not.toBeNull() :
        expect(fixture.debugElement.query(By.css(attrTag))).toBeNull();
    });
  });

  it('should show Arrival and Departure grid columns in the correct DDMMMYYYY date format', () => {
    component.hasDoNotMoveAccess = true;
    fixture.detectChanges();

    const arrival = fixture.debugElement.query(By.css('[data-slnm-ihg="GuestArrival1-SID"]'));
    const departure = fixture.debugElement.query(By.css('[data-slnm-ihg="GuestDeparture1-SID"]'));

    expect(arrival.nativeElement.textContent.trim()).toEqual('10Dec2019');
    expect(departure.nativeElement.textContent.trim()).toEqual('13Dec2019');
  });

  it('should open new window for #viewReservation prepare tab linking for View action', () => {
    const sessionStorage = spyOn(TestBed.get(SessionStorageService), 'setSessionStorage').and.callThrough();
    const windowSpy = spyOn(window, 'open').and.callThrough();
    component.hasDoNotMoveAccess = false;

    fixture.detectChanges();
    const firstViewAction = fixture.debugElement.query(By.css('[data-slnm-ihg="ViewActionLabel1-SID"]'));
    expect(firstViewAction).not.toBeNull();
    firstViewAction.triggerEventHandler('click', null);

    expect(windowSpy).toHaveBeenCalled();
    expect(windowSpy.calls.mostRecent().args[0]).toContain('/fdk/#/guest-list-details');
    expect(sessionStorage).toHaveBeenCalledWith('guestArrivalParams', {
      selectedDate: component.conflicts[0].checkInDate,
      reservationNumber: component.conflicts[0].reservationNumber,
      localDate: moment().format('YYYY-MM-DD'),
      expanded: true
    });
  });
});
