import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { SessionStorageService, UserService } from 'ihg-ng-common-core';

import { RoomConflict } from '@modules/offers/models/room-conflict.interface';
import { AppConstants } from '@app/constants/app-constants';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ReleaseRoomResponse } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { OffersService } from '@modules/offers/services/offers.service';
import { InHouseEditStayService } from '@modules/manage-stay/services/in-house-edit-stay-service/in-house-edit-stay.service';
import { DoNotMoveService } from '@modules/do-not-move/services/do-not-move.service';
import { GuestArrivalParams } from '@modules/guests/models/guest-list.models';

@Component({
  selector: 'app-conflicts-grid',
  templateUrl: './conflicts-grid.component.html',
  styleUrls: ['./conflicts-grid.component.scss']
})
export class ConflictsGridComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @Input() conflicts: RoomConflict[] = [];
  @Input() hasDoNotMoveAccess = false;
  @Input() conflictRoom: number;
  @Output() conflictsResolved = new EventEmitter<boolean>();

  enableRoomMove: boolean;
  appConstants = AppConstants;
  desktopColumnWidths = {
    guestName: 100,
    loyalty: 80,
    arrival: 80,
    departure: 80,
    status: 90,
    doNotMove: 12,
    action: 90,
  };

  constructor(private offersService: OffersService,
              private inHouseEditService: InHouseEditStayService,
              private doNotMoveService: DoNotMoveService,
              private storageService: SessionStorageService,
              private userService: UserService) {
    super();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.storageService.setSessionStorage('guestArrivalParams', null);
  }

  releaseRoom(conflict: RoomConflict) {
    if (conflict.doNotMoveRoom) {
      this.removeDoNotMoveFromRoomConflict(conflict);
    } else {
      this.releaseRoomConflict(conflict);
    }
  }

  viewReservation({reservationNumber, checkInDate}: RoomConflict) {
    const arrivalParams: GuestArrivalParams = {
      selectedDate: checkInDate,
      reservationNumber,
      localDate: this.userService.getHotelCurrentDate(),
      expanded: true
    };
    this.storageService.setSessionStorage('guestArrivalParams', arrivalParams);
    window.open(`${window.location.origin}/fdk/#/guest-list-details`, '_blank');
  }

  private releaseRoomConflict(conflict: RoomConflict) {
    this.offersService.getRoomReleased(this.offersService.getAssignReleasePayload(conflict))
      .subscribe(
        (response: ReleaseRoomResponse) => {
          if (!super.checkIfAnyApiErrors(response)) {
            this.handleReleaseResponse(conflict);
          }
        },
        error => super.processHttpErrors(error));
  }

  private removeDoNotMoveFromRoomConflict(conflict: RoomConflict) {
    this.doNotMoveService.updateDoNotMove({
      doNotMoveRoom: false,
      resNum: conflict.reservationNumber,
      pmsUniqueNumber: conflict.pmsConfirmationNumber
    })
      .subscribe(
        () => this.handleDoNotMoveResponse(conflict),
        error => super.processHttpErrors(error));
  }

  private handleReleaseResponse(conflict: RoomConflict) {
    conflict.isConflictActive = false;
    conflict.doNotMoveRoom = false;
    this.inHouseEditService.setUnassignedGuestEvent(conflict);
    this.checkIfAnyConflictsExists(this.conflicts);
  }

  private handleDoNotMoveResponse(conflict: RoomConflict) {
    conflict.doNotMoveRoom = false;
    this.releaseRoomConflict(conflict);
  }

  private checkIfAnyConflictsExists(conflicts: RoomConflict[]) {
    this.conflictsResolved.emit(!conflicts.some(item => item.isConflictActive));
  }
}
