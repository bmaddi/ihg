import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { cloneDeep } from 'lodash';
import { NgbActiveModal, NgbModalModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { DetachedToastMessageService, GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { ConflictsModalComponent } from './conflicts-modal.component';
import { MOCK_RESERVATION_NUMBER } from '@modules/stay-info/mocks/stay-information-mock';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { mockSearchCriteria, originalReservationMocData } from '@modules/offers/mocks/offers.mock';
import { confirmationPayloadMock, mockListItem, mockRoomConflicts } from '@modules/offers/mocks/room-conflicts.mock';
import { OffersService } from '@modules/offers/services/offers.service';
import { ConflictView } from '@modules/offers/enums/conflict-view.enum';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ErrorToastMessageComponent } from '@app-shared/components/error-toast-message/error-toast-message.component';
import { SAVE_ERROR_TOAST } from '@modules/offers/constants/error-toast.constant';

describe('ConflictsModalComponent', () => {
  let component: ConflictsModalComponent;
  let fixture: ComponentFixture<ConflictsModalComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(DetachedToastMessageService, {
        useValue: {}
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: {
          openAutofilledReportIssueModal: (value: object): void => {

          }
        }
      })
      .configureTestingModule({
        imports: [
          NgbTooltipModule,
          TranslateModule.forChild(),
          NgbModalModule,
          HttpClientTestingModule,
          RouterTestingModule],
        declarations: [ConflictsModalComponent, ErrorToastMessageComponent],
        providers: [
          NgbActiveModal,
          TranslateStore,
          UserService,
          GoogleAnalyticsService,
          ReportIssueService,
          ConfirmationModalService,
          DetachedToastMessageService],
        schemas: [NO_ERRORS_SCHEMA]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConflictsModalComponent);
    component = fixture.componentInstance;
    component.reservationNumber = MOCK_RESERVATION_NUMBER;
    component.searchCriteria = cloneDeep(mockSearchCriteria);
    component.originalReservationData = cloneDeep(originalReservationMocData);
    component.conflicts = cloneDeep(mockRoomConflicts);
    component.confirmationPayload = cloneDeep(confirmationPayloadMock);
    component.listItem = cloneDeep(mockListItem);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the modal on clicking x', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.close'));
    expect(element).not.toBeNull();
    element.triggerEventHandler('click', null);
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('should close on clicking Cancel Button', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="Cancel-SID"]'));
    expect(element).not.toBeNull();
    element.triggerEventHandler('click', null);
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('should have Rooms not available message title header', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.conflicts = cloneDeep(mockRoomConflicts);

    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('[translate="LBL_ROOM__NOT_AVLBL"]')).nativeElement;

    expect(element).not.toBeNull();
    expect(element.textContent.indexOf('LBL_ROOM__NOT_AVLBL') > -1).toBeTruthy();
  });

  it('should have Rooms not available information message in body', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.conflicts = cloneDeep(mockRoomConflicts);

    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('[translate="LBL_ROOM__NOT_AVLBL_CONFLICT_MSG"]')).nativeElement;
    expect(element.textContent.indexOf('LBL_ROOM__NOT_AVLBL_CONFLICT_MSG') > -1).toBeTruthy();
  });

  it('should have conflicts grid in modal body', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.conflicts = cloneDeep(mockRoomConflicts);

    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('app-conflicts-grid'));

    expect(element).not.toBeNull();
  });

  it('should display the refresh button until conflicts are resolved', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.conflicts = cloneDeep(mockRoomConflicts);

    fixture.detectChanges();

    const refreshButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'));

    expect(refreshButton).not.toBeNull();
    expect(refreshButton.nativeElement.textContent).toEqual('COM_BTN_RFRSH');
  });

  // tslint:disable-next-line:max-line-length
  it('should display the refresh button and call #getRoomConflicts to validate conflicts are resolved and change button label to continue', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.conflicts = cloneDeep(mockRoomConflicts);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(null));
    const checkForConflictsSpy = spyOn<any>(component, 'checkForConflicts').and.callThrough();

    fixture.detectChanges();
    const refreshButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'));
    expect(refreshButton).not.toBeNull();
    expect(refreshButton.nativeElement.textContent).toEqual('COM_BTN_RFRSH');
    refreshButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(checkForConflictsSpy).toHaveBeenCalled();
    expect(component.allConflictsResolved).toBeTruthy();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]')).nativeElement.textContent).toEqual('COM_BTN_CONT');
  });


  // tslint:disable-next-line:max-line-length
  it('should display the refresh button and call #getRoomConflicts to validate conflicts are resolved and keep button as Refresh if any conflicts still exist', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.conflicts = cloneDeep(mockRoomConflicts);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of([{
      'firstName': 'AYUSH',
      'lastName': 'TEST',
      'reservationNumber': '22401014',
      'badges': [],
      'checkOutDate': null,
      'roomNumber': '100',
      'roomType': 'KSTG',
      'checkInDate': '2019-12-10',
      'reservationStatus': '',
      'pmsConfirmationNumber': '483095',
      'doNotMoveRoom': true,
      'guestName': 'TEST, AYUSH',
      'isConflictActive': true
    }]));
    const checkForConflictsSpy = spyOn<any>(component, 'checkForConflicts').and.callThrough();

    fixture.detectChanges();
    const refreshButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'));
    expect(refreshButton).not.toBeNull();
    expect(refreshButton.nativeElement.textContent).toEqual('COM_BTN_RFRSH');
    refreshButton.triggerEventHandler('click', null);

    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(checkForConflictsSpy).toHaveBeenCalled();
    expect(component.allConflictsResolved).toBeFalsy();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]')).nativeElement.textContent).toEqual('COM_BTN_RFRSH');
  });

  // tslint:disable-next-line:max-line-length
  it('should display the continue button and call #getRoomConflicts to validate conflicts are resolved and keep button as Refresh if any conflicts still exist', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of([{
      'firstName': 'AYUSH',
      'lastName': 'TEST',
      'reservationNumber': '22401014',
      'badges': [],
      'checkOutDate': null,
      'roomNumber': '100',
      'roomType': 'KSTG',
      'checkInDate': '2019-12-10',
      'reservationStatus': '',
      'pmsConfirmationNumber': '483095',
      'doNotMoveRoom': true,
      'guestName': 'TEST, AYUSH',
      'isConflictActive': true
    }]));
    const checkForConflictsSpy = spyOn<any>(component, 'checkForConflicts').and.callThrough();
    fixture.detectChanges();
    const refreshButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'));

    expect(refreshButton).not.toBeNull();
    expect(refreshButton.nativeElement.textContent).toEqual('COM_BTN_RFRSH');
    refreshButton.triggerEventHandler('click', null);

    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(checkForConflictsSpy).toHaveBeenCalled();
    expect(component.allConflictsResolved).toBeFalsy();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]')).nativeElement.textContent).toEqual('COM_BTN_RFRSH');
  });

  it('should validate reservation update API should be called when confirm button is clicked', () => {
    const mockSelectedRoom = {
      roomStatus: 'Clean',
      frontOfficeStatus: 'Vacant',
      houseKeepingStatus: 'Clean',
      roomNumber: '420',
      roomType: 'KEXN',
      features: null,
      statusFilter: 'Vacant / Clean',
      filterOrder: 2
    };

    const getAssignReleasePayloadSpy = spyOn(TestBed.get(RoomsService), 'getAssignReleasePayload').and.callThrough();
    const assignRoomSpy = spyOn(TestBed.get(RoomsService), 'assignRoom').and.callThrough();

    component['updateRoomNumber'](mockSelectedRoom);
    fixture.detectChanges();

    expect(getAssignReleasePayloadSpy).toHaveBeenCalled();
    expect(assignRoomSpy).toHaveBeenCalled();
  });

  it('clicking continue button should open cutkeys modal #triggerCutKeyModal with room num after successful reservation updates', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.allConflictsResolved = true;
    const triggerModal = spyOn(TestBed.get(OffersService), 'triggerCutKeyModal').and.callThrough();
    const confirmChangeReservation = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(of({
      reservationData: {},
      status: 'SUCCESS',
      errors: []
    }));
    const assignRoom = spyOn(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({
      roomStatusList: [],
      errors: null
    }));

    fixture.detectChanges();
    const continueButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'));
    expect(continueButton).not.toBeNull();
    continueButton.triggerEventHandler('click', null);

    expect(component.allConflictsResolved).toBeTruthy();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]')).nativeElement.textContent).toEqual('COM_BTN_CONT');
    expect(confirmChangeReservation).toHaveBeenCalled();
    expect(assignRoom).not.toHaveBeenCalled();
  });

  it('should have manual room assignment grid in modal body if Manual Room Assignment defaulted', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.manualAssign;

    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('app-manual-assign-grid'));
    const toggleButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ToggleView-SID"]'));
    const continueButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ContinueManualRoomAssignment-SID"]'));

    expect(element).not.toBeNull();
    expect(toggleButton).toBeNull();
    expect(continueButton).not.toBeNull();
  });

  it('should enable continue button on the manual room assignment view upon user room selection', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.manualAssign;

    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('app-manual-assign-grid'));
    const continueButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ContinueManualRoomAssignment-SID"]'));
    expect(element).not.toBeNull();
    expect(continueButton).not.toBeNull();
    expect(continueButton.nativeElement.disabled).toBeTruthy();
    component.onRoomSelection({roomNumber: '202'});
    fixture.detectChanges();

    expect(continueButton.nativeElement.disabled).toBeFalsy();
  });

  it('should update reservation and room assignment on continue button action on manual room assignment selection', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.manualAssign;
    component.selectedRoom =  {
      roomStatus: 'Clean',
      frontOfficeStatus: 'Vacant',
      houseKeepingStatus: 'Clean',
      roomNumber: '420',
      roomType: 'KEXN',
      features: null,
      statusFilter: 'Vacant / Clean',
      filterOrder: 2
    };
    const manualAssignRoomContinueSpy = spyOn(component, 'manualAssignRoomContinue').and.callThrough();
    const updateReservationDetailsSpy = spyOn<any>(component, 'updateReservationDetails').and.callThrough();
    const updateRoomNumberSpy = spyOn<any>(component, 'updateRoomNumber').and.callThrough();
    const confirmChangeReservationSpy = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(of(
      { status: 'SUCCESS' }));

    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('app-manual-assign-grid'));
    const continueButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ContinueManualRoomAssignment-SID"]'));
    expect(element).not.toBeNull();
    expect(continueButton).not.toBeNull();
    expect(continueButton.nativeElement.disabled).toBeFalsy();
    continueButton.triggerEventHandler('click', null);

    expect(manualAssignRoomContinueSpy).toHaveBeenCalled();
    expect(updateReservationDetailsSpy).toHaveBeenCalled();
    expect(confirmChangeReservationSpy).toHaveBeenCalled();
    expect(updateRoomNumberSpy).toHaveBeenCalled();
  });

  it('should allow toggle between both conflict views #toggleView', () => {
    component.viewChangeEnabled = true;
    component.view = ConflictView.roomConflicts;

    fixture.detectChanges();
    const toggleButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ToggleView-SID"]'));
    expect(fixture.debugElement.query(By.css('app-manual-assign-grid'))).toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ContinueManualRoomAssignment-SID"]'))).toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'))).not.toBeNull();
    expect(toggleButton).not.toBeNull();
    toggleButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('app-manual-assign-grid'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'))).toBeNull();
    expect(fixture.debugElement.query(By.css('app-manual-assign-grid'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ContinueManualRoomAssignment-SID"]'))).not.toBeNull();
  });

  it('on choosing Assign will select that room but not update the reservation', () => {
    component.viewChangeEnabled = false;
    component.view = ConflictView.roomConflicts;
    component.allConflictsResolved = true;
    const triggerModal = spyOn(TestBed.get(OffersService), 'triggerCutKeyModal').and.callThrough();
    const confirmChangeReservation = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(of({
      reservationData: {},
      status: 'SUCCESS',
      errors: []
    }));
    const assignRoom = spyOn(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({
      roomStatusList: [],
      errors: null
    }));

    fixture.detectChanges();
    const continueButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]'));
    expect(continueButton).not.toBeNull();
    continueButton.triggerEventHandler('click', null);

    expect(component.allConflictsResolved).toBeTruthy();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="ConflictsConfirm-SID"]')).nativeElement.textContent).toEqual('COM_BTN_CONT');
    expect(confirmChangeReservation).toHaveBeenCalled();
    expect(assignRoom).not.toHaveBeenCalled();
  });

  it('validate error content', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.errorContent).toBe(SAVE_ERROR_TOAST);
  });

  it('validate report issue is prepopulated', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.reportIssueAutoFill.genericError.subject).toBe('Service Error while updating reservation 47593592');
  });
  
});
