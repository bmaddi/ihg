import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { find, get } from 'lodash';
import { Subject, Subscription } from 'rxjs';

import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

import { RoomConflict, RoomConflictsPayload } from '@modules/offers/models/room-conflict.interface';
import { OffersService } from '@modules/offers/services/offers.service';
import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { Room, RoomsListResponse } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { ConflictView } from '@modules/offers/enums/conflict-view.enum';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { GuestCheckInModel } from '@modules/offers/models/offers.models';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { MAN_ASSIGN_SAVE_ERROR_TOAST, SAVE_ERROR_TOAST } from '@modules/offers/constants/error-toast.constant';
import { MANAGE_STAY_ASSIGN_ROOM_AUTOFILL, MANAGE_STAY_OFFERS_SAVE_ERROR } from '@app/constants/error-autofill-constants';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

@Component({
  selector: 'app-conflicts-modal',
  templateUrl: './conflicts-modal.component.html',
  styleUrls: ['./conflicts-modal.component.scss']
})
export class ConflictsModalComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @Input() conflicts: RoomConflict[];
  @Input() reservationNumber: string;
  @Input() searchCriteria: StayInfoSearchModel;
  @Input() viewChangeEnabled: boolean;
  @Input() view: ConflictView;
  @Input() originalReservationData: StayInfoSearchModel;
  @Input() confirmationPayload: { [key: string]: string | number };
  @Input() listItem: { [key: string]: any };
  @Input() stateName = AppStateNames.manageStay;
  @Output() conflictAction: EventEmitter<RoomConflict[]> = new EventEmitter();

  hasDoNotMoveAccess = false;
  allConflictsResolved = false;
  selectedRoom: Room;
  manualAssignRoomRefreshTrigger = new Subject<null>();
  viewTypes = ConflictView;
  reportIssueAutoFill: ReportIssueAutoFillObject;
  errorContent = SAVE_ERROR_TOAST;
  manualAssignErrorEvent = new Subject<EmitErrorModel>();
  manualAssignErrorContent = MAN_ASSIGN_SAVE_ERROR_TOAST;
  manualAssignErrorAutoFill: ReportIssueAutoFillObject;

  private rootSubscription = new Subscription();

  constructor(private activeModal: NgbActiveModal,
              public offersService: OffersService,
              private roomsService: RoomsService) {
    super();
  }

  ngOnInit() {
    this.hasDoNotMoveAccess = this.offersService.getFrontDeskAccess();
    this.reportIssueAutoFill =  MANAGE_STAY_OFFERS_SAVE_ERROR(this.reservationNumber);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

  dismissModal() {
    this.activeModal.dismiss();
  }

  toggleView() {
    this.view = this.view === this.viewTypes.roomConflicts ? this.viewTypes.manualAssign : this.viewTypes.roomConflicts;
    this.resetAnyErrors();
    this.resetSpecificError(this.emitError);
    this.resetSpecificError(this.manualAssignErrorEvent);
  }

  onRoomSelection(room: Room) {
    this.selectedRoom = room;
  }

  continue() {
    if (this.allConflictsResolved) {
      this.updateReservationDetails();
    } else {
      this.refreshConflictsList();
    }
  }

  manualAssignRoomContinue() {
    if (this.selectedRoom) {
      this.manualAssignErrorAutoFill = MANAGE_STAY_ASSIGN_ROOM_AUTOFILL(this.reservationNumber, this.selectedRoom.roomNumber);
      this.updateReservationDetails(this.selectedRoom, this.manualAssignErrorEvent);
    }
  }

  tryAgain() {
    this.view === this.viewTypes.roomConflicts ?
      this.continue() :
      this.manualAssignRoomContinue();
  }

  handleConflictsResolvedUpdate(allResolved: boolean) {
    this.allConflictsResolved = allResolved;
  }

  private refreshConflictsList() {
    this.rootSubscription
      .add(
        this.offersService.getRoomConflicts(<RoomConflictsPayload>{
          checkInDate: this.confirmationPayload.checkInDate,
          checkOutDate: this.confirmationPayload.checkOutDate,
          reservationNumber: this.reservationNumber,
          roomNumber: this.originalReservationData.roomNumber,
          roomType: this.confirmationPayload.roomTypeCode
        }).subscribe(
          conflicts => {
            this.checkForConflicts(conflicts);
          },
          error => this.processHttpErrors(error)
        ));
  }

  private checkForConflicts(conflicts: RoomConflict[]) {
    if (conflicts && conflicts.length) {
      this.allConflictsResolved = !conflicts.some(item => item.isConflictActive);
      this.conflicts.forEach(c => {
        const matching = find(conflicts, conflictRes => conflictRes.reservationNumber === c.reservationNumber);
        if (matching) {
          c.isConflictActive = matching.isConflictActive;
          c.doNotMoveRoom = matching.doNotMoveRoom;
        }
      });
    } else {
      this.allConflictsResolved = true;
    }
  }

  private openCutKey() {
    this.offersService.triggerCutKeyModal({
      conflicts: this.conflicts,
      reservationNumber: this.reservationNumber,
      pmsNumber: get(this.offersService.getInHouseReservationData(), 'pmsReservationNumber', null),
    });
    this.dismissModal();
  }

  private updateReservationDetails(room?: Room, errorSubject?: Subject<EmitErrorModel>) {
    this.rootSubscription.add(
      this.offersService.confirmChangeReservation({
          ...this.confirmationPayload,
          ...{roomTypeCode: get(room, 'roomType', this.confirmationPayload.roomTypeCode)}
        })
        .subscribe((response: GuestCheckInModel) => {
          if (!super.checkIfAnyApiErrors(response, null, errorSubject)) {
            if (response && response.status === 'SUCCESS') {
              if (room) {
                this.updateRoomNumber(room);
              } else {
                this.handleAssignSuccessResponse();
              }
            }
          } else {
          }
        }, error => {
          super.processHttpErrors(error, errorSubject);
        })
    );
  }

  private updateRoomNumber(selectedRoom: Room) {
    this.rootSubscription.add(
      this.roomsService.assignRoom(this.roomsService.getAssignReleasePayload(this.listItem as ArrivalsListModel, selectedRoom))
        .subscribe((data: RoomsListResponse) => {
            if (!this.checkIfAnyApiErrors(data, null, this.manualAssignErrorEvent)) {
              this.handleAssignSuccessResponse();
            } else {
            }
          },
          error => {
            this.processHttpErrors(error, this.manualAssignErrorEvent);
          })
    );
  }

  private handleAssignSuccessResponse() {
    this.openCutKey();
  }
}
