import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateStore } from '@ngx-translate/core';
import { of } from 'rxjs';

import { ReportIssueService } from 'ihg-ng-common-pages';
import { EnvironmentService } from 'ihg-ng-common-core';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { OfferByProductsGridComponent } from './offer-by-products-grid.component';
import { mockEnvs } from '@app/constants/app-test-constants';
import { mockSearchCriteria } from '@modules/offers/mocks/offers.mock';
import { offersByProductMockResponse } from '@modules/offers/mocks/offers-by-products.mock';
import { OffersByProductResponseModel } from '../../models/offers.models';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';

describe('OfferByProductsGridComponent', () => {
  let component: OfferByProductsGridComponent;
  let fixture: ComponentFixture<OfferByProductsGridComponent>;

  beforeEach(async(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
    TestBed
      .configureTestingModule({
        declarations: [OfferByProductsGridComponent],
        imports: [CommonTestModule, HttpClientTestingModule],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [NgbActiveModal, TranslateStore, EnvironmentService,
          {provide: ReportIssueService, useValue: {}},
          {provide: GoogleAnalyticsService, useValue: googleAnalyticsStub}]
      })
      .compileComponents();
    TestBed.get(EnvironmentService).setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferByProductsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch offers by product on search criteria updates', () => {
    const getOffersSpy = spyOn<any>(component, 'getOffersByProduct').and.callThrough();
    const getOffersServiceSpy = spyOn(TestBed.get(OffersService), 'getOffersByProduct').and.returnValue(of(offersByProductMockResponse));

    component.searchCritera = {...mockSearchCriteria};
    component.ngOnChanges({
      searchCritera: new SimpleChange(null, mockSearchCriteria, false)
    });
    fixture.detectChanges();

    expect(getOffersSpy).toHaveBeenCalled();
    expect(getOffersServiceSpy).toHaveBeenCalled();
  });

  it('should validate that the \'Offer Not Available\' modal gets displayed when there are no items in the offers', () => {

    const mockRes: OffersByProductResponseModel = {
      'pageNumber': 1,
      'pageSize': 4,
      'startItem': 1,
      'endItem': 4,
      'totalItems': 0,
      'totalPages': 4,
      'products': [],
      suggestedOffers: null,
      'rateOverrideEligible':true
    };

    const getOffersSpy = spyOn(TestBed.get(OffersService), 'getOffersByProduct').and.returnValue(of(mockRes));

    const handleResponseSpy = spyOn<any>(component, 'handleResponse').and.callThrough();

    const handleNoOffers = spyOn<any>(component, 'handleNoOffers').and.callThrough();

    const noOffersModalSpy = spyOn(TestBed.get(ModalHandlerService), 'openNoOffersModal').and.returnValue(null);


    component.searchCritera = {...mockSearchCriteria};
    component.ngOnChanges({
      searchCritera: new SimpleChange(null, mockSearchCriteria, false)
    });

    fixture.detectChanges();

    expect(getOffersSpy).toHaveBeenCalled();
    expect(handleResponseSpy).toHaveBeenCalled();
    expect(handleNoOffers).toHaveBeenCalled();
    expect(noOffersModalSpy).toHaveBeenCalled();

  });
});
