import { Component, Input, OnChanges, OnInit, OnDestroy, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Subject, Subscription } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { StayInfoSearchModel, StayInformationModel } from '@modules/stay-info/models/stay-information.models';
import { OffersByProductResponseModel, ProductModel, SuggestedOffersModel } from '../../models/offers.models';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { manageStayGaConstants } from '../../constants/offer-types.constant';

@Component({
  selector: 'app-offer-by-products-grid',
  templateUrl: './offer-by-products-grid.component.html',
  styleUrls: ['./../../common/offers-grid.scss']
})
export class OfferByProductsGridComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {

  public gridData: GridDataResult;
  public gridState: State = {
    take: 4,
    skip: 0
  };
  public panelCollapsed = false;
  private shopOffersError = new Subject<EmitErrorModel>();
  private rootSubscription = new Subscription();
  private isShopOffer = false;
  private noOffersModal: NgbModalRef;

  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: StayInformationModel;
  @Input() reservationNumber: string;

  @Output() estimatedTotal = new EventEmitter<string>();
  @Output() noOffers = new EventEmitter<boolean>();
  @Output() suggestedOffers = new EventEmitter<SuggestedOffersModel>();

  constructor(private offersService: OffersService, private modalService: ModalHandlerService,
              private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.subscribeToShopOffers();
    this.subscribeToOffersErrorRetry();
    this.subscribeToRateOverrideCheck();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.searchCritera && changes.searchCritera.currentValue) {
      this.resetGridState();
      this.clearGridData();
      this.updateShopOffers(false);
      this.getOffersByProduct();
    }
  }

  private resetGridState(): void {
    this.gridState.skip = 0;
    this.gridState.take = 4;
  }

  private clearGridData(): void {
    this.gridData = {
      data: [],
      total: 0
    };
  }

  private subscribeToShopOffers(): void {
    this.rootSubscription.add(this.modalService.shopOffers$.subscribe(() => {
      this.resetGridState();
      this.clearGridData();
      this.updateShopOffers(true);
      this.getOffersByProduct();
    }));
  }

  private subscribeToOffersErrorRetry(): void {
    this.rootSubscription.add(this.offersService.searchOffersErrorRetry$.subscribe(() => {
      this.resetGridState();
      this.clearGridData();
      this.updateShopOffers(false);
      this.getOffersByProduct();
    }));
  }

  subscribeToRateOverrideCheck(): void {
    this.rootSubscription.add(this.modalService.getRateOverride$().subscribe(() => {
      this.modalService.openWarningModal({
        isFromOfferUnavailable: true,
        searchCritera: this.searchCritera,
        originalReservationData: this.originalReservationData,
        reservationNumber: this.reservationNumber,
        rateInfoAfter: this.offersService.constructRateInfo(this.originalReservationData, this.searchCritera)
      }, manageStayGaConstants.RATES , true);
    }));
  }


  private updateShopOffers(isShopOffer: boolean): void {
    this.isShopOffer = isShopOffer;
  }

  private getOffersByProduct(isPagination?: boolean): void {
    this.updateSearchCriteria();
    this.offersService.getOffersByProduct(this.searchCritera, this.gridState, this.reservationNumber, isPagination)
      .subscribe((response: OffersByProductResponseModel) => {
        if (this.isShopOffer) {
          if (!super.checkIfAnyApiErrors(response, null, this.shopOffersError)) {
            this.handleResponse(response, isPagination);
          }
        } else {
          this.handleResponse(response, isPagination);
        }
      }, error => {
        if (this.isShopOffer) {
          this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
            manageStayGaConstants.SHOP_OFFER, manageStayGaConstants.OFFER_ERROR);
          super.processHttpErrors(error, this.shopOffersError);
        } else {
          this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
            manageStayGaConstants.OFFER_SERACH, manageStayGaConstants.OFFER_ERROR);
        }
      });
  }

  private updateSearchCriteria(): void {
    this.searchCritera.searchType = this.isShopOffer ? 'ALL' : '';
  }

  private handleResponse(response: OffersByProductResponseModel, isPagination?: boolean): void {
    if (!response.errors) {
      // response.totalItems = 0;
      if (response.totalItems > 1) {
        this.handleMultipleOffers(response, isPagination);
      } else {
        if (response.totalItems === 0) {
          this.handleNoOffers(response.rateOverrideEligible);
        } else if (response.totalItems === 1) {
          const countRateCategories = response.products[0].rateCategories.length;
          if (countRateCategories > 1) {
            this.handleMultipleOffers(response, isPagination);
          } else {
            this.handleSingleOffer(response);
          }
        }
      }
    } else {
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
        manageStayGaConstants.SHOP_OFFER, manageStayGaConstants.OFFER_ERROR);
    }
  }

  private handleMultipleOffers(response: OffersByProductResponseModel, isPagination?: boolean): void {
    this.closeNoOffersModal();
    this.setGridView(response);
    if (!isPagination) {
      this.offersService.showAllOffers(true);
    }
    this.emitBroadSearchNoOffers(false);
    this.suggestedOffers.emit(response.suggestedOffers);
  }

  private handleSingleOffer(response: OffersByProductResponseModel): void {
    this.closeNoOffersModal();
    this.openWarningModal(response.products[0]);
    this.offersService.showAllOffers(false);
    this.emitBroadSearchNoOffers(false);
  }

  private handleNoOffers(eligileForRateOverride: boolean): void {
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
      manageStayGaConstants.OFFER_SERACH, manageStayGaConstants.OFFER_NOT_AVAILABLE);
    if (this.isShopOffer) {
      this.closeNoOffersModal();
      this.offersService.showAllOffers(true);
      this.emitBroadSearchNoOffers(true);
    } else {
      // ToDo Check Flag
      this.noOffersModal = this.modalService.openNoOffersModal(this.shopOffersError, eligileForRateOverride);
      this.offersService.showAllOffers(false);
      this.emitBroadSearchNoOffers(false);
    }
  }

  private closeNoOffersModal(): void {
    if (this.noOffersModal) {
      this.noOffersModal.close();
    }
  }

  private emitBroadSearchNoOffers(noOffers: boolean): void {
    this.noOffers.emit(noOffers);
  }

  private setGridView(response: OffersByProductResponseModel): void {
    this.clearGridData();
    this.gridData = {
      data: response.products,
      total: response.totalItems
    };
  }

  public handlePageChange(event: PageChangeEvent): void {
    this.gridState.skip = event.skip;
    this.gridState.take = event.take;
    this.clearGridData();
    this.getOffersByProduct(true);
  }

  public getPanelTitle(product: ProductModel): string {
    return `${product.name} (${product.code})`;
  }

  public setEstimatedTotal(estTotal: string): void {
    this.estimatedTotal.emit(estTotal);
  }

  private openWarningModal(product: ProductModel): void {
    this.modalService.openWarningModal({
      searchCritera: this.searchCritera,
      originalReservationData: this.originalReservationData,
      reservationNumber: this.reservationNumber,
      rateInfoAfter: product.rateCategories[0]
    });
    this.setEstimatedTotal(product.rateCategories[0].rateInfo.totalAmountAfterTax);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
