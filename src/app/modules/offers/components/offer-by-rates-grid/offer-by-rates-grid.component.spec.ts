import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateStore } from '@ngx-translate/core';

import { ReportIssueService } from 'ihg-ng-common-pages';
import { EnvironmentService, GoogleAnalyticsService } from 'ihg-ng-common-core';

import { OfferByRatesGridComponent } from './offer-by-rates-grid.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { googleAnalyticsStub, mockEnvs } from '@app/constants/app-test-constants';
import { OffersByRateResponseModel } from '../../models/offers.models';
import { OffersService } from '../../services/offers.service';
import { mockSearchCriteria } from '../../mocks/offers.mock';
import { of } from 'rxjs';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';

describe('OfferByRatesGridComponent', () => {
  let component: OfferByRatesGridComponent;
  let fixture: ComponentFixture<OfferByRatesGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OfferByRatesGridComponent],
      imports: [CommonTestModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [NgbActiveModal, TranslateStore, EnvironmentService,
        {provide: ReportIssueService, useValue: {}},
        {provide: GoogleAnalyticsService, useValue: googleAnalyticsStub}]
    })
      .compileComponents();
    TestBed.get(EnvironmentService).setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferByRatesGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate that the \'Offer Not Available\' modal gets displayed when there are no items in the offers', () => {

    const mockRes: OffersByRateResponseModel = {
      'pageNumber': 1,
      'pageSize': 4,
      'startItem': 1,
      'endItem': 4,
      'totalItems': 0,
      'totalPages': 4,
      'offersByRateCategory': [],
      'rateOverrideEligible':true
    };

    const getOffersSpy = spyOn(TestBed.get(OffersService), 'getOffersByRate').and.returnValue(of(mockRes));

    const handleResponseSpy = spyOn<any>(component, 'handleResponse').and.callThrough();

    const handleNoOffers = spyOn<any>(component, 'handleNoOffers').and.callThrough();

    const noOffersModalSpy = spyOn(TestBed.get(ModalHandlerService), 'openNoOffersModal').and.returnValue(null);


    component.searchCritera = {...mockSearchCriteria};
    component.ngOnChanges({
      searchCritera: new SimpleChange(null, mockSearchCriteria, false)
    });

    fixture.detectChanges();

    expect(getOffersSpy).toHaveBeenCalled();
    expect(handleResponseSpy).toHaveBeenCalled();
    expect(handleNoOffers).toHaveBeenCalled();
    expect(noOffersModalSpy).toHaveBeenCalled();
  });
});
