import { Component, Input, OnChanges, OnInit, OnDestroy, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Subject, Subscription } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { StayInfoSearchModel, StayInformationModel } from '@modules/stay-info/models/stay-information.models';
import { OffersByRateResponseModel, OfferByRateCategoryModel, ProductModel, RateCategoryModel } from '../../models/offers.models';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { manageStayGaConstants } from '../../constants/offer-types.constant';
import { GuestInfoService } from '@app/modules/guest-info/services/guest-information.service';


@Component({
  selector: 'app-offer-by-rates-grid',
  templateUrl: './offer-by-rates-grid.component.html',
  styleUrls: ['./../../common/offers-grid.scss']
})
export class OfferByRatesGridComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {

  public gridData: GridDataResult;
  public gridState: State = {
    take: 4,
    skip: 0
  };
  public shopOffersError = new Subject<EmitErrorModel>();
  private rootSubscription = new Subscription();
  private isShopOffer = false;
  private noOffersModal: NgbModalRef;

  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: StayInformationModel;
  @Input() reservationNumber: string;

  @Output() estimatedTotal = new EventEmitter<string>();
  @Output() noOffers = new EventEmitter<boolean>();

  constructor(private offersService: OffersService, private modalService: ModalHandlerService,
              private gaService: GoogleAnalyticsService) {
    super();
  }

  ngOnInit() {
    this.subscribeToShopOffers();
    this.subscribeToOffersErrorRetry();
    this.subscribeToRateOverrideCheck();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.searchCritera && changes.searchCritera.currentValue) {
      this.resetGridState();
      this.clearGridData();
      this.updateShopOffers(false);
      this.getOffersByRate();
    }
  }

  private resetGridState(): void {
    this.gridState.skip = 0;
    this.gridState.take = 4;
  }

  private clearGridData(): void {
    this.gridData = {
      data: [],
      total: 0
    };
  }

  private subscribeToShopOffers(): void {
    this.rootSubscription.add(this.modalService.shopOffers$.subscribe(() => {
      this.resetGridState();
      this.clearGridData();
      this.updateShopOffers(true);
      this.getOffersByRate();
    }));
  }

  private subscribeToOffersErrorRetry(): void {
    this.rootSubscription.add(this.offersService.searchOffersErrorRetry$.subscribe(() => {
      this.resetGridState();
      this.clearGridData();
      this.updateShopOffers(false);
      this.getOffersByRate();
    }));
  }

 private subscribeToRateOverrideCheck(): void {
    this.rootSubscription.add(this.modalService.getRateOverride$().subscribe(() => {
      this.closeNoOffersModal();
      this.modalService.openWarningModal({
        isFromOfferUnavailable: true,
        searchCritera: this.searchCritera,
        originalReservationData: this.originalReservationData,
        reservationNumber: this.reservationNumber,
        rateInfoAfter: this.offersService.constructRateInfo(this.originalReservationData, this.searchCritera)
      }, manageStayGaConstants.RATES , true);
    }));
  }

  private updateShopOffers(isShopOffer: boolean): void {
    this.isShopOffer = isShopOffer;
  }

  private getOffersByRate(isPagination?: boolean): void {
    this.updateSearchCriteria();
    this.offersService.getOffersByRate(this.searchCritera, this.gridState, this.reservationNumber, isPagination)
      .subscribe((response: OffersByRateResponseModel) => {
        if (this.isShopOffer) {
          if (!super.checkIfAnyApiErrors(response, null, this.shopOffersError)) {
            this.handleResponse(response, isPagination);
          }
        } else {
          this.handleResponse(response, isPagination);
        }
      }, error => {
        if (this.isShopOffer) {
          this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
            manageStayGaConstants.SHOP_OFFER, manageStayGaConstants.OFFER_ERROR);
          super.processHttpErrors(error, this.shopOffersError);
        } else {
          this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
            manageStayGaConstants.OFFER_SERACH, manageStayGaConstants.OFFER_ERROR);
        }
      });
  }

  private updateSearchCriteria(): void {
    this.searchCritera.searchType = this.isShopOffer ? 'ALL' : '';
  }

  private handleResponse(response: OffersByRateResponseModel, isPagination?: boolean): void {
    if (!response.errors) {
      if (response.totalItems > 1) {
        this.handleMultipleOffers(response, isPagination);
      } else {
        if (response.totalItems === 0) {
          this.handleNoOffers(response.rateOverrideEligible);
        } else if (response.totalItems === 1) {
          const countProducts = response.offersByRateCategory[0].products.length;
          if (countProducts > 1) {
            this.handleMultipleOffers(response, isPagination);
          } else {
            this.handleSingleOffer(response);
          }
        }
      }
    } else {
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
        manageStayGaConstants.SHOP_OFFER, manageStayGaConstants.OFFER_ERROR);
    }
  }

  private handleMultipleOffers(response: OffersByRateResponseModel, isPagination?: boolean): void {
    this.closeNoOffersModal();
    this.setGridView(response);
    if (!isPagination) {
      this.offersService.showAllOffers(true);
    }
    this.emitBroadSearchNoOffers(false);
  }

  private handleSingleOffer(response: OffersByRateResponseModel): void {
    this.closeNoOffersModal();
    this.openWarningModal(response.offersByRateCategory[0]);
    this.offersService.showAllOffers(false);
    this.emitBroadSearchNoOffers(false);
  }

  private handleNoOffers(eligibleForRateOverride: boolean): void {

    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
      manageStayGaConstants.OFFER_SERACH, manageStayGaConstants.OFFER_NOT_AVAILABLE);
    if (this.isShopOffer) {
      this.closeNoOffersModal();
      this.offersService.showAllOffers(true);
      this.emitBroadSearchNoOffers(true);
    } else {
      this.noOffersModal = this.modalService.openNoOffersModal(this.shopOffersError, eligibleForRateOverride);
      this.offersService.showAllOffers(false);
      this.emitBroadSearchNoOffers(false);
    }
  }

  private closeNoOffersModal(): void {
    if (this.noOffersModal) {
      this.noOffersModal.close();
    }
  }

  private emitBroadSearchNoOffers(noOffers: boolean): void {
    this.noOffers.emit(noOffers);
  }

  private setGridView(response: OffersByRateResponseModel): void {
    this.clearGridData();
    this.gridData = {
      data: response.offersByRateCategory,
      total: response.totalItems
    };
  }

  public handlePageChange(event: PageChangeEvent): void {
    this.gridState.skip = event.skip;
    this.gridState.take = event.take;
    this.clearGridData();
    this.getOffersByRate(true);
  }

  public getPanelTitle(rateCategory: OfferByRateCategoryModel): string {
    return `${rateCategory.name} (${rateCategory.code})`;
  }

  public setEstimatedTotal(estTotal: string): void {
    this.estimatedTotal.emit(estTotal);
  }

  private openWarningModal(rateCategory: OfferByRateCategoryModel): void {
    this.modalService.openWarningModal({
      searchCritera: this.searchCritera,
      originalReservationData: this.originalReservationData,
      reservationNumber: this.reservationNumber,
      rateInfoAfter: rateCategory.products[0]
    }, manageStayGaConstants.RATES);
    this.setEstimatedTotal(rateCategory.products[0].rateInfo.totalAmountAfterTax);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

}
