import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { OfferTypeButtonsComponent } from './offer-type-buttons.component';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { manageStayGaConstants } from '../../constants/offer-types.constant';

export class MockOffersService {
  trackClickManageStay(action, label) { }
  getShowOffers$() { }
}

export class MockModalHandlerService {
  getShopOffers$() { }
}

describe('OfferTypeButtonsComponent', () => {
  let component: OfferTypeButtonsComponent;
  let fixture: ComponentFixture<OfferTypeButtonsComponent>;

  const checkSelectedState = (btnIndex: number, shoulBeSelected: boolean) => {
    const btn = component.buttons[btnIndex];
    component.handleInteraction(btn, false);
    if (shoulBeSelected) {
      expect(btn.selected).toBeTruthy();
    } else {
      expect(btn.selected).toBeFalsy();
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OfferTypeButtonsComponent],
      providers: [
        { provide: OffersService, useClass: MockOffersService },
        { provide: ModalHandlerService, useClass: MockModalHandlerService }]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferTypeButtonsComponent);
    component = fixture.componentInstance;
  });

  it('should NOT apply selected state to View All Offers when clicked', () => {
    checkSelectedState(0, false);
  });

  it('should NOT apply selected state to View Suggested Offers when clicked', () => {
    checkSelectedState(1, false);
  });

  it('should apply selected state to Products when clicked', () => {
    checkSelectedState(2, true);
  });

  it('should apply selected state to Rates when clicked', () => {
    checkSelectedState(3, true);
  });

  it('should display all buttons except View All Offers when clicked on View All Offers button', () => {
    const allOffersBtn = component.buttons[0];
    component.handleInteraction(allOffersBtn, false);
    component.buttons.forEach(btn => {
      if (btn === allOffersBtn) {
        expect(btn.visible).toBeFalsy();
      } else {
        expect(btn.visible).toBeTruthy();
      }
    });
  });

  it('should display only View All Offers buttons when clicked on View Suggested Offers button', () => {
    const suggestedOffersBtn = component.buttons[1];
    const allOffersBtn = component.buttons[0];
    component.handleInteraction(suggestedOffersBtn, false);
    component.buttons.forEach(btn => {
      if (btn === allOffersBtn) {
        expect(btn.visible).toBeTruthy();
      } else {
        expect(btn.visible).toBeFalsy();
      }
    });
  });

  it('should call Google Analytics with proper parameters when a button is clicked', () => {
    const btn = component.buttons[2];
    const offersService: MockOffersService = TestBed.get(OffersService);
    const trackSpy = spyOn(offersService, 'trackClickManageStay');
    component.handleInteraction(btn, true);
    expect(trackSpy).toHaveBeenCalledWith(btn.gaLabel, manageStayGaConstants.LBL_SELECTED);
  });

  it('#subscribeToShopOffers should display View All Offers Button when offers are returned by Shop Offers action', fakeAsync(() => {
    const btn = component.buttons[0];
    const modalService: MockModalHandlerService = TestBed.get(ModalHandlerService);
    spyOn(modalService, 'getShopOffers$').and.returnValue(of(true));
    spyOn(component, 'subscribeToShowOffers').and.callFake(() => { });
    fixture.detectChanges();
    tick();
    expect(btn.visible).toBeTruthy();
  }));

  it('#subscribeToShowOffers should display View All Offers Button when offers are returned by Apply action on Stay Info tab', fakeAsync(() => {
    const btn = component.buttons[0];
    const offersService: MockOffersService = TestBed.get(OffersService);
    spyOn(offersService, 'getShowOffers$').and.returnValue(of(true));
    spyOn(component, 'subscribeToShopOffers').and.callFake(() => { });
    fixture.detectChanges();
    tick();
    expect(btn.visible).toBeTruthy();
  }));

  it('should persist previously selected button when navigated to All Offers view', () => {
    const allOffrsBtn = component.buttons[0];
    const suggestedOffrsBtn = component.buttons[1];
    const productsBtn = component.buttons[2];
    const ratesBtn = component.buttons[3];

    component.handleInteraction(allOffrsBtn, true); // Go to all offers
    component.handleInteraction(ratesBtn, true); // select Rates button
    component.handleInteraction(suggestedOffrsBtn, true); // Go back to suggested offers
    component.handleInteraction(allOffrsBtn, true); // Go to all offers again

    expect(ratesBtn.selected).toBeTruthy();
    expect(productsBtn.selected).toBeFalsy();
  });
});
