import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { cloneDeep } from "lodash";

import { offerButtons, manageStayGaConstants, offerTypes } from '../../constants/offer-types.constant';
import { OfferTypeBtnModel } from '../../models/offers.models';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';

@Component({
  selector: 'app-offer-type-buttons',
  templateUrl: './offer-type-buttons.component.html',
  styleUrls: ['./offer-type-buttons.component.scss']
})
export class OfferTypeButtonsComponent implements OnInit, OnDestroy {

  public buttons: OfferTypeBtnModel[] = cloneDeep(offerButtons);
  private rootSubscription = new Subscription();

  @Output() selection = new EventEmitter<OfferTypeBtnModel>();

  constructor(private offersService: OffersService, private modalService: ModalHandlerService) { }

  ngOnInit() {
    this.subscribeToShopOffers();
    this.subscribeToShowOffers();
  }

  subscribeToShopOffers(): void {
    this.rootSubscription.add(this.modalService.getShopOffers$().subscribe(() => {
      this.resetSelectedState();
      this.handleInteraction(this.buttons[1], false);
    }));
  }

  subscribeToShowOffers(): void {
    this.rootSubscription.add(this.offersService.getShowOffers$().subscribe((shouldShow) => {
      if (shouldShow) {
        this.resetSelectedState();
        this.handleInteraction(this.buttons[1], false);
      }
    }));
  }

  handleInteraction(button: OfferTypeBtnModel, shouldTrack: boolean): void {
    this.updateVisibleState(button);
    this.updateSelectedState(button);
    this.emitSelection(button);
    if (shouldTrack) {
      this.offersService.trackClickManageStay(button.gaLabel,
        manageStayGaConstants.LBL_SELECTED);
    }
  }

  resetSelectedState(): void {
    this.buttons.forEach(btn => btn.selected = false);
  }

  updateSelectedState(button: OfferTypeBtnModel): void {
    if (button.type === offerTypes.ALL || button.type === offerTypes.SUGGESTED) {
      if (this.isProductAndRateUnselected()) {
        this.getBtnByType(offerTypes.PRODUCTS).selected = true;
      }
    } else {
      this.resetSelectedState();
      button.selected = true;
    }
  }

  emitSelection(button: OfferTypeBtnModel): void {
    let effectiveOfferType: OfferTypeBtnModel;
    if (button.type === offerTypes.ALL) {
      if (this.isProductAndRateUnselected()) {
        effectiveOfferType = this.getBtnByType(offerTypes.PRODUCTS);
      } else {
        effectiveOfferType = this.buttons.find(btn => btn.selected);
      }
    } else {
      effectiveOfferType = button;
    }
    this.selection.emit(effectiveOfferType);
  }

  isProductAndRateUnselected(): boolean {
    return !this.getBtnByType(offerTypes.PRODUCTS).selected &&
      !this.getBtnByType(offerTypes.RATES).selected;
  }

  getBtnByType(type: string): OfferTypeBtnModel {
    return this.buttons.find(btn => btn.type === type);
  }

  updateVisibleState(button: OfferTypeBtnModel): void {
    if (button.type === offerTypes.ALL) {
      this.buttons.forEach(btn => btn.visible = btn.type !== offerTypes.ALL);
    } else if (button.type === offerTypes.SUGGESTED) {
      this.buttons.forEach(btn => btn.visible = btn.type === offerTypes.ALL);
    }
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
