import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';
import { UserService, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { OfferUnavailableModalComponent } from './offer-unavailable-modal.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('OfferUnavailableModalComponent', () => {
  let component: OfferUnavailableModalComponent;
  let fixture: ComponentFixture<OfferUnavailableModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forChild(), NgbModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [OfferUnavailableModalComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [NgbActiveModal, HttpClient, TranslateStore, UserService, GoogleAnalyticsService,
        {provide: ReportIssueService, useValue: {}}]
    })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferUnavailableModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the modal on clicking x', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.close')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('modal title should have offers not available', () => {
    const element = fixture.debugElement.query(By.css('.modal-header')).nativeElement;
    expect(element).not.toBeNull();
    expect(element.textContent.indexOf('LBL_OFR_UNAVLBL') > -1).toBeTruthy();
  });

  it('should close the modal on clicking close', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.btn-default')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('should display rate override button if eligible is true', () => {
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideButtonNoffers-SID"]')).nativeElement;
    expect(el).toBeDefined();
  });


  it('check trackEvent method is called when rate override click', () => {
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();    
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideButtonNoffers-SID"]')).nativeElement;
    overrideButton.click();
    fixture.detectChanges();    
    expect(trackEventSpy).toHaveBeenCalledWith('Check In - Manage Stay', 'Offer Unavailable Modal', 'Rate Override Selected');
  });

  it('should display rate override button if eligible is true', () =>  {
    component.isEligibleForRateOverride = false;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideButtonNoffers-SID"]'));
    expect(el).toBeNull();
  });
});
