import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { OFFER_ERROR_TOAST } from '../../constants/error-toast.constant';
import { OffersService } from '../../services/offers.service';
import { MANAGE_STAY_LOAD_ERROR, OFFERS_LOAD_ERROR } from '@app/constants/error-autofill-constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { manageStayGaConstants } from '../../constants/offer-types.constant';

@Component({
  selector: 'app-offer-unavailable-modal',
  templateUrl: './offer-unavailable-modal.component.html',
  styleUrls: ['./offer-unavailable-modal.component.scss']
})
export class OfferUnavailableModalComponent implements OnInit, OnDestroy {
  public reportIssueAutoFill = OFFERS_LOAD_ERROR;
  public errorContent = OFFER_ERROR_TOAST;

  @Input() shopOffersError = new Subject<EmitErrorModel>();
  @Input() isEligibleForRateOverride = false;
  @Output() shopOffers = new EventEmitter<void>();
  @Output() rateOverride = new EventEmitter<void>();

  constructor(private activeModal: NgbActiveModal, private offersService: OffersService, private gaService: GoogleAnalyticsService,) { }

  ngOnInit() {
    this.resetPageError();
    this.getReportIssue();
  }

  public handleShopOffers(): void {
    this.resetPageError();
    this.shopOffers.emit();
  }

  public dismissModal(): void {
    this.activeModal.dismiss();
  }

  private resetPageError(): void {
    this.offersService.resetOffersError();
    this.offersService.updateOffersErrorTrigger(false, false, true);
  }

  private getReportIssue() {
    if (this.offersService.checkForManageStayEdit()) {
      this.reportIssueAutoFill = MANAGE_STAY_LOAD_ERROR;
    }
  }

  public handleRateOverride(): void {
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, manageStayGaConstants.ACT_OFR_NOT_AVAIL, manageStayGaConstants.LBL_RATE_OVERRIDE);
    this.resetPageError();
    this.rateOverride.emit();
  }

  ngOnDestroy() {
    this.resetPageError();
  }
}
