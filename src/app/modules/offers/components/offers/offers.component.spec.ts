import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject, Subscription } from 'rxjs';
import { cloneDeep } from 'lodash';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { ConfirmationModalComponent, ConfirmationModalService } from 'ihg-ng-common-components';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { OffersComponent } from './offers.component';
import { OffersService } from '../../services/offers.service';
import { MOCK_SEARCHCRITERIA, MOCK_SUGGESTED_OFFRS } from '../../mocks/suggested-offers-tabs-mock';
import { OfferTypeBtnModel, SuggestedOffersModel } from '../../models/offers.models';
import { manageStayGaConstants, offerButtons, offerTypes } from '../../constants/offer-types.constant';

import { RouterTestingModule } from '@angular/router/testing';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { UserGuidanceComponent } from 'ihg-ng-common-kendo';
import { By } from '@angular/platform-browser';
import { ForceSellService } from '../../services/force-sell/force-sell.service';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ChangeStayService } from '@modules/offers/services/change-stay/change-stay.service';
import { offersByProductMockResponse } from '@modules/offers/mocks/offers-by-products.mock';
import { ModalHandlerService } from '@modules/offers/services/modal-handler/modal-handler.service';

export class MockOffersService {
  private showOffers = new BehaviorSubject(false);
  private isShowOffers = this.showOffers.asObservable();
  getShowOffers$(): Observable<boolean> {
    return this.isShowOffers;
  }
  showAllOffers(showOffers: boolean) {
    this.showOffers.next(showOffers);
  }
  checkForManageStayEdit() {
    return false;
  }
}

export class MockRouter {
  navigate(path: [string]) { }
}

export class MockGoogleAnalyticsService {
  trackEvent() { }
}


describe('OffersComponent', () => {
  let component: OffersComponent;
  let fixture: ComponentFixture<OffersComponent>;
  let service: OffersService;
  let suggestedOffers: SuggestedOffersModel;
  let offerTypeButtons: OfferTypeBtnModel[];

  function verifyCardTitle(offerType: OfferTypeBtnModel, expectedTitle: string): void {
    component.handleOfferTypeSelection(offerType);
    fixture.detectChanges();
    expect(component.cardTitle).toEqual(expectedTitle);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [OffersComponent, UserGuidanceComponent, ConfirmationModalComponent],
      providers: [
        ConfirmationModalService,
        // { provide: Router, useClass: MockRouter },
        { provide: OffersService, useClass: MockOffersService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .overrideModule(BrowserDynamicTestingModule,
        { set: { entryComponents: [ConfirmationModalComponent] } })
      .compileComponents().then(() => {
      setTestEnvironment();
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersComponent);
    component = fixture.componentInstance;
    service = TestBed.get(OffersService);
    suggestedOffers = cloneDeep(MOCK_SUGGESTED_OFFRS);
    offerTypeButtons = cloneDeep(offerButtons);
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should properly handle visibility of offer section', () => {
    service.showAllOffers(false);
    fixture.detectChanges();
    expect(component.shouldShowOffers).toBeFalsy();

    service.showAllOffers(true);
    fixture.detectChanges();
    expect(component.shouldShowOffers).toBeTruthy();
  });

  it('should properly handle subscription', () => {
    const dummySubscription = new Subscription();
    const subscribeSpy = spyOn(component, 'subscribeToShowOffers').and.returnValue(dummySubscription);
    const addSpy = spyOn(component['rootSubscription'], 'add');
    component.ngOnInit();
    expect(subscribeSpy).toHaveBeenCalledTimes(1);
    expect(addSpy).toHaveBeenCalledWith(dummySubscription);
  });

  it('should properly handle title of offer section', () => {
    verifyCardTitle(offerTypeButtons[0], 'LBL_ALL_OFFERS');
    verifyCardTitle(offerTypeButtons[1], 'LBL_SGSTD_OFRS');
    verifyCardTitle(offerTypeButtons[2], 'LBL_ALL_OFFERS');
    verifyCardTitle(offerTypeButtons[3], 'LBL_ALL_OFFERS');
  });

  it('should show only Suggested Offers section when clicked on View Suggested Offers button', () => {
    component.handleOfferTypeSelection(offerTypeButtons[1]);
    fixture.detectChanges();
    expect(component.offerCategoryVisibility[offerTypes.PRODUCTS]).toBeFalsy();
    expect(component.offerCategoryVisibility[offerTypes.RATES]).toBeFalsy();
    expect(component.offerCategoryVisibility[offerTypes.SUGGESTED]).toBeTruthy();
  });

  it('should show only Products grid when clicked on Products button', () => {
    component.handleOfferTypeSelection(offerTypeButtons[2]);
    fixture.detectChanges();
    expect(component.offerCategoryVisibility[offerTypes.PRODUCTS]).toBeTruthy();
    expect(component.offerCategoryVisibility[offerTypes.RATES]).toBeFalsy();
    expect(component.offerCategoryVisibility[offerTypes.SUGGESTED]).toBeFalsy();
  });

  it('should show only Rates grid when clicked on Rates button', () => {
    component.handleOfferTypeSelection(offerTypeButtons[3]);
    fixture.detectChanges();
    expect(component.offerCategoryVisibility[offerTypes.PRODUCTS]).toBeFalsy();
    expect(component.offerCategoryVisibility[offerTypes.RATES]).toBeTruthy();
    expect(component.offerCategoryVisibility[offerTypes.SUGGESTED]).toBeFalsy();
  });

  it('should properly handle no offers scenario', () => {
    expect(component.noOffers).toBeFalsy();
    component.cardTitle = 'Random title';
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    fixture.detectChanges();
    component.handleNoOffers(true);
    expect(component.searchCritera).toBeDefined();
    expect(component.searchCritera.roomType).toEqual('CSTN');
    fixture.detectChanges();
    expect(component.noOffers).toBeTruthy();
    expect(component.cardTitle).toEqual('LBL_ALL_OFFERS');

    component.handleNoOffers(false);
    fixture.detectChanges();
    expect(component.noOffers).toBeFalsy();
  });

  it('should take the user back to check in when clicked on Back to Check In button', () => {
    const spy = spyOn(component['router'], 'navigate');
    expect(spy).not.toHaveBeenCalled();
    component.navigateToCheckIn();
    expect(spy).toHaveBeenCalledWith(['/check-in']);
  });

  it('should properly set the suggested offers', () => {
    expect(component.suggestedOffers).toBeUndefined();
    component.handleSuggestedOffers(suggestedOffers);
    fixture.detectChanges();
    expect(component.suggestedOffers).toEqual(suggestedOffers);
  });

  it('should emit estimetedTotal with proper value', () => {
    const spy = spyOn(component.estimatedTotal, 'emit');
    component.setEstimatedTotal('100.00');
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith('100.00');
  });

  it('should properly unsubscribe from registered subscriptions', () => {
    const spy = spyOn(component['rootSubscription'], 'unsubscribe');
    fixture.detectChanges();
    expect(spy).not.toHaveBeenCalled();
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it('should show correct No Offers Button and perform correct action for In House - Edit Stay Flow', () => {
    const handleManageStayNavigation = spyOn(component, 'handleManageStayNavigation').and.callThrough();
    const handleManageStayEmit = spyOn<any>(component.handleManageStay, 'emit').and.callThrough();
    component.noOffers = true;
    component.isInHouseStay = true;
    fixture.detectChanges();

    const backToManageStayButton = fixture.debugElement.query(By.css('[translate="LBL_BK_TO_MANAGE_STAY"]'));
    backToManageStayButton.triggerEventHandler('click', null);

    expect(fixture.debugElement.query(By.css('[translate="LBL_BK_TO_CHK_IN"]'))).toBeNull();
    expect(backToManageStayButton).toBeDefined();
    expect(handleManageStayNavigation).toHaveBeenCalled();
    expect(handleManageStayEmit).toHaveBeenCalled();
  });

  it('should show correct No Offers Button and perform correct action for default Check In - Edit Stay Flow', () => {
    component.noOffers = true;
    fixture.detectChanges();

    const backToManageStayButton = fixture.debugElement.query(By.css('[translate="LBL_BK_TO_CHK_IN"]'));

    expect(fixture.debugElement.query(By.css('[translate="LBL_BK_TO_MANAGE_STAY"]'))).toBeNull();
    expect(backToManageStayButton).toBeDefined();
  });

  it('should display Force Sell button when no rooms are available for the requested offer', () => {
    component.noOffers = true;
    component.roomsForceSold = 0;
    component.isInHouseStay = false;
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    fixture.detectChanges();
    const forceSellButton = fixture.debugElement.query(By.css('[translate="BTN_FRCE_SELL"]')).nativeElement;
    expect(forceSellButton.textContent).toBeDefined();
  });

  it('should display warning modal after clicking on the Force Sell button', () => {
    component.noOffers = true;
    component.roomsForceSold = 0;
    component.isInHouseStay = false;
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    fixture.detectChanges();
    const openForceSellWarningModalSpy = spyOn(TestBed.get(ForceSellService), 'forceSellWarningModal').and.returnValue(of(''));
    fixture.detectChanges();
    const forceSellButton = fixture.debugElement.query(By.css('[translate="BTN_FRCE_SELL"]')).nativeElement;
    expect(forceSellButton).not.toBeNull();
    expect(forceSellButton.textContent).toBeDefined();
    forceSellButton.click();
    expect(openForceSellWarningModalSpy).toHaveBeenCalled();
    expect(openForceSellWarningModalSpy).toBeDefined();
  });

  it('should track GA events when force sell button is displayed for no offers page', () => {
    component.roomsForceSold = 0;
    component.isInHouseStay = false;
    const trackForceSellEventsSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent');
    component['trackForceSellButtonDisplay']();
    fixture.detectChanges();
    expect(trackForceSellEventsSpy).toHaveBeenCalled();
    expect(trackForceSellEventsSpy).toHaveBeenCalledWith(
      manageStayGaConstants.CATEGORY_MANAGE_STAY,
      manageStayGaConstants.FORCE_SELL_NO_OFFERS,
      manageStayGaConstants.LBL_FORCE_SELL_VIEW);
  });

  it('should track GA events when force sell button is clicked on the modal', () => {
    component.noOffers = true;
    component.roomsForceSold = 0;
    component.isInHouseStay = false;
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    const spy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent');
    component['onSuccessForceSell']();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith(manageStayGaConstants.CATEGORY_MANAGE_STAY,
      manageStayGaConstants.FORCE_SELL_NO_OFFERS_MODAL,
      manageStayGaConstants.LBL_FORCE_SELL_SELECTED);
  });

  it('Should call the products Force Sell API on Force Sell click from the confirmation modal #forceSellRoom', () => {
    component.noOffers = true;
    component.roomsForceSold = 0;
    component.isInHouseStay = false;
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    const confirm = new Subject<void>();
    const openForceSellWarningModalSpy = spyOn(TestBed.get(ForceSellService), 'forceSellWarningModal')
      .and.returnValue(confirm.asObservable());
    const forceSellRoom = spyOn(component, 'forceSellRoom').and.callThrough();
    fixture.detectChanges();

    component.displayForceSellModal();
    confirm.next();
    fixture.detectChanges();

    expect(openForceSellWarningModalSpy).toHaveBeenCalled();
    expect(forceSellRoom).toHaveBeenCalled();
  });

  it('Should display the warning confirmation modal when Force Sell button is clicked #forceSellRoom', () => {
    component.noOffers = true;
    component.roomsForceSold = 0;
    component.isInHouseStay = false;
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    const confirm = new Subject<void>();
    const openForceSellWarningModalSpy = spyOn(TestBed.get(ForceSellService), 'forceSellWarningModal')
      .and.returnValue(confirm.asObservable());
    const forceSellRoom = spyOn(component, 'forceSellRoom').and.callThrough();
    const getOffersByProductsForceSell = spyOn(TestBed.get(ChangeStayService), 'getOffersByProductsForceSell')
      .and.returnValue(of(cloneDeep(offersByProductMockResponse)));
    const openWarningModal = spyOn(TestBed.get(ModalHandlerService), 'openWarningModal')
      .and.callFake(() => {
      });
    fixture.detectChanges();

    component.displayForceSellModal();
    confirm.next();
    fixture.detectChanges();

    expect(openForceSellWarningModalSpy).toHaveBeenCalled();
    expect(forceSellRoom).toHaveBeenCalled();
    expect(getOffersByProductsForceSell).toHaveBeenCalled();
    expect(openWarningModal).toHaveBeenCalled();
  });

});
