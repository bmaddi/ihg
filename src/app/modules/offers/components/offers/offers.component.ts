import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { get } from 'lodash';

import { StayInformationModel, StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { OfferTypeBtnModel, SuggestedOffersModel } from '../../models/offers.models';
import { manageStayGaConstants, offerTypes } from '../../constants/offer-types.constant';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '@modules/offers/services/modal-handler/modal-handler.service';
import { ForceSellService } from '../../services/force-sell/force-sell.service';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ChangeStayService } from '@modules/offers/services/change-stay/change-stay.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit, OnDestroy {

  public shouldShowOffers = false;
  public cardTitle = 'LBL_ALL_OFFERS';
  public cardSeleniumId = 'AllOffers-SID';
  public offerCategory = offerTypes;
  public offerCategoryVisibility = {
    [this.offerCategory.SUGGESTED]: true,
    [this.offerCategory.PRODUCTS]: false,
    [this.offerCategory.RATES]: false
  };
  public noOffers = false;
  public suggestedOffers: SuggestedOffersModel;
  private rootSubscription = new Subscription();
  public isInHouseStay: boolean;
  public overbookingValue: number;
  public roomsForceSold: number;

  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: StayInformationModel;
  @Input() reservationNumber: string;

  @Output() estimatedTotal = new EventEmitter<string>();
  @Output() isHavingErrror = new EventEmitter<boolean>();
  @Output() handleManageStay = new EventEmitter<void>();

  constructor(private forceSellService: ForceSellService,
              private gaService: GoogleAnalyticsService,
              private offersService: OffersService,
              private modalHandler: ModalHandlerService,
              private router: Router,
              private changeService: ChangeStayService) {
  }

  ngOnInit() {
    this.rootSubscription.add(this.subscribeToShowOffers());
    this.checkForInHouseEditStay();
    this.subscribeToShopNoOffersSelection();
  }

  subscribeToShowOffers(): Subscription {
    return this.offersService.getShowOffers$().subscribe((shouldShow) => {
      this.shouldShowOffers = shouldShow;
    });
  }

  subscribeToShopNoOffersSelection() {
    if (!this.isInHouseStay) {
      this.rootSubscription
        .add(
          this.modalHandler.shopOffers$.subscribe(() => {
            this.getRoomsForceSell();
          })
        );
    }
  }

  private checkForInHouseEditStay() {
    this.isInHouseStay = this.offersService.checkForManageStayEdit();
    if (this.isInHouseStay) {
      this.rootSubscription.add(this.offersService.getConflictsTriggerObservable().subscribe(
        data => {
          this.modalHandler.openRoomConflictsModal(data).subscribe(
            () => {
            },
            () => {
            });
        }
      ));
    }
  }

  public handleOfferTypeSelection(offerType: OfferTypeBtnModel): void {
    this.cardTitle = offerType.cardLabel;
    this.cardSeleniumId = offerType.cardSeleniumId;
    Object.keys(this.offerCategoryVisibility).forEach(key => {
      this.offerCategoryVisibility[key] = key === offerType.type;
      
    });
  }

  public setEstimatedTotal(estTotal: string): void {
    this.estimatedTotal.emit(estTotal);
  }

  public navigateToCheckIn(): void {
    this.router.navigate(['/check-in']);
  }

  public handleManageStayNavigation(): void {
    this.handleManageStay.emit();
  }

  public handleNoOffers(noOffers: boolean): void {
    this.noOffers = noOffers;
    if (noOffers) {
      this.cardTitle = 'LBL_ALL_OFFERS';
    }
  }

  public handleSuggestedOffers(offers: SuggestedOffersModel): void {
    this.suggestedOffers = offers;
  }

  ngOnDestroy() {
    this.forceSellService.clearRoomTypeCache();
    this.rootSubscription.unsubscribe();
  }

  getRoomsForceSell() {
    this.forceSellService.getForceSell(this.getSelectedRoomType()).subscribe(response => {
      if (response) {
        this.roomsForceSold = response.roomsForceSold;
        const numberOfRooms = this.searchCritera ? Number(this.searchCritera.numberOfRooms) : this.originalReservationData.numberOfRooms;
        this.overbookingValue = response.roomsForceSold - numberOfRooms;
        this.trackForceSellButtonDisplay();
      }
    });
  }

  private trackForceSellButtonDisplay() {
    if (!this.isInHouseStay && this.roomsForceSold <= 0) {
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
        manageStayGaConstants.FORCE_SELL_NO_OFFERS,
        manageStayGaConstants.LBL_FORCE_SELL_VIEW
      );
    }
  }

  displayForceSellModal() {
    this.forceSellService.forceSellWarningModal('hotel', this.overbookingValue)
      .subscribe(() => {
        this.forceSellRoom();
        this.onSuccessForceSell();
      });
  }

  forceSellRoom() {
    this.changeService.getOffersByProductsForceSell(this.searchCritera)
      .subscribe(res =>
        this.modalHandler.openWarningModal({
          searchCritera: this.searchCritera,
          originalReservationData: this.originalReservationData,
          reservationNumber: this.reservationNumber,
          rateInfoAfter: get(res, 'products[0].rateCategories[0]', null)
        }, manageStayGaConstants.PRODUCTS));
  }

  private onSuccessForceSell() {
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY,
      manageStayGaConstants.FORCE_SELL_NO_OFFERS_MODAL,
      manageStayGaConstants.LBL_FORCE_SELL_SELECTED);
  }

  private getSelectedRoomType(): string {
    return this.searchCritera.roomType || this.originalReservationData.roomTypeCode;
  }
}
