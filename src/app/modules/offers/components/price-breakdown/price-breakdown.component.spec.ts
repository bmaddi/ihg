import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {cloneDeep} from 'lodash';

import {PriceBreakdownComponent} from './price-breakdown.component';
import {By} from '@angular/platform-browser';
import {CommonTestModule} from '@modules/common-test/common-test.module';
import {SuggestedOfferComponent} from '@modules/offers/components/suggested-offer/suggested-offer.component';
import {TruncatedTooltipComponent} from '@app-shared/components/truncated-tooltip/truncated-tooltip.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {mockRateInfo} from './../../mocks/rate-info.mock';

import {RateInfoModel, SuggestedOffersModel} from '@modules/offers/models/offers.models';
import {MOCK_SUGGESTED_OFFRS} from '@modules/offers/mocks/suggested-offers-tabs-mock';

describe('PriceBreakdownComponent', () => {
  let component: PriceBreakdownComponent;
  let fixture: ComponentFixture<PriceBreakdownComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [PriceBreakdownComponent],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceBreakdownComponent);
    component = fixture.componentInstance;
    component.rateInfo = (cloneDeep(mockRateInfo) as RateInfoModel);

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should verify first night price break down is displayed at the top of the view Details Offer Page', () => {
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg="PriceBreakDown-SID"]')).nativeElement;
    expect(el).toBeDefined();
  });

  it('should verify multiple night price break down is displayed at the top of the view Details Offer Page', () => {
    fixture.detectChanges();
    for (let i = 0; i < component.rateInfo.dailyRates.length; i++) {
      const el: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg="PriceBreakDownRow' + i + '-SID"]')).nativeElement;
      expect(el).toBeDefined();
      expect(el).toBeTruthy();
    }
  });
});
