import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { RateInfoModel } from '../../models/offers.models';

@Component({
  selector: 'app-price-breakdown',
  templateUrl: './price-breakdown.component.html',
  styleUrls: ['./price-breakdown.component.scss']
})
export class PriceBreakdownComponent implements OnInit {

  @Input() rateInfo: RateInfoModel;
  @Input() currency: string;

  constructor() { }

  ngOnInit() {
    this.createDateRange();
  }

  private createDateRange(): void {
    this.rateInfo.dailyRates.forEach(dailyRate => {
      const start = moment(dailyRate.date).format('D MMMM');
      const end = moment(dailyRate.date).add(1, 'day').format('D MMMM');
      dailyRate.dateRange = start + ' - ' + end;
    });
  }

  public convertToNumberVal(val: string | null) {
    return Number(val);
  }

  public transformData(val: string | null) {
    return (val) ? val : "0.00";
  }
}
