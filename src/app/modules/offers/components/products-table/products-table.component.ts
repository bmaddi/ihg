import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

import { GuestWalkInModel, ProductModel } from '../../models/offers.models';
import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { manageStayGaConstants } from '../../constants/offer-types.constant';
import { RateInfoModel } from '../../models/offers.models';
import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html',
  styleUrls: ['./../../common/offers-grid.scss']
})
export class ProductsTableComponent implements OnInit, OnChanges {

  public readonly defaultItemCount = 4;
  public visibleItems: ProductModel[];

  @Input() productsData: ProductModel[];
  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: RateInfoModel;
  @Input() rateCode: string;
  @Input() rateName: string;
  @Input() pointsEligible: boolean;
  @Input() reservationNumber: string;
  
  @Output() estimatedTotal = new EventEmitter<string>();

  constructor(private offersService: OffersService, private modalService: ModalHandlerService) { }

  ngOnInit() {
    this.updateVisibleItems(this.defaultItemCount);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.productsData && changes.productsData.currentValue) {
      this.updateVisibleItems(this.defaultItemCount);
    }
  }

  public handleToggle(showAll: boolean) {
    this.offersService.trackClickManageStay(showAll ? manageStayGaConstants.ACTION_SHOW_MORE : manageStayGaConstants.ACTION_SHOW_LESS, manageStayGaConstants.LBL_SELECTED);
    const itemsToShow = showAll ? this.productsData.length : this.defaultItemCount;
    this.updateVisibleItems(itemsToShow);
  }

  private updateVisibleItems(itemsToShow: number): void {
    this.visibleItems = this.productsData.slice(0, itemsToShow);
  }

  public openWarningModal(index: number, secondarySelection?: boolean): void {
    const action = secondarySelection ? manageStayGaConstants.ACTION_PRODUCT_DESC : manageStayGaConstants.RATES;
    this.checkForReservationTypeFlow(index, action);
    this.estimatedTotal.emit(this.productsData[index].rateInfo.totalAmountAfterTax);
  }

  private checkForReservationTypeFlow(index: number, action: string) {
    if (this.reservationNumber) {
      this.modalService.openWarningModal({
        searchCritera: this.searchCritera,
        originalReservationData: this.originalReservationData,
        reservationNumber: this.reservationNumber,
        rateInfoAfter: this.productsData[index]
      }, action);
    } else {
      this.offersService.setNewReservationData(
        this.offersService.setInitialWalkInSelection(this.productsData[index], this.searchCritera)
      );
    }
  }

  public openDescriptionModal(item: ProductModel, index: number): void {
    this.modalService.openDescriptionModal({
      rateName: this.rateName,
      rateCode: this.rateCode,
      roomName: item.name,
      roomCode: item.code,
      roomDesc: item.description,
      pointsEligible: this.pointsEligible,
      rateInfo: item.rateInfo,
      arrivalDate: this.searchCritera.arrivalDate,
      departureDate: this.searchCritera.departureDate
    }, manageStayGaConstants.ACTION_PRODUCT_DESC
    ).result.then(select => {
      this.openWarningModal(index, true);
    }, cancel => { });
  }
}
