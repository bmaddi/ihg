import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GridModule } from '@progress/kendo-angular-grid';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { cloneDeep } from 'lodash';
import { By } from '@angular/platform-browser';

import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { RatesTableComponent } from './rates-table.component';
import { offersByRateCategoryMock } from '@modules/offers/mocks/offers-by-rates.mock';
import { ModalHandlerService } from '@modules/offers/services/modal-handler/modal-handler.service';

describe('RatesTableComponent', () => {
  let component: RatesTableComponent;
  let fixture: ComponentFixture<RatesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RatesTableComponent],
      imports: [
        GridModule,
        TranslateModule.forChild(),
        HttpClientTestingModule,
        RouterTestingModule,
        GridModule
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        UserService,
        TranslateStore,
        {provide: ReportIssueService, useValue: {}},
        {provide: GoogleAnalyticsService, useValue: {}}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatesTableComponent);
    component = fixture.componentInstance;
    component.ratesData = cloneDeep(offersByRateCategoryMock[0].products);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open warning modal #openWarningModal upon offer selection', () => {
    const openWarningModal = spyOn(component, 'openWarningModal').and.callThrough(),
      checkReservationFlowSpy = spyOn<any>(component, 'checkForReservationTypeFlow').and.callThrough(),
      handlerSpy = spyOn<any>(TestBed.get(ModalHandlerService), 'openWarningModal').and.returnValue(null);
    component.reservationNumber = '12312313';
    fixture.detectChanges();

    const selectButtons = fixture.debugElement.queryAll(By.css('[translate="COM_LBL_SLCT"]'));
    selectButtons[0].triggerEventHandler('click', null);

    expect(openWarningModal).toHaveBeenCalled();
    expect(checkReservationFlowSpy).toHaveBeenCalled();
    expect(handlerSpy).toHaveBeenCalled();
  });
});
