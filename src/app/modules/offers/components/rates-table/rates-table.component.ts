import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

import { OffersService } from '../../services/offers.service';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { manageStayGaConstants } from '../../constants/offer-types.constant';
import { GuestWalkInModel, RateCategoryModel, RateInfoModel } from '../../models/offers.models';
import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';

@Component({
  selector: 'app-rates-table',
  templateUrl: './rates-table.component.html',
  styleUrls: ['./../../common/offers-grid.scss']
})
export class RatesTableComponent implements OnInit, OnChanges {

  public readonly defaultItemCount = 4;
  public visibleItems: RateCategoryModel[];

  @Input() ratesData: RateCategoryModel[];
  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: RateInfoModel;
  @Input() roomCode: string;
  @Input() roomName: string;
  @Input() roomDesc: string;
  @Input() reservationNumber: string;

  @Output() estimatedTotal = new EventEmitter<string>();

  constructor(private offersService: OffersService, private modalService: ModalHandlerService) { }

  ngOnInit() {
    this.updateVisibleItems(this.defaultItemCount);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.ratesData && changes.ratesData.currentValue) {
      this.updateVisibleItems(this.defaultItemCount);
    }
  }

  public handleToggle(showAll: boolean) {
    this.offersService.trackClickManageStay(showAll ? manageStayGaConstants.ACTION_SHOW_MORE : manageStayGaConstants.ACTION_SHOW_LESS, manageStayGaConstants.LBL_SELECTED);
    const itemsToShow = showAll ? this.ratesData.length : this.defaultItemCount;
    this.updateVisibleItems(itemsToShow);
  }

  private updateVisibleItems(itemsToShow: number): void {
    this.visibleItems = this.ratesData.slice(0, itemsToShow);
  }

  public openWarningModal(index: number, secondarySelection?: boolean): void {
    const action = secondarySelection ? manageStayGaConstants.ACTION_RATE_DESC : manageStayGaConstants.PRODUCTS;
    this.checkForReservationTypeFlow(index, action);
    this.estimatedTotal.emit(this.ratesData[index].rateInfo.totalAmountAfterTax);
  }

  private checkForReservationTypeFlow(index: number, action: string) {
    if (this.reservationNumber) {
      this.modalService.openWarningModal({
        searchCritera: this.searchCritera,
        originalReservationData: this.originalReservationData,
        reservationNumber: this.reservationNumber,
        rateInfoAfter: this.ratesData[index]
      }, action);
    } else {
      this.offersService.setNewReservationData(
        this.offersService.setInitialWalkInSelection(this.ratesData[index], this.searchCritera)
      );
    }
  }

  public openDescriptionModal(item: RateCategoryModel, index: number): void {
    this.modalService.openDescriptionModal({
      rateName: item.name,
      rateCode: item.code,
      roomName: this.roomName,
      roomCode: this.roomCode,
      roomDesc: this.roomDesc,
      pointsEligible: item.pointsEligible,
      rateInfo: item.rateInfo,
      arrivalDate: this.searchCritera.arrivalDate,
      departureDate: this.searchCritera.departureDate
    }, manageStayGaConstants.ACTION_RATE_DESC
    ).result.then(select => {
      this.openWarningModal(index, true);
    }, cancel => { });
  }
}
