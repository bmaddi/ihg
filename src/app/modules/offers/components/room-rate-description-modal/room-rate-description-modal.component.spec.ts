import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import { By } from '@angular/platform-browser';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { AppSharedModule } from '@modules/shared/app-shared.module';
import { RoomRateDescriptionModalComponent } from './room-rate-description-modal.component';
import { mockRateInfo } from '../../mocks/rate-info.mock';
import { RateInfoModel } from '../../models/offers.models';
import { BehaviorSubject } from 'rxjs';
import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { EnvironmentService } from 'ihg-ng-common-core';
import { environmentServiceServiceStub } from '@modules/guest-relations/mocks/guest-relations.mock';
import { MOCK_SERVER_ERROR } from '@app/modules/all-guests/mocks/guest-reservation.mock';

export class MockActiveModal {
  close() { }

  dismiss() { }
}

export class MockReportIssueService { }

describe('RoomRateDescriptionModalComponent', () => {
  let component: RoomRateDescriptionModalComponent;
  let fixture: ComponentFixture<RoomRateDescriptionModalComponent>;
  const includedInRate = 'included in rate from';
  const notIncludedInRate = 'not included in rate from';
  const totalCharge = 'Total Service Charges of';
  const totalTax = 'Total Tax of';

  beforeEach(async(() => {
    TestBed
      .overrideProvider(EnvironmentService, {
        useValue: environmentServiceServiceStub
      })
      .configureTestingModule({
        imports: [CommonTestModule, AppSharedModule, NgbModule],
        declarations: [RoomRateDescriptionModalComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [EnvironmentService,
          { provide: NgbActiveModal, useClass: MockActiveModal },
          { provide: ReportIssueService, useClass: MockReportIssueService },
        ]
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomRateDescriptionModalComponent);
    component = fixture.componentInstance;
    setupComponentData();
  });

  function setupComponentData() {
    component.rateName = 'BEST FLEXIBLE RATE';
    component.rateCode = 'IGCOR';
    component.roomName = 'STANDARD ROOM NONSMOKING';
    component.roomCode = 'CSTN';
    component.roomDesc = 'WHEN YOU ARRIVE WE WILL SERVE YOU BEST AVAILABLE ROOM. TEST IHG AND RC.';
    component.pointsEligible = true;
    component.arrivalDate = '2019-11-01';
    component.departureDate = '2019-11-04';
    component.rateInfo = cloneDeep(mockRateInfo);
  }

  function checkZeroVal(key: string) {
    const rateInfo: RateInfoModel = cloneDeep(mockRateInfo);
    rateInfo.rateRules[0][key] = '0.0';
    component.rateInfo = rateInfo;
    fixture.detectChanges();
    const de = fixture.debugElement.query(By.css(`#${key}`));
    expect(de).toBeNull();
  }

  function checkSentenceCase(id: string, expected: string) {
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css(`#${id} p`)).nativeElement;
    expect(el.textContent.trim()).toEqual(expected);
  }

  it('should display price break down table', () => {
    fixture.detectChanges();
    const priceTable = fixture.debugElement.query(By.css('app-price-breakdown'));
    expect(priceTable).toBeDefined();
  });

  it('should display sections in proper order', () => {
    fixture.detectChanges();
    const sections = fixture.debugElement.queryAll(By.css('.section'));
    const orderedIds = ['roomDesc', 'rateRuleDesc', 'rateRules', 'serviceCharge', 'tax',
      'earlyDeparture', 'noShowPolicyDesc', 'extraPersonCharge', 'guaranteePolicy'];

    sections.forEach((section, index) => {
      expect(section.properties['id']).toEqual(orderedIds[index]);
    });
  });

  it('should render room description in sentence case leaving IHG and RC in upper case', () => {
    const expected = 'When you arrive we will serve you best available room. Test IHG and RC.';
    checkSentenceCase('roomDesc', expected);
  });

  it('should render rate description in sentence case leaving IHG and RC in upper case', () => {
    const expected = 'Includes room and 1000 rewards club points per night. Test IHG and RC.';
    checkSentenceCase('rateRuleDesc', expected);
  });

  it('should not render early departure if it is zero', () => {
    checkZeroVal('earlyDeparture');
  });

  it('should not render extra person charge if it is zero', () => {
    checkZeroVal('extraPersonCharge');
  });

  it('should not render tax if it is zero', () => {
    checkZeroVal('tax');
  });

  it('should not render service charge if it is zero', () => {
    checkZeroVal('serviceCharge');
  });

  it('should render proper text if service charge is included', () => {
    const rateInfo: RateInfoModel = cloneDeep(mockRateInfo);
    rateInfo['serviceChargeIncluded'] = true;
    component.rateInfo = rateInfo;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css(`#${'serviceCharge'} p`)).nativeElement;
    expect(el.textContent.trim()).toContain(includedInRate);
    expect(el.textContent.trim()).toContain(totalCharge);
  });

  it('should render proper text if service charge is NOT included', () => {
    const rateInfo: RateInfoModel = cloneDeep(mockRateInfo);
    rateInfo['serviceChargeIncluded'] = false;
    component.rateInfo = rateInfo;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css(`#${'serviceCharge'} p`)).nativeElement;
    expect(el.textContent.trim()).toContain(notIncludedInRate);
    expect(el.textContent.trim()).toContain(totalCharge);
  });

  it('should render proper text if tax is included', () => {
    const rateInfo: RateInfoModel = cloneDeep(mockRateInfo);
    rateInfo['taxesChargeIncluded'] = true;
    component.rateInfo = rateInfo;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css(`#${'tax'} p`)).nativeElement;
    expect(el.textContent.trim()).toContain(includedInRate);
    expect(el.textContent.trim()).toContain(totalTax);
  });

  it('should render proper text if tax is NOT included', () => {
    const rateInfo: RateInfoModel = cloneDeep(mockRateInfo);
    rateInfo['taxesChargeIncluded'] = false;
    component.rateInfo = rateInfo;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css(`#${'tax'} p`)).nativeElement;
    expect(el.textContent.trim()).toContain(notIncludedInRate);
    expect(el.textContent.trim()).toContain(totalTax);
  });

  it('should render proper heading if no data is available', () => {
    const error = new BehaviorSubject<EmitErrorModel>(null);
    error.next({ hasNoData: true });
    component.noRateDetailsErrorEvent = error;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css(`.title`)).nativeElement;
    expect(el.textContent.trim()).toBe('Rate Category');
  });

  it('Verify user encounters a system error when clicking on the rate code hyperlink  from All Guests Tab.', () => {
    const error = new BehaviorSubject<EmitErrorModel>(null);
    error.next(MOCK_SERVER_ERROR);
    component.noRateDetailsErrorEvent = error;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg="NoSearchResult-SID"]')).nativeElement;
    expect(el.textContent.trim()).toContain('Error');
  });
});
