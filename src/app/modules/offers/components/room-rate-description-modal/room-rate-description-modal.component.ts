import {Component, OnInit, Input, Inject, LOCALE_ID, Output, EventEmitter} from '@angular/core';
import { formatNumber } from '@angular/common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import * as moment from 'moment';
import { RateInfoModel, RoomDescriptionModel, RateRuleModel, RateCategoryDataModel } from '../../models/offers.models';
import { rateDescriptionMap, rateDescriptionOnlyMap } from '../../constants/rate-description-map.constant';
import { AppConstants } from '@app/constants/app-constants';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';
import { RATE_CODE_MODAL_ERROR_AUTOFILL } from '@app/constants/error-autofill-constants';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { ManageStayService } from '@modules/manage-stay/services/manage-stay.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import {BehaviorSubject, Observable} from 'rxjs';
import {EmitErrorModel} from '@app-shared/models/app-service-errors/app-service-errors-models';

@Component({
  selector: 'app-room-rate-description-modal',
  templateUrl: './room-rate-description-modal.component.html',
  styleUrls: ['./room-rate-description-modal.component.scss']
})
export class RoomRateDescriptionModalComponent extends AppErrorBaseComponent implements OnInit {
  currency: string;
  sections: RoomDescriptionModel[];
  checkInTime: string;
  checkOutTime: string;
  sectionMap: { [key: string]: RoomDescriptionModel; } = cloneDeep(rateDescriptionMap);
  sectionMapOnlyRate: { [key: string]: RoomDescriptionModel; } = cloneDeep(rateDescriptionOnlyMap);
  allCapsWords = ['IHG', 'RC'];
  errorAutofill: ReportIssueAutoFillObject = RATE_CODE_MODAL_ERROR_AUTOFILL;
  ga = GUEST_CONST.GA;

  @Input() rateCode: string;
  @Input() roomName: string;
  @Input() roomCode: string;
  @Input() roomDesc: string;
  @Input() pointsEligible: boolean;
  @Input() rateInfo: RateInfoModel;
  @Input() rateName: string;
  @Input() arrivalDate: string;
  @Input() departureDate: string;
  @Input() isOnlyRateInfo?: boolean;
  @Input() topic?: string;
  @Input() noRateDetailsErrorEvent?: BehaviorSubject<EmitErrorModel>;
  @Input() rateCodeData?: RateCategoryDataModel;
  @Input() noData?: boolean;

  constructor(private activeModal: NgbActiveModal,
              @Inject(LOCALE_ID) private locale: string,
              private gaService: GoogleAnalyticsService,
              private manageStayService: ManageStayService) {
    super();
  }

  ngOnInit() {
    if (!this.noRateDetailsErrorEvent) {
      this.currency = this.rateInfo.currency;
      this.rateInfo.rateRules[0].roomDesc = this.roomDesc;
      this.mapSections();
    } else {
      this.emitError = this.noRateDetailsErrorEvent;
      this.hasErrors = true;
    }
    this.errorAutofill.genericError.topic = this.errorAutofill.timeoutError.topic = this.topic;
  }

  closeModal(): void {
    this.activeModal.close('selected');
  }

  dismissModal(): void {
    this.activeModal.dismiss();
  }

  updateCheckinCheckout(): void {
    this.checkInTime = this.get12HrsFormat(this.rateInfo.rateRules[0].checkinTime);
    this.checkOutTime = this.get12HrsFormat(this.rateInfo.rateRules[0].checkoutTime);
  }

  get12HrsFormat(time: string): string {
    return moment(time, 'HH:mm:ss').format('hh:mm a');
  }

  mapSections(): void {
    if (this.isOnlyRateInfo) {
      this.sectionMap = this.sectionMapOnlyRate;
    }

    const rules = this.rateInfo.rateRules[0];
    this.sections = Object.keys(rules).map(key => {
      if (rules[key]) {
        if (this.sectionMap[key]) {
          this.prepareContent(rules, key);
          return this.sectionMap[key];
        } else if (key === 'checkinTime') {
          this.updateCheckinCheckout();
          return this.sectionMap['rateRules'];
        }
      }
    }).filter(item => item !== undefined && item !== null)
      .sort((curr, next) => curr.priority - next.priority);
  }

  prepareContent(rules: RateRuleModel, key: string): void {
    if (this.sectionMap[key].staticText) {
      this.handleStaticText(rules, key);
    } else {
      this.sectionMap[key].description = rules[key];
    }
  }

  handleStaticText(rules: RateRuleModel, key: string): void {
    if (parseFloat(rules[key]) > 0) {
      switch (key) {
        case 'earlyDeparture':
        case 'extraPersonCharge':
          this.sectionMap[key].translateParams = { value: this.formatAmount(rules[key]) };
          break;
        case 'tax':
          this.setConditionalStaticText(rules, key, this.rateInfo.taxesChargeIncluded);
          break;
        case 'serviceCharge':
          this.setConditionalStaticText(rules, key, this.rateInfo.serviceChargeIncluded);
          break;
      }
    } else {
      this.sectionMap[key] = null;
    }
  }

  onRefresh(error) {
    switch (error) {
      case 0:
        this.gaService.trackEvent(this.topic, this.ga.DATA_TIMEOUT, this.ga.TRY_AGAIN);
        break;
      case 2:
        this.gaService.trackEvent(this.topic, this.ga.ERROR, this.ga.REFRESH);
        break;
    }
    this.getRateData();
  }

  getRateData() {
    this.manageStayService.getRateCodeData(this.rateCodeData).subscribe((response) => {
      this.rateName = response.name;
      this.rateCode = response.code;
      this.pointsEligible = response.pointsEligible;
      this.rateInfo = response.rateInfo;
    }, error => this.processHttpErrors(error));
  }

  setConditionalStaticText(rules: RateRuleModel, key: string, included: boolean): void {
    const section = this.sectionMap[key];
    section.staticText = included ? section.staticText['included'] : section.staticText['notIncluded'];
    section.translateParams = this.getTranslateParams(rules, key);
  }

  getTranslateParams(rules: RateRuleModel, key: string) {
    return {
      value1: this.formatAmount(rules[key]),
      value2: moment(this.arrivalDate).format(AppConstants.VW_DT_FORMAT).toUpperCase(),
      value3: moment(this.departureDate).format(AppConstants.VW_DT_FORMAT).toUpperCase()
    };
  }

  formatAmount(amount: string): string {
    return `${formatNumber(+amount, this.locale, '.2-2')} ${this.currency}`;
  }
}
