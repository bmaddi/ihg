import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMoreLessComponent } from './show-more-less.component';

xdescribe('ShowMoreLessComponent', () => {
  let component: ShowMoreLessComponent;
  let fixture: ComponentFixture<ShowMoreLessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowMoreLessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMoreLessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
