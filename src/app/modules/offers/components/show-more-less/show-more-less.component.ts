import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-show-more-less',
  templateUrl: './show-more-less.component.html',
  styleUrls: ['./show-more-less.component.scss']
})
export class ShowMoreLessComponent implements OnInit {

  public btnLabel: string;

  @Input() isShowingMore: boolean;
  @Output() toggle = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
    this.updateBtnLabel();
  }

  private updateBtnLabel(): void {
    this.btnLabel = this.isShowingMore ? 'LBL_GST_SHOW_LESS': 'LBL_GST_SHOW_MORE';
  }

  public handleToggle(): void {
    this.isShowingMore = !this.isShowingMore;
    this.updateBtnLabel();
    this.toggle.emit(this.isShowingMore);
  }

}
