import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { cloneDeep } from 'lodash';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { SuggestedOfferComponent } from './suggested-offer.component';
import { TruncatedTooltipComponent } from '@app-shared/components/truncated-tooltip/truncated-tooltip.component';
import { MOCK_SUGGESTED_OFFRS } from './../../mocks/suggested-offers-tabs-mock';
import { SuggestedOffersModel } from '../../models/offers.models';

describe('SuggestedOfferComponent', () => {
  let component: SuggestedOfferComponent;
  let fixture: ComponentFixture<SuggestedOfferComponent>;

  function hasClass(element: HTMLElement, classMatcher: string): boolean {
    return element.className.split(' ').indexOf(classMatcher) !== -1;
  }

  function getItem(index: number): HTMLElement {
    return fixture.debugElement.queryAll(By.css('.items li'))[index].nativeElement.querySelector('.fas');
  }

  function checkIcon(index: number, checkIcon: boolean) {
    fixture.detectChanges();
    const element = getItem(index);
    const icon = checkIcon ? 'fa-check' : 'fa-times';
    expect(hasClass(element, icon)).toBeTruthy();
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [SuggestedOfferComponent, TruncatedTooltipComponent],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestedOfferComponent);
    component = fixture.componentInstance;
    component.offer = (cloneDeep(MOCK_SUGGESTED_OFFRS) as SuggestedOffersModel).suggestedProducts.ONE_BED.rateCategories[0];
    component.roomType = 'KING BED NONSMOKING KING BED NONSMOKING';
    component.roomCode = 'KNGN';
    fixture.detectChanges();
  });

  it('should verify that selected state is applied upon click', () => {
    component.handleSelect();
    fixture.detectChanges();
    const offerPanel: HTMLElement = fixture.debugElement.query(By.css('.offer-panel')).nativeElement;
    const btnContainer = fixture.debugElement.query(By.css('.btn-wrap'));
    expect(hasClass(offerPanel, 'selected')).toBeTruthy();
    expect(btnContainer).toBeDefined();
  });

  it('should verify that average nightly rate is displayed with proper decimal formatting', () => {
    const value = '123';
    const expected = '123.00';
    component.offer.averageNightlyRate = value;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css('.nightly-rate')).nativeElement;
    const actual = el.textContent.trim();
    expect(actual).toEqual(expected);
  });

  it('should verify that currency code is displayed in uppercase', () => {
    const value = 'usd';
    const expected = 'USD';
    component.offer.rateInfo.currency = value;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg="CurrencyCode-SID"]')).nativeElement;
    const actual = el.textContent.trim();
    expect(actual).toEqual(expected);
  });

  it('should verify that rate name is displayed in upper case and truncated properly', () => {
    const value = 'THIS IS A RATE NAME THIS IS A RATE NAME THIS IS A RATE NAME';
    const expected = 'THIS IS A RATE NAME THIS IS A RATE NAME THIS IS A RATE NAME';
    component.offer.name = value;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css('app-truncated-tooltip .truncate-text__container.rate-name')).nativeElement;
    const actual = el.textContent.trim();
    expect(actual).toEqual(expected);
  });

  it('should verify that the room type name is truncated properly', () => {
    const value = 'THIS IS ROOM TYPE NAME';
    const expected = 'This Is Room Type Name';
    component.roomType = value;
    fixture.detectChanges();
    const el: HTMLElement = fixture.debugElement.query(By.css('app-truncated-tooltip .truncate-text__container.room-type')).nativeElement;
    const actual = el.textContent.trim();
    expect(actual).toEqual(expected);
  });

  it('should verify that room code and icon are displayed properly', () => {
    const value = 'CSTN';
    const expected = '(CSTN)';
    component.roomCode = value;
    fixture.detectChanges();
    const roomCode: HTMLElement = fixture.debugElement.query(By.css('.room-code')).nativeElement;
    const actual = roomCode.textContent.trim();
    expect(actual).toEqual(expected);

    const icon: HTMLElement = fixture.debugElement.query(By.css('.room-info .fa-bed')).nativeElement;
    expect(icon).toBeDefined();
  });

  it('should verify that rate code is displayed properly', () => {
    const value = 'IGCOR';
    const expected = '(IGCOR)';
    component.roomCode = value;
    fixture.detectChanges();
    const roomCode: HTMLElement = fixture.debugElement.query(By.css('.rate-code')).nativeElement;
    const actual = roomCode.textContent.trim();
    expect(actual).toEqual(expected);
  });

  it('should verify that refundable is displayed with proper icon', () => {
    component.offer.rateInfo.rateRules[0].refundable = true;
    checkIcon(0, true);

    component.offer.rateInfo.rateRules[0].refundable = false;
    checkIcon(0, false);
  });

  it('should verify that Points Eligible is displayed with proper icon', () => {
    component.offer.pointsEligible = true;
    checkIcon(1, true);

    component.offer.pointsEligible = false;
    checkIcon(1, false);
  });

  it('should verify that Breakfast Included is displayed with proper icon', () => {
    component.offer.breakfastIncluded = true;
    checkIcon(2, true);

    component.offer.breakfastIncluded = false;
    checkIcon(2, false);
  });

  it('should verify that view Details button is displayed and is clickable', () => {
    component.handleSelect();
    fixture.detectChanges();
    component.selected = true;
    const el: HTMLElement = fixture.debugElement.query(By.css('[data-slnm-ihg="ViewDetails-SID"]')).nativeElement;
    expect(el).toBeDefined();
    el.click();
    expect(el).toBeTruthy();
  });
});
