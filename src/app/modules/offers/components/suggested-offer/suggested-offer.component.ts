import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RateCategoryModel } from '../../models/offers.models';

@Component({
  selector: 'app-suggested-offer',
  templateUrl: './suggested-offer.component.html',
  styleUrls: ['./suggested-offer.component.scss']
})
export class SuggestedOfferComponent implements OnInit {

  @Input() offer: RateCategoryModel;
  @Input() roomType: string;
  @Input() roomCode: string;
  @Input() selected: boolean;
  @Output() viewDetails = new EventEmitter<void>();
  @Output() select = new EventEmitter<void>();
  @Output() selectOffer = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  public handleSelect(): void {
    this.selected = true;
    this.select.emit();
  }

  public handleViewDetails(): void {
    this.viewDetails.emit();
  }

  public handleSelectOffer(): void {
    this.selectOffer.emit();
  }

}
