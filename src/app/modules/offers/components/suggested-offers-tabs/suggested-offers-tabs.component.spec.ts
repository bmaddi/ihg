import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbTabChangeEvent, NgbTabset, NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';

import { SuggestedOffersTabsComponent } from './suggested-offers-tabs.component';
import { SuggestedOfferComponent } from './../suggested-offer/suggested-offer.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';

import { cloneDeep } from 'lodash';

import { MOCK_ORIGINAL_RESERV, MOCK_SEARCHCRITERIA, MOCK_SUGGESTED_OFFRS } from './../../mocks/suggested-offers-tabs-mock';
import { RateCategoryModel, SuggestedOffersModel, SuggestedOffersTabsModel } from '@app/modules/offers/models/offers.models';
import {
  gaSuggestedOffersConstants,
  suggestedOfferTabs,
  suggestedOfferTypes
} from '@app/modules/offers/constants/suggested-offers.constant';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { Alert, AlertType, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { OffersService } from '../../services/offers.service';
import { ConfirmationModalComponent, ConfirmationModalService } from 'ihg-ng-common-components';
import { ForceSellService } from '@modules/offers/services/force-sell/force-sell.service';

export class MockNgbTabSet {
  select() { }
}

export class MockModalHandlerService {
  openWarningModal() { }

  openDescriptionModal() { }
}

export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve('x'));
}

export class MockGoogleAnalyticsService {
  trackEvent() { }
}
export class OffersServiceMock {
  checkForManageStayEdit() {
    return false;
  }
}

describe('SuggestedOffersTabsComponent', () => {
  let component: SuggestedOffersTabsComponent;
  let fixture: ComponentFixture<SuggestedOffersTabsComponent>;
  let gaService: GoogleAnalyticsService;
  let offersService: OffersService;
  let mockModalRef: MockNgbModalRef = new MockNgbModalRef();

  function hasClass(element: HTMLElement, classMatcher: string) {
    return element.className.split(' ').indexOf(classMatcher) !== -1;
  }

  function selectOffer(tabType: string, selectedOfferIndex: number): void {
    const allOffersInTab: RateCategoryModel[] = component.offers.suggestedProducts[tabType].rateCategories;
    const selectedOffer = allOffersInTab[selectedOfferIndex];
    component.handleSelection(selectedOffer, allOffersInTab);
  }

  function verifySelectionState(offers: HTMLElement[], selectedOfferIndex: number): void {
    offers.forEach((offer, index) => {
      if (index === selectedOfferIndex) {
        expect(hasClass(offer, 'selected')).toBeTruthy();
      } else {
        expect(hasClass(offer, 'selected')).toBeFalsy();
      }
    });
  }

  function getSelectedRoomType(): string {
    const defaultBedType = component.offers.defaultBedType;
    const selectedRoomType = component.offers.suggestedProducts[defaultBedType].code;
    return selectedRoomType;
  }

  function triggerNgOnChange() {
    component.ngOnChanges({
      offers: new SimpleChange(null, cloneDeep(MOCK_SUGGESTED_OFFRS), false)
    });
    fixture.detectChanges();
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, NgbModule, BrowserAnimationsModule, HttpClientTestingModule],
      declarations: [SuggestedOffersTabsComponent, SuggestedOfferComponent, ConfirmationModalComponent],
      providers: [
        { provide: ModalHandlerService, useClass: MockModalHandlerService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: OffersService, useClass: OffersServiceMock },
        ReportIssueService,
        ConfirmationModalService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).overrideModule(BrowserDynamicTestingModule,
      { set: { entryComponents: [ConfirmationModalComponent] } });
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(SuggestedOffersTabsComponent);
    gaService = TestBed.get(GoogleAnalyticsService);
    component = fixture.componentInstance;
    component.offers = cloneDeep(MOCK_SUGGESTED_OFFRS);
    component.tabs = cloneDeep(suggestedOfferTabs);
    component.tabSet = new NgbTabset(new NgbTabsetConfig());
    component.searchCritera = cloneDeep(MOCK_SEARCHCRITERIA);
    component.originalReservationData = cloneDeep(MOCK_ORIGINAL_RESERV);
    spyOn(component.tabSet, 'select').and.callFake(() => { });
    triggerNgOnChange();
  });

  it('should create appropriate number of tabs', () => {
    const tabs = fixture.debugElement.queryAll(By.css('.nav-item'));
    expect(tabs.length).toEqual(component.tabs.length);
  });

  it('should render room counts for all tabs', () => {
    const roomCountElems = fixture.debugElement.queryAll(By.css('.room-count'));
    expect(roomCountElems.length).toEqual(component.tabs.length);
  });

  it('should render tabs in proper order', () => {
    const tabs = fixture.debugElement.queryAll(By.css('.nav-item'));
    component.tabs.forEach((tab, index) => {
      const linkId = tabs[index].query(By.css('.nav-link')).properties['id'];
      expect(tab.id).toEqual(linkId);
    });
  });

  it('should render max 4 offers in a tab', () => {
    const offerBox = fixture.debugElement.nativeElement.querySelector('.tab-pane.active').querySelectorAll('app-suggested-offer');
    expect(offerBox.length).toEqual(4);
  });

  it('should test defaultTab selection', () => {
    const expected = component.tabs.find(tab => tab.type === component.offers.defaultBedType).id;
    const selectedId = fixture.debugElement.query(By.css('.nav-link.active')).properties['id'];
    expect(selectedId).toEqual(expected);
  });

  it('should verify that the first offer is selected by default in each tab', () => {
    const firstOfferIndex = 0;
    const tabPanes: HTMLElement[] = Array.from(fixture.debugElement.nativeElement.querySelectorAll('.tab-pane'));
    tabPanes.forEach(tabPane => {
      const offers: HTMLElement[] = Array.from(tabPane.querySelectorAll('app-suggested-offer .offer-panel'));
      verifySelectionState(offers, firstOfferIndex);
    })
  });

  it('should verify that selected state is applied only to the selected offer', () => {
    const tabType = suggestedOfferTypes.TWO_BEDS;
    const selectedOfferIndex = 1;
    const selectedTabId = suggestedOfferTabs.find(tab => tab.type === tabType).id;
    selectOffer(tabType, selectedOfferIndex);
    fixture.detectChanges();
    const tabPane: HTMLElement = fixture.debugElement.nativeElement.querySelector(`#${selectedTabId}-panel`);
    const offers: HTMLElement[] = Array.from(tabPane.querySelectorAll('app-suggested-offer .offer-panel'));
    verifySelectionState(offers, selectedOfferIndex);
  });

  it('should verify that action buttons are displayed only for selected offer', () => {
    const tabType = suggestedOfferTypes.SUITE;
    const selectedOfferIndex = 2;
    const selectedTabId = suggestedOfferTabs.find(tab => tab.type === tabType).id;
    selectOffer(tabType, selectedOfferIndex);
    fixture.detectChanges();
    const tabPane: HTMLElement = fixture.debugElement.nativeElement.querySelector(`#${selectedTabId}-panel`);
    const offers: HTMLElement[] = Array.from(tabPane.querySelectorAll('app-suggested-offer'));
    offers.forEach((offer, index) => {
      if (index === selectedOfferIndex) {
        expect(offer.querySelector('.btn-wrap')).toBeTruthy();
      } else {
        expect(offer.querySelector('.btn-wrap')).toBeFalsy();
      }
    });
  });

  it('should verify that message is displayed if there are no offers for selected room type code in search', () => {
    expect(component.roomTypeNotMatchingAlert).toEqual(null);
  });

  it('should verify that message is NOT displayed if there are offers matching selected room type code in search', () => {
    component.searchCritera.roomType = getSelectedRoomType();
    triggerNgOnChange();
    expect(component.roomTypeNotMatchingAlert).toEqual(null);
  });

  it('should verify that message is displayed if there are no offers for room type code in original reservation and no room type selected', () => {
    component.searchCritera.roomType = '';
    triggerNgOnChange();
    expect(component.roomTypeNotMatchingAlert).not.toBeNull();
  });

  it('should verify that message is NOT displayed if there are offers matching room type code in original reservation and no room type selected', () => {
    component.searchCritera.roomType = '';
    component.originalReservationData.roomTypeCode = getSelectedRoomType();
    triggerNgOnChange();
    expect(component.roomTypeNotMatchingAlert).toEqual(null);
  });

  it('#handleOfferSelection should call proper modal handler service method with proper arguments', () => {
    const bedTypeOffer = component.offers.suggestedProducts.ONE_BED;
    const offer = bedTypeOffer.rateCategories[0];
    const service: MockModalHandlerService = TestBed.get(ModalHandlerService);
    const spiedMethod = spyOn(service, 'openWarningModal');
    component.reservationNumber = '123456';
    component.handleOfferSelection(offer, bedTypeOffer);
    expect(offer.parentProductRoomCode).toEqual(bedTypeOffer.code);
    expect(spiedMethod).toHaveBeenCalledWith(
      {
        searchCritera: MOCK_SEARCHCRITERIA,
        originalReservationData: MOCK_ORIGINAL_RESERV,
        reservationNumber: '123456',
        rateInfoAfter: offer
      }
    );
  });

  it('should call GoogleAnalytics with proper params when clicked on Select Offer', () => {
    const bedTypeOffer = component.offers.suggestedProducts.ONE_BED;
    const offer = bedTypeOffer.rateCategories[0];
    const spy = spyOn(gaService, 'trackEvent');
    component.handleOfferSelection(offer, bedTypeOffer);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith('Check In - Manage Stay', 'Suggested Offers', 'Offer Selected');
  });

  it('should NOT display no offers message when there are offers for a bed type', () => {
    const tabType = suggestedOfferTypes.ONE_BED;
    const tabId = suggestedOfferTabs.find(tab => tab.type === tabType).id;
    const tabEl: Element = fixture.debugElement.query(By.css(`#${tabId}-panel`)).nativeElement;
    const noOfferMsgEl = tabEl.querySelector('uic-user-guidance');
    expect(component.offers.suggestedProducts[tabType]).toBeDefined();
    expect(noOfferMsgEl).toBeNull();
  });

  it('should display no offers message when there are no offers for a bed type', () => {
    const tabType = suggestedOfferTypes.ONE_BED;
    const tabId = suggestedOfferTabs.find(tab => tab.type === tabType).id;
    const offers: SuggestedOffersModel = cloneDeep(MOCK_SUGGESTED_OFFRS);
    delete offers.suggestedProducts[tabType];
    component.offers = offers;
    component.offers.defaultBedType = tabType;
    triggerNgOnChange();
    const tabEl: Element = fixture.debugElement.query(By.css(`#${tabId}-panel`)).nativeElement;
    const noOfferMsgEl = tabEl.querySelector('uic-user-guidance');
    expect(noOfferMsgEl).not.toBeNull();
  });

  it('#handleTabChange should proeprly track tab selections', () => {
    const spy = spyOn(gaService, 'trackEvent');
    const evt: NgbTabChangeEvent = { nextId: 'oneBed' } as NgbTabChangeEvent;
    component.handleTabChange(evt);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith('Check In - Manage Stay', 'One Bed', 'Selected');
  });

  it('#handleTabChange should properly track tab selections for One Bed in Manage Stay', () => {
    const tabType = suggestedOfferTypes.ONE_BED;
    const spy = spyOn(gaService, 'trackEvent').and.callThrough();
    const spyManageStay = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const evt: NgbTabChangeEvent = { nextId: 'oneBed' } as NgbTabChangeEvent;
    component.handleTabChange(evt);
    expect(spy).toHaveBeenCalledWith('In House - Manage Stay', 'One Bed', 'Selected');
    expect(spyManageStay).toHaveBeenCalled();
  });

  it('#handleTabChange should properly track tab selections for Two Bed in Manage Stay', () => {
    const tabType = suggestedOfferTypes.TWO_BEDS;
    const spy = spyOn(gaService, 'trackEvent');
    const spyManageStay = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const evt: NgbTabChangeEvent = { nextId: 'twoBeds' } as NgbTabChangeEvent;
    component.handleTabChange(evt);
    expect(spy).toHaveBeenCalledWith('In House - Manage Stay', 'Two Beds', 'Selected');
    expect(spyManageStay).toHaveBeenCalled();
  });

  it('#handleTabChange should properly track tab selections for Suite in Manage Stay', () => {
    const tabType = suggestedOfferTypes.SUITE;
    const spy = spyOn(gaService, 'trackEvent');
    const spyManageStay = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const evt: NgbTabChangeEvent = { nextId: 'suite' } as NgbTabChangeEvent;
    component.handleTabChange(evt);
    expect(spy).toHaveBeenCalledWith('In House - Manage Stay', 'Suite', 'Selected');
    expect(spyManageStay).toHaveBeenCalled();
  });

  it('#handleTabChange should properly track tab selections for Others in Manage Stay', () => {
    const tabType = suggestedOfferTypes.OTHER;
    const spy = spyOn(gaService, 'trackEvent');
    const spyManageStay = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const evt: NgbTabChangeEvent = { nextId: 'other' } as NgbTabChangeEvent;
    component.handleTabChange(evt);
    expect(spy).toHaveBeenCalledWith('In House - Manage Stay', 'Other', 'Selected');
    expect(spyManageStay).toHaveBeenCalled();
  });

  it('#handleTabChange should proeprly track the scenario when bed type is NOT available', () => {
    const tabType = suggestedOfferTypes.ONE_BED;
    const offers: SuggestedOffersModel = cloneDeep(MOCK_SUGGESTED_OFFRS);
    delete offers.suggestedProducts[tabType];
    component.offers = offers;
    const spy = spyOn(gaService, 'trackEvent');
    const evt: NgbTabChangeEvent = { nextId: 'oneBed' } as NgbTabChangeEvent;
    component.handleTabChange(evt);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith('Check In - Manage Stay', 'Suggested Offers', 'Bed Type Not Available');
  });

  it('should call GoogleAnalytics with proper params when clicked on View Details', () => {
    const bedTypeOffer = component.offers.suggestedProducts.ONE_BED;
    const offer = bedTypeOffer.rateCategories[0];
    const service: MockModalHandlerService = TestBed.get(ModalHandlerService);
    const spy = spyOn(gaService, 'trackEvent');
    spyOn(service, 'openDescriptionModal').and.returnValue(mockModalRef);
    component.handleViewDetails(offer, bedTypeOffer);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith('Check In - Manage Stay', 'Suggested Offers', 'View Details Selected');
  });

  it('#handleViewDetails should call proper modal handler service method with proper arguments', () => {
    const bedTypeOffer = component.offers.suggestedProducts.ONE_BED;
    const offer = bedTypeOffer.rateCategories[0];
    const service: MockModalHandlerService = TestBed.get(ModalHandlerService);
    const spiedMethod = spyOn(service, 'openDescriptionModal').and.returnValue(mockModalRef);
    component.handleViewDetails(offer, bedTypeOffer);
    fixture.detectChanges();
    expect(spiedMethod).toHaveBeenCalledWith(
      {
        rateName: offer.name,
        rateCode: offer.code,
        roomName: bedTypeOffer.name,
        roomCode: bedTypeOffer.code,
        roomDesc: bedTypeOffer.description,
        pointsEligible: offer.pointsEligible,
        rateInfo: offer.rateInfo,
        arrivalDate: MOCK_SEARCHCRITERIA.arrivalDate,
        departureDate: MOCK_SEARCHCRITERIA.departureDate
      }
    );
  });

  it('#updateTabs should conditionally display Others tab', () => {
    const allTabs: SuggestedOffersTabsModel[] = cloneDeep(suggestedOfferTabs);
    const tabsExceptOthers = allTabs.filter(tab => tab.type !== suggestedOfferTypes.OTHER);
    component['updateTabs']();
    expect(component.tabs).toEqual(allTabs);
    delete component.offers.suggestedProducts.OTHER;
    component['updateTabs']();
    expect(component.tabs).toEqual(tabsExceptOthers);
  });

  it('Force Sell button should be displayed/added on the toast message', () => {
    const inHouseManageStay = false;
    const roomTypeNotMatchingAlert: Alert = {
      type: AlertType.Info,
      message: 'DUMMY is unavailable!',
      detailMessage: 'LBL_ROOM_TYPE_NOT_MATCHING_DETAIL_MSG',
      dismissible: false,
      animation: false,
      buttons: []
    };
    component.roomTypeNotMatchingAlert = roomTypeNotMatchingAlert;
    component.roomsForceSold = 0;
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert).toBeDefined();
    expect(component.roomTypeNotMatchingAlert).not.toBeNull();
    component['addForceSellButton'](inHouseManageStay);
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert.buttons[0].label).toEqual('BTN_FRCE_SELL');
 });

 it('display toast message on Force Sell button', () => {
   const inHouseManageStay = false;
   const roomTypeNotMatchingAlert: Alert = {
     type: AlertType.Info,
     message: 'DUMMY is unavailable!',
     detailMessage: 'LBL_ROOM_TYPE_NOT_MATCHING_DETAIL_MSG',
     dismissible: false,
     animation: false,
     buttons: []
   };
    component.roomTypeNotMatchingAlert = roomTypeNotMatchingAlert;
    component.roomsForceSold = 0;
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert).toBeDefined();
    expect(component.roomTypeNotMatchingAlert).not.toBeNull();
    component['addForceSellButton'](inHouseManageStay);
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert.buttons[0].label).toEqual('BTN_FRCE_SELL');
    expect(component.roomTypeNotMatchingAlert.detailMessage).toEqual('LBL_ROOM_TYPE_NOT_MATCHING_MSG_FRCE_SELL');
  });

  it('display Warning Modal after clicking the Force Sell button', () => {
    const inHouseManageStay = false;
    const roomTypeNotMatchingAlert: Alert = {
      type: AlertType.Info,
      message: 'DUMMY is unavailable!',
      detailMessage: 'LBL_ROOM_TYPE_NOT_MATCHING_DETAIL_MSG',
      dismissible: false,
      animation: false,
      buttons: []
    };
    component.roomTypeNotMatchingAlert = roomTypeNotMatchingAlert;
    component.roomsForceSold = 0;
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert).toBeDefined();
    expect(component.roomTypeNotMatchingAlert).not.toBeNull();
    component['addForceSellButton'](inHouseManageStay);
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert.buttons[0].label).toEqual('BTN_FRCE_SELL');
    component.roomTypeNotMatchingAlert.buttons[0].onClick();
  });

  it('should track GA events when force sell button is clicked on the modal confirmation', () => {
    const spy = spyOn(gaService, 'trackEvent');
    component['onSuccessForceSell']();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(gaSuggestedOffersConstants.CATEGORY,
      gaSuggestedOffersConstants.ACTION_MODAL_NO_ROOM_TYPE,
      gaSuggestedOffersConstants.LBL_FORCE_SELL_SELECTED);
  });

  it('should track GA events when force sell button is displayed on the toast message', () => {
    const inHouseManageStay = false;
    const roomTypeNotMatchingAlert: Alert = {
      type: AlertType.Info,
      message: 'DUMMY is unavailable!',
      detailMessage: 'LBL_ROOM_TYPE_NOT_MATCHING_DETAIL_MSG',
      dismissible: false,
      animation: false,
      buttons: []
    };

    component.roomTypeNotMatchingAlert = roomTypeNotMatchingAlert;
    component.roomsForceSold = 0;
    fixture.detectChanges();
    expect(component.roomTypeNotMatchingAlert).toBeDefined();
    expect(component.roomTypeNotMatchingAlert).not.toBeNull();
    const spy = spyOn(gaService, 'trackEvent');
    component['addForceSellButton'](inHouseManageStay);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(gaSuggestedOffersConstants.CATEGORY,
      gaSuggestedOffersConstants.ACTION_TOAST_NO_ROOM_TYPE,
      gaSuggestedOffersConstants.LBL_FORCE_SELL_VIEW);
  });

  it('should trigger #roomForceSold event emitter when after clicking the Force Sell button on confirmation modal', () => {
    component.roomTypeNotMatchingAlert = {
      type: AlertType.Info,
      message: 'DUMMY is unavailable!',
      detailMessage: 'LBL_ROOM_TYPE_NOT_MATCHING_DETAIL_MSG',
      dismissible: false,
      animation: false,
      buttons: []
    };
    component.roomsForceSold = 0;
    const confirm = new Subject<void>();
    const openForceSellWarningModalSpy = spyOn(TestBed.get(ForceSellService), 'forceSellWarningModal')
      .and.returnValue(confirm.asObservable());
    const roomForceSoldEmit = spyOn(component.roomForceSold, 'emit').and.callThrough();

    fixture.detectChanges();
    component['addForceSellButton'](false);
    component.roomTypeNotMatchingAlert.buttons[0].onClick();
    confirm.next();

    expect(openForceSellWarningModalSpy).toHaveBeenCalled();
    expect(roomForceSoldEmit).toHaveBeenCalled();
  });

});
