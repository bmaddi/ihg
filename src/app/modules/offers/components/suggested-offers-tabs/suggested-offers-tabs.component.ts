import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgbTabChangeEvent, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';

import { Alert, AlertButtonType, AlertType, GoogleAnalyticsService } from 'ihg-ng-common-core';

import { StayInformationModel, StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { BedTypeOfferModel, RateCategoryModel, SuggestedOffersModel, SuggestedOffersTabsModel } from '../../models/offers.models';
import { gaSuggestedOffersConstants, suggestedOfferTabs, suggestedOfferTypes } from '../../constants/suggested-offers.constant';
import { ModalHandlerService } from '../../services/modal-handler/modal-handler.service';
import { OffersService } from '../../services/offers.service';
import { ForceSellService } from '../../services/force-sell/force-sell.service';

@Component({
  selector: 'app-suggested-offers-tabs',
  templateUrl: './suggested-offers-tabs.component.html',
  styleUrls: ['./suggested-offers-tabs.component.scss']
})
export class SuggestedOffersTabsComponent implements OnChanges {

  public tabs: SuggestedOffersTabsModel[] = cloneDeep(suggestedOfferTabs);
  public defaultTab = suggestedOfferTabs[0].id;
  public maxOfferCount = 4;
  public overbookingValue: number;
  public roomsForceSold: number;
  public roomTypeNotMatchingAlert: Alert;

  @Input() offers: SuggestedOffersModel;
  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: StayInformationModel;
  @Input() reservationNumber: string;
  @Output() estimatedTotal = new EventEmitter<string>();
  @Output() roomForceSold = new EventEmitter<void>();

  @ViewChild('tabSet') tabSet: NgbTabset;

  constructor(private forceSellService: ForceSellService,
    private gaService: GoogleAnalyticsService,
    private modalService: ModalHandlerService,
    private offersService: OffersService,
    private translate: TranslateService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.offers && changes.offers.currentValue) {
      this.updateTabs();
      this.setRoomTypeNotMatchingAlert();
      this.checkForceSell();
      const tabType = this.tabs.find(tab => tab.type === this.offers.defaultBedType);
      this.defaultTab = tabType ? tabType.id : suggestedOfferTabs[0].id;
      this.tabSet.select(this.defaultTab);
      this.handleInitialOfferSelection();
    }
  }

  private checkForceSell() {
    const inHouseManageStay = this.offersService.checkForManageStayEdit();
    this.forceSellService.getForceSell(this.getSelectedRoomType()).subscribe(res => {
      if (res) {
        this.roomsForceSold = res.roomsForceSold;
        const numberOfRooms = this.searchCritera ? Number(this.searchCritera.numberOfRooms) : this.originalReservationData.numberOfRooms;
        this.overbookingValue = res.roomsForceSold - numberOfRooms;
        this.addForceSellButton(inHouseManageStay);
      }
    });
  }

  private addForceSellButton(inHouseManageStay: boolean) {
    if (!!this.roomTypeNotMatchingAlert && this.roomsForceSold <= 0 && !inHouseManageStay) {
      this.roomTypeNotMatchingAlert.detailMessage = 'LBL_ROOM_TYPE_NOT_MATCHING_MSG_FRCE_SELL';
      this.gaService.trackEvent(gaSuggestedOffersConstants.CATEGORY, gaSuggestedOffersConstants.ACTION_TOAST_NO_ROOM_TYPE, gaSuggestedOffersConstants.LBL_FORCE_SELL_VIEW);
      this.roomTypeNotMatchingAlert.buttons = [{
        label: 'BTN_FRCE_SELL',
        type: AlertButtonType.Info,
        onClick: () => {
          this.forceSellService.forceSellWarningModal(this.getSelectedRoomType(), this.overbookingValue)
            .subscribe(() => {
              this.onSuccessForceSell();
              this.roomForceSold.emit();
            });
        }
      }];
    }
  }

  private onSuccessForceSell() {
    this.gaService.trackEvent(gaSuggestedOffersConstants.CATEGORY, gaSuggestedOffersConstants.ACTION_MODAL_NO_ROOM_TYPE, gaSuggestedOffersConstants.LBL_FORCE_SELL_SELECTED);
  }

  private updateTabs(): void {
    const allTabs = cloneDeep(suggestedOfferTabs);
    this.tabs = this.offers.suggestedProducts.OTHER ? allTabs :
      allTabs.filter(tab => tab.type !== suggestedOfferTypes.OTHER);
  }

  public handleInitialOfferSelection(): void {
    this.tabs.forEach(tab => {
      if (this.offers.suggestedProducts[tab.type]) {
        const offers = (this.offers.suggestedProducts[tab.type] as BedTypeOfferModel).rateCategories;
        offers[0].selected = true;
      }
    });
  }

  public handleViewDetails(offer: RateCategoryModel, bedTypeOffer: BedTypeOfferModel): void {
    this.checkGAForManageStay(gaSuggestedOffersConstants.ACTION_SUGGESTED_OFFER, gaSuggestedOffersConstants.LABEL_VIEW_DETAIL_SELECTED);
    this.modalService.openDescriptionModal({
      rateName: offer.name,
      rateCode: offer.code,
      roomName: bedTypeOffer.name,
      roomCode: bedTypeOffer.code,
      roomDesc: bedTypeOffer.description,
      pointsEligible: offer.pointsEligible,
      rateInfo: offer.rateInfo,
      arrivalDate: this.searchCritera.arrivalDate,
      departureDate: this.searchCritera.departureDate
    }).result.then(select => {
      this.handleOfferSelection(offer, bedTypeOffer);
    }, cancel => { });
  }

  public handleOfferSelection(offer: RateCategoryModel, bedTypeOffer: BedTypeOfferModel): void {
    this.checkGAForManageStay(gaSuggestedOffersConstants.ACTION_SUGGESTED_OFFER,
      gaSuggestedOffersConstants.LABEL_OFFER_SELECTED);
    offer.parentProductRoomCode = bedTypeOffer.code;
    this.modalService.openWarningModal({
      searchCritera: this.searchCritera,
      originalReservationData: this.originalReservationData,
      reservationNumber: this.reservationNumber,
      rateInfoAfter: offer
    });
    this.estimatedTotal.emit(offer.rateInfo.totalAmountAfterTax);
  }

  private getDefaultBedRoomType(): string {
    const bedTypeOffer = this.offers.suggestedProducts[this.offers.defaultBedType];
    return bedTypeOffer ? bedTypeOffer.code : '';
  }

  private getSelectedRoomType(): string {
    return this.searchCritera.roomType || this.originalReservationData.roomTypeCode;
  }

  private setRoomTypeNotMatchingAlert(): void {
    this.roomTypeNotMatchingAlert = (this.getDefaultBedRoomType() === this.getSelectedRoomType()) ? null : {
      type: AlertType.Info,
      message: this.translate.instant('LBL_ROOM_TYPE_NOT_MATCHING_MSG', { 'roomTypeCode': this.getSelectedRoomType() }),
      detailMessage: 'LBL_ROOM_TYPE_NOT_MATCHING_DETAIL_MSG',
      dismissible: false,
      animation: false,
      buttons: []
    };
    if (this.roomTypeNotMatchingAlert !== null) {
      if (this.offersService.checkForManageStayEdit()) {
        this.gaService.trackEvent(gaSuggestedOffersConstants.CATEGORY_MANAGE_STAY, gaSuggestedOffersConstants.ACTION_SUGGESTED_OFFER, gaSuggestedOffersConstants.LABEL_ROOM_TYPE_NOT_AVAIL);
      } else {
        this.gaService.trackEvent(gaSuggestedOffersConstants.CATEGORY, gaSuggestedOffersConstants.ACTION_SUGGESTED_OFFER, gaSuggestedOffersConstants.LABEL_ROOM_TYPE_NOT_AVAIL);
      }
    }
  }

  public handleSelection(offer: RateCategoryModel, allOffers: RateCategoryModel[]): void {
    allOffers.forEach(offer => offer.selected = false);
    offer.selected = true;
  }

  handleTabChange(event: NgbTabChangeEvent): void {
    const currentTab = this.tabs.find(tab => tab.id === event.nextId);
    if (!this.offers.suggestedProducts[currentTab.type]) {
      this.checkGAForManageStay(gaSuggestedOffersConstants.ACTION_SUGGESTED_OFFER,
        gaSuggestedOffersConstants.LABEL_BED_TYPE_NOT_AVAIL);
    }
    this.checkGAForManageStay(currentTab.gaAction, gaSuggestedOffersConstants.LABEL_SELECTED);
  }

  private trackGAEvents(category: string, action: string, label: string) {
    this.gaService.trackEvent(category, action, label);
  }

  private checkGAForManageStay(action, label) {
    if (!this.offersService.checkForManageStayEdit()) {
      this.trackGAEvents(gaSuggestedOffersConstants.CATEGORY, action,
        label);
    } else {
      this.trackGAEvents(gaSuggestedOffersConstants.CATEGORY_MANAGE_STAY, action,
        label);
    }
  }
}
