import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';
import { WarningModalComponent } from '@modules/offers/components/warning-modal/warning-modal.component';
import { OffersService } from '@modules/offers/services/offers.service';
import { of, throwError } from 'rxjs';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';

import { AverageRateComponent } from './../average-rates-modal/avg-rate.component';
import { AppSharedModule } from '@app-shared/app-shared.module';
import * as cloneDeep from 'lodash/cloneDeep';

import { MOCK_ORIGINAL_RESERVATION_DATA, MOCK_RATEINFO_AFTER, MOCK_SEARCH_CRITERIA, MOCK_UPDATE_TOTALS_RESPONSE} from '@app/modules/offers/mocks/warning-modal.mock';
import { mockRoomConflicts } from '@modules/offers/mocks/room-conflicts.mock';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { ConflictView } from '@modules/offers/enums/conflict-view.enum';
import { isEqual } from 'lodash';
import { UserService } from 'ihg-ng-common-core';
import { USER_MOCK_VALID_OVERRIDE_ROLES, USER_MOCK_INVALID_OVERRIDE_ROLES, USER_MOCK_UNDEFINED_ROLES } from '../../mocks/user-mock';
import { MOCK_UPDATE_TOTALS_ERROR } from '../../mocks/warning-modal.mock';
import { UPDATE_TOTALS_SAVE_ERROR, MANAGE_STAY_SHOP_OFFERS } from '../../../../constants/error-autofill-constants';

describe('WarningModalComponent', () => {
  let component: WarningModalComponent;
  let fixture: ComponentFixture<WarningModalComponent>;

  function clickUpdateTotals() {
    const updateTotalsBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="UpdateTotal-SID"]')).nativeElement;
    updateTotalsBtn.click();
    fixture.detectChanges();
  }

  function clickCancel() {
    const cancelBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="Cancel-SID"]')).nativeElement;
    cancelBtn.click();
    fixture.detectChanges();
  }

  function isEllipsisActive(textElmWidth, toolTipContainerWidth) {
    return (textElmWidth > toolTipContainerWidth);
  }

  function initialiseComponent() {
    fixture = TestBed.createComponent(WarningModalComponent);
    component = fixture.componentInstance;
    component.reservationNumber = '44002733';
    component.searchCritera = cloneDeep(MOCK_SEARCH_CRITERIA);
    component.originalReservationData = cloneDeep(MOCK_ORIGINAL_RESERVATION_DATA);
    component.rateInfoAfter = cloneDeep(MOCK_RATEINFO_AFTER);
    component.action = 'Products';
  }

  function isOriginalGuestNameTruncated() {
    const originalGuestNameTextElm = fixture.debugElement.query(By.css('#originalGuestName .truncate-text__container span'));
    const tooltipContainerElm = fixture.debugElement.query(By.css('#originalGuestName .truncate-text__container'));
    const result = isEllipsisActive(originalGuestNameTextElm.nativeElement.offsetWidth, tooltipContainerElm.nativeElement.offsetWidth);
    return result;
  }

  function isRequestedGuestNameTruncated() { 
    const requestedGuestNameTextElm = fixture.debugElement.query(By.css('#requestedGuestName .truncate-text__container span'));
    const reqstTooltipContainerElm = fixture.debugElement.query(By.css('#requestedGuestName .truncate-text__container'));
    const result = isEllipsisActive(requestedGuestNameTextElm.nativeElement.offsetWidth,
      reqstTooltipContainerElm.nativeElement.offsetWidth);
    return result;
  }

  function openAvgNightlyForm() {
    const isEditStay = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(false);
    openAvgNightlyFormWithoutSpy();
  }

  function openAvgNightlyFormWithoutSpy() {
    component.isEligibleForRateOverride = true;
    component.openInEditableMode = false;
    fixture.detectChanges();
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideBtn-SID"]')).nativeElement;
    overrideButton.click();
    fixture.detectChanges();
  }

  function setUpAvgNighlyFormAsDirty() {
    openAvgNightlyForm();
    component.avgRateComp.avgNightlyRateForm.markAsDirty();
    fixture.detectChanges();
  }

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
  });

  beforeEach(async(() => {
    TestBed
      .overrideProvider(ReportIssueService, {
        useValue: {}
      })
      .configureTestingModule({
        imports: [ CommonTestModule, AppSharedModule, FormsModule, ReactiveFormsModule ],
        declarations: [ WarningModalComponent, AverageRateComponent,  ],
        providers: [ NgbActiveModal, ReportIssueService, FormBuilder,
          {provide: GoogleAnalyticsService, useValue: googleAnalyticsStub} ]
      })
      .compileComponents().then(() => {
      setTestEnvironment();
    });
  }));

  beforeEach(() => {
    initialiseComponent();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the modal on clicking x', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.close')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  xit('check if on modal close pending reservation changes are reverted to original reservation values', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const ignorePendingReservationSpy = spyOn(component.offersService, 'ignorePendingReservation').and.returnValue("SUCCESS");
    component.isPendingReservationCreated = true;
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('.close')).nativeElement;
    element.click();
    const checkIfObjsEqual = isEqual(component.rateInfoAfter.rateInfo, component.originalRateInfo);
    expect(checkIfObjsEqual).toBeTruthy();
    expect(ignorePendingReservationSpy).toHaveBeenCalled();
  });

  it('modal title should have offers not available', () => {    
    component.openInEditableMode = false;
    component.ngOnInit();
    fixture.detectChanges();
    const warningModalTitle = fixture.debugElement.query(By.css('[data-slnm-ihg="WarningHeader-SID"] span')).nativeElement;
    expect(warningModalTitle.textContent.trim()).toBe('LBL_WARNING_MODAL_TITLE');
  });

  it('should close the modal on clicking close', () => {
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const element = fixture.debugElement.query(By.css('.btn-default')).nativeElement;
    expect(element).not.toBeNull();
    element.click();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('should check for conflicts if when is editing stay while on Manage Stay Page and is an room extension', () => {
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(false);
    const isExtendedDuration = spyOn<any>(component, 'isExtendedDuration').and.returnValue(true);
    const activeModalSpy = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(null));
    const successSpy = spyOn<any>(component, 'handleSuccessfulConflictsResponse').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    component.continue();

    expect(isRoomTypeChanged).toHaveBeenCalled();
    expect(isExtendedDuration).toHaveBeenCalled();
    expect(activeModalSpy).toHaveBeenCalled();
    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(successSpy).toHaveBeenCalled();
  });

  it('should proceed with #proceedWithChangeConfirmation when is editing stay not an In House- Edit Stay ', () => {
    const checkForManageStayEditSpy = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(false);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(null));
    const changeConfirmationSpy = spyOn<any>(component, 'proceedWithChangeConfirmation').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    component.continue();

    expect(checkForManageStayEditSpy).toHaveBeenCalled();
    expect(getRoomConflictsSpy).not.toHaveBeenCalled();
    expect(changeConfirmationSpy).toHaveBeenCalled();
  });

  it('should proceed with #openConflictsModal when room type is changed', () => {
    const isEditStay = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(true);
    const isExtendedDuration = spyOn<any>(component, 'isExtendedDuration').and.returnValue(true);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(null));

    component.ngOnInit();
    fixture.detectChanges();
    component.continue();

    expect(isRoomTypeChanged).toHaveBeenCalled();
    expect(isExtendedDuration).not.toHaveBeenCalled();
    expect(isEditStay).toHaveBeenCalled();
    expect(getRoomConflictsSpy).not.toHaveBeenCalled();
  });

  // tslint:disable-next-line:max-line-length
  it('should call #proceedWithChangeConfirmation when editing stay while on Manage Stay Page and is not an extended duration nor room type change', () => {
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(false);
    const isExtendedDuration = spyOn<any>(component, 'isExtendedDuration').and.returnValue(false);

    const checkForManageStayEditSpy = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(null));
    const proceedWithChangeConfirmationSpy = spyOn<any>(component, 'proceedWithChangeConfirmation').and.callThrough();

    component.ngOnInit();
    component.continue();
    fixture.detectChanges();

    expect(checkForManageStayEditSpy).toHaveBeenCalled();
    expect(getRoomConflictsSpy).not.toHaveBeenCalled();
    expect(proceedWithChangeConfirmationSpy).toHaveBeenCalled();
  });

  // tslint:disable-next-line:max-line-length
  it('should proceed with #openConflictsModal when is editing In House while on Manage Stay Page and has room conflicts', () => {
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(false);
    const isExtendedDuration = spyOn<any>(component, 'isExtendedDuration').and.returnValue(true);
    const activeModalSpy = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(mockRoomConflicts));
    const triggerConflictsModalSpy = spyOn(TestBed.get(OffersService), 'triggerConflictsModal').and.callThrough();
    const dismissSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    component.continue();

    expect(activeModalSpy).toHaveBeenCalled();
    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(triggerConflictsModalSpy).toHaveBeenCalled();
    expect(dismissSpy).toHaveBeenCalled();
  });

  it('should pop up warning message when found error on warning modal component', () => {
    const checkForManageStayEditSpy = spyOn(TestBed.get(OffersService), 'checkForManageStayEdit').and.returnValue(true);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(throwError({status: 500}));
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(false);
    const isExtendedDuration = spyOn<any>(component, 'isExtendedDuration').and.returnValue(true);
    const confirmChangesError = spyOn<any>(component['confirmChangesError'], 'next').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    component.continue();

    expect(isRoomTypeChanged).toHaveBeenCalled();
    expect(isExtendedDuration).toHaveBeenCalled();
    expect(checkForManageStayEditSpy).toHaveBeenCalled();
    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(confirmChangesError).toHaveBeenCalled();
  });

  it('Verify if Guest Name is bigger than container width then text is truncated', () => {
    const result = isOriginalGuestNameTruncated();
    const result1 = isRequestedGuestNameTruncated();
    expect(result1).toBeTruthy();
    expect(result).toBeTruthy();
  });

  it('Verify if Guest Name is smaller than container width then text is not truncated', () => {
    component.searchCritera.guestInfo.firstName = 'A';
    component.originalReservationData.guestInfo.firstName = 'A';
    component.ngOnInit();
    fixture.detectChanges();
    const result = isOriginalGuestNameTruncated();
    const result1 = isRequestedGuestNameTruncated();
    expect(result).toBeFalsy();
    expect(result1).toBeFalsy();
  });

  // tslint:disable-next-line:max-line-length
  it('should validate when the user confirms selection and is changing room type then the user will be prompted with the manual room assignment modal for In-House Edit Stay', () => {
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(true);
    const getConfirmationPayload = spyOn<any>(component, 'getConfirmationPayload').and.returnValue({roomTypeCode: 'SDXC'});
    const openConflictsModal = spyOn<any>(component, 'openConflictsModal').and.callThrough();
    const triggerConflictsModal = spyOn(TestBed.get(OffersService), 'triggerConflictsModal').and.callThrough();
    component['isInHouseEditStay'] = true;
    fixture.detectChanges();

    component.continue();

    expect(isRoomTypeChanged).toHaveBeenCalled();
    expect(getConfirmationPayload).toHaveBeenCalled();
    expect(triggerConflictsModal).toHaveBeenCalled();
    expect(openConflictsModal).toHaveBeenCalledWith(ConflictView.manualAssign, false);
  });

  it('should check text in table is highlighted when changes are detected in reservation', () => {
    const requestedDateTitleElement = fixture.debugElement.query(By.css('[data-slnm-ihg="ReservationTitle3-SID"]')).nativeElement;
    expect(requestedDateTitleElement).toBeDefined();
    expect(requestedDateTitleElement.textContent).toContain('LBL_GST_DEPARTURE');
    component.formattedData.forEach((data, index) => {
      const requestedDateChangeElement = fixture.debugElement.query(By.css('[data-slnm-ihg="requestedChange' + index + '-SID"]')).nativeElement;
      expect(requestedDateChangeElement).toBeDefined();
      if (data.originalReservation !== data.requestedChange) {
        expect(requestedDateChangeElement.getAttribute('class')).toContain('highlight-cell');
      }
    });

    const requestedAvgRateElement = fixture.debugElement.query(By.css('[data-slnm-ihg="AverageAfter-SID"]')).nativeElement;
    expect(requestedAvgRateElement).toBeDefined();
    expect(requestedAvgRateElement.getAttribute('class')).toContain('highlight-cell');


    const totalElement = fixture.debugElement.query(By.css('[data-slnm-ihg="TotalAmoutAfterTax-SID"]')).nativeElement;
    expect(totalElement).toBeDefined();
    expect(totalElement.getAttribute('class')).toContain('highlight-cell');
  });

  it('should open cut key modal for in house manage stay after offer is changed w/ no conflicts', () => {
    const isRoomTypeChanged = spyOn<any>(component, 'isRoomTypeChanged').and.returnValue(false);
    const isExtendedDurationSpy = spyOn<any>(component, 'isExtendedDuration').and.returnValue(true);
    const getRoomConflictsSpy = spyOn(TestBed.get(OffersService), 'getRoomConflicts').and.returnValue(of(null));
    const confirmChangeReservationSpy = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(of(
      { status: 'SUCCESS' }));
    const handleChangeReservationSpy = spyOn<any>(component, 'handleChangeReservationResponse').and.callThrough();
    const openCutKeySpy = spyOn<any>(component, 'openCutKey').and.callThrough();

    component['isInHouseEditStay'] = true;
    fixture.detectChanges();
    component.continue();

    expect(isRoomTypeChanged).toHaveBeenCalled();
    expect(isExtendedDurationSpy).toHaveBeenCalled();
    expect(getRoomConflictsSpy).toHaveBeenCalled();
    expect(confirmChangeReservationSpy).toHaveBeenCalled();
    expect(handleChangeReservationSpy).toHaveBeenCalled();
    expect(openCutKeySpy).toHaveBeenCalled();
  });

  it('should display rate override button if eligibleForRateOverride is true', () => {
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideBtn-SID"]')).nativeElement;
    expect(overrideButton).toBeDefined();

  });

  it('should not display rate override button if eligibleForRateOverride is false', () => {
    component.isEligibleForRateOverride = false;
    fixture.detectChanges();
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideBtn-SID"]'));
    expect(overrideButton).toBeNull();
  });

  it('check if expandRowInEditableMode method is called', () => {
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideBtn-SID"]')).nativeElement;
    const spyExpandRowInEditableMode = spyOn(component, 'expandRowInEditableMode');
    overrideButton.click();
    fixture.detectChanges();
    expect(spyExpandRowInEditableMode).toHaveBeenCalled();
  });

  it('expandRowInEditableMode should expand the average nightly rate section', () => {
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();
    component.expandRowInEditableMode();
    fixture.detectChanges();
    expect(component.isRowExpanded).toBeTruthy();
    expect(component.openInEditableMode).toBeTruthy();
  });

  it('check warning modal title is "LBL_WARNING_MODAL_UPDATE_TITLE" when rate override button is clicked', () => {
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();
    const warningModalTitle = fixture.debugElement.query(By.css('[data-slnm-ihg="WarningHeader-SID"] span')).nativeElement;
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideBtn-SID"]')).nativeElement;
    overrideButton.click();
    fixture.detectChanges();
    expect(warningModalTitle.textContent.trim()).toBe('LBL_WARNING_MODAL_UPDATE_TITLE');
  });

  it('check warning modal title is "LBL_WARNING_MODAL_TITLE" when rate override button is not clicked', () => {
    const warningModalTitle = fixture.debugElement.query(By.css('[data-slnm-ihg="WarningHeader-SID"] span')).nativeElement;
    expect(warningModalTitle.textContent.trim()).toBe('LBL_WARNING_MODAL_TITLE');
  });

  it('check if TotalAmoutAfterTax column is showing pending text if avgNightlyRateForm is dirty', () => {
    setUpAvgNighlyFormAsDirty();
    const totalAmtAfterTaxElm = fixture.debugElement.query(By.css('[data-slnm-ihg="TotalAmoutAfterTax-SID"] span')).nativeElement;
    expect(totalAmtAfterTaxElm).toBeDefined();
    expect(totalAmtAfterTaxElm.textContent.trim()).toBe('LBL_PENDING');
  });

  it('check if TotalAmoutAfterTax column is not highlighted if avgNightlyRateForm is dirty', () => {
    setUpAvgNighlyFormAsDirty();
    const totalAmtAfterTaxElm = fixture.debugElement.query(By.css('[data-slnm-ihg="TotalAmoutAfterTax-SID"]')).nativeElement;
    const elmClassList = totalAmtAfterTaxElm.attributes.class.ownerElement.classList;
    expect(elmClassList.contains('highlight-cell')).toBeFalsy();
  });

  it('check if update totals button text is correct', () => {
    openAvgNightlyForm();
    const updateTotalsBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="UpdateTotal-SID"]')).nativeElement;
    expect(updateTotalsBtn.textContent.trim()).toBe('LBL_UPDATE_TOTAL_BTN')
  });

  it('check if update totals button is displayed if rate overrride button is clicked', () => {
    openAvgNightlyForm();
    const updateTotalsBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="UpdateTotal-SID"]')).nativeElement;
    expect(updateTotalsBtn).toBeDefined();
  });

  it('check if confirm changes button is not displayed if rate overrride button is clicked', () => {
    openAvgNightlyForm();
    const confirmBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="Confirm-SID"]'));
    expect(confirmBtn).toBeNull();
  });

  it('check if confirm changes button is displayed if rate overrride button is not clicked', () => {
    const confirmBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="Confirm-SID"]')).nativeElement;
    expect(confirmBtn).toBeDefined();
  });

  it('check if update totals button is not displayed if rate overrride button is not clicked', () => {
    const updateTotalsBtn = fixture.debugElement.query(By.css('[data-slnm-ihg="UpdateTotal-SID"]'));
    expect(updateTotalsBtn).toBeNull();
  });

  it('check if updateTotals method is called if "Update Totals" button is clicked', () => {
    openAvgNightlyForm();
    const updateTotalsSpy = spyOn(component, 'updateTotals');
    clickUpdateTotals();
    expect(updateTotalsSpy).toHaveBeenCalled();
  });

  it('check if "checkAndUpdateTotals","continue" method is called if update totals button is clicked and avgNightlyRateForm is dirty', () => {
    openAvgNightlyForm();
    const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(true);
    const continueSpy = spyOn(component, 'continue');
    component.avgRateComp.nightlyRates.at(Number(0)).get('indvNightlyRate').patchValue("1123.20");
    component.avgRateComp.avgNightlyRateForm.markAsDirty();
    fixture.detectChanges();
    clickUpdateTotals();
    expect(checkAndUpdateTotalsSpy).toHaveBeenCalled();
    expect(continueSpy).toHaveBeenCalled();
  });

  it('check if "continue" method is not called if update totals button is clicked and avgNightlyRateForm is dirty and the field values have unchanged values', () => {
    openAvgNightlyForm();
    const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(false);
    const continueSpy = spyOn(component, 'continue');
    component.avgRateComp.avgNightlyRateForm.markAsDirty();
    fixture.detectChanges();
    clickUpdateTotals();
    expect(checkAndUpdateTotalsSpy).toHaveBeenCalled();
    expect(continueSpy).not.toHaveBeenCalled();
  });

  it('check if "checkAndUpdateTotals","continue" method is not called if update totals button is clicked and avgNightlyRateForm is pristine', () => {
    openAvgNightlyForm();
    const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals');
    const continueSpy = spyOn(component, 'continue');
    component.avgRateComp.avgNightlyRateForm.markAsPristine();
    clickUpdateTotals();
    expect(checkAndUpdateTotalsSpy).not.toHaveBeenCalled();
    expect(continueSpy).not.toHaveBeenCalled();
  });

  it('check "continue" method when update totals button is clicked and avgNightlyRateForm is dirty', () => {
    fixture.detectChanges();
    openAvgNightlyForm();
    const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(true);
    const confirmChangeReservationSpy = spyOn(component.offersService, 'confirmChangeReservation').and.returnValue(of(MOCK_UPDATE_TOTALS_RESPONSE));
    component.pendingReservation = true;
    fixture.detectChanges();
    component.continue();
    const checkEquality = isEqual(component.rateInfoAfter.rateInfo, MOCK_UPDATE_TOTALS_RESPONSE.reservationData.rateOverrideInfo);
    expect(checkEquality).toBeTruthy();
  });

  it('Error on clicking Update Total', () => {
    fixture.detectChanges();
    openAvgNightlyForm();
    const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(true);
    const restorePrevDataSpy = spyOn(component.avgRateComp, 'restorePrevData');
    const confirmChangeReservationSpy = spyOn(component.offersService, 'confirmChangeReservation').and.returnValue(of(MOCK_UPDATE_TOTALS_ERROR));
    component.pendingReservation = true;
    fixture.detectChanges();
    component.continue();
    const updateTotatlsErrorElm = fixture.debugElement.query(By.css('.updateTotals-error'));
    expect(updateTotatlsErrorElm.nativeElement).toBeDefined();
    expect(restorePrevDataSpy).toHaveBeenCalled();
  });

  it('check if Report an Issue modal values are correct on update totals failure', () => {
    const updateReportIssueSubject = UPDATE_TOTALS_SAVE_ERROR(component.reservationNumber);
    expect(component.updateTotalsReportIssue.genericError.subject).toEqual(updateReportIssueSubject.genericError.subject);
    expect(component.updateTotalsReportIssue.genericError.function).toEqual(MANAGE_STAY_SHOP_OFFERS.function);
    expect(component.updateTotalsReportIssue.genericError.topic).toEqual(MANAGE_STAY_SHOP_OFFERS.topic);
    expect(component.updateTotalsReportIssue.genericError.subtopic).toEqual(MANAGE_STAY_SHOP_OFFERS.subtopic);
  });

  xit('check if rate override changes are cancelled when user clicks cancel in non-editable mode', () => {
    const ignorePendingReservationSpy = spyOn(component.offersService, 'ignorePendingReservation').and.returnValue("SUCCESS");
    component.isPendingReservationCreated = true;
    clickCancel();
    const checkIfObjsEqual = isEqual(component.rateInfoAfter.rateInfo, component.originalRateInfo);
    expect(ignorePendingReservationSpy).toHaveBeenCalled();
    expect(checkIfObjsEqual).toBeTruthy();
  });

  xit('check if average nighly rate values are cancelled back to previous value when user clicks cancel in editable mode', () => {
    openAvgNightlyForm();
    component.avgRateComp.nightlyRates.at(Number(0)).get('indvNightlyRate').patchValue("1123.20");
    component.avgRateComp.avgNightlyRateForm.markAsDirty();
    const avgRateCompSpy = spyOn(component.avgRateComp, 'cancelChanges');
    fixture.detectChanges();
    clickCancel();
    expect(avgRateCompSpy).toHaveBeenCalled();
    expect(component.openInEditableMode).toBeFalsy();
  });

  xit('Clicking Cancel in editable mode - pending res not yet created in quick override flow', () => {
    openAvgNightlyForm();
    const activeModalSpy = spyOn(TestBed.get(NgbActiveModal), 'dismiss').and.callThrough();
    const ignorePendingReservationSpy = spyOn(component.offersService, 'ignorePendingReservation').and.returnValue("SUCCESS");
    component.avgRateComp.nightlyRates.at(Number(0)).get('indvNightlyRate').patchValue("1123.20");
    component.avgRateComp.avgNightlyRateForm.markAsDirty();
    component.isPendingReservationCreated = false;
    component.isOpenInEditableMode = true
    fixture.detectChanges();
    clickCancel();
    expect(ignorePendingReservationSpy).not.toHaveBeenCalled();
    expect(activeModalSpy).toHaveBeenCalled();
  });

  it('check if info message is displayed and is in viewport when update totals button is clicked and avgNightlyRateForm is dirty', async(() => {
    fixture.whenRenderingDone().then(() => {
      openAvgNightlyForm();
      const scrollIntoViewSpy = spyOn(document.querySelector('.modal-content'), 'scrollIntoView');
      const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(true);
      const confirmChangeReservationSpy = spyOn(component.offersService, 'confirmChangeReservation').and.returnValue(of(MOCK_UPDATE_TOTALS_RESPONSE));
      component.pendingReservation = true;
      fixture.detectChanges();
      component.continue();
      const infoMsgElm = fixture.debugElement.query(By.css('[data-slnm-ihg="RatesEditedToast-SID"]'));
      expect(infoMsgElm).toBeDefined();
      expect(scrollIntoViewSpy).toHaveBeenCalled();
    });
  }));

  it('check if info message is not displayed after rate override button is clicked', async(() => {
    fixture.whenRenderingDone().then(() => {
      openAvgNightlyForm();
      const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(true);
      const confirmChangeReservationSpy = spyOn(component.offersService, 'confirmChangeReservation').and.returnValue(of(MOCK_UPDATE_TOTALS_RESPONSE));
      clickUpdateTotals();
      openAvgNightlyFormWithoutSpy();
      const infoMsgElm = fixture.debugElement.query(By.css('[data-slnm-ihg="RatesEditedToast-SID"]'));
      expect(infoMsgElm).toBeNull();
    });
  }));

  it('check if info message is displayed when update totals button is clicked and rates are not edited', async(() => {
    fixture.whenRenderingDone().then(() => {
      openAvgNightlyForm();
      const checkAndUpdateTotalsSpy = spyOn(component.avgRateComp, 'checkAndUpdateTotals').and.returnValue(false);
      component.avgRateComp.avgNightlyRateForm.markAsPristine();
      clickUpdateTotals();
      const infoMsgElm = fixture.debugElement.query(By.css('[data-slnm-ihg="RatesEditedToast-SID"]'));
      expect(infoMsgElm).toBeNull();
    });    
  }));  

  it('check trackEvent method is called when update totals button is clicked and avgNightlyRateForm is dirty', () => {
    openAvgNightlyForm();
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');
    component.avgRateComp.avgNightlyRateForm.markAsDirty();
    clickUpdateTotals();
    expect(trackEventSpy).toHaveBeenCalled();
  });

  it('check trackEvent method is called when "continue" method is called and pendingReservation is false', () => {
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');
    component.continue();
    fixture.detectChanges();
    expect(trackEventSpy).toHaveBeenCalled();
  });

  it('check trackEvent method is called when clicking on rate override', () => {
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');
    component.isEligibleForRateOverride = true;
    fixture.detectChanges();    
    const overrideButton = fixture.debugElement.query(By.css('[data-slnm-ihg="RateOverrideBtn-SID"]')).nativeElement;
    overrideButton.click();
    fixture.detectChanges();
    expect(trackEventSpy).toHaveBeenCalledWith('Check In - Manage Stay', 'Confirmation Modal', 'Rate Override Selected');
  });

  it('check trackEvent method is called when clicking on confirm changes after rate override', () => {
    fixture.detectChanges();
    component.isRateOverride = true;
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');
    component.continue();
    fixture.detectChanges();
    expect(trackEventSpy).toHaveBeenCalledWith('Check In - Manage Stay', 'Confirm Changes', 'Selected with Rate Override' );
  });

  xit('track GA Confirm Changes Successful - Without Overriding Rates', () => {
    component.isRateOverride=false;
    const confirmChangeReservationSpy = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(of(
      { status: 'SUCCESS' }));
    const handleChangeReservationSpy = spyOn<any>(component, 'handleChangeReservationResponse').and.callThrough();
    const openCutKeySpy = spyOn<any>(component, 'openCutKey').and.callThrough();
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');

    component['isInHouseEditStay'] = true;
    fixture.detectChanges();
    component['proceedWithChangeConfirmation']();

    expect(confirmChangeReservationSpy).toHaveBeenCalled();
    expect(handleChangeReservationSpy).toHaveBeenCalled();
    expect(openCutKeySpy).toHaveBeenCalled();
    expect(trackEventSpy).toHaveBeenCalledWith('Check In - Manage Stay', 'Confirm Changes', 'Success');
  });

  
  xit('track GA Confirm Changes Successful - with Overriding Rates', () => {
    component.isRateOverride=true;
    const confirmChangeReservationSpy = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(of(
      { status: 'SUCCESS' }));
    const handleChangeReservationSpy = spyOn<any>(component, 'handleChangeReservationResponse').and.callThrough();
    const openCutKeySpy = spyOn<any>(component, 'openCutKey').and.callThrough();
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');

    component['isInHouseEditStay'] = true;
    fixture.detectChanges();
    component['proceedWithChangeConfirmation']();

    expect(confirmChangeReservationSpy).toHaveBeenCalled();
    expect(handleChangeReservationSpy).toHaveBeenCalled();
    expect(openCutKeySpy).toHaveBeenCalled();
    // expect(trackEventSpy).toHaveBeenCalledWith('Check In - Manage Stay', 'Confirm Changes', 'Success');
  });

  it('track GA Confirm Changes failure - with Overriding Rates', () => {
    component.isRateOverride=true;
    const confirmChangeReservationSpy = spyOn(TestBed.get(OffersService), 'confirmChangeReservation').and.returnValue(throwError({
      name: '"500"',
      message: 'Failure'
    }));
    const handleChangeReservationSpy = spyOn<any>(component, 'handleConfirmChangesFailure').and.callThrough();    
    const trackEventSpy = spyOn(component['gaService'], 'trackEvent');
    component['isInHouseEditStay'] = true;
    fixture.detectChanges();
    component['proceedWithChangeConfirmation']();
    expect(confirmChangeReservationSpy).toHaveBeenCalled();
    expect(handleChangeReservationSpy).toHaveBeenCalled();        
    expect(trackEventSpy).toHaveBeenCalledWith( 'Check In - Manage Stay', 'Confirm Changes', 'Error with Rate Override' );
  });
});
