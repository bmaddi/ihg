import { Component, OnInit, Input, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { get } from 'lodash';

import { GoogleAnalyticsService, Alert, AlertType, UserService } from 'ihg-ng-common-core';

import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { RateCategoryModel, ProductModel, GuestCheckInModel, RoomAvailabilityPayloadModel, ModifyReservationPayloadModel, RateInfoModifyReservationPayloadModel, RateInfoModel } from '../../models/offers.models';
import { OriginalReservationFields, RequestedReservationFields, Labels } from '../../constants/reservation-types.constant';
import { OffersService } from '../../services/offers.service';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { SAVE_ERROR_TOAST, UPDATE_TOTALS_ERROR_TOAST } from '../../constants/error-toast.constant';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { manageStayGaConstants } from '../../constants/offer-types.constant';
import { MANAGE_STAY_OFFERS_SAVE_ERROR, OFFERS_SAVE_ERROR, UPDATE_TOTALS_SAVE_ERROR } from '@app/constants/error-autofill-constants';
import { RoomConflict, RoomConflictsPayload } from '@modules/offers/models/room-conflict.interface';
import { ConflictView } from '@modules/offers/enums/conflict-view.enum';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { VALID_ROLES } from '../../constants/valid-rate-override-roles';
import { RATE_OVERIDE_INFO_MSG } from '../../constants/warning-modal.constant';
import { AverageRateComponent } from '../average-rates-modal/avg-rate.component';

import * as cloneDeep from 'lodash/cloneDeep';
import { ReportIssueAutoFillObject } from 'ihg-ng-common-pages';

@Component({
  selector: 'hin-warning-modal',
  templateUrl: './warning-modal.component.html',
  styleUrls: ['./warning-modal.component.scss']
})

export class WarningModalComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public isRowExpanded = false;
  public formattedData: {
    reservationTitle: string;
    originalReservation: string;
    requestedChange: string;
  }[] = [];
  public originalReservationFields: string[] = OriginalReservationFields;
  public requestedReservationFields = RequestedReservationFields;
  public labels = Labels;
  public rateInfoBefore: StayInfoSearchModel;
  public reportIssueAutoFill = OFFERS_SAVE_ERROR;
  public updateTotalsReportIssue: ReportIssueAutoFillObject;
  public errorContent = SAVE_ERROR_TOAST;
  public updateTotalsSaveErrorContent = UPDATE_TOTALS_ERROR_TOAST;
  public confirmChangesError = new Subject<EmitErrorModel>();
  public updateTotalsError = new Subject<EmitErrorModel>();
  public roomUnassignmentWarningToast: Alert;
  public ratesEditedToast: Alert;
  private isInHouseEditStay = false;
  public openInEditableMode = false;
  public pendingReservation = false;
  public isPendingReservationCreated = false;
  public originalRateInfo: RateInfoModel;
  private rootSubscription = new Subscription();

  @Input() reservationNumber: string;
  @Input() searchCritera: StayInfoSearchModel;
  @Input() originalReservationData: StayInfoSearchModel;
  @Input() rateInfoAfter: RateCategoryModel | ProductModel;
  @Input() action: string;
  @Input() isEligibleForRateOverride: boolean;
  @Input() isOpenInEditableMode: boolean;
  @Input() isFromOfferUnavailable = false;

  @ViewChild(AverageRateComponent) avgRateComp: AverageRateComponent;
  isRateOverride: boolean;
  constructor(
    public activeModal: NgbActiveModal,
    public offersService: OffersService,
    public cdRef: ChangeDetectorRef,
    private gaService: GoogleAnalyticsService,
    private roomsService: RoomsService,
    private userService: UserService) {
    super();
  }

  ngOnInit() {
    this.originalRateInfo = cloneDeep(this.rateInfoAfter.rateInfo);
    this.formatGridData();
    this.handleDoNotMoveReservation();
    this.checkForInHouseEdit();
    if (this.isOpenInEditableMode) {
      this.expandRowInEditableMode();
    }
    this.updateTotalsAutofill();
  }

  public updateTotalsAutofill(): void {
    this.updateTotalsReportIssue = UPDATE_TOTALS_SAVE_ERROR(this.reservationNumber);
  }

  checkForInHouseEdit() {
    this.isInHouseEditStay = this.offersService.checkForManageStayEdit();
    if (this.isInHouseEditStay) {
      this.reportIssueAutoFill = MANAGE_STAY_OFFERS_SAVE_ERROR(this.reservationNumber);
    }
  }

  updateTotals() {
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, manageStayGaConstants.ACT_CONFIRM_MODAL, manageStayGaConstants.LBL_UPD_TOTAL);
    if (this.avgRateComp.avgNightlyRateForm.dirty || this.isFromOfferUnavailable) {
      if (this.avgRateComp.checkAndUpdateTotals() || this.isFromOfferUnavailable) {
        this.pendingReservation = true;
        this.isRateOverride = true;
        this.continue();
      } else {
        this.avgRateComp.avgNightlyRateForm.markAsPristine();
        this.openInEditableMode = false;
        this.isFromOfferUnavailable = false;
      }
    } else {
      this.openInEditableMode = false;
    }
  }

  get warningModalTitle(): string {
    return (this.openInEditableMode) ? 'LBL_WARNING_MODAL_UPDATE_TITLE' : 'LBL_WARNING_MODAL_TITLE';
  }

  expandRowInEditableMode(): void {
    this.isRowExpanded = true;
    this.openInEditableMode = true;
    if (this.ratesEditedToast) {
      this.handleRatesEditedAlertMessage(true);
    }
    this.cdRef.detectChanges();
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, manageStayGaConstants.ACT_CONFIRM_MODAL, manageStayGaConstants.LBL_RATE_OVERRIDE);
  }

  expandAvgRateView() {
    this.isRowExpanded = !this.isRowExpanded;
  }

  closeModal() {
    if(this.isPendingReservationCreated) {
      this.ignorePendingReservation();
    } else {
      this.activeModal.dismiss();
    }
  }

  public handleCancel(): void {
    if(this.pendingReservation) {
      super.resetSpecificError(this.updateTotalsError);
    }
    if(this.openInEditableMode) {
      if(this.isOpenInEditableMode && !this.isPendingReservationCreated) {
        this.closeModal();
      } else {
        this.cancelRateOverrideChanges();
      }
    } else {
      this.closeModal();
    }
  }

  public cancelRateOverrideChanges(): void {
    this.avgRateComp.cancelChanges();
    this.avgRateComp.avgNightlyRateForm.markAsPristine();
    this.openInEditableMode = false;
    this.pendingReservation = false;
  }

  public ignorePendingReservation(): void {
    this.rootSubscription.add(
      this.offersService.ignorePendingReservation(this.reservationNumber).subscribe(
        (response) => this.resetChanges()
      )
    );
  }

  public resetChanges(): void {
    this.rateInfoAfter.rateInfo = { ...this.originalRateInfo };
    this.isPendingReservationCreated = false;
    this.closeModal();
  }

  private handleDoNotMoveReservation(): void {
    if (this.isDoNotMoveRoomAssigned()) {
      if (this.isRoomTypeChanged()) {
        this.updateUnassignmentToast();
      } else if (this.isExtendedDuration()) {
        this.checkExtendedStayAvailability();
      } else {
        this.updateUnassignmentToast(true);
      }
    } else {
      this.updateUnassignmentToast(true);
    }
  }

  private isExtendedDuration(): boolean {
    const item = this.formattedData.find(item => item.reservationTitle === 'LBL_GST_NIGHTS');
    return item.requestedChange > item.originalReservation;
  }

  private checkExtendedStayAvailability(): void {
    this.offersService.getRoomAvailability(this.getRoomAvailabilityPayload())
      .subscribe((isRoomAvailable: boolean) => {
        this.updateUnassignmentToast(isRoomAvailable);
      });
  }

  private getRoomAvailabilityPayload(): RoomAvailabilityPayloadModel {
    return {
      checkInDate: this.originalReservationData.checkOutDate,
      checkOutDate: this.searchCritera.departureDate,
      reservationNumber: this.reservationNumber,
      roomNumber: this.originalReservationData.roomNumber,
      roomType: this.originalReservationData.roomTypeCode
    };
  }

  private isRoomTypeChanged(): boolean {
    const item = this.formattedData.find(item => item.reservationTitle === 'LBL_ROOM_TYPE');
    return item.requestedChange !== item.originalReservation;
  }

  private isDoNotMoveRoomAssigned(): boolean {
    const reservation = this.originalReservationData;
    return !!(reservation.doNotMoveRoom && reservation.roomNumber);
  }

  private handleRatesEditedAlertMessage(check: boolean): void {
    this.ratesEditedToast = (check) ? null : RATE_OVERIDE_INFO_MSG;
  }

  private updateUnassignmentToast(reset?: boolean): void {
    this.roomUnassignmentWarningToast = reset ? null : {
      type: AlertType.Warning,
      message: 'COM_TOAST_WARNING',
      detailMessage: 'MSG_DONT_MOVE',
      dismissible: false
    };

    if (!reset) {
      this.gaService.trackEvent(
        manageStayGaConstants.CATEGORY_MANAGE_STAY,
        manageStayGaConstants.CONFIRM_CHANGES,
        manageStayGaConstants.ROOM_UNASGNMNT);
    }
  }

  transformReservationData(reservation, reservationTypes) {
    const transformedData = [];
    const reservationKeys = reservationTypes;
    reservationKeys.map((key): any => {
      Object.keys(reservation).find((item): any => {
        if (item === key) {
          if (item === 'guestInfo') {
            const tempGuestName = [reservation[key]['firstName'], reservation[key]['middleName'], reservation[key]['lastName']];
            transformedData.push({ [key]: tempGuestName.filter(Boolean).join(' ') });
          } else {
            transformedData.push({ [key]: reservation[key] });
          }
        }
      });
    });
    return transformedData;
  }

  formatGridData() {
    this.formattedData = [];
    this.originalReservationFields.map((label: string) => {
      this.formattedData.push({
        reservationTitle: label,
        originalReservation: '',
        requestedChange: ''
      });
    });
    this.rateInfoBefore = this.originalReservationData;
    const originalData = this.transformReservationData(this.originalReservationData, this.originalReservationFields);
    const changedData = this.transformReservationData(this.searchCritera, this.requestedReservationFields);
    originalData.map((obj, index) => {
      this.formattedData.find((item, i): any => {
        if (item.reservationTitle === Object.keys(obj).find((key) => key === item.reservationTitle)) {
          item.originalReservation = obj[item.reservationTitle];
          item.requestedChange = changedData[index][this.requestedReservationFields[i]];
          item.reservationTitle = this.labels[i];
        }
      });
    });
    this.formattedData.find((item): any => {
      if (item.reservationTitle === 'LBL_RATE_CAT') {
        item.requestedChange = this.rateInfoAfter['parentItemRateCode'] ?
          this.rateInfoAfter['parentItemRateCode'] : this.rateInfoAfter.code;
      }
    });
    this.formattedData.find((item): any => {
      if (item.reservationTitle === 'LBL_ROOM_TYPE') {
        item.requestedChange = this.rateInfoAfter['parentProductRoomCode'] ?
          this.rateInfoAfter['parentProductRoomCode'] : this.rateInfoAfter.code;
      }
    });
  }

  get rateInfoPayload(): RateInfoModifyReservationPayloadModel {
    return {
      rateOverrideAction: (this.pendingReservation) ? 'PENDING' : 'CONFIRM',
      dailyRates: this.rateInfoAfter.rateInfo.dailyRates
    };
  }

  continue() {
    if (this.isInHouseEditStay) {
      if (this.isRoomTypeChanged()) {
        this.openConflictsModal(ConflictView.manualAssign, false);
      } else if (this.isExtendedDuration()) {
        this.checkForConflicts();
      } else {
        this.proceedWithChangeConfirmation();
      }
    } else {
      this.proceedWithChangeConfirmation();
    }
  }

  private checkForConflicts() {
    this.offersService.getRoomConflicts(<RoomConflictsPayload>{
      checkInDate: this.searchCritera.arrivalDate,
      checkOutDate: this.searchCritera.departureDate,
      reservationNumber: this.reservationNumber,
      roomNumber: this.originalReservationData.roomNumber,
      roomType: this.getConfirmationPayload().roomTypeCode
    }).subscribe(
      conflicts => this.handleSuccessfulConflictsResponse(conflicts),
      error => this.handleConflictsResponseError(error)
    );
  }

  private handleSuccessfulConflictsResponse(conflicts: RoomConflict[]) {
    if (!super.checkIfAnyApiErrors(conflicts, null, this.confirmChangesError) && conflicts.length) {
      this.openConflictsModal(ConflictView.roomConflicts, true, conflicts);
    } else {
      this.proceedWithChangeConfirmation();
    }
  }

  private handleConflictsResponseError(error: HttpErrorResponse) {
    this.offersService.changeMessage(false);
    super.processHttpErrors(error, this.confirmChangesError);
  }

  private proceedWithChangeConfirmation() {
    this.trackGAForConfirmChanges();
    const payload = this.getConfirmationPayload();
    const errorSubject = (this.pendingReservation) ? this.updateTotalsError : this.confirmChangesError;
    this.rootSubscription.add(this.offersService.confirmChangeReservation(payload)
      .subscribe((response: GuestCheckInModel) => {
        if (!super.checkIfAnyApiErrors(response, null, errorSubject)) {
          if (response && response.status === 'SUCCESS') {
            if (!this.pendingReservation) {
              this.trackGAForConfirmChangesSuccess();
              this.handleChangeReservationResponse(response);
            }
            if (this.pendingReservation) {
              this.handleUpdateTotalsResponse(response);
            }
          }
        } else {
          this.handleConfirmChangesFailure();
          this.handleUpdateTotalsFailure();
        }
      }, error => {
        this.handleConfirmChangesFailure();
        super.processHttpErrors(error, errorSubject);
        this.handleUpdateTotalsFailure();
      }));
  }

  public handleUpdateTotalsFailure(): void {
    if(this.pendingReservation) {
      this.avgRateComp.restorePrevData();
      document.querySelector('.modal-content').scrollIntoView();
    }
  }

  private trackGAForConfirmChanges() {
    const gaLabel = this.isRateOverride ? manageStayGaConstants.LBL_SELECTED_RT_OVERRIDE : manageStayGaConstants.LBL_SELECTED;
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, manageStayGaConstants.CONFIRM_CHANGES, gaLabel);
  }

  private trackGAForConfirmChangesSuccess() {
    const gaLabel = this.isRateOverride ? manageStayGaConstants.LBL_CONF_SUCESS_RT_OVERRIDE : manageStayGaConstants.LBL_CONF_SUCESS
    this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, manageStayGaConstants.CONFIRM_CHANGES, gaLabel);
  }

  private handleConfirmChangesFailure() {
    if (!this.pendingReservation) {
      const gaLabel = this.isRateOverride ? manageStayGaConstants.LBL_CONF_ERR_RT_OVERRIDE : manageStayGaConstants.OFFER_ERROR
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, manageStayGaConstants.CONFIRM_CHANGES, gaLabel);
      this.rootSubscription.add(this.offersService.changeMessage(false));
    }
  }

  private handleChangeReservationResponse(response: GuestCheckInModel) {
    this.activeModal.dismiss();
    this.isRateOverride = false;
    if (this.isInHouseEditStay) {
      this.openCutKey();
    } else {
      this.rootSubscription.add(this.offersService.setReservationData(response));
      this.rootSubscription.add(this.offersService.changeMessage(true));
    }
  }

  private openCutKey() {
    this.offersService.triggerCutKeyModal({
      conflicts: [],
      reservationNumber: this.reservationNumber,
      pmsNumber: get(this.offersService.getInHouseReservationData(), 'pmsReservationNumber', null),
    });
  }

  private getConfirmationPayload(): { [key: string]: string | number | any } {
    const reservationData = this.originalReservationData;
    let rateCategoryCode = this.rateInfoAfter.code;
    let roomTypeCode = this.rateInfoAfter['parentProductRoomCode'] ? this.rateInfoAfter['parentProductRoomCode'] : this.rateInfoAfter.parentItemRateCode;
    if ((this.action === manageStayGaConstants.RATES) && !this.isOpenInEditableMode) {
      rateCategoryCode = this.rateInfoAfter['parentProductRoomCode'] ? this.rateInfoAfter['parentProductRoomCode'] : this.rateInfoAfter.parentItemRateCode;
      roomTypeCode = this.rateInfoAfter.code;
    }
    return {
      hotelCode: reservationData.hotelCode,
      confirmationNumber: this.reservationNumber,
      checkInDate: this.searchCritera.arrivalDate,
      checkOutDate: this.searchCritera.departureDate,
      rateCategoryCode: rateCategoryCode,
      roomTypeCode: roomTypeCode,
      numberOfRooms: this.searchCritera.numberOfRooms,
      adultQuantity: this.searchCritera.adultsCount,
      childQuantity: this.searchCritera.childCount,
      ihgRcNumber: this.searchCritera.ihgRcNumber,
      firstName: this.searchCritera.guestInfo.firstName,
      middleName: this.searchCritera.guestInfo.middleName,
      lastName: this.searchCritera.guestInfo.lastName,
      rateInfo: this.isRateOverride? this.rateInfoPayload:null
    };
  }

  private openConflictsModal(conflictViewType: ConflictView, viewChangeEnabled: boolean, conflicts?: RoomConflict[]) {
    const confirmationPayload = this.getConfirmationPayload();
    this.offersService.triggerConflictsModal({
      conflicts,
      reservationNumber: this.reservationNumber,
      searchCriteria: this.searchCritera,
      confirmationPayload,
      originalReservationData: this.originalReservationData,
      listItem: {
        inspected: get(this.offersService.getInHouseReservationData(), 'inspected', false),
        ...confirmationPayload,
        roomType: confirmationPayload.roomTypeCode,
        pmsReservationNumber: get(this.offersService.getInHouseReservationData(), 'pmsReservationNumber', null),
        reservationNumber: this.reservationNumber
      },
      conflictViewType,
      viewChangeEnabled
    });
    this.activeModal.dismiss();
  }

  displayRateOverride(): boolean {
    return this.isEligibleForRateOverride && this.userHasValidRole();
  }

  private userHasValidRole(): boolean {
    const userRoles = this.userService.getUser().roles;

    return userRoles ? userRoles
      .some(userRole => VALID_ROLES.map(validRole => validRole.name)
        .indexOf(userRole.name) > -1) : false;
  }

  public handleUpdateTotalsResponse(resp) {
    this.rateInfoAfter.rateInfo = resp.reservationData.rateOverrideInfo;
    this.avgRateComp.transformDataToGrid();
    this.avgRateComp.avgNightlyRateForm.markAsPristine();
    this.pendingReservation = false;
    this.handleRatesEditedAlertMessage(false);
    document.querySelector('.modal-content').scrollIntoView();
    this.isPendingReservationCreated = true;
    this.openInEditableMode = false;
    this.isFromOfferUnavailable = false;
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
