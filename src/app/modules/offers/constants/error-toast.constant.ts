export const OFFER_ERROR_TOAST = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'SRCH_FLD_TRY_AGN'
  }
};

export const SAVE_ERROR_TOAST = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_SAVE_FAIL_DESC'
  },
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_SAVE_FAIL_DESC'
  }
};

export const UPDATE_TOTALS_ERROR_TOAST = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_UPDATE_TOTAL_FAIL_DESC'
  },
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_UPDATE_TOTAL_FAIL_DESC'
  }
};

export const MAN_ASSIGN_SAVE_ERROR_TOAST = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_FAIL_UPD_RSVN'
  },
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_FAIL_UPD_RSVN'
  }
};
