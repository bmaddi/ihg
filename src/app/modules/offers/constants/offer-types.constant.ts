import { OfferTypeBtnModel } from '../models/offers.models';

export const manageStayGaConstants = {
  CATEGORY_IN_HOUSE_MANAGE_STAY: 'In House - Manage Stay',
  CATEGORY_MANAGE_STAY: 'Check In - Manage Stay',
  LBL_SELECTED: 'Selected',
  LBL_OFFER_SELECTED: 'Offer Selected',
  ACTION_SHOW_MORE: 'Show More',
  ACTION_SHOW_LESS: 'Show Less',
  ACTION_COLLAPSE: 'Collapse',
  ACTION_EXPAND: 'Expand',
  ACTION_RATE_DESC: 'Rate Description',
  ACTION_PRODUCT_DESC: 'Product Description',
  PRODUCTS: 'Products',
  RATES: 'Rates',
  CONFIRM_CHANGES: 'Confirm Changes',
  OFFER_SERACH: 'Offer Search',
  OFFER_NOT_AVAILABLE: 'Offer Not Available',
  OFFER_ERROR: 'Error',
  SHOP_OFFER: 'Shop Offers',
  ROOM_UNASGNMNT: 'Room Unassignment Warning',
  ACT_CONFIRM_MODAL: 'Confirmation Modal',
  LBL_RATE_OVERRIDE: 'Rate Override Selected',
  ACT_OFR_NOT_AVAIL: 'Offer Unavailable Modal',
  LBL_UPD_TOTAL: 'Update Total Selected',
  ACT_CONF_CHNGS : 'Confirm Changes',
  LBL_SELECTED_RT_OVERRIDE : 'Selected with Rate Override',
  LBL_CONF_SUCESS : 'Success',
  LBL_CONF_SUCESS_RT_OVERRIDE: 'Success with Rate Override',
  LBL_CONF_ERR_RT_OVERRIDE: 'Error with Rate Override',
  FORCE_SELL_NO_OFFERS: 'Offer Unavailable Message - No Offer view',
  FORCE_SELL_NO_OFFERS_MODAL: 'Offer Unavailable Modal - No Offer view',
  LBL_FORCE_SELL_VIEW: 'Force Sell Displayed',
  LBL_FORCE_SELL_SELECTED: 'Force Sell Selected'
}

export enum offerTypes {
  ALL = 'all',
  SUGGESTED = 'suggested',
  PRODUCTS = 'products',
  RATES = 'rates'
};

export const offerButtons: OfferTypeBtnModel[] = [
  {
    label: 'LBL_VW_ALL_OFRS',
    cardLabel: 'LBL_ALL_OFFERS',
    seleniumId: 'AllOffersButton-SID',
    cardSeleniumId: 'AllOffers-SID',
    type: offerTypes.ALL,
    selected: false,
    gaLabel: 'View All Offers',
    visible: false
  },
  {
    label: 'LBL_VW_SGSTD_OFRS',
    cardLabel: 'LBL_SGSTD_OFRS',
    seleniumId: 'SuggestedOffersButton-SID',
    cardSeleniumId: 'SuggestedOffers-SID',
    type: offerTypes.SUGGESTED,
    selected: false,
    gaLabel: 'View Suggested Offers',
    visible: false
  },
  {
    label: 'LBL_PRODUCTS',
    cardLabel: 'LBL_ALL_OFFERS',
    seleniumId: 'ProductsButton-SID',
    cardSeleniumId: 'AllOffers-SID',
    type: offerTypes.PRODUCTS,
    selected: false,
    gaLabel: manageStayGaConstants.PRODUCTS,
    visible: false
  },
  {
    label: 'LBL_RATES',
    cardLabel: 'LBL_ALL_OFFERS',
    seleniumId: 'RatesButton-SID',
    cardSeleniumId: 'AllOffers-SID',
    type: offerTypes.RATES,
    selected: false,
    gaLabel: manageStayGaConstants.RATES,
    visible: false
  }
];
