import { RoomDescriptionModel } from '../models/offers.models';

export const rateDescriptionMap: { [key: string]: Partial<RoomDescriptionModel> } = {
  roomDesc: {
    id: 'roomDesc',
    label: 'LBL_ROOM_DESC',
    seleniumId: {
      header: 'RoomDescriptionHeader-SID',
      body: 'RoomDescriptionBody-SID'
    },
    priority: 1,
    applySentenceCase: true,
    allCapsWords: ['IHG', 'RC']
  },
  rateRuleDesc: {
    id: 'rateRuleDesc',
    label: 'LBL_RATE_DESC',
    seleniumId: {
      header: 'RateDescriptionHeader-SID',
      body: 'RateDescriptionBody-SID'
    },
    priority: 2,
    applySentenceCase: true,
    allCapsWords: ['IHG', 'RC']
  },
  rateRules: {
    id: 'rateRules',
    label: 'LBL_RATE_RULE',
    seleniumId: {
      header: 'RateRuleHeader-SID'
    },
    priority: 3
  },
  serviceCharge: {
    id: 'serviceCharge',
    label: 'LBL_SRVC_CHRG',
    seleniumId: {
      header: 'ServiceChargeHeader-SID',
      body: 'ServiceChargeBody-SID'
    },
    priority: 4,
    staticText: {
      included: 'LBL_SRVC_CHRG_INCLD',
      notIncluded: 'LBL_SRVC_CHRG_NOT_INCLD'
    }
  },
  tax: {
    id: 'tax',
    label: 'LBL_TAX',
    seleniumId: {
      header: 'TaxHeader-SID',
      body: 'TaxBody-SID'
    },
    priority: 5,
    staticText: {
      included: 'LBL_TAX_INCLD',
      notIncluded: 'LBL_TAX_NOT_INCLD'
    }
  },
  earlyDeparture: {
    id: 'earlyDeparture',
    label: 'LBL_ERLY_DEP',
    seleniumId: {
      header: 'EarlyDepartureHeader-SID',
      body: 'EarlyDepartureBody-SID'
    },
    priority: 6,
    staticText: 'LBL_ERLY_DEP_FEE_APLD'
  },
  noShowPolicyDesc: {
    id: 'noShowPolicyDesc',
    label: 'LBL_NO_SHOW_PLCY',
    seleniumId: {
      header: 'NoShowPolicyHeader-SID',
      body: 'NoShowPolicyBody-SID'
    },
    priority: 7
  },
  extraPersonCharge: {
    id: 'extraPersonCharge',
    label: 'LBL_EXTRA_PERSON_CHARGE',
    seleniumId: {
      header: 'ExtraPersonChargeHeader-SID',
      body: 'ExtraPersonChargeBody-SID'
    },
    priority: 8,
    staticText: 'LBL_RATE_APPLIES'
  },
  guaranteePolicy: {
    id: 'guaranteePolicy',
    label: 'LBL_GRNTY_PLCY',
    seleniumId: {
      header: 'GuaranteePolicyHeader-SID',
      body: 'GuaranteePolicyBody-SID'
    },
    priority: 9
  }
};

export const rateDescriptionOnlyMap: { [key: string]: Partial<RoomDescriptionModel> } = {
  rateRules: {
    id: 'rateRules',
    label: 'LBL_RATE_RULE',
    seleniumId: {
      header: 'RateRuleHeader-SID'
    },
    priority: 1
  },
  rateRuleDesc: {
    id: 'rateRuleDesc',
    label: 'LBL_RATE_DESC',
    seleniumId: {
      header: 'RateDescriptionHeader-SID',
      body: 'RateDescriptionBody-SID'
    },
    priority: 2,
    applySentenceCase: true,
    allCapsWords: ['IHG', 'RC']
  },
  noShowPolicyDesc: {
    id: 'noShowPolicyDesc',
    label: 'LBL_NO_SHOW_PLCY',
    seleniumId: {
      header: 'NoShowPolicyHeader-SID',
      body: 'NoShowPolicyBody-SID'
    },
    priority: 3
  },
  extraPersonCharge: {
    id: 'extraPersonCharge',
    label: 'LBL_EXTRA_PERSON_CHARGE',
    seleniumId: {
      header: 'ExtraPersonChargeHeader-SID',
      body: 'ExtraPersonChargeBody-SID'
    },
    priority: 4,
    staticText: 'LBL_RATE_APPLIES'
  },
  earlyDeparture: {
    id: 'earlyDeparture',
    label: 'LBL_ERLY_DEP',
    seleniumId: {
      header: 'EarlyDepartureHeader-SID',
      body: 'EarlyDepartureBody-SID'
    },
    priority: 5,
    staticText: 'LBL_ERLY_DEP_FEE_APLD'
  }
};
