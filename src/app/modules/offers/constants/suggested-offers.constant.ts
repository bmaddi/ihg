import { SuggestedOffersTabsModel } from "../models/offers.models";

export enum suggestedOfferTypes {
  ONE_BED = 'ONE_BED',
  TWO_BEDS = 'TWO_BEDS',
  SUITE = 'SUITE',
  OTHER = 'OTHER',
};

export const suggestedOfferTabs: SuggestedOffersTabsModel[] = [
  {
    id: 'oneBed',
    label: 'LBL_ONE_BED',
    seleniumId: 'OneBed-SID',
    roomCountSeleniumId: 'OneBedRoomCount-SID',
    type: suggestedOfferTypes.ONE_BED,
    gaAction : 'One Bed'
  },
  {
    id: 'twoBeds',
    label: 'LBL_TWO_BEDS',
    seleniumId: 'TwoBeds-SID',
    roomCountSeleniumId: 'TwoBedsRoomCount-SID',
    type: suggestedOfferTypes.TWO_BEDS,
    gaAction: 'Two Beds'
  },
  {
    id: 'suite',
    label: 'LBL_SUITE',
    seleniumId: 'Suite-SID',
    roomCountSeleniumId: 'SuiteRoomCount-SID',
    type: suggestedOfferTypes.SUITE,
    gaAction: 'Suite'
  } ,
  {
    id: 'other',
    label: 'LBL_OTH',
    seleniumId: 'Other-SID',
    roomCountSeleniumId: 'OtherRoomCount-SID',
    type: suggestedOfferTypes.OTHER,
    gaAction: 'Other'
  }
];


export const gaSuggestedOffersConstants = {
  CATEGORY: 'Check In - Manage Stay',
  CATEGORY_MANAGE_STAY: 'In House - Manage Stay',
  ACTION_SUGGESTED_OFFER: 'Suggested Offers',
  ACTION_VIEW_ALL_OFFERS: 'View All Offers',
  ACTION_VIEW_SUGGESTED_OFFERS: 'View Suggested Offers',
  LABEL_SELECTED : 'Selected',
  LABEL_OFFER_SELECTED: 'Offer Selected',
  LABEL_VIEW_DETAIL_SELECTED: 'View Details Selected',
  LABEL_ROOM_TYPE_NOT_AVAIL: 'Room Type Not Available',
  LABEL_BED_TYPE_NOT_AVAIL: 'Bed Type Not Available',
  ACTION_TOAST_NO_ROOM_TYPE: 'Offer Unavailable Toast - No Room Type',
  ACTION_MODAL_NO_ROOM_TYPE: 'Offer Unavailable Modal - No Room Type',
  LBL_FORCE_SELL_VIEW: 'Force Sell Displayed',
  LBL_FORCE_SELL_SELECTED: 'Force Sell Selected'
}
