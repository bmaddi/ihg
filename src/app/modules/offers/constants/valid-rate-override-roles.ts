export const VALID_ROLES = [
    {
        id: '5010',
        name: 'CORPORATE_ADMINISTRATOR'
    },
    {
        id: '5007',
        name: 'GUEST_COMMUNICATIONS'
    },
    {
        id: '5006',
        name: 'RESERVATIONS'
    },
    {
        id: '5005',
        name: 'FRONT_DESK'
    },
    {   id: '5004',
        name: 'FRONT_DESK_MANAGEMENT'
    },
    {   id: '5001',
        name: 'SALES'
    },
    {
        id: '5000',
        name: 'HOTEL_MANAGEMENT'
    }];
