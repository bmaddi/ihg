import { AlertType } from 'ihg-ng-common-core';

export const RATE_OVERIDE_INFO_MSG = {
  type: AlertType.Info,
  message: 'LBL_NOTE_MSG',
  detailMessage: 'LBL_RATE_OVERIDE_INFO',
  dismissible: false
}