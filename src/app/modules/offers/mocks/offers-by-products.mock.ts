/* tslint:disable:max-line-length */
import { OffersByProductResponseModel } from '@modules/offers/models/offers.models';

export const mockProducts = [
  {
    'code': 'CSTN',
    'name': 'STANDARD ROOM NONSMOKING',
    'description': 'WHEN YOU ARRIVE WE WILL DO OUR BEST TO MEET YOUR ROOM BED TYPE PREFERENCE THESE ARE SUBJECT TO AVAILABILITY.',
    'roomsAvailable': '73',
    'bedType': 'OTHER',
    'rateCategories': [
      {
        'name': 'BEST FLEXIBLE RATE',
        'code': 'IGCOR',
        'averageNightlyRate': '125.00',
        'rateInfo': {
          'averageRate': '125.00',
          'currency': 'GBP',
          'totalTaxAmount': '41.67',
          'totalAmountAfterTax': '250.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '104.17',
              'amountAfterTax': '125.00',
              'baseAmount': '125.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '104.17',
              'amountAfterTax': '125.00',
              'baseAmount': '125.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY. ihg rc test content.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '2220.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '41.67',
              'serviceCharge': '45.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      },
      {
        'name': 'IHG RC 1000 PTS NT',
        'code': 'IKME3',
        'averageNightlyRate': '126.72',
        'rateInfo': {
          'averageRate': '126.72',
          'currency': 'GBP',
          'totalTaxAmount': '42.24',
          'totalAmountAfterTax': '253.44',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '105.60',
              'amountAfterTax': '126.72',
              'baseAmount': '126.72'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '105.60',
              'amountAfterTax': '126.72',
              'baseAmount': '126.72'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '42.24',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      },
      {
        'name': '1000 BONUS POINTS NT',
        'code': 'IKPCM',
        'averageNightlyRate': '132.00',
        'rateInfo': {
          'averageRate': '132.00',
          'currency': 'GBP',
          'totalTaxAmount': '44.00',
          'totalAmountAfterTax': '264.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '110.00',
              'amountAfterTax': '132.00',
              'baseAmount': '132.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '110.00',
              'amountAfterTax': '132.00',
              'baseAmount': '132.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '44.00',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      },
      {
        'name': 'IHG RC BREAKFAST BB',
        'code': 'IKME0',
        'averageNightlyRate': '134.40',
        'rateInfo': {
          'averageRate': '134.40',
          'currency': 'GBP',
          'totalTaxAmount': '44.80',
          'totalAmountAfterTax': '268.80',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '112.00',
              'amountAfterTax': '134.40',
              'baseAmount': '134.40'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '112.00',
              'amountAfterTax': '134.40',
              'baseAmount': '134.40'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'EXCLUSIVE SAVINGS FOR OUR IHG REWARDS CLUB MEMBERS. MUST BE AN IHG REWARDS CLUB MEMBER TO BOOK. INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '44.80',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      },
      {
        'name': 'BEST FLEX WITH BKFST',
        'code': 'IGBBB',
        'averageNightlyRate': '140.00',
        'rateInfo': {
          'averageRate': '140.00',
          'currency': 'GBP',
          'totalTaxAmount': '46.67',
          'totalAmountAfterTax': '280.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '116.67',
              'amountAfterTax': '140.00',
              'baseAmount': '140.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '116.67',
              'amountAfterTax': '140.00',
              'baseAmount': '140.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '46.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      },
      {
        'name': '2000 POINTS PER STAY',
        'code': 'IKHB2',
        'averageNightlyRate': '140.00',
        'rateInfo': {
          'averageRate': '140.00',
          'currency': 'GBP',
          'totalTaxAmount': '46.67',
          'totalAmountAfterTax': '280.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '116.67',
              'amountAfterTax': '140.00',
              'baseAmount': '140.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '116.67',
              'amountAfterTax': '140.00',
              'baseAmount': '140.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 2000 IHG REWARD CLUB BONUS POINTS PER STAY.COMPLIMENTARY WIFI INCLUDED.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '46.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      },
      {
        'name': 'DINNER BED AND BKFST',
        'code': 'IBDNG',
        'averageNightlyRate': '180.00',
        'rateInfo': {
          'averageRate': '180.00',
          'currency': 'GBP',
          'totalTaxAmount': '60.00',
          'totalAmountAfterTax': '360.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '150.00',
              'amountAfterTax': '180.00',
              'baseAmount': '180.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '150.00',
              'amountAfterTax': '180.00',
              'baseAmount': '180.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'FULLY FLEXIBLE RATE INCLUDES BREAKFAST AND THREE COURSE DINNER FROM SET MENU OR BUFFET IN A DESIGNATED HOTEL RESTAURANT FOR ONE OR TWO RESIDENT ADULTS SHARING A ROOM. ROOM SERVICE MAY INCUR A SUPPLEMENT.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '60.00',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'CSTN'
      }
    ],
    'productIncludedInOffer': false,
    'serviceChargeIncluded': false,
    'taxesChargeIncluded': true
  },
  {
    'code': 'KEXN',
    'name': '1 KING BED CLUB ROOM NONSMOKING',
    'description': 'ENJOY EXTRA SERVICE WITH A LUXURY KING CLUB ROOM. ACCESS TO THE EXCLUSIVE 8TH FLOOR CLUB LOUNGE IS INCLUDED. HERE YOU CAN ENJOY PANORAMIC CITY CENTRE VIEWS, CONTINENTAL BREAKFAST & UNLIMITED TEA, COFFEE & SOFT DRINKS. ALSO BETWEEN 6-8PM, COMPLIMENTARY CANAPES & ALCOHOLIC BEVERAGES ARE SERVED.',
    'roomsAvailable': '21',
    'bedType': 'ONE_BED',
    'rateCategories': [
      {
        'name': 'BEST FLEXIBLE RATE',
        'code': 'IGCOR',
        'averageNightlyRate': '155.00',
        'rateInfo': {
          'averageRate': '155.00',
          'currency': 'GBP',
          'totalTaxAmount': '51.67',
          'totalAmountAfterTax': '310.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '129.17',
              'amountAfterTax': '155.00',
              'baseAmount': '155.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '129.17',
              'amountAfterTax': '155.00',
              'baseAmount': '155.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': '',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '51.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      },
      {
        'name': 'IHG RC 1000 PTS NT',
        'code': 'IKME3',
        'averageNightlyRate': '155.52',
        'rateInfo': {
          'averageRate': '155.52',
          'currency': 'GBP',
          'totalTaxAmount': '51.84',
          'totalAmountAfterTax': '311.04',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '129.60',
              'amountAfterTax': '155.52',
              'baseAmount': '155.52'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '129.60',
              'amountAfterTax': '155.52',
              'baseAmount': '155.52'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '51.84',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      },
      {
        'name': '1000 BONUS POINTS NT',
        'code': 'IKPCM',
        'averageNightlyRate': '162.00',
        'rateInfo': {
          'averageRate': '162.00',
          'currency': 'GBP',
          'totalTaxAmount': '54.00',
          'totalAmountAfterTax': '324.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '135.00',
              'amountAfterTax': '162.00',
              'baseAmount': '162.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '135.00',
              'amountAfterTax': '162.00',
              'baseAmount': '162.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '54.00',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      },
      {
        'name': 'IHG RC BREAKFAST BB',
        'code': 'IKME0',
        'averageNightlyRate': '163.20',
        'rateInfo': {
          'averageRate': '163.20',
          'currency': 'GBP',
          'totalTaxAmount': '54.40',
          'totalAmountAfterTax': '326.40',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '136.00',
              'amountAfterTax': '163.20',
              'baseAmount': '163.20'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '136.00',
              'amountAfterTax': '163.20',
              'baseAmount': '163.20'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'EXCLUSIVE SAVINGS FOR OUR IHG REWARDS CLUB MEMBERS. MUST BE AN IHG REWARDS CLUB MEMBER TO BOOK. INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '54.40',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      },
      {
        'name': 'BEST FLEX WITH BKFST',
        'code': 'IGBBB',
        'averageNightlyRate': '170.00',
        'rateInfo': {
          'averageRate': '170.00',
          'currency': 'GBP',
          'totalTaxAmount': '56.67',
          'totalAmountAfterTax': '340.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '141.67',
              'amountAfterTax': '170.00',
              'baseAmount': '170.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '141.67',
              'amountAfterTax': '170.00',
              'baseAmount': '170.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '56.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      },
      {
        'name': '2000 POINTS PER STAY',
        'code': 'IKHB2',
        'averageNightlyRate': '170.00',
        'rateInfo': {
          'averageRate': '170.00',
          'currency': 'GBP',
          'totalTaxAmount': '56.67',
          'totalAmountAfterTax': '340.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '141.67',
              'amountAfterTax': '170.00',
              'baseAmount': '170.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '141.67',
              'amountAfterTax': '170.00',
              'baseAmount': '170.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 2000 IHG REWARD CLUB BONUS POINTS PER STAY.COMPLIMENTARY WIFI INCLUDED.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '56.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      },
      {
        'name': 'DINNER BED AND BKFST',
        'code': 'IBDNG',
        'averageNightlyRate': '210.00',
        'rateInfo': {
          'averageRate': '210.00',
          'currency': 'GBP',
          'totalTaxAmount': '70.00',
          'totalAmountAfterTax': '420.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '175.00',
              'amountAfterTax': '210.00',
              'baseAmount': '210.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '175.00',
              'amountAfterTax': '210.00',
              'baseAmount': '210.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'FULLY FLEXIBLE RATE INCLUDES BREAKFAST AND THREE COURSE DINNER FROM SET MENU OR BUFFET IN A DESIGNATED HOTEL RESTAURANT FOR ONE OR TWO RESIDENT ADULTS SHARING A ROOM. ROOM SERVICE MAY INCUR A SUPPLEMENT.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '70.00',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXN'
      }
    ],
    'productIncludedInOffer': false,
    'serviceChargeIncluded': false,
    'taxesChargeIncluded': true
  },
  {
    'code': 'KDXG',
    'name': 'KING DELUXE',
    'description': '',
    'roomsAvailable': '10',
    'bedType': 'ONE_BED',
    'rateCategories': [
      {
        'name': 'BEST FLEXIBLE RATE',
        'code': 'IGCOR',
        'averageNightlyRate': '100.00',
        'rateInfo': {
          'averageRate': '100.00',
          'currency': 'GBP',
          'totalTaxAmount': '33.33',
          'totalAmountAfterTax': '200.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '83.33',
              'amountAfterTax': '100.00',
              'baseAmount': '100.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '83.33',
              'amountAfterTax': '100.00',
              'baseAmount': '100.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': '',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '33.33',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KDXG'
      },
      {
        'name': 'IHG RC BREAKFAST BB',
        'code': 'IKME0',
        'averageNightlyRate': '110.40',
        'rateInfo': {
          'averageRate': '110.40',
          'currency': 'GBP',
          'totalTaxAmount': '36.80',
          'totalAmountAfterTax': '220.80',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '92.00',
              'amountAfterTax': '110.40',
              'baseAmount': '110.40'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '92.00',
              'amountAfterTax': '110.40',
              'baseAmount': '110.40'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'EXCLUSIVE SAVINGS FOR OUR IHG REWARDS CLUB MEMBERS. MUST BE AN IHG REWARDS CLUB MEMBER TO BOOK. INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '36.80',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KDXG'
      },
      {
        'name': 'BEST FLEX WITH BKFST',
        'code': 'IGBBB',
        'averageNightlyRate': '115.00',
        'rateInfo': {
          'averageRate': '115.00',
          'currency': 'GBP',
          'totalTaxAmount': '38.33',
          'totalAmountAfterTax': '230.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '38.33',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KDXG'
      },
      {
        'name': '2000 POINTS PER STAY',
        'code': 'IKHB2',
        'averageNightlyRate': '115.00',
        'rateInfo': {
          'averageRate': '115.00',
          'currency': 'GBP',
          'totalTaxAmount': '38.33',
          'totalAmountAfterTax': '230.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 2000 IHG REWARD CLUB BONUS POINTS PER STAY.COMPLIMENTARY WIFI INCLUDED.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '38.33',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KDXG'
      },
      {
        'name': 'DINNER BED AND BKFST',
        'code': 'IBDNG',
        'averageNightlyRate': '155.00',
        'rateInfo': {
          'averageRate': '155.00',
          'currency': 'GBP',
          'totalTaxAmount': '51.67',
          'totalAmountAfterTax': '310.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '129.17',
              'amountAfterTax': '155.00',
              'baseAmount': '155.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '129.17',
              'amountAfterTax': '155.00',
              'baseAmount': '155.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'FULLY FLEXIBLE RATE INCLUDES BREAKFAST AND THREE COURSE DINNER FROM SET MENU OR BUFFET IN A DESIGNATED HOTEL RESTAURANT FOR ONE OR TWO RESIDENT ADULTS SHARING A ROOM. ROOM SERVICE MAY INCUR A SUPPLEMENT.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '51.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KDXG'
      }
    ],
    'productIncludedInOffer': false,
    'serviceChargeIncluded': false,
    'taxesChargeIncluded': true
  },
  {
    'code': 'KEXG',
    'name': 'KING EXECUTIVE',
    'description': '',
    'roomsAvailable': '10',
    'bedType': 'ONE_BED',
    'rateCategories': [
      {
        'name': 'BEST FLEXIBLE RATE',
        'code': 'IGCOR',
        'averageNightlyRate': '100.00',
        'rateInfo': {
          'averageRate': '100.00',
          'currency': 'GBP',
          'totalTaxAmount': '33.33',
          'totalAmountAfterTax': '200.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '83.33',
              'amountAfterTax': '100.00',
              'baseAmount': '100.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '83.33',
              'amountAfterTax': '100.00',
              'baseAmount': '100.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': '',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '33.33',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXG'
      },
      {
        'name': 'IHG RC BREAKFAST BB',
        'code': 'IKME0',
        'averageNightlyRate': '110.40',
        'rateInfo': {
          'averageRate': '110.40',
          'currency': 'GBP',
          'totalTaxAmount': '36.80',
          'totalAmountAfterTax': '220.80',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '92.00',
              'amountAfterTax': '110.40',
              'baseAmount': '110.40'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '92.00',
              'amountAfterTax': '110.40',
              'baseAmount': '110.40'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'EXCLUSIVE SAVINGS FOR OUR IHG REWARDS CLUB MEMBERS. MUST BE AN IHG REWARDS CLUB MEMBER TO BOOK. INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '36.80',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXG'
      },
      {
        'name': 'BEST FLEX WITH BKFST',
        'code': 'IGBBB',
        'averageNightlyRate': '115.00',
        'rateInfo': {
          'averageRate': '115.00',
          'currency': 'GBP',
          'totalTaxAmount': '38.33',
          'totalAmountAfterTax': '230.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '38.33',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXG'
      },
      {
        'name': '2000 POINTS PER STAY',
        'code': 'IKHB2',
        'averageNightlyRate': '115.00',
        'rateInfo': {
          'averageRate': '115.00',
          'currency': 'GBP',
          'totalTaxAmount': '38.33',
          'totalAmountAfterTax': '230.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '95.83',
              'amountAfterTax': '115.00',
              'baseAmount': '115.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 2000 IHG REWARD CLUB BONUS POINTS PER STAY.COMPLIMENTARY WIFI INCLUDED.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '38.33',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXG'
      },
      {
        'name': 'DINNER BED AND BKFST',
        'code': 'IBDNG',
        'averageNightlyRate': '155.00',
        'rateInfo': {
          'averageRate': '155.00',
          'currency': 'GBP',
          'totalTaxAmount': '51.67',
          'totalAmountAfterTax': '310.00',
          'totalExtraPersonAmount': '0.00',
          'totalServiceChargeAmount': '0.00',
          'dailyRates': [
            {
              'date': '2019-10-29',
              'amountBeforeTax': '129.17',
              'amountAfterTax': '155.00',
              'baseAmount': '155.00'
            },
            {
              'date': '2019-10-30',
              'amountBeforeTax': '129.17',
              'amountAfterTax': '155.00',
              'baseAmount': '155.00'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'FULLY FLEXIBLE RATE INCLUDES BREAKFAST AND THREE COURSE DINNER FROM SET MENU OR BUFFET IN A DESIGNATED HOTEL RESTAURANT FOR ONE OR TWO RESIDENT ADULTS SHARING A ROOM. ROOM SERVICE MAY INCUR A SUPPLEMENT.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '0.00',
              'earlyDeparture': '50.00',
              'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
              'checkinTime': '14:00:00',
              'checkoutTime': '11:00:00',
              'tax': '51.67',
              'serviceCharge': '0.00',
              'refundable': false
            }
          ],
          'serviceChargeIncluded': false,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'breakfastIncluded': false,
        'pointsEligible': true,
        'parentProductRoomCode': 'KEXG'
      }
    ],
    'productIncludedInOffer': false,
    'serviceChargeIncluded': false,
    'taxesChargeIncluded': true
  }
];

export const offersByProductMockResponse: OffersByProductResponseModel = {
  pageNumber: 1,
  pageSize: 4,
  totalPages: 4,
  totalItems: 14,
  startItem: 1,
  endItem: 4,
  products: mockProducts,
  suggestedOffers: null,
  rateOverrideEligible : true
};
