/* tslint:disable:max-line-length */
import { OffersByRateResponseModel } from '@modules/offers/models/offers.models';

export const offersByRateCategoryMock = [
  {
    'code': 'IKPCM',
    'name': '1000 BONUS POINTS TEST',
    'products': [
      {
        'name': '1 KING BED NONSMOKING',
        'code': 'KNGN',
        'roomsAvailable': '3',
        'description': '',
        'rateInfo': {
          'averageRate': '161.44',
          'currency': 'USD',
          'totalTaxAmount': '26.23',
          'totalAmountAfterTax': '161.44',
          'totalExtraPersonAmount': '30.09',
          'totalServiceChargeAmount': '30.00',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '135.21',
              'amountAfterTax': '161.44',
              'baseAmount': '161.44'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '161.44',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '16.23',
              'serviceCharge': '10.00'
            }
          ],
          'serviceChargeIncluded': true,
          'taxesChargeIncluded': true,
          'extraPersonChargeIncluded': true
        },
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKPCM'
      },
      {
        'name': 'TWO QUEEN BEDS NONSMOKING',
        'code': 'TQNN',
        'roomsAvailable': '88',
        'description': '',
        'rateInfo': {
          'averageRate': '161.44',
          'currency': 'USD',
          'totalTaxAmount': '26.23',
          'totalAmountAfterTax': '161.44',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '135.21',
              'amountAfterTax': '161.44',
              'baseAmount': '161.44'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '161.44',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '16.23',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKPCM'
      },
      {
        'name': '1 KING BED SUITE NONSMOKING',
        'code': 'XKLN',
        'roomsAvailable': '7',
        'description': '',
        'rateInfo': {
          'averageRate': '171.47',
          'currency': 'USD',
          'totalTaxAmount': '27.30',
          'totalAmountAfterTax': '171.47',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '144.17',
              'amountAfterTax': '171.47',
              'baseAmount': '171.47'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '171.47',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '17.30',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKPCM'
      },
      {
        'name': 'STANDARD ROOM NONSMOKING',
        'code': 'CSTN',
        'roomsAvailable': '99',
        'description': '',
        'rateInfo': {
          'averageRate': '166.45',
          'currency': 'USD',
          'totalTaxAmount': '26.76',
          'totalAmountAfterTax': '166.45',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '139.69',
              'amountAfterTax': '166.45',
              'baseAmount': '166.45'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '166.45',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '16.76',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKPCM'
      },
      {
        'name': 'SUITE DELUXE HEAR ACCESSIBLE TUB NOSMK',
        'code': 'XWKN',
        'roomsAvailable': '77',
        'description': '',
        'rateInfo': {
          'averageRate': '171.47',
          'currency': 'USD',
          'totalTaxAmount': '27.30',
          'totalAmountAfterTax': '171.47',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '144.17',
              'amountAfterTax': '171.47',
              'baseAmount': '171.47'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '171.47',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '17.30',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKPCM'
      }
    ],
    'pointsEligible': true,
    'minLengthOfStay': '2',
    'maxLengthOfStay': '4'
  },
  {
    'code': 'IKME3',
    'name': 'IHG RC 1000 PTS NT',
    'products': [
      {
        'name': '1 KING BED NONSMOKING',
        'code': 'KNGN',
        'roomsAvailable': '3',
        'description': '',
        'rateInfo': {
          'averageRate': '153.37',
          'currency': 'USD',
          'totalTaxAmount': '25.36',
          'totalAmountAfterTax': '153.37',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '128.01',
              'amountAfterTax': '153.37',
              'baseAmount': '153.37'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '153.37',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '15.36',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKME3'
      },
      {
        'name': 'TWO QUEEN BEDS NONSMOKING',
        'code': 'TQNN',
        'roomsAvailable': '88',
        'description': '',
        'rateInfo': {
          'averageRate': '153.37',
          'currency': 'USD',
          'totalTaxAmount': '25.36',
          'totalAmountAfterTax': '153.37',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '128.01',
              'amountAfterTax': '153.37',
              'baseAmount': '153.37'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '153.37',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '15.36',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKME3'
      },
      {
        'name': '1 KING BED SUITE NONSMOKING',
        'code': 'XKLN',
        'roomsAvailable': '7',
        'description': '',
        'rateInfo': {
          'averageRate': '162.90',
          'currency': 'USD',
          'totalTaxAmount': '26.38',
          'totalAmountAfterTax': '162.90',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '136.52',
              'amountAfterTax': '162.90',
              'baseAmount': '162.90'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '162.90',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '16.38',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKME3'
      },
      {
        'name': 'STANDARD ROOM NONSMOKING',
        'code': 'CSTN',
        'roomsAvailable': '99',
        'description': '',
        'rateInfo': {
          'averageRate': '158.13',
          'currency': 'USD',
          'totalTaxAmount': '25.87',
          'totalAmountAfterTax': '158.13',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '132.26',
              'amountAfterTax': '158.13',
              'baseAmount': '158.13'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '158.13',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '15.87',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKME3'
      },
      {
        'name': 'SUITE DELUXE HEAR ACCESSIBLE TUB NOSMK',
        'code': 'XWKN',
        'roomsAvailable': '77',
        'description': '',
        'rateInfo': {
          'averageRate': '162.90',
          'currency': 'USD',
          'totalTaxAmount': '26.38',
          'totalAmountAfterTax': '162.90',
          'dailyRates': [
            {
              'date': '2019-09-24',
              'amountBeforeTax': '136.52',
              'amountAfterTax': '162.90',
              'baseAmount': '162.90'
            }
          ],
          'rateRules': [
            {
              'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
              'noShowPolicyDesc': 'Canceling your reservation or failing to arrive will result in forfeiture of your deposit. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
              'extraPersonCharge': '162.90',
              'earlyDeparture': '',
              'guaranteePolicy': '',
              'checkinTime': '15:00:00',
              'checkoutTime': '11:00:00',
              'tax': '16.38',
              'serviceCharge': '10.00'
            }
          ]
        },
        'serviceChargeIncluded': true,
        'taxesChargeIncluded': true,
        'productIncludedInOffer': false,
        'parentItemRateCode': 'IKME3'
      }
    ],
    'pointsEligible': true
  }
];

export const offersByRateMockResponse: OffersByRateResponseModel = {
  'pageNumber': 1,
  'pageSize': 4,
  'totalPages': 1,
  'totalItems': 2,
  'startItem': 1,
  'endItem': 2,
  'offersByRateCategory': offersByRateCategoryMock,
  'rateOverrideEligible': true
};
/* tslint:disable-next-line:max-line-length */

