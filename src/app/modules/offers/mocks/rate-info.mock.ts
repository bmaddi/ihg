/* tslint:disable:max-line-length */
import { RateInfoModel } from '../models/offers.models';

export const mockRateInfo: RateInfoModel = {
  averageRate: '125.00',
  currency: 'GBP',
  totalTaxAmount: '41.67',
  totalAmountAfterTax: '250.00',
  totalExtraPersonAmount: '0.00',
  totalServiceChargeAmount: '0.00',
  dailyRates: [
    {
      date: '2019-10-29',
      amountBeforeTax: '104.17',
      amountAfterTax: '125.00',
      baseAmount: '125.00'
    },
    {
      date: '2019-10-30',
      amountBeforeTax: '104.17',
      amountAfterTax: '125.00',
      baseAmount: '125.00'
    }
  ],
  rateRules: [
    {
      rateRuleDesc: 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. TEST IHG AND RC.',
      noShowPolicyDesc: 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
      extraPersonCharge: '9.00',
      earlyDeparture: '50.00',
      guaranteePolicy: 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
      checkinTime: '14:00:00',
      checkoutTime: '11:00:00',
      tax: '41.67',
      serviceCharge: '45.00',
      refundable: false
    }
  ],
  serviceChargeIncluded: false,
  taxesChargeIncluded: true,
  extraPersonChargeIncluded: true
};

export const rateAfterInfoMockData = {
  'name': 'IHG RC 1000 PTS NT',
  'code': 'IKME3',
  'averageNightlyRate': '126.72',
  'rateInfo': {
    'averageRate': '126.72',
    'currency': 'GBP',
    'totalTaxAmount': '42.24',
    'totalAmountAfterTax': '253.44',
    'totalExtraPersonAmount': '0.00',
    'totalServiceChargeAmount': '0.00',
    'dailyRates': [
      {
        'date': '2019-10-29',
        'amountBeforeTax': '105.60',
        'amountAfterTax': '126.72',
        'baseAmount': '126.72'
      },
      {
        'date': '2019-10-30',
        'amountBeforeTax': '105.60',
        'amountAfterTax': '126.72',
        'baseAmount': '126.72'
      }
    ],
    'rateRules': [
      {
        'rateRuleDesc': 'INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.',
        'noShowPolicyDesc': 'Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n',
        'extraPersonCharge': '0.00',
        'earlyDeparture': '50.00',
        'guaranteePolicy': 'GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR',
        'checkinTime': '14:00:00',
        'checkoutTime': '11:00:00',
        'tax': '42.24',
        'serviceCharge': '0.00',
        'refundable': false
      }
    ],
    'serviceChargeIncluded': false,
    'taxesChargeIncluded': true,
    'extraPersonChargeIncluded': true
  },
  'breakfastIncluded': false,
  'pointsEligible': true,
  'parentProductRoomCode': 'CSTN'
};





