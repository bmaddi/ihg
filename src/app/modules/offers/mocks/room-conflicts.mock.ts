/* tslint:disable:max-line-length */
export const mockRoomConflicts = [
  {
    'firstName': 'AYUSH',
    'lastName': 'TEST',
    'reservationNumber': '22401014',
    'badges': [
      'gold',
      'karma',
      'spire'],
    'checkOutDate': '2019-12-13',
    'roomNumber': '100',
    'roomType': 'KSTG',
    'checkInDate': '2019-12-10',
    'reservationStatus': '',
    'pmsConfirmationNumber': '483095',
    'doNotMoveRoom': true,
    'guestName': 'TEST, AYUSH',
    'isConflictActive': true,
    'doNotMoveRoomComments': [
      {
        'commentId': 0,
        'userName': null,
        'date': '2019-12-10',
        'reason': 'Guest loves the view for room 100, has stayed here countless times and is the preferred room of choice on every visit. Let\'s not upset our guest'
      }
    ],
    'doNotMoveRoomReason': 'Guest loves the view for room 100, has stayed here countless times and is the preferred room of choice on every visit. Let\'s not upset our guest'
  },
  {
    'firstName': 'TEST',
    'lastName': 'TEST',
    'reservationNumber': '28649094',
    'badges': [
      'gold',
      'karma',
      'spire',
      'ambassador'],
    'checkOutDate': '2019-12-09',
    'roomNumber': '100',
    'roomType': 'KSTG',
    'checkInDate': '2019-12-08',
    'reservationStatus': '',
    'pmsConfirmationNumber': '482366',
    'doNotMoveRoom': false,
    'guestName': 'TEST, TEST',
    'isConflictActive': true,
    'doNotMoveRoomComments': null,
    'doNotMoveRoomReason': null
  },
  {
    'firstName': 'OXI',
    'lastName': 'TEST',
    'reservationNumber': '44220850',
    'badges': [],
    'checkOutDate': '2019-12-12',
    'roomNumber': '100',
    'roomType': 'KSTG',
    'checkInDate': '2019-12-07',
    'reservationStatus': '',
    'pmsConfirmationNumber': '482598',
    'doNotMoveRoom': false,
    'guestName': 'TEST, OXI',
    'isConflictActive': true,
    'doNotMoveRoomComments': null,
    'doNotMoveRoomReason': null
  }
];

export const confirmationPayloadMock = {
  'hotelCode': 'GRVAB',
  'confirmationNumber': '44471012',
  'checkInDate': '2019-12-27',
  'checkOutDate': '2019-12-31',
  'rateCategoryCode': 'IBDNG',
  'roomTypeCode': 'ODXG',
  'numberOfRooms': 1,
  'adultQuantity': 1,
  'childQuantity': 0,
  'ihgRcNumber': '119200393',
  'firstName': 'Test User B',
  'middleName': '',
  'lastName': 'Angels'
};

export const mockListItem = {
  'inspected': true,
  'hotelCode': 'GRVAB',
  'confirmationNumber': '44471012',
  'checkInDate': '2019-12-27',
  'checkOutDate': '2019-12-31',
  'rateCategoryCode': 'IBDNG',
  'roomTypeCode': 'OSTG',
  'numberOfRooms': 1,
  'adultQuantity': 1,
  'childQuantity': 0,
  'ihgRcNumber': '119200393',
  'firstName': 'Test User B',
  'middleName': '',
  'lastName': 'Angels',
  'roomType': 'OSTG',
  'pmsReservationNumber': '49169',
  'reservationNumber': '44471012'
};
