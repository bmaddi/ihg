export const MOCK_SUGGESTED_OFFRS = {
  "defaultBedType": "OTHER",
  "suggestedProducts": {
    "ONE_BED": {
      "code": "KNGN",
      "name": "1 KING BED NONSMOKING",
      "description": "",
      "roomsAvailable": "99",
      "bedType": "ONE_BED",
      "rateCategories": [
        {
          "name": "BEST FLEXIBLE RATES1",
          "code": "IGCOR",
          "averageNightlyRate": "125.34",
          "isBreakfastIncluded": true,
          "rateInfo": {
            "averageRate": "125.34",
            "currency": "USD",
            "totalTaxAmount": "12.36",
            "totalAmountAfterTax": "125.34",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "102.98",
                "amountAfterTax": "125.34",
                "baseAmount": "125.34"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "Best and least restrictive public rate. Changes usually acceptable, but check hotel cancellation policy before booking.",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "12.36",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        },
        {
          "name": "AD002",
          "code": "AD002",
          "averageNightlyRate": "142.87",
          "isBreakfastIncluded": false,
          "rateInfo": {
            "averageRate": "142.87",
            "currency": "USD",
            "totalTaxAmount": "14.24",
            "totalAmountAfterTax": "142.87",
            "totalExtraPersonAmount": "",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "118.63",
                "amountAfterTax": "142.87",
                "baseAmount": "142.87"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "14.24",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        },
        {
          "name": "IHG RC BEST FLEXIBLE",
          "code": "IDME0",
          "averageNightlyRate": "119.07",
          "isBreakfastIncluded": true,
          "rateInfo": {
            "averageRate": "119.07",
            "currency": "USD",
            "totalTaxAmount": "11.69",
            "totalAmountAfterTax": "119.07",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "97.38",
                "amountAfterTax": "119.07",
                "baseAmount": "119.07"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "Exclusive savings for our IHG Rewards Club Members. Must be an IHG Rewards Club Member to book. ",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "11.69",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        }
      ],
      "productIncludedInOffer": false,
      "serviceChargeIncluded": true,
      "taxesChargeIncluded": true
    },
    "TWO_BEDS": {
      "code": "TQNN",
      "name": "TWO QUEEN BEDS NONSMOKING",
      "description": "",
      "roomsAvailable": "99",
      "bedType": "TWO_BEDS",
      "rateCategories": [
        {
          "name": "BEST FLEXIBLE RATES1",
          "code": "IGCOR",
          "averageNightlyRate": "125.34",
          "isBreakfastIncluded": true,
          "rateInfo": {
            "averageRate": "125.34",
            "currency": "USD",
            "totalTaxAmount": "12.36",
            "totalAmountAfterTax": "125.34",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "102.98",
                "amountAfterTax": "125.34",
                "baseAmount": "125.34"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "Best and least restrictive public rate. Changes usually acceptable, but check hotel cancellation policy before booking.",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "12.36",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        },
        {
          "name": "AD002",
          "code": "AD002",
          "averageNightlyRate": "142.87",
          "isBreakfastIncluded": true,
          "rateInfo": {
            "averageRate": "142.87",
            "currency": "USD",
            "totalTaxAmount": "14.24",
            "totalAmountAfterTax": "142.87",
            "totalExtraPersonAmount": "",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "118.63",
                "amountAfterTax": "142.87",
                "baseAmount": "142.87"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "14.24",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        },
        {
          "name": "IHG RC BEST FLEXIBLE",
          "code": "IDME0",
          "averageNightlyRate": "119.07",
          "isBreakfastIncluded": false,
          "rateInfo": {
            "averageRate": "119.07",
            "currency": "USD",
            "totalTaxAmount": "11.69",
            "totalAmountAfterTax": "119.07",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "97.38",
                "amountAfterTax": "119.07",
                "baseAmount": "119.07"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "Exclusive savings for our IHG Rewards Club Members. Must be an IHG Rewards Club Member to book. ",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "11.69",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        }
      ],
      "productIncludedInOffer": false,
      "serviceChargeIncluded": true,
      "taxesChargeIncluded": true
    },
    "SUITE": {
      "code": "XKLN",
      "name": "1 KING BED SUITE NONSMOKING",
      "description": "",
      "roomsAvailable": "13",
      "bedType": "SUITE",
      "rateCategories": [
        {
          "name": "BEST FLEXIBLE RATES1",
          "code": "IGCOR",
          "averageNightlyRate": "135.37",
          "rateInfo": {
            "averageRate": "135.37",
            "currency": "USD",
            "totalTaxAmount": "13.43",
            "totalAmountAfterTax": "135.37",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "111.94",
                "amountAfterTax": "135.37",
                "baseAmount": "135.37"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "Best and least restrictive public rate. Changes usually acceptable, but check hotel cancellation policy before booking.",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "13.43",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        },
        {
          "name": "AD002",
          "code": "AD002",
          "averageNightlyRate": "153.91",
          "rateInfo": {
            "averageRate": "153.91",
            "currency": "USD",
            "totalTaxAmount": "15.42",
            "totalAmountAfterTax": "153.91",
            "totalExtraPersonAmount": "",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "128.49",
                "amountAfterTax": "153.91",
                "baseAmount": "153.91"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "15.42",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        },
        {
          "name": "IHG RC BEST FLEXIBLE",
          "code": "IDME0",
          "averageNightlyRate": "128.60",
          "rateInfo": {
            "averageRate": "128.60",
            "currency": "USD",
            "totalTaxAmount": "12.71",
            "totalAmountAfterTax": "128.60",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "10.00",
            "dailyRates": [
              {
                "date": "2019-10-25",
                "amountBeforeTax": "105.89",
                "amountAfterTax": "128.60",
                "baseAmount": "128.60"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "Exclusive savings for our IHG Rewards Club Members. Must be an IHG Rewards Club Member to book. ",
                "noShowPolicyDesc": "Canceling your reservation before 12:00 PM (local hotel time) on Friday, 25 October, 2019 will result in no charge. Canceling your reservation after 12:00 PM (local hotel time) on 25 October, 2019, or failing to show, will result in a charge of 1 night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "15:00:00",
                "checkoutTime": "11:00:00",
                "tax": "12.71",
                "serviceCharge": "10.00",
                "isRefundable": true
              }
            ],
            "serviceChargeIncluded": true,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "pointsEligible": true
        }
      ],
      "productIncludedInOffer": false,
      "serviceChargeIncluded": true,
      "taxesChargeIncluded": true
    },
    "OTHER": {
      "code": "CSTN",
      "name": "STANDARD ROOM NONSMOKING",
      "description": "WHEN YOU ARRIVE WE WILL DO OUR BEST TO MEET YOUR ROOM BED TYPE PREFERENCE THESE ARE SUBJECT TO AVAILABILITY.",
      "roomsAvailable": "73",
      "bedType": "OTHER",
      "rateCategories": [
        {
          "name": "BEST FLEXIBLE RATE",
          "code": "IGCOR",
          "averageNightlyRate": "125.00",
          "rateInfo": {
            "averageRate": "125.00",
            "currency": "GBP",
            "totalTaxAmount": "41.67",
            "totalAmountAfterTax": "250.00",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "0.00",
            "dailyRates": [
              {
                "date": "2019-10-29",
                "amountBeforeTax": "104.17",
                "amountAfterTax": "125.00",
                "baseAmount": "125.00"
              },
              {
                "date": "2019-10-30",
                "amountBeforeTax": "104.17",
                "amountAfterTax": "125.00",
                "baseAmount": "125.00"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "",
                "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "50.00",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "14:00:00",
                "checkoutTime": "11:00:00",
                "tax": "41.67",
                "serviceCharge": "0.00",
                "refundable": false
              }
            ],
            "serviceChargeIncluded": false,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "breakfastIncluded": true,
          "pointsEligible": true
        },
        {
          "name": "IHG RC 1000 PTS NT",
          "code": "IKME3",
          "averageNightlyRate": "126.72",
          "rateInfo": {
            "averageRate": "126.72",
            "currency": "GBP",
            "totalTaxAmount": "42.24",
            "totalAmountAfterTax": "253.44",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "0.00",
            "dailyRates": [
              {
                "date": "2019-10-29",
                "amountBeforeTax": "105.60",
                "amountAfterTax": "126.72",
                "baseAmount": "126.72"
              },
              {
                "date": "2019-10-30",
                "amountBeforeTax": "105.60",
                "amountAfterTax": "126.72",
                "baseAmount": "126.72"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY.",
                "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "50.00",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "14:00:00",
                "checkoutTime": "11:00:00",
                "tax": "42.24",
                "serviceCharge": "0.00",
                "refundable": false
              }
            ],
            "serviceChargeIncluded": false,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "breakfastIncluded": true,
          "pointsEligible": true
        },
        {
          "name": "1000 BONUS POINTS NT",
          "code": "IKPCM",
          "averageNightlyRate": "132.00",
          "rateInfo": {
            "averageRate": "132.00",
            "currency": "GBP",
            "totalTaxAmount": "44.00",
            "totalAmountAfterTax": "264.00",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "0.00",
            "dailyRates": [
              {
                "date": "2019-10-29",
                "amountBeforeTax": "110.00",
                "amountAfterTax": "132.00",
                "baseAmount": "132.00"
              },
              {
                "date": "2019-10-30",
                "amountBeforeTax": "110.00",
                "amountAfterTax": "132.00",
                "baseAmount": "132.00"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "INCLUDES ROOM AND 1000 REWARDS CLUB BONUS POINTS PER NIGHT IF YOU COLLECT MILES WITH PARTICIPATING AIRLINES POINTS CONVERT INTO MILES. BONUS WILL BE FULFILLED ELECTRONICALLY.",
                "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "50.00",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "14:00:00",
                "checkoutTime": "11:00:00",
                "tax": "44.00",
                "serviceCharge": "0.00",
                "refundable": false
              }
            ],
            "serviceChargeIncluded": false,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "breakfastIncluded": true,
          "pointsEligible": true
        },
        {
          "name": "IHG RC BREAKFAST BB",
          "code": "IKME0",
          "averageNightlyRate": "134.40",
          "rateInfo": {
            "averageRate": "134.40",
            "currency": "GBP",
            "totalTaxAmount": "44.80",
            "totalAmountAfterTax": "268.80",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "0.00",
            "dailyRates": [
              {
                "date": "2019-10-29",
                "amountBeforeTax": "112.00",
                "amountAfterTax": "134.40",
                "baseAmount": "134.40"
              },
              {
                "date": "2019-10-30",
                "amountBeforeTax": "112.00",
                "amountAfterTax": "134.40",
                "baseAmount": "134.40"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "EXCLUSIVE SAVINGS FOR OUR IHG REWARDS CLUB MEMBERS. MUST BE AN IHG REWARDS CLUB MEMBER TO BOOK. INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.",
                "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "50.00",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "14:00:00",
                "checkoutTime": "11:00:00",
                "tax": "44.80",
                "serviceCharge": "0.00",
                "refundable": false
              }
            ],
            "serviceChargeIncluded": false,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "breakfastIncluded": true,
          "pointsEligible": true
        },
        {
          "name": "BEST FLEX WITH BKFST",
          "code": "IGBBB",
          "averageNightlyRate": "140.00",
          "rateInfo": {
            "averageRate": "140.00",
            "currency": "GBP",
            "totalTaxAmount": "46.67",
            "totalAmountAfterTax": "280.00",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "0.00",
            "dailyRates": [
              {
                "date": "2019-10-29",
                "amountBeforeTax": "116.67",
                "amountAfterTax": "140.00",
                "baseAmount": "140.00"
              },
              {
                "date": "2019-10-30",
                "amountBeforeTax": "116.67",
                "amountAfterTax": "140.00",
                "baseAmount": "140.00"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "INCLUDES BREAKFAST FOR ONE OR TWO ADULTS SHARING A ROOM.",
                "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "50.00",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "14:00:00",
                "checkoutTime": "11:00:00",
                "tax": "46.67",
                "serviceCharge": "0.00",
                "refundable": false
              }
            ],
            "serviceChargeIncluded": false,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "breakfastIncluded": true,
          "pointsEligible": true
        },
        {
          "name": "2000 POINTS PER STAY",
          "code": "IKHB2",
          "averageNightlyRate": "140.00",
          "rateInfo": {
            "averageRate": "140.00",
            "currency": "GBP",
            "totalTaxAmount": "46.67",
            "totalAmountAfterTax": "280.00",
            "totalExtraPersonAmount": "0.00",
            "totalServiceChargeAmount": "0.00",
            "dailyRates": [
              {
                "date": "2019-10-29",
                "amountBeforeTax": "116.67",
                "amountAfterTax": "140.00",
                "baseAmount": "140.00"
              },
              {
                "date": "2019-10-30",
                "amountBeforeTax": "116.67",
                "amountAfterTax": "140.00",
                "baseAmount": "140.00"
              }
            ],
            "rateRules": [
              {
                "rateRuleDesc": "INCLUDES ROOM AND 2000 IHG REWARD CLUB BONUS POINTS PER STAY.COMPLIMENTARY WIFI INCLUDED.",
                "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
                "extraPersonCharge": "0.00",
                "earlyDeparture": "50.00",
                "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
                "checkinTime": "14:00:00",
                "checkoutTime": "11:00:00",
                "tax": "46.67",
                "serviceCharge": "0.00",
                "refundable": false
              }
            ],
            "serviceChargeIncluded": false,
            "taxesChargeIncluded": true,
            "extraPersonChargeIncluded": true
          },
          "breakfastIncluded": true,
          "pointsEligible": true
        }
      ],
      "productIncludedInOffer": false,
      "serviceChargeIncluded": false,
      "taxesChargeIncluded": true
    }
  }
}

export const MOCK_SEARCHCRITERIA = {
  "accessible": false,
  "adultsCount": 1,
  "arrivalDate": "2019-10-18",
  "childCount": 0,
  "corporateId": null,
  "departureDate": "2019-10-19",
  "groupCode": null,
  "guestInfo": {
    "firstName": "debasmita",
    "lastName": "pradhan",
    "middleName": "",
    "addressLine1": "",
    "addressLine2": "",
    "countryCode": "",
    "countryName": "",
    "zipCode": "",
    "stateProvince": "",
    "cityName": "",
    "phoneType": "",
    "phoneNumber": "12111",
    "email": ""
  },
  "ihgRcNumber": "",
  "numberOfNights": 1,
  "numberOfRooms": 1,
  "preference": null,
  "rateCategory": "IGCOR",
  "roomType": "CSTN",
  "rateInfo": {
    "averageRate": "100.00",
    "currency": "GBP",
    "totalTaxAmount": "16.67",
    "totalAmountAfterTax": "100.00",
    "totalExtraPersonAmount": "0.00",
    "totalServiceChargeAmount": "0.00",
    "dailyRates": [
      {
        "date": "2019-10-18",
        "amountBeforeTax": "83.33",
        "amountAfterTax": "100.00",
        "baseAmount": "100.00"
      }
    ],
    "rateRules": [
      {
        "rateRuleDesc": "",
        "noShowPolicyDesc": "",
        "extraPersonCharge": "0.00",
        "earlyDeparture": "",
        "guaranteePolicy": "",
        "checkinTime": "",
        "checkoutTime": "",
        "tax": "16.67",
        "serviceCharge": "0.00"
      }
    ],
    "serviceChargeIncluded": false,
    "taxesChargeIncluded": true,
    "extraPersonChargeIncluded": true
  },
  "hotelCode": "GRVAB",
  "confirmationNumber": "47593592",
  "searchType": ""
}

export const MOCK_ORIGINAL_RESERV = {
  "hotelCode": "GRVAB",
  "loyaltyMembershipId": "",
  "confirmationNumber": "47593592",
  "roomCategoryCode": "",
  "corporateId": "",
  "checkInDate": null,
  "checkOutDate": "2019-10-19",
  "confirmationDate": null,
  "roomNumber": null,
  "iataNumber": null,
  "amenityPoints": null,
  "arrivalDate": "2019-10-18",
  "numberOfNights": 1,
  "numberOfRooms": 1,
  "adultQuantity": 1,
  "childQuantity": 0,
  "rateCategoryCode": "IGCOR",
  "roomTypeCode": "KDXG",
  "groupCode": "",
  "appUserId": "",
  "rateInfo": {
    "averageRate": "100.00",
    "currency": "GBP",
    "totalTaxAmount": "16.67",
    "totalAmountAfterTax": "100.00",
    "totalExtraPersonAmount": "0.00",
    "totalServiceChargeAmount": "0.00",
    "dailyRates": [
      {
        "date": "2019-10-18",
        "amountBeforeTax": "83.33",
        "amountAfterTax": "100.00",
        "baseAmount": "100.00"
      }
    ],
    "rateRules": [
      {
        "rateRuleDesc": "",
        "noShowPolicyDesc": "",
        "extraPersonCharge": "0.00",
        "earlyDeparture": "",
        "guaranteePolicy": "",
        "checkinTime": "",
        "checkoutTime": "",
        "tax": "16.67",
        "serviceCharge": "0.00"
      }
    ],
    "serviceChargeIncluded": false,
    "taxesChargeIncluded": true,
    "extraPersonChargeIncluded": true
  },
  "guestInfo": {
    "firstName": "debasmita",
    "lastName": "pradhan",
    "middleName": "",
    "addressLine1": "",
    "addressLine2": "",
    "countryCode": "",
    "countryName": "",
    "zipCode": "",
    "stateProvince": "",
    "cityName": "",
    "phoneType": "",
    "phoneNumber": "12111",
    "email": ""
  },
  "rateCategories": [
    "ADAVP",
    "AG000",
    "IBDNG",
    "IBGP2"
  ],
  "doNotMoveRoom": false,
  "productIncludedInOffer": false,
  "serviceChargeIncluded": false,
  "taxesChargeIncluded": true,
  "confirmationDateString": "",
  "checkInDateString": "",
  "checkOutDateString": "2019-10-19"
}