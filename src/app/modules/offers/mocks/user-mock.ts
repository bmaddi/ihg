export const USER_MOCK_VALID_OVERRIDE_ROLES = {
    roles: [
        {
            id: 5000,
            name: 'HOTEL_MANAGEMENT',
            description: 'HOTEL_MANAGEMENT',
            functions: null
        },
        {
            id: 5004,
            name: 'FRONT_DESK_MANAGEMENT',
            description: 'FRONT_DESK_MANAGEMENT',
            functions: null
        },
        {
            id: 5020,
            name: 'CORPORATE_VIEW_ONLY',
            description: 'CORPORATE_VIEW_ONLY',
            functions: null
        },
        {
            id: 5022,
            name: 'RMS - PRODUCT ADMIN',
            description: 'RMS - PRODUCT ADMIN',
            functions: null
        }
      ],
};

export const USER_MOCK_INVALID_OVERRIDE_ROLES = {
    roles: [
        {
            id: 9999,
            name: 'INVALID ROLE',
            description: 'INVALID TEST ROLE',
            functions: null
        }
    ]
};

export const USER_MOCK_UNDEFINED_ROLES = {
    roles: undefined
};
