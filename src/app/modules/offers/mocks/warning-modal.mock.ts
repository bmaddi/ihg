import { StayInfoSearchModel } from "@app/modules/stay-info/models/stay-information.models";

export const MOCK_RATEINFO_AFTER = {
  "name": "BEST FLEXIBLE RATE",
  "code": "IGCOR",
  "averageNightlyRate": "125.00",
  "rateInfo": {
    "averageRate": "125.00",
    "currency": "GBP",
    "totalTaxAmount": "41.67",
    "totalAmountAfterTax": "250.00",
    "totalExtraPersonAmount": "0.00",
    "totalServiceChargeAmount": "0.00",
    "dailyRates": [
      {
        "date": "2019-10-29",
        "amountBeforeTax": "104.17",
        "amountAfterTax": "125.00",
        "baseAmount": "125.00"
      },
      {
        "date": "2019-10-30",
        "amountBeforeTax": "104.17",
        "amountAfterTax": "125.00",
        "baseAmount": "125.00"
      }
    ],
    "rateRules": [
      {
        "rateRuleDesc": "INCLUDES ROOM AND 1000 REWARDS CLUB POINTS PER NIGHT. BONUS WILL BE FULFILLED ELECTRONICALLY. ihg rc test content.",
        "noShowPolicyDesc": "Canceling your reservation or failing to show will result in a charge for the first night per room to your credit card. Taxes may apply. Failing to call or show before check-out time after the first night of a reservation will result in cancellation of the remainder of your reservation.\r\n",
        "extraPersonCharge": "2220.00",
        "earlyDeparture": "50.00",
        "guaranteePolicy": "GUARANTEE METHOD: ACCEPTED CREDIT CARDS PRE-APRVD ARC-IATA NBR",
        "checkinTime": "14:00:00",
        "checkoutTime": "11:00:00",
        "tax": "41.67",
        "serviceCharge": "45.00",
        "refundable": false
      }
    ],
    "serviceChargeIncluded": false,
    "taxesChargeIncluded": true,
    "extraPersonChargeIncluded": true
  },
  "breakfastIncluded": false,
  "pointsEligible": true,
  "parentProductRoomCode": "CSTN"
}

export const MOCK_ORIGINAL_RESERVATION_DATA = {
  "accessible": false,
  "adultsCount": 1,
  "childCount": 0,
  "departureDate": "2019-10-19",
  "hotelCode": "GRVAB",
  "confirmationNumber": "47593592",
  "corporateId": "",
  "checkOutDate": "2019-10-19",
  "roomNumber": null,
  "arrivalDate": "2019-10-18",
  "numberOfNights": 1,
  "numberOfRooms": 1,
  "roomTypeCode": "KDXG",
  "rateCategory": "IGCOR",
  "groupCode": "123456",
  "rateInfo": {
    "averageRate": "100.00",
    "currency": "GBP",
    "totalTaxAmount": "16.67",
    "totalAmountAfterTax": "100.00",
    "totalExtraPersonAmount": "0.00",
    "totalServiceChargeAmount": "0.00",
    "dailyRates": [
      {
        "date": "2019-10-18",
        "amountBeforeTax": "83.33",
        "amountAfterTax": "100.00",
        "baseAmount": "100.00"
      }
    ],
    "rateRules": [
      {
        "rateRuleDesc": "",
        "noShowPolicyDesc": "",
        "extraPersonCharge": "0.00",
        "earlyDeparture": "",
        "guaranteePolicy": "",
        "checkinTime": "",
        "checkoutTime": "",
        "tax": "16.67",
        "serviceCharge": "0.00"
      }
    ],
    "serviceChargeIncluded": false,
    "taxesChargeIncluded": true,
    "extraPersonChargeIncluded": true
  },
  "guestInfo": {
    "firstName": "This is a Large Text.This is a Large Text.This is a Large Text.",
    "lastName": "A",
    "middleName": "",
    "addressLine1": "",
    "addressLine2": "",
    "countryCode": "",
    "countryName": "",
    "zipCode": "",
    "stateProvince": "",
    "cityName": "",
    "phoneType": "",
    "phoneNumber": "12111",
    "email": ""
  },
  "doNotMoveRoom": false,
  "productIncludedInOffer": false,
  "serviceChargeIncluded": false,
  "taxesChargeIncluded": true,
  "ihgRcNumber": "",
  "preference": null,
  "roomType": "CSTN"
}

export const MOCK_UPDATE_TOTALS_RESPONSE = {
  "status": "SUCCESS",
  "reservationData": {
    "prefixName": "Mr",
    "firstName": "Saurav",
    "lastName": "Agrawal",
    "suffixName": "",
    "reservationNumber": "44002733",
    "pmsReservationNumber": "447469",
    "pmsReservationStatus": "DUEIN",
    "pmsUniqueNumber": "466611",
    "arrivalTime": "",
    "checkInDate": "2019-06-05",
    "checkOutDate": "2019-06-07",
    "numberOfNights": 2,
    "ihgRcNumber": "123456",
    "ihgRcMembership": "Spire Elite",
    "ihgRcPoints": 5000,
    "vipCode": "",
    "isKeyCut": false,
    "companyName": "",
    "groupName": "",
    "rateCategoryCode": "IGBBB",
    "roomTypeCode": "KEXG",
    "roomTypeDescription": "1 KING BED EXECUTIVE",
    "numberOfRooms": 1,
    "badges": [
      "priorityEnrollment"
    ],
    "assignedRoomNumber": "",
    "assignedRoomStatus": null,
    "numberOfAdults": 1,
    "numberOfChildren": 0,
    "preferences": [],
    "specialRequests": [
      "Jia Ge7.21/299.32;7.20/299.32;7.19/299.32;;ResID:574637246EarliestArriveTime-15:00\\LatestArriveTime-18:00ELONG-574637246GDS RECORD LOCATOR -CO -574637246GDS HOTEL -GRVGGPE--Priority Enrollment"
    ],
    "infoItems": [],
    "prepStatus": "NOTSTARTED",
    "heartBeatActions": null,
    "heartBeatComment": "",
    "heartBeatImprovement": "",
    "heartBeatActionsInd": "",
    "amenityPoints": null,
    "otaFlag": true,
    "resVendorCode": "",
    "ratesInfo": {
      "currencyCode": "USD",
      "totalAmountBeforeTax": "",
      "totalAmountAfterTax": "410.0",
      "totalTaxes": ""
    },
    "rateOverrideInfo": {
      "averageRate":"101.00",
      "currency":"USD",
      "totalTaxAmount":"22.22",
      "totalAmountAfterTax":"224.22",
      "totalExtraPersonAmount":"0.00",
      "totalServiceChargeAmount":"2.00",
      "dailyRates": [
        {
          "date":"2020-02-14",
          "amountBeforeTax":"100.00",
          "amountAfterTax":"112.11",
          "baseAmount":"101.00"
        },
        {
          "date":"2020-02-15",
          "amountBeforeTax":"100.00",
          "amountAfterTax":"112.11",
          "baseAmount":"101.00"
        }
      ],
      "rateRules":[
        {
          "rateRuleDesc":"",
          "noShowPolicyDesc":"",
          "extraPersonCharge":"0.00",
          "earlyDeparture":"",
          "guaranteePolicy":"",
          "checkinTime":"",
          "checkoutTime":"",
          "tax":"22.22",
          "serviceCharge":"2.00",
          "refundable":false
        }
      ],
      "serviceChargeIncluded":true,
      "taxesChargeIncluded":false,
      "extraPersonChargeIncluded":true
    },
    "payment": null,
    "confirmationDate": "2019-07-26",
    "corporateId": "",
    "iataNumber": null,
    "selectedAmenity": "",
    "paymentType": "OTHER",
    "keysCut": false
  }
}

export const MOCK_SEARCH_CRITERIA = {
  "accessible": false,
  "adultsCount": 1,
  "arrivalDate": "2019-10-18",
  "childCount": 0,
  "corporateId": null,
  "departureDate": "2019-10-19",
  "groupCode": "",
  "guestInfo": {
    "firstName": "This is a long text.This is a long text.This is a long text.",
    "lastName": "A",
    "middleName": "",
    "addressLine1": "",
    "addressLine2": "",
    "countryCode": "",
    "countryName": "",
    "zipCode": "",
    "stateProvince": "",
    "cityName": "",
    "phoneType": "",
    "phoneNumber": "12111",
    "email": ""
  },
  "ihgRcNumber": "",
  "numberOfNights": 1,
  "numberOfRooms": 1,
  "preference": null,
  "rateCategory": "IGCOR",
  "roomType": "CSTN",
  "rateInfo": {
    "averageRate": "100.00",
    "currency": "GBP",
    "totalTaxAmount": "16.67",
    "totalAmountAfterTax": "100.00",
    "totalExtraPersonAmount": "0.00",
    "totalServiceChargeAmount": "0.00",
    "dailyRates": [
      {
        "date": "2019-10-18",
        "amountBeforeTax": "83.33",
        "amountAfterTax": "100.00",
        "baseAmount": "100.00"
      }
    ],
    "rateRules": [
      {
        "rateRuleDesc": "",
        "noShowPolicyDesc": "",
        "extraPersonCharge": "0.00",
        "earlyDeparture": "",
        "guaranteePolicy": "",
        "checkinTime": "",
        "checkoutTime": "",
        "tax": "16.67",
        "serviceCharge": "0.00"
      }
    ],
    "serviceChargeIncluded": false,
    "taxesChargeIncluded": true,
    "extraPersonChargeIncluded": true
  },
  "hotelCode": "GRVAB",
  "confirmationNumber": "47593592",
  "searchType": ""
}

export const MOCK_UPDATE_TOTALS_ERROR = {
  errors: [{
      code: "Error code",
      message: "Error message"
  }]
}