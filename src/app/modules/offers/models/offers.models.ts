import { StayInfoSearchModel, StayInformationModel } from '@modules/stay-info/models/stay-information.models';

export interface RateCategoryModel {
  averageNightlyRate: string;
  code: string;
  name: string;
  pointsEligible: boolean;
  rateInfo: RateInfoModel;
  parentProductRoomCode?: string;
  parentItemRateCode?: string;
  breakfastIncluded?: boolean;
  selected?: boolean;
}

export interface DailyRateModel {
  date: string;
  amountBeforeTax: string;
  amountAfterTax: string;
  dateRange?: string;
  baseAmount: string;
}

export interface RateInfoModel {
  averageRate?: string;
  currency?: string;
  totalTaxAmount?: string;
  totalAmountAfterTax: string;
  dailyRates: DailyRateModel[];
  currencyCode?: string;
  rateRules: RateRuleModel[];
  taxesChargeIncluded?: boolean;
  serviceChargeIncluded?: boolean;
  productIncludedInOffer?: boolean;
  extraPersonChargeIncluded?: boolean;
  totalExtraPersonAmount?: string;
  totalServiceChargeAmount?: string;
  isBreakfastIncluded?: boolean;
}

export interface ProductModel {
  code: string;
  description: string;
  name: string;
  roomsAvailable: string;
  rateInfo?: RateInfoModel;
  rateCategories?: RateCategoryModel[];
  parentItemRateCode?: string;
  parentProductRoomCode?: string;
}

export interface OffersByProductResponseModel {
  pageNumber: number;
  pageSize: number;
  startItem: number;
  endItem: number;
  totalItems: number;
  totalPages: number;
  rateOverrideEligible?: boolean;
  products: ProductModel[];
  suggestedOffers: SuggestedOffersModel;
  errors?: [
    {
      code: string;
      detailText: string;
    }
  ];
}

export interface OfferTypeBtnModel {
  label: string;
  cardLabel: string;
  type: string;
  selected: boolean;
  seleniumId: string;
  cardSeleniumId: string;
  gaLabel: string;
  visible: boolean;
}

export interface OfferByRateCategoryModel {
  code: string;
  name: string;
  products: ProductModel[];
  pointsEligible: boolean;
  minLengthOfStay?: string;
  maxLengthOfStay?: string;
}


export interface OffersByRateResponseModel {
  pageNumber: number;
  pageSize: number;
  startItem: number;
  endItem: number;
  totalItems: number;
  totalPages: number;
  rateOverrideEligible?: boolean;
  offersByRateCategory: OfferByRateCategoryModel[];
  errors?: [
    {
      code: string;
      detailText: string;
    }
  ];
}

export interface GuestCheckInModel {
  reservationData?: ReservationDataModel;
  status: string;
  errors?: ErrorsModel[];
}

export interface ErrorsModel {
  code: string;
  message: string;
}

export interface RoomDescriptionModel {
  id?: string;
  label?: string;
  description?: string;
  seleniumId?: {
    header?: string;
    body?: string;
  };
  priority?: number;
  staticText?: string | { included: string; notIncluded: string };
  translateParams?: any;
  applySentenceCase?: boolean;
  allCapsWords?: string[];
}

export interface GuestWalkInModel extends ReservationDataModel {
  guestCount?: number;
  hotelCode: string;
}

export interface ReservationDataModel {
  amenityPoints?: string;
  prefixName?: string;
  firstName?: string;
  lastName?: string;
  suffixName?: string;
  reservationNumber?: string;
  pmsReservationNumber?: string;
  pmsUniqueNumber?: string;
  checkInDate: string;
  checkOutDate: string;
  arrivalTime?: string;
  ihgRcNumber?: string;
  ihgRcMembership?: string;
  ihgRcPoints?: string;
  vipCode?: string;
  keysCut?: boolean;
  companyName?: string;
  groupName?: string;
  rateCategoryCode?: string;
  roomTypeCode: string;
  roomTypeDescription?: string;
  badges?: string[];
  assignedRoomNumber?: string;
  numberOfNights?: number;
  numberOfRooms?: number;
  numberOfAdults: number;
  numberOfChildren?: number;
  preferences?: string[];
  specialRequests?: string[];
  prepStatus?: string;
  pmsReservationStatus?: string;
  heartBeatComment?: string;
  heartBeatImprovement?: string;
  otaFlag?: boolean;
  resVendorCode?: string;
  inspected?: boolean;
  ratesInfo: RateInfoModel;
  confirmationDate?: string;
  selectedAmenity?: string;
  pointsPosted?: boolean;
  authError?: boolean;
  isCheckedIn?: boolean;
  paymentType?: string;
  ineligibleReservation?: boolean;
  isCutkeyError?: boolean;
  rateOverrideInfo?: RateInfoModel;
}

export interface RateRuleModel {
  roomDesc?: string;
  rateRuleDesc: string;
  noShowPolicyDesc: string;
  extraPersonCharge: string;
  earlyDeparture: string;
  guaranteePolicy: string;
  checkinTime: string;
  checkoutTime: string;
  tax: string;
  serviceCharge: string;
  isRefundable?: boolean;
  refundable?: boolean;
}

export interface ModifyReservationPayloadModel {
  hotelCode?: string;
  confirmationNumber: string;
  checkInDate: string;
  checkOutDate: string;
  rateCategoryCode: string;
  roomTypeCode: string;
  numberOfRooms: string | number;
  adultQuantity: string | number;
  childQuantity: string | number;
  ihgRcNumber: string | number;
  firstName: string;
  middleName?: string;
  lastName: string;
  rateInfo: RateInfoModifyReservationPayloadModel;
}

export interface RateInfoModifyReservationPayloadModel {
  rateOverrideAction: string;
  dailyRates: DailyRateModel[]
}

export interface WarningModalInputModel {
  searchCritera: StayInfoSearchModel;
  originalReservationData: StayInformationModel | RateInfoModel;
  rateInfoAfter: RateCategoryModel | ProductModel;
  reservationNumber: string;
  isFromOfferUnavailable?: boolean;
}

export interface RateModalInputModel {
  rateName: string;
  rateCode: string;
  roomName: string;
  roomCode: string;
  roomDesc: string;
  pointsEligible: boolean;
  rateInfo: RateInfoModel;
  departureDate: string;
  arrivalDate: string;
}


export interface RoomAvailabilityPayloadModel {
  checkInDate: string;
  checkOutDate: string;
  reservationNumber: string;
  roomNumber: string;
  roomType: string;
}

export interface RoomAvailabilityResponseModel {
  availability: boolean;
  errors?: ErrorsModel[];
}

export interface RateCategoryDataModel {
  checkInDate: string;
  checkOutDate: string;
  ihgRcNumber: string;
  roomTypeCode: string;
  rateCategoryCode: string;
}
export interface SuggestedOffersModel {
  defaultBedType: string;
  suggestedProducts: {
    ONE_BED: BedTypeOfferModel;
    TWO_BEDS: BedTypeOfferModel;
    SUITE: BedTypeOfferModel;
    OTHER?: BedTypeOfferModel;
  };
}

export interface BedTypeOfferModel extends ProductModel {
  bedType: string;
  productIncludedInOffer: boolean;
  serviceChargeIncluded: boolean;
  taxesChargeIncluded: boolean;
}

export interface SuggestedOffersTabsModel {
  id: string;
  label: string;
  seleniumId: string;
  roomCountSeleniumId: string;
  type: string;
  gaAction: string;
}

