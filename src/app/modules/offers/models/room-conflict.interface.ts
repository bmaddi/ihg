import { StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { ConflictView } from '@modules/offers/enums/conflict-view.enum';

export interface RoomConflict {
  badges: string[];
  checkInDate: string;
  checkOutDate: string;
  firstName: string;
  lastName: string;
  reservationNumber: string;
  reservationStatus: string;
  doNotMoveRoom?: boolean;
  roomNumber: string;
  roomType: string;
  guestName?: string;
  pmsConfirmationNumber?: string;
  pmsReservationNumber?: string;
  isConflictActive?: boolean;
  doNotMoveRoomReason?: string;
  doNotMoveRoomComments?: {
    commentId?: number;
    userName?: string;
    date?: string;
    reason?: string;
  }[];
}

export interface RoomConflictsMetaData {
  conflicts?: RoomConflict[];
  reservationNumber: string;
  pmsNumber?: string;
  searchCriteria?: StayInfoSearchModel;
  originalReservationData?: StayInfoSearchModel;
  confirmationPayload?: { [key: string]: string | number };
  listItem?: { [key: string]: any };
  conflictViewType?: ConflictView;
  viewChangeEnabled?: boolean;
}

export interface CutKeyModalMetaData {
  conflicts?: RoomConflict[];
  reservationNumber: string;
  pmsNumber: string;
}

export interface RoomConflictsPayload {
  checkInDate: string;
  checkOutDate: string;
  reservationNumber: string;
  roomNumber: string;
  roomType: string;
}
