import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { AppSharedModule } from '@app-shared/app-shared.module';

import { NgxMaskModule } from 'ngx-mask';

import { RatesTableComponent } from './components/rates-table/rates-table.component';
import { OfferTypeButtonsComponent } from './components/offer-type-buttons/offer-type-buttons.component';
import { OfferByProductsGridComponent } from './components/offer-by-products-grid/offer-by-products-grid.component';
import { OfferByRatesGridComponent } from './components/offer-by-rates-grid/offer-by-rates-grid.component';
import { OffersComponent } from './components/offers/offers.component';
import { CollapsibleItemComponent } from './components/collapsible-item/collapsible-item.component';
import { ShowMoreLessComponent } from './components/show-more-less/show-more-less.component';
import { WarningModalComponent } from './components/warning-modal/warning-modal.component';
import { AverageRateComponent } from './components/average-rates-modal/avg-rate.component';
import { ProductsTableComponent } from './components/products-table/products-table.component';
import { ConfirmChangesComponent } from './components/confirm-changes/confirm-changes.component';
import { RoomRateDescriptionModalComponent } from './components/room-rate-description-modal/room-rate-description-modal.component';
import { PriceBreakdownComponent } from './components/price-breakdown/price-breakdown.component';
import { OfferUnavailableModalComponent } from './components/offer-unavailable-modal/offer-unavailable-modal.component';
import { SuggestedOffersTabsComponent } from './components/suggested-offers-tabs/suggested-offers-tabs.component';
import { SuggestedOfferComponent } from './components/suggested-offer/suggested-offer.component';
import { ConflictsModalComponent } from './components/conflicts-modal/conflicts-modal.component';
import { ConflictsGridComponent } from './components/conflicts-grid/conflicts-grid.component';
import { BadgeListModule } from '@modules/badge-list/badge-list.module';
import { CutKeyModalComponent } from '@modules/manage-stay/components/cut-key-modal/cut-key-modal.component';
import { PrepareArrivalsDetailsModule } from '@modules/prepare/components/prepare-arrivals-details/prepare-arrivals-details.module';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    GridModule,
    NgbModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    AppSharedModule,
    RouterModule,
    BadgeListModule,
    PrepareArrivalsDetailsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    RatesTableComponent,
    OfferTypeButtonsComponent,
    OfferByProductsGridComponent,
    OfferByRatesGridComponent,
    OffersComponent,
    CollapsibleItemComponent,
    ShowMoreLessComponent,
    WarningModalComponent,
    AverageRateComponent,
    ProductsTableComponent,
    ConfirmChangesComponent,
    RoomRateDescriptionModalComponent,
    PriceBreakdownComponent,
    OfferUnavailableModalComponent,
    SuggestedOffersTabsComponent,
    SuggestedOfferComponent,
    OfferUnavailableModalComponent,
    ConflictsModalComponent,
    ConflictsGridComponent
  ],
  exports: [
    OffersComponent,
    ConfirmChangesComponent
  ],
  entryComponents: [
    WarningModalComponent,
    RoomRateDescriptionModalComponent,
    OfferUnavailableModalComponent,
    ConflictsModalComponent,
    CutKeyModalComponent
  ]
})
export class OffersModule { }
