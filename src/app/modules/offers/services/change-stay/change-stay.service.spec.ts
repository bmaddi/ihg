import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormControl, FormGroup } from '@angular/forms';

import { ChangeStayService } from './change-stay.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { MOCK_SEARCHCRITERIA } from '@modules/offers/mocks/suggested-offers-tabs-mock';
import { offersByRateCategoryMock } from '@modules/offers/mocks/offers-by-rates.mock';
import { MOCK_ORIGINAL_RESERVATION_DATA } from '@modules/offers/mocks/warning-modal.mock';

describe('ChangeStayService', () => {
  let service: ChangeStayService;
  const stayInformationForm = new FormGroup({
    departure: new FormControl(),
    ihgRcNumber: new FormControl(),
    adults: new FormControl(),
    children: new FormControl(),
    rateCategory: new FormControl(),
    roomType: new FormControl(),
    corporateID: new FormControl(),
    groupCode: new FormControl(),
    smokingPreference: new FormControl(),
    accessible: new FormControl()
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, HttpClientTestingModule]
    });
    setTestEnvironment();
    service = TestBed.get(ChangeStayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should disable controls when the departure date is reduced', () => {
    const disableAllControlsSpy = spyOn<any>(service, 'disableAllControls').and.callThrough();
    const isReducedStaySpy = spyOn<any>(service, 'isReducedStay').and.returnValue(true);
    service.handleDepartureChange(MOCK_SEARCHCRITERIA, '2019-10-18', stayInformationForm);
    expect(isReducedStaySpy).toHaveBeenCalled();
    expect(disableAllControlsSpy).toHaveBeenCalled();
    expect(Object.keys(stayInformationForm.controls).every((key) => {
      return key !== 'departure' ? stayInformationForm.get(key).disabled : true;
    })).toBeTruthy();
  });

  it('should enable controls when the departure date is changed back', () => {
    const reEnableControlsSpy = spyOn<any>(service, 'reEnableControls').and.callThrough();
    const isReducedStaySpy = spyOn<any>(service, 'isReducedStay').and.returnValue(false);
    service['disabledControls'] = ['adults', 'children', 'rateCategory', 'roomType', 'accessible'];
    service.handleDepartureChange(MOCK_SEARCHCRITERIA, '2019-10-19', stayInformationForm);
    expect(isReducedStaySpy).toHaveBeenCalled();
    expect(reEnableControlsSpy).toHaveBeenCalled();
    expect(service['disabledControls'].every((key) => !stayInformationForm.get(key).disabled)).toBeTruthy();
    expect(service['disabledControls'].length).toEqual(0);
  });

  it('should display min stay warning when departure date falls behind the min stay limit', () => {
    service['offersByRate'] = offersByRateCategoryMock;
    const alert = service.getMinMaxStayAlert('2019-10-19', MOCK_ORIGINAL_RESERVATION_DATA);
    expect(service['exceededMaxStay']).toBeFalsy();
    expect(alert.detailMessage).toEqual('LBL_RC_UNAVBL_MIN_STY');
  });

  it('should display max stay warning when departure date exceeds the max stay limit', () => {
    service['offersByRate'] = offersByRateCategoryMock;
    const alert = service.getMinMaxStayAlert('2019-10-23', MOCK_ORIGINAL_RESERVATION_DATA);
    expect(service['exceededMaxStay']).toBeTruthy();
    expect(alert.detailMessage).toEqual('LBL_RC_UNAVBL_MAX_STY');
  });

});

