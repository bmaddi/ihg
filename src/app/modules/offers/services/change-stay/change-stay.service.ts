import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';

import { Alert, AlertType, UserService } from 'ihg-ng-common-core';

import { StayInformationModel, StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import {
  OfferByRateCategoryModel,
  OffersByProductResponseModel,
  OffersByRateResponseModel,
  ProductModel,
  RateCategoryModel
} from '@modules/offers/models/offers.models';
import { environment } from '@env/environment';
import { manageStayGaConstants } from '@modules/offers/constants/offer-types.constant';
import { ModalHandlerService } from '@modules/offers/services/modal-handler/modal-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ChangeStayService {
  private readonly API_URL = environment.fdkAPI;
  private disabledControls = [];
  private offersByRate: OfferByRateCategoryModel[] = [];
  private exceededMaxStay = false;
  departureRateChange = new Subject<StayInfoSearchModel>();

  constructor(private http: HttpClient,
              private userService: UserService,
              private modalService: ModalHandlerService) {
  }

  public getOffersByRate(searchCriteria: StayInfoSearchModel, displaySpinner = true): Observable<OffersByRateResponseModel> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}offers/offersByRateCategory/${location}?page=1&pageSize=4`;
    const httpOptions = { headers: new HttpHeaders({ spinnerConfig: displaySpinner ? 'Y' : 'N' }) };
    searchCriteria.allowPropertyForceSell = true;
    return this.http.post(apiURl, searchCriteria, httpOptions).pipe(map((response: OffersByRateResponseModel) => {
        this.handleOffersResponse(response);
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        this.offersByRate = [];
        return throwError(error);
      })
    );
  }

  public getOffersByProductsForceSell(searchCriteria: StayInfoSearchModel,
                                      displaySpinner = true,
                                      pagination: { page: number, pageSize } = {
                                        page: 1,
                                        pageSize: 4
                                      }): Observable<OffersByProductResponseModel> {
    const apiURl = `${this.API_URL}offers/offersByProduct/${this.userService.getCurrentLocationId()}`;
    searchCriteria.allowPropertyForceSell = true;
    return this.http.post(
      apiURl,
      searchCriteria,
      {
        headers: new HttpHeaders({spinnerConfig: displaySpinner ? 'Y' : 'N'}),
        params: pagination as any
      }
    ).pipe(
      map((response: OffersByProductResponseModel) => this.mapProductResponse(response)),
      catchError((error: HttpErrorResponse) => throwError(error))
    );
  }

  private handleOffersResponse(response: OffersByRateResponseModel) {
    if (!response.errors) {
      this.mapRateResponse(response);
      this.offersByRate = response ? response.offersByRateCategory : [];
    } else {
      this.offersByRate = [];
    }
  }

  private mapRateResponse(response: OffersByRateResponseModel) {
    response.offersByRateCategory.forEach((item: OfferByRateCategoryModel) => {
      item.products.forEach((product: ProductModel) => {
        product.parentItemRateCode = item.code;
      });
    });
  }

  private mapProductResponse(response: OffersByProductResponseModel): OffersByProductResponseModel {
    if (response) {
      response.products.forEach((item: ProductModel) => {
        item.rateCategories.forEach((rate: RateCategoryModel) => {
          rate.parentProductRoomCode = item.code;
        });
      });
    }
    return response;
  }

  public handleDepartureChange(searchCriteria: StayInfoSearchModel, originalCheckOutDate: string, form: FormGroup) {
    this.checkForReducedStay(searchCriteria, originalCheckOutDate, form);
    this.emitDepartureRateChange(searchCriteria);
  }

  private checkForReducedStay(searchCriteria: StayInfoSearchModel, originalCheckOutDate: string, form: FormGroup) {
    if (this.isReducedStay(searchCriteria.departureDate, originalCheckOutDate)) {
      this.disableAllControls(form);
    } else {
      this.reEnableControls(form);
    }
  }

  public emitDepartureRateChange(searchCriteria: StayInfoSearchModel) {
    this.departureRateChange.next(searchCriteria);
  }

  private isReducedStay(departureDate: Date | string, originalCheckOutDate: string): boolean {
    return moment(departureDate).isBefore(moment(originalCheckOutDate));
  }

  private disableAllControls(form: FormGroup) {
    Object.keys(form.controls).forEach((key => {
      if (key !== 'departure' && !form.get(key).disabled) {
        form.get(key).disable();
        this.disabledControls.push(key);
      }
    }));
  }

  private reEnableControls(form: FormGroup) {
    this.disabledControls.forEach(key => {
      form.get(key).enable();
    });
    this.disabledControls = [];
  }

  public isReducedStayApplicable(departureDate: string, originalReservation: StayInformationModel) {
    return this.isReducedStay(departureDate, originalReservation.checkOutDate) && this.isOfferAvailable() &&
      !this.hasMinMaxStayRestriction(departureDate, originalReservation);
  }

  private hasMinMaxStayRestriction(departureDate: string, originalReservation: StayInformationModel): boolean {
    if (this.offersByRate[0].minLengthOfStay || this.offersByRate[0].maxLengthOfStay) {
      const los = moment(departureDate).diff(moment(originalReservation.arrivalDate), 'days');
      const fellBehindMinStay = los < Number(this.offersByRate[0].minLengthOfStay);
      this.exceededMaxStay = los > Number(this.offersByRate[0].maxLengthOfStay);
      return fellBehindMinStay || this.exceededMaxStay;
    }
    return false;
  }

  private isOfferAvailable(): boolean {
    return !!(this.offersByRate[0] && this.offersByRate[0].products && this.offersByRate[0].products.length);
  }

  public getMinMaxStayAlert(departureDate: string, originalReservation: StayInformationModel): Alert {
    if (this.hasMinMaxStayRestriction(departureDate, originalReservation)) {
      return {
        type: AlertType.Warning,
        message: 'COM_TOAST_WARNING',
        detailMessage: this.exceededMaxStay ? 'LBL_RC_UNAVBL_MAX_STY' : 'LBL_RC_UNAVBL_MIN_STY',
        dismissible: false,
        translateValues: { detailMessage: { rateCategory: this.offersByRate[0].code } }
      };
    }
    return null;
  }

  public openConfirmationModal(searchCriteria: StayInfoSearchModel, originalReservation: StayInformationModel) {
    this.modalService.openWarningModal({
      searchCritera: searchCriteria,
      originalReservationData: originalReservation,
      reservationNumber: originalReservation.confirmationNumber,
      rateInfoAfter: this.offersByRate[0].products[0]
    }, manageStayGaConstants.RATES);
  }

  public clearSessionData() {
    this.disabledControls = [];
    this.offersByRate = [];
  }
}
