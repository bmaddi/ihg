import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ConfirmationModalService } from 'ihg-ng-common-components';

import { ForceSellService } from './force-sell.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';

describe('ForceSellService', () => {
  let service: ForceSellService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, HttpClientTestingModule],
      providers: [ConfirmationModalService],
    });
    service = TestBed.get(ForceSellService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
