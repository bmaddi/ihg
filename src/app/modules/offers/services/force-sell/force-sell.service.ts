import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError, share } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { environment } from '@env/environment';

import {
  UserService,
  UserConfirmationModalConfig
} from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { ForceSellModel } from '@modules/offers/models/force-sell.interface';

@Injectable({
  providedIn: 'root'
})

export class ForceSellService {

  private readonly API_URL = environment.fdkAPI;
  private initialDataLoad$: Observable<ForceSellModel> = null;
  private roomTypeCache = null;

  constructor(
    private confirmationModalService: ConfirmationModalService,
    private http: HttpClient,
    private translate: TranslateService,
    private userService: UserService
  ) {
  }

  getForceSell(roomTypeCode: string): Observable<ForceSellModel> {
    if (this.roomTypeCache !== roomTypeCode) {
      this.initialDataLoad$ = this.invokeForceSell(roomTypeCode).pipe(share());
      this.roomTypeCache = roomTypeCode;
    }
    return this.initialDataLoad$;
  }

  invokeForceSell(roomTypeCode: string): Observable<ForceSellModel> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}rooms/forceSell/${location}?roomType=${roomTypeCode}`;
    return this.http.get(apiURl).pipe(map((response: ForceSellModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  forceSellWarningModal(roomType, roomsForceSell): Observable<void> {
    return this.confirmationModalService.openConfirmationModal(this.getConfirmModalConfig(roomType, roomsForceSell));
  }

  private getConfirmModalConfig(roomType, roomsForceSell): UserConfirmationModalConfig {
    return {
      modalHeader: 'BTN_FRCE_SELL',
      primaryButtonLabel: 'BTN_FRCE_SELL',
      primaryButtonClass: 'btn-primary',
      defaultButtonLabel: 'COM_BTN_CANCEL',
      defaultButtonClass: 'btn-default',
      messageHeader: this.translate.instant('LBL_FORCE_SELL', { roomType, roomsForceSell }),
      windowClass: 'modal-medium',
      dismissible: true
    };
  }

  clearRoomTypeCache() {
    this.roomTypeCache = null;
  }
}
