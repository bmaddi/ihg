import { TestBed } from '@angular/core/testing';

import { ModalHandlerService } from './modal-handler.service';
import { UserService, RouteUtilityService, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { USER_MOCK_VALID_OVERRIDE_ROLES, USER_MOCK_INVALID_OVERRIDE_ROLES } from '../../mocks/user-mock';

class MockGoogleAnalyticsService {

}

class MockRouteUtilityService {
  getCurrentRoute() {

  }
}

class MockUserService {
  getUser() {

  }
}

describe('ModalHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: RouteUtilityService, useClass: MockRouteUtilityService},
      {provide: UserService, useClass: MockUserService},
      {provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService}],
    imports: []
  }));

  it('should be created', () => {
    const service: ModalHandlerService = TestBed.get(ModalHandlerService);
    expect(service).toBeTruthy();
  });

  describe('eligibleForOverride', () => {
    it('should return true when role is valid and state is valid', () => {
      const service: ModalHandlerService = TestBed.get(ModalHandlerService);
      spyOn(TestBed.get(UserService), 'getUser').and.returnValue(USER_MOCK_VALID_OVERRIDE_ROLES);
      spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue('edit-stay');
      expect(service.isEligibleForRateOverride).toBeTruthy();
    });
    it('should return false when role is valid and state is not valid', () => {
      const service: ModalHandlerService = TestBed.get(ModalHandlerService);
      spyOn(TestBed.get(UserService), 'getUser').and.returnValue(USER_MOCK_VALID_OVERRIDE_ROLES);
      spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue('manage-stay');
      expect(service.isEligibleForRateOverride).toBeTruthy();
    });
    it('should return false when role is not valid and state is valid', () => {
      const service: ModalHandlerService = TestBed.get(ModalHandlerService);
      spyOn(TestBed.get(UserService), 'getUser').and.returnValue(USER_MOCK_INVALID_OVERRIDE_ROLES);
      spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue('edit-stay');
      expect(service.isEligibleForRateOverride).toBeTruthy();
    });
    it('should return false when role is not valid and state is valid', () => {
      const service: ModalHandlerService = TestBed.get(ModalHandlerService);
      spyOn(TestBed.get(UserService), 'getUser').and.returnValue(USER_MOCK_INVALID_OVERRIDE_ROLES);
      spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue('edit-stay');
      expect(service.isEligibleForRateOverride).toBeTruthy();
    });
  });
});
