import { Injectable } from '@angular/core';
import { from, Observable, Subject } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService, RouteUtilityService, UserService } from 'ihg-ng-common-core';

import { OfferUnavailableModalComponent } from '../../components/offer-unavailable-modal/offer-unavailable-modal.component';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';
import { RoomRateDescriptionModalComponent } from '../../components/room-rate-description-modal/room-rate-description-modal.component';
import { RateModalInputModel, WarningModalInputModel } from '../../models/offers.models';
import { manageStayGaConstants } from '../../constants/offer-types.constant';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { CutKeyModalMetaData, RoomConflictsMetaData } from '@modules/offers/models/room-conflict.interface';
import { ConflictsModalComponent } from '@modules/offers/components/conflicts-modal/conflicts-modal.component';
import { CutKeyModalComponent } from '@modules/manage-stay/components/cut-key-modal/cut-key-modal.component';
import { AppStateNames } from '@app/modules/shared/enums/app-state-names.enum';
import { get, includes } from 'lodash';
import { VALID_ROLES } from '../../constants/valid-rate-override-roles';

@Injectable({
  providedIn: 'root'
})
export class ModalHandlerService {

  private shopOffers = new Subject<null>();
  shopOffers$ = this.shopOffers.asObservable();

  private rateOverride = new Subject<null>();
  rateOverride$ = this.rateOverride.asObservable();

  constructor(private modalService: NgbModal,
    private gaService: GoogleAnalyticsService,
    private routeUtilityService: RouteUtilityService,
    private userService: UserService) { }

  getShopOffers$(): Observable<null> {
    return this.shopOffers$;
  }

  getRateOverride$(): Observable<null> {
    return this.rateOverride$;
  }

  public openNoOffersModal(shopOffersError: Subject<EmitErrorModel>, isOfferEligible: boolean): NgbModalRef {
    if (!this.modalService.hasOpenModals()) {
      const modal = this.modalService.open(OfferUnavailableModalComponent,
        { backdrop: 'static', keyboard: false });
      modal.componentInstance.shopOffersError = shopOffersError;
      modal.componentInstance.isEligibleForRateOverride = isOfferEligible && this.isEligibleForRateOverride();
      modal.componentInstance.shopOffers.subscribe(() => {
        this.shopOffers.next();
      });
      modal.componentInstance.rateOverride.subscribe(() => {
        this.modalService.dismissAll();
        this.rateOverride.next();
      });

      return modal;
    }
  }

  public openWarningModal(data: WarningModalInputModel, gaAction?: string, openInEditableMode?: boolean): void {
    this.modalService.dismissAll();
    if (true) {
      // this.modalService.hasOpenModals() always retruning true, need to fix this.
      // for time being dismissed all modals and allowing to open new modal
      const modal = this.modalService.open(WarningModalComponent,
        { windowClass: 'confirmation-modal', backdrop: 'static', keyboard: false });
      modal.componentInstance.searchCritera = data.searchCritera;
      modal.componentInstance.originalReservationData = data.originalReservationData;
      modal.componentInstance.rateInfoAfter = data.rateInfoAfter;
      modal.componentInstance.reservationNumber = data.reservationNumber;
      modal.componentInstance.action = gaAction;
      modal.componentInstance.isEligibleForRateOverride = this.isEligibleForRateOverride();
      modal.componentInstance.isOpenInEditableMode = openInEditableMode;
      modal.componentInstance.isFromOfferUnavailable = data.isFromOfferUnavailable;
      if (gaAction) {
        this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, gaAction, manageStayGaConstants.LBL_OFFER_SELECTED);
      }
    }
  }

  public openRoomConflictsModal(data: RoomConflictsMetaData): Observable<string> {
    const modalRef: NgbModalRef = this.modalService.open(ConflictsModalComponent,
      {windowClass: 'conflicts-modal', backdrop: 'static', keyboard: false});
    modalRef.componentInstance.conflicts = data.conflicts;
    modalRef.componentInstance.reservationNumber = data.reservationNumber;
    modalRef.componentInstance.searchCriteria = data.searchCriteria;
    modalRef.componentInstance.originalReservationData = data.originalReservationData;
    modalRef.componentInstance.confirmationPayload = data.confirmationPayload;
    modalRef.componentInstance.listItem = data.listItem;
    modalRef.componentInstance.viewChangeEnabled = data.viewChangeEnabled;
    modalRef.componentInstance.view = data.conflictViewType;
    return from(modalRef.result);
  }

  public openDescriptionModal(data: RateModalInputModel, gaAction?: string): NgbModalRef {
    const modal = this.modalService.open(RoomRateDescriptionModalComponent,
      { windowClass: 'description-modal', backdrop: 'static', keyboard: false });
    modal.componentInstance.rateName = data.rateName;
    modal.componentInstance.rateCode = data.rateCode;
    modal.componentInstance.roomName = data.roomName;
    modal.componentInstance.roomCode = data.roomCode;
    modal.componentInstance.roomDesc = data.roomDesc;
    modal.componentInstance.pointsEligible = data.pointsEligible;
    modal.componentInstance.rateInfo = data.rateInfo;
    modal.componentInstance.arrivalDate = data.arrivalDate;
    modal.componentInstance.departureDate = data.departureDate;

    if (gaAction) {
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, gaAction, manageStayGaConstants.LBL_SELECTED);
    }

    return modal;
  }

  public openCutKeysModal(data: CutKeyModalMetaData): Observable<string> {
    const modalRef: NgbModalRef = this.modalService.open(CutKeyModalComponent,
      {windowClass: 'cut-keys-modal', backdrop: 'static', keyboard: false});
    modalRef.componentInstance.conflicts = data.conflicts;
    modalRef.componentInstance.reservationNumber = data.reservationNumber;
    modalRef.componentInstance.pmsNumber = data.pmsNumber;
    return from(modalRef.result);
  }

  public isEligibleForRateOverride(): boolean {
      return this.isInEditStay() && this.userHasValidRole();
  }

  private isInEditStay(): boolean {
    const currentRoutePath = get(this.routeUtilityService.getCurrentRoute(), ['path'], null);
    return includes(currentRoutePath, AppStateNames.editStay);
  }

  private userHasValidRole(): boolean {
    const userRoles = this.userService.getUser().roles;

    return userRoles ? userRoles
      .some(userRole => VALID_ROLES.map(validRole => validRole.name)
        .indexOf(userRole.name) > -1) : false;
  }
}
