import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { GoogleAnalyticsService, RouteUtilityService, UserService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { OffersService } from './offers.service';
import { googleAnalyticsStub, mockUser } from '@app/constants/app-test-constants';
import { HttpRequest } from '@angular/common/http';
import { RoomConflictsPayload } from '@modules/offers/models/room-conflict.interface';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

describe('OffersService', () => {
  let service: OffersService;
  let httpMockController: HttpTestingController;
  let request: TestRequest;

  const fakeApiCall = <T>(mockResponse: T | any,
                          matchFn: ((req: HttpRequest<any>) => boolean),
                          error: boolean = false,
                          queryParameters: string[] = []) => {
    request = httpMockController.expectOne(matchFn);
    queryParameters.forEach(q => {
      expect(request.request.params.get(q)).toBeDefined();
    });
    error ? request.error(mockResponse) : request.flush(mockResponse);
    httpMockController.verify();
  };

  beforeEach(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ReportIssueService, {
        useValue: {}
      })
      .configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule],
        providers: [GoogleAnalyticsService, UserService, ReportIssueService, RouteUtilityService],
      });
    TestBed.get(UserService).setUser(mockUser);

    service = TestBed.get(OffersService);
    httpMockController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getRoomConflicts should call conflicts API with the proper query string properties', () => {
    service.getRoomConflicts(<RoomConflictsPayload>{
      checkInDate: '2020-11-10',
      checkOutDate: '2020-11-10',
      reservationNumber: '12123123',
      roomNumber: '1231',
      roomType: 'KDXG',
    }).subscribe(data => {
      expect(data.length).toEqual(1);
    });

    fakeApiCall([{
        'firstName': 'AYUSH',
        'lastName': 'TEST',
        'reservationNumber': '22401014',
        'badges': [],
        'checkOutDate': null,
        'roomNumber': '100',
        'roomType': 'KSTG',
        'checkInDate': '2019-12-10',
        'reservationStatus': '',
        'pmsConfirmationNumber': '483095',
        'doNotMoveRoom': true
      }],
      (req: HttpRequest<any>) => req.method === 'GET' && req.url.indexOf('guest/conflicts/') !== -1, false,
      ['checkInDate', 'checkOutDate', 'reservationNumber', 'roomNumber', 'roomType']);
  });

  it('#getRoomConflicts should map conflicts correctly if present', () => {
    service.getRoomConflicts(<RoomConflictsPayload>{
      checkInDate: '2020-11-10',
      checkOutDate: '2020-11-10',
      reservationNumber: '12123123',
      roomNumber: '1231',
      roomType: 'KDXG',
    }).subscribe(data => {
      expect(data[0].guestName).toEqual('TEST, AYUSH');
      expect(data[0].isConflictActive).toEqual(true);
    });

    fakeApiCall([{
      'firstName': 'AYUSH',
      'lastName': 'TEST',
      'reservationNumber': '22401014',
      'badges': [],
      'checkOutDate': null,
      'roomNumber': '100',
      'roomType': 'KSTG',
      'checkInDate': '2019-12-10',
      'reservationStatus': '',
      'pmsConfirmationNumber': '483095',
      'doNotMoveRoom': true
    }], (req: HttpRequest<any>) => req.method === 'GET' && req.url.indexOf('guest/conflicts/') !== -1);
  });

  it('#checkForManageStayEdit handle null scenario and be false', () => {
    spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue(null);
    expect( service.checkForManageStayEdit()).toBeFalsy();
  });

  it('#checkForManageStayEdit should be true if is manage-stay route', () => {
    spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue({path: AppStateNames.manageStay});
    expect(service.checkForManageStayEdit()).toBeTruthy();
  });

  it('#checkForManageStayEdit should be false if is not manage-stay route', () => {
    spyOn(TestBed.get(RouteUtilityService), 'getCurrentRoute').and.returnValue({path: AppStateNames.checkIn});
    expect(service.checkForManageStayEdit()).toBeFalsy();
  });
});
