import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';
import { State } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { get, includes } from 'lodash';
import * as moment from 'moment';
import * as cloneDeep from 'lodash/cloneDeep';

import {
  Alert,
  AlertButton,
  AlertButtonType,
  AlertType,
  GoogleAnalyticsService,
  RouteUtilityService,
  UserService
} from 'ihg-ng-common-core';
import { ReportIssueComponent, ReportIssueService } from 'ihg-ng-common-pages';

import { environment } from '@env/environment';
import { StayInfoSearchModel, StayInformationModel } from '@modules/stay-info/models/stay-information.models';
import { manageStayGaConstants } from '@modules/offers/constants/offer-types.constant';
import {
  GuestCheckInModel, GuestWalkInModel, OfferByRateCategoryModel, OffersByProductResponseModel,
  OffersByRateResponseModel, ProductModel, RateCategoryModel, RoomAvailabilityPayloadModel, RoomAvailabilityResponseModel, DailyRateModel, RateInfoModel
} from '../models/offers.models';
import { MANAGE_STAY_LOAD_ERROR, OFFERS_LOAD_ERROR } from '@app/constants/error-autofill-constants';
import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { hasDoNotPostAccessRoles } from '@modules/do-not-move/constants/do-not-move.constants';
import {
  CutKeyModalMetaData,
  RoomConflict,
  RoomConflictsMetaData,
  RoomConflictsPayload
} from '@modules/offers/models/room-conflict.interface';
import {
  ReleaseRoomResponse,
  RoomInfoQueryAssignReleasePayload,
  RoomsListResponse
} from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { AppConstants } from '@app/constants/app-constants';

@Injectable({
  providedIn: 'root'
})

export class OffersService {

  private readonly API_URL = environment.fdkAPI;
  private readonly maxOffersErrorCount = 6; // Both products and rates will fail simultaneously so 3 x 2
  private readonly errorKeys = {
    products: 'products',
    rates: 'rates'
  };
  private offersErrorCount = {
    [this.errorKeys.products]: 0,
    [this.errorKeys.rates]: 0
  };
  private confirmationSuccess = new Subject<any>();
  private messageSource = new BehaviorSubject(false);
  private showOffers = new BehaviorSubject(false);
  private reservationDataModal: GuestCheckInModel;
  private reservationDataOnly: ReservationDataModel;
  private reservationData = new BehaviorSubject(this.reservationDataModal);
  private newReservationDataModal: BehaviorSubject<GuestWalkInModel> = new BehaviorSubject(null);
  private searchOffersError = new Subject<Alert>();
  private searchOffersErrorRetry = new Subject<number>();
  private showConflictsModal = new Subject<RoomConflictsMetaData>();
  private showCutKeysModal = new Subject<CutKeyModalMetaData>();
  private rateError;
  private productError;

  currentMessage = this.messageSource.asObservable();
  isShowOffers = this.showOffers.asObservable();
  updatedReservationData = this.reservationData.asObservable();
  newReservationData = this.newReservationDataModal.asObservable();
  searchOffersError$ = this.searchOffersError.asObservable();
  searchOffersErrorRetry$ = this.searchOffersErrorRetry.asObservable();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private gaService: GoogleAnalyticsService,
    private reportIssueService: ReportIssueService,
    private modalService: NgbModal,
    private routeUtilityService: RouteUtilityService
  ) { }

  getShowOffers$(): Observable<boolean> {
    return this.isShowOffers;
  }

  private isTimeout(error1, error2): boolean {
    return (!!error1.name && error1.name === 'TimeoutError') || (!!error2.name && error2.name === 'TimeoutError');
  }

  public getOffersByProduct(searchCriteria: StayInfoSearchModel, state: State, resNum: string, isPagination?: boolean): Observable<OffersByProductResponseModel> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}offers/offersByProduct/${location}?${this.getPaginationParams(state)}&reservationNumber=${resNum}`;
    return this.http.post(apiURl, searchCriteria).pipe(map((response: OffersByProductResponseModel) => {
      if (response.errors) {
        this.productError = response.errors[0];
        this.updateOffersErrorCount(this.errorKeys.products);
        this.updateOffersErrorTrigger(true, isPagination);
      } else {
        this.productError = null;
        this.mapProductResponse(response);
        this.resetOffersErrorCount(this.errorKeys.products);
        this.updateOffersErrorTrigger(false, isPagination);
      }
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        this.productError = error;
        this.updateOffersErrorCount(this.errorKeys.products);
        this.updateOffersErrorTrigger(true, isPagination);
        return throwError(error);
      })
    );
  }

  private mapProductResponse(response: OffersByProductResponseModel): void {
    response.products.forEach((item: ProductModel) => {
      item.rateCategories.forEach((rate: RateCategoryModel) => {
        rate.parentProductRoomCode = item.code;
      });
    });
  }

  public getOffersByRate(searchCriteria: StayInfoSearchModel, state: State, resNum: string, isPagination?: boolean): Observable<OffersByRateResponseModel> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}offers/offersByRateCategory/${location}?${this.getPaginationParams(state)}&reservationNumber=${resNum}`;
    return this.http.post(apiURl, searchCriteria).pipe(map((response: OffersByRateResponseModel) => {
      if (response.errors) {
        this.rateError = response.errors[0];
        this.updateOffersErrorCount(this.errorKeys.rates);
        this.updateOffersErrorTrigger(true, isPagination);
      } else {
        this.rateError = null;
        this.mapRateResponse(response);
        this.resetOffersErrorCount(this.errorKeys.rates);
        this.updateOffersErrorTrigger(false, isPagination);
      }
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        this.rateError = error;
        this.updateOffersErrorCount(this.errorKeys.rates);
        this.updateOffersErrorTrigger(true, isPagination);
        return throwError(error);
      })
    );
  }

  private mapRateResponse(response: OffersByRateResponseModel) {
    response.offersByRateCategory.forEach((item: OfferByRateCategoryModel) => {
      item.products.forEach((product: ProductModel) => {
        product.parentItemRateCode = item.code;
      });
    });
  }

  private resetOffersErrorCount(errorType: string): void {
    this.offersErrorCount[errorType] = 0;
  }

  private updateOffersErrorCount(errorType: string): void {
    if (!this.modalService.hasOpenModals()) {
      this.offersErrorCount[errorType]++;
    }
  }

  private getConsolidatedOffersErrorCount(): number {
    return Object.values(this.offersErrorCount).reduce((a, b) => a + b);
  }

  private isMaxOffersErrorCount(): boolean {
    return this.getConsolidatedOffersErrorCount() >= this.maxOffersErrorCount;
  }

  public updateOffersErrorTrigger(hasError: boolean, isPagination?: boolean, noOffers?: boolean): void {
    this.searchOffersError.next(hasError ? this.getOffersErrorAlert() : null);
    if (hasError || noOffers) {
      this.showAllOffers(false);
    } else {
      if (!isPagination) {
        this.showAllOffers(true);
      }
    }
  }

  public checkForManageStayEdit(): boolean {
    const currentRoutePath = get(this.routeUtilityService.getCurrentRoute(), ['path'], null);
    return includes(currentRoutePath, AppStateNames.manageStay);
  }

  public triggerConflictsModal(data: RoomConflictsMetaData) {
    this.showConflictsModal.next(data);
  }

  public getConflictsTriggerObservable(): Observable<RoomConflictsMetaData> {
    return this.showConflictsModal.asObservable()
      .pipe(
        filter(data => data !== null),
      );
  }

  public triggerCutKeyModal(data: CutKeyModalMetaData) {
    this.showCutKeysModal.next(data);
  }

  public getCutKeyTriggerObservable(): Observable<CutKeyModalMetaData> {
    return this.showCutKeysModal.asObservable()
      .pipe(
        filter(data => data !== null),
      );
  }

  public getOffersErrorAlert(): Alert {
    if (this.getConsolidatedOffersErrorCount() > 0) {
      const button = this.isMaxOffersErrorCount() ? this.getReportAnIssueButton() : this.getRetryButton();
      return {
        type: AlertType.Danger,
        message: 'COM_TOAST_ERR',
        detailMessage: 'SRCH_FLD_TRY_AGN',
        dismissible: false,
        buttons: [button]
      };
    } else {
      return null;
    }
  }

  private getRetryButton(): AlertButton {
    return <AlertButton>{
      label: 'LBL_ERR_TRYAGAIN',
      type: AlertButtonType.Danger,
      onClick: () => {
        this.searchOffersErrorRetry.next();
      }
    };
  }

  private getAutoFill(config: any): ReportIssueAutoFillData {
    if (!!config.timeoutError) {
      return this.isTimeout(this.productError, this.rateError) ? config.timeoutError : config.genericError;
    } else {
      return config;
    }
  }

  private getReportAnIssueButton(): AlertButton {
    return <AlertButton>{
      label: 'REPORT_AN_ISSUE_MENU',
      type: AlertButtonType.Danger,
      onClick: () => {
        this.clickReportIssueManageStay();
      }
    };
  }

  private clickReportIssueManageStay(): void {
    if (this.checkForManageStayEdit()) {
      this.reportIssueService.openAutofilledReportIssueModal(ReportIssueComponent, this.getAutoFill(MANAGE_STAY_LOAD_ERROR));
    } else {
      this.reportIssueService.openAutofilledReportIssueModal(ReportIssueComponent, this.getAutoFill(OFFERS_LOAD_ERROR));
    }
  }

  private getPaginationParams(state: State): string {
    const page = (state.skip / state.take) + 1;
    return `page=${page}&pageSize=${state.take}`;
  }

  private getBrandCode(): string {
    return this.userService.getCurrentLocation().brandCode;
  }

  public ignorePendingReservation(reservationNumber: string): Observable<any> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}reservation/ignore/${location}?reservationNumber=${reservationNumber}`;
    return this.http.delete(apiURl).pipe(map((response: any) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public confirmChangeReservation(payload: any): Observable<any> {
    const location = this.userService.getCurrentLocationId();
    payload.brandCode = this.getBrandCode();
    payload.hotelCode = location;
    const apiURl = `${this.API_URL}reservation/modify/${location}`;
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.put(apiURl, JSON.stringify(payload), httpOptions).pipe(map((response: GuestCheckInModel) => {
      this.confirmationSuccess.next(true);
      this.resetOffersError();
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public resetOffersError(): void {
    this.resetOffersErrorCount(this.errorKeys.products);
    this.resetOffersErrorCount(this.errorKeys.rates);
  }

  changeMessage(show: boolean) {
    this.messageSource.next(show);
  }

  showAllOffers(showOffers: boolean) {
    this.showOffers.next(showOffers);
  }

  setReservationData(data: GuestCheckInModel) {
    this.reservationData.next(data);
  }

  setNewReservationData(walkInSelection: GuestWalkInModel) {
    this.newReservationDataModal.next(walkInSelection);
  }

  setInitialWalkInSelection(selectionModel: ProductModel | RateCategoryModel, searchCriteria: StayInfoSearchModel): GuestWalkInModel {
    selectionModel.rateInfo.currencyCode = selectionModel.rateInfo.currency || null;
    return <GuestWalkInModel>{
      ...{
        firstName: '',
        lastName: '',
        checkInDate: searchCriteria.arrivalDate,
        checkOutDate: searchCriteria.departureDate,
        numberOfNights: searchCriteria.numberOfNights,
        numberOfAdults: isNaN(+searchCriteria.adultsCount) ? 0 : +searchCriteria.adultsCount,
        numberOfChildren: isNaN(+searchCriteria.childCount) ? 0 : +searchCriteria.childCount,
        guestCount: searchCriteria.numberOfRooms,
        numberOfRooms: searchCriteria.numberOfRooms,
        ratesInfo: selectionModel.rateInfo,
        ihgRcNumber: searchCriteria.ihgRcNumber,
        rateCategoryCode: selectionModel.parentItemRateCode || selectionModel.code,
        hotelCode: searchCriteria.hotelCode,
        corporateId: searchCriteria.corporateId,
        groupCode: searchCriteria.groupCode,
        roomTypeCode: selectionModel.parentProductRoomCode || selectionModel.code,
        accessible: searchCriteria.accessible,
        smokingPreference: searchCriteria.preference
      }
    };
  }

  public trackClickManageStay(action: string, label: string): void {
    if(!this.checkForManageStayEdit()){
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_MANAGE_STAY, action, label);
    } else {
      this.gaService.trackEvent(manageStayGaConstants.CATEGORY_IN_HOUSE_MANAGE_STAY, action, label);
    }
  }

  public getFrontDeskAccess() {
    const roles = get(this.userService.getUser(), 'roles', []);
    return hasDoNotPostAccessRoles(roles);
  }

  public getRoomAvailability(payload: RoomAvailabilityPayloadModel): Observable<boolean> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}rooms/roomAvailability/${location}`;
    const params = new HttpParams({
      fromObject: {
        checkInDate: payload.checkInDate,
        checkOutDate: payload.checkOutDate,
        reservationNumber: payload.reservationNumber,
        roomNumber: payload.roomNumber,
        roomType: payload.roomType
      }
    });
    return this.http.get(apiURl, { params }).pipe(map((response: RoomAvailabilityResponseModel) => {
      return response.availability;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getRoomConflicts(conflictParams: RoomConflictsPayload): Observable<RoomConflict[]> {
    const apiURl = `${this.API_URL}guest/conflicts/${this.userService.getCurrentLocationId()}`;
    return this.http.get(apiURl, {params: conflictParams as any})
      .pipe(map((response: RoomConflict[]) => {
          if (response && response.length) {
            response.forEach(conflict => {
              conflict.guestName = `${conflict.lastName}, ${conflict.firstName}`;
              conflict.isConflictActive = true;
              conflict.doNotMoveRoomReason = conflict.doNotMoveRoomComments ? get(conflict.doNotMoveRoomComments[0], 'reason', null) : null;
            });
          }
          return response;
        }
        )
      );
  }
  public getRoomReleased(queryParams): Observable<ReleaseRoomResponse> {
    const url = `${this.API_URL}rooms/releaseRoom/${this.getLocationId()}`;
    return this.http.put(url, queryParams).pipe(
      map((response: RoomsListResponse) => response),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  getAssignReleasePayload(listItem: RoomConflict): RoomInfoQueryAssignReleasePayload {
    return <RoomInfoQueryAssignReleasePayload>{
      checkInDate: listItem.checkInDate,
      checkOutDate: listItem.checkOutDate,
      pmsReservationNumber: listItem.pmsReservationNumber,
      reservationNumber: listItem.reservationNumber,
      roomNumber: listItem.roomNumber,
      roomType: listItem.roomType
    };
  }

  setInHouseReservationData(data: ReservationDataModel) {
    this.reservationDataOnly = data;
  }

  getInHouseReservationData(): ReservationDataModel {
    return this.reservationDataOnly;
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public constructRateInfo(reservationData: StayInformationModel, searchCritera: StayInfoSearchModel): RateCategoryModel {
    let rateInfoAfter: RateCategoryModel;
    let rateInfo:RateInfoModel= cloneDeep(reservationData.rateInfo) ;
    if (this.checkIsDayExtended(reservationData.checkOutDate, searchCritera.departureDate)) {
      rateInfo.dailyRates = this.addExtendedNight(rateInfo.dailyRates, searchCritera.departureDate);
    }
    rateInfoAfter = {
      code: reservationData.rateCategoryCode,
      name: reservationData.guestInfo.firstName + ' ' + reservationData.guestInfo.lastName,
      rateInfo: rateInfo,
      averageNightlyRate: reservationData.rateInfo.averageRate,
      parentProductRoomCode: reservationData.roomTypeCode,
      pointsEligible: true,
      breakfastIncluded: false
    };
    return rateInfoAfter
  }

  private checkIsDayExtended(checkOut: string, departure: string): boolean {
    const checkOutDate = moment(checkOut);
    const departureDate = moment(departure)

    if (departureDate.diff(checkOutDate, 'days') == 1) {
      return true;
    }
    return false;
  }

  addExtendedNight(dailyRates: DailyRateModel[],departureDate: string): DailyRateModel[] {
    let extendNight: DailyRateModel = cloneDeep(dailyRates[dailyRates.length-1])
    extendNight.date = moment(departureDate, 'YYYY-MM-DD').subtract(1,'days').format(AppConstants.DB_DT_FORMAT);
    dailyRates.push(extendNight);
    return dailyRates;
  }
}
