import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { BulkNotifyGuestModalComponent } from './bulk-notify-guest-modal.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { mockArrivalList } from '../../mocks/prepare-arrivals-mock';
import { MockNgbActiveModal, MockGuestNotificationService, MockAnalyticsTrackingService } from '../../mocks/services.mock';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GuestNotificationService } from '../../services/guest-notification/guest-notification.service';
import { AnalyticsTrackingService } from '../prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { MockComponent } from '@modules/common-test/functions/component-mocker';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { ArrivalsListModel } from '../../models/arrivals-checklist.model';
import { NotifyListTypes } from '../../models/notify-list';

describe('BulkNotifyGuestModalComponent', () => {
  let component: BulkNotifyGuestModalComponent;
  let fixture: ComponentFixture<BulkNotifyGuestModalComponent>;
  let activeModalService: MockNgbActiveModal;
  let notificationservice: MockGuestNotificationService;
  let trackingservice: MockAnalyticsTrackingService;

  function getGuestsWithAllTasksResolved(arrivalList: ArrivalsListModel[]): ArrivalsListModel[] {
    return arrivalList.filter(item => {
      return (item.tasks && item.tasks.length)
        ? item.tasks.every(task => task.isResolved)
        : true;
    });
  }

  function getGuestsWithAllTasksNotResolved(arrivalList: ArrivalsListModel[]): ArrivalsListModel[] {
    return arrivalList.filter(item => {
      return (item.tasks && item.tasks.length)
        ? item.tasks.some(task => !task.isResolved)
        : false;
    });
  }

  function splitResolvedList(arrivalList: ArrivalsListModel[]) {
    const numColumns = 2;
    let listOneCount;
    if (arrivalList.length % numColumns === 1) {
      listOneCount = (arrivalList.length + 1) / numColumns;
    } else {
      listOneCount = arrivalList.length / numColumns;
    }
    return {
      listOne: arrivalList.slice(0, listOneCount),
      listTwo: arrivalList.slice(listOneCount)
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule, NgbModule.forRoot()],
      declarations: [BulkNotifyGuestModalComponent,
        MockComponent({
          selector: 'app-error-toast-message',
          inputs: ['errorEvent', 'translations', 'reportIssueAutoFill'],
          outputs: ['retryAction']
        }),
        MockComponent({
          selector: 'app-trace-list',
          inputs: ['arrival', 'ordertask']
        })],
      providers: [
        { provide: NgbActiveModal, useClass: MockNgbActiveModal },
        { provide: GuestNotificationService, useClass: MockGuestNotificationService },
        { provide: AnalyticsTrackingService, useClass: MockAnalyticsTrackingService }
      ]
    })
  }));

  beforeEach(() => {
    setTestEnvironment();
    activeModalService = TestBed.get(NgbActiveModal);
    notificationservice = TestBed.get(GuestNotificationService);
    trackingservice = TestBed.get(AnalyticsTrackingService);
    fixture = TestBed.createComponent(BulkNotifyGuestModalComponent);
    component = fixture.componentInstance;
    component.arrivals = cloneDeep(mockArrivalList);    
  });

  it('should call proper set of methods on initialization', () => {
    const setTaskSpy = spyOn<any>(component, 'setTasksOutstandingLists');
    const setDisplaySpy = spyOn<any>(component, 'setDisplayType');
    const setListSpy = spyOn<any>(component, 'setLists');

    component.ngOnInit();

    expect(setTaskSpy).toHaveBeenCalled();
    expect(setDisplaySpy).toHaveBeenCalled();
    expect(setListSpy).toHaveBeenCalled();
  });

  it('#setTasksOutstandingLists should properly build lists for task outstanding and no task outstanding', () => {
    component['setTasksOutstandingLists']();

    expect(component['tasksOutstandingList']).toEqual(getGuestsWithAllTasksNotResolved(component.arrivals));
    expect(component['noTasksOutstandingList']).toEqual(getGuestsWithAllTasksResolved(component.arrivals));
  });

  it('#setDisplayType should properly set the display type', () => {
    component.ngOnInit();
    expect(component.displayType).toEqual(NotifyListTypes.BOTH);

    component.arrivals = getGuestsWithAllTasksResolved(mockArrivalList);
    component.ngOnInit();
    expect(component.displayType).toEqual(NotifyListTypes.ALL_RESOLVED);

    component.arrivals = getGuestsWithAllTasksNotResolved(mockArrivalList);
    component.ngOnInit();
    expect(component.displayType).toEqual(NotifyListTypes.ALL_UNRESOLVED);
  });

  it('#setLists should build two lists of guests with both resolved und unresolved tasks if display type is both', () => {
    component.ngOnInit();
    expect(component.lists).toEqual(
      [
        {
          items: getGuestsWithAllTasksResolved(component.arrivals),
          type: NotifyListTypes.ALL_RESOLVED
        },
        {
          items: getGuestsWithAllTasksNotResolved(component.arrivals),
          type: NotifyListTypes.ALL_UNRESOLVED
        }
      ]
    );
  });

  it('#setLists should build one list of guests with unresolved tasks if display type is unresolved', () => {
    component.arrivals = getGuestsWithAllTasksNotResolved(mockArrivalList);
    component.ngOnInit();
    expect(component.lists).toEqual(
      [
        {
          items: getGuestsWithAllTasksNotResolved(component.arrivals),
          type: NotifyListTypes.ALL_UNRESOLVED
        },
        {
          items: [],
          type: null
        }
      ]
    );
  });

  it('#setLists should build two lists of guests with resolved tasks if display type is resolved', () => {
    component.arrivals = getGuestsWithAllTasksResolved(mockArrivalList);
    component.ngOnInit();
    expect(component.lists).toEqual(
      [
        {
          items: splitResolvedList(component.arrivals).listOne,
          type: NotifyListTypes.ALL_RESOLVED
        },
        {
          items: splitResolvedList(component.arrivals).listTwo,
          type: NotifyListTypes.ALL_RESOLVED
        }
      ]
    );
  });

  /* it('#continue should notify guests and properly track action with or without warning on clicking Notify button', () => {
    const notifySpy = spyOn<any>(component, 'notifyGuests');
    const warningSpy = spyOn(trackingservice, 'trackBulkNotifyWarning').and.callThrough();
    const noWarningSpy = spyOn(trackingservice, 'trackBulkNotifyNoWarning').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    component.continue();
    expect(notifySpy).toHaveBeenCalled();
    expect(warningSpy).toHaveBeenCalled();
    expect(noWarningSpy).not.toHaveBeenCalled();

    component.arrivals = getGuestsWithAllTasksResolved(mockArrivalList);
    component.ngOnInit();
    expect(notifySpy).toHaveBeenCalled();
    expect(warningSpy).not.toHaveBeenCalled();
    expect(noWarningSpy).toHaveBeenCalled();
  }); */

  it('#close should dismiss the modal', () => {
    const dismissSpy = spyOn(activeModalService, 'dismiss');
    component.close();
    expect(dismissSpy).toHaveBeenCalled();
  });

  it('#deleteItemFrom should delete item from correponding notification list', () => {
    const indexToBeDeleted = 0;
    const expectedResolvedList = getGuestsWithAllTasksResolved(mockArrivalList);
    expectedResolvedList.splice(indexToBeDeleted, 1);

    component.ngOnInit();
    expect(component.listOne.items).toEqual(getGuestsWithAllTasksResolved(component.arrivals));
    component['deleteItemFrom'](indexToBeDeleted, component.listOne);
    expect(component.listOne.items).toEqual(expectedResolvedList);
  });
});
