import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash';

import { ArrivalsListModel, NotifyGuestModel } from '@app/modules/prepare/models/arrivals-checklist.model';
import { GuestNotificationService } from '@app/modules/prepare/services/guest-notification/guest-notification.service';
import { NotifyList, NotifyListTypes } from '@app/modules/prepare/models/notify-list';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { BULK_NOTIFY_ERROR } from '@modules/prepare/constants/prepare-error-constants';
import { ErrorToastMessageTranslations } from '@app-shared/components/error-toast-message/error-toast-message.component';
import { AnalyticsTrackingService } from '@app/modules/check-in/services/analytics-tracking/analytics-tracking.service';
import { PREPARE_NOTIFICATION_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';

@Component({
  selector: 'app-bulk-notify-guest-modal',
  templateUrl: './bulk-notify-guest-modal.component.html',
  styleUrls: ['./bulk-notify-guest-modal.component.scss']
})
export class BulkNotifyGuestModalComponent extends AppErrorBaseComponent implements OnInit {
  public selectedDate: Date;
  public arrivals: ArrivalsListModel[];
  private roomReadyActionCode = 'room-ready';
  private tasksOutstandingList: ArrivalsListModel[];
  private noTasksOutstandingList: ArrivalsListModel[];
  public listOne: NotifyList;
  public listTwo: NotifyList;
  public displayType: NotifyListTypes;
  public lists: NotifyList[];
  public errorContent: ErrorToastMessageTranslations = BULK_NOTIFY_ERROR;
  public reportIssueAutoFill = PREPARE_NOTIFICATION_ISSUE_AUTOFILL.bulkNoticationError;

  constructor(
    private activeModal: NgbActiveModal,
    private guestsNotifiedService: GuestNotificationService,
    private analyticsTrackingService: AnalyticsTrackingService) {
    super();
  }

  ngOnInit() {
    this.setTasksOutstandingLists();
    this.setDisplayType();
    this.setLists();
  }

  private setTasksOutstandingLists() {
    const arrivals = JSON.parse(JSON.stringify(this.arrivals));
    this.tasksOutstandingList = _.remove(arrivals, function (arrival) {
      const tasks = arrival.tasks || [];
      return _.some(tasks, ['isResolved', false]);
    });
    this.noTasksOutstandingList = arrivals || [];
  }

  private setDisplayType() {
    let type: NotifyListTypes;
    if (this.someNotFullyReady() && !this.someFullyReady()) {
      type = NotifyListTypes.ALL_UNRESOLVED;
    } else if (!this.someNotFullyReady() && this.someFullyReady()) {
      type = NotifyListTypes.ALL_RESOLVED;
    } else if (this.someNotFullyReady() && this.someFullyReady()) {
      type = NotifyListTypes.BOTH;
    }
    this.displayType = type;
  }

  private setLists(): void {
    switch (this.displayType) {
      case NotifyListTypes.BOTH:
        this.splitListBoth();
        break;
      case NotifyListTypes.ALL_UNRESOLVED:
        this.splitListUnresolved();
        break;
      case NotifyListTypes.ALL_RESOLVED:
        this.splitListResolved();
        break;
    }
    this.lists = [this.listOne, this.listTwo];
  }

  private createList(items: ArrivalsListModel[], type: NotifyListTypes) {
    return {
      items: items,
      type: type
    };
  }

  public deleteItemFrom(index, list) {
    const item = JSON.parse(JSON.stringify(list.items[index]));
    list.items.splice(index, 1);
    _.remove(this.arrivals, _.matches(item));
  }

  private someNotFullyReady(): boolean {
    return (this.tasksOutstandingList && this.tasksOutstandingList.length > 0);
  }

  private someFullyReady(): boolean {
    return (this.noTasksOutstandingList && this.noTasksOutstandingList.length > 0);
  }

  private splitListBoth(): void {
    this.listOne = this.createList(this.noTasksOutstandingList, NotifyListTypes.ALL_RESOLVED);
    this.listTwo = this.createList(this.tasksOutstandingList, NotifyListTypes.ALL_UNRESOLVED);
  }

  private splitListUnresolved(): void {
    this.listOne = this.createList(this.tasksOutstandingList, NotifyListTypes.ALL_UNRESOLVED);
    this.listTwo = this.createList([], null);
  }

  private splitListResolved(): void {
    const numColumns = 2;
    let lengthOne;
    if (this.arrivals.length % numColumns === 1) {
      lengthOne = (this.arrivals.length + 1) / numColumns;
    } else {
      lengthOne = this.arrivals.length / numColumns;
    }
    const firstColumn = this.arrivals.slice(0, lengthOne);
    const secondColumn = this.arrivals.slice(lengthOne);
    this.listOne = this.createList(firstColumn, NotifyListTypes.ALL_RESOLVED);
    this.listTwo = this.createList(secondColumn, NotifyListTypes.ALL_RESOLVED);
  }

  close() {
    this.activeModal.dismiss();
  }

  private trackBulkNotifyGuest() {
    if (this.someNotFullyReady()) {
      this.analyticsTrackingService.trackBulkNotifyWarning();
    } else {
      this.analyticsTrackingService.trackBulkNotifyNoWarning();
    }
  }

  continue() {
    this.trackBulkNotifyGuest();
    this.notifyGuests();
  }

  public notifyGuests(): void {
    this.guestsNotifiedService.notifyGuest(this.selectedDate, this.arrivals).subscribe((response: NotifyGuestModel[]) => {
      if (!super.checkIfAnyApiErrors(response)) {
        this.handleNotifyResponse(response);
      } else {
        this.handleNotifyErrorResponse(response);
      }
    }, (error) => {
      super.processHttpErrors(error);
    });
  }

  private handleNotifyResponse(response: NotifyGuestModel[]) {
    this.guestsNotifiedService.emitNotifyGuestStatus(response);
    this.activeModal.close(true);
  }

  private handleNotifyErrorResponse(response: NotifyGuestModel[]): void {
    if (response && response.length) {
      const notifiedGuests = response.filter((item: NotifyGuestModel) => !(item.errors && item.errors.length) &&
        item.actionItemCode === this.roomReadyActionCode && item.guestNotifyStatus);
      this.guestsNotifiedService.emitNotifyGuestStatus(notifiedGuests);
      this.resetFailedGuestLists(notifiedGuests);
    }
  }

  private resetFailedGuestLists(notifiedGuests: NotifyGuestModel[]): void {
    this.arrivals = this.arrivals.filter((data: ArrivalsListModel) => !(notifiedGuests.some((item: NotifyGuestModel) =>
      item.reservationNumber === data.reservationNumber
    )));
    this.ngOnInit();
  }

}
