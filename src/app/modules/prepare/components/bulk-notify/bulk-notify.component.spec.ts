import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { BulkNotifyComponent } from './bulk-notify.component';
import { mockArrivalList } from '../../mocks/prepare-arrivals-mock';
import { MockArrivalsService, MockNgbModal } from '../../mocks/services.mock';
import { ArrivalsService } from '../../services/arrivals-service/arrivals.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BulkNotifyGuestModalComponent } from '../bulk-notify-guest-modal/bulk-notify-guest-modal.component';

describe('BulkNotifyComponent', () => {
  let component: BulkNotifyComponent;
  let fixture: ComponentFixture<BulkNotifyComponent>;
  let arrivalService: MockArrivalsService;
  let modalService: MockNgbModal;
  let openSpy: jasmine.Spy;
  let openBulkModalSpy: jasmine.Spy;
  let handleTaskSpy: jasmine.Spy;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [BulkNotifyComponent],
      providers: [
        { provide: ArrivalsService, useClass: MockArrivalsService },
        { provide: NgbModal, useClass: MockNgbModal }
      ]
    })
  }));

  beforeEach(() => {
    arrivalService = TestBed.get(ArrivalsService);
    modalService = TestBed.get(NgbModal);
    fixture = TestBed.createComponent(BulkNotifyComponent);
    component = fixture.componentInstance;
    component.listItems = cloneDeep(mockArrivalList);
    openSpy = spyOn(modalService, 'open').and.returnValue({ componentInstance: { arrivals: {} } });
    openBulkModalSpy = spyOn<any>(component, 'openBulkNotificationWarning').and.callThrough();
    handleTaskSpy = spyOn(arrivalService, 'handleUnsavedTasks').and.callThrough();
    fixture.detectChanges();
  });

  it('should open the bulk notification guest modal on button click and should appply "large-modal" class', () => {
    component.openModal();
    expect(openSpy).toHaveBeenCalledWith(BulkNotifyGuestModalComponent, { backdrop: 'static', windowClass: 'large-modal' });
  });

  it('should call proper method to handle unsaved tasks', () => {
    const bindSpy = spyOn<any>(component['openBulkNotificationWarning'], 'bind').and.callThrough();
    component.openModal();
    expect(bindSpy).toHaveBeenCalled();
    expect(handleTaskSpy).toHaveBeenCalled();
  });

  it('should pass selected guests as input to the modal component', () => {
    const selectedGuests = component.listItems.filter(item => item.selected);
    component.openModal();
    expect(openBulkModalSpy).toHaveBeenCalledWith(selectedGuests);
  });
});
