import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { BulkNotifyGuestModalComponent } from '@app/modules/prepare/components/bulk-notify-guest-modal/bulk-notify-guest-modal.component';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';

@Component({
  selector: 'app-bulk-notify',
  templateUrl: './bulk-notify.component.html',
  styleUrls: ['./bulk-notify.component.scss']
})
export class BulkNotifyComponent {
  @Input() listItems: ArrivalsListModel[];
  @Input() selectedDate: Date;

  constructor(
    private arrivalsService: ArrivalsService,
    private modalService: NgbModal) {
  }

  public openModal(): void {
    const selectedGuests = this.listItems.filter(item => item.selected);
    const callbackFn = this.openBulkNotificationWarning.bind(this, selectedGuests);
    this.arrivalsService.handleUnsavedTasks(callbackFn, null);
  }

  private openBulkNotificationWarning(selectedGuests: ArrivalsListModel[]): void {
    const modal = this.modalService.open(BulkNotifyGuestModalComponent, { backdrop: 'static', windowClass: 'large-modal' });
    const modalComponent = modal.componentInstance as BulkNotifyGuestModalComponent;
    modalComponent.arrivals = selectedGuests;
  }

}
