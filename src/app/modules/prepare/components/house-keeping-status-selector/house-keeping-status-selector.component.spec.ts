import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { By } from '@angular/platform-browser';
import { DropDownListComponent, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UserService, DetachedToastMessageService, SessionStorageService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { HouseKeepingStatusSelectorComponent } from './house-keeping-status-selector.component';

export class MockSessionStorageService{
  getSessionStorage() {
    return {};
  }
  public setSessionStorage(key, value) { }
}

describe('HouseKeepingStatusSelectorComponent', () => {
  let component: HouseKeepingStatusSelectorComponent;
  let fixture: ComponentFixture<HouseKeepingStatusSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseKeepingStatusSelectorComponent ],
      imports: [ DropDownsModule, TranslateModule.forRoot(), FormsModule, HttpClientTestingModule, RouterTestingModule ],
      providers: [UserService, TranslateService, ConfirmationModalService, DetachedToastMessageService,
        {provide: SessionStorageService, useClass: MockSessionStorageService}],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseKeepingStatusSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the housekeeping status', () => {
    const dropdownElement = fixture.debugElement.query(By.directive(DropDownListComponent));
    expect(dropdownElement).toBeDefined();
  });
});
