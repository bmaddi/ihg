import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { HOUSE_KEEPING_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';

@Component({
  selector: 'app-house-keeping-status-selector',
  templateUrl: './house-keeping-status-selector.component.html',
  styleUrls: ['./house-keeping-status-selector.component.scss']
})
export class HouseKeepingStatusSelectorComponent implements OnInit {

  houseKeepingList: string[];
  selectedStatus: string;

  @Input() roomNo: string;
  @Input() inspectedFlag: boolean;
  @Output() status: EventEmitter<string> = new EventEmitter();

  constructor(private roomsService: RoomsService) { }

  ngOnInit() {
    this.setHouseKeepingStatus();
  }

  setHouseKeepingStatus() {
    this.houseKeepingList = this.inspectedFlag ? HOUSE_KEEPING_STATUS.Inspected : HOUSE_KEEPING_STATUS.Clean;
  }

  statusSelected() {
    this.status.emit(this.selectedStatus);
  }
}
