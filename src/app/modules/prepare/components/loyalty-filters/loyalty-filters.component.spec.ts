import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { LoyaltyFiltersComponent } from './loyalty-filters.component';
import { ArrivalsService } from '../../services/arrivals-service/arrivals.service';
import { mockLoyaltyFilters } from '../../mocks/prepare-arrivals-mock';
import { MockArrivalsService, MockGAService } from '../../mocks/services.mock';
import { FilterModel } from '../../models/arrivals-checklist.model';
import { hasClass } from '@modules/common-test/functions/common-test.function';
import { cloneDeep } from 'lodash';

describe('LoyaltyFiltersComponent', () => {
  let component: LoyaltyFiltersComponent;
  let fixture: ComponentFixture<LoyaltyFiltersComponent>;
  let arrivalService: MockArrivalsService;
  let gaService: MockGAService;

  function getAllFilter(): FilterModel {
    return component.filters.find(filter => filter.filterName === component['totalStr']);
  }

  function getAllFilterBtn(): HTMLElement {
    return fixture.debugElement.query(By.css('[data-slnm-ihg=FilterLBL_FILTER_TOTAL-SID]')).nativeElement;
  }

  function getFilterButtons(): DebugElement[] {
    return fixture.debugElement.queryAll(By.css('.btn.filter'));
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [LoyaltyFiltersComponent],
      providers: [
        { provide: ArrivalsService, useClass: MockArrivalsService },
        { provide: GoogleAnalyticsService, useClass: MockGAService }
      ]
    });
  }));

  beforeEach(() => {
    arrivalService = TestBed.get(ArrivalsService);
    gaService = TestBed.get(GoogleAnalyticsService);
    fixture = TestBed.createComponent(LoyaltyFiltersComponent);
    component = fixture.componentInstance;
    component.filters = cloneDeep(mockLoyaltyFilters);
    fixture.detectChanges();
  });

  it('should render as many loyalty filters as provided in input', () => {
    expect(getFilterButtons().length).toEqual(component.filters.length);
  });

  it('should prompt the user about unsaved tasks on selecting a filter', () => {
    const bindSpy = spyOn<any>(component['doFilterClick'], 'bind');
    const handleTaskSpy = spyOn(arrivalService, 'handleUnsavedTasks');
    component.handleFilterClick(component.filters[1]);
    expect(bindSpy).toHaveBeenCalled();
    expect(handleTaskSpy).toHaveBeenCalled();
  });

  it('should select "All" when no other loyalty filter is selected', () => {
    const filterIndex = 1;
    component['doFilterClick'](component.filters[filterIndex]);//Turn on a filter
    component['doFilterClick'](component.filters[filterIndex]);//Turn off the same filter
    fixture.detectChanges();

    expect(getAllFilter().selected).toEqual(true);
    expect(hasClass(getAllFilterBtn(), 'filter-active')).toEqual(true);
  });

  it('should deselect "All" when any other loyalty filter is selected', () => {
    const filterIndex = 2;
    component['doFilterClick'](component.filters[filterIndex]);
    fixture.detectChanges();
    expect(getAllFilter().selected).toEqual(false);
    expect(hasClass(getAllFilterBtn(), 'filter-active')).toEqual(false);
  });

  it('should deselect other filters when "All" is selected', () => {
    const filterIndices = [1, 2];
    //Select two other filters
    component['doFilterClick'](component.filters[filterIndices[0]]);
    component['doFilterClick'](component.filters[filterIndices[1]]);
    //Select "All" filter
    component['doFilterClick'](component.filters[0]);
    fixture.detectChanges();
    component.filters.forEach(item => {
      expect(item.selected).toEqual(item.filterName === component['totalStr']);
    });
  });

  it('should toggle filter selection when it is clicked', () => {
    const filterIndex = 4;
    component.filters[filterIndex].selected = false;
    component['doFilterClick'](component.filters[filterIndex]);
    fixture.detectChanges();
    expect(component.filters[filterIndex].selected).toEqual(true);
    expect(hasClass(getFilterButtons()[filterIndex].nativeElement, 'filter-active')).toEqual(true);

    component['doFilterClick'](component.filters[filterIndex]);
    fixture.detectChanges();
    expect(component.filters[filterIndex].selected).toEqual(false);
    expect(hasClass(getFilterButtons()[filterIndex].nativeElement, 'filter-active')).toEqual(false);
  });

  it('should properly emit the selected filter names', () => {
    const emitSpy = spyOn(component.filterChange, 'emit');
    component['doFilterClick'](component.filters[2]);
    expect(emitSpy).toHaveBeenCalledWith([component.filters[2].filterName]);

    component['doFilterClick'](component.filters[3]);
    expect(emitSpy).toHaveBeenCalledWith([
      component.filters[2].filterName,
      component.filters[3].filterName,
    ]);

    component['doFilterClick'](component.filters[5]);
    expect(emitSpy).toHaveBeenCalledWith([
      component.filters[2].filterName,
      component.filters[3].filterName,
      component.filters[5].filterName
    ]);
  });

  it('should display the count in each loyalty filter in proper format', () => {
    const filterIndex = 2;
    component.filters[filterIndex].count = 10;
    fixture.detectChanges();
    const filterCount = (getFilterButtons()[filterIndex].nativeElement as HTMLElement).querySelector('.filter-count');
    expect(filterCount.textContent.trim()).toEqual('(10)');
  });

  it('should NOT display the count if it is 0', () => {
    const filterIndex = 1;
    component.filters[filterIndex].count = 0;
    fixture.detectChanges();
    const filterCount: HTMLElement = (getFilterButtons()[filterIndex].nativeElement as HTMLElement).querySelector('.filter-count');
    expect(filterCount.offsetHeight).toEqual(0);
    expect(filterCount.offsetWidth).toEqual(0);
  });

  it('should track filter selection', () => {
    const trackingSpy = spyOn(gaService, 'trackEvent');
    component['doFilterClick'](component.filters[0]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Orange Filters', 'All');

    component['doFilterClick'](component.filters[1]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Orange Filters', 'Spire Elite');

    component['doFilterClick'](component.filters[2]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Orange Filters', 'Platinum Elite');

    component['doFilterClick'](component.filters[3]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Orange Filters', 'Gold Elite');

    component['doFilterClick'](component.filters[4]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Orange Filters', 'Club');

    component['doFilterClick'](component.filters[5]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Orange Filters', 'Non Member');
  });

  it('#changeStateOfTotalFilter should properly update the state of "All" filter', () => {
    component['changeStateOfTotalFilter'](true);
    expect(getAllFilter().selected).toEqual(true);

    component['changeStateOfTotalFilter'](false);
    expect(getAllFilter().selected).toEqual(false);
  });
});
