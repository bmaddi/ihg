import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FilterModel } from '@app/modules/prepare/models/arrivals-checklist.model';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { LoyaltyFiltersTypes } from '@modules/prepare/constants/prepare-filter-constants';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';

@Component({
  selector: 'app-loyalty-filters',
  templateUrl: './loyalty-filters.component.html',
  styleUrls: ['./loyalty-filters.component.scss']
})
export class LoyaltyFiltersComponent implements OnInit {
  private readonly totalStr = 'total';
  @Input() filters: FilterModel[];
  @Output() filterChange: EventEmitter<string[]> = new EventEmitter();

  constructor(
    private gaService: GoogleAnalyticsService,
    private arrivalsService: ArrivalsService) { }

  ngOnInit() {
  }

  handleFilterClick(filter: FilterModel): void {
    const callbackFn = this.doFilterClick.bind(this, filter);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  private doFilterClick(filter: FilterModel): void {
    this.toggleFilterState(filter);
    this.checkEmptyFilters();
    this.emitFilterChange();
    this.trackGoogleAnalytics(filter.filterName);
  }

  private toggleFilterState(filter: FilterModel): void {
    if (filter.filterName === this.totalStr) {
      this.filters.forEach(loyaltyFilter => { loyaltyFilter.selected = false; });
    } else {
      this.changeStateOfTotalFilter(false);
    }
    filter.selected = !filter.selected;
  }

  private checkEmptyFilters(): void {
    if (this.filters.every(filter => !filter.selected)) {
      this.changeStateOfTotalFilter(true);
    }
  }

  private changeStateOfTotalFilter(shouldSelect: boolean): void {
    this.filters.find(loyaltyFilter => loyaltyFilter.filterName === this.totalStr).selected = shouldSelect;
  }

  private emitFilterChange(): void {
    const filters = this.filters.filter(item => item.selected).map(item => item.filterName);
    this.filterChange.emit(filters);
  }

  private trackGoogleAnalytics(filterName: string): void {
    const action: string = PrepareForArrivalsGaConstants.ACTION_PRIMARY_FILTER;
    let label: string;
    
    switch (filterName) {
      case LoyaltyFiltersTypes.TOTAL: label = PrepareForArrivalsGaConstants.LBL_PRIMARY_FILTER_ALL; break;
      case LoyaltyFiltersTypes.SPIRE_ELITE: label = PrepareForArrivalsGaConstants.LBL_PRIMARY_FILTER_SPIRE; break;
      case LoyaltyFiltersTypes.PLATINUM_ELITE: label = PrepareForArrivalsGaConstants.LBL_PRIMARY_FILTER_PLTNM; break;
      case LoyaltyFiltersTypes.GOLD_ELITE: label = PrepareForArrivalsGaConstants.LBL_PRIMARY_FILTER_GOLD; break;
      case LoyaltyFiltersTypes.CLUB: label = PrepareForArrivalsGaConstants.LBL_PRIMARY_FILTER_CLUB; break;
      case LoyaltyFiltersTypes.NON_MEMBERS: label = PrepareForArrivalsGaConstants.LBL_PRIMARY_FILTER_NON_MEMBER; break;
    }
    
    if (action) {
      this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY, action, label);
    }
  }

}
