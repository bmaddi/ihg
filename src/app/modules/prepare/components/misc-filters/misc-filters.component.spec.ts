import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MiscFiltersComponent } from './misc-filters.component';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { ArrivalsService } from '../../services/arrivals-service/arrivals.service';
import { mockMiscFilters } from '../../mocks/prepare-arrivals-mock';
import { MockArrivalsService, MockGAService } from '../../mocks/services.mock';
import { hasClass } from '@modules/common-test/functions/common-test.function';
import { cloneDeep } from 'lodash';

describe('MiscFiltersComponent', () => {
  let component: MiscFiltersComponent;
  let fixture: ComponentFixture<MiscFiltersComponent>;
  let arrivalService: MockArrivalsService;
  let gaService: MockGAService;

  function getFilterButtons(): DebugElement[] {
    return fixture.debugElement.queryAll(By.css('.btn.filter'));
  }

  function getBtnBySlnmTag(label: string): HTMLElement {
    return fixture.debugElement.query(By.css(`[data-slnm-ihg=Filter${label}-SID]`)).nativeElement;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [MiscFiltersComponent],
      providers: [
        { provide: ArrivalsService, useClass: MockArrivalsService },
        { provide: GoogleAnalyticsService, useClass: MockGAService }
      ]
    });
  }));

  beforeEach(() => {
    arrivalService = TestBed.get(ArrivalsService);
    gaService = TestBed.get(GoogleAnalyticsService);
    fixture = TestBed.createComponent(MiscFiltersComponent);
    component = fixture.componentInstance;
    component.filters = cloneDeep(mockMiscFilters);
    fixture.detectChanges();
  });

  it('should render as many miscellaneous(blue) filters as provided in input', () => {
    expect(getFilterButtons().length).toEqual(component.filters.length);
  });

  it('should prompt the user about unsaved tasks on selecting a filter', () => {
    const bindSpy = spyOn<any>(component['doFilterClick'], 'bind');
    const handleTaskSpy = spyOn(arrivalService, 'handleUnsavedTasks');
    component.handleFilterClick(component.filters[1]);
    expect(bindSpy).toHaveBeenCalled();
    expect(handleTaskSpy).toHaveBeenCalled();
  });

  it('should toggle filter selection when it is clicked', () => {
    const filterIndex = 4;
    component.filters[filterIndex].selected = false;
    component['doFilterClick'](component.filters[filterIndex]);
    fixture.detectChanges();
    expect(component.filters[filterIndex].selected).toEqual(true);
    expect(hasClass(getFilterButtons()[filterIndex].nativeElement, 'filter-active')).toEqual(true);

    component['doFilterClick'](component.filters[filterIndex]);
    fixture.detectChanges();
    expect(component.filters[filterIndex].selected).toEqual(false);
    expect(hasClass(getFilterButtons()[filterIndex].nativeElement, 'filter-active')).toEqual(false);
  });

  it('should add specific classes to the filter button for "Ambassaor", "Inner Circle" and "Karma"', () => {
    const ambBtn = getBtnBySlnmTag('LBL_FILTER_AMB');
    const icBtn = getBtnBySlnmTag('LBL_FILTER_IC');
    const karmaBtn = getBtnBySlnmTag('LBL_FILTER_KAR');

    expect(hasClass(ambBtn, 'd-ICON')).toEqual(true);
    expect(hasClass(icBtn, 'd-KIKI')).toEqual(true);
    expect(hasClass(karmaBtn, 'd-KIKI')).toEqual(true);
  });

  it('should properly emit the selected filter names', () => {
    const emitSpy = spyOn(component.filterChange, 'emit');
    component['doFilterClick'](component.filters[2]);
    expect(emitSpy).toHaveBeenCalledWith([component.filters[2].filterName]);

    component['doFilterClick'](component.filters[3]);
    expect(emitSpy).toHaveBeenCalledWith([
      component.filters[2].filterName,
      component.filters[3].filterName,
    ]);

    component['doFilterClick'](component.filters[5]);
    expect(emitSpy).toHaveBeenCalledWith([
      component.filters[2].filterName,
      component.filters[3].filterName,
      component.filters[5].filterName
    ]);
  });

  it('should display the count in each loyalty filter in proper format', () => {
    const filterIndex = 2;
    component.filters[filterIndex].count = 10;
    fixture.detectChanges();
    const filterCount = (getFilterButtons()[filterIndex].nativeElement as HTMLElement).querySelector('.filter-count');
    expect(filterCount.textContent.trim()).toEqual('(10)');
  });

  it('should NOT display the count if it is 0', () => {
    const filterIndex = 1;
    component.filters[filterIndex].count = 0;
    fixture.detectChanges();
    const filterCount: HTMLElement = (getFilterButtons()[filterIndex].nativeElement as HTMLElement).querySelector('.filter-count');
    expect(filterCount.offsetHeight).toEqual(0);
    expect(filterCount.offsetWidth).toEqual(0);
  });

  it('should deselect any linked filter if the other linked filter is selected', () => {
    const deselectSpy = spyOn<any>(component, 'disableLinkedFilters').and.callThrough();
    const alreadySelectedIndex = 13;
    const toBeSelectedIndex = 14;
    component.filters[alreadySelectedIndex].selected = true;
    component['doFilterClick'](component.filters[toBeSelectedIndex]);

    expect(deselectSpy).toHaveBeenCalledWith(component.filters[toBeSelectedIndex]);
    expect(component.filters[toBeSelectedIndex].selected).toEqual(true);
    expect(component.filters[alreadySelectedIndex].selected).toEqual(false);
  });

  it('should track filter selection', () => {
    const trackingSpy = spyOn(gaService, 'trackEvent');
    component['doFilterClick'](component.filters[0]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'My VIPs');

    component['doFilterClick'](component.filters[1]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Filter: Ambassador');

    component['doFilterClick'](component.filters[2]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Filter: Inner Circle');

    component['doFilterClick'](component.filters[3]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Filter: Karma');

    component['doFilterClick'](component.filters[4]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Employee');

    component['doFilterClick'](component.filters[5]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Special Requests');

    component['doFilterClick'](component.filters[6]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Stay Preferences');

    component['doFilterClick'](component.filters[7]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Heartbeat Action');

    component['doFilterClick'](component.filters[8]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Arrival Time');

    component['doFilterClick'](component.filters[9]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Company');

    component['doFilterClick'](component.filters[10]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Group');

    component['doFilterClick'](component.filters[11]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Priority Enrollment');

    component['doFilterClick'](component.filters[12]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Potential Members');

    component['doFilterClick'](component.filters[13]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Filter: Pre-Arrival Complete');

    component['doFilterClick'](component.filters[14]);
    expect(trackingSpy).toHaveBeenCalledWith('Guest List - Prepare', 'Blue Filters', 'Filter: Pre-Arrival Incomplete');
  });
});
