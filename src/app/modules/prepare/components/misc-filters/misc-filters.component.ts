import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FilterModel } from '@app/modules/prepare/models/arrivals-checklist.model';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { MiscellaneousFilterTypes } from '@modules/prepare/constants/prepare-filter-constants';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';

@Component({
  selector: 'app-misc-filters',
  templateUrl: './misc-filters.component.html',
  styleUrls: ['./misc-filters.component.scss']
})
export class MiscFiltersComponent implements OnInit {
  @Input() filters: FilterModel[];
  @Output() filterChange: EventEmitter<string[]> = new EventEmitter();

  constructor(
    private gaService: GoogleAnalyticsService,
    private arrivalsService: ArrivalsService) { }

  ngOnInit() {
  }

  handleFilterClick(filter: FilterModel): void {
    const callbackFn = this.doFilterClick.bind(this, filter);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  private doFilterClick(filter: FilterModel): void {
    this.disableLinkedFilters(filter);
    filter.selected = !filter.selected;
    this.emitFilterChange();
    this.trackGoogleAnalytics(filter.filterName);
  }

  private disableLinkedFilters(filter: FilterModel): void {
    if (!filter.selected && filter.link) {
      const linkedActive = this.filters.find(item => item.link === filter.link && item.selected);
      if (linkedActive) {
        linkedActive.selected = false;
      }
    }
  }

  private emitFilterChange(): void {
    const filters = this.filters.filter(item => item.selected).map(item => item.filterName);
    this.filterChange.emit(filters);
  }

  private trackGoogleAnalytics(filterName: string): void {
    const action: string = PrepareForArrivalsGaConstants.ACTION_SECONDARY_FILTERS;
    let label: string;

    switch (filterName) {
      case MiscellaneousFilterTypes.VIP: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_VIPS; break;
      case MiscellaneousFilterTypes.EMPLOYEE: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_EMPLOYEE; break;
      case MiscellaneousFilterTypes.SPECIAL_REQUESTS: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_SPECIAL_REQUESTS; break;
      case MiscellaneousFilterTypes.STAY_PREFERENCES: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_STAY_PREFS; break;
      case MiscellaneousFilterTypes.HEART_BEAT_ACTION: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_HEARTBEAT; break;
      case MiscellaneousFilterTypes.ARRIVAL_TIME: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_ARRIVAL_TIME; break;
      case MiscellaneousFilterTypes.COMPANY: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_COMPANY; break;
      case MiscellaneousFilterTypes.PRIORITY_ENROLLMENTS: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_PRIORITY_ENROLLMENTS; break;
      case MiscellaneousFilterTypes.POTENTIAL_MEMBERS: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_POTENTIAL_MEMBERS; break;
      case MiscellaneousFilterTypes.GROUP: label = PrepareForArrivalsGaConstants.LBL_SECONDARY_FILTER_GROUP; break;
      case MiscellaneousFilterTypes.AMBASSADOR: label = PrepareForArrivalsGaConstants.FILTER_AMBASSADOR; break;
      case MiscellaneousFilterTypes.INNER_CIRCLE: label = PrepareForArrivalsGaConstants.FILTER_INNER_CIRCLE; break;
      case MiscellaneousFilterTypes.KARMA: label = PrepareForArrivalsGaConstants.FILTER_KARMA; break;
      case MiscellaneousFilterTypes.PRE_ARRIVAL_COMPLETE: label = PrepareForArrivalsGaConstants.FILTER_PRE_ARRIVAL_COMPLETE; break;
      case MiscellaneousFilterTypes.PRE_ARRIVAL_INCOMPLETE: label = PrepareForArrivalsGaConstants.FILTER_PRE_ARRIVAL_INCOMPLETE; break;
    }

    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY, action, label);
  }

}
