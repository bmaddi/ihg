import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of, Subject, Subscription } from 'rxjs';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { ManualAssignGridComponent } from './manual-assign-grid.component';
import { RoomsListResponse } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
// tslint:disable-next-line:max-line-length
import { AnalyticsTrackingService } from '@modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { MOCK_STATE_NAME } from '@app/modules/prepare/components/prepare-arrivals-details/mocks/manual-room-assign-mock';

import { isEqual, cloneDeep } from 'lodash';
import { BASIC_ROOM } from '@app/modules/prepare/components/prepare-arrivals-details/mocks/room-models-mock';

import { DEFAULT_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { MockRoomsResponse, RoomList, } from '@modules/prepare/components/prepare-arrivals-details/constants/room-test-constants';

export class MockRoomService {
  public getRooms(listItem: ArrivalsListModel, stateName: string): Observable<RoomsListResponse> {
    return of(MockRoomsResponse);
  }
}

export class MockAnalyticsTrackingService {
  public trackManualRoomLoadError(stateName: string) {
  }

  public trackAssignRoomManually(stateName: string) {
  }
}

describe('ManualAssignGridComponent', () => {
  let component: ManualAssignGridComponent;
  let fixture: ComponentFixture<ManualAssignGridComponent>;
  let roomServ: RoomsService;
  const blockTimeout = 1000000;

  function checkObjectEquality(a, b) {
    return isEqual(a, b);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        GridModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
        IhgNgCommonCoreModule,
        IhgNgCommonComponentsModule],
      declarations: [ManualAssignGridComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: RoomsService, useClass: MockRoomService }, {
        provide: AnalyticsTrackingService,
        useClass: MockAnalyticsTrackingService
      }]
    })
      .compileComponents().then(() => {
      jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
      setTestEnvironment();
    });
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ManualAssignGridComponent);
    component = fixture.componentInstance;
    component.listItem = { ...listItemMock };
    component.getListWatcher = new Subject<null>();
    component.showRoomsPanel = true;
    component.stateName = MOCK_STATE_NAME;
    component.displayedRooms = [];
    component['rooms$'] = new Subscription();
    roomServ = TestBed.get(RoomsService);
    fixture.detectChanges();
  }));

  it('should check if room data is correctly fetched from api', () => {
    const roomDataSpy = spyOn(roomServ, 'getRooms').and.returnValue(of(MockRoomsResponse));
    const ifObjsEqual = checkObjectEquality(component.roomsList, MockRoomsResponse.roomStatusList);
    expect(ifObjsEqual).toEqual(true);
  }, blockTimeout);

  it('should call api service to get room data', () => {
    const roomDataSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue(Observable.of(MockRoomsResponse));
    const getListSpy = spyOn<any>(component, 'getList').and.callThrough();
    component['getList']();
    fixture.detectChanges();
    expect(roomDataSpy).toBeDefined();
    expect(getListSpy).toHaveBeenCalled();
    expect(roomDataSpy).toHaveBeenCalled();
  });

  it('should check room search component is loaded', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomSearch-SID"]')).nativeElement;
    expect(element).not.toBeNull();
  }, blockTimeout);

  it('should check room type component is loaded', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomTypeSearch-SID"]')).nativeElement;
    expect(element).not.toBeNull();
  }, blockTimeout);

  it('should check status filter component is loaded', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="StatusSearch-SID"]')).nativeElement;
    expect(element).not.toBeNull();
    expect(element).toBeDefined();
  }, blockTimeout);

  it('should filter based on status', () => {
    const testMockResponse = cloneDeep(MockRoomsResponse);
    testMockResponse.roomStatusList.push({
      frontOfficeStatus: 'Vacant', houseKeepingStatus: 'Inspected', roomNumber: '0105', roomType: 'KNGN',
      'roomStatus': 'Inspected', features: [{ 'code': 'KB', description: 'King Bed', matchedWithResFeature: false }]
    });

    const getRoomsSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue(of(testMockResponse));
    const handleStatusChangeSpy = spyOn<any>(component, 'handleStatusChange').and.callThrough();
    const filterSort = spyOn<any>(component, 'filterAndSort').and.callThrough();
    const applyFilterSpy = spyOn<any>(component, 'applyFilter').and.callThrough();
    const doSortSpy = spyOn<any>(component, 'doSort').and.callThrough();

    component.ngOnInit();
    fixture.detectChanges();
    component.handleStatusChange(DEFAULT_STATUS.RoomInspected);

    expect(filterSort).toHaveBeenCalled();
    expect(applyFilterSpy).toHaveBeenCalled();
    expect(doSortSpy).toHaveBeenCalled();
    expect(component.filteredRooms.length).toEqual(1);
    expect(component.filteredRooms[0].roomNumber).toBe('0105');
    expect(component.roomsList.length).not.toEqual(component.filteredRooms.length);
  });

  xit('On Load PM should not be selected and all rooms except PM should be displayed', () => {
    const getRoomsSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue(of(RoomList));
    component.ngOnInit();
    fixture.detectChanges();

    expect(getRoomsSpy).toHaveBeenCalled();
    component.displayedRooms.forEach((value) => {
      expect(value.roomType).not.toEqual('PM');
    });
  });

  it('On click of PM all rooms including PM should be displayed', () => {
    const getRoomsSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue(of(MockRoomsResponse));
    const handleStatusChangeSpy = spyOn<any>(component, 'handleStatusChange').and.callThrough();
    const filterSort = spyOn<any>(component, 'filterAndSort').and.callThrough();
    const applyFilterSpy = spyOn<any>(component, 'applyFilter').and.callThrough();
    const doSortSpy = spyOn<any>(component, 'doSort').and.callThrough();
    component.ngOnInit();
    component.showPostMasterRoom = true;
    component['filterPostMasterRoom']();
    fixture.detectChanges();
    expect(getRoomsSpy).toHaveBeenCalled();
    expect(filterSort).toHaveBeenCalled();
    expect(applyFilterSpy).toHaveBeenCalled();
    expect(doSortSpy).toHaveBeenCalled();
    expect(component.displayedRooms.some(value => value.roomType === 'PM')).toBeTruthy();
  });

  xit('should custom sort Status on priority ascending', () => {
    const getRoomsSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue(of(MockRoomsResponse));
    const determineSortRulesSpy = spyOn<any>(component, 'determineSortRules').and.callThrough();
    const filterSort = spyOn<any>(component, 'filterAndSort').and.callThrough();
    const applyFilterSpy = spyOn<any>(component, 'applyFilter').and.callThrough();
    const doSortSpy = spyOn<any>(component, 'doSort').and.callThrough();
    const sortChangeSpy = spyOn<any>(component, 'sortChange').and.callThrough();
    const getStatusSortRuleSpy = spyOn<any>(component, 'getStatusSortRule').and.callThrough();
    const expectedExcludingPM = ['0102', '400', '404'];
    component.showPostMasterRoom = false;
    component.ngOnInit();
    fixture.detectChanges();
    component.sortChange([{
      field: 'status',
      dir: 'asc'
    }]);
    fixture.detectChanges();

    expect(getRoomsSpy).toHaveBeenCalled();
    expect(sortChangeSpy).toHaveBeenCalled();
    expect(determineSortRulesSpy).toHaveBeenCalled();
    expect(filterSort).toHaveBeenCalled();
    expect(applyFilterSpy).toHaveBeenCalled();
    expect(doSortSpy).toHaveBeenCalled();
    expect(getStatusSortRuleSpy).toHaveBeenCalled();
    component.displayedRooms.forEach((r, i) => {
      expect(r.roomNumber).toEqual(expectedExcludingPM[i]);
    });
  });

  xit('should custom sort Status on priority descending', () => {
    const getRoomsSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue(of(MockRoomsResponse));
    const determineSortRulesSpy = spyOn<any>(component, 'determineSortRules').and.callThrough();
    const filterSort = spyOn<any>(component, 'filterAndSort').and.callThrough();
    const applyFilterSpy = spyOn<any>(component, 'applyFilter').and.callThrough();
    const doSortSpy = spyOn<any>(component, 'doSort').and.callThrough();
    const sortChangeSpy = spyOn<any>(component, 'sortChange').and.callThrough();
    const getStatusSortRuleSpy = spyOn<any>(component, 'getStatusSortRule').and.callThrough();
    const expectedExcludingPM = ['400', '404', '0102'];
    component.showPostMasterRoom = false;
    component.ngOnInit();
    component.sortChange([{
      field: 'status',
      dir: 'desc'
    }]);
    fixture.detectChanges();
    expect(getRoomsSpy).toHaveBeenCalled();
    expect(sortChangeSpy).toHaveBeenCalled();
    expect(determineSortRulesSpy).toHaveBeenCalled();
    expect(filterSort).toHaveBeenCalled();
    expect(applyFilterSpy).toHaveBeenCalled();
    expect(doSortSpy).toHaveBeenCalled();
    expect(getStatusSortRuleSpy).toHaveBeenCalled();
    component.displayedRooms.forEach((r, i) => {
      expect(r.roomNumber).toEqual(expectedExcludingPM[i]);
    });
  });

  it('check "getRoomList"', () => {
    const roomDataSpy = spyOn(roomServ, 'getRooms').and.returnValue(of(MockRoomsResponse));
    component.showPostMasterRoom = true;
    const matchObjAgainst1 = component['getRoomList']();
    fixture.detectChanges();
    const ifObjsEqual = checkObjectEquality(matchObjAgainst1, MockRoomsResponse.roomStatusList);
    expect(ifObjsEqual).toEqual(true);
    component.showPostMasterRoom = false;
    const matchObjAgainst = component['getRoomList']();
    fixture.detectChanges();
    const cloneMock = cloneDeep(MockRoomsResponse.roomStatusList);
    const mockObj = cloneMock.filter(x => x.roomType !== 'PM');
    const checkIfEqual = checkObjectEquality(matchObjAgainst, mockObj);
    component.roomsList = null;
    const matchEmptyObj = component['getRoomList']();
    const ifObjEqlAfterEmpty = checkObjectEquality(matchEmptyObj, []);
    expect(ifObjEqlAfterEmpty).toEqual(true);
  });

  it('check "handleRoomsErrorResponse"', () => {
    component['handleRoomsErrorResponse']();
    expect(component.hasError).toBeTruthy();
  });

  it('check "handleAssign"', () => {
    spyOn(component.assignRoom, 'emit');
    component.handleAssign(BASIC_ROOM);
    fixture.detectChanges();
    expect(component.assignRoom.emit).toHaveBeenCalledWith(BASIC_ROOM);
  });

  it('check "applyFilter" method', () => {
    const roomDataSpy = spyOn(roomServ, 'getRooms').and.returnValue(of(MockRoomsResponse));
    component['roomSearchString'] = 'A';
    component['applyFilter']();
    fixture.detectChanges();
    expect(component.filterApplied).toBeTruthy();
    component['roomSearchString'] = '';
    component['applyFilter']();
    fixture.detectChanges();
    expect(component.filterApplied).toBeFalsy();
  });

  it('check roomSearchString is properly set', () => {
    component.searchByNumber('A');
    fixture.detectChanges();
    expect(component['roomSearchString']).toEqual('A');
  });

  it('check cloneToRoomtype method is called', () => {
    const cloneToRoomtypeSpy = spyOn<any>(component, 'cloneToRoomtype');
    component['filterPostMasterRoom']();
    fixture.detectChanges();
    expect(cloneToRoomtypeSpy).toHaveBeenCalled();
  });

  it('check if selectedStatus is properly set', () => {
    component.handleStatusChange(['Mock Data']);
    fixture.detectChanges();
    expect(component['selectedStatus'][0]).toEqual('Mock Data');
  });
});
