import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CompositeFilterDescriptor, filterBy, orderBy, SortDescriptor } from '@progress/kendo-data-query';
import { get } from 'lodash';
import { cloneDeep } from 'lodash';
import { RoomsService } from '@app/modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { Room, RoomsListResponse } from '@app/modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { MAN_RM_ASGN_LOAD_ERR } from '@modules/prepare/components/prepare-arrivals-details/constants/arrivals-details-constants';
import { AnalyticsTrackingService } from './../../services/analytics-tracking/analytics-tracking.service';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ROOM_STATUS, STATUS_HASHMAP } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Component({
  selector: 'app-manual-assign-grid',
  templateUrl: './manual-assign-grid.component.html',
  styleUrls: ['./manual-assign-grid.component.scss']
})
export class ManualAssignGridComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {

  public roomsList: Room[];
  public filteredRooms: Room[] = [];
  public displayedRooms: Room[] = []; // displayed on grid html
  public roomsOnStatusFilter: Room[]; // clone required to display the complete list on roomtype and status filter
  public gridPrimarySort: SortDescriptor[] = [{
    field: 'roomNumber',
    dir: undefined
  }];
  public showSpinner = false;
  public filterApplied = false;
  public hasError = false;
  public gridLoadErrorContent = MAN_RM_ASGN_LOAD_ERR;
  public roomStatusList = ROOM_STATUS;
  public inspected: boolean;
  public stateNames = AppStateNames;

  private sortRules: SortDescriptor[] = [{
    field: 'roomNumber',
    dir: undefined
  }];

  private roomSearchString = '';
  private selectedStatus: string[] = [];
  private rooms$: Subscription = new Subscription();

  @Input() listItem: ArrivalsListModel;
  @Input() showRoomsPanel: boolean; // TODO: check if we can remove this input
  @Input() getListWatcher: Observable<null>;
  @Input() stateName: string;
  @Input() reportIssueAutoFill = ROOM_REPORT_ISSUE_AUTOFILL;
  @Input() showPostMasterRoom: boolean;
  @Input() selectedRoom: Room;
  @Input() disabledRoomType = false;
  @Input() calculateRemainingGridHeight = true;
  @Input() selectedRoomTypes: string[] = [];
  @Output() assignRoom: EventEmitter<Room> = new EventEmitter();

  constructor(
    private roomsService: RoomsService,
    private trackingService: AnalyticsTrackingService) {
    super();
  }

  ngOnInit() {
    this.getList();
    this.subscribeToGetListWatcher();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.showPostMasterRoom && !changes.showPostMasterRoom.isFirstChange()) {
      this.filterPostMasterRoom();
    }
  }

  private subscribeToGetListWatcher(): void {
    this.rooms$.add(this.getListWatcher.subscribe(() => {
      this.getList();
    }));
  }

  private getList(): void {
    this.showSpinner = true;
    this.rooms$.add(this.roomsService.getRooms(this.listItem, this.stateName).subscribe((response: RoomsListResponse) => {
        if (!super.checkIfAnyApiErrors(response)) {
          this.handleRoomsResponse(response);
        } else {
          this.handleRoomsErrorResponse();
        }
      }, (error: any) => {
        super.processHttpErrors(error);
        this.handleRoomsErrorResponse();
      })
    );
  }

  private handleRoomsResponse(response: RoomsListResponse): void {
    this.hasError = false;
    this.roomsList = response.roomStatusList;
    this.showSpinner = false;
    this.computeStatusField(this.roomsList);
    this.cloneToRoomtype(); // adding after filter will display all status in modal
    if (this.shouldFilter()) {
      this.filterAndSort();
    }
  }

  private computeStatusField(roomList: Room[]): void {
    const status1: string[] = [];
    roomList.forEach((value, index, array) => {
      status1[index] = array[index].frontOfficeStatus.concat(' / ').concat(array[index].roomStatus);
      array[index].statusFilter = status1[index];
      array[index].filterOrder = Number(STATUS_HASHMAP[array[index].statusFilter]);
    });
  }

  private handleRoomsErrorResponse(): void {
    this.hasError = true;
    this.showSpinner = false;
    this.trackingService.trackManualRoomLoadError(this.stateName);
  }

  public refresh(): void {
    this.getList();
  }

  private filterAndSort(): void {
    this.applyFilter();
    this.displayedRooms = this.doSort(this.sortRules, this.filteredRooms);
  }

  private doSort(sortRules: SortDescriptor[], list: Room[]): Room[] {
    return orderBy(list, sortRules);
  }

  private applyFilter(): void {
    const roomList = this.getRoomList();
    let filteredRooms: Room[];

    if (this.shouldFilter()) {
      this.filterApplied = true;
      filteredRooms = filterBy(roomList, this.getFilters());
    } else {
      this.filterApplied = false;
      filteredRooms = Object.assign([], roomList);
    }
    this.filteredRooms = filteredRooms;
  }

  private getRoomList(): Room[] {
    if (this.roomsList) {
      if (this.showPostMasterRoom) {
        return cloneDeep(this.roomsList);
      } else {
        return cloneDeep(this.roomsList.filter(x => x.roomType !== 'PM'));
      }
    } else {
      return [];
    }
  }

  private shouldFilter(): boolean {
    return this.roomSearchString !== '' || this.selectedRoomTypes.length > 0 || this.selectedStatus.length > 0;
  }

  private getFilters(): CompositeFilterDescriptor {
    return {
      logic: 'and',
      filters: [
        {field: 'roomNumber', operator: 'contains', value: this.roomSearchString, ignoreCase: true},
        {field: 'roomType', operator: this.containsRoomType, value: this.selectedRoomTypes},
        {field: 'statusFilter', operator: this.containsStatusType, value: this.selectedStatus}
      ]
    };
  }

  private containsRoomType = (itemValue: string, filterValue: string[]): boolean =>
                              filterValue === null || filterValue === undefined || filterValue.length === 0 ?
                              true : filterValue.indexOf(itemValue) > -1;

  private containsStatusType(itemValue: string, filterValue: string[]): boolean {
    return filterValue === null
    || filterValue === undefined
    || filterValue.length === 0 ? true : filterValue.indexOf(itemValue) > -1;
  }

  private determineSortRules(gridSort: SortDescriptor[]): SortDescriptor[] {
    const gridPrimarySort: SortDescriptor = gridSort[0];
    let sortRules: SortDescriptor[];
    if (gridPrimarySort.dir === undefined) {
      sortRules = [this.getDefaultSortRule()];
    } else if (gridPrimarySort.field === 'roomNumber') {
      sortRules = [gridPrimarySort];
    } else if (gridPrimarySort.field === 'roomType') {
      sortRules = [gridPrimarySort, this.getDefaultSortRule()];
    } else if (gridPrimarySort.field === 'status') {
      sortRules = this.getStatusSortRule();
    }
    return sortRules;
  }

  private getDefaultSortRule = (): SortDescriptor => ({field: 'roomNumber', dir: 'asc'});

  private getStatusSortRule(): SortDescriptor[] {
    return [
      { field: 'filterOrder', dir: this.gridPrimarySort[0].dir },
      { field: 'roomType', dir: 'asc' },
      { field: 'roomNumber', dir: 'asc' }
    ];
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.gridPrimarySort = [sort[0]];
    this.sortRules = this.determineSortRules(sort);

    this.filterAndSort();
  }

  searchByNumber(searchString: string): void {
    this.roomSearchString = searchString;
    this.filterAndSort();
  }

  private emitAssignRoom(room: Room): void {
    this.assignRoom.emit(room);
  }

  handleAssign(room: Room): void {
    this.emitAssignRoom(room);
    this.trackingService.trackAssignRoomManually(this.stateName);
  }

  getErrorPanelHeight() {
    const arbitraryValue = 70;
    return this.calculateHeight(arbitraryValue);
  }

  public getGridHeight() {
    const arbitraryValue = 72;
    return !this.calculateRemainingGridHeight ? '255px' : this.calculateHeightContainer(arbitraryValue);
  }

  private calculateHeight(arbitraryValue: number): string {
    // TODO: Check if we can make this more stable
    const defaultHeight = 255;
    const panelElement = document.querySelector('div.room-tasks-col') as HTMLElement;
    const alertElement = document.querySelector('div.flyout div.toast-messages') as HTMLElement;
    if (this.showRoomsPanel && panelElement) {
      return `${panelElement.offsetHeight - (alertElement ? alertElement.offsetHeight : 0) - arbitraryValue}px`;
    }
    return `${defaultHeight - (alertElement ? alertElement.offsetHeight : 0)}px`;
  }

  private calculateHeightContainer(arbitraryValue: number): string {
    const defaultHeight = 265;
    const parentElement = document.querySelector('div.flyout-contents') as HTMLElement;
    const alertElement = document.querySelector('div.flyout div.toast-messages') as HTMLElement;
    const cellElement = document.querySelector('div.manualAssignmentSearch') as HTMLElement;

    let parentMaxHeight = get(parentElement, ['style', 'maxHeight'], null);
    parentMaxHeight = parentMaxHeight ? +parentMaxHeight.replace('px', '') : defaultHeight;

    return `${(parentMaxHeight - (alertElement ? alertElement.offsetHeight : 0) - (cellElement.offsetHeight)) - arbitraryValue}px`;
  }

  public handleRoomTypeChange(roomTypes: string[]): void {
    this.selectedRoomTypes = roomTypes;
    this.filterAndSort();
    this.getGridHeight();
  }

  public handleStatusChange(status: string[]): void {
    this.selectedStatus = status;
    this.filterAndSort();
    this.getGridHeight();
  }

  private cloneToRoomtype(): void {
    this.displayedRooms = this.getRoomList();
    this.roomsOnStatusFilter = cloneDeep(this.displayedRooms);
  }

  private filterPostMasterRoom(): void {
    this.cloneToRoomtype();
    this.filterAndSort();
    this.getGridHeight();
  }

  ngOnDestroy(): void {
    this.rooms$.unsubscribe();
  }
}
