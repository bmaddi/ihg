import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';

import { ConfirmationModalComponent, ConfirmationModalService } from 'ihg-ng-common-components';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { of } from 'rxjs';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { ManualRoomAssignComponent } from './manual-room-assign.component';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { MOCK_RESERVATION_DATA } from '@app/modules/prepare/components/prepare-arrivals-details/mocks/room-assign-mock';
import { RoomList } from '@app/modules/prepare/components/prepare-arrivals-details/constants/room-test-constants';
import { AnalyticsTrackingService } from './../../services/analytics-tracking/analytics-tracking.service';
import { WarningRoomUnassignedModalComponent } from '../../../warning-room-unassigned-modal/warning-room-unassigned-modal.component';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { RoomAssignmentWarningService } from '@modules/prepare/services/room-assignment-warning/room-assignment-warning.service';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import {
  RM_ASGNMNT_ERR_CODES,
  RM_ERR_SLNM_ID
} from '@modules/prepare/components/prepare-arrivals-details/constants/room-assignment-error-code.constant';

describe('ManualRoomAssignComponent', () => {
  let component: ManualRoomAssignComponent;
  let fixture: ComponentFixture<ManualRoomAssignComponent>;
  let roomService: RoomsService;
  let analyticsServ: AnalyticsTrackingService;

  const checkRoomAssignmentErrorTracking = (errorCode?: string) => {
    component.stateName = 'Prepare';
    const roomMock = RoomList[0];
    const mockresponse = {
      'errors': [{
        'code': errorCode,
        'detailText': 'Room assignment error text'
      }]
    };
    spyOn(roomService, 'assignRoom').and.returnValue(of(mockresponse));
    spyOn(analyticsServ, 'trackRoomAssignmentError');

    component.handleAssign(roomMock);
    fixture.detectChanges();

    expect(analyticsServ.trackRoomAssignmentError).toHaveBeenCalledWith('Prepare', true, errorCode);
  };

  const checkRoomAssignmentErrorToast = (errorCode: string, slnmId: string) => {
    component.displayGrid = true;
    component.manualRoomAssignmentSelectorOptions = { selector: 'test', arbitrarySpaceValue: 50 };
    const roomMock = RoomList[0];
    const mockResponse = {
      'errors': [{
        'code': errorCode,
        'detailText': 'Room assignment error text'
      }]
    };
    spyOn(roomService, 'assignRoom').and.returnValue(of(mockResponse));

    component.handleAssign(roomMock);
    fixture.detectChanges();

    const errorToast = fixture.debugElement.query(By.css(`[data-slnm-ihg="${slnmId}"]`)).nativeElement;
    expect(errorToast).toBeTruthy();
  };

  beforeEach(async(() => {
    TestBed
      .overrideProvider(RoomAssignmentWarningService, {
        useValue: {
          openWarningRoomUnassignedModal: (roomNumber?: string) => of('roomMove')
        }
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        declarations: [ManualRoomAssignComponent, WarningRoomUnassignedModalComponent, ConfirmationModalComponent],
        schemas: [NO_ERRORS_SCHEMA],
        imports: [CommonTestModule, BrowserAnimationsModule],
        providers: [ConfirmationModalService, GoogleAnalyticsService]
      })
      .overrideModule(BrowserDynamicTestingModule,
        { set: { entryComponents: [ConfirmationModalComponent] } })
      .overrideComponent(WarningRoomUnassignedModalComponent, {
        set: {
          template: `WarningRoomUnassignedModalComponent`
        }
      })
      .overrideComponent(ConfirmationModalComponent, {
        set: {
          template: `ConfirmationModalComponent`
        }
      })
      .compileComponents().then(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000000;
        setTestEnvironment();
        fixture = TestBed.createComponent(ManualRoomAssignComponent);
        component = fixture.componentInstance;
        roomService = TestBed.get(RoomsService);
        analyticsServ = TestBed.get(AnalyticsTrackingService);
        component.listItem = MOCK_RESERVATION_DATA;
        fixture.detectChanges();
      });

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should check roomassignment error scenario', async(() => {
    component.stateName = '';
    const roomMock = RoomList[0];
    const mockresponse = {
      'errors': [{
        'code': RM_ASGNMNT_ERR_CODES.roomUnavailable,
        'detailText': 'Selected Room is no longer available'
      }]
    };
    spyOn(roomService, 'assignRoom').and.returnValue(of(mockresponse));
    spyOn(component as any, 'handleAssignErrorResponse');
    component.handleAssign(roomMock);
    fixture.detectChanges();
    expect(component['handleAssignErrorResponse']).toHaveBeenCalled();
  }));

  it('should check roomassignment success scenario', async(() => {
    const roomMock = RoomList[0];
    const mockresponse = {};
    spyOn(roomService, 'assignRoom').and.returnValue(of(mockresponse));
    spyOn(component as any, 'handleAssignResponse');
    component.handleAssign(roomMock);
    fixture.detectChanges();
    expect(component['handleAssignResponse']).toHaveBeenCalled();
  }));

  it('should track inventory unavailable error when assigning a room', async(() => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailable);
  }));

  it('should track inventory unavailable error at group level when assigning a room', async(() => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp);
  }));

  it('should track room no longer available error when assigning a room', async(() => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.roomUnavailable);
  }));

  it('Should check if roomassignment general failure error is tracked', async(() => {
    component.stateName = 'Prepare';
    const roomMock = RoomList[0];
    const mockresponse = { 'errors': [{ 'code': 'Some error code', 'detailText': 'Selected Room is no longer available' }] };
    spyOn(roomService, 'assignRoom').and.returnValue(of(mockresponse));
    spyOn(analyticsServ, 'trackRoomAssignmentError');
    component.handleAssign(roomMock);
    fixture.detectChanges();
    expect(analyticsServ.trackRoomAssignmentError).toHaveBeenCalledWith('Prepare', true, undefined);
  }));

  it('"assignRoom()" should call handleMoveRoom() when enableRoomMove is enabled', async(() => {
    component.enableRoomMove = true;
    fixture.detectChanges();
    const handleMoveRoomSpy = spyOn<any>(component, 'handleMoveRoom').and.callThrough();

    const roomMock = RoomList[0];
    component.assignRoom(roomMock);
    expect(handleMoveRoomSpy).toHaveBeenCalled();
  }));

  it('"assignRoom()" should call handleAssignRoom() when enableRoomMove is not enabled', async(() => {
    component.enableRoomMove = false;
    fixture.detectChanges();
    const handleAssignRoomSpy = spyOn<any>(component, 'handleAssignRoom').and.callThrough();

    const roomMock = RoomList[0];
    component.assignRoom(roomMock);
    expect(handleAssignRoomSpy).toHaveBeenCalled();
  }));

  it('"handleMoveRoom()" should check for tasks and show WarningRoomUnassignedModalComponent modal', async(() => {
    const openWarningRoomUnassignedModalSpy = spyOn(TestBed.get(RoomAssignmentWarningService), 'openWarningRoomUnassignedModal')
      .and.returnValue(of('moveRoom'));
    component.enableRoomMove = true;
    component.listItem = {
      ...listItemMock, ...{
        tasks: [{
          'traceId': 443500,
          'traceText': 'Trying to chg dept',
          'departmentCode': 'XYZ',
          'departmentName': '',
          'traceTimestamp': '2019-02-28T10:16:00.0000000-05:00',
          'isResolved': false
        }]
      }
    };
    fixture.detectChanges();

    const roomMock = RoomList[0];
    component['handleMoveRoom'](roomMock);

    expect(openWarningRoomUnassignedModalSpy).toHaveBeenCalled();
  }));

  it('"handleMoveRoom()" should open the no tasks confirmation modal if no tasks are present', async(() => {
    const openWarningRoomUnassignedModalSpy = spyOn(TestBed.get(RoomAssignmentWarningService), 'openWarningRoomUnassignedModal').and.returnValue(of(''));
    component.enableRoomMove = true;
    component.listItem = { ...listItemMock };
    fixture.detectChanges();

    const roomMock = RoomList[0];
    component['handleMoveRoom'](roomMock);

    expect(openWarningRoomUnassignedModalSpy).toHaveBeenCalled();
  }));

  it('should call #assignRoom API service to assign selected room', () => {
    const roomMock = RoomList[0];
    component.listItem = { ...listItemMock };
    const getAssignReleasePayloadSpy = spyOn(TestBed.get(RoomsService), 'getAssignReleasePayload').and.callThrough();
    const assignRoom = spyOn(TestBed.get(RoomsService), 'assignRoom').and.callThrough();
    component.handleAssign(roomMock);
    fixture.detectChanges();

    expect(getAssignReleasePayloadSpy).toBeDefined();
    expect(getAssignReleasePayloadSpy).toHaveBeenCalledWith(component.listItem, roomMock);
    expect(assignRoom).toHaveBeenCalled();
  });

  it('should check post master component is loaded', () => {
    component.displayGrid = true;
    component.manualRoomAssignmentSelectorOptions = { selector: 'test', arbitrarySpaceValue: 50 };
    expect(component.displayGrid).toBeTruthy();

    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="PostMaster-SID"]')).nativeElement;
    expect(element).toBeDefined();
    expect(element.checked).toBeFalsy();
  });

  it('should show proper error toast message when inventory is not available at house level', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse]);
  });

  it('should track room assignment error when inventory is unavailable at house level ', async(() => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse);
  }));

  xit('check "updateGridDisplay"', () => {
    component.displayGrid = false;
    component['shouldShow'] = true;
    component['updateGridDisplay'](true);
    fixture.detectChanges();
    expect(component['displayGrid']).toEqual(component['shouldShow']);
  });
});
