import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AnalyticsTrackingService } from './../../services/analytics-tracking/analytics-tracking.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import {
  ROOM_ERROR_CONTENT
} from '@modules/prepare/components/prepare-arrivals-details/constants/arrivals-details-constants';
import { flyInOut } from '@app/animations/fly-in-out.animation';
import { Room, RoomsListResponse } from '../../models/rooms-list.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ROOM_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { RoomAssignmentWarningService } from '@modules/prepare/services/room-assignment-warning/room-assignment-warning.service';
import { Alert } from 'ihg-ng-common-core';

@Component({
  selector: 'app-manual-room-assign',
  templateUrl: './manual-room-assign.component.html',
  styleUrls: ['./manual-room-assign.component.scss'],
  animations: [flyInOut]
})

export class ManualRoomAssignComponent extends AppErrorBaseComponent implements OnChanges {

  public roomToAssign: Room;
  public showPostMaster: boolean;
  public manualRoomAssignmentError = new Subject<EmitErrorModel>();
  public roomErrorContent = ROOM_ERROR_CONTENT;
  public manualAssignRoomListTrigger = new Subject<null>();
  roomAsignmentDefinedError: Alert;
  roomAsignmentDefinedErrorSlnmId: string;

  @Input() listItem: ArrivalsListModel;
  @Input() displayGrid = false;
  @Input() stateName: string;
  @Input() showRoomNotReadyAlert: boolean;
  @Input() showRoomsPanel: boolean; // TODO: Check if we can remove this Input
  @Input() reportIssueAutoFill = ROOM_REPORT_ISSUE_AUTOFILL;
  @Input() isCheckedIn: boolean;
  @Input() enableRoomMove: boolean;
  @Input() manualRoomAssignmentSelectorOptions: { selector: string, arbitrarySpaceValue: number };
  @Output() gridDisplayChange = new EventEmitter<boolean>();
  @Output() manualRoomAssignment = new EventEmitter<Room>();

  constructor(
    private trackingService: AnalyticsTrackingService,
    private roomsService: RoomsService,
    private roomWarningModalService: RoomAssignmentWarningService
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.displayGrid) {
      this.updateGridDisplay(changes.displayGrid.currentValue);
    }
  }

  private updateGridDisplay(shouldShow: boolean): void {
    if (shouldShow && !this.displayGrid) {
      this.trackingService.trackOpenManualAssignment(this.stateName);
      this.resetAllRoomAssignmentErrors();
    }
    this.displayGrid = shouldShow;
    this.listItem.showManualGrid = shouldShow;
  }

  public handleGridDisplay(shouldShow: boolean): void {
    this.updateGridDisplay(shouldShow);
    this.gridDisplayChange.emit(shouldShow);
  }

  public assignRoom(room: Room): void {
    if (this.enableRoomMove) {
      this.handleMoveRoom(room);
    } else {
      this.handleAssignRoom(room);
    }
  }

  private handleMoveRoom(room: Room) {
    if (this.listItem.tasks && this.listItem.tasks.length > 0) {
      this.roomWarningModalService.openWarningRoomUnassignedModal(this.listItem, this.enableRoomMove, this.stateName, true)
        .subscribe(result => {
          if (result === 'roomMove') {
            this.handleAssignRoom(room);
          }
        }, () => { }
        );
    } else {
      this.roomWarningModalService.openWarningRoomUnassignedModal(this.listItem, this.enableRoomMove, this.stateName, false)
        .subscribe(
          () => this.handleAssignRoom(room),
          () => {
          });
    }
  }

  private handleAssignRoom(room: Room) {
    if (this.shouldShowWarning() && this.isRoomVacantNotInspected(room)) {
      this.roomsService.openRoomInspectionWarningModal(room).subscribe(() => {
        this.handleAssign(room);
      });
    } else {
      this.handleAssign(room);
    }
  }

  private shouldShowWarning(): boolean {
    return this.showRoomNotReadyAlert && this.listItem.inspected;
  }

  private isRoomVacantNotInspected(room: Room): boolean {
    return room ? room.frontOfficeStatus === ROOM_STATUS.Vacant.value && room.roomStatus !== ROOM_STATUS.Inspected.value : false;
  }

  public handleAssign(room: Room): void {
    this.roomToAssign = room;
    this.roomsService.assignRoom(this.roomsService.getAssignReleasePayload(this.listItem, room)).subscribe(
      (response: RoomsListResponse) => {
        if (this.roomsService.isRoomAsignmentDefinedError(response)) {
          this.handleAssignErrorResponse({ room, errorCode: response.errors[0].code });
        } else {
          if (!super.checkIfAnyApiErrors(response, null, this.manualRoomAssignmentError)) {
            this.handleAssignResponse(room);
          } else {
            this.handleAssignErrorResponse();
          }
        }
      }, error => {
        super.processHttpErrors(error, this.manualRoomAssignmentError);
        this.handleAssignErrorResponse();
      });
  }

  private resetAllRoomAssignmentErrors(): void {
    this.resetSpecificError(this.manualRoomAssignmentError);
    this.roomAsignmentDefinedError = null;
  }

  private handleAssignResponse(room: Room): void {
    this.updateGridDisplay(false);
    this.manualRoomAssignment.emit(room);
  }

  private handleAssignErrorResponse(definedError?: { room: Room, errorCode: string }): void {
    if (definedError) {
      const error = this.roomsService.getRoomAssignmentDefinedErrors(definedError.room,
        definedError.errorCode);
      this.resetSpecificError(this.manualRoomAssignmentError);
      this.roomAsignmentDefinedError = error.toast;
      this.roomAsignmentDefinedErrorSlnmId = error.slnmId;
      this.manualAssignRoomListTrigger.next();
    } else {
      this.roomAsignmentDefinedError = null;
    }
    this.trackingService.trackRoomAssignmentError(this.stateName, true, definedError && definedError.errorCode);
  }

  public setFlyOutContainerHeight(): string {
    return this.calculateFlyOutHeight(this.manualRoomAssignmentSelectorOptions.arbitrarySpaceValue);
  }

  private calculateFlyOutHeight(arbitraryValue: number): string {
    const panelElement = document.querySelector(this.manualRoomAssignmentSelectorOptions.selector) as HTMLElement;
    return panelElement ? `${panelElement.offsetHeight - arbitraryValue}px` : null;
  }

  checkShowPostMaster(PMFlag: boolean) {
    this.showPostMaster = PMFlag;
  }
}
