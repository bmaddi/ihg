import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostMasterComponent } from './post-master.component';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

describe('PostMasterComponent', () => {
  let component: PostMasterComponent;
  let fixture: ComponentFixture<PostMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostMasterComponent],
      imports: [FormsModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change value on click', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="PostMasterCheckbox-SID"]')).nativeElement;
    expect(element.checked).toBeFalsy();
    element.click();
    fixture.detectChanges();

    expect(element.checked).toBeTruthy();
  });

  it('should check translate is defined inside checkbox', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="PostMasterLabel-SID"]')).nativeElement;
    expect(element).toBeDefined();
    expect(element.translate).toEqual(true);
  });
});
