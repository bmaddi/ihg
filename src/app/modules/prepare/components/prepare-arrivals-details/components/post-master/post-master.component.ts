import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-post-master',
  templateUrl: './post-master.component.html',
  styleUrls: ['./post-master.component.scss']
})
export class PostMasterComponent implements OnInit {

  public selectedCheckBox: boolean;
  @Output() postMasterChecked = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  showPMRooms(e) {
    this.postMasterChecked.emit(this.selectedCheckBox);
  }
}
