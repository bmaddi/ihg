import { TemplateRef, ViewContainerRef, Renderer2 } from '@angular/core';
import { AccessControlPermission, AccessControlPermissionType } from 'ihg-ng-common-core';
export class PrepareArrivalsDetailsAccessCustomizer {
    constructor() { }

    static taskAccessCustomizer(templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        accessControlItem: AccessControlPermission,
        alternateRef: TemplateRef<any>) {
        if (accessControlItem.access === AccessControlPermissionType.NoAccess) {
            viewContainer.createEmbeddedView(alternateRef);
        } else {
            viewContainer.createEmbeddedView(templateRef);
        }
    }
}
