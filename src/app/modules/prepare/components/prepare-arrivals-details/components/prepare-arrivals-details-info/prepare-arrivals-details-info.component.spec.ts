import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';

import { PrepareArrivalsDetailsInfoComponent } from './prepare-arrivals-details-info.component';
import { DetailListItemMock } from '@modules/prepare/components/prepare-arrivals-details/mocks/details-info-mock.constants';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GuestHeartbeatService } from '@app/modules/guests/services/guest-heartbeat.service';
import { MOCK_HEARTBEAT_DATA } from '@app/modules/prepare/components/prepare-arrivals-details/mocks/prepare-arrival-details-mock';

import { of, Observable, BehaviorSubject, throwError } from 'rxjs';
import { isEqual } from 'lodash';
import { PrepareForArrivalsGaConstants } from '@app/modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { GuestHeartBeat } from '@app/modules/guests/models/guest-profile-heartbeat.model';
import { HeartBeatCommentsService } from '@app/modules/heart-beat-comments/services/heart-beat-comments.service';
import { ManageStayService } from '@modules/manage-stay/services/manage-stay.service';
import { RateCategoryModel } from '@modules/offers/models/offers.models';
import {
  RoomRateDescriptionModalComponent
} from '@modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';
import { RATE_CODE_DETAILS, RATE_CODE_MODAL_PARAMS } from '@modules/manage-stay/mocks/manage-stay-mock';
import { MOCK_RATE_DETAILS } from '../../mocks/prepare-arrival-details-mock';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) {
  }
}

export class MockHeartBeatCommentsService {
  public openHeartBeatCommentsModal(comment: string, improvement: string, date?: string): void { }
}

export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve('x'));
  componentInstance = RATE_CODE_MODAL_PARAMS;
}

describe('PrepareArrivalsDetailsInfoComponent', () => {
  let component: PrepareArrivalsDetailsInfoComponent;
  let fixture: ComponentFixture<PrepareArrivalsDetailsInfoComponent>;
  let guestHeartBeat: GuestHeartbeatService;
  let gaServ: MockGoogleAnalyticsService;
  let manageStayService: ManageStayService;
  let heartBeatCommentServ: HeartBeatCommentsService;

  let check_api: boolean;
  let modalService: NgbModal;
  const mockModalRef: MockNgbModalRef = new MockNgbModalRef();

  class MockManageStayService {
    public getRateCodeData(reservationNumber) {
      return check_api ? of(RATE_CODE_DETAILS) : throwError('error');
    }
  }

  function checkObjectEquality(a, b) {
    return isEqual(a, b);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [TranslateModule.forRoot(), NgbModule, HttpClientTestingModule],
      declarations: [PrepareArrivalsDetailsInfoComponent],
      providers: [
        TranslateService, UserService,
        { provide: HeartBeatCommentsService, useClass: MockHeartBeatCommentsService },
        { provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService },
        { provide: ManageStayService, useClass: MockManageStayService },
        GuestHeartbeatService
      ]
    })
      .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(PrepareArrivalsDetailsInfoComponent);
    component = fixture.componentInstance;
    component.listItem = DetailListItemMock;
    component.showReservationInfo = true;
    component.stateName = 'guest-list-details';
    guestHeartBeat = TestBed.get(GuestHeartbeatService);
    gaServ = TestBed.get(GoogleAnalyticsService);
    heartBeatCommentServ = TestBed.get(HeartBeatCommentsService);
    modalService = TestBed.get(NgbModal);
    manageStayService = TestBed.get(ManageStayService);
    fixture.detectChanges();
  });

  xit('Check room to charge popup displayed on mouseenter', async(() => {
    const roomcategoryInfo = fixture.debugElement.queryAll(By.css('.section-space'))[1];
    let event = new Event('mouseenter');
    const roomCat = roomcategoryInfo.nativeElement;
    roomCat.query(By.css('span')).dispatchEvent(event);
    fixture.detectChanges();
    const popover = fixture.debugElement.query(By.css('popover'));
    expect(popover.nativeElement).not.toBe(null);
  }));

  xit('verify heartBeat Response', () => {
    const getGuestProfileHeartbeatSpy = spyOn(guestHeartBeat, 'getGuestProfileHeartbeat').and.returnValue(of(MOCK_HEARTBEAT_DATA));
    component['handleHeartBeatResponse'](MOCK_HEARTBEAT_DATA);
    fixture.detectChanges();
    const checkObjectEqual = checkObjectEquality(component.heartBeatData, MOCK_HEARTBEAT_DATA);
    expect(checkObjectEqual).toEqual(true);
  });

  xit('verify "handleIconClick" method', () => {
    const trackEventSpy = spyOn(gaServ, 'trackEvent');
    component.handleIconClick(MOCK_HEARTBEAT_DATA.heartBeatOtherHotel);
    fixture.detectChanges();
    expect(trackEventSpy).toHaveBeenCalledWith(PrepareForArrivalsGaConstants.CATEGORY,
      PrepareForArrivalsGaConstants.VIEW_HEARTBEAT_COMMENTS,
      PrepareForArrivalsGaConstants.SELECTED);
  });

  it('Should fetch Rate code data from getRateCodeDetails and Call openDescriptionModal function', () => {
    /* check_api = true;
    spyOn(component, 'openDescriptionModal');
    spyOn(gaServ, 'trackEvent').and.callThrough();
    component.getRateCodeDetails();
    expect(component.openDescriptionModal).toHaveBeenCalledWith(null, RATE_CODE_DETAILS);
    expect(gaServ.trackEvent).toHaveBeenCalledWith('Guest List - Prepare', 'Rate Category', 'Selected'); */
  });

  it('Should open the Rate code modal from openDescriptionModal function', () => {
    /* this.data = RATE_CODE_DETAILS;
    spyOn(modalService, 'open').and.returnValue(mockModalRef);
    component.openDescriptionModal(null, RATE_CODE_DETAILS);
    expect(modalService.open).toHaveBeenCalledWith(RoomRateDescriptionModalComponent,
      { windowClass: 'large-modal', backdrop: 'static', keyboard: false }); */
  });

  it('Should check if modal opens in case of Data timeout/Internal Server Error', () => {
    /* check_api = false;
    component.listItem.checkInDate = MOCK_RATE_DETAILS.checkInDate;
    component.listItem.checkOutDate = MOCK_RATE_DETAILS.checkOutDate;
    component.listItem.ihgRcNumber = MOCK_RATE_DETAILS.ihgRcNumber;
    component.listItem.roomTypeCode = MOCK_RATE_DETAILS.roomTypeCode;
    component.listItem.rateCategoryCode = MOCK_RATE_DETAILS.rateCategoryCode;
    spyOn(modalService, 'open').and.returnValue(mockModalRef);
    component.getRateCodeDetails();
    expect(modalService.open).toHaveBeenCalledWith(RoomRateDescriptionModalComponent,
      { windowClass: 'large-modal', backdrop: 'static', keyboard: false }); */
  });

  it('should display tooltip on the free room upgrade icon', () => {
    component.showReservationInfo = true;
    component.listItem.freeUpgradeInd = true;
    component.listItem.assignedRoomStatus = {
      roomNumber: '104', roomStatus: 'Clean', roomType: 'KDXG', houseKeepingStatus: 'Clean', frontOfficeStatus: 'Vacant',
      features: [], reservationFeatures: []
    };
    fixture.detectChanges();

    const roomCat = fixture.debugElement.query(By.css('[data-slnm-ihg=UpgradeRoomTypeCategoryCode-SID]'));
    expect(roomCat).toBeDefined();
    expect(roomCat).not.toBeNull();

    const upgradeContent = fixture.debugElement.query(By.css('[data-slnm-ihg=UpgradeRoomContent-SID]'));
    expect(upgradeContent).toBeDefined();
    expect(upgradeContent).not.toBeNull();

    upgradeContent.triggerEventHandler('mouseenter', null);

    fixture.detectChanges();
    const tooltipElement = document.querySelector('ngb-tooltip-window');
    expect(tooltipElement).toBeDefined();
    expect(tooltipElement).not.toBeNull();
  });

});
