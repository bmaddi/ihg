import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { HeartBeatCommentsService } from '@modules/heart-beat-comments/services/heart-beat-comments.service';
import { ShowMoreSpecialRequestsComponent } from '../show-more-special-requests/show-more-special-requests.component';
import { GuestHeartbeatService } from '@modules/guests/services/guest-heartbeat.service';
import { GuestHeartBeat, GuestHeartBeatSurvey } from '@modules/guests/models/guest-profile-heartbeat.model';
import { shouldNotShowHeartBeat } from '@modules/guests/constants/guest-profile-heartbeat.constants';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ManageStayService } from '@modules/manage-stay/services/manage-stay.service';
import { RateCategoryModel, RateCategoryDataModel } from '@modules/offers/models/offers.models';
import { RoomRateDescriptionModalComponent } from '@modules/offers/components/room-rate-description-modal/room-rate-description-modal.component';
import {GUEST_CONST} from '@modules/guests/guests.constants';


@Component({
  selector: 'app-prepare-arrivals-details-info',
  templateUrl: './prepare-arrivals-details-info.component.html',
  styleUrls: ['./prepare-arrivals-details-info.component.scss']
})
export class PrepareArrivalsDetailsInfoComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  public showReservationInfo: boolean;
  public characterCount = 50;
  public specialRequests: string;
  public heartBeatData: GuestHeartBeat;
  private ga = GUEST_CONST.GA;
  private detailsInfo$: Subscription = new Subscription();

  @Input() listItem: ArrivalsListModel;
  @Input() stateName: string;
  @Input() preferenceLabel: string;
  @Input() showGuestRelations = true;
  popupContentClass: string;

  constructor(private gaService: GoogleAnalyticsService,
    private modalService: NgbModal,
    private heartBeatService: HeartBeatCommentsService,
    private guestHeartBeat: GuestHeartbeatService,
    private manageStayService: ManageStayService) {
    super();
  }

  ngOnInit() {
    this.setShowReservationInfo();
    this.specialRequests = this.listItem.specialRequests.join(' ');
    this.getHeartBeatData();
    this.assignCSSClass();
  }

  private assignCSSClass(){
    this.popupContentClass = "";
    if(!this.listItem.ihgRcNumber){
      this.popupContentClass = "popover-content";
    }
  }

  private setShowReservationInfo(): void {
    this.showReservationInfo = this.stateName && this.stateName === 'guest-list-details';
  }

  private getHeartBeatData() {
    const memberId = this.listItem.ihgRcNumber;
    if (memberId) {
      this.detailsInfo$.add(this.guestHeartBeat.getGuestProfileHeartbeat({ memberId }, false).subscribe(
        (res: GuestHeartBeat) => this.handleHeartBeatResponse(res)
      ));
    }
  }

  private handleHeartBeatResponse(res: GuestHeartBeat): void {
    this.heartBeatData = res;
  }

  public handleIconClick(data: GuestHeartBeatSurvey): void {
    this.trackHeartBeatComments();
    this.openCommentsModal(data);
  }

  public openCommentsModal(data: GuestHeartBeatSurvey): void {
    this.heartBeatService.openHeartBeatCommentsModal(data.likeComment, data.makeStayBetterComment, data.surveyDate);
  }

  private trackHeartBeatComments(): void {
    this.gaService.trackEvent(
      PrepareForArrivalsGaConstants.CATEGORY,
      PrepareForArrivalsGaConstants.VIEW_HEARTBEAT_COMMENTS,
      PrepareForArrivalsGaConstants.SELECTED);
  }

  private showMoreSpecialReq() {
    const modal = this.modalService.open(ShowMoreSpecialRequestsComponent, { windowClass: 'special-requests' });
    const modalComponent = modal.componentInstance as ShowMoreSpecialRequestsComponent;
    modalComponent.specialRequests = this.specialRequests;
  }

  public hasMyHotelData(): boolean {
    return this.heartBeatData && !shouldNotShowHeartBeat(this.heartBeatData.heartBeatMyHotel);
  }

  public hasOtherHotelData(): boolean {
    return this.heartBeatData && !shouldNotShowHeartBeat(this.heartBeatData.heartBeatOtherHotel);
  }

  public isValidGuestLoveScore(val: string): boolean {
    return (val === 'R' || val === 'R*');
  }

  public getRateCodeDetails() {
    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY, this.ga.RATE_CATEGORY, this.ga.SELECTED);
    const rateDetailsData = {
      'checkInDate': this.listItem.checkInDate,
      'checkOutDate': this.listItem.checkOutDate,
      'ihgRcNumber': this.listItem.ihgRcNumber,
      'roomTypeCode': this.listItem.roomTypeCode,
      'rateCategoryCode': this.listItem.rateCategoryCode
    };
    this.manageStayService.getRateCodeData(rateDetailsData).subscribe((response) => {
      if (!this.checkIfAnyApiErrors(response)) {
        this.openDescriptionModal(null, response);
      } else {
        this.openDescriptionModal(rateDetailsData, null, true);
      }
    }, (error) => {
      this.processHttpErrors(error);
      this.openDescriptionModal(rateDetailsData, null);
    });
  }

  public openDescriptionModal(serviceDetails: RateCategoryDataModel, data: RateCategoryModel, noData: boolean = false) {
    const modal = this.modalService.open(RoomRateDescriptionModalComponent,
      { windowClass: 'large-modal', backdrop: 'static', keyboard: false });
    modal.componentInstance.topic = 'Guest List - Prepare';
    modal.componentInstance.roomCode = this.listItem.roomTypeCode;
    modal.componentInstance.roomDesc = '';
    modal.componentInstance.arrivalDate = this.listItem.checkInDate;
    modal.componentInstance.departureDate = this.listItem.checkOutDate;
    modal.componentInstance.isOnlyRateInfo = true;
    modal.componentInstance.roomName = '';

    if (data) {
      modal.componentInstance.rateName = data.name;
      modal.componentInstance.rateCode = data.code;
      modal.componentInstance.pointsEligible = data.pointsEligible;
      modal.componentInstance.rateInfo = data.rateInfo;
    } else {
      modal.componentInstance.noData = noData;
      modal.componentInstance.noRateDetailsErrorEvent = this.emitError;
      modal.componentInstance.rateCodeData = serviceDetails;
    }
    return modal;
  }

  ngOnDestroy(): void {
    this.detailsInfo$.unsubscribe();
  }

}
