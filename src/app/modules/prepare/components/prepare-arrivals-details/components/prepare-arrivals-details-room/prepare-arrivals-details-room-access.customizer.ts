
import { TemplateRef, ViewContainerRef, Renderer2 } from '@angular/core';
import { AccessControlPermission, AccessControlPermissionType } from 'ihg-ng-common-core';

export class PrepareArrivalsDetailsRoomAccessCustomizer {
    constructor(private renderer: Renderer2) { }

    static doNotMoveAccessCustomizer(templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        accessControlItem: AccessControlPermission,
        alternateRef: TemplateRef<any>) {
            if (accessControlItem.access === AccessControlPermissionType.ReadWrite) {
                viewContainer.createEmbeddedView(templateRef);
            } else if (accessControlItem.access === AccessControlPermissionType.ReadOnly) {
                viewContainer.createEmbeddedView(alternateRef);
            } else if (accessControlItem.access === AccessControlPermissionType.NoAccess) {
                viewContainer.clear();
            }
    }
}