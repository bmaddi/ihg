import { async, ComponentFixture, fakeAsync, getTestBed, TestBed, tick } from '@angular/core/testing';
import { TranslateFakeLoader, TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, Subject } from 'rxjs';
import { By } from '@angular/platform-browser';
import { cloneDeep } from 'lodash';

import {
  AccessControlModule, AccessControlService, AccessControlPermission,
  GoogleAnalyticsService, PipesModule, UserService, AlertType
} from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { UserGuidanceModule } from 'ihg-ng-common-kendo';

import { PrepareArrivalsDetailsRoomComponent } from './prepare-arrivals-details-room.component';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { RoomsService } from '../../services/details-rooms/rooms.service';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { ReportIssueByStateName } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { ROOM_SUGGESTIONS_MOCK } from '@modules/prepare/components/prepare-arrivals-details/mocks/room-assign-mock';
import { RoomList } from '@modules/prepare/components/prepare-arrivals-details/constants/room-test-constants';
import {
  AnalyticsTrackingService
} from '@modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { CommentModel } from '@app/modules/do-not-move/models/do-not-move.models';
import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';
import { RM_ASGNMNT_ERR_CODES, RM_ERR_SLNM_ID } from '../../constants/room-assignment-error-code.constant';

import {
  MOCK_PREP_ARRV_RESRV_DATA, MOCK_ROOM, MOCK_NO_MATCHES
} from '@app/modules/prepare/components/prepare-arrivals-details/mocks/prepare-arrival-details-mock';

import { DoNotMoveService } from '@app/modules/do-not-move/services/do-not-move.service';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { RoomInfoComponent } from '@modules/prepare/components/prepare-arrivals-details/components/room-info/room-info.component';

describe('PrepareArrivalsDetailsRoomComponent', () => {
  let component: PrepareArrivalsDetailsRoomComponent;
  let fixture: ComponentFixture<PrepareArrivalsDetailsRoomComponent>;
  let roomServ: RoomsService;

  function prepareRoomData() {
    component.listItem = MOCK_PREP_ARRV_RESRV_DATA.reservationData;
    const mySpy = spyOn(roomServ, 'getSuggestedRooms').and.returnValue(of(MOCK_ROOM));
    component.ngOnInit();
    fixture.detectChanges();
  }

  const checkRoomAssignmentErrorToast = (errorCode: string, slnmId: string, isUpgrade: boolean) => {
    const roomMock = RoomList[0];
    const mockResponse = {
      'errors': [{
        'code': errorCode,
        'detailText': 'Room assignment error text'
      }]
    };
    const assignRoomSpy = spyOn(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of(mockResponse));
    component.upgradeRoom = isUpgrade ? {
      roomNumber: '5001',
      selected: true
    } : null;
    spyOn(roomServ, 'isRoomAsignmentDefinedError').and.returnValue(true);
    spyOn(roomServ, 'getRoomAssignmentDefinedErrors').and.returnValue({
      toast: {
        type: AlertType.Danger,
        message: '',
        dismissible: false,
        detailMessage: 'LBL_NO_INVNTRY',
        test: ''
      },
      slnmId: slnmId
    });
    component.handleAssign(roomMock);
    fixture.detectChanges();

    const errorToast = fixture.debugElement.query(By.css(`[data-slnm-ihg="${slnmId}"]`)).nativeElement;
    expect(assignRoomSpy).toHaveBeenCalled();
    expect(errorToast).toBeDefined();
  };

  const checkRoomAssignmentErrorTracking = (errorCode: string, isUpgrade: boolean) => {
    const analyticsService = TestBed.get(AnalyticsTrackingService);
    const roomService = TestBed.get(RoomsService);
    const roomMock = RoomList[0];
    const mockresponse = {
      'errors': [{
        'code': errorCode,
        'detailText': 'Room assignment error text'
      }]
    };
    component.stateName = 'Prepare';
    component.upgradeRoom = isUpgrade ? {
      roomNumber: '5001',
      selected: true
    } : null;
    spyOn(roomService, 'assignRoom').and.returnValue(of(mockresponse));
    spyOn(roomServ, 'isRoomAsignmentDefinedError').and.returnValue(true);
    spyOn(analyticsService, 'trackRoomAssignmentError');
    spyOn(roomServ, 'getRoomAssignmentDefinedErrors').and.returnValue({
      toast: {
        type: AlertType.Danger,
        message: '',
        dismissible: false,
        detailMessage: 'LBL_NO_INVNTRY',
        test: ''
      },
      slnmId: 'slnmId'
    });

    component.handleAssign(roomMock);
    fixture.detectChanges();

    expect(analyticsService.trackRoomAssignmentError).toHaveBeenCalledWith('Prepare', false, errorCode, isUpgrade);
  };

  const checkRoomAssignmentSystemError = (isUpgrade: boolean) => {
    const roomMock = RoomList[0];
    const mockResponse = { 'errors': [{ 'code': 'ERR-500', 'detailText': '' }] };
    const assignRoom = spyOn(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of(mockResponse));
    const trackError = spyOn(TestBed.get(AnalyticsTrackingService), 'trackRoomAssignmentError').and.callThrough();
    component.stateName = 'Prepare';
    component.upgradeRoom = isUpgrade ? {
      roomNumber: '5001',
      selected: true
    } : null;

    component.handleAssign(roomMock);
    fixture.detectChanges();

    expect(assignRoom).toHaveBeenCalled();
    expect(trackError).toHaveBeenCalledWith(component.stateName, false, null, isUpgrade);
  };

  let mockUserService;
  let mockConfirmationModalService;
  let mockGoogleAnalyticsService;
  let mockAnalyticsTrackingService;
  let mockAppErrorsService;
  let mockAccessControlService;
  let mockArrivalsService;
  let mockDoNotMoveService;
  let testBed: TestBed;
  let translate: any;

  class MockRoomService {
    getSuggestedRooms() {
      return new Subject();
    }
    roomSettingsEnabled() { }
    assignRoom() {
      return { arrivalDate: '00:00', checkInDate: '2019-10-25', checkOutDate: '2019-10-26', frontOfficeStatus: 'Occupied', houseKeepingStatus: 'PickUp', pmsReservationNumber: '30902', reservationFeatureTOList: [], reservationNumber: '21726579', roomNumber: '0101', roomStatus: 'PickUp', roomType: 'KNGN' };
    }
    getAssignReleasePayload() { }
    getRoomAssignErrorSubject() { }
    isRoomAsignmentDefinedError() { }
    listenDnmErrorRetryCheckInPage() {
      return of({});
    }
    emitDnmErrorRetryCheckInPage() { }
    getDNMAddReasonErrorContent() { }
    emitDnmErrorCheckInPage() { }
    emitDnmAddedCheckInPage() { }
    isEntitledForUpgrade() { }
    setRoom() { }
    showSuccessfulRoomChangeMessage() { }
    getRoomAssignmentDefinedErrors() { }
  }

  class MockAnalyticsTrackingService {
    trackAssignSuggestedRoom() { }
    trackSuggestedRoomError() { }
    checkFreeRoomUpgrade() { }
    trackRoomAssignmentError() { }
  }

  class MockArrivalService {
    ifCheckInPage = true;
  }

  beforeEach(async(() => {
    mockUserService = jasmine.createSpyObj(['']);
    mockGoogleAnalyticsService = jasmine.createSpyObj(['']);
    mockConfirmationModalService = jasmine.createSpyObj(['']);
    mockUserService = jasmine.createSpyObj(['']);
    mockAppErrorsService = jasmine.createSpyObj(['']);
    mockArrivalsService = jasmine.createSpyObj(['']);
    mockDoNotMoveService = jasmine.createSpyObj(['retryAddReason']);
    mockAccessControlService = jasmine.createSpyObj(['getAccessControlPermissions']);
    mockAccessControlService.accessControlPermissions = [
      { 'name': 'Do Not Move Room', 'tagName': 'do_not_move_room', 'type': 'C', 'access': 'W' }];
    mockAnalyticsTrackingService = jasmine.createSpyObj(['trackAssignSuggestedRoom', 'trackSuggestedRoomError']);
    mockDoNotMoveService.retryAddReason.and.returnValue(new Subject<boolean>());
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      }).overrideProvider(ArrivalsService, {
        useValue: MockArrivalService
      }).configureTestingModule({
        imports: [
          TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: TranslateFakeLoader } }),
          HttpClientTestingModule,
          UserGuidanceModule,
          CommonModule,
          NgbModule,
          BrowserAnimationsModule,
          AccessControlModule,
          NgbTooltipModule,
          PipesModule
        ],
        declarations: [
          PrepareArrivalsDetailsRoomComponent,
          RoomInfoComponent
        ],
        providers: [
          TitleCasePipe,
          {
            provide: UserService,
            useValue: mockUserService
          },
          {
            provide: ConfirmationModalService,
            useValue: mockConfirmationModalService
          },
          {
            provide: GoogleAnalyticsService,
            useValue: mockGoogleAnalyticsService
          },
          {
            provide: RoomsService,
            useClass: MockRoomService
          },
          {
            provide: AnalyticsTrackingService,
            useClass: MockAnalyticsTrackingService
          },
          {
            provide: AppErrorsService,
            useValue: mockAppErrorsService
          },
          {
            provide: AccessControlService,
            useValue: mockAccessControlService
          },
          {
            provide: ArrivalsService,
            useClass: MockArrivalService
          },
          {
            provide: DoNotMoveService,
            useValue: mockDoNotMoveService
          }

        ],
        // Todo: remove as unit test on this component progresses
        // Using No Errors Schema to for Shallow Component Testing,
        // ideally this should be removed as unit tests are added for different shallow components within this component
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    testBed = getTestBed();
    translate = testBed.get(TranslateService);
    translate.setTranslation('en', {
      'LBL_READ_ONLY_PERMISSION': 'You do not have permission to perform this action.',
      'LBL_ROOM_SETTINGS': 'Room Settings'
    });
    translate.use('en');
  }));

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  });

  beforeEach(() => {
    TestBed.get(RoomsService);
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000000;
    mockAccessControlService = TestBed.get(AccessControlService);
    fixture = TestBed.createComponent(PrepareArrivalsDetailsRoomComponent);
    component = fixture.componentInstance;
    roomServ = TestBed.get(RoomsService);
    component.suggestedRoomCount = 3;
    component.stateName = 'manage-stay';
    component.listItem = { ...listItemMock };
    component.roomAnimationContext = ".k-detail-row";
    component.animationDelay = "10ms";
    spyOn<any>(component, 'getDistanceToMove').and.returnValue('translateX(5px)');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the Suggested Upgrade Panel for free upgrade when user if #isEntitledForUpgrade is true', async(() => {
    spyOn<any>(TestBed.get(RoomsService), 'roomSettingsEnabled').and.returnValue(false);
    const spy = spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK)),
      isEntitledForUpgrade = spyOn(TestBed.get(RoomsService), 'isEntitledForUpgrade').and.returnValue(true);

    component.stateName = AppStateNames.guestListDetails;
    component.ngOnInit();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(component.rooms.length).toEqual(3);
    expect(fixture.debugElement.query(By.css('.upgrade-panel-header'))).not.toBeNull();
  }));

  it('should show not the Suggested Upgrade Panel for when guest is NOT entitled to an upgrade', async(() => {
    spyOn<any>(TestBed.get(RoomsService), 'roomSettingsEnabled').and.returnValue(false);
    const spy = spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK)),
      isEntitledForUpgrade = spyOn(TestBed.get(RoomsService), 'isEntitledForUpgrade').and.returnValue(false);

    component.stateName = AppStateNames.guestListDetails;
    component.ngOnInit();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(component.rooms.length).toEqual(3);
    expect(fixture.debugElement.query(By.css('.upgrade-panel-header')).nativeElement.hasAttribute('hidden')).toBeTruthy();
  }));

  it('"Do Not Move" settings should be created but remain hidden until room selection is made and access check passed', () => {
    fixture.detectChanges();

    expect(component.roomSettingsNotVisible).toBeTruthy();
  });

  // tslint:disable-next-line:max-line-length
  it('should NOT assign button to Suggested Upgrade Room if room is not available for free upgrade when #handleUpgradePanelSelection and No Suggested Matches', () => {
    const initBtnBasePosition = spyOn<any>(component, 'initBtnBasePosition').and.callThrough(),
      setAssignBtnPosition = spyOn<any>(component, 'setAssignBtnPosition').and.callThrough();

    component.isEntitledForUpgrade = true;
    component.noMatches = true;
    expect(component.upgradeRoom).not.toBeDefined();
    component.handleUpgradePanelSelection(null);

    expect(initBtnBasePosition).not.toHaveBeenCalled();
    expect(component.upgradeRoom).toBeDefined();
    expect(component.upgradeRoom).toBeNull();
    expect(setAssignBtnPosition).not.toHaveBeenCalled();
  });

  // tslint:disable-next-line:max-line-length
  it('should assign button to Suggested Upgrade Room if upgrade room is available for free upgrade when #handleUpgradePanelSelection and No Suggested Matches', fakeAsync(() => {
    const initBtnBasePosition = spyOn<any>(component, 'initBtnBasePosition').and.callThrough(),
      resetSelectionState = spyOn<any>(component, 'resetSelectionState').and.callThrough(),
      setAssignBtnPosition = spyOn<any>(component, 'setAssignBtnPosition').and.callThrough();
    component.isEntitledForUpgrade = true;
    component.noMatches = true;

    expect(component.upgradeRoom).not.toBeDefined();
    component.handleUpgradePanelSelection({ selected: true, roomNumber: '12132' });
    tick(1000);
    fixture.detectChanges();

    expect(initBtnBasePosition).toHaveBeenCalled();
    expect(component.upgradeRoom).toBeDefined();
    expect(setAssignBtnPosition).toHaveBeenCalledWith(0, true);
    expect(resetSelectionState).not.toHaveBeenCalled();
    expect(component.upgradeRoom).not.toBeNull();
  }));

  it('should update free upgrade indicator if upgraded room is released', () => {
    component.listItem.freeUpgradeInd = true;
    component.handleReleaseRoom();
    expect(component.listItem.freeUpgradeInd).toBeFalsy();
  });

  it('should update free upgrade indicator if non upgrade room is assigned', () => {
    component.upgradeRoom = RoomList[0];
    component.upgradeRoom.selected = false;
    component.handleAssignResponse(RoomList[0], component.upgradeRoom.selected);
    expect(component.listItem.freeUpgradeInd).toBeFalsy();
  });

  /* UT for feat(F16494|US102932) */
  it('Should show \'Do Not Move\' toggle button in Read  mode when user has been given read permission to \'Do Not Move\' sub capablity', () => {
    component.listItem.assignedRoomNumber = '12333';
    component.showRoomsPanel = true;


    spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK));
    const assign = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({}));
    recreate([{ 'name': 'Do Not Move Room', 'tagName': 'do_not_move_room', 'type': 'C', 'access': 'R' }]);
    component.stateName = 'manage-stay';

    component.handleAssign(component.rooms[0]);
    fixture.detectChanges();
    const doNotMoveRoom = fixture.debugElement.query(By.css('bswitch[data-slnm-ihg^="DoNotMoveToggle-SID"]'));

    expect(component.rooms.length).toEqual(3);
    expect(component.selectedRoomIndex).toEqual(0);
    expect(assign).toHaveBeenCalled();
    expect(doNotMoveRoom.attributes['switch-disabled']).toBeTruthy();
    expect(fixture.debugElement.query(By.css('span[data-slnm-ihg^="RoomSettings-SID"]'))).toBeDefined();
    expect(fixture.debugElement.query(By.css('span[data-slnm-ihg^="RoomSettings-SID"]')).nativeElement.innerText).toBe('Room Settings');
  });

  /* UT for feat(F16494|US102932) */
  it('Should show tooltip \'Do Not Move\' toggle button in Read  mode when user has been given read permission to \'Do Not Move\' sub capablity', () => {
    component.listItem.assignedRoomNumber = '12333';
    component.showRoomsPanel = true;
    spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK));
    const assign = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({}));
    recreate([{ 'name': 'Do Not Move Room', 'tagName': 'do_not_move_room', 'type': 'C', 'access': 'R' }]);
    component.stateName = 'manage-stay';

    component.handleAssign(component.rooms[0]);
    fixture.detectChanges();

    const doNotMoveRoom = fixture.debugElement.query(By.css('bswitch[data-slnm-ihg^="DoNotMoveToggle-SID"]'));

    expect(component.rooms.length).toEqual(3);
    expect(component.selectedRoomIndex).toEqual(0);
    expect(assign).toHaveBeenCalled();
    expect(doNotMoveRoom.attributes['switch-disabled']).toBeTruthy();

    const bulkNotificationBell = fixture.debugElement.queryAll(By.css('div.toggle-switch__toggle[data-slnm-ihg^="DoNotMoveToggleToolTip-SID"]'));
    bulkNotificationBell[0].triggerEventHandler('mouseenter', null);

    const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
    expect(tooltip).not.toBeNull();
    expect(tooltip.nativeElement.innerText).toEqual('You do not have permission to perform this action.');
  });

  /* UT for feat(F16494|US102933) */
  it('Should show \'Do Not Move\' toggle button in Read Write mode when user has been given write permission to \'Do Not Move\' sub capablity', () => {
    component.listItem.assignedRoomNumber = '23332423';
    component.showRoomsPanel = true;
    spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK));
    const assign = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({}));
    component.stateName = 'manage-stay';
    recreate([{ 'name': 'Do Not Move Room', 'tagName': 'do_not_move_room', 'type': 'C', 'access': 'W' }]);

    component.handleAssign(component.rooms[0]);
    fixture.detectChanges();

    const doNotMoveRoom = fixture.debugElement.query(By.css('app-do-not-move'));
    expect(component.rooms.length).toEqual(3);
    expect(component.selectedRoomIndex).toEqual(0);
    expect(assign).toHaveBeenCalled();
    expect(doNotMoveRoom.attributes['switch-disabled']).toBeFalsy();
    expect(fixture.debugElement.query(By.css('span[data-slnm-ihg^="RoomSettings-SID"]'))).toBeDefined();
    expect(fixture.debugElement.query(By.css('span[data-slnm-ihg^="RoomSettings-SID"]')).nativeElement.innerText).toBe('Room Settings');

  });

  /* UT for feat(F16494|US102574) */
  it('Should show \'Do Not Move\' toggle button in No Access mode when user has been given No Access permission to \'Do Not Move\' sub capablity', () => {
    component.listItem.assignedRoomNumber = '35345324';
    component.showRoomsPanel = true;
    spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK));
    const assign = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({}));
    component.stateName = 'manage-stay';
    recreate([{ 'name': 'Do Not Move Room', 'tagName': 'do_not_move_room', 'type': 'C', 'access': 'X' }]);

    component.handleAssign(component.rooms[0]);
    fixture.detectChanges();

    expect(component.rooms.length).toEqual(3);
    expect(component.selectedRoomIndex).toEqual(0);
    expect(assign).toHaveBeenCalled();
    expect(fixture.debugElement.query(By.css('app-do-not-move'))).toBeNull();
    expect(fixture.debugElement.query(By.css('span[data-slnm-ihg^="RoomSettings-SID"]'))).toBeNull();
  });

  it('"Do Not Move" settings should be unhidden once room selection is made and access check is true', () => {
    fixture.detectChanges();

    expect(component.showRoomSettingsPanel).toBeFalsy();
    expect(component.roomSettingsNotVisible).toBeTruthy();

    component.listItem.assignedRoomNumber = '204';

    fixture.detectChanges();

    expect(component.roomSettingsNotVisible).toBeFalsy();
  });

  it('should initialize report an issue auto fill for Manage Stay', () => {
    const spy = spyOn<any>(component, 'setReportIssueAutoFill').and.callThrough();
    const reportIssueContent = ReportIssueByStateName['manage-stay'];
    component.stateName = 'manage-stay';
    fixture.detectChanges();
    component['setReportIssueAutoFill']();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    // expect(component.reportIssueAutoFill).toEqual(reportIssueContent);
  });

  it('should assign room if successful', () => {

    const spy = spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(ROOM_SUGGESTIONS_MOCK));
    const assign = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of({}));
    const handleResponse = spyOn<any>(component, 'handleAssignResponse');

    component.stateName = 'manage-stay';
    component.ngOnInit();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(component.rooms.length).toEqual(3);
    fixture.detectChanges();
    expect(component.selectedRoomIndex).toEqual(0);
    component.handleAssign(component.rooms[0]);

    expect(assign).toHaveBeenCalled();
  });

  it('should show proper toast message when the room is no longer available', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.roomUnavailable,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.roomUnavailable], false);
  });

  it('should show proper toast message when inventory is unavailable', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailable,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailable], false);
  });

  it('should show proper toast message  in free upgrade scenario when inventory is unavailable', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailable,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailable], true);
  });

  it('should show proper toast message when inventory is unavailable for group reservations', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp], false);
  });

  it('should show proper error toast message when inventory is unavailable at house level', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse], false);
  });

  it('should show proper toast message in free upgrade scenario when inventory is unavailable for group reservations', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp], true);
  });

  it('should show proper error toast message in free upgrade scenario when inventory is unavailable at house level', () => {
    checkRoomAssignmentErrorToast(RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse,
      RM_ERR_SLNM_ID[RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse], true);
  });

  it('Should track google analytics for suggested room load failure', async(() => {
    const mockResponse = { 'errors': [{ 'code': 'ERR-500', 'detailText': '' }] };
    const handleSuggestedError = spyOn<any>(component, 'handleSuggestedRoomsErrorResponse').and.callThrough();
    const suggestedRoomService = spyOn<any>(TestBed.get(RoomsService), 'getSuggestedRooms').and.returnValue(of(mockResponse));
    const trackingSuggestedLoad = spyOn<any>(TestBed.get(AnalyticsTrackingService), 'trackSuggestedRoomError').and.callThrough();
    component.stateName = 'manage-stay';
    fixture.detectChanges();
    component.getSuggestedRoomsList();
    fixture.detectChanges();
    expect(suggestedRoomService).toHaveBeenCalled();
    expect(handleSuggestedError).toHaveBeenCalled();
    expect(trackingSuggestedLoad).toHaveBeenCalledWith('manage-stay');
  }));

  it('should properly track suggested room assignment system failure', () => {
    checkRoomAssignmentSystemError(false);
  });

  it('should properly track room assignment system failure while doing a free upgrade', () => {
    checkRoomAssignmentSystemError(true);
  });

  it('Should update listItem with "Do Not Move" reason', () => {
    const comment: CommentModel = {
      reason: 'Test Reason',
      date: '12DEC2019 11:45:10',
      userName: 'John Doe'
    };

    component.handleReasonAdded(comment);
    fixture.detectChanges();

    expect(component.listItem.dnmReason).toEqual(comment);
  });

  it('check if suggested matches method is called', () => {
    component.listItem = MOCK_PREP_ARRV_RESRV_DATA.reservationData;
    const mySpy = spyOn<any>(component, 'getSuggestedRoomsList');
    component.ngOnInit();
    fixture.detectChanges();
    expect(mySpy).toHaveBeenCalled();
  });

  it('check if correct roomStatusList is getting populated', () => {
    prepareRoomData();
    // @ts-ignore
    expect(component.roomStatusList.length).toEqual(MOCK_ROOM.roomStatusList.length);
  });

  it('check if proper number of rooms are showing', () => {
    prepareRoomData();
    const predictedRooms = cloneDeep(MOCK_ROOM.roomStatusList).slice(0, component.suggestedRoomCount);
    expect(component.rooms.length).toEqual(predictedRooms.length);
  });

  it('check if no rooms are displayed', async(() => {
    component.listItem = MOCK_PREP_ARRV_RESRV_DATA.reservationData;
    const mySpy = spyOn<any>(component['roomsService'], 'getSuggestedRooms').and.returnValue(of(MOCK_NO_MATCHES));
    component.ngOnInit();
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('[data-slnm-ihg="NoSuggestedMatches-SID"]')).nativeElement;
    expect(el).toBeDefined();
  }));

  it('verify "handleReasonAdded" method for check in page', () => {
    const arrivalService = TestBed.get(ArrivalsService);
    arrivalService.ifCheckInPage = true;
    const roomServ = TestBed.get(RoomsService);
    const comment: CommentModel = {
      reason: 'Test Reason',
      date: '12DEC2019 11:45:10',
      userName: 'John Doe'
    };
    const emitDnmAddedCheckInPageSpy = spyOn(roomServ, 'emitDnmAddedCheckInPage');
    component.handleReasonAdded(comment);
    fixture.detectChanges();

    expect(emitDnmAddedCheckInPageSpy).toHaveBeenCalled();
  });

  it('verify "handleReasonAdded" method for prepare tab', () => {
    const resetErrorSpy = spyOn(Object.getPrototypeOf(Object.getPrototypeOf(component)), 'resetSpecificError');
    const arrivalService = TestBed.get(ArrivalsService);
    arrivalService.ifCheckInPage = false;
    const comment: CommentModel = {
      reason: 'Test Reason',
      date: '12DEC2019 11:45:10',
      userName: 'John Doe'
    };
    component.handleReasonAdded(comment);
    fixture.detectChanges();

    expect(Object.getPrototypeOf(component).resetSpecificError).toHaveBeenCalled();
  });

  it('verify "handleReasonAddedError" for prepare tab', () => {
    const getDNMAddReasonErrorContentSpy = spyOn(roomServ, 'getDNMAddReasonErrorContent');
    component.handleReasonAddedError(false);
    expect(getDNMAddReasonErrorContentSpy).toHaveBeenCalled();
  });

  it('verify "handleReasonAddedError" for check in page', () => {
    const emitDnmErrorCheckInPageSpy = spyOn(roomServ, 'emitDnmErrorCheckInPage');
    component.handleReasonAddedError(true);
    expect(emitDnmErrorCheckInPageSpy).toHaveBeenCalled();
  });

  it('should track inventory unavailable error when assigning a room', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailable, false);
  });

  it('should track inventory unavailable error when upgrading a room', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailable, true);
  });

  it('should track inventory unavailable error for group reservations when assigning a room', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp, false);
  });

  it('should track room no longer available error when assigning a room', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.roomUnavailable, false);
  });

  it('should track room assignment error when inventory is unavailable at house level ', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse, false);
  });

  it('should track inventory unavailable error for group reservations when doing a free upgrade', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp, true);
  });

  it('should track inventory unavailable error at house level when doing a free upgrade', () => {
    checkRoomAssignmentErrorTracking(RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse, true);
  });

  it('check if suggested matches method is called', () => {
    component.listItem = MOCK_PREP_ARRV_RESRV_DATA.reservationData;
    const mySpy = spyOn(component, 'getSuggestedRoomsList');
    component.ngOnInit();
    fixture.detectChanges();
    expect(mySpy).toHaveBeenCalled();
  });

  function recreate(accessControlPermissions: AccessControlPermission[]) {
    fixture.destroy();
    mockAccessControlService = TestBed.get(AccessControlService);
    mockAccessControlService.accessControlPermissions = accessControlPermissions;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000000;
    fixture = TestBed.createComponent(PrepareArrivalsDetailsRoomComponent);
    component = fixture.componentInstance;
    component.suggestedRoomCount = 3;
    component.stateName = 'manage-stay';
    component.listItem = { ...listItemMock };
    component.roomAnimationContext = '.k-detail-row';
    component.animationDelay = '10ms';
    fixture.detectChanges();
  }

  it('should track google analytics for free room upgrade in check-in', () => {
    const roomMock = RoomList[0];
    const mockResponse = {};
    component.stateName = 'check-in';
    component.upgradeRoom = {
      roomNumber: '5001',
      selected: true
    };

    const freeRoomUpgradeSpy = spyOn(TestBed.get(AnalyticsTrackingService), 'checkFreeRoomUpgrade').and.callThrough();
    const roomServiceSpy = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of(mockResponse));
    component.handleAssign(roomMock);
    fixture.detectChanges();
    expect(freeRoomUpgradeSpy).toBeDefined();
    expect(roomServiceSpy).toBeDefined();
    expect(freeRoomUpgradeSpy).toHaveBeenCalledWith(true, 'check-in');
  });

  it('should track google analytics for free room upgrade in prepare', () => {
    const roomMock = RoomList[0];
    const mockResponse = {};
    component.stateName = 'guest-list-details';
    component.upgradeRoom = {
      roomNumber: '5001',
      selected: true
    };

    spyOn(roomServ, 'isRoomAsignmentDefinedError').and.returnValue(false);
    const freeRoomUpgradeSpy = spyOn(TestBed.get(AnalyticsTrackingService), 'checkFreeRoomUpgrade').and.callThrough();
    const roomServiceSpy = spyOn<any>(TestBed.get(RoomsService), 'assignRoom').and.returnValue(of(mockResponse));
    component.handleAssign(roomMock);
    fixture.detectChanges();

    expect(freeRoomUpgradeSpy).toBeDefined();
    expect(roomServiceSpy).toBeDefined();
    expect(freeRoomUpgradeSpy).toHaveBeenCalledWith(true, 'guest-list-details');
  });
});
