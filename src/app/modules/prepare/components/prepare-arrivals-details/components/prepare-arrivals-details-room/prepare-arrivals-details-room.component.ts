import {
  Component, Input, OnInit, AfterViewInit, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';
import { Subscription, of, Subject } from 'rxjs';
import { delay, take, tap } from 'rxjs/operators';
import { cloneDeep, get, isNil } from 'lodash';

import { GoogleAnalyticsService, Alert } from 'ihg-ng-common-core';

import { ArrivalsListModel, RoomChangeModel } from '@modules/prepare/models/arrivals-checklist.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { AnalyticsTrackingService } from './../../services/analytics-tracking/analytics-tracking.service';
import { Room, RoomsListResponse } from '../../models/rooms-list.model';
import { ManualAssignGridComponent } from './../manual-assign-grid/manual-assign-grid.component';
import { slideUpDown } from '@app/animations/slide-up-down.animation';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ROOM_ERROR_CONTENT, ROOM_ASSINMENT } from './../../constants/arrivals-details-constants';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { ReservationStatus } from '@app/modules/guests/enums/guest-arrivals.enums';
import { ReportIssueByStateName } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { CommentModel } from '@app/modules/do-not-move/models/do-not-move.models';
import { DoNotMoveComponent } from '@app/modules/do-not-move/components/do-not-move/do-not-move.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';
import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';
import { PrepareArrivalsDetailsRoomAccessCustomizer } from './prepare-arrivals-details-room-access.customizer';
import { UtilityService } from '@services/utility/utility.service';


@Component({
  selector: 'app-prepare-arrivals-details-room',
  templateUrl: './prepare-arrivals-details-room.component.html',
  styleUrls: ['./prepare-arrivals-details-room.component.scss'],
  animations: [slideUpDown]
})

export class PrepareArrivalsDetailsRoomComponent extends AppErrorBaseComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('appDoNotMove') appDoNotMove: DoNotMoveComponent;
  private btnBasePosition: number;
  private rootSubscription = new Subscription();
  public rooms: Room[];
  public upgradeRoom: Room;
  public disableAnimation = true;
  public assignBtnPosition: string;
  public selectedRoomPosition: string;
  public upgradeRoomPosition: string;
  public roomSettingsPosition: string;
  public selectedRoomIndex: number;
  public shouldAnimate = false;
  public noMatches = false;
  public roomErrorContent = ROOM_ERROR_CONTENT;
  public showRoomsPanel = false;
  public showRoomSettingsPanel = false;
  public placeholderPanels: any[];
  public compactMode = false;
  public enableRoomSettings: boolean;
  public suggestedMatchError = new Subject<EmitErrorModel>();
  public roomAssignmentError = new Subject<EmitErrorModel>();
  public addReasonErrorContent: Partial<ErrorToastMessageTranslations>;
  public addDNMSwitchErrorContent: Partial<ErrorToastMessageTranslations>;
  public changeRoomError = new Subject<EmitErrorModel>();
  public dnmReasonChangeError = new Subject<EmitErrorModel>();
  public dnmSwitchError = new Subject<EmitErrorModel>();
  public retryRelease = new Subject<void>();
  public reportIssueAutoFill = ROOM_REPORT_ISSUE_AUTOFILL;
  public serviceErrorType: string;
  accessModifier = PrepareArrivalsDetailsRoomAccessCustomizer;
  public isEntitledForUpgrade = false;
  roomAsignmentDefinedError: Alert;
  roomAsignmentDefinedErrorSlnmId: string;
  private roomStatusList: Room[];

  @Input() listItem: ArrivalsListModel;
  @Input() expanded: boolean;
  @Input() animationDelay: string;
  @Input() suggestedRoomCount: number;
  @Input() roomAnimationContext: string;
  @Input() showNotifiedAlert: boolean;
  @Input() showArrivalInfo: boolean;
  @Input() showRoomNotReadyAlert: boolean;
  @Input() showPrintRegCard: boolean;
  @Input() stateName: string;
  @Input() enableRoomMove = false;
  @Input() isTodaySelected: boolean;
  @Input() manualRoomAssignmentSelectorOptions: { selector: string, arbitrarySpaceValue: number } = {
    selector: 'div.room-tasks-col',
    arbitrarySpaceValue: 50
  };
  @ViewChild(ManualAssignGridComponent) manualGridRef: ManualAssignGridComponent;
  @Output() emitRoomAssignmentModified: EventEmitter<Room> = new EventEmitter();
  @Output() emitChangedRoom: EventEmitter<RoomChangeModel> = new EventEmitter();
  @Output() roomSettingsChange: EventEmitter<boolean> = new EventEmitter();
  public isCheckedIn: boolean;
  public dnmInteraction = false;
  protected initialRoom: string;
  reportIssueFillData: any;

  get roomSettingsNotVisible(): boolean {
    return !this.listItem.assignedRoomNumber && !this.showRoomSettingsPanel;
  }

  get showUpgradePanel(): boolean {
    return !this.listItem.assignedRoomNumber &&
      this.isEntitledForUpgrade &&
      !this.showRoomSettingsPanel &&
      !(this.upgradeRoom === undefined);
  }

  get selectedRoom(): Room {
    return this.upgradeRoom && this.upgradeRoom.selected ? this.upgradeRoom : this.rooms[this.selectedRoomIndex];
  }

  get isUpgradeSelected(): boolean {
    return get(this.upgradeRoom, 'selected', false);
  }

  constructor(private roomsService: RoomsService,
    private trackingService: AnalyticsTrackingService,
    private appErrorsService: AppErrorsService,
    private gaService: GoogleAnalyticsService,
    private arrivalsService: ArrivalsService,
    private utilityService: UtilityService) {
    super();
  }

  ngOnInit() {
    this.resetAllErrors();
    if (this.listItem.numberOfRooms <= 1) {
      this.initRoomInfo();
      this.determineCompactMode();
      this.setReportIssueAutoFill();
    }
    this.isCheckedIn = this.isGuestCheckedIn();
    this.initialRoom = this.listItem.assignedRoomNumber;
    this.triggerDnmSaveRetry();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.expanded && changes.expanded.currentValue) {
      this.isCheckedIn = this.isGuestCheckedIn();
      this.resetAllRoomAssignmentErrors();
      super.resetSpecificError(this.changeRoomError);
      super.resetSpecificError(this.dnmReasonChangeError);
      super.resetSpecificError(this.dnmSwitchError);
    } else if (changes.suggestedRoomCount && changes.suggestedRoomCount.currentValue) {
      this.handleSuggestedRoomCountChange();
    }
    this.checkForReservationDataChange(changes, this.stateName === AppStateNames.manageStay);
  }

  ngAfterViewInit() {
    setTimeout(() => this.disableAnimation = false);
  }

  onResize() {
    this.shouldAnimate = true;
    this.isUpgradeSelected
      ? this.setAssignBtnPosition(0, true)
      : this.setAssignBtnPosition(this.selectedRoomIndex);
  }

  triggerDnmSaveRetry() {
    this.rootSubscription.add(
      this.roomsService.listenDnmErrorRetryCheckInPage().subscribe(() => {
        this.retrySaveReason();
      })
    );
  }

  private setReportIssueAutoFill(): void {
    if (this.stateName) {
      this.reportIssueAutoFill = ReportIssueByStateName[this.stateName] || ROOM_REPORT_ISSUE_AUTOFILL;
    }
  }

  private checkForReservationDataChange(changes: SimpleChanges, secondaryCondition = true) {
    if (this.utilityService.hasSimpleParamChanges(changes, 'listItem') && secondaryCondition) {
      this.ngOnInit();
    }
  }

  private handleSuggestedRoomCountChange(): void {
    this.determineCompactMode();
    this.placeholderPanels = new Array(this.suggestedRoomCount);
    if (this.roomStatusList && this.roomStatusList.length) {
      this.rooms = cloneDeep(this.roomStatusList).slice(0, this.suggestedRoomCount);
      this.initRoomSelection();
    }
  }

  private determineCompactMode(): void {
    this.compactMode = this.suggestedRoomCount !== 3;
  }

  private resetAllErrors(): void {
    this.resetAllRoomAssignmentErrors();
    super.resetSpecificError(this.suggestedMatchError);
    super.resetSpecificError(this.changeRoomError);
    super.resetSpecificError(this.dnmReasonChangeError);
    super.resetSpecificError(this.dnmSwitchError);
  }

  private setAssignBtnPosition(roomPanelIndex: number, upgrade: boolean = false): void {
    this.assignBtnPosition = !upgrade ?
      this.getDistanceToMove(roomPanelIndex) :
      this.getDistanceToMove(0, this.noMatches ? '.no-matches-upgrade' : '.upgrade-room-panel');
  }

  private getDistanceToMove(roomPanelIndex: number = 0, selector: string = '.room-panel'): string {
    const panel = this.getElement(selector)[roomPanelIndex] as HTMLElement;
    if (panel) {
      const panelCenter = panel.offsetLeft + (panel.offsetWidth / 2);
      return `translateX(${panelCenter - this.btnBasePosition}px)`;
    } else {
      return null;
    }
  }

  private initBtnBasePosition(): void {
    const btn = this.getElement('.action-btn')[0] as HTMLElement;
    if (btn) {
      this.btnBasePosition = btn.offsetLeft + (btn.offsetWidth / 2);
    }
  }

  private initRoomInfo(): void {
    if (this.listItem.assignedRoomNumber) {
      this.checkAssignedRoomStatus();
      this.addRoomsPanel(this.listItem.assignedRoomStatus);
    } else {
      this.placeholderPanels = new Array(this.suggestedRoomCount);
      this.getSuggestedRoomsList();
      this.checkForRoomUpgrade();
    }
  }

  private addRoomsPanel(room: Room): void {
    this.placeholderPanels = [];
    this.rooms = [room];
    this.showRoomsPanel = true;
    this.initRoomSelection();
  }

  public getSuggestedRoomsList(): void {
    this.roomsService.getSuggestedRooms(this.listItem, this.stateName).subscribe((response: RoomsListResponse) => {
      if (!super.checkIfAnyApiErrors(response, null, this.suggestedMatchError)) {
        this.handleRoomStatusResponse(response);
        this.initRoomSelection();
      } else {
        this.handleSuggestedRoomsErrorResponse();
      }
    }, error => {
      this.handleSuggestedRoomsErrorResponse();
      super.processHttpErrors(error, this.suggestedMatchError);
    });
  }

  private handleSuggestedRoomsErrorResponse(): void {
    this.showRoomsPanel = false;
    this.rooms = [];
    this.trackingService.trackSuggestedRoomError(this.stateName);
  }

  private checkAssignedRoomStatus(): void {
    if (!this.listItem.assignedRoomStatus) {
      this.listItem.assignedRoomStatus = {
        roomNumber: this.listItem.assignedRoomNumber
      };
    }
  }

  private handleRoomStatusResponse(response: RoomsListResponse): void {
    if (this.noMatchesFound(response)) {
      this.noMatches = true;
      this.rooms = [];
      this.showRoomsPanel = true;
      this.gaService.trackEvent(this.stateName === ROOM_ASSINMENT.stateName ?
        ROOM_ASSINMENT.categoryCheckIn : ROOM_ASSINMENT.categoryPrepareArrivals,
        ROOM_ASSINMENT.action, ROOM_ASSINMENT.label.replace('{ROOM TYPE}', this.listItem.roomTypeCode));
    } else if (response.roomStatusList && response.roomStatusList.length) {
      this.roomStatusList = cloneDeep(response.roomStatusList);
      this.rooms = (<Room[]>response.roomStatusList).slice(0, this.suggestedRoomCount);
      this.showRoomsPanel = true;
    }
  }

  private noMatchesFound = (response: RoomsListResponse): boolean => (response === {} || response.roomStatusList.length === 0);

  private initRoomSelection(): void {
    if (this.rooms && this.rooms.length) {
      this.rooms[0].selected = true;
      this.selectedRoomIndex = 0;
      setTimeout(() => {
        this.initBtnBasePosition();
        this.setAssignBtnPosition(0);
        this.animateSelectedRoomPanel();
        this.animateRoomSettingsPanel();
        if (!this.listItem.assignedRoomNumber) {
          this.resetUpgradePanel();
        }
      });
    }
  }

  public handleManualAssignGrid(shouldDisplay: boolean): void {
    this.listItem.showManualGrid = shouldDisplay;
    this.listItem.disableManualAssign = shouldDisplay;
    if (shouldDisplay) {
      this.resetAllErrors();
    } else if (!this.showRoomsPanel) {
      this.initRoomInfo();
    }
  }

  private resetAllRoomAssignmentErrors(): void {
    this.resetSpecificError(this.roomAssignmentError);
    this.roomAsignmentDefinedError = null;
  }

  public handleAssign(room: Room): void {
    if (!this.isUpgradeSelected) {
      const matchNumber = this.selectedRoomIndex + 1;
      this.trackingService.trackAssignSuggestedRoom(this.stateName, matchNumber);
    }
    this.roomsService.assignRoom(this.roomsService.getAssignReleasePayload(
      this.listItem, room, this.isUpgradeSelected), this.isUpgradeSelected).subscribe(
        (response: RoomsListResponse) => {
          if (this.roomsService.isRoomAsignmentDefinedError(response)) {
            this.handleAssignErrorResponse({ room, errorCode: response.errors[0].code }, this.isUpgradeSelected);
          } else {
            if (!super.checkIfAnyApiErrors(response, null, this.roomAssignmentError)) {
              this.trackingService.checkFreeRoomUpgrade(this.isUpgradeSelected, this.stateName);
              this.handleAssignResponse(room, this.isUpgradeSelected);
            } else {
              this.handleAssignErrorResponse(null, this.isUpgradeSelected);
            }
          }
        }, error => {
          super.processHttpErrors(error, this.roomAssignmentError);
          this.handleAssignErrorResponse(null, this.isUpgradeSelected);
        });
  }

  public handleReleaseRoom(): void {
    this.listItem.assignedRoomStatus = null;
    this.listItem.assignedRoomNumber = null;
    this.listItem.disableManualAssign = false;
    this.listItem.freeUpgradeInd = false;
    this.showRoomSettingsPanel = false;
    this.selectedRoomPosition = null;
    this.roomsService.setRoom(null);
    this.emitChangedRoom.emit({
      previousRoomNumber: this.initialRoom,
      newRoomNumber: '',
      reservationNumber: this.listItem.reservationNumber
    });
    this.initialRoom = '';
    this.initRoomInfo();
  }

  public handleAssignResponse(room: Room, isUpgradeSelected?: boolean): void {
    this.emitChangedRoom.emit({
      previousRoomNumber: this.initialRoom,
      newRoomNumber: room.roomNumber,
      reservationNumber: this.listItem.reservationNumber
    });
    this.initialRoom = room.roomNumber;
    this.resetAllErrors();
    if (this.hasRoom()) {
      this.hideUnselectedRoomPanels();
      this.showRoomSettingsPanel = !isNil(this.listItem.doNotMoveRoom);
      this.rootSubscription.add(of(true).pipe(
        delay(700),
        tap(() => {
          this.animateSelectedRoomPanel();
          this.animateRoomSettingsPanel();
          this.animateUpgradeRoomPanel();
          this.setAssignBtnPosition(0);
          this.reflectRoomAssignment(room, isUpgradeSelected);
        })
      ).subscribe());
    } else {
      this.addRoomsPanel(room);
      this.reflectRoomAssignment(room, isUpgradeSelected);
    }
  }

  private hideUnselectedRoomPanels(): void {
    this.rooms.forEach((room: Room, index: number) => {
      room.invisible = index !== this.selectedRoomIndex;
    });
  }

  private hasRoom(): boolean {
    return !!(this.rooms && this.rooms.length);
  }

  private animateSelectedRoomPanel(): void {
    const firstRoom = this.getElement('.room-panel')[0] as HTMLElement;
    const selectedRoom = this.getElement('.room-panel')[this.selectedRoomIndex] as HTMLElement;
    if (firstRoom && selectedRoom) {
      this.selectedRoomPosition = `translateX(${firstRoom.offsetLeft - selectedRoom.offsetLeft}px)`;
    }
  }

  private animateRoomSettingsPanel() {
    const firstRoom = <HTMLElement>this.getElement('.room-panel')[0];
    const roomSettings = <HTMLElement>this.getElement('.room-settings-panel')[0];
    const margin = 15;
    if (firstRoom && roomSettings) {
      this.roomSettingsPosition = `translateX(${firstRoom.offsetLeft + firstRoom.offsetWidth + margin - roomSettings.offsetLeft}px)`;
    }
  }

  private resetUpgradePanel(): void {
    this.upgradeRoomPosition = `translateX(0px)`;
    this.roomSettingsPosition = `translateX(0px)`;
  }

  private animateUpgradeRoomPanel(): void {
    const firstRoom = this.getElement('.room-panel')[0] as HTMLElement;
    const upgradeRoom = this.getElement('.upgrade-room-panel')[0] as HTMLElement;
    const offsetLeft = 15;
    this.upgradeRoomPosition = `translateX(${firstRoom.offsetLeft + firstRoom.offsetWidth + offsetLeft - upgradeRoom.offsetLeft}px)`;
  }

  private reflectRoomAssignment(room: Room, isUpgradeSelected?: boolean): void {
    this.rootSubscription.add(of(true).pipe(
      delay(1000),
      tap(() => {
        this.listItem.assignedRoomNumber = room.roomNumber;
        this.listItem.assignedRoomStatus = room;
        this.listItem.disableManualAssign = true;
        this.listItem.freeUpgradeInd = isUpgradeSelected;
        this.roomsService.setRoom(room);
        this.rooms[this.selectedRoomIndex] = room;
        this.rooms[this.selectedRoomIndex].selected = true;
        this.handleManageStayRoomUpdate(this.listItem);
      }),
      delay(100),
      tap(() => {
        this.initBtnBasePosition();
        this.setAssignBtnPosition(0);
      })
    ).subscribe());
  }

  private handleManageStayRoomUpdate({ assignedRoomStatus }: ArrivalsListModel) {
    if (this.stateName === AppStateNames.manageStay) {
      this.roomsService.showSuccessfulRoomChangeMessage(assignedRoomStatus.roomNumber);
      this.emitRoomAssignmentModified.emit(
        assignedRoomStatus
      );
    }
  }

  private handleAssignErrorResponse(definedError?: { room: Room, errorCode: string }, isUpgrade?: boolean): void {
    if (definedError) {
      const error = this.roomsService.getRoomAssignmentDefinedErrors(definedError.room,
        definedError.errorCode);
      this.resetSpecificError(this.roomAssignmentError);
      this.roomAsignmentDefinedError = error.toast;
      this.roomAsignmentDefinedErrorSlnmId = error.slnmId;
      this.initRoomInfo();
    } else {
      this.roomAsignmentDefinedError = null;
    }
    this.trackingService.trackRoomAssignmentError(this.stateName, false,
      definedError && definedError.errorCode, isUpgrade);
  }

  handleRoomPanelSelection(roomPanelIndex: number): void {
    this.resetSelectionState();
    this.shouldAnimate = true;
    this.setAssignBtnPosition(roomPanelIndex);
    this.rooms.forEach((room: Room, index) => {
      room.selected = index === roomPanelIndex;
    });
    this.selectedRoomIndex = roomPanelIndex;
  }

  private getElement(selector: string): NodeList {
    return document.querySelectorAll(`${this.roomAnimationContext} .prepare-arrivals-details-room ${selector}`);
  }

  public displayableRooms(): boolean {
    return (!this.noMatches && this.rooms && this.rooms.length !== 0);
  }

  ngOnDestroy() {
    this.listItem.showManualGrid = false;
    this.listItem.disableManualAssign = false;
    this.rootSubscription.unsubscribe();
  }

  public isGuestCheckedIn(): boolean {
    return this.listItem.prepStatus === ReservationStatus.checkedIn || this.listItem.prepStatus === ReservationStatus.inHouse;
  }

  public onDoNotMoveUpdate(doNotMoveFlag: boolean) {
    this.listItem.doNotMoveRoom = doNotMoveFlag;
    this.roomSettingsChange.emit(doNotMoveFlag);
    if (!doNotMoveFlag) {
      this.listItem.dnmReason = null;
    }
    super.resetSpecificError(this.dnmSwitchError);
    this.resetDnmError();
  }

  public handleReasonAdded(comment: CommentModel): void {
    this.listItem.dnmReason = comment;
    this.resetDnmError();
  }

  resetDnmError() {
    if (this.arrivalsService.ifCheckInPage) {
      this.roomsService.emitDnmAddedCheckInPage();
    } else {
      super.resetSpecificError(this.dnmReasonChangeError);
    }
  }

  public handleReasonAddedError(ifCheckInPage: boolean): void {
    if (!ifCheckInPage) {
      this.addReasonErrorContent = this.roomsService.getDNMAddReasonErrorContent();
      super.processHttpErrors({}, this.dnmReasonChangeError);
    } else {
      this.roomsService.emitDnmErrorCheckInPage();
    }
  }

  public retrySaveReason() {
    if (this.appDoNotMove) {
      this.appDoNotMove.retrySaveReason();
    }
  }

  public handleDNMSwitchError(event: any): void {
    this.addDNMSwitchErrorContent = this.roomsService.getDNMSwitchErrorContent();
    this.reportIssueFillData = event.reportFillData;
    super.processHttpErrors(event.error, this.dnmSwitchError);
  }

  public retryDNMSwitch() {
    this.appDoNotMove.handleTryAgain();
  }

  public updateDnmSwitchInteraction(dnmInteraction: boolean): void {
    this.dnmInteraction = dnmInteraction;
  }

  public getDNMReasonAutoFill() {
    const dnmReasonError = this.reportIssueAutoFill.dnmReasonError;
    if (dnmReasonError) {
      dnmReasonError.subject = dnmReasonError.subject.replace('<Reservation #>', this.listItem.reservationNumber);
    }
    return dnmReasonError;
  }

  private checkForRoomUpgrade() {
    this.isEntitledForUpgrade = this.roomsService.isEntitledForUpgrade(this.listItem.badges);
    this.upgradeRoom = undefined;
  }

  private resetSelectionState(upgrade: boolean = false) {
    if (upgrade && this.hasRoom()) {
      this.rooms.forEach(r => r.selected = false);
    } else {
      if (this.upgradeRoom) {
        this.upgradeRoom.selected = false;
      }
    }
  }

  handleUpgradePanelSelection(room: Room) {
    this.upgradeRoom = room;
    if (this.upgradeRoom) {
      if (this.hasRoom()) {
        this.resetSelectionState(true);
      }
      this.shouldAnimate = true;
      of(true).pipe(
        delay(0),
        tap(() => {
          this.initBtnBasePosition();
          this.setAssignBtnPosition(0, true);
        }),
        take(1)
      ).subscribe();
    }
  }
}
