import { TemplateRef, ViewContainerRef, Renderer2 } from '@angular/core';
import { AccessControlPermission, AccessControlPermissionType } from 'ihg-ng-common-core';
export class PrepareArrivalsDetailsTasksAccessCustomizer {
    constructor(private renderer: Renderer2) { }

    static addTaskAccessCustomizer(templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        accessControlItem: AccessControlPermission,
        alternateRef: TemplateRef<any>,
        data: number) {
            if (accessControlItem.access === AccessControlPermissionType.ReadOnly) {
                viewContainer.createEmbeddedView(alternateRef);
            } else if (accessControlItem.access === AccessControlPermissionType.ReadWrite)  {
                viewContainer.createEmbeddedView(templateRef);
            }
        
    }
}