import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpTestingController } from '@angular/common/http/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateLoader, TranslateModule, TranslateService, TranslateFakeLoader } from '@ngx-translate/core';

import { TruncatedTooltipComponent } from '@app/modules/shared/components/truncated-tooltip/truncated-tooltip.component';
import { BrowserModule, By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '@env/environment';

import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  GoogleAnalyticsService, ToastMessageOutletService, TransitionGuardService, AccessControlModule, AccessControlService, AccessControlPermission  } from 'ihg-ng-common-core';
import { EnrollNewMemberService } from 'ihg-ng-common-pages';

import { not } from '@progress/kendo-angular-grid/dist/es2015/utils';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';

import { of } from 'rxjs';
import { cloneDeep } from 'lodash';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';

import { PrepareArrivalsDetailsTasksComponent } from './prepare-arrivals-details-tasks.component';
import { ArrivalsService } from '@app/modules/prepare/services/arrivals-service/arrivals.service';
import { PrepareArrivalsDetailsTasksService } from '@app/modules/prepare/components/prepare-arrivals-details/services/details-tasks/prepare-arrivals-details-tasks.service';
import { MOCKLISTITEM, MOCKEXPANDED, MOCKANIMATIONDELAY, MOCKSTATENAME } from '@app/modules/prepare/components/prepare-arrivals-details/mocks/prepare-arrival-details-tasks.constant';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';

describe('PrepareArrivalsDetailsTasksComponent', () => {
  let component: PrepareArrivalsDetailsTasksComponent;
  let fixture: ComponentFixture<PrepareArrivalsDetailsTasksComponent>;
  let arrivalService: ArrivalsService;
  let transitionGuardService: TransitionGuardService;
  let tasksService: PrepareArrivalsDetailsTasksService;
  let mockAccessControlService;
  let translateService: TranslateService;

  const translations = {
    LBL_READ_ONLY_PERMISSION: 'You do not have permission to perform this action.'
  };

  beforeEach(async(() => {
    mockAccessControlService = jasmine.createSpyObj(['getAccessControlPermissions']);
    mockAccessControlService.accessControlPermissions = [{'name': 'Add Task', 'tagName': 'add_task', 'type': 'C', 'access': 'W'}];
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        declarations: [
          PrepareArrivalsDetailsTasksComponent,
          TruncatedTooltipComponent,
        ],
        imports: [
          TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: TranslateFakeLoader } }),
          BrowserModule,
          HttpClientModule,
          DatePickerModule,
          CommonTestModule,
          BrowserAnimationsModule,
          AccessControlModule
        ],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          GoogleAnalyticsService,
          ArrivalsService,
          ConfirmationModalService
        ]
      })
      .compileComponents().then(() => {
        setTestEnvironment();
        translateService = TestBed.get(TranslateService);
        translateService.setTranslation('en', translations);
        translateService.use('en');
        fixture = TestBed.createComponent(PrepareArrivalsDetailsTasksComponent);
        component = fixture.componentInstance;
        arrivalService = TestBed.get(ArrivalsService);
        component.listItem = cloneDeep(MOCKLISTITEM);
        component.expanded = cloneDeep(MOCKEXPANDED);
        component.animationDelay = cloneDeep(MOCKANIMATIONDELAY);
        component.stateName = cloneDeep(MOCKSTATENAME);
        component.listItem = { ...listItemMock };
        fixture.detectChanges();
      });
  }));

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  });

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PrepareArrivalsDetailsTasksComponent);
    component = fixture.componentInstance;
    arrivalService = TestBed.get(ArrivalsService);
    component.listItem = cloneDeep(MOCKLISTITEM);
    component.expanded = cloneDeep(MOCKEXPANDED);
    component.animationDelay = cloneDeep(MOCKANIMATIONDELAY);
    component.stateName = cloneDeep(MOCKSTATENAME);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test if it is check in page then registerInTransitionGuard method of arrivalservice is not called', () => {
    arrivalService.ifCheckInPage = false;
    const mySpy = spyOn(arrivalService, 'registerInTransitionGuard');
    fixture.detectChanges();
    expect(arrivalService.registerInTransitionGuard).not.toHaveBeenCalled();
  });

  it('Verify on component destroy roomassignmentListItem is set properly', async(() => {
      component.listItem = cloneDeep(MOCKLISTITEM);      
      fixture.detectChanges();
      fixture.destroy();      
      expect(arrivalService.roomAssignementListItem).toEqual(MOCKLISTITEM);
  }));

  it('Verify authDetailsChangeSubject is setting properly when user is in check in page', () => {
      component.listItem = cloneDeep(MOCKLISTITEM);      
      arrivalService.ifCheckInPage = true;
      fixture.detectChanges();
      //@ts-ignore
      component.setDataChange(true);
      arrivalService.authDetailsChangeSubject.subscribe(val => {
        expect(val).toEqual(true);
      })
  });

  it('Verify authDetailsChangeSubject is not changing when user is in check in page and there is unsaved dnmreason', () => {
    component.listItem = cloneDeep(MOCKLISTITEM);      
    arrivalService.ifCheckInPage = true;
    arrivalService.creditCardUnsavedData = false;
    arrivalService.dnmReasonData = 'Test Reason';
    fixture.detectChanges();
    //@ts-ignore
    component.setDataChange(false);
    arrivalService.authDetailsChangeSubject.subscribe(val => {
      expect(val).toEqual(null);
    })
});

  it('Verify taskChangeSubject is setting properly when user is in prepare tab', () => {
    component.listItem = cloneDeep(MOCKLISTITEM);      
    arrivalService.ifCheckInPage = false;
    fixture.detectChanges();
    //@ts-ignore
    component.setDataChange(true);
    arrivalService.taskChangeSubject.subscribe(val => {
      expect(val).toEqual(true);
    })
  });

  it('Verify taskChangeSubject is setting properly when user is in prepare tab and there is no unsaved dnmReasonData', () => {
    component.listItem = cloneDeep(MOCKLISTITEM);      
    arrivalService.ifCheckInPage = false;
    arrivalService.dnmReasonData = '';
    fixture.detectChanges();
    //@ts-ignore
    component.setDataChange(false);
    arrivalService.taskChangeSubject.subscribe(val => {
      expect(val).toEqual(false);
    });
  });

  it('Verify taskChangeSubject is not changing when user is in prepare tab and there is unsaved dnmReasonData', () => {
    arrivalService.taskChangeSubject.next(true);
    component.listItem = cloneDeep(MOCKLISTITEM);      
    arrivalService.ifCheckInPage = false;
    arrivalService.dnmReasonData = 'Test Reason';
    fixture.detectChanges();
    //@ts-ignore
    component.setDataChange(false);
    arrivalService.taskChangeSubject.subscribe(val => {
      expect(val).toEqual(true);
    });
  });

  /*** TC205134 (F16494/US102935) ***/
  it('add new task button should be readonly for a user having readonly access for addnewtask button', () => {
    recreate([{ name: 'Add Task', tagName: 'add_task', type: 'C', access: 'R' }]);
    const addTaskButton = fixture.debugElement.query(By.css('.btn-add-tsk'));
    expect(addTaskButton.nativeElement.disabled).toBeTruthy();
  });

  /*** TC205136 (F16494/US102935) ***/
  it('should show tooltip on hover add new task button for user having readonly permission ', () => {
    recreate([{ name: 'Add Task', tagName: 'add_task', type: 'C', access: 'R' }]);
    const addTaskButton = fixture.debugElement.query(By.css('.add-task-div'));
    addTaskButton.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();
    const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
    expect(tooltip).not.toBeNull();
    expect(tooltip.nativeElement.innerText).toEqual(translations.LBL_READ_ONLY_PERMISSION);
  });

  /*** TC205599 (F16494/US102935) ***/
  it('add new task button should be Enabled for a user having Read and Write access for addnewtask button', () => {
    recreate([{ name: 'Add Task', tagName: 'add_task', type: 'C', access: 'W' }]);
    const addTaskButton = fixture.debugElement.query(By.css('.btn-add-tsk'));
    expect(addTaskButton.nativeElement.disabled).toBeFalsy();
  });



  function recreate(accessControlPermissions: AccessControlPermission[]) {
    fixture.destroy();
    fixture = TestBed.createComponent(PrepareArrivalsDetailsTasksComponent);
    component = fixture.componentInstance;
    mockAccessControlService = TestBed.get(AccessControlService);
    mockAccessControlService.accessControlPermissions = accessControlPermissions;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
    arrivalService = TestBed.get(ArrivalsService);
    component.listItem = cloneDeep(MOCKLISTITEM);
    component.expanded = cloneDeep(MOCKEXPANDED);
    component.animationDelay = cloneDeep(MOCKANIMATIONDELAY);
    component.stateName = cloneDeep(MOCKSTATENAME);
    component.listItem = { ...listItemMock };
    component.initialized = true;
    component.expanded = true;
    fixture.detectChanges();
  }

});
