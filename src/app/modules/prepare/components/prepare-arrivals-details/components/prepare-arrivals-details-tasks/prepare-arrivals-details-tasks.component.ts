import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { cloneDeep } from 'lodash';
import { Observable, Subject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { TransitionGuardService, GuardRegistrationParams } from 'ihg-ng-common-core';

import { ArrivalsListModel, TasksModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ADD_TASKS_ERR, defaultTask, LOAD_TASKS_ERR } from './../../constants/arrivals-details-constants';
import { DepartmentsListModel, DepartmentsModel, TasksResponseModel } from './../../models/arrivals-tasks.model';
import { PrepareArrivalsDetailsTasksService } from './../../services/details-tasks/prepare-arrivals-details-tasks.service';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { slideUpDown } from '@app/animations/slide-up-down.animation';
import { AnalyticsTrackingService } from '@app/modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ErrorToastMessageTranslations } from '@app-shared/components/error-toast-message/error-toast-message.component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import {ReportIssueAutoFillData} from 'ihg-ng-common-pages';
import {TASKS_CHECK_IN_ADD_AUTOFILL, TASKS_CHECK_IN_FETCH_AUTOFILL} from '@app/constants/error-autofill-constants';
import {PrepareArrivalsDetailsTasksAccessCustomizer} from './prepare-arrivals-details-tasks-access.customizer'

@Component({
  selector: 'app-prepare-arrivals-details-tasks',
  templateUrl: './prepare-arrivals-details-tasks.component.html',
  styleUrls: ['./prepare-arrivals-details-tasks.component.scss'],
  animations: [slideUpDown]
})
export class PrepareArrivalsDetailsTasksComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  private detailsTasks$: Subscription = new Subscription();
  private lastTraceSent: TasksModel;
  public maxTasksCount = 25;
  public departmentsList: DepartmentsModel[];
  public initialized = false;
  public addTaskErrorContent: ErrorToastMessageTranslations = ADD_TASKS_ERR;
  public loadTaskErrorContent: ErrorToastMessageTranslations = LOAD_TASKS_ERR;
  public addTaskError = new Subject<EmitErrorModel>();
  public loadTaskError = new Subject<EmitErrorModel>();
  public fetchAutofill: ReportIssueAutoFillData = TASKS_CHECK_IN_FETCH_AUTOFILL;
  public addAutofill: ReportIssueAutoFillData = TASKS_CHECK_IN_ADD_AUTOFILL;
  public addTaskAccessCustomizer = PrepareArrivalsDetailsTasksAccessCustomizer['addTaskAccessCustomizer'];

  @Input() listItem: ArrivalsListModel;
  @Input() expanded: boolean;
  @Input() animationDelay: string;
  @Input() stateName: string;
  @Input() labels: { [key: string]: string };
  @Output() emitDoneEvent: EventEmitter<void> = new EventEmitter();

  constructor(
    private tasksService: PrepareArrivalsDetailsTasksService,
    private arrivalService: ArrivalsService,
    private translate: TranslateService,
    private transitionGuardService: TransitionGuardService,
    private trackingService: AnalyticsTrackingService) {
    super();
  }

  ngOnInit(): void {
    this.initializeTasksList();
    this.subscribeTaskChange();
    if(!this.arrivalService.ifCheckInPage) {
      this.registerInTransitionGuard();
    } else {
      if(this.arrivalService.getUnsavedTask()) {
        this.getDepartmentsList();
      }
    }
  }

  private initializeTasksList(): void {
    if (!this.listItem.tasks || !this.listItem.tasks.length) {
      this.getTasksList();
    } else {
      this.initialized = true;
    }
  }

  public getTasksList(): void {
    this.detailsTasks$.add(this.tasksService.fetchTraces(this.listItem).subscribe((response: TasksResponseModel) => {
      this.handleFetchTasksResponse(response);
    }, error => {
      this.handleFetchTasksError();
      super.processHttpErrors(error, this.loadTaskError);
    }));
  }

  private handleFetchTasksResponse(response: TasksResponseModel): void {
    if (!super.checkIfAnyApiErrors(response, null, this.loadTaskError)) {
      this.listItem.tasks = response.tasks || [];
      this.arrivalService.refreshTasksStatus.next();
      this.initialized = true;
    } else {
      this.handleFetchTasksError();
    }
  }

  private handleFetchTasksError(): void {
    this.initialized = false;
    this.trackingService.trackTasksLoadError(this.stateName);
  }

  private subscribeTaskChange(): void {
    this.detailsTasks$.add(this.arrivalService.taskChangeSubject.subscribe((changed: boolean) => {
      if (!changed && !this.arrivalService.ifCheckInPage) {
        super.resetSpecificError(this.addTaskError);
      }
    }));
  }

  private registerInTransitionGuard() {
    this.transitionGuardService.register(<GuardRegistrationParams>{
      dataChangesObserver: this.arrivalService.taskChangeSubject.asObservable(),
      useCancelSaveDiscardModal: false,
      userConfirmModalConfig: this.arrivalService.getConfirmModalConfig(),
      noToastMessage: true,
      onSave: null,
      onDiscard: () => {
        return this.handleConfirmActions(true);
      },
      onCancel: () => {
        return this.handleConfirmActions(false);
      }
    });
  }

  private handleConfirmActions(shouldDiscard: boolean): Observable<boolean> {
    const subject = new Subject<boolean>();
    setTimeout(() => {
      if (shouldDiscard) {
        this.arrivalService.discardUnsentTasks(this.listItem);
        this.arrivalService.setUnsavedTask(false);
        if(this.arrivalService.dnmReasonData) {
          this.arrivalService.clearUnsavedDnsReason();
        }
      }
      subject.next(shouldDiscard);
      this.setDataChange(!shouldDiscard);
    });
    return subject.asObservable();
  }

  private setDataChange(dataChanged: boolean): void {
    if(this.arrivalService.ifCheckInPage) {
      if(dataChanged || (!dataChanged && !this.arrivalService.creditCardUnsavedData && !this.arrivalService.dnmReasonData)) {
        this.arrivalService.authDetailsChangeSubject.next(dataChanged);
      }
    } else {
      if(dataChanged || (!dataChanged && !this.arrivalService.dnmReasonData)) {
        this.arrivalService.taskChangeSubject.next(dataChanged);
      }
    }
  }

  public addTask(): void {
    this.trackingService.trackClickAddTask(this.stateName);
    this.addDefaultTaskItem();
    this.getDepartmentsList();
  }

  private addDefaultTaskItem(): void {
    this.listItem.tasks.push(cloneDeep(defaultTask));
  }

  private getDepartmentsList(): void {
    if (!this.departmentsList) {
      this.detailsTasks$.add(this.tasksService.getDepartmentsList(true).subscribe((response: DepartmentsListModel) => {
        this.departmentsList = response.departments;
      }));
    }
  }

  public isTraceDisabled(dataItem: TasksModel): boolean {
    return !dataItem.traceText || this.isDepartmentUndefined(dataItem);
  }

  private isDepartmentUndefined(dataItem: TasksModel): boolean {
    return !dataItem.departmentCode || (dataItem.departmentCode.indexOf(this.translate.instant('LBL_SELECT')) > -1);
  }

  public handleTraceChange(): void {
    this.arrivalService.setUnsavedTask(this.hasUnsentTasks());
    this.setDataChange(this.hasUnsentTasks());
  }

  private hasUnsentTasks(): boolean {
    return this.listItem.tasks.some(item => item.isResolved === null && (!!item.traceText || !this.isDepartmentUndefined(item)));
  }

  public onDepartmentChange(departmentCode): void {
    this.trackingService.trackSelectTaskDepartment(this.stateName, departmentCode);
    this.handleTraceChange();
  }

  public sendTrace(dataItem: TasksModel): void {
    this.trackingService.trackClickSendTrace(this.stateName);
    this.lastTraceSent = dataItem;
    this.detailsTasks$.add(this.tasksService.addTrace(this.listItem, dataItem).subscribe((response: TasksResponseModel) => {
      if (!super.checkIfAnyApiErrors(response, null, this.addTaskError)) {
        this.handleTraceResponse(dataItem);
      } else {
        this.handleSendTraceError();
      }
    }, error => {
      this.handleSendTraceError();
      super.processHttpErrors(error, this.addTaskError);
    }));
  }

  private handleTraceResponse(dataItem: TasksModel) {
    dataItem.isResolved = false;
    this.handleTraceChange();
    this.arrivalService.refreshTasksStatus.next();
  }

  private handleSendTraceError(): void {
    this.trackingService.trackSendTaskError(this.stateName);
  }

  public deleteTrace(rowIndex: number): void {
    this.trackingService.trackClickDeleteTrace(this.stateName);
    this.listItem.tasks.splice(rowIndex, 1);
    this.handleTraceChange();
  }

  public handleDone(): void {
    this.trackingService.trackClickTasksDone(this.stateName);
    this.emitDoneEvent.emit();
  }

  public retrySendTrace(): void {
    this.sendTrace(this.lastTraceSent);
  }

  ngOnDestroy(): void {
    this.arrivalService.roomAssignementListItem = this.listItem;
    this.detailsTasks$.unsubscribe();
  }
}
