import { async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule} from '@ngx-translate/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { cloneDeep } from 'lodash';

import { UserService, GoogleAnalyticsService, SaveDiscardMessageService, DetachedToastMessageService, AccessControlModule, AccessControlService, AccessControlPermission } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import { ReportIssueService, ReportIssueComponent } from 'ihg-ng-common-pages';

import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { PrepareArrivalsDetailsComponent } from './prepare-arrivals-details.component';
import {
  PrepareArrivalsDetailsTasksService
} from '@modules/prepare/components/prepare-arrivals-details/services/details-tasks/prepare-arrivals-details-tasks.service';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ScrollUtilService } from '@app-shared/services/scroll-util/scroll-util.service';
import {
  AnalyticsTrackingService
} from '@modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { MOCK_PREP_ARRV_RESRV_DATA } from '../mocks/prepare-arrival-details-mock';
import { MOCK_RELEASED_ROOM } from '@app/modules/prepare/mocks/prepare-arrivals-mock';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { RoomFeatureModel } from '@modules/check-in/models/check-in.models';


describe('PrepareArrivalsDetailsComponent', () => {
  let component: PrepareArrivalsDetailsComponent;
  let fixture: ComponentFixture<PrepareArrivalsDetailsComponent>;
  let arrivalsService: ArrivalsService;
  let mockReservationData;
  let mockAccessControlService;


  function setUpMockForPmsCheck(pmsNumber: string) {
    mockReservationData.reservationData.pmsUniqueNumber = pmsNumber;
    const reservData = mockReservationData;
    component.allowManage = true;
    component.listItem = reservData.reservationData;
    return reservData;
  }

  beforeEach(async(() => {
    mockAccessControlService = jasmine.createSpyObj(['getAccessControlPermissions']);
    mockAccessControlService.accessControlPermissions = [{'name': 'Add Task', 'tagName': 'add_task', 'type': 'C', 'access': 'W'}];

    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        imports: [RouterTestingModule.withRoutes([]),
        TranslateModule.forRoot(),
        HttpClientTestingModule, CommonTestModule, BrowserAnimationsModule],
        declarations: [PrepareArrivalsDetailsComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          PrepareArrivalsDetailsTasksService,
          ArrivalsService,
          RoomsService,
          ScrollUtilService,
          AnalyticsTrackingService,
          UserService,
          ConfirmationModalService,
          GoogleAnalyticsService,
          ReportIssueService,
          SaveDiscardMessageService,
          DetachedToastMessageService
        ],
      })
      .compileComponents().then(() => {
      setTestEnvironment();
      });
  }));

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareArrivalsDetailsComponent);
    component = fixture.componentInstance;
    mockReservationData = cloneDeep(MOCK_PREP_ARRV_RESRV_DATA);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize as task panel as collapsed and room panel as expanded', () => {
    const spy = spyOn(component, 'setInitSelection').and.callThrough();
    expect(component.isAssignRoomExpanded).toBeFalsy();
    expect(component.isTasksExpanded).toBeFalsy();
    component.allowManage = true;
    component.setInitSelection();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(component.isAssignRoomExpanded).toBeTruthy();
    expect(component.isTasksExpanded).toBeFalsy();
  });

  it('should keep room panel as expanded after change room (unassign room)', () => {
    const spy = spyOn(component, 'handleReleaseRoom').and.callThrough();
    component.isAssignRoomExpanded = true;
    component.handleReleaseRoom(MOCK_RELEASED_ROOM);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(component.isAssignRoomExpanded).toBeTruthy();
  });

  it('should keep room panel as expanded after room assignment', () => {
    const spy = spyOn(component, 'handleRoomAssignment').and.callThrough();
    component.isAssignRoomExpanded = true;
    component.handleRoomAssignment();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
    expect(component.isAssignRoomExpanded).toBeTruthy();
  });

  it(`TC201758: Verify if Room Assignment button is disabled and Room Assignment Panel is not Expanded when PMS Conf No is not available`,
    async(inject([HttpTestingController, ArrivalsService, AnalyticsTrackingService],
      (httpClient: HttpTestingController, apiService: ArrivalsService, analyticsService: AnalyticsTrackingService) => {
        const reservData = setUpMockForPmsCheck('');
        spyOn(apiService, 'getPrepareReservationData').and.returnValue(of(reservData));
        // @ts-ignore
        component.subscribeUpdateReservation();
        fixture.detectChanges();
        expect(component.isPmsUniqueNumber).toBeTruthy();
        expect(component.isAssignRoomExpanded).toBeFalsy();
      })));

  it(`TC201717: Verify Google analytics when msg is displayed if OWS call FAILED to retrieve the PMS confirmation #`,
    async(inject([HttpTestingController, ArrivalsService, AnalyticsTrackingService],
      (httpClient: HttpTestingController, apiService: ArrivalsService, analyticsService: AnalyticsTrackingService) => {
        const reservData = setUpMockForPmsCheck('');
        spyOn(apiService, 'getPrepareReservationData').and.returnValue(of(reservData));
        spyOn(analyticsService, 'trackPmsConfNumUnavailable');
        // @ts-ignore
        component.subscribeUpdateReservation();
        fixture.detectChanges();
        expect(analyticsService.trackPmsConfNumUnavailable).toHaveBeenCalled();
      })));

  it(`TC201759: Verify if Room Assignment button is enabled and Room Assignment Panel is Expanded when PMS Conf No is available`,
    async(inject([HttpTestingController, ArrivalsService, AnalyticsTrackingService],
      (httpClient: HttpTestingController, apiService: ArrivalsService, analyticsService: AnalyticsTrackingService) => {
        const reservData = setUpMockForPmsCheck('180536');
        spyOn(apiService, 'getPrepareReservationData').and.returnValue(of(reservData));
        // @ts-ignore
        component.subscribeUpdateReservation();
        fixture.detectChanges();
        expect(component.isAssignRoomExpanded).toBeTruthy();
      })));

  it(`TC201728: Verify Google analytics when msg is NOT displayed if OWS call is Successful to retrieve the PMS confirmation #`,
    async(inject([HttpTestingController, ArrivalsService, AnalyticsTrackingService],
      (httpClient: HttpTestingController, apiService: ArrivalsService, analyticsService: AnalyticsTrackingService) => {
        const reservData = setUpMockForPmsCheck('180536');
        spyOn(apiService, 'getPrepareReservationData').and.returnValue(of(reservData));
        const spyMethod = spyOn(analyticsService, 'trackExpandRowPmsAvlb');
        // @ts-ignore
        component.subscribeUpdateReservation();
        fixture.detectChanges();
        expect(spyMethod).toHaveBeenCalled();
      })));

  it('TC201760: Should check if Refresh button label is "LBL_ERR_REFRESH" after "REFRESH" button is clicked once', () => {
    // @ts-ignore
    component.pmsUnavailableCounter = 0;
    // @ts-ignore
    component.handleRefresh();
    fixture.detectChanges();
    expect(component.pmsButtonLabel).toEqual('LBL_ERR_REFRESH');
  });

  it('TC201761: Should check if "REFRESH" button Label is "REPORT_AN_ISSUE_MENU" when "REFRESH" button is clicked twice', () => {
    // @ts-ignore
    component.pmsUnavailableCounter = 1;
    // @ts-ignore
    component.handleRefresh();
    fixture.detectChanges();
    expect(component.pmsButtonLabel).toEqual('REPORT_AN_ISSUE_MENU');
  });

  it(`TC201764: Should check if Report an issue modal is opened when "REFRESH" button is clicked thrice`,
    async(inject([HttpTestingController, ReportIssueService],
      (httpClient: HttpTestingController, apiService: ReportIssueService) => {
        // @ts-ignore
        component.pmsUnavailableCounter = 2;
        spyOn(apiService, 'openAutofilledReportIssueModal');
        // @ts-ignore
        component.handleRefresh();
        fixture.detectChanges();
        expect(apiService.openAutofilledReportIssueModal).toHaveBeenCalled();
      })));

  it(`TC201729: Verify SNOW ticket is generated when msg is displayed if OWS call is Failed to retrieve the PMS confirmation #`,
    async(inject([HttpTestingController, ReportIssueService],
      (httpClient: HttpTestingController, apiService: ReportIssueService) => {
        // @ts-ignore
        component.pmsUnavailableCounter = 2;
        spyOn(apiService, 'openAutofilledReportIssueModal');
        // @ts-ignore
        component.handleRefresh();
        fixture.detectChanges();
        expect(apiService.openAutofilledReportIssueModal).toHaveBeenCalledWith(ReportIssueComponent, component.pmsUnavlReportIssueAutoFill);
      })));

  it('verify "onRoomCaretClick" method when user is in check-in page', () => {
    const arrvServ = TestBed.get(ArrivalsService);
    arrvServ.ifCheckInPage = true;
    component.isAssignRoomExpanded = true;
    component.onRoomCaretClick();
    fixture.detectChanges();
    expect(component.isAssignRoomExpanded).toEqual(false);
  });

  it('verify "onRoomCaretClick" method when user is in prepare tab and room assignment panel is expanded and DNM Reason Unsaved', () => {
      const arrvServ = TestBed.get(ArrivalsService);
      const handleUnsavedTasksSpy = spyOn(arrvServ, 'handleUnsavedTasks');
      arrvServ.ifCheckInPage = false;
      arrvServ.dnmReasonData = 'Test Reason';
      component.isAssignRoomExpanded = true;
      component.onRoomCaretClick();
      fixture.detectChanges();
      expect(handleUnsavedTasksSpy).toHaveBeenCalled();
  });

  it('verify "onRoomCaretClick" method when user is in prepare tab and room assignment panel is expanded and DNM Reason saved', () => {
    const arrvServ = TestBed.get(ArrivalsService);
    arrvServ.ifCheckInPage = false;
    component.isAssignRoomExpanded = true;
    arrvServ.dnmReasonData = '';
    component.onRoomCaretClick();
    fixture.detectChanges();
    expect(component.isAssignRoomExpanded).toEqual(false);
});

/*** TC205265 (F16494/US102936) ***/
it('task accordion should be disabled for a user having no access for addnewtask button', () => {
  recreate([{ name: 'Add Task', tagName: 'add_task', type: 'C', access: 'X' }]);
  const addTaskButton = fixture.debugElement.query(By.css('.preapre-guest-task-btn'));
  expect(addTaskButton.nativeElement.disabled).toBeTruthy();
});

/*** TC205267 (F16494/US102936) ***/
it('should show tooltip on hover task accordion for user having readonly permission ', () => {
  recreate([{ name: 'Add Task', tagName: 'add_task', type: 'C', access: 'X' }]);
  const addTaskButton = fixture.debugElement.query(By.css('.task-div'));
  addTaskButton.triggerEventHandler('mouseenter', null);
  fixture.detectChanges();
  const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
  expect(tooltip).not.toBeNull();
});

  it('should show or hide upgrade icon based on the free room upgrade indicator', () => {
    component.expanded = true;
    component.listItem = { ... listItemMock };
    component.listItem.assignedRoomNumber = '104';
    component.listItem.assignedRoomStatus = {
      roomNumber: '104', roomStatus: 'Clean', roomType: 'KDXG', houseKeepingStatus: 'Clean', frontOfficeStatus: 'Vacant',
      features: [], reservationFeatures: []
    };
    component.listItem.freeUpgradeInd = true;
    fixture.detectChanges();
    let upgradeIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="UpgradeIcon-SID"]'));
    expect(upgradeIcon).not.toBeNull();

    component.listItem.freeUpgradeInd = false;
    fixture.detectChanges();
    upgradeIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="UpgradeIcon-SID"]'));
    expect(upgradeIcon).toBeNull();
  });

  it('should display tooltip on the free room upgrade icon', () => {
    component.expanded = true;
    component.listItem = { ... listItemMock };
    component.listItem.assignedRoomNumber = '104';
    component.listItem.assignedRoomStatus = {
      roomNumber: '104', roomStatus: 'Clean', roomType: 'KDXG', houseKeepingStatus: 'Clean', frontOfficeStatus: 'Vacant',
      features: [], reservationFeatures: []
    };
    component.listItem.freeUpgradeInd = true;
    fixture.detectChanges();
    const upgradeIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="UpgradeIcon-SID"]'));
    expect(upgradeIcon).not.toBeNull();

    upgradeIcon.triggerEventHandler('mouseenter', null);

    fixture.detectChanges();
    const tooltipElement = document.querySelector('ngb-tooltip-window');
    expect(tooltipElement).toBeDefined();
    const tooltipPopover = fixture.debugElement.query(By.css('[data-slnm-ihg="UpgradeTooltip-SID"]')).nativeElement;
    expect(tooltipPopover).not.toBeNull();
  });

  it('should not display tooltip when room upgrade icon is not seen', () => {
    component.expanded = true;
    component.listItem = { ... listItemMock };
    component.listItem.assignedRoomNumber = '104';
    component.listItem.assignedRoomStatus = {
      roomNumber: '104', roomStatus: 'Clean', roomType: 'KDXG', houseKeepingStatus: 'Clean', frontOfficeStatus: 'Vacant',
      features: [], reservationFeatures: []
    };
    component.listItem.freeUpgradeInd = false;
    fixture.detectChanges();
    const upgradeIcon = fixture.debugElement.query(By.css('[data-slnm-ihg="UpgradeIcon-SID"]'));
    expect(upgradeIcon).toBeNull();

    fixture.detectChanges();
    const tooltipElement = document.querySelector('ngb-tooltip-window');
    expect(tooltipElement).toBeDefined();
    expect(tooltipElement).toBeNull();
  });

function recreate(accessControlPermissions: AccessControlPermission[]) {
  fixture.destroy();
  fixture = TestBed.createComponent(PrepareArrivalsDetailsComponent);
  component = fixture.componentInstance;
  component.expanded = true;
  component.listItem = { ...listItemMock };
  mockReservationData = cloneDeep(MOCK_PREP_ARRV_RESRV_DATA);
  mockAccessControlService = TestBed.get(AccessControlService);
  mockAccessControlService.accessControlPermissions = accessControlPermissions;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  fixture.detectChanges();

}
});
