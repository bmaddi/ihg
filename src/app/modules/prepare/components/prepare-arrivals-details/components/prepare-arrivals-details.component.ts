import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { isEqual } from 'lodash';

import { Alert, AlertType, AlertButtonType } from 'ihg-ng-common-core';

import {ArrivalsListModel, PrepareReservationData, RoomChangeModel} from '../../../models/arrivals-checklist.model';
import { PrepareArrivalsDetailsTasksService } from './../services/details-tasks/prepare-arrivals-details-tasks.service';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { slideUpDownNgIf } from '@app/animations/slide-up-down.animation';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ScrollUtilService } from '@modules/shared/services/scroll-util/scroll-util.service';
import { ActivatedRoute } from '@angular/router';
import { AnalyticsTrackingService } from '@app/modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { ReportIssueService, ReportIssueComponent, ReportIssueAutoFillData } from 'ihg-ng-common-pages';
import { PMS_CONF_UNAVL_AUTOFILL } from '@app/constants/error-autofill-constants';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';
import {PrepareArrivalsDetailsAccessCustomizer} from './prepare-arrivals-details-access-customizer'


@Component({
  selector: 'app-prepare-arrivals-details',
  templateUrl: './prepare-arrivals-details.component.html',
  styleUrls: ['./prepare-arrivals-details.component.scss'],
  animations: [slideUpDownNgIf]
})
export class PrepareArrivalsDetailsComponent implements OnInit, OnChanges, OnDestroy {
  public stateName: string;
  private readonly states = {
    standard: 'standard',
    roomAssigned: 'roomAssigned',
    viewTask: 'viewTask'
  };
  private readonly animationDelay = {
    [this.states.standard]: {
      room: '10ms',
      task: '10ms'
    },
    [this.states.roomAssigned]: {
      room: '2000ms',
      task: '3000ms'
    },
    [this.states.viewTask]: {
      room: '10ms',
      task: '900ms'
    }
  };
  private roomReadyActionCode = 'room-ready';
  private details$: Subscription = new Subscription();
  public isAssignRoomExpanded = false;
  public isTasksExpanded = false;
  public featureUnavailAlert: Alert;
  public pmsConfNumUnavailAlert: Alert;
  public roomAnimDelay: string;
  public taskAnimDelay: string;
  public initialized = false;

  public isPmsUniqueNumber = false;
  private pmsUnavailableCounter = 0;
  public pmsButtonLabel = 'LBL_ERR_REFRESH';
  public pmsUnavlReportIssueAutoFill = PMS_CONF_UNAVL_AUTOFILL;
  public addTaskAccessCustomizer = PrepareArrivalsDetailsAccessCustomizer['taskAccessCustomizer'];
  public viewTaskTriggered = false;

  @Input() allowManage = false;
  @Input() listItem: ArrivalsListModel;
  @Input() expanded: boolean;
  @Input() rowIndex: number;
  @Input() showArrivalInfo = true;
  @Input() suggestedRoomCount = 3;
  @Input() roomAnimationContext = '.k-detail-row';
  @Input() showNotifiedAlert = true;
  @Input() showRoomNotReadyAlert = false;
  @Input() showPrintRegCard = false;
  @Input() isTodaySelected: boolean;
  @Input() updateReservation = false;
  @Output() emitTaskDoneEvent: EventEmitter<null> = new EventEmitter();
  @Output() emitRoomChange = new EventEmitter<RoomChangeModel>();

  constructor(
    private tasksService: PrepareArrivalsDetailsTasksService,
    private arrivalsService: ArrivalsService,
    private roomsService: RoomsService,
    private scrollUtil: ScrollUtilService,
    private activatedRoute: ActivatedRoute,
    private trackingService: AnalyticsTrackingService,
    private reportIssueService: ReportIssueService,
    private appCommonService: AppCommonService
  ) {
  }

  ngOnInit() {
    this.getDepartmentsList();
    this.setAnimDelay(this.states.standard);
    this.handleUpdateReservation();
    this.handleFeatureUnvail();
    this.subscribeToRoomSet();
    this.subscribeToViewTaskAction();
    this.setStateName();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.expanded && changes.expanded.currentValue && !changes.expanded.isFirstChange()) {
      this.handleUpdateReservation();
    }
  }

  private handleUpdateReservation(): void {
    if (this.updateReservation && this.expanded) {
      this.subscribeUpdateReservation();
    } else {
      this.handleInitSelection();
    }
  }

  private subscribeUpdateReservation(): void {
    this.appCommonService.setReservationNumber(this.listItem.reservationNumber);
    this.details$.add(this.arrivalsService.getPrepareReservationData(this.listItem.reservationNumber)
      .pipe(finalize(() => {
        this.handleInitSelection();
      }))
      .subscribe((item: PrepareReservationData) => {
        if (item && !(item.errors && item.errors.length) && item.reservationData) {
          this.handleReservationResponse(item);
        }
      }));
  }

  private handleInitSelection(): void {
    this.setInitSelection();
    this.initialized = true;
  }

  private handleReservationResponse(item: PrepareReservationData): void {
    const previousAssignedRoomStatus = this.listItem.assignedRoomStatus;
    this.listItem.assignedRoomNumber = item.reservationData.assignedRoomNumber;
    this.listItem.assignedRoomStatus = item.reservationData.assignedRoomStatus;
    this.listItem.pmsReservationNumber = item.reservationData.pmsReservationNumber;
    this.listItem.pmsUniqueNumber = item.reservationData.pmsUniqueNumber;
    this.listItem.prepStatus = item.reservationData.prepStatus;
    this.listItem.pmsReservationStatus = item.reservationData.pmsReservationStatus;
    this.listItem.vipCode = item.reservationData.vipCode;
    this.listItem.roomTypeCode = item.reservationData.roomTypeCode;
    this.isPmsUniqueNumber = !(item.reservationData.pmsUniqueNumber);
    this.setTimestampNotify(item);
    this.setRoomUpdate(previousAssignedRoomStatus);
  }

  private setTimestampNotify(item: PrepareReservationData): void {
    if (item.reservationData.infoItems && item.reservationData.infoItems.length) {
      const notifyGuestItem = item.reservationData.infoItems.find(data => data.type === this.roomReadyActionCode && data.indicator === 'Y');
      this.listItem.notified = notifyGuestItem ? notifyGuestItem.createTimestamp : '';
    }
  }

  private setRoomUpdate(previousAssignedRoomStatus: Room): void {
    if (!isEqual(previousAssignedRoomStatus, this.listItem.assignedRoomStatus)) {
      this.roomsService.setRoom(this.listItem.assignedRoomStatus);
    }
  }

  private setStateName() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.stateName = data.stateName;
    });
  }

  private subscribeToRoomSet(): void {
    this.details$.add(this.roomsService.getRoom().subscribe((room: Room) => {
      if (room) {
        this.handleRoomAssignment();
      }
    }));
  }

  private subscribeToViewTaskAction(): void {
    this.details$.add(this.tasksService.getViewTaskAction().subscribe(() => {
      this.viewTaskTriggered = true;
      this.setAnimDelay(this.states.viewTask);
      this.isAssignRoomExpanded = false;
      this.isTasksExpanded = true;
      if(this.initialized) {
        this.viewTaskTriggered = false;
      }
    }));
  }

  private getDepartmentsList(): void {
    this.details$.add(this.tasksService.getDepartmentsList().subscribe());
  }

  setInitSelection(): void {
    if (this.allowManage && !(this.isPmsUniqueNumber)) {
      this.pmsUnavailableCounter = 0;
      if(!this.viewTaskTriggered) {
        this.isTasksExpanded = false;
        this.isAssignRoomExpanded = true;
      }
      this.trackingService.trackExpandRowPmsAvlb(this.stateName);
      this.viewTaskTriggered = false;
    } else if(this.isPmsUniqueNumber) {
      this.setPmsConfirmationUnavailableAlert();
      this.trackingService.trackPmsConfNumUnavailable(this.stateName);
    }
  }

  handleFeatureUnvail(): void {
    if (!this.allowManage) {
      this.setFeatureUnavailAlert();
    }
  }

  private setPmsConfirmationUnavailableAlert(): void {
    this.pmsConfNumUnavailAlert = {
      type: AlertType.Info,
      message: 'LBL_NOTE_PMS_UNAVAILABLE',
      detailMessage: 'LBL_PMS_CONF_UNAVL',
      buttons: [{
        label: this.pmsButtonLabel,
        type: AlertButtonType.Info,
        onClick: () => {
          this.handleRefresh();
        }
      }],
      dismissible: false,
      animation: false
    };
  }

  private handleRefresh(): void {
    this.pmsUnavailableCounter++;
    if(this.pmsUnavailableCounter < 3) {
      this.handleUpdateReservation();
      if(this.pmsUnavailableCounter == 2) {
        this.pmsButtonLabel = 'REPORT_AN_ISSUE_MENU';
      }
    } else {
      this.reportIssueService.openAutofilledReportIssueModal(ReportIssueComponent, this.pmsUnavlReportIssueAutoFill);
    }
  }

  setFeatureUnavailAlert(): void {
    this.featureUnavailAlert = {
      type: AlertType.Info,
      message: '',
      detailMessage: 'MSG_OWS_FTR_ONLY',
      dismissible: false,
      animation: false
    };
  }

  onTaskCaretClick() {
    this.trackingService.trackClickToggleTasks(this.stateName);
    if (this.isTasksExpanded) {
      const callbackFn = this.handleTaskExpandCollapse.bind(this);
      this.arrivalsService.handleUnsavedTasks(callbackFn, null, this.listItem);
    } else {
      this.handleTaskExpandCollapse();
    }
  }

  onRoomCaretClick() {
    this.setAnimDelay(this.states.standard);
    if(this.arrivalsService.ifCheckInPage) {
      this.handleRoomExpandCollapse();
    } else {
      this.handleRoomExpandCollapsePrepTab();
    }
  }

  private handleRoomExpandCollapsePrepTab() {
    if(this.isAssignRoomExpanded && this.arrivalsService.dnmReasonData) {
      const callbackFn = this.handleRoomExpandCollapse.bind(this);
      this.arrivalsService.handleUnsavedTasks(callbackFn, null, this.listItem);
    } else {
      this.handleRoomExpandCollapse();
    }
  }

  private handleRoomExpandCollapse() {
    this.isAssignRoomExpanded = !this.isAssignRoomExpanded;
  }

  private handleTaskExpandCollapse(): void {
    this.setAnimDelay(this.states.standard);
    this.isTasksExpanded = !this.isTasksExpanded;
  }

  getTasksCount(): number {
    if (this.listItem.tasks && this.listItem.tasks.length) {
      return this.listItem.tasks.filter(item => item.isResolved !== null).length;
    }
    return 0;
  }

  handleRoomAssignment(): void {
    if (this.rowIndex !== undefined && this.initialized) {
      this.scrollToRow();
    }
    this.setAnimDelay(this.states.roomAssigned);
  }

  public handleReleaseRoom(room: RoomChangeModel): void {
    this.emitRoomChange.emit(room);

    if (!!room && room.newRoomNumber === '') {
      this.isAssignRoomExpanded = true;
    }
  }

  private scrollToRow(): void {
    const headerHeight = (document.querySelector('uic-header .navbar.header') as HTMLElement).offsetHeight;
    const gridHeaderHeight = (document.querySelector('.k-grid-header') as HTMLElement).offsetHeight;
    const pagerHeight = (document.querySelector('kendo-pager.k-grid-pager') as HTMLElement).offsetHeight;
    const selectedRowOffset = this.scrollUtil.getOffset(document.querySelectorAll('tr.k-master-row')[this.rowIndex] as HTMLElement).top;
    const scrollTop = selectedRowOffset - (headerHeight + gridHeaderHeight + pagerHeight);

    this.scrollUtil.scrollWindow(0, scrollTop);
  }

  private setAnimDelay(state: string): void {
    this.roomAnimDelay = this.animationDelay[state].room;
    this.taskAnimDelay = this.animationDelay[state].task;
  }

  ngOnDestroy(): void {
    this.details$.unsubscribe();
  }
}
