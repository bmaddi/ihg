import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { Observable, of, Subject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { find } from 'lodash';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { GoogleAnalyticsService, UserService } from 'ihg-ng-common-core';

import { RoomActionsComponent } from './room-actions.component';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
// tslint:disable-next-line:max-line-length
import { WarningRoomUnassignedModalComponent } from '@modules/prepare/components/warning-room-unassigned-modal/warning-room-unassigned-modal.component';
import { AnalyticsTrackingService } from '../../services/analytics-tracking/analytics-tracking.service';
import { RoomAssignmentWarningService } from '@modules/prepare/services/room-assignment-warning/room-assignment-warning.service';


describe('RoomActionsComponent', () => {
  let component: RoomActionsComponent;
  let fixture: ComponentFixture<RoomActionsComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ArrivalsService, {
        useValue: {
          handleUnsavedTasks: (callBackAction: Function, cancelCallBackFn?: Function, dataItem?): void => {
          }
        }
      })
      .overrideProvider(RoomsService, {
        useValue: {
          getRoomReleased: (queryParams): Observable<void> => {
            return of();
          },
          getAssignReleasePayload: (listItem: ArrivalsListModel, room?: Room, isUpgrade = false) => new Subject<void>().asObservable(),
          openNoTasksConfirmation: ( roomNumber?: string) => of('Close')
        }
      })
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          NgbModule,
          RouterTestingModule,
          HttpClientTestingModule
        ],
        declarations: [
          RoomActionsComponent,
          WarningRoomUnassignedModalComponent
        ],
        providers: [
          GoogleAnalyticsService,
          TranslateService,
          TranslateStore,
          UserService,
          AnalyticsTrackingService,
          RoomAssignmentWarningService,
          {
            provide: ActivatedRoute,
            useValue: {
              data: new Subject<void>().asObservable()
            }
          }
        ],
      })
      .overrideComponent(WarningRoomUnassignedModalComponent, {
        set: {
          template: `WarningRoomUnassignedModalComponent`
        }
      })
      .overrideModule(BrowserDynamicTestingModule,
        {set: {entryComponents: [WarningRoomUnassignedModalComponent]}})
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomActionsComponent);
    component = fixture.componentInstance;
    component.retryRelease = new Subject<void>().asObservable();
    component.listItem = {...listItemMock};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('"Change Room Button" should NOT be disabled if room is assigned and "Do Not Move" is off', () => {
    component.listItem = {...listItemMock, ...{assignedRoomNumber: '223', doNotMoveRoom: false}};
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      const button = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
      expect(button).not.toBeNull();
      expect(button.nativeElement.disabled).toBeFalsy();
    });
  });

  // tslint:disable-next-line:max-line-length
  it('"Change Room Button" should NOT be disabled if Room is assigned and "Do Not Move" is OFF and should not show the Room Assignment Tooltip message', () => {
    component.listItem = {...listItemMock, ...{assignedRoomNumber: '223', doNotMoveRoom: false}};
    fixture.detectChanges();

    const button = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
    expect(button).not.toBeNull();
    expect(button.nativeElement.disabled).toBeFalsy();

    const buttonInnerContent = fixture.debugElement.query(By.css('.btn-content'));
    const tooltipAttribute = find(buttonInnerContent.nativeElement.getAttributeNames(), e => e.indexOf('ngb-tooltip') > -1);

    expect(buttonInnerContent).not.toBeNull();
    expect(buttonInnerContent.nativeElement.getAttribute(tooltipAttribute)).toBeNull();
  });

  it('"Change Room Button" should be disabled if room is assigned and "Do Not Move" is on and show the proper tooltip message', () => {
    const roomLockedMsg = 'LBL_ROOM_ASSIGNMENT_LOCKED';
    component.listItem = {...listItemMock, ...{assignedRoomNumber: '223', doNotMoveRoom: true}};
    component.enableRoomMove = false;
    fixture.detectChanges();

    const button = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
    expect(button).not.toBeNull();
    expect(button.nativeElement.disabled).toBeTruthy();

    const buttonInnerContent = fixture.debugElement.query(By.css('.modify-room'));
    const tooltipAttribute = find(buttonInnerContent.nativeElement.getAttributeNames(), e => e.indexOf('ngb-tooltip') > -1);

    expect(buttonInnerContent).not.toBeNull();
    expect(buttonInnerContent.nativeElement.getAttribute(tooltipAttribute)).toEqual(roomLockedMsg);
  });


  it('should NOT show the Change Room Button if enableRoom Move is true', () => {
    component.listItem = {...listItemMock, ...{assignedRoomNumber: '104'}};
    component.enableRoomMove = true;
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
    expect(button).toBeNull();
    component.enableRoomMove = false;
    fixture.detectChanges();
    const changeButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
    expect(changeButton).not.toBeNull();
  });

  it('should call handleAssignForManageStay if enableRoom Move is true', () => {
    const handleAssignForManageStaySpy = spyOn<any>(component, 'handleAssignForManageStay').and.callThrough();
    component.enableRoomMove = true;
    fixture.detectChanges();
    component.handleAssign();
    expect(handleAssignForManageStaySpy).toHaveBeenCalledWith();
  });

  it('should call handleAssignForManageStay if enableRoom Move is true and open No Task Confirmation Message', () => {
    const handleAssignForManageStaySpy = spyOn<any>(component, 'handleAssignForManageStay').and.callThrough();
    const openNoTasksConfirmationSpy = spyOn(TestBed.get(RoomAssignmentWarningService), 'openWarningRoomUnassignedModal').and.callThrough();
    component.enableRoomMove = true;
    fixture.detectChanges();
    component.handleAssign();
    expect(handleAssignForManageStaySpy).toHaveBeenCalled();
    expect(openNoTasksConfirmationSpy).toHaveBeenCalled();
  });

  it('should  call handleAssignForManageStay if enableRoom Move is true and then call openWarningModal when there are tasks present', () => {
    const openWarningModalSpy = spyOn<any>(component, 'openWarningModal').and.callThrough();
    component.listItem = {
      ...listItemMock, ...{
        tasks: [{
          'traceId': 443500,
          'traceText': 'Trying to chg dept',
          'departmentCode': 'XYZ',
          'departmentName': '',
          'traceTimestamp': '2019-02-28T10:16:00.0000000-05:00',
          'isResolved': false
        }]
      }
    };
    component.enableRoomMove = true;
    fixture.detectChanges();
    component.handleAssign();
    expect(openWarningModalSpy).toHaveBeenCalledWith();
  });

it('Should track google analytics for change room manage stay', () =>  {
  component.stateName = 'manage-stay';
  component.listItem = {...listItemMock};
  component.listItem.assignedRoomStatus = {roomNumber: '1234', roomStatus: 'Dirty', roomType: 'KDXG'};
  const ChangeRoomSpy = spyOn<any>(component, 'changeRoom').and.callThrough();
  const trackChangeRoomService = spyOn(TestBed.get(AnalyticsTrackingService), 'trackClickChangeRoom').and.callThrough();
  component['changeRoom']();
  fixture.detectChanges();
  expect(ChangeRoomSpy).toHaveBeenCalled();
  expect(trackChangeRoomService).toHaveBeenCalledWith('manage-stay');
});

it('Should track google analytics for change room manage stay before warning modal', () =>  {
  component.stateName = 'manage-stay';
  component.listItem = {...listItemMock};
  component.listItem.assignedRoomStatus = {roomNumber: '1234', roomStatus: 'Dirty', roomType: 'KDXG'};
  const handleBeforeWarningModal = spyOn<any>(component, 'handleAssignForManageStay').and.callThrough();
  const trackChangeBeforewarningModal = spyOn(TestBed.get(AnalyticsTrackingService), 'trackChangeRoomBeforeModalWarning').and.callThrough();
  component['handleAssignForManageStay']();
  fixture.detectChanges();
  expect(handleBeforeWarningModal).toHaveBeenCalled();
  expect(trackChangeBeforewarningModal).toHaveBeenCalledWith('manage-stay');
});

it('Should track google analytics for change room manage stay In warning modal', () =>  {
  component.stateName = 'manage-stay';
  component.listItem = {...listItemMock};
  component.listItem.assignedRoomStatus = {roomNumber: '1234', roomStatus: 'Dirty', roomType: 'KDXG'};
  const handleInWarningModal = spyOn<any>(component, 'handleAssignForManageStay').and.callThrough();
  const trackChangeInwarningModal = spyOn(TestBed.get(AnalyticsTrackingService), 'trackChangeRoomInModalWarning').and.callThrough();
  const openNoTasksConfirmationSpy = spyOn(TestBed.get(RoomAssignmentWarningService), 'openWarningRoomUnassignedModal').and.returnValue(of('Success'));
  component['handleAssignForManageStay']();
  component.enableRoomMove = true;
  fixture.detectChanges();

  expect(handleInWarningModal).toHaveBeenCalled();
  expect(openNoTasksConfirmationSpy).toHaveBeenCalled();
  expect(trackChangeInwarningModal).toHaveBeenCalledWith('manage-stay');
});

});
