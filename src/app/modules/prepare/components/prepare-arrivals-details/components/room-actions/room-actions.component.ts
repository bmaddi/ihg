import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { AnalyticsTrackingService } from './../../services/analytics-tracking/analytics-tracking.service';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { ReleaseRoomResponse } from '../../models/rooms-list.model';
import { RoomsService } from '../../services/details-rooms/rooms.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { RoomAssignmentWarningService } from '@modules/prepare/services/room-assignment-warning/room-assignment-warning.service';

@Component({
  selector: 'app-room-actions',
  templateUrl: './room-actions.component.html',
  styleUrls: ['./room-actions.component.scss']
})
export class RoomActionsComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  @Input() listItem: ArrivalsListModel;
  @Input() shouldAnimate: boolean;
  @Input() assignBtnPosition: string;
  @Input() showArrivalInfo: boolean;
  @Input() isCheckedIn = false;
  @Input() enableRoomMove = false;
  @Input() errorSubject: Subject<EmitErrorModel>;
  @Input() retryRelease: Observable<void>;
  @Output() emitAssign: EventEmitter<void> = new EventEmitter();
  @Output() emitChangeRoom: EventEmitter<void> = new EventEmitter();

  private rootSubscription = new Subscription();
   stateName: string;

  constructor(
    private trackingService: AnalyticsTrackingService,
    private activatedRoute: ActivatedRoute,
    private prepareArrivalsService: ArrivalsService,
    private roomsService: RoomsService,
    private roomWarningModalService: RoomAssignmentWarningService) {
    super();
  }

  ngOnInit() {
    this.setStateName();
    this.subscribeRetryRelease();
  }

  private setStateName() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.stateName = data.stateName;
    });
  }

  private subscribeRetryRelease(): void {
    this.rootSubscription.add(this.retryRelease.subscribe(() => this.releaseRoom()));
  }

  public handleChangeRoom(): void {
    const callbackFn = this.changeRoom.bind(this);
    this.prepareArrivalsService.handleUnsavedTasks(callbackFn, null, this.listItem);
  }

  private changeRoom(): void {
    this.handleReleaseRoom();
    this.trackingService.trackClickChangeRoom(this.stateName);
  }

  public handleAssign(): void {
    this.enableRoomMove ?
      this.handleAssignForManageStay() :
      this.emitAssign.emit();
  }

  private handleAssignForManageStay() {
    this.trackingService.trackChangeRoomBeforeModalWarning(this.stateName);
    if (this.listItem.tasks && this.listItem.tasks.length > 0) {
     this.openWarningModal();
    } else {
      this.roomWarningModalService.openWarningRoomUnassignedModal(this.listItem, this.enableRoomMove, this.stateName, false)
        .subscribe(
          () => {
            this.emitAssign.emit();
            this.trackingService.trackChangeRoomInModalWarning(this.stateName);
          },
          () => {});
    }
  }

  private releaseRoom() {
    this.roomsService.getRoomReleased(this.roomsService.getAssignReleasePayload(this.listItem, this.listItem['assignedRoomStatus'])).subscribe((response: ReleaseRoomResponse) => {
      if (!super.checkIfAnyApiErrors(response, null, this.errorSubject)) {
        this.emitChangeRoom.emit();
      }
    }, error => {
      super.processHttpErrors(error, this.errorSubject);
    });
  }

  public handleReleaseRoom(): void {
    if (this.listItem.tasks && this.listItem.tasks.length > 0) {
      this.clearErrorToast();
      this.openWarningModal();
    } else {
      this.releaseRoom();
    }
  }

  private clearErrorToast(): void {
    this.errorSubject.next();
  }

  private openWarningModal(): void {
    this.roomWarningModalService.openWarningRoomUnassignedModal(this.listItem, this.enableRoomMove, this.stateName, true)
      .subscribe(
        action => this.handleWarningModalResult(action),
        () => {
        }
      );
  }

  private handleWarningModalResult(action: string) {
    if (action === 'changeRoom') {
      this.emitChangeRoom.emit();
    } else if (action === 'viewTasks') {
    } else if (action === 'roomMove') {
      this.trackingService.trackChangeRoomInModalWarning(this.stateName);
      this.emitAssign.emit();
    }
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
