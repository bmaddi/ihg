import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RoomInfoComponent } from './room-info.component';
import { BASIC_ROOM } from '@app/modules/prepare/components/prepare-arrivals-details/mocks/room-models-mock';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { ConfirmationModalService } from 'ihg-ng-common-components';
import {
  MOCK_PREP_ARRV_RESRV_DATA, MOCK_ROOM
} from '@app/modules/prepare/components/prepare-arrivals-details/mocks/prepare-arrival-details-mock';

import { cloneDeep } from 'lodash';
import { sortBy } from 'lodash';
import { UtilityService } from '@app/services/utility/utility.service';
import { By } from '@angular/platform-browser';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';

describe('RoomInfoComponent', () => {
  let component: RoomInfoComponent;
  let fixture: ComponentFixture<RoomInfoComponent>;

  function prepareComponent() {
    component.listItem = cloneDeep(MOCK_PREP_ARRV_RESRV_DATA.reservationData);
    component.roomInfo = cloneDeep(MOCK_ROOM.roomStatusList["0"]);
    component.emphasized = false;
    component.enableRoomMove = false;
    component.isCheckedIn = false;
    component.showRoomNotReadyAlert = false;
    fixture.detectChanges();
  }

  function transformText(text: string) {
    return text.trim().toLocaleLowerCase();
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoomInfoComponent],
      imports: [CommonTestModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [ConfirmationModalService, UtilityService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should properly emit the room click operation', () => {
    spyOn(component.selection, 'emit');

    component.handleRoomPanelClick();
    expect(component.selection.emit).toHaveBeenCalledTimes(0);

    component.roomInfo = cloneDeep(BASIC_ROOM);

    component.handleRoomPanelClick();
    expect(component.selection.emit).toHaveBeenCalledTimes(1);
  });

  it('should verify number of features are same', () => {
    prepareComponent();
    const el = Array.from(fixture.debugElement.nativeElement.querySelectorAll('.room-features.preference .feature-item'));
    expect(el.length).toEqual(component.maxFeatures);
  });

  xit('check if features are displayed in proper order', () => {
    prepareComponent();
    const el = fixture.debugElement.nativeElement.querySelector('.room-features.preference .feature-item:last-child .feature-text');
    const mockRoomData = cloneDeep(MOCK_ROOM.roomStatusList["0"]);
    const orderedFeatures = sortBy(mockRoomData.reservationFeatures, ['matched']).reverse();
    const utilityServ = TestBed.get(UtilityService);
    const firstFeatureText = transformText(utilityServ.truncateWithEllipsis(orderedFeatures["0"].description, component.maxCharLimit));
    expect(firstFeatureText).toEqual(transformText(el.innerHTML));
  });

  it('should check if room number and room type are correctly populated', () => {
    prepareComponent();
    const roomNumberElm = transformText(fixture.debugElement.nativeElement.querySelector('.room-number').innerHTML);
    const roomTypeElm = transformText(fixture.debugElement.nativeElement.querySelector('.room-type').innerHTML);
    const mockRoomData = cloneDeep(MOCK_ROOM.roomStatusList["0"]);
    expect(roomNumberElm).toEqual(transformText(mockRoomData.roomNumber));
    expect(roomTypeElm).toEqual(transformText(mockRoomData.roomType));
  });

  it('should have no suggested upgrade message if it is upgrade panel and no room info is not available', () => {
    component.upgradeRoom = true;
    fixture.detectChanges();

    const noUpgrade = fixture.debugElement.query(By.css('[data-slnm-ihg="NoSuggestedUpgrade-SID"]'));

    expect(noUpgrade).not.toBeNull();
    expect(noUpgrade.nativeElement.innerText).toContain('LBL_NO_SGG_UPG');
  });


  it('should display DNM icon and tooltip', () => {
    component.roomInfo = cloneDeep(BASIC_ROOM);
    component.listItem = {...listItemMock, ...{doNotMoveRoom: true, assignedRoomNumber: BASIC_ROOM.roomNumber}};

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('app-dnm-reason-tooltip'))).not.toBeNull();
  });
});
