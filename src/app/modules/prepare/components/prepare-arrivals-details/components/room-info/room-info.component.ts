import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { sortBy } from 'lodash';

import { Room } from '../../models/rooms-list.model';
import { ROOM_STATUS } from '../../constants/room-status.constants';
import { UtilityService } from '@services/utility/utility.service';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';

@Component({
  selector: 'app-room-info',
  templateUrl: './room-info.component.html',
  styleUrls: ['./room-info.component.scss']
})

export class RoomInfoComponent implements OnChanges {
  roomStatus;
  maxFeatures = 4;
  maxCharLimit = 16;

  @Input() listItem: ArrivalsListModel;
  @Input() roomInfo: Room;
  @Input() emphasized: boolean;
  @Input() isCheckedIn: boolean;
  @Input() enableRoomMove: boolean;
  @Input() upgradeRoom = false;
  @Input() showRoomNotReadyAlert: boolean;
  @Input() dnmInteraction: boolean;
  @Output() selection: EventEmitter<null> = new EventEmitter();

  constructor(public utility: UtilityService, private roomService: RoomsService) {
    this.roomStatus = ROOM_STATUS;
  }

  ngOnChanges() {
    this.sortFeatures();
  }

  private sortFeatures(): void {
    if (this.roomInfo && this.roomInfo.reservationFeatures) {
      this.roomInfo.reservationFeatures = sortBy(this.roomInfo.reservationFeatures, ['matched']).reverse();
    }
  }

  handleRoomPanelClick(): void {
    if (this.roomInfo) {
      this.roomInfo.selected = true;
      this.selection.emit();
    }
  }

  get isRoomNotReady(): boolean {
    return !this.roomService.isRoomReady(this.listItem);
  }
}
