import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateFakeLoader, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  DetachedToastMessageService,
  GoogleAnalyticsService,
  ToastMessageComponent,
  UserService,
  MenuService,
  RecentNotificationsService
} from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { RoomNotificationsComponent } from './room-notifications.component';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';

describe('RoomNotificationsComponent', () => {
  let component: RoomNotificationsComponent;
  let fixture: ComponentFixture<RoomNotificationsComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .overrideProvider(ConfirmationModalService, {
        useValue: {}
      })
      .overrideProvider(DetachedToastMessageService, {
        useValue: {}
      })
      .overrideProvider(MenuService, {
        useValue: {}
      })
      .overrideProvider(RecentNotificationsService, {
        useValue: {}
      })
      .configureTestingModule({
        imports: [
          NgbAlertModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useClass: TranslateFakeLoader
            }
          }),
          HttpClientTestingModule],
        declarations: [RoomNotificationsComponent, ToastMessageComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          GoogleAnalyticsService,
          UserService,
          ConfirmationModalService,
          DetachedToastMessageService,
          MenuService,
          RecentNotificationsService
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomNotificationsComponent);
    component = fixture.componentInstance;
    component.listItem = {...listItemMock};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show room upgrade alert if guest is upgrade entitled and room is not assigned', () => {
    expect(component.roomUpgradeAlert).toBeDefined();
    component.showUpgradeAlert = true;

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="EligibilityMessage-SID"]'))).not.toBeNull();
  });

  it('should not show the room upgrade alert if room is assigned', () => {
    expect(component.roomUpgradeAlert).toBeDefined();
    component.showUpgradeAlert = true;
    component.listItem.assignedRoomNumber = '12123';

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="EligibilityMessage-SID"]'))).toBeNull();
  });

  it('should show room upgraded successfully message if guest is assigned an upgrade room', () => {
    expect(component.roomUpgradeAlert).toBeDefined();
    component.listItem.freeUpgradeInd = true;
    component.listItem.assignedRoomNumber = '12123';

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradedAlert-SID"]'))).not.toBeNull();
  });

  it('should not show room upgraded successfully message', () => {
    expect(component.roomUpgradeAlert).toBeDefined();
    component.listItem.freeUpgradeInd = true;
    component.listItem.assignedRoomNumber = null;

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomUpgradedAlert-SID"]'))).toBeNull();
  });
});
