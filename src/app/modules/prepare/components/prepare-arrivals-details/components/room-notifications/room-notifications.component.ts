import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { Alert, AlertType } from 'ihg-ng-common-core';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ROOM_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import {
  AnalyticsTrackingService
} from '@modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';

@Component({
  selector: 'app-room-notifications',
  templateUrl: './room-notifications.component.html',
  styleUrls: ['./room-notifications.component.scss']
})
export class RoomNotificationsComponent implements OnInit {
  public guestNotifyAlert = { type: AlertType.Info, message: 'LBL_NOTE', detailMessage: 'LBL_ASGN_ROOM_NTFD' };
  public roomUpgradeAlert = this.roomsService.getUpgradeEligibilityMessage();
  public roomUpgradeSuccessAlert = this.roomsService.getRoomUpgradedMessage();
  public roomNotReadyAlert: Alert;

  private notify$: Subscription = new Subscription();

  @Input() listItem: ArrivalsListModel;
  @Input() stateName: string;
  @Input() showNotifiedAlert: boolean;
  @Input() showRoomNotReadyAlert: boolean;
  @Input() showUpgradeAlert: boolean;

  constructor(private trackingService: AnalyticsTrackingService, private roomsService: RoomsService, private translate: TranslateService) {
  }

  ngOnInit() {
    this.checkRoomNotReadyAlert();
    this.subscribeRoomChange();
    this.subscribeToLanguageChange();
  }

  private subscribeRoomChange(): void {
    this.roomsService.getRoom().subscribe(() => this.checkRoomNotReadyAlert());
  }

  private checkRoomNotReadyAlert(): void {
    if (this.showRoomNotReadyAlert && this.listItem.assignedRoomNumber && this.isRoomNotReady) {
      this.setRoomNotReadyAlert();
      this.trackingService.trackRoomInspectionWarning(this.stateName);
    }
  }

  get isRoomNotReady(): boolean {
    return !this.roomsService.isRoomReady(this.listItem);
  }

  private setRoomNotReadyAlert(): void {
    this.roomNotReadyAlert = {
      type: AlertType.Warning,
      message: 'COM_TOAST_WARNING',
      detailMessage: 'LBL_ASGND_RM_STAT',
      dismissible: false,
      translateValues: {
        detailMessage: { status: this.listItem.assignedRoomStatus ? this.getRoomStatus() : '' }
      }
    };
  }

  private getRoomStatus(): string {
    const assignedRoom = this.listItem.assignedRoomStatus;
    let status = assignedRoom.frontOfficeStatus ? this.translate.instant(ROOM_STATUS[assignedRoom.frontOfficeStatus].label) : '';
    if (assignedRoom.frontOfficeStatus && assignedRoom.roomStatus) {
      status = status.concat(` / ${this.translate.instant(ROOM_STATUS[assignedRoom.roomStatus].label)}`);
    } else if (assignedRoom.roomStatus) {
      status = this.translate.instant(ROOM_STATUS[assignedRoom.roomStatus].label);
    }
    return status;
  }

  private subscribeToLanguageChange(): void {
    this.notify$.add(this.translate.onLangChange.subscribe(() => {
      this.setRoomNotReadyAlert();
    }));
  }
}
