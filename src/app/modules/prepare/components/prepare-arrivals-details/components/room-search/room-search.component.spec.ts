import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { RoomSearchComponent } from './room-search.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';


describe('RoomSearchComponent', () => {
  let component: RoomSearchComponent;
  let fixture: ComponentFixture<RoomSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomSearchComponent ]
    })
    .configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule],
      declarations: [RoomSearchComponent],
    })
    .compileComponents();
  }));

    beforeEach(() => {
      fixture = TestBed.createComponent(RoomSearchComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check the placeholder value in the search box', () => {
    const searchBox = fixture.debugElement.query(By.css('.form-control'));
    expect(searchBox.nativeElement.placeholder).toEqual('');

  });

  it('should check the Search box label and should be same as for Search"', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomSearchInput-SID"]')).nativeElement;
    expect(element).not.toBeNull();
    expect(element.textContent).toEqual('');
  });

  it('should check doSearch method', () => {
    const search = component.searchInput;
    const searchQuery = 'Test';
    const event = new KeyboardEvent('keypress', {
      'key': 'up'
    });
    spyOn(component, 'doSearch');
    component.doSearch(searchQuery);
    expect(component.doSearch).toHaveBeenCalled();
  });
 });

