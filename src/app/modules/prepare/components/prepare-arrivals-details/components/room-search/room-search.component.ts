import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-room-search',
  templateUrl: './room-search.component.html',
  styleUrls: ['./room-search.component.scss']
})
export class RoomSearchComponent implements OnInit {
  @ViewChild('searchInput') searchInput;
  searchQuery = '';
  private _placeholder: string = "";
  @Output() search: EventEmitter<string> = new EventEmitter();

  @Input()
  set placeholder(value: string) {
    this._placeholder = value;
  }
  get placeholder(): string {
    return this._placeholder;
  }

  constructor() { }
  
  ngOnInit() {
  }
  
  doSearch(qstring: string): void {
    this.search.emit(qstring);
  }

}
