import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EventEmitter, Input, Output } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MultiSelectModule } from '@progress/kendo-angular-dropdowns';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { RoomTypeFilterComponent } from './room-type-filter.component';
import { RoomList } from '@modules/prepare/components/prepare-arrivals-details/constants/room-test-constants';

export class StubRoomComponent {
  @Input() roomList = RoomList;
  @Output() roomTypeChange = new EventEmitter<string[]>();
}

describe('RoomTypeFilterComponent', () => {
  let component: RoomTypeFilterComponent;
  let fixture: ComponentFixture<RoomTypeFilterComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomTypeFilterComponent ],
      imports: [ MultiSelectModule, TranslateModule.forRoot(), FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomTypeFilterComponent);
    component = fixture.componentInstance;
    component.roomList = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('multiselect component to be loaded', () => {
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomTypeSelect-SID"]')).nativeElement;
    expect(element).not.toBeNull();
  });

  it('should to check autoClose attribute value is set to false', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="RoomTypeSelect-SID"]')).nativeElement;
    expect(element).not.toBeNull();
    expect(element.attributes).toBeDefined();
    expect(element.attributes.autoClose).toBeFalsy();
  });

  it('should not have PM room type selected if not available in the dropdown list', () => {
    component.distinctRoomTypes = ['KDXG', 'CSTN'];
    component.selectedRoomTypes = ['PM'];
    component['splicePostMaster']();
    expect(component.selectedRoomTypes).toEqual([]);
  });
});
