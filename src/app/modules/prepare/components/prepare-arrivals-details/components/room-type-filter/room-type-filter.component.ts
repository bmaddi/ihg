import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';

import { Room } from '@app/modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';

@Component({
  selector: 'app-room-type-filter',
  templateUrl: './room-type-filter.component.html',
  styleUrls: ['./room-type-filter.component.scss']
})
export class RoomTypeFilterComponent implements OnChanges {

  public distinctRoomTypes: string[] = [];
  public filterSettings: DropDownFilterSettings = {
    caseSensitive: false,
    operator: 'startsWith'
  };

  @Input() roomList: Room[];
  @Input() disabled: boolean;
  @Input() selectedRoomTypes: string[] = [];
  @Output() roomTypeChange = new EventEmitter<string[]>();

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.roomList && changes.roomList.currentValue && changes.roomList.currentValue.length) {
      this.prepareDistinctRoomTypes();
      this.splicePostMaster();
    }
  }

  private prepareDistinctRoomTypes(): void {
    this.distinctRoomTypes = Array.from(new Set(this.roomList.map(item => item.roomType)))
      .sort((a, b) => {
        return a.localeCompare(b);
      });
  }

  public handleValueChange(roomTypes: string[]): void {
    this.roomTypeChange.emit(roomTypes);
  }

  private splicePostMaster() {
    if (this.distinctRoomTypes.indexOf('PM') === -1 && this.selectedRoomTypes.indexOf('PM') > -1) {
      const index = this.selectedRoomTypes.indexOf('PM');
      this.selectedRoomTypes.splice(index, 1);
    }
  }
}
