import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateFakeLoader, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { cloneDeep } from 'lodash';
import { TitleCasePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { DetachedToastMessageService, GoogleAnalyticsService, PipesModule, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { RoomUpgradeComponent } from './room-upgrade.component';
import { RoomInfoComponent } from '@modules/prepare/components/prepare-arrivals-details/components/room-info/room-info.component';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ROOM_SUGGESTIONS_MOCK } from '@modules/prepare/components/prepare-arrivals-details/mocks/room-assign-mock';
import { UtilityService } from '@services/utility/utility.service';
import { AnalyticsTrackingService } from '../../services/analytics-tracking/analytics-tracking.service';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';

describe('RoomUpgradeComponent', () => {
  let component: RoomUpgradeComponent;
  let fixture: ComponentFixture<RoomUpgradeComponent>;

  beforeEach(async(() => {
    TestBed
    .overrideProvider(GoogleAnalyticsService, {
      useValue: googleAnalyticsStub
    })
    .overrideProvider(DetachedToastMessageService, {useValue: {}})
    .configureTestingModule({
        imports: [
          TranslateModule.forRoot({loader: {provide: TranslateLoader, useClass: TranslateFakeLoader}}),
          PipesModule,
          NgbTooltipModule,
          HttpClientTestingModule
        ],
        declarations: [RoomUpgradeComponent, RoomInfoComponent],
        providers: [GoogleAnalyticsService, UserService, ConfirmationModalService, DetachedToastMessageService, TitleCasePipe],
        schemas: [NO_ERRORS_SCHEMA]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomUpgradeComponent);
    component = fixture.componentInstance;
    component.reservationDetails = cloneDeep(listItemMock);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should highlight the room upgrade by default if available to enabled selection and button animation', () => {
    const getRoomUpgradeSpy = spyOn(TestBed.get(RoomsService), 'getRoomUpgrade').and.returnValue(of(ROOM_SUGGESTIONS_MOCK)),
      upgradeSelected = spyOn(component.upgradeSelected, 'emit').and.callThrough();
    component.upgradeRoom = null;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="NoSuggestedUpgrade-SID"]'))).not.toBeNull();

    component.ngOnInit();
    fixture.detectChanges();

    expect(getRoomUpgradeSpy).toHaveBeenCalled();
    expect(upgradeSelected.calls.mostRecent().args[0].roomNumber).toEqual('0100');
    expect(upgradeSelected.calls.mostRecent().args[0].selected).toBeTruthy();
  });

  it('should display room upgrade if available ', () => {
    const mockSelection = cloneDeep(ROOM_SUGGESTIONS_MOCK.roomStatusList[0]);
    mockSelection.selected = true;
    component.upgradeRoom = mockSelection;

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomNumber-SID"]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="RoomStatus-SID"]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('.room-features'))).not.toBeNull();
  });

  it('should fetch room upgrade if available on room unassignment', () => {
    const mockSelection = cloneDeep(ROOM_SUGGESTIONS_MOCK.roomStatusList[1]);
    mockSelection.selected = true;
    component.upgradeRoom = mockSelection;
    const getRoomUpgradeSpy = spyOn(TestBed.get(RoomsService), 'getRoomUpgrade').and.returnValue(of(ROOM_SUGGESTIONS_MOCK)),
      upgradeSelected = spyOn(component.upgradeSelected, 'emit').and.callThrough(),
      paramsChange = spyOn(TestBed.get(UtilityService), 'hasSimpleParamChanges').and.callThrough();

    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="NoSuggestedUpgrade-SID"]'))).toBeNull();
    component.isRoomAssigned = false;
    component.ngOnChanges({
      isRoomAssigned: new SimpleChange(true, false, false)
    });
    fixture.detectChanges();

    expect(getRoomUpgradeSpy).toHaveBeenCalled();
    expect(paramsChange.calls.mostRecent().returnValue).toBeTruthy();
    expect(upgradeSelected.calls.mostRecent().args[0].roomNumber).toEqual('0100');
    expect(upgradeSelected.calls.mostRecent().args[0].selected).toBeTruthy();
  });

  it('should display "No Suggested Upgrade Panel" if no upgrades are available', () => {
    const getRoomUpgradeSpy = spyOn(TestBed.get(RoomsService), 'getRoomUpgrade').and.returnValue(of({roomStatusList: []}));
    component.upgradeRoom = null;

    component.ngOnInit();
    fixture.detectChanges();

    expect(getRoomUpgradeSpy).toHaveBeenCalled();
    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="NoSuggestedUpgrade-SID"]'))).not.toBeNull();
  });

  it('should track google analytics for the selection for free upgrade for check-in', () => {
    component.stateName = 'check-in';
    component.upgradeRoom = {
      roomNumber: '5001',
      selected: true
    };

    const trackFreeUpgradeSpy = spyOn(TestBed.get(AnalyticsTrackingService), 'trackSelectFreeUpgrade');
    component.handleUpgradePanelSelection();
    fixture.detectChanges();

    expect(trackFreeUpgradeSpy).toBeDefined();
    expect(trackFreeUpgradeSpy).toHaveBeenCalledWith('check-in');
  });

  it('should track google analytics for the selection for free upgrade for prepare', () => {
    component.stateName = 'guest-list-details';
    component.upgradeRoom = {
      roomNumber: '5001',
      selected: true
    };

    const trackFreeUpgradeSpy = spyOn(TestBed.get(AnalyticsTrackingService), 'trackSelectFreeUpgrade');
    component.handleUpgradePanelSelection();
    fixture.detectChanges();

    expect(trackFreeUpgradeSpy).toBeDefined();
    expect(trackFreeUpgradeSpy).toHaveBeenCalledWith('guest-list-details');
  });
});
