import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { head } from 'lodash';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

import {
  Room,
  RoomsListResponse,
  RoomUpgradeQueryParameters,
  RoomUpgradeRequestPayload
} from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { UtilityService } from '@services/utility/utility.service';
import { AnalyticsTrackingService } from '../../services/analytics-tracking/analytics-tracking.service';

@Component({
  selector: 'app-room-upgrade',
  templateUrl: './room-upgrade.component.html',
  styleUrls: ['./room-upgrade.component.scss']
})
export class RoomUpgradeComponent extends AppErrorBaseComponent implements OnInit, OnChanges {
  @Input() reservationDetails: ArrivalsListModel;
  @Input() isRoomAssigned: boolean;
  @Input() upgradeRoom: Room;
  @Input() stateName: string;
  @Input() viewOnlyUpgrade = false;
  @Output() upgradeSelected: EventEmitter<Room> = new EventEmitter();

  private upgradeRoomSubscriptions$ = new Subscription();

  constructor(private roomService: RoomsService,
              private trackingService: AnalyticsTrackingService,
              private utilityService: UtilityService) {
    super();
  }

  ngOnInit() {
    this.getRoomUpgradeOption();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.utilityService.hasSimpleParamChanges(changes, 'isRoomAssigned') && !this.isRoomAssigned) {
      this.getRoomUpgradeOption();
    }
  }

  handleUpgradePanelSelection() {
    this.upgradeRoom.selected = true;
    this.upgradeSelected.emit(this.upgradeRoom);
    this.trackingService.trackSelectFreeUpgrade(this.stateName);
  }

  private getRoomUpgradeOption() {
    if (!this.viewOnlyUpgrade) {
      this.upgradeRoomSubscriptions$.add(
        this.roomService.getRoomUpgrade(
          this.getRoomQueryParameters(this.reservationDetails),
          this.getRequestPayload(this.reservationDetails))
          .subscribe(
            response => this.handleSuccessfulResponse(response),
            err => this.handleError(err)
          )
      );
    }
  }

  private handleSuccessfulResponse(response: RoomsListResponse) {
    if (!super.checkIfAnyApiErrors(response, null)) {
      const upgradeRoom: Room = head(response.roomStatusList);
      if (upgradeRoom) {
        upgradeRoom.selected = true;
      }
      this.upgradeSelected.emit(upgradeRoom);
    } else {
      this.upgradeSelected.emit(null);
    }
  }

  private handleError(error: HttpErrorResponse) {
    super.processHttpErrors(error);
    this.upgradeSelected.emit(null);
  }

  private getRoomQueryParameters({reservationNumber, badges}: ArrivalsListModel): RoomUpgradeQueryParameters {
    return {
      page: 1,
      reservationNumber,
      upgradeLevel: this.roomService.getUpgradeLevel(badges)
    };
  }

  private getRequestPayload(reservation: ArrivalsListModel): RoomUpgradeRequestPayload {
    return {
      arrivalDate: reservation.checkInDate,
      departureDate: reservation.checkOutDate,
      ihgRcNumber: reservation.ihgRcNumber,
      numberOfNights: reservation.numberOfNights,
      numberOfRooms: reservation.numberOfRooms,
      rateCategory: reservation.rateCategoryCode,
      roomType: reservation.roomTypeCode,
      pmsReservationNum: reservation.pmsReservationNumber
    };
  }
}
