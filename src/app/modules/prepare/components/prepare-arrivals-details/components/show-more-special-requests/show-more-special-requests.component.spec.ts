import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMoreSpecialRequestsComponent } from './show-more-special-requests.component';

xdescribe('ShowMoreSpecialRequestsComponent', () => {
  let component: ShowMoreSpecialRequestsComponent;
  let fixture: ComponentFixture<ShowMoreSpecialRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowMoreSpecialRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMoreSpecialRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
