import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-show-more-special-requests',
  templateUrl: './show-more-special-requests.component.html',
  styleUrls: ['./show-more-special-requests.component.scss']
})
export class ShowMoreSpecialRequestsComponent implements OnInit {

  public specialRequests: string;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onClose() {
    this.activeModal.close();
  }
}
