import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusFilterComponent } from './status-filter.component';
import { By } from '@angular/platform-browser';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { RoomList } from '@modules/prepare/components/prepare-arrivals-details/constants/room-test-constants';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';

describe('StatusFilterComponent', () => {
  let component: StatusFilterComponent;
  let fixture: ComponentFixture<StatusFilterComponent>;
  let translateService: TranslateService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusFilterComponent ],
      providers: [ TranslateService ],
      imports: [IhgNgCommonKendoModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusFilterComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check status filter component to be loaded', () => {
    component.roomList = RoomList;
    fixture.detectChanges();
    const element = fixture.nativeElement.querySelector('.status-filter');
    expect(element).toBeDefined();
  });

  it('should check multiselect component is loaded', () => {
    component.roomList = RoomList;
    expect(component.roomList).toBeTruthy();
    fixture.detectChanges();
    const element = fixture.nativeElement.querySelector('.status-filter');
    expect(element).not.toBeNull();
    const multiselectElement = fixture.nativeElement.querySelector('.k-multiselect-wrap');
    expect(multiselectElement).toBeDefined();
  });

  it('should check status filter value is prepopulated', () => {
    component.roomList = RoomList;
    expect(component.roomList).toBeTruthy();
    fixture.detectChanges();
    const element = fixture.nativeElement.querySelector('.status-filter');
    expect(element).not.toBeNull();
    const multiselectElement = fixture.nativeElement.querySelector('.k-multiselect-wrap');
    expect(multiselectElement).toBeDefined();
    const kendoTag = fixture.nativeElement.querySelector('.k-multiselect .k-button');
    expect(kendoTag).toBeDefined();
  });

});
