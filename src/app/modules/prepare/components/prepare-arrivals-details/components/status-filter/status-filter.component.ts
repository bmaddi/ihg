import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import { DEFAULT_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Component({
  selector: 'app-status-filter',
  templateUrl: './status-filter.component.html',
  styleUrls: ['./status-filter.component.scss']
})
export class StatusFilterComponent implements  OnChanges {

  public distinctStatusList: string[];
  public selectedStatus: string[];
  public inspectExists = false;
  public filterSettings: DropDownFilterSettings = {
    caseSensitive: false,
    operator: 'startsWith'
  };

  @Input() roomList: Room[];
  @Input() stateName: string;
  @Input() inspected: boolean;
  @Input() postMasterSelected: boolean;
  @Input() updatedRoomList: Room[];
  @Output() statusTypeChange = new EventEmitter<string[]>();

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.roomList && changes.roomList.currentValue && changes.roomList.currentValue.length &&
      !changes.roomList.isFirstChange()) {
      if (this.selectedStatus === undefined) {
        this.getDefaultFilter();
      }
      this.prepareDistinctStatus();
    }
  }

  private getDefaultFilter() {
    const matchingStateName: boolean = [AppStateNames.checkIn as string, AppStateNames.manageStay as string].indexOf(this.stateName) >= 0;
    if (matchingStateName && this.inspected && this.checkDefaultExists(DEFAULT_STATUS.RoomInspected)) {
        this.selectedStatus = DEFAULT_STATUS.RoomInspected;
        this.handleValueChange(this.selectedStatus);
    } else if (matchingStateName && !this.inspected && this.checkDefaultExists(DEFAULT_STATUS.RoomClean)) {
      this.selectedStatus = DEFAULT_STATUS.RoomClean;
      this.handleValueChange(this.selectedStatus);
    } else {
      this.selectedStatus = [];
    }
  }

  private prepareDistinctStatus(): void {
    this.distinctStatusList = Array.from(new Set(this.updatedRoomList.map(item => item.statusFilter)))
      .sort((a, b) => {
        return a.localeCompare(b);
      });
  }

  public handleValueChange(statusVal: string[]): void {
    this.statusTypeChange.emit(statusVal);
  }

  private checkDefaultExists(statusVal: string[]): boolean {
    if (this.updatedRoomList) {
      return this.updatedRoomList.some(item => item.statusFilter.indexOf(statusVal.toString()) > -1);
    }
  }

}
