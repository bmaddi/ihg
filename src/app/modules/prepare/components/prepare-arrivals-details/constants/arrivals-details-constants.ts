export const defaultTask = {
  traceId: null,
  traceText: null,
  departmentCode: null,
  departmentName: null,
  traceTimestamp: null,
  isResolved: null,
};

export const ROOM_ERROR_CONTENT = {
  suggestedMatch: {
    retryButton: 'COM_BTN_RFRSH',
    reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
    timeoutError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_SUGG_MTCH_LOAD'
    },
    generalError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_SUGG_MTCH_LOAD'
    }
  },
  assignRoom: {
    retryButton: 'LBL_ERR_TRYAGAIN',
    reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
    timeoutError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_RM_ASGNMNT'
    },
    generalError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_RM_ASGNMNT'
    }
  },
  changeRoom: {
    retryButton: 'LBL_ERR_TRYAGAIN',
    reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
    timeoutError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_RM_UNASGN'
    },
    generalError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'LBL_ERR_RM_UNASGN'
    }
  }
};

export const MAN_RM_ASGN_LOAD_ERR = {
  retryButton: 'COM_BTN_RFRSH',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_MAN_RM_ASGN_LOAD'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_MAN_RM_ASGN_LOAD'
  }
};

export const ADD_TASKS_ERR = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_SND_TSK'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_SND_TSK'
  }
};

export const LOAD_TASKS_ERR = {
  retryButton: 'COM_BTN_RFRSH',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_LOAD_TSK'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_LOAD_TSK'
  }
};


export const ROOM_ASSINMENT = {
  categoryPrepareArrivals: 'Guest List - Prepare',
  categoryCheckIn : 'Check In',
  action : 'Room Assignment Expanded',
  label : 'Suggested Matches Not Loaded for Flow-Through Room Type {ROOM TYPE}',
  stateName : 'check-in'
};
