export const RM_ASGNMNT_ERR_CODES = {
  roomUnavailable: 'ERR-300',
  inventoryUnavailable: 'ERR-302',
  inventoryUnavailableGrp: 'ERR-303',
  inventoryUnavailableHouse: 'ERR-304'
};

export const RM_ERR_SLNM_ID = {
  [RM_ASGNMNT_ERR_CODES.roomUnavailable]: 'RoomNoMoreAvailableError-SID',
  [RM_ASGNMNT_ERR_CODES.inventoryUnavailable]: 'InventoryNotAvailableError-SID',
  [RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp]: 'InventoryNotAvailableGroupError-SID',
  [RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse]: 'InventoryNotAvailableHouseError-SID',
};
