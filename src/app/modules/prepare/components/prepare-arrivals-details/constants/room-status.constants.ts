import { MANAGE_STAY_ROOM_REPORT_ISSUE, ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';

export const ROOM_STATUS = {
  Vacant: {
    label: 'LBL_VCNT',
    value: 'Vacant'
  },
  Dirty: {
    label: 'LBL_DRTY',
    value: 'Dirty'
  },
  Clean: {
    label: 'LBL_CLN',
    value: 'Clean'
  },
  Inspected: {
    label: 'LBL_INSPCTD',
    value: 'Inspected'
  },
  PickUp: {
    label: 'LBL_PCKUP',
    value: 'PickUp'
  },
  OutOfOrder: {
    label: 'LBL_OOO',
    value: 'OutOfOrder'
  },
  OutOfService: {
    label: 'LBL_OOS',
    value: 'OutOfService'
  },
  Occupied: {
    label: 'LBL_OCPD',
    value: 'Occupied'
  },
  Other: {
    label: 'LBL_OTH',
    value: 'Other'
  }
};

export const STATE_PARAMS = {
  'check-in': 'checkIn',
  'guest-list-details': 'prepareArrivals',
  'walk-in': 'checkIn',
  'manage-stay': 'inHouse'
};

export const ReportIssueByStateName = {
  'manage-stay': MANAGE_STAY_ROOM_REPORT_ISSUE,
  'guest-list-details': ROOM_REPORT_ISSUE_AUTOFILL
};

export const DEFAULT_STATUS = {
  RoomInspected: ['Vacant / Inspected'],
  RoomClean: ['Vacant / Clean']
};

export const STATUS_HASHMAP = {
  'Vacant / Inspected': 1,
  'Vacant / Clean': 2,
  'Vacant / PickUp': 3,
  'Vacant / Dirty': 4,
  'Occupied / Inspected': 5,
  'Occupied / Clean': 6,
  'Occupied / Pickup': 7,
  'Occupied / Dirty': 8
};

export const HOUSE_KEEPING_STATUS = {
  Inspected: ['Dirty', 'Clean', 'Inspected'],
  Clean: ['Dirty', 'Clean']
};

