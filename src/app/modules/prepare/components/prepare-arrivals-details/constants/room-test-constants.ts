export const RoomList =
  [
    {
      'roomStatus': 'Clean',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Clean',
      'roomNumber': '0100',
      'roomType': 'KNGN',
      'features': [{'code': 'BL', 'description': 'Balcony'}, {'code': 'BR', 'description': 'Business Room'}, {
        'code': 'CR',
        'description': 'Corner Room'
      }, {'code': 'HF', 'description': 'High Floor Near Elevator'}, {'code': 'CV', 'description': 'City View'}, {
        'code': 'GV',
        'description': 'Garden View'
      }, {'code': 'OV', 'description': 'Ocean View'}],
      'reservationFeatures': [{'code': 'BL', 'type': 'R', 'description': 'Balcony', 'matched': false}, {
        'code': 'CV',
        'type': 'R',
        'description': 'City View',
        'matched': true
      }, {'code': 'GV', 'type': 'R', 'description': 'Garden View', 'matched': true}, {
        'code': 'HF',
        'type': 'R',
        'description': 'High Floor Near Elevator',
        'matched': true
      }, {'code': 'OV', 'type': 'R', 'description': 'Ocean View', 'matched': false}]
    },
    {
      'roomStatus': 'Dirty',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Dirty',
      'roomNumber': '402',
      'roomType': 'KEXN',
      'features': [{'code': 'BL', 'description': 'Balcony'}, {'code': 'BR', 'description': 'Business Room'}, {
        'code': 'CR',
        'description': 'Corner Room'
      }]
    },
    {
      'roomStatus': 'Clean',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Clean',
      'roomNumber': '403',
      'roomType': 'PM',
      'features': [{'code': 'BL', 'description': 'Balcony'}]
    },
    {
      'roomStatus': 'Inspected',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Inspected',
      'roomNumber': '405',
      'roomType': 'PM',
      'features': [{'code': 'BL', 'description': 'Balcony'}]
    }];

export const MockRoomsResponse = {
  roomStatusList: [
    {
      'roomStatus': 'Dirty',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Dirty',
      'roomNumber': '400',
      'roomType': 'KEXN',
      'features': null
    },
    {
      'roomStatus': 'Dirty',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Dirty',
      'roomNumber': '404',
      'roomType': 'KEXN',
      'features': null
    },
    {
      'roomStatus': 'Clean',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Clean',
      'roomNumber': '0102',
      'roomType': 'KNGN',
      'features': [
        {
          'code': 'KB',
          'description': 'King Bed',
          'matchedWithResFeature': false
        }
      ]
    },
    {
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Inspected',
      'roomNumber': '0103',
      'roomType': 'PM',
      'roomStatus': 'Inspected',
      'features': [
        {
          'code': 'KB',
          'description': 'King Bed',
          'matchedWithResFeature': false
        }]
    },
  ]
};

