export const eligibilityMap: { [key: string]: { level: number, brandEntitlements?: string[] } } = {
  spire: {level: 1},
  platinum: {level: 1},
  royalambassador: {level: 2, brandEntitlements: ['ICON']},
  ambassador: {level: 1, brandEntitlements: ['ICON']},
  innercircle: {level: 1, brandEntitlements: ['KIKI']}
};

