import { RateCategoryDataModel } from "@app/modules/offers/models/offers.models";

export const MOCK_PREP_ARRV_RESRV_DATA = {
  "reservationData": {
        "firstName": "TEST USER B",
        "lastName": "ANGELS",
        "reservationNumber": "21836327",
        "arrivalTime": "",
        "ihgRcNumber": "119200393",
        "ihgRcMembership": "spire",
        "vipCode": "Y",
        "companyName": "INTERCONTINENTAL HOTELS GROUP",
        "groupName": "",
        "arrival": "",
        "rateCategoryCode": "IKPCM",
        "roomTypeCode": "CSTN",
        "badges": [
          "spire"
        ],
        "heartBeatActions": null,
        "heartBeatComment": "",
        "heartBeatImprovement": "",
        "heartBeatActionInd": "",
        "assignedRoomNumber": "",
        "assignedRoomStatus": null,
        "numberOfNights": 2,
        "numberOfRooms": 1,
        "numberOfAdults": 1,
        "numberOfChildren": 1,
        "preferences": [
          "BALCONY",
          "ROOM NEAR ELEVATOR"
        ],
        "specialRequests": [
          "Need Extra Towels"
        ],
        "infoItems": [],
        "pmsReservationNumber": "18124",
        "pmsUniqueNumber": "",
        "inspected": true,
        "prepStatus": "READY",
        "tasks": [
          {
            "traceId": 7410,
            "traceText": "Need Extra Towels",
            "departmentCode": "ZIF",
            "departmentName": "CRS Information/Comments",
            "traceTimestamp": "",
            "isResolved": false
          },
          {
            "traceId": 7411,
            "traceText": "OTHS NN1/Need Meal for Lunch",
            "departmentCode": "ZSV",
            "departmentName": "CRS Service Requests",
            "traceTimestamp": "",
            "isResolved": false
          }
        ],
        "checkInDate": "2019-09-25",
        "checkOutDate": "2019-09-27",
        "unresolvedTasksExists": true
      }
};

export const MOCK_RATE_DETAILS: RateCategoryDataModel = {
  checkInDate: '2019-09-25',
  checkOutDate: '2019-09-27',
  ihgRcNumber: '119200393',
  roomTypeCode: 'CSTN',
  rateCategoryCode: 'IKPCM'
};

export const MOCK_ROOM = {
  "roomStatusList":[
     {
        "roomStatus":"Clean",
        "frontOfficeStatus":"Vacant",
        "houseKeepingStatus":"Clean",
        "roomNumber":"0100",
        "roomType":"KNGN",
        "features":[
           {
              "code":"BL",
              "description":"Balcony"
           },
           {
              "code":"BR",
              "description":"Business Room"
           },
           {
              "code":"CR",
              "description":"Corner Room"
           },
           {
              "code":"HF",
              "description":"High Floor Near Elevator"
           },
           {
              "code":"CV",
              "description":"City View"
           },
           {
              "code":"GV",
              "description":"Garden View"
           },
           {
              "code":"OV",
              "description":"Ocean View"
           }
        ],
        "reservationFeatures":[
           {
              "code":"BL",
              "type":"R",
              "description":"Balcony",
              "matched":false
           },
           {
              "code":"CV",
              "type":"R",
              "description":"City View",
              "matched":true
           },
           {
              "code":"GV",
              "type":"R",
              "description":"Garden View",
              "matched":true
           },
           {
              "code":"HF",
              "type":"R",
              "description":"High Floor Near Elevator",
              "matched":true
           },
           {
              "code":"OV",
              "type":"R",
              "description":"Ocean View",
              "matched":false
           }
        ]
     },
     {
        "roomStatus":"PickUp",
        "frontOfficeStatus":"Occupied",
        "houseKeepingStatus":"PickUp",
        "roomNumber":"0101",
        "roomType":"KNGN",
        "features":[
           {
              "code":"BL",
              "description":"Balcony"
           },
           {
              "code":"OV",
              "description":"Ocean View"
           },
           {
              "code":"NE",
              "description":"Near Elevator"
           }
        ],
        "reservationFeatures":[

        ]
     },
     {
        "roomStatus":"Clean",
        "frontOfficeStatus":"Vacant",
        "houseKeepingStatus":"Clean",
        "roomNumber":"0102",
        "roomType":"KNGN",
        "features":[
           {
              "code":"KB",
              "description":"King Bed"
           }
        ],
        "reservationFeatures":[

        ]
     }
  ]
};

export const MOCK_NO_MATCHES = {
  "roomStatusList":[]
};

export const MOCK_HEARTBEAT_DATA = {"memberId":135641665,"heartBeatMyHotel":{"surveyDate":"2019-03-24","sleepIssue":"Sleep issues exists","guestLove":"R","likeComment":"Very friendly employees which feel my like at home","makeStayBetterComment":"Maybe check the Wifi connection part. It would be very helpful if the login information would be kept permanent for such recurring users as we from Canon are (Dave Hamer and Roger Erne). "},"heartBeatOtherHotel":{"hotelCode":"CODE1","surveyDate":"2019-02-12","sleepIssue":null,"guestLove":"G"}};
