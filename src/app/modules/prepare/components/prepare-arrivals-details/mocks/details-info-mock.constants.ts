export const DetailListItemMock = {
  arrivalTime: '00:00',
  assignedRoomNumber: '',
  assignedRoomStatus: null,
  badges: ['spire'],
  checkInDate: '2019-10-25',
  checkOutDate: '2019-10-26',
  companyName: 'Intercontinental Hotels Group',
  doNotMoveRoom: false,
  firstName: 'TEST USER B',
  groupName: '',
  heartBeatActions: null,
  heartBeatComment: '',
  heartBeatImprovement: '',
  ihgRcMembership: 'spire',
  ihgRcNumber: '119200393',
  infoItems: [],
  inspected: true,
  lastName: 'ANGELS',
  numberOfNights: 1,
  numberOfRooms: 1,
  pmsReservationNumber: '30902',
  pmsUniqueNumber: '30652',
  preferences: ['Balcony'],
  prepStatus: '',
  rateCategoryCode: 'IBDNG',
  reservationNumber: '21726579',
  roomTypeCode: 'KDXG',
  specialRequests: ['Need Extra Towels'],
  tasks: [],
  vipCode: 'Y',
};

export const MockGuestHeartBeat = {
  "memberId": 135641665,
  "heartBeatMyHotel": {
    "surveyDate": "2019-03-24",
    "sleepIssue": "Sleep issues exists",
    "guestLove": "R",
    "likeComment": "Very friendly employees which feel my like at home",
    "makeStayBetterComment": "Maybe check the Wifi connection part. It would be very helpful if the login information would be kept permanent for such recurring users as we from Canon are (Dave Hamer and Roger Erne). "
  },
  "heartBeatOtherHotel": {
    "hotelCode": "CODE1",
    "surveyDate": "2019-02-12",
    "sleepIssue": null,
    "guestLove": "G"
  }
}