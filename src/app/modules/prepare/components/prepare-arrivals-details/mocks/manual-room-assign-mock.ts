export const MOCK_LIST_ITEM = {
  'firstName': 'Mahesh',
  'lastName': 'B',
  'reservationNumber': 'PMS464500',
  'arrivalTime': '',
  'ihgRcNumber': '134311124',
  'ihgRcMembership': 'non',
  'vipCode': 'Y',
  'companyName': '',
  'groupName': '',
  'rateCategoryCode': 'IGCOR',
  'roomTypeCode': 'CSTN',
  'badges': [],
  'heartBeatActions': null,
  'heartBeatComment': '',
  'heartBeatImprovement': '',
  'assignedRoomNumber': '',
  'assignedRoomStatus': null,
  'numberOfRooms': 1,
  'numberOfNights': 1,
  'preferences': [],
  'specialRequests': [],
  'infoItems': [],
  'pmsReservationNumber': '18124',
  'pmsUniqueNumber': '111',
  'checkInDate': '2019-04-17',
  'checkOutDate': '2019-04-18',
  'inspected': true,
  'doNotMoveRoom': false,
  'expanded': true,
  'done': false,
  'prepStatus': 'READY',
  'showManualGrid': true,
  'tasks': [
    {
      'traceId': 443500,
      'traceText': 'Trying to chg dept',
      'departmentCode': 'XYZ',
      'departmentName': '',
      'traceTimestamp': '2019-02-28T10:16:00.0000000-05:00',
      'isResolved': false
    },
    {
      'traceId': 443502,
      'traceText': 'Trace Creation with invalid dept',
      'departmentCode': 'RPnx',
      'departmentName': '',
      'traceTimestamp': '2019-02-28T15:04:00.0000000-05:00',
      'isResolved': false
    },
    {
      'traceId': 443513,
      'traceText': 'comment on 28th ( free text 500 chars) After mod in',
      'departmentCode': 'ZIF',
      'departmentName': '',
      'traceTimestamp': '2019-03-05T15:11:41.0000000-05:00',
      'isResolved': false
    },
    {
      'traceId': 443514,
      'traceText': 'PMS More comments   Again more comments',
      'departmentCode': 'ZIF',
      'departmentName': '',
      'traceTimestamp': '2019-03-05T15:11:41.0000000-05:00',
      'isResolved': false
    }
  ],
  'disableManualAssign': true
};

export const MOCK_STATE_NAME = 'guest-list-details';

export const MOCK_REPORT_ISSUE = {
  'suggestedRoomError': {
    'function': 'Guests',
    'topic': 'Guest List - Prepare',
    'subtopic': 'Room Assignment',
    'subject': 'Error Suggested Room Assignment Load/Timeout'
  },
  'manualRoomError': {
    'function': 'Guests',
    'topic': 'Guest List - Prepare',
    'subtopic': 'Room Assignment',
    'subject': 'Error Manual Room Assignment Load/Timeout'
  },
  'changeRoomError': {
    'function': 'Guests',
    'topic': 'Change Room',
    'subtopic': 'Error Message'
  },
  'suggestedAssignRoomError': {
    'function': 'Guests',
    'topic': 'Guest List - Prepare',
    'subtopic': 'Room Assignment',
    'subject': 'Error Suggested Room Assignment Failure'
  },
  'manualRoomAssignError': {
    'function': 'Guests',
    'topic': 'Guest List - Prepare',
    'subtopic': 'Room Assignment',
    'subject': 'Error Manual Room Assignment Failure'
  }
};
