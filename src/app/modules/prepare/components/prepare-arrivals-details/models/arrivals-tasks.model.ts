import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { TasksModel } from '@modules/prepare/models/arrivals-checklist.model';

export interface DepartmentsModel {
  code: string;
  name: string;
}

export interface DepartmentsListModel {
  errors?: Array<ErrorsModel>;
  departments: Array<DepartmentsModel>;
}

export interface TraceChangeModel {
  departmentCode: string;
  traceText: string;
}

export interface TasksResponseModel {
  tasks: Array<TasksModel>;
  errors?: Array<ErrorsModel>;
}
