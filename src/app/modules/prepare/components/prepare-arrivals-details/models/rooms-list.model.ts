import { EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';
import { Subject } from 'rxjs';

export interface RoomFeature {
  code: string;
  description: string;
}

export interface ReservationFeature {
  code: string;
  type: string;
  description: string;
  matched: boolean;
}

export interface Room {
  roomNumber: string;
  roomStatus?: string;
  roomType?: string;
  houseKeepingStatus?: string;
  frontOfficeStatus?: string;
  features?: RoomFeature[];
  reservationFeatures?: ReservationFeature[];
  selected?: boolean;
  invisible?: boolean;
  statusFilter?: string;
  filterOrder?: number;
}

export interface RoomsListResponse {
  roomStatusList?: Room[];
  errors?: Error[];
}

export interface RoomInfoQueryParams {
  pmsReservationNumber: string;
  confirmationNumber: string;
  roomType: string;
  checkInDate: string;
  checkOutDate: string;
  roomNum?: string;
}

export interface RoomInfoQueryAssignReleasePayload {
  arrivalDate: string;
  checkInDate: string;
  checkOutDate: string;
  frontOfficeStatus?: string;
  houseKeepingStatus?: string;
  pmsReservationNumber: string;
  reservationFeatureTOList?: ReservationFeature[];
  reservationNumber: string;
  roomNumber: string;
  roomStatus?: string;
  roomType: string;
  freeRoomUpgradeInd?: boolean;
}

export interface ReleaseRoomParams {
  pmsReservationNumber: string;
}

export interface ReleaseRoomResponse {
  errors?: Error[];
}

export interface Error {
  code: string;
  detailText: string;
}

export interface RoomAssignmentError {
  code: string;
  errorSubject: Subject<EmitErrorModel>;
}

export interface RoomUpgradeRequestPayload {
  accessible?: boolean;
  adultsCount?: string;
  arrivalDate: string;
  childCount?: string;
  departureDate: string;
  ihgRcNumber: string;
  numberOfNights?: number;
  numberOfRooms?: number;
  rateCategory?: string;
  roomType: string;
  pmsReservationNum: string;
}

export interface RoomUpgradeQueryParameters {
  page?: number;
  reservationNumber?: string;
  upgradeLevel?: number;
}
