import { PrepareArrivalsDetailsModule } from './prepare-arrivals-details.module';

describe('PrepareArrivalsDetailsModule', () => {
  let prepareArrivalsDetailsModule: PrepareArrivalsDetailsModule;

  beforeEach(() => {
    prepareArrivalsDetailsModule = new PrepareArrivalsDetailsModule();
  });

  it('should create an instance', () => {
    expect(prepareArrivalsDetailsModule).toBeTruthy();
  });
});
