import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DropDownsModule, MultiSelectModule } from '@progress/kendo-angular-dropdowns';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';

import { AppSharedModule } from '@app/modules/shared/app-shared.module';
import { PrepareArrivalsDetailsComponent } from './components/prepare-arrivals-details.component';
import { PrepareArrivalsDetailsInfoComponent } from './components/prepare-arrivals-details-info/prepare-arrivals-details-info.component';
import { PrepareArrivalsDetailsRoomComponent } from './components/prepare-arrivals-details-room/prepare-arrivals-details-room.component';
import { PrepareArrivalsDetailsTasksComponent } from './components/prepare-arrivals-details-tasks/prepare-arrivals-details-tasks.component';
import { ManualAssignGridComponent } from './components/manual-assign-grid/manual-assign-grid.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { RoomSearchComponent } from './components/room-search/room-search.component';
import { RoomInfoComponent } from './components/room-info/room-info.component';
import { RoomActionsComponent } from './components/room-actions/room-actions.component';
import { ManualRoomAssignComponent } from './components/manual-room-assign/manual-room-assign.component';
import { RoomTypeFilterComponent } from './components/room-type-filter/room-type-filter.component';
import { HeartBeatCommentsModule } from '@modules/heart-beat-comments/heart-beat-comments.module';
import { RoomNotificationsComponent } from './components/room-notifications/room-notifications.component';
import { ShowMoreSpecialRequestsComponent } from './components/show-more-special-requests/show-more-special-requests.component';
import { PrintRecordModule } from '@app/modules/print-record/print-record.module';
import { DoNotMoveModule } from '@modules/do-not-move/do-not-move.module';
import { StatusFilterComponent } from './components/status-filter/status-filter.component';
import { GuestRelationsModule } from '@modules/guest-relations/guest-relations.module';
import { PostMasterComponent } from '@modules/prepare/components/prepare-arrivals-details/components/post-master/post-master.component';
import { RoomUpgradeComponent } from './components/room-upgrade/room-upgrade.component';


@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    BrowserAnimationsModule,
    TranslateModule,
    FormsModule,
    DropDownsModule,
    MultiSelectModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    GridModule,
    IhgNgCommonKendoModule,
    AppSharedModule,
    HeartBeatCommentsModule,
    PrintRecordModule,
    DoNotMoveModule,
    GuestRelationsModule,
    JWBootstrapSwitchModule
  ],
  declarations: [
    PrepareArrivalsDetailsComponent,
    PrepareArrivalsDetailsInfoComponent,
    PrepareArrivalsDetailsRoomComponent,
    PrepareArrivalsDetailsTasksComponent,
    ManualAssignGridComponent,
    RoomSearchComponent,
    RoomInfoComponent,
    RoomActionsComponent,
    ManualRoomAssignComponent,
    RoomTypeFilterComponent,
    RoomNotificationsComponent,
    ShowMoreSpecialRequestsComponent,
    StatusFilterComponent,
    PostMasterComponent,
    RoomUpgradeComponent
  ],
  exports: [
    PrepareArrivalsDetailsComponent,
    PrepareArrivalsDetailsRoomComponent,
    PrepareArrivalsDetailsTasksComponent,
    PrepareArrivalsDetailsInfoComponent,
    ManualAssignGridComponent
  ],
  providers: [
  ],
  entryComponents: [
    ShowMoreSpecialRequestsComponent
  ]
})
export class PrepareArrivalsDetailsModule {
}
