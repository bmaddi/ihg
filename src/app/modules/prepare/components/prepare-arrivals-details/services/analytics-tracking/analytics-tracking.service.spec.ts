import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { AnalyticsTrackingService } from './analytics-tracking.service';
import { RM_ASGNMNT_ERR_CODES } from '@app/modules/prepare/components/prepare-arrivals-details/constants/room-assignment-error-code.constant';
import { PrepareForArrivalsGaConstants } from '@app/modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { ACTION, TRACK_ERROR_LABELS } from '../../../../mocks/analytics-tracking-service-mock';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, button: string, selected: string) { }
}

describe('AnalyticsTrackingService', () => {

  let service: AnalyticsTrackingService;
  let httpMockController: HttpTestingController;
  let googleAnalyticsService: GoogleAnalyticsService;
  const stateName = 'check-in';

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        providers: [{ provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService }],
        imports: [HttpClientTestingModule]
      });
    service = TestBed.get(AnalyticsTrackingService);
    httpMockController = TestBed.get(HttpTestingController);
    googleAnalyticsService = TestBed.get(GoogleAnalyticsService);
  });

  function checkErrorTrackingLabels(isRoomUpgrade: boolean) {
    const trackEventSpy = spyOn(googleAnalyticsService, 'trackEvent');
    Object.values(RM_ASGNMNT_ERR_CODES).forEach(val => {      
      service.trackRoomAssignmentError(stateName, false, val, isRoomUpgrade);
      const expectedLabel = isRoomUpgrade ? TRACK_ERROR_LABELS[val].labels.RoomUpgrade : TRACK_ERROR_LABELS[val].labels.NonRoomUpgrade;
      const expectedAction = PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT;
      expect(trackEventSpy).toHaveBeenCalledWith(service['getCategory'](stateName), expectedAction, expectedLabel);
    });
  }

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("check if proper label is getting send during room Upgrade", () => {
    checkErrorTrackingLabels(true);
  });

  it("check if proper label is getting send during normal room assignment", () => {
    checkErrorTrackingLabels(false);
  });

  it("check if proper label is getting set in unknown error code scenario during manual room assignment", () => {
    const trackEventSpy = spyOn(googleAnalyticsService, 'trackEvent');
    service.trackRoomAssignmentError(stateName, true, "ERR-305", false);
    const expectedAction = ACTION.manualRoomAssign;
    const expectedLabel = TRACK_ERROR_LABELS.Default.labels.manualAssign;
    expect(trackEventSpy).toHaveBeenCalledWith(service['getCategory'](stateName), expectedAction, expectedLabel);
  });

  it("check if proper label is getting set in unknown error code scenario during room upgrade", () => {
    const trackEventSpy = spyOn(googleAnalyticsService, 'trackEvent');
    service.trackRoomAssignmentError(stateName, false, "ERR-305", true);
    const expectedAction = ACTION.roomAssign;
    const expectedLabel = TRACK_ERROR_LABELS.Default.labels.RoomUpgrade;
    expect(trackEventSpy).toHaveBeenCalledWith(service['getCategory'](stateName), expectedAction, expectedLabel);
  });

  it("check if proper label is getting set in unknown error code scenario during room assignment", () => {
    const trackEventSpy = spyOn(googleAnalyticsService, 'trackEvent');
    service.trackRoomAssignmentError(stateName, false, "ERR-305", false);
    const expectedAction = ACTION.roomAssign;
    const expectedLabel = TRACK_ERROR_LABELS.Default.labels.NonRoomUpgrade;
    expect(trackEventSpy).toHaveBeenCalledWith(service['getCategory'](stateName), expectedAction, expectedLabel);
  });

});
