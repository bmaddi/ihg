import { Injectable } from '@angular/core';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { CheckinAnalyticsConstants } from '@app/modules/check-in/enums/checkin-analytics-constants.enum';
import { ManageStayEnumConstants } from '@app/modules/manage-stay/enums/manage-stay-constants.enum';
import { RM_ASGNMNT_ERR_CODES } from '@modules/prepare/components/prepare-arrivals-details/constants/room-assignment-error-code.constant';
@Injectable({
  providedIn: 'root'
})
export class AnalyticsTrackingService {
  private readonly categoryMap = {
    'guest-list-details': PrepareForArrivalsGaConstants.CATEGORY,
    'check-in': CheckinAnalyticsConstants.CATEGORY_CHECK_IN,
    'walk-in': PrepareForArrivalsGaConstants.CATEGORY_WALK_IN,
    'manage-stay': ManageStayEnumConstants.CATEGORY_IN_HOUSE
  };

  constructor(private gaService: GoogleAnalyticsService) {
  }

  private getCategory(stateName: string): any {
    return this.categoryMap[stateName];
  }

  public trackClickChangeRoom(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.MDFY_ASGND_RM);
  }

  public trackAssignSuggestedRoom(stateName: string, suggestionNumber: number): void {
    const category = this.getCategory(stateName);
    let label: string;
    switch (suggestionNumber) {
      case 1:
        label = PrepareForArrivalsGaConstants.ASGN_RM_FIRST_SUGGESTED;
        break;
      case 2:
        label = PrepareForArrivalsGaConstants.ASGN_RM_SECOND_SUGGESTED;
        break;
      case 3:
        label = PrepareForArrivalsGaConstants.ASGN_RM_THIRD_SUGGESTED;
        break;
    }
    if (label) {
      this.gaService.trackEvent(category,
        PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
        label);
    }
  }

  public trackOpenManualAssignment(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.OPN_MANUAL_ASGN_WNDW);
  }

  public trackAssignRoomManually(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.ASGN_RM_MANUALLY);
  }

  public trackClickProfileIcon(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_VIEW_GUEST_PROFILE,
      PrepareForArrivalsGaConstants.SELECTED);
  }

  public trackClickToggleTasks(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.GA_TASKS,
      PrepareForArrivalsGaConstants.GA_EXP_COLLAPSE);
  }

  public trackPmsConfNumUnavailable(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPAND_ROW,
      PrepareForArrivalsGaConstants.LBL_PMS_UNAVAILABLE);
  }

  public trackExpandRowPmsAvlb(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPAND_ROW,
      PrepareForArrivalsGaConstants.LBL_ROW_EXPANDED);
  }

  public trackClickAddTask(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.GA_TASKS,
      PrepareForArrivalsGaConstants.GA_ADD_NW_TASK);
  }

  public trackSelectTaskDepartment(stateName: string, departmentCode: string): void {
    const category = this.getCategory(stateName);
    const label = `${PrepareForArrivalsGaConstants.SELECT_DEPT} - ${departmentCode}`;
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.GA_TASKS,
      label);
  }

  public trackClickSendTrace(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.GA_TASKS,
      PrepareForArrivalsGaConstants.GA_TRACE_BTN);
  }

  public trackClickDeleteTrace(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.GA_TASKS,
      PrepareForArrivalsGaConstants.GA_DELETE_BTN);
  }

  public trackClickTasksDone(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category,
      PrepareForArrivalsGaConstants.GA_TASKS,
      PrepareForArrivalsGaConstants.GA_DONE_BTN);
  }

  public trackSuggestedRoomError(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.GA_LBL_ERR_SUG_MTCH_LOAD);
  }

  public trackManualRoomLoadError(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.GA_ACN_MAN_RM_ASGN,
      PrepareForArrivalsGaConstants.GA_LBL_ERR_MAN_RM_LOAD);
  }

  public trackRoomInspectionWarning(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.ACTION_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.GA_LBL_RM_INSP_WARNG);
  }

  public trackTasksLoadError(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.GA_TASKS, PrepareForArrivalsGaConstants.GA_LBL_ERR_LOAD_TSK_LST);
  }

  public trackSendTaskError(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.GA_TASKS, PrepareForArrivalsGaConstants.GA_LBL_ERR_SND_TSK_PMS);
  }

  public trackChangeRoomInModalWarning(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.CHNG_ROOM_IN_WRNG);
  }

  public trackChangeRoomBeforeModalWarning(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.CHNG_ROOM_BFR_WRNG);
  }

  trackRoomAssignmentError(stateName: string, manualAssign: boolean, errorCode?: string,
    isUpgrade?: boolean): void {
    const category = this.getCategory(stateName);
    const action = manualAssign ? PrepareForArrivalsGaConstants.GA_ACN_MAN_RM_ASGN :
      PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT;
    let label: string;
    switch (errorCode) {
      case RM_ASGNMNT_ERR_CODES.roomUnavailable:
        label = isUpgrade ? PrepareForArrivalsGaConstants.FREE_UPGRDE_ERROR :
          PrepareForArrivalsGaConstants.GA_LBL_ERR_RM_NOT_AVAIL;
        break;
      case RM_ASGNMNT_ERR_CODES.inventoryUnavailable:
        label = isUpgrade ? PrepareForArrivalsGaConstants.FREE_UPGRADE_INV_UNAVLBL :
          PrepareForArrivalsGaConstants.GA_LBL_ERR_INV_NOT_AVAIL;
        break;
      case RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp:
        label = isUpgrade ? PrepareForArrivalsGaConstants.FREE_UPGRDE_INV_UNAVLBL_GRP :
          PrepareForArrivalsGaConstants.GA_LBL_ERR_INV_NOT_AVAIL_GRP;
        break;
      case RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse:
        label = isUpgrade ? PrepareForArrivalsGaConstants.FREE_UPGRDE_INV_UNAVLBL_HOUSE :
          PrepareForArrivalsGaConstants.GA_LBL_ERR_INV_NOT_AVAIL_HOUSE;
        break;
      default:
        label = manualAssign
          ? PrepareForArrivalsGaConstants.GA_LBL_ERR_FAIL_ASGN_RM_MAN
          : isUpgrade
            ? PrepareForArrivalsGaConstants.FREE_UPGRDE_ERROR
            : PrepareForArrivalsGaConstants.GA_LBL_ERR_FAIL_ASGN_SUG_MTCH;
    }
    this.gaService.trackEvent(category, action, label);
  }

  public trackSelectFreeUpgrade(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT, PrepareForArrivalsGaConstants.SLCT_FREE_UPGRD);
  }

  public checkFreeRoomUpgrade(isUpgradeSelected: boolean, stateName: string) {
    if (isUpgradeSelected) {
      this.trackSelectFreeUpgradeSuccess(stateName);
    }
  }

  private trackSelectFreeUpgradeSuccess(stateName: string): void {
    const category = this.getCategory(stateName);
    this.gaService.trackEvent(category, PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT, PrepareForArrivalsGaConstants.FREE_UPGRDE_SUCCESS);
  }
}