import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UserService , DetachedToastMessageService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { RoomsService } from './rooms.service';
import { TranslateService } from '@ngx-translate/core';
import { mockUser, translateServiceStub } from '@app/constants/app-test-constants';
import { STATE_PARAMS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';

describe('RoomsService', () => {

  let service: RoomsService;
  let httpMockController: HttpTestingController;

  beforeEach(() => {
    TestBed
      .overrideProvider(TranslateService, {
        useValue: translateServiceStub
      })
      .overrideProvider(DetachedToastMessageService, {
        useValue: {
          success: (s1, s2): void => {
          }
        }
      })
      .configureTestingModule({
        providers: [UserService, ConfirmationModalService, TranslateService, DetachedToastMessageService],
        imports: [HttpClientTestingModule]
      });
    service = TestBed.get(RoomsService);
    httpMockController = TestBed.get(HttpTestingController);
    TestBed.get(UserService).setUser(mockUser);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return false if user does not have proper room settings role for Do Not Move', () => {
    expect(service['getLocationId']()).toEqual('ATLCP');
    expect(service.roomSettingsEnabled(false)).toBeFalsy();
    expect(service.roomSettingsEnabled(true)).toBeFalsy();
    expect(service.roomSettingsEnabled(null)).toBeFalsy();
    expect(service.roomSettingsEnabled(undefined)).toBeFalsy();
  });

  it('should return true if user has proper room settings role for Do Not Move', () => {
    spyOn(TestBed.get(UserService), 'getUser').and.returnValue({
      'roles': [
        {
          id: 5004,
          name: 'FRONT_DESK_MANAGEMENT',
          description: 'FRONT_DESK_MANAGEMENT',
          functions: null
        }]
    });
    expect(service.roomSettingsEnabled(false)).toBeTruthy();
    expect(service.roomSettingsEnabled(true)).toBeTruthy();
    expect(service.roomSettingsEnabled(null)).toBeFalsy();
    expect(service.roomSettingsEnabled(undefined)).toBeFalsy();
  });

  // tslint:disable-next-line:max-line-length
  it('should check eligibility map with guest loyalty statuses "none matching brand codes" #isEntitledForUpgrade and return correct upgrade eligibility boolean', () => {
    const currentLocation = mockUser.user.currentLocation;
    spyOn(TestBed.get(UserService), 'getCurrentLocation').and.returnValue(currentLocation);
    const scenarios = [
      {scenario: null, expectation: false},
      {scenario: undefined, expectation: false},
      {scenario: [], expectation: false},
      {scenario: ['spire'], expectation: true},
      {scenario: ['karma'], expectation: false},
      {scenario: ['ambassador'], expectation: false},
      {scenario: ['innercircle'], expectation: false},
      {scenario: ['royalambassador'], expectation: false},
      {scenario: ['gold', 'club'], expectation: false},
      {scenario: ['gold', 'club', 'spire'], expectation: true},
      {scenario: ['gold', 'club', 'platinum'], expectation: true},
      {scenario: ['gold', 'spire', 'platinum'], expectation: true}
    ];

    scenarios.forEach(s => expect(service.isEntitledForUpgrade(s.scenario)).toEqual(s.expectation));
  });

  // tslint:disable-next-line:max-line-length
  it('should check eligibility map with guest loyalty statuses "matching entitled brand codes Intercontinental" #isEntitledForUpgrade and return correct upgrade eligibility boolean', () => {
    spyOn(TestBed.get(UserService), 'getCurrentLocation').and.returnValue({...mockUser.user.currentLocation, brandCode: 'ICON'});
    const scenarios = [
      {scenario: ['spire'], expectation: true},
      {scenario: ['karma'], expectation: false},
      {scenario: ['ambassador'], expectation: true},
      {scenario: ['innercircle'], expectation: false},
      {scenario: ['royalambassador'], expectation: true},
      {scenario: ['gold', 'club'], expectation: false},
      {scenario: ['gold', 'club', 'spire'], expectation: true},
      {scenario: ['gold', 'club', 'platinum'], expectation: true},
      {scenario: ['gold', 'spire', 'platinum'], expectation: true}
    ];

    scenarios.forEach(s => expect(service.isEntitledForUpgrade(s.scenario)).toEqual(s.expectation));
  });

  // tslint:disable-next-line:max-line-length
  it('should check eligibility map with guest loyalty statuses "matching entitled brand codes KIMPTON" #isEntitledForUpgrade and return correct upgrade eligibility boolean', () => {
    spyOn(TestBed.get(UserService), 'getCurrentLocation').and.returnValue({...mockUser.user.currentLocation, brandCode: 'KIKI'});
    const scenarios = [
      {scenario: ['ambassador'], expectation: false},
      {scenario: ['innercircle'], expectation: true},
      {scenario: ['royalambassador'], expectation: false},
      {scenario: ['gold', 'club'], expectation: false}
    ];

    scenarios.forEach(s => expect(service.isEntitledForUpgrade(s.scenario)).toEqual(s.expectation));
  });

  // tslint:disable-next-line:max-line-length
  it('should check for proper level upgrade based on loyalty entitlement with guest loyalty statuses #getUpgradeLevel and return correct upgrade level', () => {
    spyOn(TestBed.get(UserService), 'getCurrentLocation').and.returnValue({...mockUser.user.currentLocation, brandCode: 'ICON'});
    const scenarios = [
      {scenario: ['spire'], expectation: 1},
      {scenario: ['ambassador'], expectation: 1},
      {scenario: ['innercircle', 'ambassador'], expectation: 1},
      {scenario: ['royalambassador'], expectation: 2},
      {scenario: ['gold', 'club', 'spire'], expectation: 1},
      {scenario: ['gold', 'club', 'platinum'], expectation: 1},
      {scenario: ['gold', 'spire', 'platinum'], expectation: 1}
    ];

    scenarios.forEach(s => expect(service.getUpgradeLevel(s.scenario)).toEqual(s.expectation));
  });

  it('should return dirty rooms as well for inHouse page', () => {
    expect(service['getLocationId']()).toEqual('ATLCP');
    const paramsSpy = spyOn(TestBed.get(RoomsService), 'getRoomsServiceParams').and.returnValue
    (`roomType=TSTG&checkInDate=06-02-20&checkOutDate=20-02-20}&page=inHouse}`);
     const getAvailableRoomsSpy = spyOn(TestBed.get(RoomsService), 'getRooms').and.returnValue({
      'roomStatusList':
      [{
        'roomStatus': 'Inspected',
        'frontOfficeStatus': 'Vacant',
        'houseKeepingStatus': 'Inspected',
        'roomNumber': '2011',
        'roomType': 'TSTG',
        'features': [],
        'reservationFeatures': []
      }, {
        'roomStatus': 'Dirty',
        'frontOfficeStatus': 'Vacant',
        'houseKeepingStatus': 'Dirty',
        'roomNumber': '1011',
        'roomType': 'TSTG',
        'features': [],
        'reservationFeatures': []
      }]
    }
     );
    service.getRooms(listItemMock, 'manage-stay', false);
    expect(getAvailableRoomsSpy).toHaveBeenCalled();
  });

});
