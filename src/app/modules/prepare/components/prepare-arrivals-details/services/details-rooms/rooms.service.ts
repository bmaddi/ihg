import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { get, has, isNil, max } from 'lodash';

import { Alert, AlertType, DetachedToastMessageService, UserConfirmationModalConfig, UserService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { environment } from '@env/environment';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ErrorToastMessageTranslations } from '@app/modules/shared/components/error-toast-message/error-toast-message.component';
import { ROOM_STATUS, STATE_PARAMS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { hasDoNotPostAccessRoles } from '@modules/do-not-move/constants/do-not-move.constants';
import { eligibilityMap } from '@modules/prepare/components/prepare-arrivals-details/constants/room-upgrade.constants';
import {
  ReleaseRoomResponse,
  Room,
  RoomInfoQueryAssignReleasePayload,
  RoomInfoQueryParams,
  RoomsListResponse,
  RoomAssignmentError,
  RoomUpgradeQueryParameters,
  RoomUpgradeRequestPayload
} from '../../models/rooms-list.model';
import {
  RM_ASGNMNT_ERR_CODES,
  RM_ERR_SLNM_ID
} from '@modules/prepare/components/prepare-arrivals-details/constants/room-assignment-error-code.constant';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  readonly roomAssignmentDefinedErrorCodes = RM_ASGNMNT_ERR_CODES;
  public houseKeepingStatusEvent = new Subject<string>();
  private readonly API_URL = environment.fdkAPI;
  private roomInfo: Room;
  private roomSubject = new Subject<Room>();
  private listnerDnmReasonErrorCheckInPage = new Subject<null>();
  private listnerDnmReasonErrorRetryCheckInPage = new Subject<null>();
  private listnerDnmReasonAddedCheckInPage = new Subject<null>();

  public listenDnmErrorCheckInPage(): Observable<null> {
    return this.listnerDnmReasonErrorCheckInPage.asObservable();
  }

  public emitDnmErrorCheckInPage() {
    this.listnerDnmReasonErrorCheckInPage.next();
  }

  public listenDnmErrorRetryCheckInPage(): Observable<null> {
    return this.listnerDnmReasonErrorRetryCheckInPage.asObservable();
  }

  public emitDnmErrorRetryCheckInPage() {
    this.listnerDnmReasonErrorRetryCheckInPage.next();
  }

  public listenDnmAddedCheckInPage(): Observable<null> {
    return this.listnerDnmReasonAddedCheckInPage.asObservable();
  }

  public emitDnmAddedCheckInPage() {
    this.listnerDnmReasonAddedCheckInPage.next();
  }

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private translate: TranslateService,
    private modalService: ConfirmationModalService,
    private toastMessageService: DetachedToastMessageService) {
  }

  public getRooms(listItem: ArrivalsListModel, stateName: string, displaySpinner = false): Observable<RoomsListResponse> {
    const apiURl = `${this.API_URL}rooms/available/${this.getLocationId()}?${this.getRoomsServiceParams(listItem, stateName)}`;
    const httpOptions = { headers: new HttpHeaders({ spinnerConfig: displaySpinner ? 'Y' : 'N' }) };
    return this.http.get(apiURl, httpOptions).pipe(map((response: RoomsListResponse) => {
      return response;
    }),
      catchError((error: any) => {
        return throwError(error);
      })
    );
  }

  private getRoomsServiceParams(listItem: ArrivalsListModel, stateName: string): string {
    const roomType = listItem.roomTypeCode;
    const checkInDate = listItem.checkInDate;
    const checkOutDate = listItem.checkOutDate;
    return `roomType=${roomType}&checkInDate=${checkInDate}&checkOutDate=${checkOutDate}&page=${STATE_PARAMS[stateName]}`;
  }

  public getSuggestedRooms(listItem: ArrivalsListModel, stateName: string): Observable<RoomsListResponse> {
    const queryParams = this.getParams(listItem, null, this.checkForManageStay(stateName));
    const url = `${this.API_URL}rooms/autoSuggestedMatch/${this.getLocationId()}`;
    return this.getSuggestedMatches(url, queryParams, true);
  }

  // To display Vacant/Clean or Vacant/Inspected rooms only similar to checkIn page for In House Manage Stay
  private checkForManageStay(stateName: string): string {
    if (stateName === AppStateNames.manageStay) {
      stateName = AppStateNames.checkIn;
    }
    return stateName;
  }

  private getSuggestedMatches(url: string, queryParams, storeRoomInfo?: boolean): Observable<RoomsListResponse> {
    const params = new HttpParams({ fromObject: queryParams });
    return this.http.get(url, { params }).pipe(
      map((response: RoomsListResponse) => {
        if (response && response.roomStatusList) {
          const suggestedRooms: Room[] = response.roomStatusList;
          if (storeRoomInfo) {
            this.storeRoomInfo(suggestedRooms[0]);
          }
        }
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getRoomReleased(queryParams): Observable<ReleaseRoomResponse> {
    const url = `${this.API_URL}rooms/releaseRoom/${this.getLocationId()}`;
    return this.http.put(url, queryParams).pipe(
      map((response: RoomsListResponse) => response),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getRoomUpgrade(params: RoomUpgradeQueryParameters, payload: RoomUpgradeRequestPayload): Observable<RoomsListResponse> {
    const url = `${this.API_URL}rooms/freeUpgrades/${this.getLocationId()}`;
    return this.http.post(url, payload, {
      params: params as {
        [param: string]: string;
      }
    }).pipe(
      map((response: RoomsListResponse) => response),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private storeRoomInfo(roomInfo: Room): void {
    this.roomInfo = roomInfo;
  }

  public getStoredRoomInfo(): Room {
    return this.roomInfo;
  }

  public assignRoom(queryParams, freeRoomUpgradeInd = false): Observable<RoomsListResponse> {
    const url = `${this.API_URL}rooms/assignRoom/${this.getLocationId()}`;
    return this.http.put(url, queryParams, {
      params: {
        freeRoomUpgradeInd
      } as any
    }).pipe(
      map((response: RoomsListResponse) => response),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  getRoom(): Observable<Room> {
    return this.roomSubject.asObservable();
  }

  setRoom(room: Room): void {
    this.roomSubject.next(room);
  }

  isRoomAsignmentDefinedError(data: RoomsListResponse): boolean {
    return data.errors && Object.values(this.roomAssignmentDefinedErrorCodes).includes(data.errors[0].code);
  }

  getRoomAssignmentDefinedErrors(room: Room, errorCode: string): { toast: Alert, slnmId: string } {
    let error;
    let slnmId: string;
    switch (errorCode) {
      case this.roomAssignmentDefinedErrorCodes.roomUnavailable:
        error = {
          detailMessage: 'LBL_ERR_RM_UNAVLBL',
          translateValues: { detailMessage: { roomNo: room.roomNumber } }
        };
        slnmId = RM_ERR_SLNM_ID[this.roomAssignmentDefinedErrorCodes.roomUnavailable];
        break;
      case this.roomAssignmentDefinedErrorCodes.inventoryUnavailable:
        error = {
          detailMessage: 'LBL_NO_INVNTRY_AVLBL',
          translateValues: { detailMessage: { roomTyp: room.roomType } }
        };
        slnmId = RM_ERR_SLNM_ID[this.roomAssignmentDefinedErrorCodes.inventoryUnavailable];
        break;
      case this.roomAssignmentDefinedErrorCodes.inventoryUnavailableHouse:
        error = {
          detailMessage: 'LBL_NO_INVNTRY'
        };
        slnmId = RM_ERR_SLNM_ID[this.roomAssignmentDefinedErrorCodes.inventoryUnavailableHouse];
        break;
      case this.roomAssignmentDefinedErrorCodes.inventoryUnavailableGrp:
        error = {
          detailMessage: 'LBL_NO_INVNTRY_AVLBL_GRP',
          translateValues: { detailMessage: { roomTyp: room.roomType } }
        };
        slnmId = RM_ERR_SLNM_ID[this.roomAssignmentDefinedErrorCodes.inventoryUnavailableGrp];
        break;
    }
    return {
      toast: {
        type: AlertType.Danger,
        message: '',
        dismissible: false,
        ...error
      },
      slnmId: slnmId
    };
  }

  getDNMAddReasonErrorContent(): Partial<ErrorToastMessageTranslations> {
    return {
      retryButton: 'LBL_ERR_TRYAGAIN',
      reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
      generalError: {
        message: 'COM_TOAST_ERR',
        detailMessage: 'LBL_ERR_ADD_REASON'
      }
    };
  }

  getDNMSwitchErrorContent(): Partial<ErrorToastMessageTranslations> {
    return {
      retryButton: 'LBL_ERR_TRYAGAIN',
      reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
      generalError: {
        message: 'COM_TOAST_ERR',
        detailMessage: 'LBL_DO_NOT_MOVE_CHANGES_FAIL'
      }
    };
  }

  getParams(listItem: ArrivalsListModel, room?: Room, stateName?: string): RoomInfoQueryParams {
    return <RoomInfoQueryParams>{
      pmsReservationNumber: listItem.pmsReservationNumber,
      confirmationNumber: listItem.reservationNumber,
      roomType: listItem.roomTypeCode,
      checkInDate: listItem.checkInDate,
      checkOutDate: listItem.checkOutDate,
      roomNum: room ? room.roomNumber : null,
      page: stateName ? STATE_PARAMS[stateName] : null
    };
  }

  getAssignReleasePayload(listItem: ArrivalsListModel, room?: Room, isUpgrade = false): RoomInfoQueryAssignReleasePayload {
    return <RoomInfoQueryAssignReleasePayload>{
      arrivalDate: listItem.arrivalTime,
      checkInDate: listItem.checkInDate,
      checkOutDate: listItem.checkOutDate,
      frontOfficeStatus: room ? room.frontOfficeStatus : null,
      houseKeepingStatus: room ? room.houseKeepingStatus : null,
      pmsReservationNumber: listItem.pmsReservationNumber,
      reservationFeatureTOList: room && room.reservationFeatures ? room.reservationFeatures : null,
      reservationNumber: listItem.reservationNumber,
      roomNumber: room ? room.roomNumber : null,
      roomStatus: room ? room.roomStatus : null,
      roomType: room.roomType,
      freeRoomUpgradeInd: isUpgrade
    };
  }

  roomSettingsEnabled(doNotMoveFlag: boolean): boolean {
    const roles = get(this.userService.getUser(), 'roles', []);
    return hasDoNotPostAccessRoles(roles) &&
      !isNil(doNotMoveFlag);
  }

  public openRoomInspectionWarningModal(room: Room) {
    return this.modalService.openConfirmationModal(this.getConfirmModalConfig(room));
  }

  showSuccessfulRoomChangeMessage(room: string) {
    this.toastMessageService.success('LBL_RM_CHANGE', this.translate.instant('LBL_KEYS_CAN_NOW_CUT', { room: room }));
  }

  private getConfirmModalConfig(room: Room): UserConfirmationModalConfig {
    return {
      modalHeader: 'LBL_MOD_TTL_RM_NT_INS',
      primaryButtonLabel: 'COM_BTN_ASSIGN',
      primaryButtonClass: 'btn-primary',
      defaultButtonLabel: 'COM_BTN_CANCEL',
      defaultButtonClass: 'btn-default',
      messageHeader: this.translate.instant('LBL_MOD_TXT_RM_NT_INS', { roomNo: room.roomNumber }),
      messageBody: '',
      windowClass: 'modal-medium',
      dismissible: true
    };
  }

  public isRoomReady(listItem: ArrivalsListModel): boolean {
    return this.isRoomVacant(listItem) && this.isRoomCleanOrInspected(listItem);
  }

  private isRoomVacant(listItem: ArrivalsListModel): boolean {
    const status = listItem.assignedRoomStatus;
    return status ? status.frontOfficeStatus === ROOM_STATUS.Vacant.value : false;
  }

  private isRoomCleanOrInspected(listItem: ArrivalsListModel): boolean {
    if (listItem.assignedRoomStatus && listItem.inspected) {
      return listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value;
    }
    return listItem.assignedRoomStatus && (listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Clean.value
      || listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value);
  }

  public isEntitledForUpgrade(loyaltyStatuses: string[]): boolean {
    if (loyaltyStatuses && loyaltyStatuses.length) {
      return loyaltyStatuses.some(loyalty => this.checkLoyaltyUpgradeEntitlement(loyalty));
    }
    return false;
  }

  private checkLoyaltyUpgradeEntitlement(loyalty: string): boolean {
    if (has(eligibilityMap, loyalty)) {
      const brandEntitlements = get(eligibilityMap, `${loyalty}.brandEntitlements`, null),
        hotelBrandCode = get(this.userService.getCurrentLocation(), 'brandCode', null);
      return brandEntitlements ? brandEntitlements.some(hc => hc === hotelBrandCode) : true;
    }
    return false;
  }

  public getUpgradeEligibilityMessage(): Alert {
    return {
      type: AlertType.Info,
      message: 'LBL_NOTE_PMS_UNAVAILABLE',
      detailMessage: 'LBL_GST_ENTL_UPG',
      dismissible: false
    };
  }

  public getRoomUpgradedMessage(): Alert {
    return {
      type: AlertType.Success,
      message: '',
      detailMessage: 'LBL_RM_UPG_SUCCESS',
      dismissible: false
    };
  }

  public getUpgradeLevel(loyaltyStatuses: string[]): number {
    const upgradeLevels = loyaltyStatuses
      .filter(s => this.checkLoyaltyUpgradeEntitlement(s))
      .map(status => get(eligibilityMap, `${status}.level`, null));
    return max(upgradeLevels);
  }

  public saveRoomStatus(roomNumber: string, roomStatus: string): Observable<string> {
    const url = `${this.API_URL}rooms/updateHouseKeepingStatus/${this.getLocationId()}`;
    return this.http.put(url, {roomNumber, roomStatus}).pipe(
      map((response: string) => response),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}

