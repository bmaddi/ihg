import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, of, throwError, Subject } from 'rxjs';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { DepartmentsListModel, TasksResponseModel, TraceChangeModel } from '../../models/arrivals-tasks.model';
import { ArrivalsListModel, TasksModel } from '@modules/prepare/models/arrivals-checklist.model';

@Injectable({
  providedIn: 'root'
})
export class PrepareArrivalsDetailsTasksService {
  private readonly API_URL = environment.fdkAPI;
  private viewTaskSubject = new Subject<void>();
  public departmentsList: DepartmentsListModel;

  constructor(private http: HttpClient, private userService: UserService) {
  }

  public getDepartmentsList(displaySpinner = false): Observable<DepartmentsListModel> {
    if (!this.departmentsList || !this.departmentsList.departments) {
      const apiURl = `${this.API_URL}traces/departments/${this.getLocationId()}`;
      const httpOptions = { headers: new HttpHeaders({ spinnerConfig: displaySpinner ? 'Y' : 'N' }) };
      return this.http.get(apiURl, httpOptions).pipe(map((response: DepartmentsListModel) => {
          this.handleDepartmentsResponse(response);
          return response;
        }),
        catchError((error: HttpErrorResponse) => {
          return throwError(error);
        })
      );
    }
    return of(this.departmentsList);
  }

  private handleDepartmentsResponse(response: DepartmentsListModel): void {
    if (response && response.departments && response.departments.length) {
      this.departmentsList = response;
    }
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public fetchTraces(listItem: ArrivalsListModel): Observable<TasksResponseModel> {
    const url = `${this.API_URL}traces/fetch/${this.getLocationId()}?pmsReservationNumber=${listItem.pmsReservationNumber}`;
    return this.http.get(url).pipe(map((response: TasksResponseModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }));
  }

  public addTrace(listItem: ArrivalsListModel, taskItem: TasksModel): Observable<TasksResponseModel> {
    const url = `${this.API_URL}traces/add/${this.getLocationId()}?pmsReservationNumber=${listItem.pmsReservationNumber}`;
    const payload = this.getTracePayload(taskItem);
    return this.http.put(url, payload).pipe(map((response: TasksResponseModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getTracePayload(taskItem: TasksModel): TraceChangeModel {
    return {
      departmentCode: taskItem.departmentCode,
      traceText: taskItem.traceText
    };
  }

  getViewTaskAction(): Observable<void> {
    return this.viewTaskSubject.asObservable();
  }

  emitViewTaskAction(): void {
    this.viewTaskSubject.next();
  }
}
