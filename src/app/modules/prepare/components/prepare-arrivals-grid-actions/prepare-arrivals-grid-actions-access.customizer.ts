import { TemplateRef, ViewContainerRef } from '@angular/core';
import { AccessControlPermission, AccessControlPermissionType } from 'ihg-ng-common-core';

export class PrepareArrivalsGridActionsCustomizer {

    static notifyCustomize(templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        accessControlItem: AccessControlPermission,
        alternateRef: TemplateRef<any>) {
        if (!accessControlItem) { viewContainer.createEmbeddedView(templateRef); return; }

        if (accessControlItem.access === AccessControlPermissionType.ReadWrite) {
            viewContainer.createEmbeddedView(templateRef);
        } else if (accessControlItem.access === AccessControlPermissionType.ReadOnly || accessControlItem.access === AccessControlPermissionType.NoAccess) {
            viewContainer.createEmbeddedView(alternateRef);
        }
    }
}