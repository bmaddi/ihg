import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule, TranslateService, TranslateFakeLoader } from '@ngx-translate/core';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs/internal/observable/of';
import {  By } from '@angular/platform-browser';

import {  GoogleAnalyticsService, EnvironmentService, UserService, UserDetailsData, AccessControlModule, AccessControlService, AccessControlPermission} from 'ihg-ng-common-core';

import { ServiceErrorComponent } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { PrepareArrivalsGridActionsComponent } from './prepare-arrivals-grid-actions.component';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { GuestNotificationService } from '@app/modules/prepare/services/guest-notification/guest-notification.service';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { NotifyGuestModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ReportIssueService } from 'ihg-ng-common-pages';

describe('PrepareArrivalsGridActionsComponent', () => {
  let component: PrepareArrivalsGridActionsComponent;
  let fixture: ComponentFixture<PrepareArrivalsGridActionsComponent>;
  let mockGoogleAnalyticsService;
  let mockArrivalService;
  let mockRoomService;
  let mockGuestNotificationService;
  let mockEnvironmentService;
  let mockUserService;
  let mockAccessControlService;

  const translations = {
    LBL_READ_ONLY_PERMISSION: 'You do not have permission to perform this action.'
  };

  beforeEach(async(() => {

    mockGoogleAnalyticsService = jasmine.createSpyObj(['']);
    mockArrivalService = jasmine.createSpyObj(['handleUnsavedTasks']);
    mockRoomService = jasmine.createSpyObj(['getRoom']);
    mockRoomService.getRoom.and.returnValue(of({}));
    mockEnvironmentService = jasmine.createSpyObj(['getApplCd', 'getAppLinks', 'getAppLogoutUrl', 'getApiUrl']);
    mockGuestNotificationService = jasmine.createSpyObj(['getGuestNotifiedAction', 'notifyGuest', 'emitNotifyGuestStatus']);
    mockEnvironmentService.getApplCd.and.returnValue('');
    mockUserService = jasmine.createSpyObj(['getUserDetails', 'hasPortfolioAccess']);
    mockUserService.getUserDetails.and.returnValue(of(<UserDetailsData>{ user: {} }));
    mockGuestNotificationService.getGuestNotifiedAction.and.returnValue(of(<NotifyGuestModel[]>[{actionItemCode: 'string',ihgRcNumber: 'string', reservationNumber: '12'}]));
    mockAccessControlService = jasmine.createSpyObj(['getAccessControlPermissions']);
    mockAccessControlService.accessControlPermissions = [{'name': 'Add Task', 'tagName': 'add_task', 'type': 'C', 'access': 'W'}];
    let translateService: TranslateService;

  TestBed.configureTestingModule({
      declarations: [ PrepareArrivalsGridActionsComponent, ServiceErrorComponent ],
      imports: [
        TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: TranslateFakeLoader } }),
        NgbModule,
        HttpClientTestingModule,
        CommonModule,
        AccessControlModule],
        providers: [
                    { provide: GoogleAnalyticsService, useValue: mockGoogleAnalyticsService },
                    { provide: ArrivalsService, useValue: mockArrivalService},
                    { provide: RoomsService, useValue: mockRoomService},
                    { provide: GuestNotificationService, useValue: mockGuestNotificationService},
                    { provide: EnvironmentService, useValue: mockEnvironmentService},
                    { provide: UserService, useValue: mockUserService},
                    { provide: ReportIssueService, useValue: {}}
                  ],
        schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
    translateService = TestBed.get(TranslateService);
    translateService.setTranslation('en', translations);
    translateService.use('en');
  }));

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
  });

  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000000;
    mockAccessControlService = TestBed.get(AccessControlService);
    fixture = TestBed.createComponent(PrepareArrivalsGridActionsComponent);
    component = fixture.componentInstance;
    component.listItem = { ...listItemMock };
    component.roomReady = true;
    component.allowManage = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  

  //Story: US110631- TC209467
  it('should  Notify Guest (Individual) bell button is disabled for user with Read Only access in prepare tab of guest list', () => {
    recreate([{"name":"Notify Guest","tagName":"notify_guest","type":"C","access":"R"}]);
    const notifyButton = fixture.debugElement.query(By.css('.readOnlyNotifyBell'));
    expect(notifyButton.nativeElement.disabled).toBeTruthy();
    
  });

   //Story: US110631- TC209468
   it('should show tooltip on hover notify button for user having readonly permission ', () => {
    recreate([{"name":"Notify Guest","tagName":"notify_guest","type":"C","access":"R"}]);
    const addTaskButton = fixture.debugElement.query(By.css('.notify-div'));
    addTaskButton.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();
    const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
    expect(tooltip).not.toBeNull();
    expect(tooltip.nativeElement.innerText).toEqual(translations.LBL_READ_ONLY_PERMISSION);
  });

  //Story: US109930- TC209471
  it('should  Notify Guest (Individual) bell button is disabled for user with No access in prepare tab of guest list', () => {
    recreate([{"name":"Notify Guest","tagName":"notify_guest","type":"C","access":"X"}]);
    const notifyButton = fixture.debugElement.query(By.css('.readOnlyNotifyBell'));
    expect(notifyButton.nativeElement.disabled).toBeTruthy();
    
  });

   //Story: US109930- TC209472
   it('should show tooltip on hover notify button for user having No access permission ', () => {
    recreate([{"name":"Notify Guest","tagName":"notify_guest","type":"C","access":"X"}]);
    const addTaskButton = fixture.debugElement.query(By.css('.notify-div'));
    addTaskButton.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();
    const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
    expect(tooltip).not.toBeNull();
    expect(tooltip.nativeElement.innerText).toEqual(translations.LBL_READ_ONLY_PERMISSION);
  });

  //Story: US110630- TC209438
  it('should  Notify Guest (Individual) bell button is enabled for user with read and write access in prepare tab of guest list', () => {
    recreate([{"name":"Notify Guest","tagName":"notify_guest","type":"C","access":"W"}]);
    const notifyButton = fixture.debugElement.query(By.css('.readOnlyNotifyBell'));
    expect(notifyButton).toBeNull();
    
  });

  function recreate(accessControlPermissions: AccessControlPermission[]) {
    fixture.destroy();
    fixture = TestBed.createComponent(PrepareArrivalsGridActionsComponent);
    component = fixture.componentInstance;
    component.hasErrors = false;
    component.listItem = { ...listItemMock };
    component.roomReady = true;
    component.allowManage = true;
    mockAccessControlService = TestBed.get(AccessControlService);
    mockAccessControlService.accessControlPermissions = accessControlPermissions;
    mockGuestNotificationService = TestBed.get(GuestNotificationService);
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
    component.hasErrors = false;
    component.listItem = { ...listItemMock };
    component.listItem.assignedRoomStatus = {frontOfficeStatus: 'Vacant', roomNumber: '2', roomStatus: 'Inspected'};
    component.isTodaySelected = true;
    component.allowManage = true;
    fixture.detectChanges();
  }
});