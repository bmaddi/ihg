import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ArrivalsListModel, NotifyGuestModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ROOM_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { WarningNotifyGuestModalComponent } from './../warning-notify-guest-modal/warning-notify-guest-modal.component';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { GuestNotificationService } from '@app/modules/prepare/services/guest-notification/guest-notification.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { NOTIFY_ERROR_CONTENT } from '@modules/prepare/constants/prepare-error-constants';
import { ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { ReservationStatus } from '@modules/guests/enums/guest-arrivals.enums';
import { PREPARE_NOTIFICATION_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import {PrepareArrivalsGridActionsCustomizer} from './prepare-arrivals-grid-actions-access.customizer';

@Component({
  selector: 'app-prepare-arrivals-grid-actions',
  templateUrl: './prepare-arrivals-grid-actions.component.html',
  styleUrls: ['./prepare-arrivals-grid-actions.component.scss']
})
export class PrepareArrivalsGridActionsComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {
  private roomReadyActionCode = 'room-ready';
  private rootSubscription = new Subscription();
  private guestNotifications$: Subscription = new Subscription();
  public roomReady: boolean;
  public unresolvedTasksCount = 0;
  public errorConfig = NOTIFY_ERROR_CONTENT;
  public reportIssueAutoFill = PREPARE_NOTIFICATION_ISSUE_AUTOFILL.notificationError;
  public errorCardType = ERROR_CARD_TYPE.LINE_ERROR;
  public notificationAccessCustomizer = PrepareArrivalsGridActionsCustomizer['notifyCustomize'];

  @Input() listItem: ArrivalsListModel;
  @Input() allowManage: boolean;
  @Input() isTodaySelected: boolean;
  @Input() selectedDate: Date;
  @Output() emitViewTasks: EventEmitter<void> = new EventEmitter();

  constructor(
    private gaService: GoogleAnalyticsService,
    private modalService: NgbModal,
    private arrivalsService: ArrivalsService,
    private roomsService: RoomsService,
    private guestsNotifiedService: GuestNotificationService) {
    super();
  }

  ngOnInit() {
    this.subscribeToRoomSet();
    this.subscribeToGuestNotified();
  }

  ngOnChanges() {
    this.setRoomReady();
  }

  private subscribeToRoomSet(): void {
    this.rootSubscription.add(this.roomsService.getRoom().subscribe((room: Room) => {
      this.ngOnChanges();
    }));
  }

  private subscribeToGuestNotified(): void {
    this.guestNotifications$.add(this.guestsNotifiedService.getGuestNotifiedAction().subscribe((response: NotifyGuestModel[]) => {
      this.guestsNotified(response);
    }));
  }

  private guestsNotified(guestsNotified: NotifyGuestModel[]): void {
    const notifyGuestItem = guestsNotified.find(item => (item.reservationNumber === this.listItem.reservationNumber) &&
      item.actionItemCode === this.roomReadyActionCode && item.guestNotifyStatus);
    if (notifyGuestItem) {
      this.setNotified(notifyGuestItem.guestNotifyTimeStamp);
    }
  }

  private setNotified(timestamp: string): void {
    this.listItem.notified = timestamp;
    this.listItem.selected = false;
  }

  private setRoomReady(): void {
    this.roomReady = this.isTodaySelected && this.isVacant() && this.isCleanOrInspected();
  }

  private isVacant(): boolean {
    const status = this.listItem.assignedRoomStatus;
    return status ? status.frontOfficeStatus === ROOM_STATUS.Vacant.value : false;
  }

  private isCleanOrInspected(): boolean {
    if (this.listItem.assignedRoomStatus && this.listItem.inspected) {
      return this.listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value;
    }
    return this.listItem.assignedRoomStatus && (this.listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Clean.value
      || this.listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value);
  }

  public isLoyaltyMember(): boolean {
    return !!this.listItem.ihgRcNumber;
  }

  public isGuestCheckedIn(): boolean {
    return this.listItem.prepStatus === ReservationStatus.checkedIn || this.listItem.prepStatus === ReservationStatus.inHouse;
  }

  public notifyGuest(): void {
    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY, PrepareForArrivalsGaConstants.ACTION_NOTIFY_GUEST_SINGLE,
      PrepareForArrivalsGaConstants.SELECTED);
    const callbackFn = this.handleGuestNotification.bind(this);
    this.arrivalsService.handleUnsavedTasks(callbackFn, null, this.listItem);
  }

  private handleGuestNotification(): void {
    this.checkUnresolvedTasksCount();
    if (this.unresolvedTasksCount) {
      this.openWarningNotifyGuestModal();
    } else {
      this.notifyGuests();
    }
  }

  private checkUnresolvedTasksCount(): void {
    const hasTasks = this.listItem.tasks && this.listItem.tasks.length;
    this.unresolvedTasksCount = hasTasks ? this.listItem.tasks.filter(item => item.isResolved === false).length : 0;
  }

  private openWarningNotifyGuestModal(): void {
    const modal = this.modalService.open(WarningNotifyGuestModalComponent, { backdrop: 'static', windowClass: 'modal-medium' });
    const modalComponent = modal.componentInstance as WarningNotifyGuestModalComponent;
    modalComponent.unresolvedTasksCount = this.unresolvedTasksCount;
    modal.result.then((action: string) => {
      if (action === 'viewTasks') {
        this.emitViewTasks.emit();
      } else {
        this.notifyGuests();
      }
    }, () => {
    });
  }

  private notifyGuests(): void {
    this.guestNotifications$.add(this.guestsNotifiedService.notifyGuest(this.selectedDate, [this.listItem])
      .subscribe((response: NotifyGuestModel[]) => {
        if (!super.checkIfAnyApiErrors(response)) {
          this.guestsNotifiedService.emitNotifyGuestStatus(response);
        }
      }, (error) => {
        super.processHttpErrors(error);
      }));
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
    this.guestNotifications$.unsubscribe();
  }
}
