import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { PagerContextChanges } from '@progress/kendo-angular-grid/dist/es2015/pager/pager-context.service';
import { LocalizationService } from '@progress/kendo-angular-l10n';
import { PagerContextService, PageChangeEvent} from '@progress/kendo-angular-grid';
import { TranslateModule, TranslateLoader, TranslateFakeLoader, TranslateService } from '@ngx-translate/core';
import { Subject, BehaviorSubject, of, Observable } from 'rxjs';
import { PrintGridComponent } from '@app/modules/print-record/components/print-grid/print-grid.component';

import { GoogleAnalyticsService, AccessControlModule, EnvironmentService, UserService, TransitionGuardService, UserConfirmationDialogService, DetachedToastMessageService, ToastMessageOutletService, AccessControlService, AccessControlPermission } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { PrepareArrivalsGridPagerComponent } from './prepare-arrivals-grid-pager.component';
import { BulkNotifyComponent } from '../bulk-notify/bulk-notify.component';

class MockPagerContextService {
  total = 3;
  skip: number;
  pageSize: number;
  changes: Subject<PagerContextChanges> = new Subject<PagerContextChanges>();
  pageChange: Subject<PageChangeEvent> = new Subject<PageChangeEvent>();
}
class MockLocalizationService {
 changes = new BehaviorSubject({ rtl: true });
 get() {
  return ;
}
}

class MockRouter {
  navigate = jasmine.createSpy('navigate');
  events = new Observable(observer => {
   observer.complete();
  });  
}

const translations = {
  LBL_READ_ONLY_PERMISSION: 'You do not have permission to perform this action.'
};

describe('PrepareArrivalsGridPagerComponent', () => {
  let component: PrepareArrivalsGridPagerComponent;
  let fixture: ComponentFixture<PrepareArrivalsGridPagerComponent>;
  let mockGoogleAnalyticsService;
  let mockEnviornmentService;
  let mockConfirmationModalService;
  let mockDetachedToastMessageService;
  let mockAccessControlService;
  let mockTransitionGuardService;
  let mockUserConfirmationDialogService;
  let mockToastMessageOutletService;
  let mockUserService;
  let testBed: TestBed;
  let translate: any;

  beforeEach(async(() => {
   mockAccessControlService = jasmine.createSpyObj(['getAccessControlPermissions']);
   mockAccessControlService.accessControlPermissions = [];
   mockGoogleAnalyticsService = jasmine.createSpyObj(['trackEvent']);
   mockEnviornmentService = jasmine.createSpyObj(['getAppLinks', 'getApplCd', 'getApiUrl']);
   mockDetachedToastMessageService = jasmine.createSpyObj(['']);
   mockTransitionGuardService = jasmine.createSpyObj(['']);
   mockUserService = jasmine.createSpyObj(['']);
   mockConfirmationModalService = jasmine.createSpyObj(['']);
   mockUserConfirmationDialogService = jasmine.createSpyObj(['']);
   mockToastMessageOutletService = jasmine.createSpyObj(['']);
   mockEnviornmentService.getAppLinks.and.returnValue([]);
   mockEnviornmentService.getAppLinks.and.returnValue(of('fdk'));
   TestBed.configureTestingModule(
      {
        imports: [
          TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: TranslateFakeLoader } }),
          AccessControlModule,
          HttpClientModule,
          NgbTooltipModule
        ],
        declarations: [ PrepareArrivalsGridPagerComponent,
        BulkNotifyComponent,
        PrintGridComponent
        ],
     providers: [
      {
        provide: LocalizationService,
        useClass: MockLocalizationService
      },
      {
        provide: PagerContextService,
        useClass: MockPagerContextService
      },
      {
        provide: GoogleAnalyticsService,
        useValue: mockGoogleAnalyticsService
      },
      {
        provide: EnvironmentService,
        useValue: mockEnviornmentService
      },
      {
        provide: ConfirmationModalService,
        useValue: mockConfirmationModalService
      },
      {
        provide: UserService,
        useValue: mockUserService
      },
      {
        provide: TransitionGuardService,
        useValue: mockTransitionGuardService
      },
      {
        provide: UserConfirmationDialogService,
        useValue: mockUserConfirmationDialogService
      },
      {
        provide: DetachedToastMessageService,
        useValue: mockDetachedToastMessageService
      },
      {
        provide: ToastMessageOutletService,
        useValue: mockToastMessageOutletService
      },
      { provide: Router, useClass: MockRouter },
      { provide: ActivatedRoute, useValue: {} },
      { provide: AccessControlService, useValue: mockAccessControlService }
     ],
     schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
    testBed = getTestBed();
    translate = testBed.get(TranslateService);
    translate.setTranslation('en', translations);
    translate.use('en');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareArrivalsGridPagerComponent);
    component = fixture.componentInstance;
    component.gridView = {
      data : [],
      total : 0
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    component.gridView = {};
    
    expect(component).toBeTruthy();
  });

 /*** TC209491 (F16494/US110101) ***/
it('should  display \'bulk notification bell\' in enabled mode for users with "Read & Write" permission to Notify Guest sub-capability when user selects more than 1 checkbox which is eligible to be notified', () => {
  recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'W'}]);
  component.gridView = getPrepareGridData();
  component.selectedListCount();
  
  fixture.detectChanges();

  expect( fixture.debugElement.queryAll(By.css('app-bulk-notify')).length).toBe(1);
  });

  /*** TC209490 (F16494/US110101) ***/
it('should  verify \'bulk notification bell\' in enabled mode for users with "Read & Write" permission to Notify Guest sub-capability when user selects more than 1 checkbox which is eligible to be notified', () => {
  recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'W'}]);
  component.gridView = getPrepareGridData();
  component.selectedListCount();
  
  fixture.detectChanges();

  expect( fixture.debugElement.queryAll(By.css('app-bulk-notify')).length).toBe(1);
  expect( fixture.debugElement.queryAll(By.css('button[data-slnm-ihg^="BulkNotifyBell-SID"]')).length).toBe(1);
  expect(fixture.debugElement.queryAll(By.css('button[data-slnm-ihg^="BulkNotifyBell-SID"] i.fa-bell')).length).toBe(1);
});

   /*** TC209985 (F16494/US110632) ***/
  it('should  display \'bulk notification bell\' in disabled mode for users with "Read only" permission to Notify Guest sub-capability when user selects more than 1 checkbox which is eligible to be notified', () => {
    recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'R'}]);
    component.gridView = getPrepareGridData();
    component.selectedListCount();

    fixture.detectChanges();
      
    expect( fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]')).length).toBe(1);
    expect(fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"] i.fa-bell')).length).toBe(1);
});

/*** TC209986 (F16494/US110632) ***/
it('should  show tooltip when user mouse-over \'bulk notification bell\' in disabled mode for users with "Read only" permission to Notify Guest sub-capability when user selects more than 1 checkbox which is eligible to be notified', () => {
    recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'R'}]);
    component.gridView = getPrepareGridData();
    component.selectedListCount();

    fixture.detectChanges();
    
    expect( fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]')).length).toBe(1);
    expect(fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"] i.fa-bell')).length).toBe(1);

    
    const bulkNotificationBell = fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]'));
    bulkNotificationBell[0].triggerEventHandler('mouseenter', null);

    const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
    expect(tooltip).not.toBeNull();
    expect(tooltip.nativeElement.innerText).toEqual(translations.LBL_READ_ONLY_PERMISSION);
});


 /*** TC209660 (F16494/US110633) ***/
it('should  display \'bulk notification bell\' in disabled mode for users with "No Access" permission to Notify Guest sub-capability when user selects more than 1 checkbox which is eligible to be notified', () => {
    recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'X'}]);
    component.gridView = getPrepareGridData();
    component.selectedListCount();

    fixture.detectChanges();
    
    expect( fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]')).length).toBe(1);
    expect(fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"] i.fa-bell')).length).toBe(1);
});

 /*** TC209661 (F16494/US110633) ***/
it('should  show tooltip when user mouse-over \'bulk notification bell\' in disabled mode for users with "No Access" permission to Notify Guest sub-capability when user selects more than 1 checkbox which is eligible to be notified', () => {
  recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'X'}]);
  component.gridView = getPrepareGridData();
  component.selectedListCount();

  fixture.detectChanges();
  
  expect( fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]')).length).toBe(1);
  expect(fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"] i.fa-bell')).length).toBe(1);

  const bulkNotificationBell = fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]'));
  bulkNotificationBell[0].triggerEventHandler('mouseenter', null);

  const tooltip = fixture.debugElement.query(By.css('ngb-tooltip-window'));
  expect(tooltip).not.toBeNull();
  expect(tooltip.nativeElement.innerText).toEqual('You do not have permission to perform this action.');
});

 /*** TC209975 (F16494/US110633) ***/
it('should not display \'bulk notification bell\' in disabled mode for users with "No Access" permission to Notify Guest sub-capability when user selects less than 1 checkbox which is eligible to be notified', () => {
    recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'X'}]);
    component.gridView = {
      data : [{
        'firstName': 'Nancy',
        'lastName': 'Goosely',
        'reservationNumber': '49063899',
        'badges': [
          'karma',
          'spire',
          'ambassador',
          'employee'
        ],
        'loyaltyId': '170409198',
        'nights': 1,
        'deparatureDate': '2019-09-17',
        'groupName': 'Rising Phoenix',
        'arrivalTime': '',
        'roomNumber': '',
        'roomType': '',
        'roomCount': 1,
        'paymentType': 'CARD',
        'arrival': '',
        'reservationStatus': 'INHOUSE',
        'prepStatus': 'INHOUSE',
        'selected': true
      }],
      total : 1
    };
    component.selectedListCount();

    fixture.detectChanges();

    expect( fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]')).length).toBe(0);
    expect(fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"] i.fa-bell')).length).toBe(0);
});

 /*** TC209967 (F16494/US110101) ***/
it('should not display \'bulk notification bell\' in enabled mode for users with "Read Write" permission to Notify Guest sub-capability when user selects less than 1 checkbox which is eligible to be notified', () => {
    recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'W'}]);
    component.gridView = {
      data : [{
        'firstName': 'Nancy',
        'lastName': 'Goosely',
        'reservationNumber': '49063899',
        'badges': [
          'karma',
          'spire',
          'ambassador',
          'employee'
        ],
        'loyaltyId': '170409198',
        'nights': 1,
        'deparatureDate': '2019-09-17',
        'groupName': 'Rising Phoenix',
        'arrivalTime': '',
        'roomNumber': '',
        'roomType': '',
        'roomCount': 1,
        'paymentType': 'CARD',
        'arrival': '',
        'reservationStatus': 'INHOUSE',
        'prepStatus': 'INHOUSE',
        'selected': true
      }],
      total : 1
    };
    
    component.selectedListCount();

    fixture.detectChanges();

    expect( fixture.debugElement.queryAll(By.css('app-bulk-notify')).length).toBe(0);
});


 /*** TC209976 (F16494/US110632) ***/
it('should not display \'bulk notification bell\' in disabled mode for users with "Read" permission to Notify Guest sub-capability when user selects less than 1 checkbox which is eligible to be notified', () => {
    recreate([{'name': 'Notify Guest', 'tagName': 'notify_guest', 'type': 'C', 'access': 'R'}]);
    component.gridView = {
      data : [{
        'firstName': 'Nancy',
        'lastName': 'Goosely',
        'reservationNumber': '49063899',
        'badges': [
          'karma',
          'spire',
          'ambassador',
          'employee'
        ],
        'loyaltyId': '170409198',
        'nights': 1,
        'deparatureDate': '2019-09-17',
        'groupName': 'Rising Phoenix',
        'arrivalTime': '',
        'roomNumber': '',
        'roomType': '',
        'roomCount': 1,
        'paymentType': 'CARD',
        'arrival': '',
        'reservationStatus': 'INHOUSE',
        'prepStatus': 'INHOUSE',
        'selected': true
      }],
      total : 1
    };
    component.selectedListCount();

    fixture.detectChanges();
   
    expect( fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"]')).length).toBe(0);
    expect(fixture.debugElement.queryAll(By.css('button.disabled-bell[data-slnm-ihg^="DisabledBulkNotifyBell-SID"] i.fa-bell')).length).toBe(0);
});

  function recreate(accessControlPermissions: AccessControlPermission[]) {
    fixture.destroy();
    mockAccessControlService = TestBed.get(AccessControlService);
    mockAccessControlService.accessControlPermissions = accessControlPermissions;
    fixture = TestBed.createComponent(PrepareArrivalsGridPagerComponent);
    component = fixture.componentInstance;
    component.gridView = {
      data : [],
      total : 0
    };
    fixture.detectChanges();
  }

 function getPrepareGridData(){
   const gridData = {
    data : [{
      'firstName': 'Nancy',
      'lastName': 'Goosely',
      'reservationNumber': '49063899',
      'badges': [
        'karma',
        'spire',
        'ambassador',
        'employee'
      ],
      'loyaltyId': '170409198',
      'nights': 1,
      'deparatureDate': '2019-09-17',
      'groupName': 'Rising Phoenix',
      'arrivalTime': '',
      'roomNumber': '',
      'roomType': '',
      'roomCount': 1,
      'paymentType': 'CARD',
      'arrival': '',
      'reservationStatus': 'INHOUSE',
      'prepStatus': 'INHOUSE',
      'selected': true
    },
    {
      'firstName': 'Michael',
      'lastName': 'Green',
      'reservationNumber': '26195183',
      'badges': [
        'gold',
        'ambassador',
        'employee'
      ],
      'loyaltyId': '147096190',
      'nights': 1,
      'deparatureDate': '2019-07-10',
      'groupName': '',
      'arrivalTime': '00:00',
      'roomNumber': '',
      'roomType': '',
      'roomCount': 0,
      'paymentType': 'OTHER',
      'arrival': '',
      'reservationStatus': 'CHECKEDIN',
      'prepStatus': 'CHECKEDIN',
      'selected': true
    }],
    total : 2
  };

  return gridData;
 }
});

