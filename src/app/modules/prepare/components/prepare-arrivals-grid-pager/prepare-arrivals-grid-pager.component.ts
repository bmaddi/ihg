import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PagerInfoComponent } from '@progress/kendo-angular-grid';
import { PrepareArrivalsGridPagerAccessCustomizer } from './prepare-arrivals-grid-pager-access.customizer';

@Component({
  selector: 'app-prepare-arrivals-grid-pager',
  templateUrl: './prepare-arrivals-grid-pager.component.html',
  styleUrls: ['./prepare-arrivals-grid-pager.component.scss']
})
export class PrepareArrivalsGridPagerComponent extends PagerInfoComponent implements OnInit {

  @Input() gridView;
  @Input() settings;
  @Input() printMenuItems: { label: string, value: string }[];
  
  @Output() selectPageSize: EventEmitter<any> = new EventEmitter();
  @Output() print = new EventEmitter<string>();
  accessModifier = PrepareArrivalsGridPagerAccessCustomizer;
  
  selectedPageSize(value: any): void {
    this.selectPageSize.emit(value);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public selectedListCount(): number {
    return this.gridView.data.filter(item => item.selected).length;
  }

  public handlePrint(printAction: string): void {
    this.print.emit(printAction);
  }
}
