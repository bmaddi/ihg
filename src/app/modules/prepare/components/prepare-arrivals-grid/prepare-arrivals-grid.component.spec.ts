import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { By } from '@angular/platform-browser';
import { GridModule } from '@progress/kendo-angular-grid';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { cloneDeep, get } from 'lodash';

import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import {
  UserService,
  TransitionGuardService,
  UserConfirmationDialogService,
  DetachedToastMessageService,
  ToastMessageOutletService,
  GoogleAnalyticsService,
  SessionStorageService
} from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { PrepareArrivalsGridComponent } from './prepare-arrivals-grid.component';
import { HighlightPipe } from '@app-shared/pipes/text-highlight';
import { TruncateTextPipe } from '@app-shared/pipes/shorten-text.pipe';
import { googleAnalyticsStub } from '@app/constants/app-test-constants';
import { RouterTestingModule } from '@angular/router/testing';
import { TitleCasePipe } from '@angular/common';
import { prepareArrivalsResponse } from '@modules/prepare/mocks/prepare-arrivals-mock';
import { sp } from '@angular/core/src/render3';
import { Test } from 'tslint';
import { GuestListService } from '@modules/guests/services/guest-list.service';

describe('PrepareArrivalsGridComponent', () => {
  let component: PrepareArrivalsGridComponent;
  let fixture: ComponentFixture<PrepareArrivalsGridComponent>;

  beforeEach(async(() => {
    TestBed
      .overrideProvider(DetachedToastMessageService, {
        useValue: {}
      })
      .overrideProvider(ToastMessageOutletService, {
        useValue: {}
      })
      .overrideProvider(SessionStorageService, {
        useValue: {}
      })
      .overrideProvider(GoogleAnalyticsService, {
        useValue: googleAnalyticsStub
      })
      .configureTestingModule({
        imports: [
          GridModule,
          NgbModule,
          TranslateModule.forChild(),
          HttpClientTestingModule,
          NgbTooltipModule,
          IhgNgCommonKendoModule,
          RouterTestingModule
        ],
        declarations: [PrepareArrivalsGridComponent,
          HighlightPipe,
          TruncateTextPipe],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          UserService,
          ConfirmationModalService,
          TransitionGuardService,
          UserConfirmationDialogService,
          DetachedToastMessageService,
          ToastMessageOutletService,
          GoogleAnalyticsService,
          SessionStorageService,
          TranslateStore,
          TitleCasePipe
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareArrivalsGridComponent);
    component = fixture.componentInstance;
    component.gridState = {
      'take': 10,
      'skip': 0,
      'filter': {
        'logic': 'and',
        'filters': []
      },
      'sort': [
        {
          'field': 'guest',
          'dir': 'asc'
        }
      ]
    };
    component.items = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngbpopover on Status', () => {
    const statusComponent = fixture.debugElement.query(By.css('.statusPopover'));

    expect(statusComponent).not.toBeNull();
    statusComponent.triggerEventHandler('hover', null);
    fixture.detectChanges();

    const popoverComponent = fixture.debugElement.query(By.css('[describedby="ngb-popver-1"]'));
    expect(popoverComponent).toBeDefined();
  });

  it('should check for expanded and expand row(s) if true and only one reservation present in the grid', () => {
    spyOn<any>(component, 'setGridView').and.callThrough();
    spyOn<any>(component, 'toggleItemExpandIcon').and.callThrough();
    const getExpandView = spyOn<any>(TestBed.get(GuestListService), 'getExpandView').and.returnValue(true);
    const mockItems = cloneDeep(get(prepareArrivalsResponse, 'paginatedArrivalsCheckList.items', [])).filter(d => d.lastName === 'A');
    component.items = mockItems;
    component.totalItems = mockItems.length;

    component.ngOnChanges({
      items: new SimpleChange([], mockItems, false)
    });
    fixture.detectChanges();

    expect(component['setGridView']).toHaveBeenCalled();
    expect(getExpandView).toHaveBeenCalled();
    expect(component.toggleItemExpandIcon).toHaveBeenCalled();
    expect(fixture.debugElement.queryAll(By.css('.k-detail-row')).length).toEqual(1);
  });
});
