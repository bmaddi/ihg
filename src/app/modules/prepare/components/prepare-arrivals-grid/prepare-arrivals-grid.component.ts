import { Component, Input, Output, OnChanges, SimpleChanges, ViewChild, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { GridComponent, GridDataResult, PageChangeEvent, RowClassArgs } from '@progress/kendo-angular-grid';
import { State, SortDescriptor } from '@progress/kendo-data-query';
import { cloneDeep } from 'lodash';
import { Observable, Subscription, Subject } from 'rxjs';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { ArrivalsListModel, NotifyGuestModel, RoomChangeModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { AppConstants } from '@app/constants/app-constants';
import { columnGAMapping, defaultGridSettings, GuestCheckBoxRules } from '@modules/prepare/constants/arrivals-constants';
import { ROOM_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { PrepareArrivalsDetailsTasksService } from '@modules/prepare/components/prepare-arrivals-details/services/details-tasks/prepare-arrivals-details-tasks.service';
import { GuestNotificationService } from '@app/modules/prepare/services/guest-notification/guest-notification.service';
import { AnalyticsTrackingService } from '@app/modules/prepare/components/prepare-arrivals-details/services/analytics-tracking/analytics-tracking.service';
import { ActivatedRoute } from '@angular/router';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { NO_DATA_ERROR_CONTENT } from '@modules/prepare/constants/prepare-error-constants';
import { printGridItems } from '@app/modules/print-record/constants/print-grid.constant';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ReservationStatus } from '@modules/guests/enums/guest-arrivals.enums';
import {PREPARE_GROUP_ISSUE_AUTOFILL, PREPARE_REPORT_ISSUE_AUTOFILL} from '@app/constants/error-autofill-constants';

@Component({
  selector: 'app-prepare-arrivals-grid',
  templateUrl: './prepare-arrivals-grid.component.html',
  styleUrls: ['./prepare-arrivals-grid.component.scss']
})
export class PrepareArrivalsGridComponent implements OnInit, OnChanges, OnDestroy {
  private defaultGridSettings = cloneDeep(defaultGridSettings);
  private roomReadyActionCode = 'room-ready';
  private maxSelectionCount = 10;
  private rootSubscription = new Subscription();
  private stateName: string;
  public gridView: GridDataResult;
  public gridSettings = cloneDeep(this.defaultGridSettings);
  public noDataSymbol = AppConstants.EMDASH;
  public characterCount = 20;
  public selectedGuests = [];
  public noDataConfig = NO_DATA_ERROR_CONTENT;
  public reportIssueAutoFill = PREPARE_REPORT_ISSUE_AUTOFILL;
  public printMenuItems = printGridItems.slice(0);
  public refreshTask = new Subject<null>();
  public roomSet = new Subject<null>();

  @Input() items: ArrivalsListModel[];
  @Input() totalItems: number;
  @Input() gridState: State;
  @Input() filterOrSearchApplied: boolean;
  @Input() searchStr: string;
  @Input() allowManage: boolean;
  @Input() isTodaySelected: boolean;
  @Input() selectedDate: Date;
  @Input() prepareDataErrorEvent: Observable<EmitErrorModel>;
  @Input() selectedGroup: string;
  @Output() print = new EventEmitter<{ printAction: string, items: ArrivalsListModel[] }>();
  @Output() roomChange = new EventEmitter<RoomChangeModel>();
  @ViewChild(GridComponent) grid: GridComponent;

  constructor(
    private arrivalsService: ArrivalsService,
    private roomsService: RoomsService,
    private gaService: GoogleAnalyticsService,
    private guestListService: GuestListService,
    private titleCasePipe: TitleCasePipe,
    private taskService: PrepareArrivalsDetailsTasksService,
    private guestsNotifiedService: GuestNotificationService,
    private activatedRoute: ActivatedRoute,
    private trackingService: AnalyticsTrackingService) {
  }

  ngOnInit(): void {
    this.subscribeToGuestNotified();
    this.subscribeRefreshTasksStatus();
    this.subscribeToRoomSet();
    this.setStateName();
  }

  private setStateName() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.stateName = data.stateName;
    });
  }

  private subscribeToGuestNotified(): void {
    this.rootSubscription.add(this.guestsNotifiedService.getGuestNotifiedAction().subscribe((response: NotifyGuestModel[]) => {
      this.setGridView();
      this.clearCheckboxSelections(response);
    }));
  }

  private subscribeToRoomSet(): void {
    this.rootSubscription.add(this.roomsService.getRoom().subscribe(() => {
      this.roomSet.next();
    }));
  }

  private subscribeRefreshTasksStatus(): void {
    this.rootSubscription.add(this.arrivalsService.refreshTasksStatus.subscribe(() => {
      this.refreshTask.next();
    }));
  }

  private clearCheckboxSelections(notifiedGuests: NotifyGuestModel[]) {
    notifiedGuests.forEach((item: NotifyGuestModel) => {
      if (item.actionItemCode === this.roomReadyActionCode && item.guestNotifyStatus) {
        this.selectedGuests = this.selectedGuests.filter(res => res !== item.reservationNumber);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.items && changes.items.currentValue) {
      this.setGuestNotifyTimeStamp();
      this.setGridView();
      this.selectedGuests = [];
    }
  }

  private setGuestNotifyTimeStamp(): void {
    this.items.forEach((data: ArrivalsListModel) => {
      if (data.infoItems && data.infoItems.length) {
        const notifyGuestItem = data.infoItems.find(item => item.type === this.roomReadyActionCode && item.indicator === 'Y');
        data.notified = notifyGuestItem ? notifyGuestItem.createTimestamp : '';
      }
    });
  }

  private setGridView(): void {
    this.gridView = {
      data: this.items,
      total: this.totalItems
    };
    if (this.guestListService.getExpandView()) {
      if (this.items.length === 1) {
        this.toggleItemExpandIcon(0, this.items[0]);
      }
      this.guestListService.setExpandView(false);
    }
  }

  public onPageChange(event: PageChangeEvent): void {
    const callbackFn = this.handlePageChange.bind(this, event);
    const cancelCallbackFn = this.revertPageSizeSelection.bind(this);
    this.arrivalsService.handleUnsavedTasks(callbackFn, cancelCallbackFn);
  }

  private handlePageChange(event: PageChangeEvent): void {
    this.gridState.skip = event.skip;
    this.gridState.take = event.take;
    this.emitRefreshGridData();
  }

  private revertPageSizeSelection(): void {
    const changedSettings = cloneDeep(this.defaultGridSettings);
    changedSettings.rows.default = this.grid.pageSize;
    this.gridSettings = changedSettings;
  }

  public fetchErrorConfig(): void {
    if (this.selectedGroup) {
      this.reportIssueAutoFill = PREPARE_GROUP_ISSUE_AUTOFILL;
    } else {
      this.reportIssueAutoFill = PREPARE_REPORT_ISSUE_AUTOFILL;
    }
  }

  public emitRefreshGridData(): void {
    this.arrivalsService.refreshGridEvent.next();
    this.fetchErrorConfig();
  }

  public displayToolTip(data: string): boolean {
    return data.length > this.characterCount;
  }

  public clickProfileIcon(loyaltyId: string): void {
    this.trackingService.trackClickProfileIcon(this.stateName);
    const callbackFn = this.setMemberProfile.bind(this, loyaltyId);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  public transformData(dataParam: string): string {
    return this.titleCasePipe.transform(dataParam);
  }

  public setMemberProfile(loyaltyId: string): void {
    this.guestListService.setMemberProfile(+loyaltyId);
  }

  public toggleItemExpandIcon(rowIndex: number, dataItem: ArrivalsListModel, toggleItem?: boolean): void {
    const toggleParam = { 'rowIndex': rowIndex, 'dataItem': dataItem, 'toggleItem': toggleItem };
    const callbackFn = this.toggleItem.bind(this, toggleParam);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  private toggleItem(toggleParam): void {
    const expanded = !toggleParam.dataItem.expanded;
    if (expanded) {
      this.collapseAll();
      this.expandItem(toggleParam.rowIndex, toggleParam.dataItem);
    } else if (!toggleParam.toggleItem) {
      this.handleCollapse(toggleParam.rowIndex, toggleParam.dataItem);
    }
  }

  public handleViewTasks(rowIndex: number, dataItem: ArrivalsListModel): void {
    if (!dataItem.expanded) {
      this.collapseAll();
      this.expandItem(rowIndex, dataItem);
    }
    setTimeout(() => {
      this.taskService.emitViewTaskAction();
    },0)
  }

  private expandItem(rowIndex: number, dataItem: ArrivalsListModel): void {
    dataItem.expanded = true;
    this.grid.expandRow(rowIndex);
  }

  private handleCollapse(rowIndex: number, dataItem: ArrivalsListModel, isDone = false): void {
    const callbackFn = this.collapseItem.bind(this, rowIndex, dataItem, isDone);
    this.arrivalsService.handleUnsavedTasks(callbackFn, null, dataItem);
  }

  private collapseItem(rowIndex: number, dataItem: ArrivalsListModel, isDone): void {
    dataItem.expanded = false;
    dataItem.done = isDone;
    setTimeout(() => {
      this.grid.collapseRow(rowIndex);
    }, 800);
  }

  private collapseAll(): void {
    this.gridView.data.forEach((dataItem, index) => {
      dataItem.expanded = false;
      dataItem.done = false;
      this.arrivalsService.discardUnsentTasks(dataItem);
      this.grid.collapseRow(index);
    });
  }

  onSortChange(sort: SortDescriptor[]): void {
    const callbackFn = this.handleSortChange.bind(this, sort);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  private handleSortChange(sort: SortDescriptor[]): void {
    this.gridState.sort = sort;
    this.emitRefreshGridData();
    this.trackSortedColumn(sort);
  }

  trackSortedColumn(sort: SortDescriptor[]): void {
    if (sort[0] && sort[0].field) {
      const action: string = PrepareForArrivalsGaConstants.ACT_COLUMN_SORT;
      const label: string = columnGAMapping[sort[0].field];
      if (label && label.length) {
        this.trackGoogleAnalytics(action, label);
      }
    }
  }

  trackGoogleAnalytics(action: string, label: string): void {
    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY, action, label);
  }

  selectPageSize(value: any): void {
    this.trackSettings(value);
  }

  trackSettings(value: any): void {
    let label: string;
    if (typeof value === 'string' || (typeof value === 'number' && value === 10000)) {
      label = PrepareForArrivalsGaConstants.LBL_CHANGE_ROWS_ALL;
    } else if (typeof value === 'number') {
      label = PrepareForArrivalsGaConstants[`LBL_CHANGE_ROWS_${value}`];
    }
    if (label) {
      this.trackGoogleAnalytics(PrepareForArrivalsGaConstants.ACTION_CHANGE_ROWS, label);
    }
  }

  public handleTaskDoneEvent(rowIndex: number, dataItem: ArrivalsListModel): void {
    this.handleCollapse(rowIndex, dataItem, true);
  }

  public getRowClass = (context: RowClassArgs) => {
    return {
      'done-highlight': context.dataItem.done,
      'no-ovevrflow': context.dataItem.doNotMoveRoom && context.dataItem.assignedRoomNumber && context.dataItem.expanded
    };
  }

  public getCheckboxRules(dataItem: ArrivalsListModel): any {
    if (!this.notifyGuestEnabled(dataItem) && !this.guestHasBeenNotified(dataItem) && this.isLoyaltyMember(dataItem) && !this.isGuestCheckedIn(dataItem)) {
      return GuestCheckBoxRules.notReady;
    } else if (!this.isLoyaltyMember(dataItem)) {
      return GuestCheckBoxRules.nonMember;
    } else if (this.guestHasBeenNotified(dataItem)) {
      return GuestCheckBoxRules.notified;
    } else if (this.isGuestCheckedIn(dataItem)) {
      return GuestCheckBoxRules.checkedIn;
    } else if (this.notifyGuestEnabled(dataItem) && !this.guestHasBeenNotified(dataItem)) {
      return GuestCheckBoxRules.roomReady;
    }
  }

  private isGuestCheckedIn(dataItem: ArrivalsListModel): boolean {
    return dataItem.prepStatus === ReservationStatus.checkedIn || dataItem.prepStatus === ReservationStatus.inHouse;
  }

  private notifyGuestEnabled(dataItem: ArrivalsListModel): boolean {
    const status = dataItem.assignedRoomStatus;
    return status ? (this.isTodaySelected && this.isVacant(dataItem) && this.isCleanOrInspected(dataItem) && this.isLoyaltyMember(dataItem))
      : false;
  }

  private guestHasBeenNotified(dataItem: ArrivalsListModel): any {
    return !!dataItem.notified;
  }

  private isVacant(dataItem: ArrivalsListModel): boolean {
    const status = dataItem.assignedRoomStatus;
    return status ? status.frontOfficeStatus === ROOM_STATUS.Vacant.value : false;
  }

  private isCleanOrInspected(dataItem: ArrivalsListModel): boolean {
    if (dataItem.assignedRoomStatus && dataItem.inspected) {
      return dataItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value;
    }
    return dataItem.assignedRoomStatus && (dataItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Clean.value
      || dataItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value);
  }

  private isLoyaltyMember(dataItem: ArrivalsListModel): boolean {
    return !!dataItem.ihgRcNumber;
  }

  public handleCheckboxSelection(): void {
    if (this.selectedGuests.length > this.maxSelectionCount) {
      this.selectedGuests.pop();
      this.arrivalsService.openNotificationLimitWarning().subscribe(() => null, () => null);
    }
  }

  public handlePrint(printAction: string): void {
    this.print.emit({
      printAction,
      items: this.gridView.data
    });
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

  handleRoomEmit(room: RoomChangeModel) {
    this.roomChange.emit(room);
  }
}
