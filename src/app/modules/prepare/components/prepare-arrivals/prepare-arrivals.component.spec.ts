import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as moment from 'moment';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { ConfirmationModalService } from 'ihg-ng-common-components';

import { PrepareArrivalsComponent } from './prepare-arrivals.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { setTestEnvironment } from '@modules/common-test/functions/common-test.function';
import {
  MOCK_GROUP_LIST_1,
  MOCK_GROUP_LIST_2,
  MOCK_GROUP_LIST_3,
  MOCK_GROUP_LIST_4,
  prepareArrivalsResponse
} from '@modules/prepare/mocks/prepare-arrivals-mock';
import { GuestArrivalParams } from '@modules/guests/models/guest-list.models';
import { AppConstants } from '@app/constants/app-constants';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { GuestListService } from '@modules/guests/services/guest-list.service';

describe('PrepareArrivalsComponent', () => {
  let component: PrepareArrivalsComponent;
  let fixture: ComponentFixture<PrepareArrivalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepareArrivalsComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [CommonTestModule],
      providers: [ConfirmationModalService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    setTestEnvironment();
    fixture = TestBed.createComponent(PrepareArrivalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle the guest selection', () => {
    spyOn(component, 'invokeClearFilter');
    spyOn(component, 'updateGridFilterState');
    expect(component.selectedGroup).toBe(null);
    expect(component.gridState.skip).toEqual(0);
    component.handleGuestSelection('one');

    expect(component.invokeClearFilter).toHaveBeenCalledWith('one');
    expect(component.selectedGroup).toEqual('');
    expect(component.gridState.skip).toEqual(0);

    component.handleGuestSelection(null);
    expect(component.invokeClearFilter).toHaveBeenCalledWith(null);
    expect(component.selectedGroup).toEqual('');
  });

  it('should handle the group list', () => {
    spyOn(component, 'updateGroupDisplay');
    expect(component.groupList).toEqual([]);

    component.handleGroupList(null);
    expect(component.groupList).toEqual([]);
    expect(component.updateGroupDisplay).not.toHaveBeenCalled();

    component.handleGroupList(MOCK_GROUP_LIST_1);
    expect(component.groupList).toEqual(null);
    expect(component.updateGroupDisplay).not.toHaveBeenCalled();

    component.handleGroupList(MOCK_GROUP_LIST_2);
    expect(component.groupList).toEqual(MOCK_GROUP_LIST_2.data);
    expect(component.updateGroupDisplay).not.toHaveBeenCalled();

    component.handleGroupList(MOCK_GROUP_LIST_3);
    expect(component.groupList).toEqual(MOCK_GROUP_LIST_3.data);
    expect(component.updateGroupDisplay).toHaveBeenCalled();

    component.handleGroupList(MOCK_GROUP_LIST_4);
    expect(component.groupList).toEqual(null);
    expect(component.updateGroupDisplay).toHaveBeenCalled();
  });

  it('should handle the group selection', () => {
    spyOn(component, 'invokeClearFilter');
    spyOn(component, 'updateGridFilterState');
    expect(component.selectedGroup).toBe(null);
    expect(component.gridState.skip).toEqual(0);

    component.handleGroupSelection('two');
    expect(component.invokeClearFilter).toHaveBeenCalledWith();
    expect(component.selectedGroup).toEqual('two');

    component.handleGroupSelection(null);
    expect(component.invokeClearFilter).toHaveBeenCalledWith();
    expect(component.selectedGroup).toEqual(null);
  });

  it('should update the arrivals and group visibility variables', () => {
    expect(component.showArrivals).toBe(true);
    expect(component.showGroups).toBe(null);

    component.updateShowGroupsShowArrivals(true, false);
    expect(component.showArrivals).toBe(false);
    expect(component.showGroups).toBe(true);

    component.updateShowGroupsShowArrivals(false, true);
    expect(component.showArrivals).toBe(true);
    expect(component.showGroups).toBe(false);

    component.updateShowGroupsShowArrivals(true, true);
    expect(component.showArrivals).toBe(true);
    expect(component.showGroups).toBe(true);

    component.updateShowGroupsShowArrivals(false, false);
    expect(component.showArrivals).toBe(false);
    expect(component.showGroups).toBe(false);

    component.updateShowGroupsShowArrivals(null, null);
    expect(component.showArrivals).toBe(null);
    expect(component.showGroups).toBe(null);
  });

  it('should properly toggle the filters', () => {
    spyOn(component, 'doCollapseFilters');
    spyOn(component, 'doExpandFilters');

    component.expandFilters = false;
    component.toggleFiltersExpanded();

    expect(component.doCollapseFilters).toHaveBeenCalledTimes(0);
    expect(component.doExpandFilters).toHaveBeenCalledTimes(1);

    component.expandFilters = true;
    component.toggleFiltersExpanded();

    expect(component.doCollapseFilters).toHaveBeenCalledTimes(1);
    expect(component.doExpandFilters).toHaveBeenCalledTimes(1);

    component.expandFilters = false;
    component.toggleFiltersExpanded();

    expect(component.doCollapseFilters).toHaveBeenCalledTimes(1);
    expect(component.doExpandFilters).toHaveBeenCalledTimes(2);
  });

  it('should check arrivals params for expanded state #checkForExpandedState and update service accordingly', () => {
    spyOn<any>(component, 'handleInit').and.callThrough();
    spyOn<any>(component, 'updateGridFilterState').and.callThrough();
    spyOn<any>(component, 'checkForExpandedState').and.callThrough();
    // tslint:disable-next-line:max-line-length
    const getArrivalsChecklistSpy = spyOn<any>(TestBed.get(ArrivalsService), 'getArrivalsChecklist').and.returnValue(of(cloneDeep(prepareArrivalsResponse))),
      setExpandViewSpy = spyOn<any>(TestBed.get(GuestListService), 'setExpandView').and.callThrough();

    component.arrivalParams = <GuestArrivalParams>{
      selectedDate: moment().format(AppConstants.DB_DT_FORMAT),
      localDate: moment().format(AppConstants.DB_DT_FORMAT),
      reservationNumber: '10231',
      expanded: true
    };
    component.isActiveTab = true;
    expect(TestBed.get(GuestListService).getExpandView()).toBeFalsy();

    component.ngOnChanges({
      isActiveTab: new SimpleChange(null, true, false)
    });
    fixture.detectChanges();

    expect(component['handleInit']).toHaveBeenCalled();
    expect(component['updateGridFilterState']).toHaveBeenCalledWith('10231');
    expect(getArrivalsChecklistSpy).toHaveBeenCalled();
    expect(component['checkForExpandedState']).toHaveBeenCalled();
    expect(setExpandViewSpy).toHaveBeenCalledWith(true);
    expect(TestBed.get(GuestListService).getExpandView()).toBeTruthy();
  });
});
