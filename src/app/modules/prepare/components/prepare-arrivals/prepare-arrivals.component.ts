import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { State } from '@progress/kendo-data-query';
import * as moment from 'moment';
import { cloneDeep, get, isEqual } from 'lodash';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { AppConstants } from '@app/constants/app-constants';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import {
  ArrivalsChecklistModel,
  ArrivalsListModel,
  FilterModel,
  CountersModel,
  RoomChangeModel
} from '../../models/arrivals-checklist.model';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { GuestArrivalParams } from '@app/modules/guests/models/guest-list.models';
import { loyaltyFiltersDefaultState, miscFiltersDefaultState } from '@modules/prepare/constants/prepare-filter-constants';
import { printTypes } from '@modules/print-record/constants/print-grid.constant';
import { GroupList } from '@modules/group-block/models/group-block.model';
import { GUEST_CONST } from '@modules/guests/guests.constants';
import { GroupBlockService } from '@modules/group-block/services/group-block.service';
import { DefaultGridState } from '@modules/prepare/constants/arrivals-constants';
import {Room} from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';

@Component({
  selector: 'app-prepare-arrivals',
  templateUrl: './prepare-arrivals.component.html',
  styleUrls: ['./prepare-arrivals.component.scss'],
  providers: [GroupBlockService]
})
export class PrepareArrivalsComponent extends AppErrorBaseComponent implements OnInit, OnChanges, OnDestroy {
  private arrival$: Subscription = new Subscription();
  private totalStr = 'total';
  public searchQuery = '';
  public selectedDate: Date = null;
  ga = GUEST_CONST.GA;
  public initialized = false;
  public checklistItems: ArrivalsChecklistModel;
  public gridState: State = cloneDeep(DefaultGridState);
  loyaltyFilters: FilterModel[];
  miscFilters: FilterModel[];
  readonly filterTypes = { loyalty: 'orangeFilter', misc: 'blueFilter', status: 'reservationStatus' };
  private defaultFilterState = { [this.filterTypes.loyalty]: [this.totalStr], [this.filterTypes.misc]: [] };
  selectedFilters = cloneDeep(this.defaultFilterState);
  reservationStatus = '';
  filterApplied = false;
  expandFilters = true;
  filterCount = 0;
  localDate = moment();
  selectedDayIndex = 0;
  startDate: Date;
  public printableRecords = new Subject<ArrivalsListModel[]>();
  public printableFilters = ['LBL_FILTER_TOTAL'];
  public detailedPrint: boolean;
  public showGroups = null;
  public showArrivals = true;
  public selectedGroup = null;
  public groupList: GroupList[] = [];
  roomChangeData: RoomChangeModel;

  @Input() isActiveTab: boolean;
  @Input() arrivalParams: GuestArrivalParams;

  constructor(private arrivalsService: ArrivalsService, private gaService: GoogleAnalyticsService,
    private guestListService: GuestListService, private groupBlockService: GroupBlockService) {
    super();
  }

  ngOnInit() {
    super.init();
    this.subscribeRefreshGridEvent();
    this.guestTabChange();
    this.subscribeRetryPrint();
  }

  guestTabChange() {
    this.arrival$.add(this.guestListService.getGuestTabEvent().subscribe((data) => {
      if (data.tabFlag) {
        this.selectedDayIndex = data.dateIndex;
        this.selectedDate = data.searchedDate;
        this.handleGuestSelection(data.selectedData.reservationNumber);
        this.guestListService.setExpandView(true);
      }
    }));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isActiveTab && changes.isActiveTab.currentValue) {
      this.selectedGroup = null;
      this.handleInit();
    } else if (changes.isActiveTab && !changes.isActiveTab.currentValue && this.initialized) {
      this.discardUnsentTasks();
    }
    super.resetSpecificError(this.arrivalsService.printPrepareError);
  }

  private handleInit(): void {
    this.updateDaySelector();
    this.updateSelectedFilters();
    this.setLoyaltyMiscFilters();
    this.setFilterAppliedCount();
    this.updateGridFilterState(get(this.arrivalParams, 'reservationNumber', null));
    if (!this.guestListService.getExpandView()) {
      this.getArrivalsChecklist();
    }
  }

  private updateDaySelector(): void {
    if (this.arrivalParams) {
      const today = this.arrivalParams.localDate ? moment(this.arrivalParams.localDate, AppConstants.DB_DT_FORMAT) : this.localDate;
      const selected = moment(this.arrivalParams.selectedDate, AppConstants.DB_DT_FORMAT).startOf('day');
      const diff = selected.diff(today.startOf('day'), 'days');
      this.selectedDayIndex = diff > 0 ? diff : 0;
    }
  }

  private updateSelectedFilters(): void {
    if (this.arrivalParams && this.arrivalParams.selectedDate) {
      const date = moment(this.arrivalParams.selectedDate, AppConstants.DB_DT_FORMAT);
      this.selectedDate = date.isValid() ? date.toDate() : this.localDate.toDate();
    }
    if (this.arrivalParams && this.arrivalParams.memberLevel) {
      this.selectedFilters[this.filterTypes.loyalty] = [this.arrivalParams.memberLevel];
    }
    if (this.arrivalParams && this.arrivalParams.reservationStatus) {
      this.reservationStatus = this.arrivalParams.reservationStatus;
    }
  }

  private setLoyaltyMiscFilters(): void {
    const filterCounters = this.checklistItems ? this.checklistItems.counters : [];
    this.loyaltyFilters = this.arrivalsService.getLoyaltyFilters(filterCounters, this.selectedFilters[this.filterTypes.loyalty]);
    this.miscFilters = this.arrivalsService.getMiscFilters(filterCounters, false, this.selectedFilters[this.filterTypes.misc]);
  }

  private resetCheckList(counters?: CountersModel[]): void {
    if (this.checklistItems) {
      this.checklistItems.checklistActionsEnabled = null;
      this.checklistItems.localDate = null;
      this.checklistItems.operaHotelFlag = null;
      this.checklistItems.paginatedArrivalsCheckList = null;
      this.checklistItems.counters = counters || [];
    }
  }

  private getArrivalsChecklist(): void {
    this.arrival$.add(this.arrivalsService.getArrivalsChecklist(this.selectedDate, this.gridState, this.selectedGroup)
      .subscribe((data: ArrivalsChecklistModel) => {
        if (!this.checkIfAnyApiErrors(data, (checkNoData) => this.hasNoData(checkNoData))) {
          this.handleChecklistResponse(data);
        } else {
          this.resetCheckList(data.counters);
          this.setLoyaltyMiscFilters();
          this.updateGroupDisplay();
        }
      }, error => {
        this.resetCheckList();
        this.setLoyaltyMiscFilters();
        this.updateGroupDisplay();
        this.processHttpErrors(error);
      }));
  }

  private hasNoData(data: ArrivalsChecklistModel): boolean {
    return !data || !data.paginatedArrivalsCheckList || !data.paginatedArrivalsCheckList.items ||
      !data.paginatedArrivalsCheckList.items.length;
  }

  private handleChecklistResponse(data: ArrivalsChecklistModel): void {
    this.checklistItems = data;
    this.localDate = this.checklistItems && this.checklistItems.localDate ? moment(this.checklistItems.localDate) : moment();
    this.startDate = this.localDate.toDate();
    this.setLoyaltyMiscFilters();
    this.resetAnyErrors();
    this.updateGroupDisplay();
    this.checkForExpandedState();
    this.initialized = true;
  }

  updateGroupDisplay(): void {
    this.updateShowGroupsShowArrivals(this.selectedGroup || (!this.groupList || this.groupList.length ? false : null), true);
  }

  private checkForExpandedState() {
    if (this.arrivalParams && this.arrivalParams.expanded && !this.initialized) {
      this.guestListService.setExpandView(true);
    }
  }

  private subscribeRefreshGridEvent(): void {
    this.arrival$.add(this.arrivalsService.refreshGridEvent.subscribe(() => {
      this.getArrivalsChecklist();
    }));
  }

  private discardUnsentTasks(): void {
    if (this.checklistItems && this.checklistItems.paginatedArrivalsCheckList) {
      const expandedRecord = this.checklistItems.paginatedArrivalsCheckList.items.find(item => item.expanded);
      this.arrivalsService.discardUnsentTasks(expandedRecord);
    }
  }

  handleDateSelection(selectedDayIndex: number): void {
    const callbackFn = this.selectNewDate.bind(this, selectedDayIndex);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  private selectNewDate(selectedDayIndex: number): void {
    this.selectedDayIndex = selectedDayIndex > 0 ? selectedDayIndex : 0;
    this.selectedDate = moment(this.localDate).add(this.selectedDayIndex, 'day').toDate();
    this.gridState.skip = 0;
    this.selectedGroup = null;
    this.getArrivalsChecklist();
    this.trackGoogleAnalytics(PrepareForArrivalsGaConstants.DATE_SELECTION, PrepareForArrivalsGaConstants.SELECTED);
  }

  handleFilterChange(filters: string[], filterType: string): void {
    this.selectedFilters[filterType] = filters;
    this.gridState.skip = 0;
    this.setFilterAppliedCount();
    this.updateGridFilterState();
    this.getArrivalsChecklist();
    this.updatePrintableFilters();
  }

  private updatePrintableFilters(): void {
    this.printableFilters = this.selectedFilters[this.filterTypes.loyalty].map(item => {
      return loyaltyFiltersDefaultState.find(filter => filter.filterName === item).label;
    }).concat(this.selectedFilters[this.filterTypes.misc].map(item => {
      return miscFiltersDefaultState.find(filter => filter.filterName === item).label;
    }));
  }

  private setFilterAppliedCount(): void {
    const loyaltyFilters = this.selectedFilters[this.filterTypes.loyalty];
    const miscFilters = this.selectedFilters[this.filterTypes.misc];
    this.filterApplied = !isEqual(this.selectedFilters, this.defaultFilterState);
    this.filterCount = (isEqual(loyaltyFilters, [this.totalStr]) ? 0 : loyaltyFilters.length) + miscFilters.length;
  }

  updateGridFilterState(searchValue = null): void {
    const appliedLoyaltyFilters = this.selectedFilters[this.filterTypes.loyalty];
    const appliedMiscFilters = this.selectedFilters[this.filterTypes.misc];
    const allAppliedFilters = [];

    if (this.selectedGroup || this.searchQuery.length || searchValue) {
      const searchBy = this.selectedGroup || searchValue || this.searchQuery;
      allAppliedFilters.push({ field: 'search', operator: 'eq', value: searchBy.replace(/['<>]/g, ' ') });
    }

    if (appliedLoyaltyFilters.length && appliedLoyaltyFilters[0] !== this.totalStr) {
      allAppliedFilters.push({ field: this.filterTypes.loyalty, operator: 'eq', value: appliedLoyaltyFilters.join(',') });
    }

    if (this.reservationStatus) {
      allAppliedFilters.push({ field: this.filterTypes.status, operator: 'eq', value: this.reservationStatus });
    }

    if (appliedMiscFilters.length) {
      allAppliedFilters.push({ field: this.filterTypes.misc, operator: 'eq', value: appliedMiscFilters.join(',') });
    }

    this.gridState.filter.filters = allAppliedFilters;
  }

  onGuestSearch(searchString: string): void {
    this.gridState.skip = 0;
    this.searchQuery = searchString;
    this.selectedGroup = null;
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.handleClearFilters();
  }

  handleClearFilters(): void {
    const callbackFn = this.invokeClearFilter.bind(this);
    this.arrivalsService.handleUnsavedTasks(callbackFn);
  }

  invokeClearFilter(searchBy?: string): void {
    this.clearFilters();
    this.updatePrintableFilters();
    this.updateGridFilterState(searchBy);
    this.getArrivalsChecklist();
  }

  private clearFilters(): void {
    this.selectedFilters = cloneDeep(this.defaultFilterState);
    this.filterApplied = false;
    this.filterCount = 0;
    this.reservationStatus = '';
  }

  refresh() {
    this.getArrivalsChecklist();
  }

  trackGoogleAnalytics(action: string, label: string): void {
    this.gaService.trackEvent(PrepareForArrivalsGaConstants.CATEGORY, action, label);
  }

  isTodaySelected(): boolean {
    const selected = this.selectedDate ? moment(this.selectedDate) : moment();
    return this.localDate.startOf('day').isSame(selected.startOf('day'));
  }

  toggleFiltersExpanded(): void {
    if (this.expandFilters) {
      this.doCollapseFilters();
    } else {
      this.doExpandFilters();
    }
  }

  doExpandFilters(): void {
    this.expandFilters = true;
    this.trackGoogleAnalytics(PrepareForArrivalsGaConstants.ACT_SHOW_FILTERS, PrepareForArrivalsGaConstants.GA_LBL_EXPAND);
  }

  doCollapseFilters(): void {
    this.expandFilters = false;
    this.trackGoogleAnalytics(PrepareForArrivalsGaConstants.ACT_SHOW_FILTERS, PrepareForArrivalsGaConstants.GA_LBL_COLLAPSE);
  }

  hasUnsentTasks(): boolean {
    return this.arrivalsService.getUnsavedTask();
  }

  public handlePrint(data: { printAction: string, items: ArrivalsListModel[] }): void {
    this.detailedPrint = data.printAction === printTypes.CURR_DTLD || data.printAction === printTypes.ALL_DTLD;

    switch (data.printAction) {
      case printTypes.CURR_CMPCT:
      case printTypes.CURR_DTLD:
        this.printableRecords.next(data.items);
        break;
      case printTypes.ALL_CMPCT:
      case printTypes.ALL_DTLD:
        this.handleAllRecordPrint();
        break;
    }
  }

  private handleAllRecordPrint(): void {
    this.getAllArrivals();
  }

  private getAllArrivals() {
    const gridState = cloneDeep(this.gridState);
    this.arrival$.add(
      this.arrivalsService.getArrivalsChecklist(this.selectedDate, { ...gridState, take: 10000, skip: 0 })
        .subscribe((data: ArrivalsChecklistModel) => {
          if (!this.checkIfAnyApiErrors(data, null, this.arrivalsService.printPrepareError)) {
            this.printableRecords.next(get(data, 'paginatedArrivalsCheckList.items', []));
          }
        },
          error => {
            super.processHttpErrors(error, this.arrivalsService.printPrepareError);
          }
        )
    );
  }

  private subscribeRetryPrint(): void {
    this.arrival$.add(this.arrivalsService.retryPrint.subscribe(() => this.getAllArrivals()));
  }

  public handleShowHideGroup(value): void {
    super.resetSpecificError(this.emitError);
    this.updateShowGroupsShowArrivals(value, !value);
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    if (this.showArrivals) {
      this.selectedGroup = null;
      this.invokeClearFilter();
    } else {
      this.groupBlockService.showGroups.next();
    }
  }

  updateShowGroupsShowArrivals(showGroups: boolean, showArrivals: boolean): void {
    this.showGroups = showGroups;
    this.showArrivals = showArrivals;
  }

  public handleGroupSelection(groupName: string): void {
    this.gridState.skip = 0;
    this.selectedGroup = groupName;
    this.invokeClearFilter();
  }

  public handleGroupList(groupList: object): void {
    if (groupList) {
      this.groupList = groupList['data'] || null;
      if (!groupList['refresh']) {
        this.updateGroupDisplay();
      }
    }
  }

  public handleGuestSelection(searchValue: string): void {
    this.selectedGroup = '';
    this.gridState.skip = 0;
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.invokeClearFilter(searchValue);
  }

  public handlePreviewGroupSelection(searchValue: string, isAll = false): void {
    this.groupBlockService.previewSearch.next({ search: searchValue, isAll: isAll });
    this.updateShowGroupsShowArrivals(true, false);
    if (!isAll) {
      this.handleGroupSelection(searchValue);
    }
  }

  ngOnDestroy() {
    super.destroy();
    this.groupBlockService.previewSearch.next({ search: '', isAll: false });
    this.arrival$.unsubscribe();
  }

  handleRoomChange(data: RoomChangeModel) {
    this.roomChangeData = data;
  }
}
