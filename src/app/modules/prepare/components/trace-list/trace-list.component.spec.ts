import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraceListComponent } from '@app/modules/prepare/components/trace-list/trace-list.component';
import { mockArrivalList } from '@modules/prepare/mocks/prepare-arrivals-mock';

describe('TraceListComponent', () => {
  let component: TraceListComponent;
  let fixture: ComponentFixture<TraceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraceListComponent);
    component = fixture.componentInstance;
    component.arrival = mockArrivalList[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
