import { Component, Input } from '@angular/core';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import * as _ from 'lodash';
import { OrderingTaskList } from '../../models/notify-list';

@Component({
  selector: 'app-trace-list',
  templateUrl: './trace-list.component.html',
  styleUrls: ['./trace-list.component.scss']
})
export class TraceListComponent {
  @Input() arrival: ArrivalsListModel;
  @Input() ordertask: string;

  private tasksOutstandingList: any[];
  private tasksResolvedList: any[];
  public renderedTaskList: any[];

  constructor() { }

  ngOnInit() {
    this.setArrivalOutstandingTasks(this.arrival);
  }

  private setArrivalOutstandingTasks(arrival: ArrivalsListModel): void {
    let tasks = arrival.tasks || [];
    this.setTasksResolvedList(tasks);
    this.setTasksOutstandingList(tasks);
    this.renderingTaskList();
  }

  private setTasksOutstandingList(tasks: any[]): void {
    this.tasksOutstandingList = _.reject(tasks, function (task) {
      return task.isResolved || false;
    });
  }

  private setTasksResolvedList(tasks: any[]): void {
    this.tasksResolvedList = _.filter(tasks, function (task) {
      return task.isResolved || false;
    });
  }

  public renderingTaskList(): void {
    if (this.ordertask === OrderingTaskList.ALL_RESOLVED) {
      this.renderedTaskList = _.concat(this.tasksResolvedList, this.tasksOutstandingList);
    } else if (this.ordertask === OrderingTaskList.ALL_UNRESOLVED) {
      this.renderedTaskList = _.concat(this.tasksOutstandingList, this.tasksResolvedList);
    } else {
      this.renderedTaskList = this.arrival.tasks || [];
    }
  }
}
