import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningNotifyGuestModalComponent } from './warning-notify-guest-modal.component';

xdescribe('WarningNotifyGuestModalComponent', () => {
  let component: WarningNotifyGuestModalComponent;
  let fixture: ComponentFixture<WarningNotifyGuestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningNotifyGuestModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningNotifyGuestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
