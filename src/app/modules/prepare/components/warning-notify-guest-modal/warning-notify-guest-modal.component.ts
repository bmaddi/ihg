import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-warning-notify-guest-modal',
  templateUrl: './warning-notify-guest-modal.component.html',
  styleUrls: ['./warning-notify-guest-modal.component.scss']
})
export class WarningNotifyGuestModalComponent implements OnInit {
  unresolvedTasksCount: number;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  dismiss() {
    this.activeModal.dismiss();
  }

  close(action?: string) {
    this.activeModal.close(action);
  }
}
