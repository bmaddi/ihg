import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// tslint:disable-next-line:max-line-length
import { WarningRoomUnassignedModalComponent } from '@modules/prepare/components/warning-room-unassigned-modal/warning-room-unassigned-modal.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbActiveModal, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { UserService, DetachedToastMessageService, SessionStorageService, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { MockSessionStorageService } from '@modules/prepare/components/house-keeping-status-selector/house-keeping-status-selector.component.spec';

import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { listItemMock } from '@modules/prepare/constants/arrivals-test.constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { of } from 'rxjs';
import { mockArrivalList } from '@modules/prepare/mocks/prepare-arrivals-mock';

export class MockGoogleAnalyticsService {
  public trackEvent(category: string, action: string, label: string) {
  }
}


describe('WarningRoomUnassignedModalComponent', () => {
  let component: WarningRoomUnassignedModalComponent;
  let fixture: ComponentFixture<WarningRoomUnassignedModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WarningRoomUnassignedModalComponent],
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, NgbTooltipModule, RouterTestingModule],
      providers: [NgbActiveModal, UserService, TranslateService, ConfirmationModalService, DetachedToastMessageService,
        {provide: SessionStorageService, useClass: MockSessionStorageService},
        {provide: GoogleAnalyticsService, useClass: MockGoogleAnalyticsService}],
      schemas: [NO_ERRORS_SCHEMA]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningRoomUnassignedModalComponent);
    component = fixture.componentInstance;
    component.arrival = mockArrivalList[2];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable change room button until dropdown selected', () => {
    const changeRoomButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
    expect(changeRoomButton).toBeDefined();
    component.enableRoomMove = true;
    component.isStatusSelected = false;
    fixture.detectChanges();

    expect(changeRoomButton.nativeElement.disabled).toBeTruthy();

    component.isStatusSelected = true;
    fixture.detectChanges();
    expect(changeRoomButton.nativeElement.disabled).toBeFalsy();
  });

  it('should display house keeping selector if guest is in House', () => {
    component.enableRoomMove = true;
    fixture.detectChanges();
    const roomStatusSelector = fixture.debugElement.query(By.css('.room-status-selector'));
    expect(roomStatusSelector).toBeDefined();
  });

  it('should not display house keeping selector if guest is not in House', () => {
    component.enableRoomMove = false;
    fixture.detectChanges();
    const roomStatusSelector = fixture.debugElement.query(By.css('.room-status-selector'));
    expect(roomStatusSelector).toBeNull();
  });

  it('on click of change room api should be called', () => {
    component.arrival = {...listItemMock};
    const changeRoomButton = fixture.debugElement.query(By.css('[data-slnm-ihg="ChangeRoomButton-SID"]'));
    const roomserviceSpy = spyOn(TestBed.get(RoomsService), 'getRoomReleased').and.returnValue({});
    const releaseRoomSpy = spyOn(component, 'releaseRoom').and.callFake(roomserviceSpy);

    component.isStatusSelected = true;
    fixture.detectChanges();

    expect(changeRoomButton.nativeElement).toBeDefined();
    changeRoomButton.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(releaseRoomSpy).toBeDefined();
    expect(releaseRoomSpy).toHaveBeenCalled();
    expect(roomserviceSpy).toHaveBeenCalled();
  });

  it('should test google analytics for unassigned room check-in tab', function () {
    component.roomNo = '123';
    component['selectedRoomStatus'] = 'Dirty';
    component.stateName = AppStateNames.checkIn;
    const category = PrepareForArrivalsGaConstants.CATEGORY_CHECK_IN;
    const action = PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT;
    const label = PrepareForArrivalsGaConstants.RM_HK_STATUS_UPDATE.concat(component['selectedRoomStatus']);
    const trackEventSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    const roomsSpy = spyOn(TestBed.get(RoomsService), 'saveRoomStatus').and.returnValue(of('Success'));

    component.changeRoomStatus();
    fixture.detectChanges();

    expect(roomsSpy).toHaveBeenCalled();
    expect(trackEventSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should check google analytics for unassigned room prepare tab', function () {
    component.roomNo = '123';
    component['selectedRoomStatus'] = 'Dirty';
    component.stateName = AppStateNames.guestListDetails;
    const category = PrepareForArrivalsGaConstants.CATEGORY;
    const action = PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT;
    const label = PrepareForArrivalsGaConstants.RM_HK_STATUS_UPDATE.concat(component['selectedRoomStatus']);
    const trackEventSpy = spyOn(TestBed.get(GoogleAnalyticsService), 'trackEvent').and.callThrough();
    const roomsSpy = spyOn(TestBed.get(RoomsService), 'saveRoomStatus').and.returnValue(of('Success'));

    component.changeRoomStatus();
    fixture.detectChanges();

    expect(roomsSpy).toHaveBeenCalled();
    expect(trackEventSpy).toHaveBeenCalledWith(category, action, label);
  });

  it('should check View Tasks is visible when tasks are available', () => {
    component.roomNo = '123';
    component['selectedRoomStatus'] = 'Dirty';
    component.stateName = AppStateNames.checkIn;
    component.enableRoomMove = true;
    const warningText = expect(fixture.debugElement.query(By.css('[data-slnm-ihg="WarningRoomUnassignedText-SID"]')));

    component.hasTasks = true;
    component.releaseRoom();
    fixture.detectChanges();

    expect(warningText).toBeDefined();
  });

  it('should check View Tasks is hidden when no tasks available', () => {
    component.roomNo = '123';
    component['selectedRoomStatus'] = 'Dirty';
    component.stateName = AppStateNames.checkIn;
    component.enableRoomMove = true;
    const warningText = fixture.debugElement.query(By.css('[data-slnm-ihg="WarningRoomUnassignedText-SID"]'));
    const warningNoTasksText = fixture.debugElement.query(By.css('[data-slnm-ihg="WarningRoomUnassignedConfirm-SID"]'));

    component.hasTasks = false;
    component.releaseRoom();
    fixture.detectChanges();

    expect(warningText).toBeNull();
    expect(warningNoTasksText).toBeDefined();
  });
});
