import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs/operators';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { ArrivalsListModel } from '../../models/arrivals-checklist.model';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { ReleaseRoomParams, ReleaseRoomResponse } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { RoomsService } from '@modules/prepare/components/prepare-arrivals-details/services/details-rooms/rooms.service';
import { ROOM_ERROR_CONTENT } from '@modules/prepare/components/prepare-arrivals-details/constants/arrivals-details-constants';
import { ROOM_REPORT_ISSUE_AUTOFILL } from '@app/constants/error-autofill-constants';
import { TranslateService } from '@ngx-translate/core';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';

@Component({
  selector: 'app-warning-room-unassigned-modal',
  templateUrl: './warning-room-unassigned-modal.component.html',
  styleUrls: ['./warning-room-unassigned-modal.component.scss']
})

export class WarningRoomUnassignedModalComponent extends AppErrorBaseComponent {
  public roomNo: string;
  public arrival: ArrivalsListModel;
  public enableRoomMove: boolean;
  public roomErrorContent = ROOM_ERROR_CONTENT;
  public reportIssueAutoFill = ROOM_REPORT_ISSUE_AUTOFILL;
  public isStatusSelected = false;
  public stateName: string;
  public hasTasks: boolean;

  private selectedRoomStatus: string;

  constructor(private activeModal: NgbActiveModal,
              private roomsService: RoomsService,
              private translateService: TranslateService,
              private googleAnalyticsService: GoogleAnalyticsService) {
    super();
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  public releaseRoom(): void {
    if (this.enableRoomMove) {
      this.close('roomMove');
    } else {
      this.roomsService.getRoomReleased(this.roomsService.getAssignReleasePayload(this.arrival, this.arrival['assignedRoomStatus']))
        .subscribe(
          (response: ReleaseRoomResponse) => {
            if (!super.checkIfAnyApiErrors(response)) {
              this.changeRoomStatus();
            }
          },
          error => super.processHttpErrors(error));
    }
  }

  private getReleaseParams(): ReleaseRoomParams {
    return <ReleaseRoomParams>{
      pmsReservationNumber: this.arrival.pmsReservationNumber
    };
  }

  statusSelected(status: string) {
    if (status !== this.translateService.instant('LBL_HK_STATUS')) {
      this.isStatusSelected = true;
      this.selectedRoomStatus = status;
      this.roomsService.houseKeepingStatusEvent.next(this.selectedRoomStatus);
    } else {
      this.isStatusSelected = false;
    }
  }

  changeRoomStatus() {
    this.roomsService.saveRoomStatus(this.roomNo, this.selectedRoomStatus)
      .pipe(finalize(() => this.close('changeRoom')))
      .subscribe((response: string) => {
        if (!super.checkIfAnyApiErrors(response)) {
          this.trackGoogleAnalytics(this.stateName, this.selectedRoomStatus);
          setTimeout(() => this.close('changeRoom'));
        }
      }, error => super.processHttpErrors(error));
  }

  trackGoogleAnalytics(stateName: string, selectedRoomStatus: string) {
    const category = this.getCategory(stateName);
    this.googleAnalyticsService.trackEvent(category,
      PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT,
      PrepareForArrivalsGaConstants.RM_HK_STATUS_UPDATE.concat(selectedRoomStatus));
  }

  close(value?: string): void {
    this.activeModal.close(value);
  }

  private getCategory(stateName: string): string {
   if (stateName === AppStateNames.checkIn) {
     return PrepareForArrivalsGaConstants.CATEGORY_CHECK_IN;
   } else {
     return PrepareForArrivalsGaConstants.CATEGORY;
   }
  }
}
