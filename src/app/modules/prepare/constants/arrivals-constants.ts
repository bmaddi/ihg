import { State } from '@progress/kendo-data-query';

import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';

export const TOOL_TIP_DATA = {
  KEYMAP: [
    { 'badge': 'spire', 'tooltipLabel': 'LBL_SPIRE' },
    { 'badge': 'platinum', 'tooltipLabel': 'LBL_PLTNM' },
    { 'badge': 'gold', 'tooltipLabel': 'LBL_GOLD' },
    { 'badge': 'club', 'tooltipLabel': 'LBL_CLUB' },
    { 'badge': 'ambassador', 'tooltipLabel': 'LBL_FILTER_AMB' },
    { 'badge': 'royalambassador', 'tooltipLabel': 'LBL_ROYAL_AMB' },
    { 'badge': 'innercircle', 'tooltipLabel': 'LBL_FILTER_IC' },
    { 'badge': 'karma', 'tooltipLabel': 'LBL_FILTER_KAR' },
    { 'badge': 'priorityEnrollment', 'tooltipLabel': 'LBL_PRIO_ENROL' },
    { 'badge': 'potentialMember', 'tooltipLabel': 'LBL_POT_MEM' },
    { 'badge': 'employee', 'tooltipLabel': 'LBL_FILTER_EMP' }
  ]
};

export const columnGAMapping = {
  guest: PrepareForArrivalsGaConstants.LBL_COLUMN_SORT_GUEST,
  companyName: PrepareForArrivalsGaConstants.LBL_COLUMN_SORT_COMPANY,
  groupName: PrepareForArrivalsGaConstants.LBL_COLUMN_SORT_GROUP,
  arrivalTime: PrepareForArrivalsGaConstants.LBL_COLUMN_SORT_ARRIVAL_TIME
};

export const GuestCheckBoxRules = {
  notReady: {
    enableCheckbox: false, tooltipText: 'LBL_RM_NT_READY'
  },
  nonMember: {
    enableCheckbox: false, tooltipText: 'LBL_NM_NT_NOTIFY'
  },
  roomReady: {
    enableCheckbox: true, tooltipText: ''
  },
  notified: {
    enableCheckbox: false, tooltipText: 'LBL_GST_NOTIFIED'
  },
  checkedIn: {
    enableCheckbox: false, tooltipText: 'LBL_GUEST_CHECKED_IN'
  }
};

export const defaultGridSettings = {
  rows: {
    title: 'COM_ROWS',
    allOption: true,
    values: [10, 25, 50, 100],
    default: 10
  }
};

export const DefaultGridState: State = {
  take: 10,
  skip: 0,
  filter: { logic: 'and', filters: [] },
  sort: [{ field: 'guest', dir: 'asc' }]
};
