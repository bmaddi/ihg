export const BULK_NOTIFY_ERROR = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_BLK_GST_NTFY'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'LBL_ERR_BLK_GST_NTFY'
  }
};

export const NOTIFY_ERROR_CONTENT = {
  timeout: {
    message: 'COM_TOAST_ERR',
    icon: '',
    showRefreshBtn: true,
    refreshActionText: 'LBL_ERR_TRYAGAIN'
  },
  general: {
    message: 'COM_TOAST_ERR',
    icon: '',
    showRefreshBtn: true,
    refreshActionText: 'LBL_ERR_TRYAGAIN'
  }
};

export const GUEST_COUNT_ERROR_CONTENT = {
  timeout: {
    message: 'LBL_ERR_RET_DATA',
    icon: '',
    showRefreshBtn: true,
    refreshActionText: 'LBL_ERR_REFRESH'
  },
  general: {
    message: 'LBL_ERR_RET_DATA',
    icon: '',
    showRefreshBtn: true,
    refreshActionText: 'LBL_ERR_REFRESH'
  }
};

export const NO_DATA_ERROR_CONTENT = {
  filterSearch: {
    noData: {
      header: 'COM_LBL_NO_RESULTS_FOUND',
      message: 'USR_GUIDANCE_NO_RSLT_MTCH'
    }
  },
  load: {
    noData: {
      header: 'USR_GUIDANCE_NO_ARR_TO_PREP',
      message: 'USR_GUIDANCE_NO_GST_ARVG'
    }
  }
};

export const PRINT_ERROR_CONTENT = {
  retryButton: 'LBL_ERR_TRYAGAIN',
  reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
  timeoutError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'TOAST_ERR_PRT_TRY_AGN'
  },
  generalError: {
    message: 'COM_TOAST_ERR',
    detailMessage: 'TOAST_ERR_PRT_TRY_AGN'
  }
};
