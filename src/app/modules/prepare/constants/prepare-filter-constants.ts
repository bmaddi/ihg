export enum LoyaltyFiltersTypes {
  TOTAL = 'total',
  SPIRE_ELITE = 'spire',
  PLATINUM_ELITE = 'pltnm',
  GOLD_ELITE = 'gold',
  CLUB = 'club',
  NON_MEMBERS = 'non'
}

export enum MiscellaneousFilterTypes {
  VIP = 'vip',
  AMBASSADOR = 'amb',
  INNER_CIRCLE = 'ic',
  KARMA = 'kar',
  EMPLOYEE = 'emp',
  SPECIAL_REQUESTS = 'sr',
  STAY_PREFERENCES = 'sp',
  HEART_BEAT_ACTION = 'hb',
  ARRIVAL_TIME = 'arr',
  COMPANY = 'comp',
  PRIORITY_ENROLLMENTS = 'pe',
  POTENTIAL_MEMBERS = 'pm',
  PRE_ARRIVAL_COMPLETE = 'cmplt',
  PRE_ARRIVAL_INCOMPLETE = 'ncmplt',
  GROUP = 'group'
}

export const loyaltyFiltersDefaultState = [
  { filterName: LoyaltyFiltersTypes.TOTAL, selected: true, label: 'LBL_FILTER_TOTAL' },
  { filterName: LoyaltyFiltersTypes.SPIRE_ELITE, selected: false, label: 'LBL_FILTER_SPIRE' },
  { filterName: LoyaltyFiltersTypes.PLATINUM_ELITE, selected: false, label: 'LBL_FILTER_PLTNM' },
  { filterName: LoyaltyFiltersTypes.GOLD_ELITE, selected: false, label: 'LBL_FILTER_GOLD' },
  { filterName: LoyaltyFiltersTypes.CLUB, selected: false, label: 'LBL_FILTER_CLUB' },
  { filterName: LoyaltyFiltersTypes.NON_MEMBERS, selected: false, label: 'LBL_FILTER_NON' }
];

export const miscFiltersDefaultState = [
  { filterName: MiscellaneousFilterTypes.VIP, selected: false, label: 'LBL_FILTER_VIP' },
  { filterName: MiscellaneousFilterTypes.AMBASSADOR, selected: false, label: 'LBL_FILTER_AMB' },
  { filterName: MiscellaneousFilterTypes.INNER_CIRCLE, selected: false, label: 'LBL_FILTER_IC' },
  { filterName: MiscellaneousFilterTypes.KARMA, selected: false, label: 'LBL_FILTER_KAR' },
  { filterName: MiscellaneousFilterTypes.EMPLOYEE, selected: false, label: 'LBL_FILTER_EMP' },
  { filterName: MiscellaneousFilterTypes.SPECIAL_REQUESTS, selected: false, label: 'LBL_FILTER_SR' },
  { filterName: MiscellaneousFilterTypes.STAY_PREFERENCES, selected: false, label: 'LBL_FILTER_SP' },
  { filterName: MiscellaneousFilterTypes.HEART_BEAT_ACTION, selected: false, label: 'LBL_FILTER_HB' },
  { filterName: MiscellaneousFilterTypes.ARRIVAL_TIME, selected: false, label: 'LBL_FILTER_ARR' },
  { filterName: MiscellaneousFilterTypes.COMPANY, selected: false, label: 'LBL_FILTER_COMP' },
  { filterName: MiscellaneousFilterTypes.GROUP, selected: false, label: 'LBL_FILTER_GRP' },
  { filterName: MiscellaneousFilterTypes.PRIORITY_ENROLLMENTS, selected: false, label: 'LBL_FILTER_PE' },
  { filterName: MiscellaneousFilterTypes.POTENTIAL_MEMBERS, selected: false, label: 'LBL_FILTER_PM' },
  { filterName: MiscellaneousFilterTypes.PRE_ARRIVAL_COMPLETE, selected: false, link: 'cmp', label: 'LBL_FILTER_CMPLT' },
  { filterName: MiscellaneousFilterTypes.PRE_ARRIVAL_INCOMPLETE, selected: false, link: 'cmp', label: 'LBL_FILTER_NCMPLT' }
];
