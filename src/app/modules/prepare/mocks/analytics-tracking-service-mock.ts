import { RM_ASGNMNT_ERR_CODES } from "@app/modules/prepare/components/prepare-arrivals-details/constants/room-assignment-error-code.constant";
import { PrepareForArrivalsGaConstants } from "@app/modules/prepare/enums/prepare-for-arrivals-ga-constants.enum";

export const ACTION = {
  manualRoomAssign: PrepareForArrivalsGaConstants.GA_ACN_MAN_RM_ASGN,
  roomAssign: PrepareForArrivalsGaConstants.ACTION_EXPND_RM_ASSIGNMENT
}

export const TRACK_ERROR_LABELS = {
  [RM_ASGNMNT_ERR_CODES.roomUnavailable]: {
    labels: {
      RoomUpgrade: PrepareForArrivalsGaConstants.FREE_UPGRDE_ERROR,
      NonRoomUpgrade: PrepareForArrivalsGaConstants.GA_LBL_ERR_RM_NOT_AVAIL
    }
  },
  [RM_ASGNMNT_ERR_CODES.inventoryUnavailable]: {
    labels: {
      RoomUpgrade: PrepareForArrivalsGaConstants.FREE_UPGRADE_INV_UNAVLBL,
      NonRoomUpgrade: PrepareForArrivalsGaConstants.GA_LBL_ERR_INV_NOT_AVAIL
    }
  },
  [RM_ASGNMNT_ERR_CODES.inventoryUnavailableGrp]: {
    labels: {
      RoomUpgrade: PrepareForArrivalsGaConstants.FREE_UPGRDE_INV_UNAVLBL_GRP,
      NonRoomUpgrade: PrepareForArrivalsGaConstants.GA_LBL_ERR_INV_NOT_AVAIL_GRP
    }
  },
  [RM_ASGNMNT_ERR_CODES.inventoryUnavailableHouse]: {
    labels: {
      RoomUpgrade: PrepareForArrivalsGaConstants.FREE_UPGRDE_INV_UNAVLBL_HOUSE,
      NonRoomUpgrade: PrepareForArrivalsGaConstants.GA_LBL_ERR_INV_NOT_AVAIL_HOUSE
    }
  },
  Default: {
    labels: {
      manualAssign: PrepareForArrivalsGaConstants.GA_LBL_ERR_FAIL_ASGN_RM_MAN,
      RoomUpgrade: PrepareForArrivalsGaConstants.FREE_UPGRDE_ERROR,
      NonRoomUpgrade: PrepareForArrivalsGaConstants.GA_LBL_ERR_FAIL_ASGN_SUG_MTCH
    }
  }
}