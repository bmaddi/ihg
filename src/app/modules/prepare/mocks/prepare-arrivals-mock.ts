import { GroupList } from '@modules/group-block/models/group-block.model';
import { RoomChangeModel, FilterModel, ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';

const groupList1: GroupList = {
  groupName: 'one',
  groupCode: 'two',
  arrivalDate: 'three',
  departureDate: 'four',
  noOfNights: 5,
  releaseDate: 'six',
  roomsContracted: 7,
  numberOfReservations: 8
};

const groupList2: GroupList = {
  groupName: 'nine',
  groupCode: 'ten',
  arrivalDate: 'elevem',
  departureDate: 'twelve',
  noOfNights: 13,
  releaseDate: 'fourteen',
  roomsContracted: 15,
  numberOfReservations: 16
};

export const MOCK_GROUP_LIST_1: { ddata: GroupList[], refresh: boolean } = {
  ddata: [groupList1, groupList2],
  refresh: true
};

export const MOCK_GROUP_LIST_2: { data: GroupList[], refresh: boolean } = {
  data: [groupList1, groupList2],
  refresh: true
};

export const MOCK_GROUP_LIST_3: { data: GroupList[], rrefresh: boolean } = {
  data: [groupList1, groupList2],
  rrefresh: true
};

export const MOCK_GROUP_LIST_4: { ddata: GroupList[], reefresh: boolean } = {
  ddata: [groupList1, groupList2],
  reefresh: true
};

export const MOCK_RELEASED_ROOM: RoomChangeModel = {
  previousRoomNumber: '2345',
  newRoomNumber: '',
  reservationNumber: '345235'
};

export const mockLoyaltyFilters: FilterModel[] = [
  { filterName: 'total', selected: true, label: 'LBL_FILTER_TOTAL', count: 16 },
  { filterName: 'spire', selected: false, label: 'LBL_FILTER_SPIRE', count: 0 },
  { filterName: 'pltnm', selected: false, label: 'LBL_FILTER_PLTNM', count: 3 },
  { filterName: 'gold', selected: false, label: 'LBL_FILTER_GOLD', count: 3 },
  { filterName: 'club', selected: false, label: 'LBL_FILTER_CLUB', count: 5 },
  { filterName: 'non', selected: false, label: 'LBL_FILTER_NON', count: 5 }
];

export const mockMiscFilters: FilterModel[] = [
  { filterName: 'vip', selected: false, label: 'LBL_FILTER_VIP', count: 10 },
  { filterName: 'amb', selected: false, label: 'LBL_FILTER_AMB', count: 0 },
  { filterName: 'ic', selected: false, label: 'LBL_FILTER_IC', count: 20 },
  { filterName: 'kar', selected: false, label: 'LBL_FILTER_KAR', count: 0 },
  { filterName: 'emp', selected: false, label: 'LBL_FILTER_EMP', count: 0 },
  { filterName: 'sr', selected: false, label: 'LBL_FILTER_SR', count: 0 },
  { filterName: 'sp', selected: false, label: 'LBL_FILTER_SP', count: 0 },
  { filterName: 'hb', selected: false, label: 'LBL_FILTER_HB', count: 0 },
  { filterName: 'arr', selected: false, label: 'LBL_FILTER_ARR', count: 0 },
  { filterName: 'comp', selected: false, label: 'LBL_FILTER_COMP', count: 0 },
  { filterName: 'group', selected: false, label: 'LBL_FILTER_GRP', count: 0 },
  { filterName: 'pe', selected: false, label: 'LBL_FILTER_PE', count: 0 },
  { filterName: 'pm', selected: false, label: 'LBL_FILTER_PM', count: 0 },
  { filterName: 'cmplt', selected: false, label: 'LBL_FILTER_PM', count: 0, link: 'cmp' },
  { filterName: 'ncmplt', selected: false, label: 'LBL_FILTER_PM', count: 0, link: 'cmp' }
];

export const mockArrivalList: ArrivalsListModel[] =
  [
    {
      'firstName': 'Tester1',
      'lastName': 'A',
      'reservationNumber': 'PMS466456',
      'arrivalTime': '',
      'ihgRcNumber': '',
      'ihgRcMembership': '',
      'vipCode': '',
      'companyName': '',
      'groupName': '',
      'rateCategoryCode': 'IGCOR',
      'roomTypeCode': 'KNGN',
      'badges': [],
      'heartBeatActions': null,
      'heartBeatComment': '',
      'heartBeatImprovement': '',
      'assignedRoomNumber': '',
      'assignedRoomStatus': null,
      'numberOfRooms': 2,
      'numberOfNights': 1,
      'preferences': [],
      'specialRequests': [],
      'infoItems': [],
      'pmsReservationNumber': '123456',
      'pmsUniqueNumber': '',
      'prepStatus': 'READY',
      'checkInDate': '2019-08-10',
      'checkOutDate': '2019-08-11',
      'selected': true
    },
    {
      'firstName': 'Raghavendra Naidu',
      'lastName': 'Amilineni',
      'reservationNumber': 'PMS464498',
      'arrivalTime': '',
      'ihgRcNumber': '134311123',
      'ihgRcMembership': 'gold',
      'vipCode': '',
      'companyName': 'corp name test2TEST',
      'groupName': '',
      'rateCategoryCode': 'IGCOR',
      'roomTypeCode': 'KNGN',
      'badges': [
        'gold',
        'employee',
        'priorityEnrollment'
      ],
      'heartBeatActions': {
        'myHotelActions': [
          'GuestComment'
        ],
        'otherHotelActions': [
          'GuestLove'
        ]
      },
      'heartBeatComment': '999 characters - Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
      'heartBeatImprovement': '999 characters - Sed ut perspiciatis unde omnis iste natus error sit.',
      'assignedRoomNumber': '',
      'assignedRoomStatus': null,
      'numberOfRooms': 1,
      'numberOfNights': 6,
      'preferences': [],
      'specialRequests': [],
      'infoItems': [],
      'pmsReservationNumber': '',
      'pmsUniqueNumber': '121345',
      'prepStatus': 'INPROGRESS',
      'checkInDate': '2019-08-10',
      'checkOutDate': '2019-08-11',
      'doNotMoveRoom': true
    },
    {
      'firstName': 'Test',
      'lastName': 'Apitester',
      'reservationNumber': 'PMS466473',
      'arrivalTime': '',
      'ihgRcNumber': '134311213',
      'ihgRcMembership': 'club',
      'vipCode': '',
      'companyName': '',
      'groupName': '',
      'rateCategoryCode': 'IGCOR',
      'roomTypeCode': 'KNGN',
      'badges': [
        'club'
      ],
      'heartBeatActions': {
        'myHotelActions': [
          'GuestLove'
        ],
        'otherHotelActions': []
      },
      'heartBeatComment': '',
      'heartBeatImprovement': '',
      'assignedRoomNumber': '444',
      'assignedRoomStatus': {
        'roomStatus': 'Clean',
        'frontOfficeStatus': 'Vacant',
        'houseKeepingStatus': 'Clean',
        'roomNumber': '444',
        'roomType': 'KNGN',
        'features': [
          {
            'code': 'BL',
            'description': 'Balcony'
          },
          {
            'code': 'BR',
            'description': 'Business Room'
          },
          {
            'code': 'BV',
            'description': 'Bay View'
          },
          {
            'code': 'CR',
            'description': 'Corner Room'
          },
          {
            'code': 'CV',
            'description': 'City View'
          },
          {
            'code': 'GV',
            'description': 'Garden View'
          },
          {
            'code': 'HF',
            'description': 'High Floor'
          },
          {
            'code': 'OV',
            'description': 'Ocean View'
          }
        ],
        'reservationFeatures': [
          {
            'code': 'BL',
            'type': 'R',
            'description': 'Balcony',
            'matched': false
          },
          {
            'code': 'CV',
            'type': 'R',
            'description': 'City View',
            'matched': true
          },
          {
            'code': 'GV',
            'type': 'R',
            'description': 'Garden View',
            'matched': true
          },
          {
            'code': 'HF',
            'type': 'R',
            'description': 'High Floor Near Elevator',
            'matched': true
          },
          {
            'code': 'OV',
            'type': 'R',
            'description': 'Ocean View',
            'matched': false
          }
        ]
      },
      'numberOfRooms': 1,
      'numberOfNights': 1,
      'preferences': [],
      'specialRequests': [
        'High Floor Near Elevator High Floor Near Elevator High Floor Near Elevator'
      ],
      'infoItems': [],
      'pmsReservationNumber': '',
      'pmsUniqueNumber': '',
      'tasks': [
        {
          'traceId': 5654,
          'traceText': 'Block Code PHX has been removed. *WT:OXI',
          'departmentCode': 'ZWN',
          'departmentName': 'CRS Warning Trace',
          'traceTimestamp': '',
          'isResolved': true
        }
      ],
      'prepStatus': 'INHOUSE',
      'checkInDate': '2019-08-10',
      'checkOutDate': '2019-08-11',
      'doNotMoveRoom': true
    },
    {
      'firstName': 'Mahesh',
      'lastName': 'B',
      'reservationNumber': 'PMS464500',
      'arrivalTime': '',
      'ihgRcNumber': '134311124',
      'ihgRcMembership': 'non',
      'vipCode': '',
      'companyName': '',
      'groupName': '',
      'rateCategoryCode': 'IGCOR',
      'roomTypeCode': 'KNGN',
      'badges': [],
      'heartBeatActions': null,
      'heartBeatComment': '',
      'heartBeatImprovement': '',
      'assignedRoomNumber': '',
      'assignedRoomStatus': null,
      'numberOfRooms': 1,
      'numberOfNights': 1,
      'preferences': [],
      'specialRequests': [],
      'infoItems': [],
      'pmsReservationNumber': '',
      'pmsUniqueNumber': '',
      'tasks': [
        {
          'traceId': 5656,
          'traceText': 'Block Code PHX has been removed. *WT:OXI',
          'departmentCode': 'ZWN',
          'departmentName': 'CRS Warning Trace',
          'traceTimestamp': '',
          'isResolved': false
        },
        {
          'traceId': 5657,
          'traceText': 'Block Code PHX has been removed. *WT:OXI',
          'departmentCode': 'ZWN',
          'departmentName': 'CRS Warning Trace',
          'traceTimestamp': '',
          'isResolved': true
        }
      ],
      'checkInDate': '2019-04-17',
      'checkOutDate': '2019-04-18',
      'inspected': true,
      'doNotMoveRoom': false,
      'selected': true
    }
  ];

export const arrivalsChecklistMock = [
  {
    'firstName': 'Tester1',
    'lastName': 'A',
    'reservationNumber': 'PMS466456',
    'arrivalTime': '',
    'ihgRcNumber': '',
    'ihgRcMembership': '',
    'vipCode': '',
    'companyName': '',
    'groupName': '',
    'rateCategoryCode': 'IGCOR',
    'roomTypeCode': 'KNGN',
    'badges': [],
    'heartBeatActions': null,
    'heartBeatComment': '',
    'heartBeatImprovement': '',
    'assignedRoomNumber': '',
    'assignedRoomStatus': null,
    'numberOfRooms': 2,
    'numberOfNights': 1,
    'preferences': [],
    'specialRequests': [],
    'infoItems': [],
    'pmsReservationNumber': '123456',
    'pmsUniqueNumber': '',
    'prepStatus': 'READY',
    'checkInDate': '2019-08-10',
    'checkOutDate': '2019-08-11'
  },
  {
    'firstName': 'Raghavendra Naidu',
    'lastName': 'Amilineni',
    'reservationNumber': 'PMS464498',
    'arrivalTime': '',
    'ihgRcNumber': '134311123',
    'ihgRcMembership': 'gold',
    'vipCode': '',
    'companyName': 'corp name test2TEST',
    'groupName': '',
    'rateCategoryCode': 'IGCOR',
    'roomTypeCode': 'KNGN',
    'badges': [
      'gold',
      'employee',
      'priorityEnrollment'
    ],
    'heartBeatActions': {
      'myHotelActions': [
        'GuestComment'
      ],
      'otherHotelActions': [
        'GuestLove'
      ]
    },
    // tslint:disable-next-line:max-line-length
    'heartBeatComment': '999 characters - Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
    'heartBeatImprovement': '999 characters - Sed ut perspiciatis unde omnis iste natus error sit.',
    'assignedRoomNumber': '',
    'assignedRoomStatus': null,
    'numberOfRooms': 1,
    'numberOfNights': 6,
    'preferences': [],
    'specialRequests': [],
    'infoItems': [],
    'pmsReservationNumber': '',
    'pmsUniqueNumber': '121345',
    'prepStatus': 'INPROGRESS',
    'checkInDate': '2019-08-10',
    'checkOutDate': '2019-08-11',
    'doNotMoveRoom': true
  },
  {
    'firstName': 'Test',
    'lastName': 'Apitester',
    'reservationNumber': 'PMS466473',
    'arrivalTime': '',
    'ihgRcNumber': '134311213',
    'ihgRcMembership': 'club',
    'vipCode': '',
    'companyName': '',
    'groupName': '',
    'rateCategoryCode': 'IGCOR',
    'roomTypeCode': 'KNGN',
    'badges': [
      'club'
    ],
    'heartBeatActions': {
      'myHotelActions': [
        'GuestLove'
      ],
      'otherHotelActions': []
    },
    'heartBeatComment': '',
    'heartBeatImprovement': '',
    'assignedRoomNumber': '444',
    'assignedRoomStatus': {
      'roomStatus': 'Clean',
      'frontOfficeStatus': 'Vacant',
      'houseKeepingStatus': 'Clean',
      'roomNumber': '444',
      'roomType': 'KNGN',
      'features': [
        {
          'code': 'BL',
          'description': 'Balcony'
        },
        {
          'code': 'BR',
          'description': 'Business Room'
        },
        {
          'code': 'BV',
          'description': 'Bay View'
        },
        {
          'code': 'CR',
          'description': 'Corner Room'
        },
        {
          'code': 'CV',
          'description': 'City View'
        },
        {
          'code': 'GV',
          'description': 'Garden View'
        },
        {
          'code': 'HF',
          'description': 'High Floor'
        },
        {
          'code': 'OV',
          'description': 'Ocean View'
        }
      ],
      'reservationFeatures': [
        {
          'code': 'BL',
          'type': 'R',
          'description': 'Balcony',
          'matched': false
        },
        {
          'code': 'CV',
          'type': 'R',
          'description': 'City View',
          'matched': true
        },
        {
          'code': 'GV',
          'type': 'R',
          'description': 'Garden View',
          'matched': true
        },
        {
          'code': 'HF',
          'type': 'R',
          'description': 'High Floor Near Elevator',
          'matched': true
        },
        {
          'code': 'OV',
          'type': 'R',
          'description': 'Ocean View',
          'matched': false
        }
      ]
    },
    'numberOfRooms': 1,
    'numberOfNights': 1,
    'preferences': [],
    'specialRequests': [
      'High Floor Near Elevator High Floor Near Elevator High Floor Near Elevator'
    ],
    'infoItems': [],
    'pmsReservationNumber': '',
    'pmsUniqueNumber': '',
    'tasks': [
      {
        'traceId': 5654,
        'traceText': 'Block Code PHX has been removed. *WT:OXI',
        'departmentCode': 'ZWN',
        'departmentName': 'CRS Warning Trace',
        'traceTimestamp': '',
        'isResolved': true
      }
    ],
    'prepStatus': 'INHOUSE',
    'checkInDate': '2019-08-10',
    'checkOutDate': '2019-08-11',
    'doNotMoveRoom': true
  },
];

export const prepareArrivalsResponse = {
  'paginatedArrivalsCheckList': {
    'pageNumber': 1,
    'pageSize': 10,
    'totalPages': 1,
    'totalItems': 3,
    'startItem': 1,
    'endItem': 3,
    'items': arrivalsChecklistMock
  },
  'counters': [
    {
      'filterName': 'amb',
      'count': 1
    },
    {
      'filterName': 'arr',
      'count': 0
    },
    {
      'filterName': 'club',
      'count': 5
    },
    {
      'filterName': 'cmplt',
      'count': 0
    },
    {
      'filterName': 'comp',
      'count': 4
    },
    {
      'filterName': 'emp',
      'count': 3
    },
    {
      'filterName': 'gold',
      'count': 3
    },
    {
      'filterName': 'group',
      'count': 3
    },
    {
      'filterName': 'hb',
      'count': 2
    },
    {
      'filterName': 'ic',
      'count': 1
    },
    {
      'filterName': 'kar',
      'count': 0
    },
    {
      'filterName': 'ncmplt',
      'count': 18
    },
    {
      'filterName': 'non',
      'count': 5
    },
    {
      'filterName': 'pe',
      'count': 0
    },
    {
      'filterName': 'pltnm',
      'count': 3
    },
    {
      'filterName': 'pm',
      'count': 0
    },
    {
      'filterName': 'rr',
      'count': 0
    },
    {
      'filterName': 'sp',
      'count': 2
    },
    {
      'filterName': 'spire',
      'count': 0
    },
    {
      'filterName': 'sr',
      'count': 0
    },
    {
      'filterName': 'total',
      'count': 16
    },
    {
      'filterName': 'vip',
      'count': 0
    }
  ],
  'checklistActionsEnabled': false,
  'localDate': '2019-11-27',
  'operaHotelFlag': true,
  'hotelCheckInTime': '15:00:00',
  'hotelCheckOutTime': '19:00:00'
};

