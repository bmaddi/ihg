import { NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";

export class MockArrivalsService {
  handleUnsavedTasks(callbackFn) { callbackFn(); }
}

export class MockGAService {
  trackEvent(ec: string, ea: string, el: string) { }
}

export class MockNgbModal {
  open(content: any, options?: NgbModalOptions) { }
}

export class MockNgbActiveModal {
  dismiss() { }
  close() { }
}

export class MockGuestNotificationService {
  notifyGuest() { }
  emitNotifyGuestStatus() { }
}

export class MockAnalyticsTrackingService {
  trackBulkNotifyWarning() { }
  trackBulkNotifyNoWarning() { }
}