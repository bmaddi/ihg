import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { Room } from '@modules/prepare/components/prepare-arrivals-details/models/rooms-list.model';
import { CommentModel } from '@app/modules/do-not-move/models/do-not-move.models';

export interface CountersModel {
  filterName: string;
  count: number;
}

export interface HeartBeatActionsModel {
  myHotelActions: Array<string>;
  otherHotelActions: Array<string>;
}

export interface TasksModel {
  traceId: number;
  traceText: string;
  departmentCode: string;
  departmentName: string;
  traceTimestamp: string;
  isResolved: boolean;
}

export interface InfoItemsModel {
  type: string;
  detailText: string;
  detailCode: string;
  indicator: string;
  createTimestamp: string;
}

export interface ArrivalsListModel {
  firstName: string;
  lastName: string;
  reservationNumber: string;
  pmsReservationNumber: string;
  pmsUniqueNumber?: string;
  pmsReservationStatus?: string;
  arrivalTime: string;
  checkInDate: string;
  checkoutDate?: string;
  rateCategoryCode: string;
  roomTypeCode: string;
  ihgRcNumber: string;
  ihgRcMembership: string;
  companyName: string;
  groupName: string;
  badges: Array<string>;
  expanded?: boolean;
  preferences?: Array<string>;
  specialRequests?: Array<string>;
  heartBeatActions?: HeartBeatActionsModel;
  heartBeatComment: string;
  heartBeatImprovement: string;
  tasks?: Array<TasksModel>;
  assignedRoomNumber: string;
  assignedRoomStatus: Room;
  vipCode: string;
  disableManualAssign?: boolean;
  showManualGrid?: boolean;
  done?: boolean;
  numberOfNights: number;
  infoItems?: Array<InfoItemsModel>;
  viewTasks?: boolean;
  selected?: boolean;
  notified?: string;
  checkOutDate?: string;
  prepStatus?: string;
  inspected?: boolean;
  isCheckedIn?: boolean;
  ineligibleReservation?: boolean;
  numberOfRooms: number;
  doNotMoveRoom?: boolean;
  previousAssignedRoomNumber?: string;
  roomToChargeCode?: string;
  origRoomTypeCode?: string;
  dnmReason?: CommentModel;
  freeUpgradeInd?: boolean;
}

export interface PaginatedArrivalsCheckListModel {
  pageNumber: number;
  pageSize: number;
  totalPages: number;
  totalItems: number;
  startItem: number;
  endItem: number;
  items: Array<ArrivalsListModel>;
}

export interface ArrivalsChecklistModel {
  errors?: Array<ErrorsModel>;
  counters: Array<CountersModel>;
  checklistActionsEnabled: boolean;
  localDate: string;
  operaHotelFlag: boolean;
  paginatedArrivalsCheckList: PaginatedArrivalsCheckListModel;
}

export interface NotifyGuestModel {
  actionItemCode: string;
  ihgRcNumber: string;
  reservationNumber: string;
  guestNotifyStatus?: boolean;
  guestNotifyTimeStamp?: string;
  errors?: Array<ErrorsModel>;
}

export interface ArrivalsDateRangeModel {
  label: string;
  value: Date;
}

export interface FilterModel extends CountersModel {
  selected: boolean;
  label: string;
  link?: string;
}

export interface PrepareReservationData {
  reservationData?: ArrivalsListModel;
  errors?: ErrorsModel[];
}

export interface RoomChangeModel {
  previousRoomNumber: string;
  newRoomNumber: string;
  reservationNumber: string;
}
