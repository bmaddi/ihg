import { ArrivalsListModel } from '@app/modules/prepare/models/arrivals-checklist.model';

export interface NotifyList {
  items: Array<ArrivalsListModel>;
  type: NotifyListTypes;
}

export enum NotifyListTypes {
  BOTH = 'both',
  ALL_UNRESOLVED = 'unresolved',
  ALL_RESOLVED = 'resolved',
}

export enum OrderingTaskList {
  ORDERED = 'ordered',
  ALL_UNRESOLVED = 'unresolved',
  ALL_RESOLVED = 'resolved',
}