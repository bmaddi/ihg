import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { AppSharedModule } from '@app/modules/shared/app-shared.module';

import { PrepareArrivalsComponent } from '@modules/prepare/components/prepare-arrivals/prepare-arrivals.component';
import { PrepareArrivalsGridComponent } from './components/prepare-arrivals-grid/prepare-arrivals-grid.component';
import { PrepareArrivalsGridPagerComponent } from './components/prepare-arrivals-grid-pager/prepare-arrivals-grid-pager.component';

import { LoyaltyFiltersComponent } from './components/loyalty-filters/loyalty-filters.component';
import { MiscFiltersComponent } from './components/misc-filters/misc-filters.component';
import { PrepareArrivalsDetailsModule } from './components/prepare-arrivals-details/prepare-arrivals-details.module';
import { PrepareArrivalsGridActionsComponent } from './components/prepare-arrivals-grid-actions/prepare-arrivals-grid-actions.component';
import { WarningNotifyGuestModalComponent } from './components/warning-notify-guest-modal/warning-notify-guest-modal.component';
import { BulkNotifyGuestModalComponent } from './components/bulk-notify-guest-modal/bulk-notify-guest-modal.component';
import { BulkNotifyComponent } from '@app/modules/prepare/components/bulk-notify/bulk-notify.component';
import { WarningRoomUnassignedModalComponent } from './components/warning-room-unassigned-modal/warning-room-unassigned-modal.component';
import { TraceListComponent } from '@app/modules/prepare/components/trace-list/trace-list.component';
import { BadgeListModule } from '@app/modules/badge-list/badge-list.module';
import { ProgressStatusModule } from '@app/modules/progress-status/progress-status.module';
import { PrintRecordModule } from '@app/modules/print-record/print-record.module';
import { GuestSearchModule } from '@modules/guest-search/guest-search.module';
import { GroupBlockModule } from '@modules/group-block/group-block.module';
import { HouseKeepingStatusSelectorComponent } from './components/house-keeping-status-selector/house-keeping-status-selector.component';

@NgModule({
  imports: [
    CommonModule,
    AppSharedModule,
    FormsModule,
    NgbModule,
    TranslateModule,
    GridModule,
    DropDownsModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    PrepareArrivalsDetailsModule,
    RouterModule,
    BadgeListModule,
    ProgressStatusModule,
    PrintRecordModule,
    GuestSearchModule,
    GroupBlockModule
  ],
  declarations: [
    PrepareArrivalsComponent,
    PrepareArrivalsGridComponent,
    PrepareArrivalsGridPagerComponent,
    LoyaltyFiltersComponent,
    MiscFiltersComponent,
    PrepareArrivalsGridActionsComponent,
    WarningRoomUnassignedModalComponent,
    WarningNotifyGuestModalComponent,
    BulkNotifyGuestModalComponent,
    BulkNotifyComponent,
    TraceListComponent,
    HouseKeepingStatusSelectorComponent
  ],
  exports: [
    PrepareArrivalsComponent,
    PrepareArrivalsGridPagerComponent,
    RouterModule
  ],
  entryComponents: [
    WarningRoomUnassignedModalComponent,
    WarningNotifyGuestModalComponent,
    BulkNotifyGuestModalComponent
  ]
})
export class PrepareModule {
}
