import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import * as moment from 'moment';
import { State, FilterDescriptor } from '@progress/kendo-data-query';
import { TranslateService } from '@ngx-translate/core';

import { UserService, UserConfirmationModalConfig, TransitionGuardService, GuardRegistrationParams } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { environment } from '@env/environment';
import { AppConstants } from '@app/constants/app-constants';
import {
  ArrivalsChecklistModel, FilterModel, CountersModel, TasksModel, PrepareReservationData, ArrivalsListModel
} from '../../models/arrivals-checklist.model';
import { loyaltyFiltersDefaultState, miscFiltersDefaultState } from '../../constants/prepare-filter-constants';
import { EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

@Injectable({
  providedIn: 'root'
})
export class ArrivalsService {
  private readonly API_URL = environment.fdkAPI;
  private maxSelectionCount = 10;
  public refreshGridEvent = new Subject<void>();
  public refreshTasksStatus = new Subject<void>();
  public hasUnsavedTask = false;
  public taskChangeSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public printPrepareError = new Subject<EmitErrorModel>();
  public retryPrint = new Subject<void>();

  public ifCheckInPage = false;
  public roomAssignementListItem: ArrivalsListModel;
  public creditCardUnsavedData = false;
  public dnmReasonUnsavedData = false;
  public dnmReasonData = '';
  public authDetailsChangeSubject: BehaviorSubject<any> = new BehaviorSubject(null); 
  private listnerTabChange = new Subject<null>();

  public listenTabChange(): Observable<null> {
    return this.listnerTabChange.asObservable();
  }

  public emitTabChange() {
    this.listnerTabChange.next();
  }

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private modalService: ConfirmationModalService,
    private transitionGuardService: TransitionGuardService,
    private translate: TranslateService) {
  }

  public discardUnsavedDoNotMoveReason() {
    if (!this.getUnsavedTask() && !this.creditCardUnsavedData && this.dnmReasonData) {
      this.setGenericUnsavedDataChange(false);
    }
    this.clearUnsavedDnsReason();
  }

  public clearUnsavedDnsReason() {
    this.dnmReasonUnsavedData = false;
    this.dnmReasonData = '';
  }

  public registerInTransitionGuard() {
    this.transitionGuardService.register(<GuardRegistrationParams>{
      dataChangesObserver: this.authDetailsChangeSubject.asObservable(),
      useCancelSaveDiscardModal: false,
      userConfirmModalConfig: this.getConfirmModalConfig(),
      noToastMessage: true,
      onSave: null,
      onDiscard: () => {
        return this.handleConfirmActions(true);
      },
      onCancel: () => {
        return this.handleConfirmActions(false);
      }
    });
  }

  private handleConfirmActions(shouldDiscard: boolean): Observable<boolean> {
    const subject = new Subject<boolean>();
    setTimeout(() => {
      if(shouldDiscard) {
        if(this.getUnsavedTask()) {
          this.discardUnsentTasks(this.roomAssignementListItem);
          this.setUnsavedTask(false);
        } 
        if(this.dnmReasonData) {
          this.discardUnsavedDoNotMoveReason();
        }       
      } else {
        this.emitTabChange();
      }
      subject.next(shouldDiscard);
      this.setGenericUnsavedDataChange(!shouldDiscard);
    });
    return subject.asObservable();
  }

  public setGenericUnsavedDataChange(dataChanged : boolean) {
    this.authDetailsChangeSubject.next(dataChanged);
  }

  public getArrivalsChecklist(selectedDate: Date = null, state: State, searchBy?: string): Observable<ArrivalsChecklistModel> {
    const params = this.getServiceParams(selectedDate, state, searchBy);
    const apiURl = `${this.API_URL}prepareArrivals/checkList/${this.getLocationId()}?${params}`;
    return this.http.get(apiURl).pipe(map((response: ArrivalsChecklistModel) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getServiceParams(selectedDate: Date = null, state: State, searchBy?: string): string {
    const brandParam = `brandCode=${this.getBrandCode()}`;
    const dateParam = selectedDate ? `&date=${moment(selectedDate).format(AppConstants.DB_DT_FORMAT)}` : '';
    const filterSearchParam = `${this.getFilterParams(state, searchBy)}${searchBy ? `&searchColumn=group` : ''}`;
    return `${brandParam}${dateParam}${this.getPageParams(state)}${this.getSortParams(state)}${filterSearchParam}`;
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  private getBrandCode(): string {
    return this.userService.getCurrentLocation().brandCode;
  }

  private getPageParams(state: State): string {
    const page = (state.skip / state.take) + 1;
    return `&page=${page}&pageSize=${state.take}`;
  }

  private getSortParams(state: State): string {
    const sorters = state.sort;
    if (sorters && sorters[0]) {
      const sortByString = sorters[0].field === 'guest' ? `&sortBy=lastName,firstName` : `&sortBy=${sorters[0].field}`;
      return encodeURI(`${sortByString}&sortDirection=${sorters[0].dir}`);
    }
    return '';
  }

  private getFilterParams(state: State, searchBy?: string): string {
    const params = state.filter;
    if (params && params.filters.length) {
      const filterString = params.filters.map((filter: FilterDescriptor) => {
        const { field } = filter;
        let { value } = filter;
        if (field === 'search') {
          value = encodeURIComponent(searchBy || value);
        }
        return `${field}=${value}`;
      }).join('&');
      return '&' + filterString;
    }
    return '';
  }

  getLoyaltyFilters(filterCounters: CountersModel[], selectedFilters: string[]): FilterModel[] {
    return loyaltyFiltersDefaultState.map(item => {
      item['count'] = this.getFilterCount(filterCounters, item.filterName);
      item.selected = selectedFilters.indexOf(item.filterName) !== -1;
      return item as FilterModel;
    });
  }

  private getFilterCount(filterCounters: CountersModel[], filterName: string): number {
    const filterItem = filterCounters.find(filter => filter.filterName === filterName);
    return filterItem ? filterItem.count : 0;
  }

  getMiscFilters(filterCounters: CountersModel[], checklistActionsEnabled: boolean, selectedFilters: string[]): FilterModel[] {
    const miscFilters = miscFiltersDefaultState.map(item => {
      item['count'] = this.getFilterCount(filterCounters, item.filterName);
      item.selected = selectedFilters.indexOf(item.filterName) !== -1;
      return item as FilterModel;
    });
    return checklistActionsEnabled ? miscFilters : miscFilters.slice(0, 13);
  }

  handleUnsavedTasks(callBackAction: Function, cancelCallBackFn?: Function, dataItem?): void {
    if ((this.getUnsavedTask() || this.dnmReasonData) && !this.ifCheckInPage) {
      this.openUnsentTaskWarningModal().subscribe(
        (proceed) => {
          this.taskChangeSubject.next(false);
          this.discardUnsentTasks(dataItem);
          this.setUnsavedTask(false);
          if(this.dnmReasonData) {
            this.clearUnsavedDnsReason();
          }
          this.executeCallBack(callBackAction);
        },
        (cancel) => {
          this.executeCallBack(cancelCallBackFn);
          return false;
        });
    } else {
      this.executeCallBack(callBackAction);
    }
  }

  discardUnsentTasks(dataItem): void {
    if (dataItem && dataItem.tasks) {
      dataItem.tasks = dataItem.tasks.filter((task: TasksModel) => task.isResolved !== null);
    }
  }

  private executeCallBack(callBackAction: Function): void {
    if (callBackAction) {
      callBackAction();
    }
  }

  private openUnsentTaskWarningModal() {
    return this.modalService.openConfirmationModal(this.getConfirmModalConfig());
  }

  getConfirmModalConfig(): UserConfirmationModalConfig {
    return {
      modalHeader: 'COM_SAVE_DISCARD_MOD_TITLE',
      primaryButtonLabel: 'COM_BTN_CONT',
      primaryButtonClass: 'btn-primary',
      defaultButtonLabel: 'COM_BTN_CANCEL',
      defaultButtonClass: 'btn-default',
      messageHeader: 'LBL_GST_UPDATE_TEXT',
      messageBody: '',
      windowClass: 'modal-medium',
      dismissible: true
    };
  }

  setUnsavedTask(unsavedTask: boolean): void {
    this.hasUnsavedTask = unsavedTask;
  }

  getUnsavedTask(): boolean {
    return this.hasUnsavedTask;
  }

  openNotificationLimitWarning(): Observable<void> {
    return this.modalService.openConfirmationModal({
      modalHeader: this.translate.instant('LBL_MOD_TTL_MAX_NOTM_LIM', { guestCount: this.maxSelectionCount }),
      primaryButtonLabel: 'COM_BTN_CLOSE',
      primaryButtonClass: 'btn-primary',
      messageHeader: this.translate.instant('LBL_MOD_TXT_MAX_NOTM_LIM', { guestCount: this.maxSelectionCount }),
      windowClass: 'notification-limit modal-medium',
      dismissible: true
    });
  }

  openAuthorizeCardWarning(): Observable<void> {
    return this.modalService.openConfirmationModal({
      modalHeader: 'LBL_WARN_CHKIN_PAYMT_TTTL',
      primaryButtonLabel: 'LBL_AUTH_CARD_FILE',
      primaryButtonClass: 'btn-primary',
      messageHeader: 'LBL_WARN_CHKIN_PAYMT_DESC',
      windowClass: 'notification-limit modal-medium',
      dismissible: true
    });
  }

  getPrepareReservationData(confirmationNumber: string): Observable<PrepareReservationData> {
    const params = `confirmationNumber=${confirmationNumber}&brandCode=${this.getBrandCode()}`;
    const apiURl = `${this.API_URL}prepareArrivals/reservation/${this.getLocationId()}?${params}`;
    return this.http.get(apiURl).pipe(map((response: PrepareReservationData) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}
