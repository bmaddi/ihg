import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError, Subject } from 'rxjs';
import * as moment from 'moment';

import { UserService } from 'ihg-ng-common-core';
import { AppConstants } from '@app/constants/app-constants';
import { environment } from '@env/environment';
import { ArrivalsListModel, NotifyGuestModel } from '@modules/prepare/models/arrivals-checklist.model';

@Injectable({
  providedIn: 'root'
})
export class GuestNotificationService {
  private readonly API_URL = environment.fdkAPI;
  public refreshGuestsNotfiedStatus = new Subject<NotifyGuestModel[]>();

  constructor(private http: HttpClient, private userService: UserService) {
  }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public notifyGuest(selectedDate: Date, guests: ArrivalsListModel[]): Observable<NotifyGuestModel[]> {
    const arrivalDate = selectedDate ? moment(selectedDate).format(AppConstants.DB_DT_FORMAT) : moment().format(AppConstants.DB_DT_FORMAT);
    const apiURl = `${this.API_URL}prepareArrivals/notifyGuest/${this.getLocationId()}?arrivalDate=${arrivalDate}`;
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.put(apiURl, this.getPayload(guests), httpOptions).pipe(
      map((response: NotifyGuestModel[]) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getPayload(guests: ArrivalsListModel[]): NotifyGuestModel[] {
    const payload: NotifyGuestModel[] = [];
    guests.forEach((guest: ArrivalsListModel) => {
      payload.push({
        ihgRcNumber: guest.ihgRcNumber,
        reservationNumber: guest.reservationNumber,
        actionItemCode: 'room-ready'
      });
    });
    return payload;
  }

  public emitNotifyGuestStatus(response: NotifyGuestModel[]) {
    if (response && response.length) {
      this.refreshGuestsNotfiedStatus.next(response);
    }
  }

  public getGuestNotifiedAction(): Observable<NotifyGuestModel[]> {
    return this.refreshGuestsNotfiedStatus.asObservable();
  }

}
