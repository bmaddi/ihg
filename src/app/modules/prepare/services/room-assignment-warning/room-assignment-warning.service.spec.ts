import { TestBed } from '@angular/core/testing';

import { RoomAssignmentWarningService } from './room-assignment-warning.service';

describe('RoomAssignmentWarningService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoomAssignmentWarningService = TestBed.get(RoomAssignmentWarningService);
    expect(service).toBeTruthy();
  });
});
