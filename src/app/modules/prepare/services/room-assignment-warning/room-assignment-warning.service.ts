import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
// tslint:disable-next-line:max-line-length
import { WarningRoomUnassignedModalComponent } from '@modules/prepare/components/warning-room-unassigned-modal/warning-room-unassigned-modal.component';

@Injectable({
  providedIn: 'root'
})
export class RoomAssignmentWarningService {

  constructor(private ngbModal: NgbModal) { }

  openWarningRoomUnassignedModal(listItem: ArrivalsListModel, enableRoomMove: boolean = false, stateName: string, hasTasks: boolean): Observable<string> {
    const modal: NgbModalRef = this.ngbModal.open(WarningRoomUnassignedModalComponent, {backdrop: 'static', windowClass: 'modal-medium'});
    const modalComponent = modal.componentInstance as WarningRoomUnassignedModalComponent;
    modalComponent.roomNo = enableRoomMove ? listItem.previousAssignedRoomNumber : listItem.assignedRoomStatus.roomNumber;
    modalComponent.arrival = listItem;
    modalComponent.enableRoomMove = enableRoomMove;
    modalComponent.stateName = stateName;
    modalComponent.hasTasks = hasTasks;
    return from(modal.result);
  }
}
