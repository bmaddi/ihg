import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestListTableComponent } from './guest-list-table.component';

xdescribe('GuestListTableComponent', () => {
  let component: GuestListTableComponent;
  let fixture: ComponentFixture<GuestListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
