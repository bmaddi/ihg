import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { AppConstants } from '@app/constants/app-constants';
import { UtilityService } from '@app/services/utility/utility.service';
import { GuestGridModel } from '@app/modules/guests/models/guest-list.models';
import { ReservationStatus } from '@app/modules/guests/enums/guest-arrivals.enums';

@Component({
  selector: 'app-guest-list-table',
  templateUrl: './guest-list-table.component.html',
  styleUrls: ['./guest-list-table.component.scss']
})
export class GuestListTableComponent implements OnInit {

  public noDataSymbol = AppConstants.EMDASH;
  public maxCharCount = 50;

  @Input() items: ArrivalsListModel[] | GuestGridModel[];
  @Input() allowManage: boolean;
  @Input() isTodaySelected: boolean;
  @Input() detailed: boolean;
  @Input() otherTab: string;
  @Input() searchDate = moment().toDate();
  @Input() localDate = moment().toDate();

  constructor(public util: UtilityService, private translate: TranslateService) { }

  ngOnInit() {
  }

  public getActionColumnValue(item: GuestGridModel): string {
    return this.isSameDate() ? this.shouldShowViewText(item) ? 'LBL_GST_VIEW' : 'LBL_CHK_IN'
      : 'LBL_GST_TAB_PREPARE';
  }

  private isSameDate(): boolean {
    return moment(this.getFormattedDate(this.searchDate)).isSame(moment(this.getFormattedDate(this.localDate)));
  }

  private getFormattedDate(date: Date): string {
    return moment(date).format(AppConstants.DB_DT_FORMAT);
  }

  private isToday(date: string): boolean {
    return moment(date).startOf('day').isSame(moment(this.localDate).startOf('day'));
  }

  public getDateValue(date: string): string {
    if (this.isToday(date)) {
      return this.translate.instant('LBL_TODAY');
    } else if (this.isTomorrow(date)) {
      return this.translate.instant('LBL_TMW');
    }
    return moment(date).format(AppConstants.VW_DT_FORMAT).toUpperCase();
  }

  private isTomorrow(date: string): boolean {
    return moment(date).startOf('day').isSame(moment(this.localDate).add(1, 'days').startOf('day'));
  }

  private shouldShowViewText(item: GuestGridModel): boolean {
    return item.ineligibleReservation ||
      item.prepStatus === ReservationStatus.inHouse ||
      item.prepStatus === ReservationStatus.checkedIn;
  }

  formatDate(date: string) {
    return moment(date).format('DDMMMYYYY').toUpperCase();
  }
}
