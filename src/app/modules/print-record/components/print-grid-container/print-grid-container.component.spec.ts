import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintGridContainerComponent } from './print-grid-container.component';

xdescribe('PrintGridContainerComponent', () => {
  let component: PrintGridContainerComponent;
  let fixture: ComponentFixture<PrintGridContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintGridContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintGridContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
