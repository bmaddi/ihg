import { Component, Input, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { PrintService, PrintData } from 'ihg-ng-common-pages';

import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { printGridStyle } from '../../constants/print-grid-style.constant';
import { GuestGridModel } from '@app/modules/guests/models/guest-list.models';

@Component({
  selector: 'app-print-grid-container',
  templateUrl: './print-grid-container.component.html',
  styleUrls: ['./print-grid-container.component.scss']
})
export class PrintGridContainerComponent implements OnInit, OnDestroy {

  public guestList: ArrivalsListModel[] | GuestGridModel[];
  private rootSubscription = new Subscription();

  @Input() selectedDate: Date;
  @Input() localDate: Date;
  @Input() searchString: string;
  @Input() filters: string[];
  @Input() allowManage: boolean;
  @Input() isTodaySelected: boolean;
  @Input() printGuestList: Observable<ArrivalsListModel[] | GuestGridModel[]>;
  @Input() detailed: boolean;
  @Input() otherTab = '';
  @ViewChild('printableContent') printableContent: ElementRef;

  constructor(private printService: PrintService) { }

  ngOnInit() {
    this.subscribeToPrintGuestList();
  }

  private subscribeToPrintGuestList(): void {
    this.rootSubscription.add(
      this.printGuestList.subscribe((guestList: ArrivalsListModel[] | GuestGridModel[]) => {
        this.guestList = guestList;
        setTimeout(() => {
          this.handlePrint();
        }, 1000);
      })
    );
  }

  private handlePrint(): void {
    const printData: PrintData = {
      title: '',
      orientation: 'portrait',
      content: this.printableContent.nativeElement.innerHTML,
      styles: printGridStyle
    };
    this.printService.print(printData);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
