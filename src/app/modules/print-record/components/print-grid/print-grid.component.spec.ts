import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintGridComponent } from './print-grid.component';

xdescribe('PrintGridComponent', () => {
  let component: PrintGridComponent;
  let fixture: ComponentFixture<PrintGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
