import { Component, Input, Output, EventEmitter } from '@angular/core';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { printGridGaMap } from '../../constants/print-grid.constant';
import { PrepareForArrivalsGaConstants } from '@modules/prepare/enums/prepare-for-arrivals-ga-constants.enum';

@Component({
  selector: 'app-print-grid',
  templateUrl: './print-grid.component.html',
  styleUrls: ['./print-grid.component.scss']
})
export class PrintGridComponent {

  @Input() menuItems: { label: string, value: string }[];
  @Input() disabled = false;
  @Output() print = new EventEmitter<string>();

  constructor(private gaService: GoogleAnalyticsService) { }

  public handleItemClick(printAction: string): void {
    this.print.emit(printAction);
    this.trackPrint(printAction);
  }

  public trackPrint(printAction: string): void {
    this.gaService.trackEvent(
      PrepareForArrivalsGaConstants.CATEGORY,
      PrepareForArrivalsGaConstants[printGridGaMap[printAction]],
      PrepareForArrivalsGaConstants.SELECTED
    );
  }
}
