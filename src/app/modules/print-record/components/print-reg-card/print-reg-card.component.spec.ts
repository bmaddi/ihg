import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintRegCardComponent } from './print-reg-card.component';

xdescribe('PrintRegCardComponent', () => {
  let component: PrintRegCardComponent;
  let fixture: ComponentFixture<PrintRegCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintRegCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintRegCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
