import { Component, Input, OnInit } from '@angular/core';
import { CheckInService } from '@modules/check-in/services/check-in.service';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { DetachedToastMessageService } from 'ihg-ng-common-core';
import { CHECK_IN_REPORT_ISSUE_AUTOFILL } from '@modules/check-in/constants/check-in-error-constants';
import { GoogleAnalyticsService } from 'ihg-ng-common-core';
import { ReservationDataModel } from '@modules/check-in/models/check-in.models';
import { printRegCardGA } from '@modules/print-record/constants/print-grid.constant';
import { PrintRegCardService } from '@modules/print-record/services/print-reg-card/print-reg-card.service';


@Component({
  selector: 'app-print-reg-card',
  templateUrl: './print-reg-card.component.html',
  styleUrls: ['./print-reg-card.component.scss']
})
export class PrintRegCardComponent extends AppErrorBaseComponent implements OnInit {

  public printErrorMsg = CHECK_IN_REPORT_ISSUE_AUTOFILL.printRegCardError;
  @Input() guestData: ReservationDataModel;
  @Input() stateName: string;
  @Input() isTodaySelected?: boolean;
  @Input() isCheckedIn = false;

  constructor(
    private checkInService: CheckInService,
    private printRegCardService: PrintRegCardService,
    private toastMessageService: DetachedToastMessageService,
    private gaService: GoogleAnalyticsService
  ) {
    super();
   }

  ngOnInit() {
  }

  PrintRegCard() {
    let gaCategory, gaEvent;
    if (this.stateName === 'check-in') {
      gaCategory = printRegCardGA.CATEGORY_CHECK_IN;
      gaEvent = printRegCardGA.PRINT_REG_CARD_CHECK_IN;
    } else if (this.stateName === 'guest-list-details') {
      gaCategory = printRegCardGA.CATEGORY_PREVIEW;
      gaEvent = printRegCardGA.PRINT_REG_CARD_PREVIEW;
    }
    this.gaService.trackEvent(gaCategory,
              gaEvent,
              printRegCardGA.SELECTED_EVENT);

    this.printRegCardService.getRegCardData(this.guestData.pmsReservationNumber)
      .subscribe((response: any) => {
        if (!this.checkIfAnyApiErrors(response)) {
          this.printRegCardService.printPdf(response);
        }
      }, (error) => {
        this.toastMessageService.danger(this.printErrorMsg.printErrorMsg, '');
        this.processHttpErrors(error);
      });
  }

}
