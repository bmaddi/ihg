import { Component, OnInit, Input } from '@angular/core';

import { UserService } from 'ihg-ng-common-core';

@Component({
  selector: 'app-search-criteria',
  templateUrl: './search-criteria.component.html',
  styleUrls: ['./search-criteria.component.scss']
})
export class SearchCriteriaComponent implements OnInit {

  public hotelName: { name: string, id: string };
  public defaultDate = new Date();

  @Input() selectedDate: Date;
  @Input() searchString: string;
  @Input() filters: string[];
  @Input() guestCount: number;
  @Input() arrivalTab: boolean;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.setHotelName();
  }

  private setHotelName(): void {
    this.hotelName = {
      name: this.userService.getCurrentLocation().name,
      id: this.userService.getCurrentLocationId()
    };
  }

}
