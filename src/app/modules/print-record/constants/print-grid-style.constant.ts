export const printGridStyle = `
.grid-title{display:none}
.search-criteria h2{font-size:14px;margin:0 0 5px}
.hotel-name{font-size:14px;font-weight:400;margin:0}
.search-string{margin:10px 0}
.small-text{font-size:10px}
.bold-font{font-weight:700}
.guest-list{margin-top:10px;border-collapse:collapse}
.guest-list thead th{text-align:center;padding:5px;width:80px;border-bottom:2px solid #ccc;border-top:2px solid #ccc}
.guest-list thead th:nth-child(1),.guest-list thead th.status-col,
.guest-list table td.status-col{width:150px}
.guest-list thead th:nth-child(2),.guest-list thead th:nth-child(3), 
.guest-list table td:nth-child(1), .guest-list table td:nth-child(2){width:100px}
.sub-heading{font-weight:400;display:flex;font-size:12px}
.sub-heading span{margin:0 5px}
.guest-list td{white-space:normal;word-break:break-word;padding:5px;font-size:12px;border-bottom:1px solid #ccc;height:50px}
.badge-icon-list{display:block;margin-top:5px}
.badge-icon{width:20px;height: 20px;margin-right:5px}
.bubble-container{margin:0 17px;display:flex;flex-direction:row;align-items:center;justify-content:stretch}
.line{display:flex;flex-basis:42px;flex-grow:1;flex-shrink:1;height:1px;background-color:#C9C9C9}
.bubble{width:20px;height:20px;flex-grow:1;flex-shrink:1;flex-direction:column;display:flex;align-items:center;justify-content:center;border-radius:50%;background-color:#C9C9C9}
.bubble .bubble-contents{display:flex;align-items:center;justify-content:center;line-height:0;color:#FFF;font-size:12px;font-weight:700;flex-basis:20px;flex-grow:1;flex-shrink:1}
.bubble.grey{background-color:#C9C9C9}
.bubble.green{background-color:#333}
.bubble.red{background-color:#333; width:18px;height:18px}
.bubble.yellow{background-color:#999}
.check-icon{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAA20lEQVQokZ2SoUpEYRBGvysiIpsWw2KSZbNZzEYRg9HX8ClMPoDBYDYZDPsAIiaLRUxGk4hFzzH8e+WX9cp6v2kDZ2a+mWmM6aOlXtTCoBnEbPwLFFeTHCe5ELeqYt0BDIFTVUD1Sh0aZ6AO1CN19I2V3AnwZtGreqguF1BH6qX6qU7VCbCungFtpyd1v54m6hi4accBrtVz9X2We1EP1JWfYPEyAW6rDq0e1d3f/KfytK3eVfAzsNd66gYLvAlMgXtg56+NN3MvZ8ZJ1pI8pMlH133nwQXV+1e/ALG8MC4xX3/WAAAAAElFTkSuQmCC);width:12px;height:15px}
.fa-check:before, app-reservation-status i:before{display:none}
.bubble-border.red{border:1px solid #333;border-radius:50%;padding:2px}
.preference ul{display:flex;padding:0;margin:0;flex-wrap:wrap}
.preference ul li{width:33.333%;list-style-position:inside;padding: 0 5px 0 0;}
.spcl-request ul li{list-style-type:none}
.guest-list .detail-row td{border-top:none}
.guest-list .detail-row .row{border-top:1px dotted #ccc}
.guest-list table{width: 100%}
.guest-list table td{width:80px;border:none;height:auto}
app-reservation-status i{background-repeat:no-repeat;width:12px;height:11px;display: inline-block;margin-right:5px}
.fas.fa-times{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAABHUlEQVQYlY3RoWtXARTF8c8ZYwwZY4iMsSQG08LwvWZV/wRBk8FiFjGYDGLwD7CIaLL5B6hJsL234D+gacgYMmTIGINj8MesHrjlcL6XezmpGodxFWttf8178ynOPRxN83QGGYZhte1t3EzyDp/ariS5i+t4i8/TPJ0tt91K8gg72Gm7nOQyHmPbX33F4XKSo7YfcCXJLl603Uiyif22X5IcQ6qGa8NWkie4j9XFtoO2z5K8nubpGJZgQX7DcVuLOcJ3nC5gS+MwXsA9PMQlHCY5SXIVT3FjHMYVWGq7iQfYbruP53jV9neS3bZ3sA7LSX7i/eKcl23ftF1LcopbST62/ffgOIzrbS/iYN6bfy9K2cAGfkzzdHIe/l/9AdeDhDmbjkbDAAAAAElFTkSuQmCC)}
.far.fa-clock{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAABLElEQVQokY2RsUoDURREzywpl+B7C5LCOmIXUuQTrEKs/AZJISH6ISFYBPEvJLW1iIRgJcHKKgi7CyG1Y7G7wcLCae7lcu+8eTMyBiCGmAg6QBc7RdoDG2Cbl8U3NVpNgz00TCT1kNrADljZngPLZi3JQkxiiCNJC0k74AI4s30J7CU9ZCEOsxATgAToCK6BZ+DKVT2X9AmM6/mESi4J0LXdB2Z5WXypmp0CaV4WW8MM6APd6sBOJbUNmyzEE8MN9qDRLHg3tG2n1acrN3aqGN4EL0gr4KO+6Qp29R4tYGN7DUwljfOyeGrYsxCPgVtgXVtMy7AF5pLugUV2FGZIm1rzFBgAY+wtgJrgshBH2BOknu12bfEKuMvL4vGv4JZIrzVzChyS5hcOL/wXP+M1cMCSNrPUAAAAAElFTkSuQmCC)}
.fas.fa-check-circle{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAABLElEQVQokY3RoWvVARTF8c8ZCyKPwfs9WJBhElmSYZBhtFkEBbOYZVHE5J8gsmDwLxDLstEkS2N5iEEMwnsPHmNBZMfw9mDD4kkX7vleLuekaqXJeBjhJkY4xffpfHbmkrICJuNhR7snudd2I8mi7WGSd9P57PgKMBkPu+VTuIG1SwfP8RNPp/PZV1ibjIfN8jpsrcxttT3XHrWFV5PxsOnCsK3d1f7Ct7bn+C05kDxO8kZ7v2zDuuW/g/YFviR5W2bhZdkMz5oMYWMJJGfaU8lDfG77PMmfshU+4G5Y4GwJcCI5xqMySrKHIbzHHa2L/ckK+IF97Q4eSD6Wa+FWW0kW2L/wXenhCfaw03YUFpIj7E/ns4NVzuuroRyEQ9zGdckpTrrs4d+m/1d/AVQWgfkNRJdLAAAAAElFTkSuQmCC)}
table{page-break-after:auto}
tr{page-break-inside:avoid;page-break-after:auto}
td{page-break-inside:avoid;page-break-after:auto}
thead{display:table-header-group}
tfoot{display:table-footer-group}
`;
