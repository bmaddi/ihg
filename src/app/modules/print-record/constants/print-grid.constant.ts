export enum printTypes {
  CURR_CMPCT = 'currentCompact',
  CURR_DTLD = 'currentDetailed',
  ALL_CMPCT = 'allCompact',
  ALL_DTLD = 'allDetailed'
}

export const printGridItems = [
  {
    label: 'LBL_CUR_VW_CMPCT',
    value: printTypes.CURR_CMPCT
  },
  {
    label: 'LBL_CUR_VW_DTLD',
    value: printTypes.CURR_DTLD
  },
  {
    label: 'LBL_ALL_VW_CMPCT',
    value: printTypes.ALL_CMPCT
  },
  {
    label: 'LBL_ALL_VW_DTLD',
    value: printTypes.ALL_DTLD
  }
];

export const printGridGaMap = {
  [printTypes.CURR_CMPCT]: 'PRNT_GRID_CURR_CMPCT',
  [printTypes.CURR_DTLD]: 'PRNT_GRID_CURR_DTLD',
  [printTypes.ALL_CMPCT]: 'PRNT_GRID_ALL_CMPCT',
  [printTypes.ALL_DTLD]: 'PRNT_GRID_ALL_DTLD'
};

export enum printRegCardGA {
  CATEGORY_CHECK_IN = 'Check In',
  PRINT_REG_CARD_CHECK_IN = 'Print Reg Card - Button',
  CATEGORY_PREVIEW = 'Check In',
  PRINT_REG_CARD_PREVIEW = 'Print Reg Card - Button',
  SELECTED_EVENT = 'Selected'
}
