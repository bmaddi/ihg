import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PrintGridComponent } from './components/print-grid/print-grid.component';
import { SearchCriteriaComponent } from './components/search-criteria/search-criteria.component';
import { PrintGridContainerComponent } from './components/print-grid-container/print-grid-container.component';
import { GuestListTableComponent } from './components/guest-list-table/guest-list-table.component';
import { BadgeListModule } from '@app/modules/badge-list/badge-list.module';
import { ProgressStatusModule } from '@app/modules/progress-status/progress-status.module';
import { PrintRegCardComponent } from './components/print-reg-card/print-reg-card.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NgbModule,
    BadgeListModule,
    ProgressStatusModule
  ],
  declarations: [
    PrintGridComponent,
    SearchCriteriaComponent,
    PrintGridContainerComponent,
    GuestListTableComponent,
    PrintRegCardComponent
  ],
  exports: [
    PrintGridComponent,
    PrintGridContainerComponent,
    PrintRegCardComponent
  ]
})
export class PrintRecordModule { }
