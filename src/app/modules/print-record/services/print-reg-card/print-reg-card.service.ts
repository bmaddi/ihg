import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { DeviceDetectorService } from 'ngx-device-detector'; 

@Injectable({
  providedIn: 'root'
})
export class PrintRegCardService {
  private readonly API_URL = environment.fdkAPI;
  constructor(
    private http: HttpClient,
    private deviceService: DeviceDetectorService
  ) { }

  public getRegCardData(pmsReservationNumber: string) {
    const apiURl = `${this.API_URL}checkIn/printReg?pmsReservationNumber=${pmsReservationNumber}`;

    return this.http.get(apiURl).pipe(map((response: any) => {
        return response;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public printPdf(fileData) {
    if (this.deviceService.isDesktop()) {
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        const byteCharacters = atob(fileData);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], {
          type: 'application/pdf'
        });
        window.navigator.msSaveOrOpenBlob(blob, 'output.pdf');
      } else {
        const pdfWindow = window.open('');
        pdfWindow.document.write('<iframe id="test" height="100%" width="100%" name="test" src="data:application/pdf;base64, '
          + encodeURI(fileData) + '"></iframe>');
      }
    } else {
      window.open('data:application/pdf;base64, ' + encodeURI(fileData));
    }
  }
}
