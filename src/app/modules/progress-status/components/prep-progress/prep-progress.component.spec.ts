import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepProgressComponent } from './prep-progress.component';

xdescribe('PrepareArrivalsDetailsTasksComponent', () => {
  let component: PrepProgressComponent;
  let fixture: ComponentFixture<PrepProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
