import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { cloneDeep } from 'lodash';
import { Subscription, Observable } from 'rxjs';

import { StatusBubbleModel } from '../../models/progress-status.model';
import { bubbleColorStatus, bubbleDefaultColor } from '../../constants/progress-status.constant';
import { ArrivalsListModel } from '@modules/prepare/models/arrivals-checklist.model';
import { ROOM_STATUS } from '@modules/prepare/components/prepare-arrivals-details/constants/room-status.constants';
import { ReservationStatus } from '@app/modules/guests/enums/guest-arrivals.enums';

@Component({
  selector: 'app-prep-progress',
  templateUrl: './prep-progress.component.html',
  styleUrls: ['./prep-progress.component.scss']
})
export class PrepProgressComponent implements OnInit, OnChanges, OnDestroy {
  
  private rootSubscription = new Subscription();
  public bubbleColor: StatusBubbleModel = cloneDeep(bubbleDefaultColor);
  public statusColor = bubbleColorStatus;
  public disabledCheckIcon = false;
  public isCheckedIn: boolean;

  @Input() listItem: ArrivalsListModel;
  @Input() isTodaySelected: boolean;
  @Input() refreshTask: Observable<null>;
  @Input() roomSet: Observable<null>;
  @Input() disabled = false;

  constructor() {
  }

  ngOnInit() {      
    this.isCheckedIn =  this.isGuestCheckedIn(); 
    if(!this.isCheckedIn) {      
      if (this.refreshTask) {
        this.subscribeRefreshTasksStatus();
      }
      if (this.roomSet) {
        this.subscribeToRoomSet();
      }
    }
    
  }

  ngOnChanges() {
    this.isCheckedIn =  this.isGuestCheckedIn(); 
    this.updateRoomAssignedStatus();
    this.updateRoomReadyStatus();
    this.updateAllTasksStatus();
  }

  private subscribeRefreshTasksStatus(): void {
    this.rootSubscription.add(this.refreshTask.subscribe(() => {
      this.updateAllTasksStatus();
    }));
  }

  private subscribeToRoomSet(): void {
    this.rootSubscription.add(this.roomSet.subscribe(() => {
      this.updateRoomAssignedStatus();
      this.updateRoomReadyStatus();
    }));
  }  

  private updateRoomAssignedStatus(): void {
    const status = this.isRoomAssigned() ? this.statusColor.complete
      : this.isLoyaltyMember() ? this.statusColor.incomplete : this.statusColor.inactive;
    this.setRoomAssignedStatus(status);
  }

  private updateAllTasksStatus(): void {
    if (!this.listItem.tasks || !this.listItem.tasks.length) {
      this.disabledCheckIcon = this.isRoomAssigned();
      this.setAllTasksStatus(this.statusColor.inactive);
    } else {
      this.disabledCheckIcon = false;
      this.handleTaskResolvedStatus();
    }
  }

  private handleTaskResolvedStatus(): void {
    if (this.hasUnresolvedTasks()) {
      this.setAllTasksStatus(this.statusColor.inProgress);
    } else {
      this.setAllTasksStatus(this.statusColor.complete);
    }
  }

  private hasUnresolvedTasks(): boolean {
    return this.listItem.tasks.some(item => !item.isResolved);
  }

  private updateRoomReadyStatus(): void {
    if (this.isTodaySelected && this.isRoomVacant() && this.isRoomCleanOrInspected()) {
      this.setRoomReadyStatus(this.statusColor.complete);
    } else {
      this.setRoomReadyStatus(this.statusColor.inactive);
    }
  }

  private isRoomVacant(): boolean {
    const status = this.listItem.assignedRoomStatus;
    return status ? status.frontOfficeStatus === ROOM_STATUS.Vacant.value : false;
  }

  private isRoomCleanOrInspected(): boolean {
    if (this.listItem.assignedRoomStatus && this.listItem.inspected) {
      return this.listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Inspected.value;
    }
    return this.listItem.assignedRoomStatus && this.listItem.assignedRoomStatus.roomStatus === ROOM_STATUS.Clean.value;
  }

  private setRoomAssignedStatus(color: string): void {
    this.bubbleColor.roomAssigned = color;
  }

  private setAllTasksStatus(color: string): void {
    this.bubbleColor.allTasks = color;
  }

  private setRoomReadyStatus(color: string): void {
    this.bubbleColor.roomReady = color;
  }

  private isRoomAssigned(): boolean {
    return !!this.listItem.assignedRoomNumber;
  }

  private isLoyaltyMember(): boolean {
    return !!this.listItem.ihgRcNumber;
  }

  ngOnDestroy(): void {
    this.rootSubscription.unsubscribe();
  }

  public isGuestCheckedIn(): boolean {
    if(this.listItem.prepStatus === '' || this.listItem.prepStatus == null){
      return false;
    }
    return this.listItem.prepStatus === ReservationStatus.checkedIn || this.listItem.prepStatus === ReservationStatus.inHouse;
  }
}
