import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationStatusComponent } from './reservation-status.component';
import { CommonTestModule } from '@modules/common-test/common-test.module';

describe('ReservationStatusComponent', () => {
  let component: ReservationStatusComponent;
  let fixture: ComponentFixture<ReservationStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationStatusComponent ],
      imports: [ CommonTestModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
