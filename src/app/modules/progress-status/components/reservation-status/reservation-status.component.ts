import { Component, Input, OnInit } from '@angular/core';

import { RESERVATION_STATUS_MAP } from '@modules/progress-status/constants/reservation-status.constants';

@Component({
  selector: 'app-reservation-status',
  templateUrl: './reservation-status.component.html',
  styleUrls: ['./reservation-status.component.scss']
})
export class ReservationStatusComponent implements OnInit {
  @Input() status: string;
  @Input() uniqueId: number;
  @Input() desktopView: boolean = true;

  reservationStatues = RESERVATION_STATUS_MAP;

  constructor() {
  }

  ngOnInit() {
  }
}
