import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusBubbleComponent } from './status-bubble.component';

describe('StatusBubbleComponent', () => {
  let component: StatusBubbleComponent;
  let fixture: ComponentFixture<StatusBubbleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusBubbleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
