import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-status-bubble',
  templateUrl: './status-bubble.component.html',
  styleUrls: ['./status-bubble.component.scss']
})
export class StatusBubbleComponent implements OnInit {

  @Input() color;

  constructor() {
  }

  ngOnInit() {
  }

}
