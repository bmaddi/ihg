export const bubbleDefaultColor = {
  roomAssigned: 'grey',
  allTasks: 'grey',
  roomReady: 'grey'
};

export const bubbleColorStatus = {
  inactive: 'grey',
  complete: 'green',
  incomplete: 'red',
  inProgress: 'yellow'
};