export const RESERVATION_STATUS_MAP = {
  'NOTSTARTED': {
    message: 'LBL_PREP_NOT_STRTD',
    icon: 'fas fa-times',
    slnmId: 'NotStartedIcon-SID',
    slmnIdUnique: (id: number) => `NotStartedIcon${id}-SID`
  },
  'INPROGRESS': {
    message: 'LBL_PREP_IN_PRGSS',
    icon: 'far fa-clock',
    slnmId: 'ProgressIcon-SID',
    slmnIdUnique: (id: number) => `ProgressIcon${id}-SID`
  },
  'READY': {
    message: 'LBL_RM_RDY',
    icon: 'fas fa-check-circle',
    slnmId: 'RoomReadyIcon-SID',
    slmnIdUnique: (id: number) => `RoomReadyIcon${id}-SID`
  },
  'CHECKEDIN': {
    message: 'LBL_CHECKED_IN',
    icon: 'fas fa-check-circle',
    slnmId: 'CheckedInIcon-SID',
    slmnIdUnique: (id: number) => `CheckedInIcon${id}-SID`
  },
  'INHOUSE': {
    message: 'LBL_CHECKED_IN',
    icon: 'fas fa-check-circle',
    slnmId: 'CheckedInIcon-SID',
    slmnIdUnique: (id: number) => `CheckedInIcon${id}-SID`
  },
  'CL': {
    message: 'LBL_IH_CLEAN',
    icon: 'fas fa-check-circle',
    slnmId: 'CleanIcon-SID',
    slmnIdUnique: (id: number) => `CleanIcon${id}-SID`
  },
  'IP': {
    message: 'LBL_IH_INSPECTED',
    icon: 'fas fa-check-circle',
    slnmId: 'InspectedIcon-SID',
    slmnIdUnique: (id: number) => `InspectedIcon${id}-SID`
  },
  'DI': {
    message: 'LBL_IH_DIRTY',
    icon: 'fas fas fa-times',
    slnmId: 'DirtyIcon-SID',
    slmnIdUnique: (id: number) => `DirtyIcon${id}-SID`
  },
  'PU': {
    message: 'LBL_IH_PICKUP',
    icon: 'fas fa-check-circle',
    slnmId: 'PickupIcon-SID',
    slmnIdUnique: (id: number) => `PickupIcon${id}-SID`
  },
};
