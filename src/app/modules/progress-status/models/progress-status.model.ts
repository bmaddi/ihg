export interface StatusBubbleModel {
  roomAssigned?: string;
  allTasks?: string;
  roomReady?: string;
}