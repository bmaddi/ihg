import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PrepProgressComponent } from './components/prep-progress/prep-progress.component';
import { StatusBubbleComponent } from './components/status-bubble/status-bubble.component';
import { ReservationStatusComponent } from './components/reservation-status/reservation-status.component';

@NgModule({
  declarations: [
    PrepProgressComponent,
    StatusBubbleComponent,
    ReservationStatusComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    NgbModule.forRoot()
  ],
  exports: [
    PrepProgressComponent,
    ReservationStatusComponent
  ]
})
export class ProgressStatusModule { }
