import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookUpRewardsMemberModalComponent } from './look-up-rewards-member-modal.component';

xdescribe('LookUpRewardsMemberModalComponent', () => {
  let component: LookUpRewardsMemberModalComponent;
  let fixture: ComponentFixture<LookUpRewardsMemberModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookUpRewardsMemberModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookUpRewardsMemberModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
