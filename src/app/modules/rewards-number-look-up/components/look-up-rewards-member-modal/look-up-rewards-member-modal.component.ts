import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-look-up-rewards-member-modal',
  templateUrl: './look-up-rewards-member-modal.component.html',
  styleUrls: ['./look-up-rewards-member-modal.component.scss']
})
export class LookUpRewardsMemberModalComponent implements OnInit {

  @Input() modalName: string;
  @Output() ihgRcNumber: EventEmitter<number> = new EventEmitter();

  constructor(
    private activeModal: NgbActiveModal    
  ) {}

  ngOnInit() {
  }  

  onIhgNumberSelected(ihgRcNumber){
    this.ihgRcNumber.emit(ihgRcNumber);
    this.activeModal.close();
  }

  closeModal() {
    this.activeModal.dismiss();
  }

}
