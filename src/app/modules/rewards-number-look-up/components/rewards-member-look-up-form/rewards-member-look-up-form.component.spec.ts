import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsMemberLookUpFormComponent } from './rewards-member-look-up-form.component';

xdescribe('RewardsMemberLookUpFormComponent', () => {
  let component: RewardsMemberLookUpFormComponent;
  let fixture: ComponentFixture<RewardsMemberLookUpFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsMemberLookUpFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsMemberLookUpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
