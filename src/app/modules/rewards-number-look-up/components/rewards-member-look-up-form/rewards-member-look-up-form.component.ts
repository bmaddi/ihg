import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';
import { pairwise, startWith } from 'rxjs/operators';

import { DEFAULT_COUNTRY, errorReasonCodes } from '../../constants/rewards-number-constants';
import { Country, PayloadModel, RewardsLookUpModel } from '../../models/rewards-number-look-up.models';
import { RewardsNumberLookUpService } from '../../services/rewards-number-look-up.service';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { State } from '@progress/kendo-data-query';
import { EnrollmentSummaryService } from 'ihg-ng-common-pages';
import { ValidateEmailModel } from 'ihg-ng-common-pages';

@Component({
  selector: 'app-rewards-member-look-up-form',
  templateUrl: './rewards-member-look-up-form.component.html',
  styleUrls: ['./rewards-member-look-up-form.component.scss']
})
export class RewardsMemberLookUpFormComponent implements OnInit {

  public rewardsMemberForm: FormGroup;
  public countries: Country[];
  public searchcount = 0;
  public firstLastCountryReq = false;
  public phnCountryReq = false;
  public firstLastPhnCountryReq = false;
  public emailValidationDescription: string;
  public emailValidCheck = false;

  public panelCollapsed = false;
  public showSearchResults = false;
  public gridData: GridDataResult;
  public gridState: State = {
    take: 10,
    skip: 0
  };

  @Output() onIhgNumberSelected: EventEmitter<string> = new EventEmitter();

  private rootSubscription: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private rewardsNumberLookUpService: RewardsNumberLookUpService,
    private enrollmentSummaryService: EnrollmentSummaryService
  ) {
    this.getCountries();
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm(): void {
    this.rewardsMemberForm = this.fb.group({
      rewardsNo: [ '', Validators.required ],
      emailAddress: [ '', [Validators.required,Validators.compose([Validators.pattern(
        /^(([^<>()%*=$[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)|(\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\]))|([^-./!#%+=*()@$, ])(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])] ],
      firstName: [''],
      lastName: [''],
      phone: [''],
      country: ['']
    });
    this.attachValueChangesToAllFormControls();
  }

  private getAllFormControls(): string[] {
    const formControlNameArr = Object.keys(this.rewardsMemberForm.value);
    return formControlNameArr;
  }

  private attachValueChangesToAllFormControls(): void {
    const formControlNameArr = this.getAllFormControls();
    for (let val of formControlNameArr) {
      let tempFormControl = this.getformControl(val);
      this.listenToValueChanges(tempFormControl,val);
    }
  }

  private listenToValueChanges(formControl: FormControl,formcontrolName: string): void {
    this.rootSubscription.add(formControl.valueChanges
      .pipe(startWith(''), pairwise())
      .subscribe(([prev, next]: [string, string]) => {
        if(next != '' && prev !== next) {
          this.checkToEnableOrDisableFields(formcontrolName,next,'disableField');
        } else if(next == '' && prev != '') {
          this.checkToEnableOrDisableFields(formcontrolName,next,'enableField');
        }
      })
    );
  }

  private checkIfNumber(val): boolean {
    return (val === '') ? true : /^\d$/.test(val);
  }

  private checkToEnableOrDisableFields(formcontrolName: string,valueToCheck: string,enableOrDisable: string) {
    switch(formcontrolName) {
      case 'rewardsNo':
        if (this.checkIfNumber(valueToCheck)){
          this.enableOrDisableFields(enableOrDisable,'emailAddress','firstName','lastName','phone','country');
        }
        break;
      case 'emailAddress':
        this.getformControl('emailAddress').setErrors({ 'notExists': true });
        this.enableOrDisableFields(enableOrDisable,'rewardsNo','firstName','lastName','phone','country')
        break;
      case 'firstName':
        this.toggleStateOnNameChange('lastName','phone','country',enableOrDisable);
        this.setCustomValidators();
        break;
      case 'lastName':
        this.toggleStateOnNameChange('firstName','phone','country',enableOrDisable);
        this.setCustomValidators();
        break;
      case 'phone':
        if (this.checkIfNumber(valueToCheck)){
          this.toggleStateOnNameChange('firstName','lastName','country',enableOrDisable);
          this.setCustomValidators();
        }
        break;
      case 'country':
        this.toggleStateOnNameChange('firstName','lastName','phone',enableOrDisable);
        this.setCustomValidators();
        break;
    }
  }

  private toggleStateOnNameChange(firstField: string,secondField: string,thirdField: string,typeOfJob: string): void {
    const firstFieldVal = this.getformControl(firstField).value;
    const secondFieldVal = this.getformControl(secondField).value;
    const thirdFieldVal = this.getformControl(thirdField).value;
    if(firstFieldVal === '' && secondFieldVal === '' && thirdFieldVal === '') {
      if(typeOfJob === 'enableField') {
        this.enableOrDisableFields('enableField','rewardsNo','emailAddress');
      } else {
        this.enableOrDisableFields('disableField','rewardsNo','emailAddress');
      }
    }
  }

  private setCustomValidators(): void {
    const firstNameVal = this.getformControl('firstName').value;
    const lastNameVal = this.getformControl('lastName').value;
    const phoneVal = this.getformControl('phone').value;
    const countryVal = this.getformControl('country').value;
    if((firstNameVal!='' || lastNameVal!='') && (phoneVal === '') && !this.firstLastCountryReq) {
      this.makeFirstNameLastNameCountryRequired();
    } else if((firstNameVal === '' && lastNameVal === '') && (phoneVal === '') && this.firstLastCountryReq) {
      this.clearFirstNameLastNameRequired();
      this.checkCountryFieldToToggleValidation(countryVal);
    } else if((firstNameVal ==='' && lastNameVal ==='') && (phoneVal !== '') && !this.phnCountryReq) {
      this.makePhoneCountryRequired();
    } else if((firstNameVal ==='' && lastNameVal ==='') && (phoneVal === '') && this.phnCountryReq) {
      this.clearPhoneCountryRequired();
      this.checkCountryFieldToToggleValidation(countryVal);
    } else if ((firstNameVal === '' && lastNameVal === '' && phoneVal === '') && (countryVal!='' && !this.firstLastPhnCountryReq)) {
      this.makeFirstLastPhoneCountryRequired();
    } else if((firstNameVal === '' && lastNameVal === '' && phoneVal === '') && (countryVal=='' && this.firstLastPhnCountryReq)) {
      this.clearFirstLastPhoneCountryRequired();
    }
  }

  private checkCountryFieldToToggleValidation(countryVal) {
    if(countryVal!='') {
      this.makeFirstLastPhoneCountryRequired();
    }
  }

  private makeFirstNameLastNameCountryRequired(): void {
    this.setReqValidators('firstName','lastName','country');
    this.firstLastCountryReq = true;
    if(this.phnCountryReq) {
      this.clearPhoneCountryRequired();
    }
    if(this.firstLastPhnCountryReq) {
      this.clearPhoneCountryRequired();
      this.firstLastPhnCountryReq = false;
    }
  }

  private clearFirstNameLastNameRequired(): void {
    this.clearReqValidators('firstName','lastName','country');
    this.firstLastCountryReq = false;
  }

  private makePhoneCountryRequired(): void {
    if(this.firstLastPhnCountryReq) {
      this.clearFirstNameLastNameRequired();
      this.firstLastPhnCountryReq = false;
    }
    if(this.firstLastCountryReq) {
      this.clearReqValidators('firstName','lastName');
      this.setReqValidators('phone');
      this.firstLastCountryReq = false;
    } else {
      this.setReqValidators('phone','country');
    }
    this.phnCountryReq = true;
  }

  private clearPhoneCountryRequired(): void {
    this.clearReqValidators('phone');
    this.phnCountryReq = false;
  }

  private makeFirstLastPhoneCountryRequired(): void {
    this.setReqValidators('firstName','lastName','phone','country');
    this.firstLastPhnCountryReq = true;
  }

  private clearFirstLastPhoneCountryRequired(): void {
    this.clearReqValidators('firstName','lastName','phone','country');
    this.firstLastPhnCountryReq = false;
  }

  private setReqValidators(...fields: string[]): void {
    for(let eachField of fields) {
      this.getformControl(eachField).setValidators(Validators.required);
      this.getformControl(eachField).updateValueAndValidity();
    }
  }

  private clearReqValidators(...fields: string[]): void {
    for(let eachField of fields) {
      this.getformControl(eachField).setValidators(null);
      this.getformControl(eachField).updateValueAndValidity();
    }
  }

  private clearEmailValidation(emailControl: FormControl): void {
    this.emailValidationDescription = '';
    emailControl.setErrors(null);
  }

  private enableOrDisableFields(typeOfJob: string, ...formControlNames: string[]) {
    for(let eachControl of formControlNames) {
      if(typeOfJob === 'disableField') {
        this.getformControl(eachControl).disable({ emitEvent: false });
      }
      else{
        this.getformControl(eachControl).enable({ emitEvent: false });
      }
    }
  }

  public clearSearch(): void {
    this.firstLastCountryReq = false;
    this.phnCountryReq = false;
    this.firstLastPhnCountryReq = false;
    this.clearReqValidators('firstName','lastName','phone','country');
    this.enableOrDisableFields('enableField','rewardsNo','emailAddress','firstName','lastName','phone','country');
    this.createForm();
  }

  public getformControl(controlName: string): FormControl {
    return this.rewardsMemberForm.get(controlName) as FormControl;
  }

  private getCountries(): void {
    this.rootSubscription.add(this.rewardsNumberLookUpService.getCountries()
      .subscribe((response: Country[]) => {
        this.countries = [DEFAULT_COUNTRY, ...response];
      })
    );
  }

  public onSubmit(): void {
    if (this.rewardsMemberForm.valid) {
      this.showSearchResults = false;
      this.searchcount++;
      this.panelCollapsed = true;
      this.gridState.skip = 0;
      this.getRewardsNumberInformation();
    }
    return;
  }

  private get payload(): PayloadModel {
    return {
      countryCode: this.getformControl('country').value || '',
      emailId: this.getformControl('emailAddress').value || '',
      firstName: this.getformControl('firstName').value || '',
      ihgRcNumber: this.getformControl('rewardsNo').value || '',
      lastName: this.getformControl('lastName').value || '',
      phone: this.getformControl('phone').value || ''
    }
  }

  private getRewardsNumberInformation(): void {
    this.rootSubscription.add(this.rewardsNumberLookUpService.getRewardsNumberInformation(this.payload, this.gridState)
      .subscribe((resp: RewardsLookUpModel) => {
        this.setGridView(resp);
      })
    );
  }

  private setGridView(response: RewardsLookUpModel): void {
    this.gridData = {
      data: response.items,
      total: response.totalItems
    };
    this.showSearchResults = true;
  }

  public handlePageChange(event: PageChangeEvent): void {
    this.gridState.skip = event.skip;
    this.gridState.take = event.take;
    this.getRewardsNumberInformation();
  }

  public handleToggle(isCollapsed: boolean): void {
    if (typeof isCollapsed === 'boolean') {
      this.panelCollapsed = isCollapsed;
    }
  }

  public onIhgRcNumberSelected(ihgRcNumber: string){
    this.onIhgNumberSelected.emit(ihgRcNumber);
  }

  public validateEmail()  {
    const emailControl = this.getformControl('emailAddress');
    if(emailControl.value != '') {
      this.rootSubscription.add(this.enrollmentSummaryService.validateEmail(emailControl.value)
        .subscribe((response: ValidateEmailModel) => {
          if (response) {
            if (errorReasonCodes.indexOf(response.reasonCode) !== -1) {
              emailControl.setErrors({ 'notExists': true });
              this.emailValidationDescription = response.reasonDescription;
            }
          } else {
            this.clearEmailValidation(emailControl);
          }
        })
      );
    } else {
      this.clearEmailValidation(emailControl);
    }
  }

  public handleAddCrptId(crprtId: number): void {
    //this.emitAddCrprtIdToForm.emit(crprtId);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

}
