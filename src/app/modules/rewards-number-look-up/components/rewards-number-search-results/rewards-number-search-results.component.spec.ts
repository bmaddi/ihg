import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsNumberSearchResultsComponent } from './rewards-number-search-results.component';

xdescribe('RewardsNumberSearchResultsComponent', () => {
  let component: RewardsNumberSearchResultsComponent;
  let fixture: ComponentFixture<RewardsNumberSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsNumberSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsNumberSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
