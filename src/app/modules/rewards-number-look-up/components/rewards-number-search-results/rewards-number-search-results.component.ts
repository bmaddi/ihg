import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { State } from '@progress/kendo-data-query';
import { RewardsLookUpModel } from '../../models/rewards-number-look-up.models';

@Component({
  selector: 'app-rewards-number-search-results',
  templateUrl: './rewards-number-search-results.component.html',
  styleUrls: ['./rewards-number-search-results.component.scss']
})
export class RewardsNumberSearchResultsComponent implements OnInit {

  @Input() rewardSearchResults: RewardsLookUpModel; 
  @Input() gridState: State;
  @Output() handlePageChangeEmitter: EventEmitter<PageChangeEvent> = new EventEmitter();
  @Output() onIhgNumberSelected: EventEmitter<number> = new EventEmitter();

  constructor() { }

  public addCorporateIdToForm(ihgRcNumber): void {
    this.onIhgNumberSelected.emit(ihgRcNumber);
  }

  ngOnInit() {
  }

  public handlePageChange(evt: PageChangeEvent): void {
    this.handlePageChangeEmitter.emit(evt);
  }

}
