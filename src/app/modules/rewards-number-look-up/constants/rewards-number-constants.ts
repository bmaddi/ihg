import { Country } from '../models/rewards-number-look-up.models';

export const DEFAULT_COUNTRY : Country = {
  "countryName": "Select",
  "countryCode": "",
  "internationalCallingCode": "0"
}; 

export const errorReasonCodes: number[] = [221, 222, 223, 224, 225, 226, 301, 302, 303, 304, 305, 306, 307, 311];
