import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export interface Country{
  countryName: string;
  countryCode: string,
  internationalCallingCode: string;    
}

export interface PayloadModel {
  countryCode: string;
  emailId: string;
  firstName: string;
  ihgRcNumber: string;
  lastName: string;
  phone: string;
}

export interface RewardsNumberInfoModel {
  ihgRcNumber: string;
  ihgRcMembership: string;
  ihgRcPoints: null | string,
  badges: string[],
  firstName: string;
  lastName: string;
  middleName: string;
  emailId: string;
  phone: string;
  countryCode: string;
}

export interface RewardsLookUpModel {
  pageNumber: number;
  pageSize: number;
  startItem: number;
  endItem: number;
  totalItems: number;
  totalPages: number;
  items: RewardsNumberInfoModel[];
  errors?: ErrorsModel[];
}
