import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppSharedModule } from '@modules/shared/app-shared.module';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';

import { LookUpRewardsMemberModalComponent } from './components/look-up-rewards-member-modal/look-up-rewards-member-modal.component';
import { RewardsMemberLookUpFormComponent } from './components/rewards-member-look-up-form/rewards-member-look-up-form.component';
import { RewardsNumberSearchResultsComponent } from './components/rewards-number-search-results/rewards-number-search-results.component';

@NgModule({
  declarations: [
    LookUpRewardsMemberModalComponent,
    RewardsMemberLookUpFormComponent,
    RewardsNumberSearchResultsComponent 
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule, 
    ReactiveFormsModule,
    DropDownsModule,
    GridModule,
    NgbModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    AppSharedModule, 
  ],
  exports: [
    LookUpRewardsMemberModalComponent,
    RewardsMemberLookUpFormComponent
  ],
  entryComponents: [
    LookUpRewardsMemberModalComponent    
  ]
})
export class RewardsNumberLookUpModule { }
