import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { Country, RewardsLookUpModel } from '../models/rewards-number-look-up.models';
import { State } from '@progress/kendo-data-query';

@Injectable({
  providedIn: 'root'
})
export class RewardsNumberLookUpService {

  private readonly API_URL = environment.fdkAPI;

  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public getCountries(): Observable<Country[]> {
    const apiURl = `${environment.apiUrl}enrollmentSummary/countries`;
    return this.http.get(apiURl).pipe(map((response: Country[]) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getPaginationParams(state: State): string {
    const page = (state.skip / state.take) + 1;
    return `page=${page}&pageSize=${state.take}`;
  }

  public getRewardsNumberInformation(payload, state): Observable<RewardsLookUpModel> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}loyalty/members/search?${this.getPaginationParams(state)}`;
    return this.http.post(apiURl,payload).pipe(map((response: RewardsLookUpModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

}
