import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';

@Component({
  selector: 'app-proposed-strategies',
  templateUrl: './proposed-strategies.component.html',
  styleUrls: ['./proposed-strategies.component.scss']
})
export class ProposedStrategiesComponent implements OnInit {

  strategyData = this.salesService.getProposedSellStrategies();
  strategyEditData = this.salesService.getStrategyEdit();

  constructor(private salesService: SalesDemoService,
    private router: Router) {
  }

  ngOnInit() {
  }

  navigateStrategyPerformance() {
    this.router.navigate([`sales-strategy-performance/`]);
  }

}
