import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StrategyEditModalComponent } from '../strategy-edit-modal/strategy-edit-modal.component';


@Component({
  selector: 'app-proposed-strategy-item',
  templateUrl: './proposed-strategy-item.component.html',
  styleUrls: ['./proposed-strategy-item.component.scss']
})
export class ProposedStrategyItemComponent implements OnInit {

  @Input() strategy;
  @Input() strategyEdit;

  constructor(
    private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  formatDate(date: Date) {
    return moment(date).format(consts.dateFormat);
  }

  getKeys(object: object) {
    return Object.keys(object);
  }

  onModify() {
    if (this.strategyEdit) {
      const modal = this.modalService.open(StrategyEditModalComponent,
        {
          backdrop: 'static',
          size: 'lg',
          windowClass: 'modal-xlg'
        });
        const modalComponent = modal.componentInstance as StrategyEditModalComponent;
        modalComponent.strategyEdit = this.strategyEdit;
        modalComponent.strategy = this.strategy;
        modal.result.then( /*succeded*/(result) => {
          const {segmentClusters, specials} = result.strategyEdit;
          const {startDate, endDate, revpar, adr, occ} = result.formData;

          this.strategy.startDate = moment(startDate).format(consts.dateFormat);
          this.strategy.endDate = moment(endDate).format(consts.dateFormat);

          this.strategy.uplifts.REVPAR = revpar;
          this.strategy.uplifts.ADR = adr;
          this.strategy.uplifts.OCC = occ;

          this.strategy.rates['Segment-2'] = segmentClusters[1].newRate;
          this.strategy.rates['Drink Specials'] = specials[0].newRate;
          this.strategy.rates['Dinner Specials'] = specials[1].newRate;

        }, /* dismissed */() => {});
    }
  }

}
