import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { TranslateService } from '@ngx-translate/core';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { NewGuestObservation, GuestProfileModel, ValueLabelModel } from '@app/modules/guests/models/guest-profile.model';
import * as moment from 'moment';
import { AppConstants } from '@app/constants/app-constants';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewPerformanceModalComponent } from '../view-performance-modal/view-performance-modal.component';


@Component({
  selector: 'app-strategy-edit-modal',
  templateUrl: './strategy-edit-modal.component.html',
  styleUrls: ['./strategy-edit-modal.component.scss']
})
export class StrategyEditModalComponent implements OnInit {

  strategyEdit;
  strategy;

  formData: {
    revpar: number;
    occ: number;
    adr: number;
    sseHotelGrouping: string;
    channel: string;
    sseAggresivenes: string;
    startDate: Date;
    endDate: Date;
  };
  disabled: {
    revpar: boolean;
    occ: boolean;
    adr: boolean;
  };

  revparOptions = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5];
  occOptions = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5];
  adrOptions = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5];
  sseHotelGroupingOptions = ['AMER', 'GROUP1'];
  channelOptions = ['OTS', 'Direct', 'B2B'];
  sseAggresivenesOptions = ['Low', 'Med', 'High'];

  constructor(
    private activeModal: NgbActiveModal,
    private modalService: NgbModal) { }

  ngOnInit() {
    const {REVPAR, ADR, OCC} = this.strategy.uplifts;

    this.formData = {
      revpar: REVPAR,
      occ: ADR,
      adr: OCC,
      sseHotelGrouping: 'AMER',
      channel: 'OTS',
      sseAggresivenes: 'Low',
      startDate: moment(this.strategy.startDate, consts.dateFormat).toDate(),
      endDate: moment(this.strategy.endDate, consts.dateFormat).toDate(),
    };
    this.updateDropdownStates();
  }

  private updateDropdownStates() {
    if (this.formData.revpar !== 0 ) {
      this.disabled = {
        revpar: false,
        occ: true,
        adr: true
      };
    } else if (this.formData.occ !== 0 ) {
      this.disabled = {
        revpar: true,
        occ: false,
        adr: true
      };
    } else if (this.formData.adr !== 0 ) {
      this.disabled = {
        revpar: true,
        occ: true,
        adr: false
      };
    } else {
      this.disabled = {
        revpar: false,
        occ: false,
        adr: false
      };
    }
    const modifier = (this.formData.revpar + this.formData.occ + this.formData.adr) * 0.01;
    this.updateRateProposals(modifier);
  }

  private updateRateProposals(modifier: number) {
    const {segmentClusters, specials} = this.strategyEdit;

    segmentClusters[1].newRate = segmentClusters[1].priorRate * (1 + modifier * 1.29);
    specials[0].newRate = specials[0].priorRate * (1 + modifier * 1.3);
    specials[1].newRate = specials[1].priorRate * (1 - modifier * 22);

    segmentClusters[1].modified = modifier !== 0;
    specials[0].modified = modifier !== 0;
    specials[1].modified = modifier !== 0;
  }

  onAnalyze() {
    this.updateDropdownStates();
  }

  onSave() {
    this.activeModal.close({
      formData: this.formData,
      strategyEdit: this.strategyEdit
    });
  }

  onClose() {
    this.dismissModal();
  }

  onCancel() {
    this.dismissModal();
  }

  onViewPerformance() {
    const modal = this.modalService.open(ViewPerformanceModalComponent,
      {
        backdrop: 'static',
        size: 'lg',
        windowClass: 'modal-xlg'
      });
    modal.result.then( /*succeded*/(result) => {
    }, /* dismissed */() => { });
  }

  private dismissModal() {
    this.activeModal.dismiss();
  }
}
