import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';

@Component({
  selector: 'app-strategy-performance-item',
  templateUrl: './strategy-performance-item.component.html',
  styleUrls: ['./strategy-performance-item.component.scss']
})
export class StrategyPerformanceItemComponent implements OnInit {

  @Input() performanceItem;

  constructor() {

  }

  ngOnInit() {
  }

  formatDate(date: Date) {
    return moment(date).format(consts.dateFormat);
  }

  getKeys(object: object) {
    return Object.keys(object);
  }

}
