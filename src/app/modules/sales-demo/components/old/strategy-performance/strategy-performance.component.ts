import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';

@Component({
  selector: 'app-strategy-performance',
  templateUrl: './strategy-performance.component.html',
  styleUrls: ['./strategy-performance.component.scss']
})
export class StrategyPerformanceComponent implements OnInit {

  performanceData = this.salesService.getStrategyPerformanceData();

  constructor(private salesService: SalesDemoService,
    private router: Router) {

  }

  ngOnInit() {
  }

  navigateProposedStrategies() {
    this.router.navigate([`sales-proposed-strategies/`]);
  }

}
