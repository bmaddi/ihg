import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { TranslateService } from '@ngx-translate/core';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { NewGuestObservation, GuestProfileModel, ValueLabelModel } from '@app/modules/guests/models/guest-profile.model';
import * as moment from 'moment';
import { AppConstants } from '@app/constants/app-constants';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-strategy-create-modal',
  templateUrl: './strategy-create-modal.component.html',
  styleUrls: ['./strategy-create-modal.component.scss']
})
export class StrategyCreateModalComponent implements OnInit {

  formData: {
    strategyName: string;
    booking: {
      start: Date;
      end: Date;
    },
    stay: {
      start: Date;
      end: Date;
    },
    propertyCluster: string;
    channel: string;
    aggressiveness: string;
    aPercentage: number;
    bPercentage: number;
    revpar: {
      min: number;
      max: number;
    },
    adr: {
      min: number;
      max: number;
    },
    occ: {
      min: number;
      max: number;
    },
    book: {
      min: number;
      max: number;
    }
  };

  propertyClusterOptions = [
    'Waterfront Cluster',
    'SPARx Cluster 1',
    'Crowne Cluster',
    'SPARx Cluster 2',
    'Marathon Cluster'
  ];

  channelOptions = [
    'B2B',
    'Web',
    'Web/B2B'
  ];

  aggressivenessOptions = [
    'Low',
    'Medium',
    'High'
  ];


  constructor(
    private activeModal: NgbActiveModal,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.formData = {
      strategyName: '',
      booking: {
        start: null,
        end: null
      },
      stay: {
        start: null,
        end: null
      },
      propertyCluster: null,
      channel: null,
      aggressiveness: null,
      aPercentage: null,
      bPercentage: null,
      revpar: {
        min: null,
        max: null
      },
      occ: {
        min: null,
        max: null
      },
      adr: {
        min: null,
        max: null
      },
      book: {
        min: null,
        max: null
      }
    };
  }

  onSave() {
    console.log('Closing modal');
    this.activeModal.close({
       formData: this.formData
    });
  }

  onCancel() {
    this.dismissModal();
  }

  private dismissModal() {
    this.activeModal.dismiss();
  }
}
