import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StrategyCreateModalComponent } from '@app/modules/sales-demo/components/strategy-management/strategy-create-modal/strategy-create-modal.component';
import { AppConstants } from '@app/constants/app-constants';
import { Router } from '@angular/router';


@Component({
  selector: 'app-strategy-management-item',
  templateUrl: './strategy-management-item.component.html',
  styleUrls: ['./strategy-management-item.component.scss']
})
export class StrategyManagementItemComponent implements OnInit {

  @Input() strategy;

  constructor(
    private router: Router,
    private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  formatDate(date: string) {
    return moment(date, AppConstants.DB_DT_FORMAT).format(consts.dateFormat);
  }

  getKeys(object: object) {
    return Object.keys(object);
  }

  onView() {
    if (this.strategy.opensViewScreen) {
      this.router.navigate([`sales-strategy-view/`]);
    }
    /*
    if (this.strategyView) {
      const modal = this.modalService.open(StrategyEditModalComponent,
        {
          backdrop: 'static',
          size: 'lg',
          windowClass: 'modal-xlg'
        });
        const modalComponent = modal.componentInstance as StrategyEditModalComponent;
        modalComponent.strategyEdit = this.strategyView;
        modalComponent.strategy = this.strategy;
        modal.result.then( (result) => { // succeded
          const {segmentClusters, specials} = result.strategyEdit;
          const {startDate, endDate, revpar, adr, occ} = result.formData;

          this.strategy.startDate = moment(startDate).format(consts.dateFormat);
          this.strategy.endDate = moment(endDate).format(consts.dateFormat);

          this.strategy.uplifts.REVPAR = revpar;
          this.strategy.uplifts.ADR = adr;
          this.strategy.uplifts.OCC = occ;

          this.strategy.rates['Segment-2'] = segmentClusters[1].newRate;
          this.strategy.rates['Drink Specials'] = specials[0].newRate;
          this.strategy.rates['Dinner Specials'] = specials[1].newRate;

        }, () => {} // dismissed
      );
    }
    */
  }

}
