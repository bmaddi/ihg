import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';
import { StrategyCreateModalComponent } from '@app/modules/sales-demo/components/strategy-management/strategy-create-modal/strategy-create-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-strategy-management-list',
  templateUrl: './strategy-management-list.component.html',
  styleUrls: ['./strategy-management-list.component.scss']
})
export class StrategyManagementListComponent implements OnInit {

  strategyData = this.salesService.getStrategyManagementData();
  // strategyEditData = this.salesService.getStrategyEdit();

  constructor(private salesService: SalesDemoService,
    private router: Router,
    private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  navigateStrategyPerformance() {
    this.router.navigate([`sales-past-strategy-performance/`]);
  }

  onCreateStreategy() {
    const modal = this.modalService.open(StrategyCreateModalComponent,
      {
        backdrop: 'static',
        size: 'lg',
        windowClass: 'modal-xlg'
      });
    const modalComponent = modal.componentInstance as StrategyCreateModalComponent;
    modal.result.then((result) => {
      this.salesService.createStrategy(result);
    }, () => { } // dismissed
    );
  }

}
