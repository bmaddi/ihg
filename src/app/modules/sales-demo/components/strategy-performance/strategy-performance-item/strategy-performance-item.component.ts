import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { AppConstants } from '@app/constants/app-constants';

@Component({
  selector: 'app-strategy-performance-item2',
  templateUrl: './strategy-performance-item.component.html',
  styleUrls: ['./strategy-performance-item.component.scss']
})
export class StrategyPerformanceItem2Component implements OnInit {

  @Input() performanceItem;

  constructor() {

  }

  ngOnInit() {
  }

  formatDate(date: string) {
    return moment(date, AppConstants.DB_DT_FORMAT).format(consts.dateFormat);
  }

  getKeys(object: object) {
    return Object.keys(object);
  }

}
