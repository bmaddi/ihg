import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';

@Component({
  selector: 'app-strategy-performance2',
  templateUrl: './strategy-performance.component.html',
  styleUrls: ['./strategy-performance.component.scss']
})
export class StrategyPerformance2Component implements OnInit {

  performanceData = this.salesService.getPastStrategyData();

  constructor(private salesService: SalesDemoService,
    private router: Router) {

  }

  ngOnInit() {
  }

  navigateSellStrategyAnalysis() {
    this.router.navigate([`sales-strategy-management/`]);
  }

}
