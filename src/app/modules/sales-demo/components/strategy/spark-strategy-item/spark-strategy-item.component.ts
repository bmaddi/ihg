import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppConstants } from '@app/constants/app-constants';
import { ViewPerformanceModal2Component } from '@app/modules/sales-demo/components/strategy/view-performance-modal/view-performance-modal.component';


@Component({
  selector: 'app-spark-strategy-item',
  templateUrl: './spark-strategy-item.component.html',
  styleUrls: ['./spark-strategy-item.component.scss']
})
export class SparkStrategyItemComponent implements OnInit {

  @Input() strategy;

  constructor(
    private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  formatDate(date: string) {
    return moment(date, AppConstants.DB_DT_FORMAT).format(consts.dateFormat);
  }

  getKeys(object: object) {
    return Object.keys(object);
  }



  onPerformanceMonitor() {
    const modal = this.modalService.open(ViewPerformanceModal2Component,
      {
        backdrop: 'static',
        size: 'lg',
        windowClass: 'modal-xlg'
      });
      modal.result.then( (result) => { // succeded
      }, () => {} // dismissed
    );
  }

}
