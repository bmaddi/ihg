import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';
import { StrategyCreateModalComponent } from '@app/modules/sales-demo/components/strategy-management/strategy-create-modal/strategy-create-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-strategy-view',
  templateUrl: './strategy-view.component.html',
  styleUrls: ['./strategy-view.component.scss']
})
export class StrategyViewComponent implements OnInit {

  sparkStrategyData = this.salesService.getSparkStrategyData();

  constructor(private salesService: SalesDemoService,
    private router: Router,
    private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  navigateSellStrategyAnalysis() {
    this.router.navigate([`sales-strategy-management/`]);
  }

}
