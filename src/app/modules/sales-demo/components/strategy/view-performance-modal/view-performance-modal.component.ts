import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { TranslateService } from '@ngx-translate/core';
import { GuestListService } from '@app/modules/guests/services/guest-list.service';
import { NewGuestObservation, GuestProfileModel, ValueLabelModel } from '@app/modules/guests/models/guest-profile.model';
import * as moment from 'moment';
import { AppConstants } from '@app/constants/app-constants';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';


@Component({
  selector: 'app-view-performance-modal2',
  templateUrl: './view-performance-modal.component.html',
  styleUrls: ['./view-performance-modal.component.scss']
})
export class ViewPerformanceModal2Component {

  constructor(
    private activeModal: NgbActiveModal) { }

  onClose() {
    this.dismissModal();
  }

  onCancel() {
    this.dismissModal();
  }

  private dismissModal() {
    this.activeModal.dismiss();
  }
}
