import { proposedSellStrategies } from './proposed-sell-strategies.constants';
import { dateFormat } from './common.constants';
import { strategyPerformanceData } from './strategy-performance.constants';
import { strategyEdit } from './strategy-edit.constants';
import { strategyManagementData } from '@app/modules/sales-demo/constants/strategy-management.constants';
import { sparkStrategyData } from '@app/modules/sales-demo/constants/spark-strategy.constants';
import { pastStrategyConstants } from '@app/modules/sales-demo/constants/past-strategy-constants.constants';

export const consts = {
  proposedSellStrategies: proposedSellStrategies,
  strategyPerformanceData: strategyPerformanceData,
  strategyEdit: strategyEdit,
  dateFormat,
  strategyManagementData,
  sparkStrategyData,
  pastStrategyConstants
};
