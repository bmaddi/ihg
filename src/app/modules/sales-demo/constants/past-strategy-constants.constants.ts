const performance1 = {
  name: 'Get The Flower Show Business',
  status: 'failure',
  startDate: '2018-06-01',
  endDate: '2018-08-15',
  projectedUplift: {
    REVPAR: { from: 0.5, to: 1.0 },
    ADR: { from: 2.0, to: 2.5},
    OCC: { from: 3.0, to: 3.5}
  },
  actualUplift: {
    REVPAR: 0.3,
    ADR: 2.0,
    OCC: 2.8
  },
  explanation: {
    text: 'The results of this strategy did not meet the goals for the following reasons:',
    reasons: [
      'Occupancy did not hit target',
      'Competitor OCC to share from IHG properties'
    ]
  }
};

const performance2 = {
  name: 'Get The Summer Beer Festival Business',
  status: 'success',
  startDate: '2018-06-01',
  endDate: '2018-07-31',
  projectedUplift: {
    REVPAR: { from: 0.5, to: 0.75},
    ADR: { from: 2.0, to: 2.5},
    OCC: { from: 3.0, to: 3.25}
  },
  actualUplift: {
    REVPAR: 0.6,
    ADR: 2.5,
    OCC: 3.0
  },
  explanation: {
    text: 'The results of this strategy meet the goals for the following reasons:',
    reasons: [
      'Occupancy did exceed strategy target',
      'Segmentation maximized ADR and met OCC goals'
    ]
  }
};

const performance3 = {
  name: 'Get The Greatful Dead Business',
  status: 'balanced',
  startDate: '2018-06-01',
  endDate: '2018-06-30',
  projectedUplift: {
    REVPAR: {from: 0.8, to: 1.5},
    ADR: {from: 2.5, to: 3},
    OCC: {from: 3.5, to: 3.75}
  },
  actualUplift: {
    REVPAR: 0.8,
    ADR: 2.5,
    OCC: 3.2
  },
  explanation: {
    text: 'The results of this strategy meet the goals for the following reasons:',
    reasons: [
      'The reduced strategy period allowed a more aggressive rate mix when applied to the brand'
    ]
  }
};

export const pastStrategyConstants = [
  performance1,
  performance2,
  performance3
];

