import * as moment from 'moment';
import { AppConstants } from '@app/constants/app-constants';
import { dateFormat } from './common.constants';

const strategy1 = {
  name: '200 (HI)',
  version: '1.1',
  startDate: moment('2019-06-01', AppConstants.DB_DT_FORMAT).format(dateFormat),
  endDate: moment('2019-07-31', AppConstants.DB_DT_FORMAT).format(dateFormat),
  rates: {
    'Segment-3': 192.5,
    'Segment-4': 193.75,
    'Segment-5': 195
  },
  uplifts: {
    REVPAR: 0.75,
    ADR: 2.2,
    OCC: 3.2
  },
  priorYear: {
    REVPAR: 156,
    ADR: 191,
    OCC: 78
  },
  compset: {
    REVPAR: 158,
    ADR: 195,
    OCC: 80
  },
  demandAndForecast: {
    hurdlePoints: {
      'C1': 192,
      'C2': 194
    },
    bfr: 193.5
  }
};
const strategy2 = {
  name: '201 (HI)',
  version: '1.0',
  startDate: moment('2019-06-01', AppConstants.DB_DT_FORMAT).format(dateFormat),
  endDate: moment('2019-06-30', AppConstants.DB_DT_FORMAT).format(dateFormat),
  rates: {
    'Segment-2': 196.5,
    'Drink Specials': 0.75,
    'Dinner Specials': 0.35
  },
  uplifts: {
    REVPAR: 1,
    ADR: 0,
    OCC: 0
  },
  priorYear: {
    REVPAR: 156,
    ADR: 191,
    OCC: 78
  },
  compset: {
    REVPAR: 158,
    ADR: 195,
    OCC: 80
  },
  demandAndForecast: {
    hurdlePoints: {
      'C1': 192.35,
      'C2': 195
    },
    bfr: 194
  }
};

const strategy3 = {
  name: '203 (HI)',
  version: '1.2',
  startDate: moment('2019-06-01', AppConstants.DB_DT_FORMAT).format(dateFormat),
  endDate: moment('2019-07-31', AppConstants.DB_DT_FORMAT).format(dateFormat),
  rates: {
    'Segment-1': 192.25,
    'Segment-3': 193.75,
    'NRI/Room': 5.75
  },
  uplifts: {
    REVPAR: 1,
    ADR: 2.9,
    OCC: 4.0
  },
  priorYear: {
    REVPAR: 156,
    ADR: 191,
    OCC: 78
  },
  compset: {
    REVPAR: 158,
    ADR: 195,
    OCC: 80
  },
  demandAndForecast: {
    hurdlePoints: {
      'C1': 192,
      'C2': 194
    },
    bfr: 193.50
  }
};

export const proposedSellStrategies = [
  strategy1,
  strategy2,
  strategy3
];


