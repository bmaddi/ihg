const strategy1 = {
  strategyName: 'Summer Loyalty Point Extravaganza',
  sparkName: 'SPARx Strategy 1',
  effectiveBookingDates: {
    start: '2019-06-01',
    end: '2019-07-31'
  },
  effectiveStayDates: {
    start: '2019-06-15',
    end: '2019-08-30'
  },
  propertyCluster: 'All Crowne Plaza Hotels (820)',
  includedRateCategories: [
    {
      name: '1 IKME3: IHG RC 1000 PTS NT',
      otherData: '- All Channels/Mon, Tue, Wed, Thu/2+ Nights'
    },
    {
      name: '2 ISHD1: 10K BONUS POINT PKG',
      otherData: '- B2B/Mon, Tue, Wed, Thu/4+ Nights'
    },
    {
      name: '3 IGBBB: BEST FLEX WITH BKFST',
      otherData: '- Web/Weekend Only/Fri, Sat/Sun/2+ Nights'
    },
  ],
  successYoyTargets: {
    REVPAR: {from: 0.5, to: 0.65, strategyTrend: 4},
    ADR: {from: 1.8, to: 2.2, strategyTrend: 3},
    OCC: {from: 2.7, to: 3.2, strategyTrend: 5}
  },
  priorYearAverages: {
    REVPAR: {value: '$156.00', compsetCompare: 3},
    ADR: {value: '$191.00', compsetCompare: 1},
    OCC: {value: '78.00%', compsetCompare: 4, pullRight: true}
  },
  abConversionPercentage: {
    a: 15,
    b: 85,
    sparx: 73,
    baseline: 50,
    ratio: 1.46
  },
  publishConsensus: {
    yes: 4,
    no: 1
  }
};

const strategy2 = {
  strategyName: 'Summer Loyalty Point Extravaganza',
  sparkName: 'SPARx Strategy 2',
  effectiveBookingDates: {
    start: '2019-06-01',
    end: '2019-07-31'
  },
  effectiveStayDates: {
    start: '2019-06-15',
    end: '2019-08-30'
  },
  propertyCluster: 'All Crowne Plaza Hotels (820)',
  includedRateCategories: [
    {
      name: '1 IKME3: IHG RC 1000 PTS NT',
      otherData: '- All Channels/Mon, Tue, Wed, Thu/2+ Nights'
    },
    {
      name: '2 IKPCM: 1000 BONUS POINTS NT',
      otherData: '- All Channels/Weekend Only/Fri, Sat/Sun/2+ Nights'
    },
    {
      name: '3 IGBBB: BEST FLEX WITH BKFST',
      otherData: '- Web/Weekend Only/Fri, Sat/Sun/2+ Nights'
    },
  ],
  successYoyTargets: {
    REVPAR: {from: 0.5, to: 0.65, strategyTrend: 3},
    ADR: {from: 1.8, to: 2.2, strategyTrend: 2},
    OCC: {from: 2.7, to: 3.2, strategyTrend: 4}
  },
  priorYearAverages: {
    REVPAR: {value: '$156.00', compsetCompare: 3},
    ADR: {value: '$191.00', compsetCompare: 1},
    OCC: {value: '78.00%', compsetCompare: 4, pullRight: true}
  },
  abConversionPercentage: {
    a: 15,
    b: 85,
    sparx: 61,
    baseline: 50,
    ratio: 1.22
  },
  publishConsensus: {
    yes: 4,
    no: 1
  }
};

export const sparkStrategyData = [
  strategy1,
  strategy2
];

