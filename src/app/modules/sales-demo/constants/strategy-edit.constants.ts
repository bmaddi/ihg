export const strategyEdit = {
    segmentClusters: [{
      name: 'Travel Agencies (Seg-1)',
      newRate: null,
      priorRate: 193,
      modified: false
    }, {
      name: 'Corporate/Government (Seg-2)',
      newRate: null,
      priorRate: 194,
      modified: false
    }, {
      name: `Direct-OTA's (Seg-3)`,
      newRate: null,
      priorRate: 193.5,
      modified: false
    }, {
      name: `Transient (Seg-4)`,
      newRate: null,
      priorRate: 192,
      modified: false
    }],
    specials: [{
      name: 'Drink Specials',
      newRate: null,
      priorRate: 0.74,
      modified: false
    }, {
      name: 'Dinner Specials',
      newRate: null,
      priorRate: 0.45,
      modified: false
    }, {
      name: 'Bike Rentals',
      newRate: null,
      priorRate: 0.15
    }, {
      name: 'Tours of Town',
      newRate: null,
      priorRate: 1.25
    }],
    roomAttributes: [{
      name: 'King Bed',
      newRate: null,
      priorRate: 0.1
    }, {
      name: 'Room Service Included',
      newRate: null,
      priorRate: 0.25
    }, {
      name: 'Oceanview',
      newRate: null,
      priorRate: 0.22
    }]
};
