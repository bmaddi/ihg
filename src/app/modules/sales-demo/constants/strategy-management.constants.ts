const rescaleRate = (rate: number) => {
  return rate * 5 - 4;
};

const strategy1 = {
  name: 'Summer Loyalty Point Extravaganza',
  effectiveBookingDates: {
    start: '2019-06-01',
    end: '2019-07-31'
  },
  effectiveStayDates: {
    start: '2019-06-15',
    end: '2019-08-30'
  },
  propertyCluster: 'All Crowne Plaza Hotels (820)',
  channels: 'All Channels',
  sparkProposedStrategies: 2,
  currentAnalystRating: {
    rate: rescaleRate(1.36),
    rateLabel: '1.36'
  },
  state: 'New',
  opensViewScreen: true
};

const strategy2 = {
  name: `Family Waterfront Fun's Sun`,
  effectiveBookingDates: {
    start: '2019-03-01',
    end: '2019-04-30'
  },
  effectiveStayDates: {
    start: '2019-06-15',
    end: '2019-08-15'
  },
  propertyCluster: 'Waterfront Properties in Northern Hemisphere (95)',
  channels: 'Web/B2B',
  sparkProposedStrategies: 9,
  currentAnalystRating: {
    rate: rescaleRate(2.15),
    rateLabel: '2.15'
  },
  state: 'Active',
  opensViewScreen: false
};

const strategy3 = {
  name: 'Get That Marathon Business',
  effectiveBookingDates: {
    start: '2019-01-01',
    end: '2019-06-30'
  },
  effectiveStayDates: {
    start: '2019-09-01',
    end: '2019-11-15'
  },
  propertyCluster: 'Cities Hosting Marathon\'s Fall 2019 (37)',
  channels: 'Web/B2B',
  sparkProposedStrategies: 6,
  currentAnalystRating: {
    rate: rescaleRate(0.97),
    rateLabel: '.97'
  },
  state: 'Analysis',
  opensViewScreen: false
};

export const strategyManagementData = [
  strategy1,
  strategy2,
  strategy3
];

