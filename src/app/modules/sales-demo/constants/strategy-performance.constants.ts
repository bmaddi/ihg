const performance1 = {
  version: '1.3',
  name: 101,
  status: 'failure',
  startDate: '2018-06-01',
  endDate: '2018-08-15',
  projectedUplift: {
    REVPAR: 0.5,
    ADR: 2.0,
    OCC: 3.0
  },
  actualUplift: {
    REVPAR: 0.3,
    ADR: 2.0,
    OCC: 2.8
  },
  explanation: {
    text: 'The results of this strategy did not meet the goals for the following reasons:',
    reasons: [
      'Occupancy did not hit target',
      'Competitor OCC to share from IHG properties'
    ]
  }
};

const performance2 = {
  version: '1.0',
  name: 102,
  status: 'success',
  startDate: '2018-06-01',
  endDate: '2018-07-31',
  projectedUplift: {
    REVPAR: 0.5,
    ADR: 2.0,
    OCC: 3.0
  },
  actualUplift: {
    REVPAR: 0.6,
    ADR: 2.5,
    OCC: 3.0
  },
  explanation: {
    text: 'The results of this strategy meet the goals for the following reasons:',
    reasons: [
      'Occupancy did exceed strategy target',
      'Segmentation maximized ADR and met OCC goals'
    ]
  }
};

const performance3 = {
  version: '2.0',
  name: 103,
  status: 'balanced',
  startDate: '2018-06-01',
  endDate: '2018-06-30',
  projectedUplift: {
    REVPAR: 0.8,
    ADR: 2.5,
    OCC: 3.5
  },
  actualUplift: {
    REVPAR: 0.8,
    ADR: 2.5,
    OCC: 3.5
  },
  explanation: {
    text: 'The results of this strategy meet the goals for the following reasons:',
    reasons: [
      'The reduced strategy period allowed a more aggressive rate mix when applied to the brand'
    ]
  }
};

export const strategyPerformanceData = [
  performance1,
  performance2,
  performance3
];

