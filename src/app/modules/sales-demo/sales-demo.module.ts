import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

import { IhgNgCommonCoreModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';

import { SalesDemoRoutes } from '@app/modules/sales-demo/sales-demo.routes';
import { SalesDemoService } from '@app/modules/sales-demo/services/sales-demo.service';
import { ProposedStrategyItemComponent } from './components/old/proposed-strategy-item/proposed-strategy-item.component';
import { ProposedStrategiesComponent } from './components/old/proposed-strategies/proposed-strategies.component';
import { StrategyPerformanceComponent } from './components/old/strategy-performance/strategy-performance.component';
import { StrategyPerformanceItemComponent } from './components/old/strategy-performance-item/strategy-performance-item.component';
import { StrategyEditModalComponent } from './components/old/strategy-edit-modal/strategy-edit-modal.component';
import { ViewPerformanceModalComponent } from './components/old/view-performance-modal/view-performance-modal.component';
import { StrategyManagementItemComponent } from './components/strategy-management/strategy-management-item/strategy-management-item.component';
import { StrategyManagementListComponent } from './components/strategy-management/strategy-management-list/strategy-management-list.component';
import { StrategyCreateModalComponent } from '@app/modules/sales-demo/components/strategy-management/strategy-create-modal/strategy-create-modal.component';
import { ViewPerformanceModal2Component } from '@app/modules/sales-demo/components/strategy/view-performance-modal/view-performance-modal.component';
import { SparkStrategyItemComponent } from '@app/modules/sales-demo/components/strategy/spark-strategy-item/spark-strategy-item.component';
import { StrategyViewComponent } from '@app/modules/sales-demo/components/strategy/strategy-view/strategy-view.component';
import { StrategyPerformance2Component } from '@app/modules/sales-demo/components/strategy-performance/strategy-performance/strategy-performance.component';
import { StrategyPerformanceItem2Component } from '@app/modules/sales-demo/components/strategy-performance/strategy-performance-item/strategy-performance-item.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbDropdownModule.forRoot(),
    NgbModule.forRoot(),
    TranslateModule,
    GridModule,
    IhgNgCommonCoreModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    DropDownsModule,
    DateInputsModule,
    SalesDemoRoutes
  ],
  declarations: [
    ProposedStrategiesComponent,
    ProposedStrategyItemComponent,
    StrategyPerformanceComponent,
    StrategyPerformanceItemComponent,
    StrategyEditModalComponent,
    ViewPerformanceModalComponent,
    StrategyManagementItemComponent,
    StrategyManagementListComponent,
    StrategyCreateModalComponent,
    ViewPerformanceModal2Component,
    SparkStrategyItemComponent,
    StrategyViewComponent,
    StrategyPerformance2Component,
    StrategyPerformanceItem2Component
  ],
  providers: [
    SalesDemoService
  ],
  entryComponents: [
    StrategyEditModalComponent,
    ViewPerformanceModalComponent,
    StrategyCreateModalComponent,
    ViewPerformanceModal2Component
  ]
})
export class SalesDemoModule {
}
