import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ProposedStrategiesComponent} from './components/old/proposed-strategies/proposed-strategies.component';
import { StrategyPerformanceComponent } from './components/old/strategy-performance/strategy-performance.component';
import { StrategyManagementListComponent } from './components/strategy-management/strategy-management-list/strategy-management-list.component';
import { StrategyViewComponent } from '@app/modules/sales-demo/components/strategy/strategy-view/strategy-view.component';
import { StrategyPerformance2Component } from '@app/modules/sales-demo/components/strategy-performance/strategy-performance/strategy-performance.component';

const routes: Routes = [
  {
    path: 'sales-proposed-strategies',
    component: ProposedStrategiesComponent,
    data: {
      pageTitle: 'Sell Strategy Analysis',
      stateName: 'sales-proposed-strategies',
      maxWidth: true
    }
  },
  {
    path: 'sales-strategy-performance',
    component: StrategyPerformanceComponent,
    data: {
      pageTitle: 'Sell Strategy Performance',
      stateName: 'sales-strategy-performance',
      maxWidth: true
    }
  },
  {
    path: 'sales-strategy-management',
    component: StrategyManagementListComponent,
    data: {
      pageTitle: 'Sell Strategy Analysis',
      stateName: 'sales-strategy-management',
      maxWidth: true,
      corporate: true,
      customSubtitle: 'IHG'
    }
  },
  {
    path: 'sales-strategy-view',
    component: StrategyViewComponent,
    data: {
      pageTitle: 'Sell Strategy Analysis',
      stateName: 'sales-strategy-view',
      maxWidth: true,
      corporate: true,
      customSubtitle: 'IHG'
    }
  },
  {
    path: 'sales-past-strategy-performance',
    component: StrategyPerformance2Component,
    data: {
      pageTitle: 'Sell Strategy Analysis',
      stateName: 'sales-past-strategy-performance',
      maxWidth: true,
      corporate: true,
      customSubtitle: 'IHG'
    }
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SalesDemoRoutes {}
