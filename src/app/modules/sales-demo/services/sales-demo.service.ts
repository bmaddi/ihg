import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';
import * as moment from 'moment';
import { consts } from '@app/modules/sales-demo/constants/consts.constants';
import { AppConstants } from '@app/constants/app-constants';


@Injectable({
  providedIn: 'root'
})
export class SalesDemoService {

  strategyManagementData: any[];
  sparkStrategyData: any[];

  constructor(
  ) {
    this.strategyManagementData = consts.strategyManagementData;
    this.sparkStrategyData = consts.sparkStrategyData;
  }

  public getStrategyManagementData() {
    return this.strategyManagementData;
  }

  public getSparkStrategyData() {
    return this.sparkStrategyData;
  }

  public getProposedSellStrategies() {
    return consts.proposedSellStrategies;
  }

  public getStrategyPerformanceData() {
    return consts.strategyPerformanceData;
  }

  public getStrategyEdit() {
    return consts.strategyEdit;
  }

  public getPastStrategyData() {
    return consts.pastStrategyConstants;
  }

  private rescaleRate(rate: number) {
    return rate * 5 - 4;
  }

  private getAbRatio(aPerc: number, bPerc: number) {
    if (aPerc && bPerc && aPerc > 0 && bPerc > 0) {
      return aPerc / bPerc;
    } else {
      return 0;
    }
  }

  private formatRatio(ratio: number) {
    const ratioStr = ratio.toFixed(2);
    const withoutLeadingZero = ratioStr.startsWith('0.') ? ratioStr.substring(1) : ratioStr;
    return withoutLeadingZero;
  }

  public createStrategy(createStrategyFormData: any) {
    const {
      strategyName,
      booking,
      stay,
      propertyCluster,
      channel,
      aPercentage,
      bPercentage
    } = createStrategyFormData.formData;

    const abRatio = this.getAbRatio(aPercentage, bPercentage);

    const newStrategy = {
      name: strategyName,
      effectiveBookingDates: {
        start: moment(booking.start).format(AppConstants.DB_DT_FORMAT),
        end: moment(booking.end).format(AppConstants.DB_DT_FORMAT)
      },
      effectiveStayDates: {
        start: moment(stay.start).format(AppConstants.DB_DT_FORMAT),
        end: moment(stay.end).format(AppConstants.DB_DT_FORMAT)
      },
      propertyCluster: propertyCluster,
      channels: channel,
      sparkProposedStrategies: 0,
      currentAnalystRating: {
        rate: this.rescaleRate(abRatio),
        rateLabel: this.formatRatio(abRatio)
      },
      state: 'New'
    };

    this.strategyManagementData.push(newStrategy);
  }

}
