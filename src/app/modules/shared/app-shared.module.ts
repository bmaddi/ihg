import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { FormsModule } from '@angular/forms';

import { FeedbackModule } from 'ihg-ng-common-core';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';

import { DayPickerComponent } from '@app-shared/components/day-picker/day-picker.component';
import { ServiceErrorComponent } from '@app-shared/components/app-service-errors/service-error/service-error.component';
import { ErrorToastMessageComponent } from '@app-shared/components/error-toast-message/error-toast-message.component';
import { HelpComponent } from '@app-shared/components/help/help.component';
import { CardKebabComponent } from '@app-shared/components/card-kebab/card-kebab.component';
import { PageLevelKebabComponent } from '@app-shared/components/page-level-kebab/page-level-kebab.component';
import { SmallErrorCardComponent } from './components/small-error-card/small-error-card.component';
import { ViewportCheckDirective } from './directives/viewport-check/viewport-check.directive';
import { AlphanumericDirective } from './directives/alpha-numeric/alpha-numeric.directive';
import { TruncatedTooltipComponent } from './components/truncated-tooltip/truncated-tooltip.component';
import { LookUpFieldComponent } from './components/look-up-field/look-up-field.component';
import { HighlightPipe } from './pipes/text-highlight';
import { TruncateTextPipe } from './pipes/shorten-text.pipe';
import { ToggleSwitchComponent } from './components/toggle-switch/toggle-switch.component';
import { SelectiveTitleCasePipe } from './pipes/selective-title-case/selective-title-case.pipe';
import { SentenceCasePipe } from './pipes/sentence-case/sentence-case.pipe';
import { MaxHeightViewMoreComponent } from './components/max-height-view-more/max-height-view-more.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    IhgNgCommonComponentsModule,
    NgbModule.forRoot(),
    DateInputsModule,
    FeedbackModule,
    FormsModule,
    JWBootstrapSwitchModule
  ],
  declarations: [
    ServiceErrorComponent,
    DayPickerComponent,
    ErrorToastMessageComponent,
    HelpComponent,
    CardKebabComponent,
    PageLevelKebabComponent,
    SmallErrorCardComponent,
    ViewportCheckDirective,
    AlphanumericDirective,
    TruncatedTooltipComponent,
    LookUpFieldComponent,
    HighlightPipe,
    TruncateTextPipe,
    ToggleSwitchComponent,
    SelectiveTitleCasePipe,
    SentenceCasePipe,
    MaxHeightViewMoreComponent
  ],
  exports: [
    ServiceErrorComponent,
    DayPickerComponent,
    ErrorToastMessageComponent,
    HelpComponent,
    CardKebabComponent,
    PageLevelKebabComponent,
    ViewportCheckDirective,
    AlphanumericDirective,
    TruncatedTooltipComponent,
    LookUpFieldComponent,
    HighlightPipe,
    TruncateTextPipe,
    ToggleSwitchComponent,
    SelectiveTitleCasePipe,
    SentenceCasePipe,
    MaxHeightViewMoreComponent
  ]
})
export class AppSharedModule { }
