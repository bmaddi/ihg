import { HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';
import { isArray } from 'lodash';

import { EmitErrorModel, ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';

export class AppErrorBaseComponent {

  public hasErrors: boolean;
  public emitError = new BehaviorSubject<EmitErrorModel>(null);

  constructor() {
  }

  init() {
    this.resetAnyErrors();
  }

  public checkIfAnyApiErrors(data: any, checkNoData?: (data: any) => boolean, error?: Subject<EmitErrorModel>): boolean {
    const hasNoData = !data || checkNoData && checkNoData(data);
    const errorSubject = error || this.emitError;
    if (hasNoData) {
      this.hasErrors = true;
      errorSubject.next({ hasNoData: true });
    } else {
      this.handleDataError(data, errorSubject);
    }
    return this.hasErrors;
  }

  private handleDataError(data: any, errorSubject: Subject<EmitErrorModel>): void {
    if (this.checkErrorInData(data)) {
      this.hasErrors = true;
      const errorsList = data.errors || this.getErrorsList(data);
      errorSubject.next({ apiErrors: errorsList });
    } else {
      this.resetSpecificError(errorSubject);
      this.hasErrors = false;
    }
  }

  private checkErrorInData(data: any): boolean {
    if (isArray(data) && data.length) {
      return data.some(item => !!(item.errors && item.errors.length));
    }
    return !!(data && data.errors && data.errors.length);
  }

  private getErrorsList(data: any) {
    let errors = [];
    data.forEach(item => {
      if (item.errors && item.errors.length) {
        errors = errors.concat(item.errors);
      }
    });
    return errors;
  }

  public processHttpErrors(httpData: any, error?: Subject<EmitErrorModel>): void {
    const errorSubject = error || this.emitError;
    this.hasErrors = true;

    if (httpData instanceof HttpErrorResponse) {
      errorSubject.next({ httpError: httpData });
    } else if (httpData instanceof Object) {
      errorSubject.next({ apiErrors: [this.mapToErrorsModel(httpData)] });
    } else {
      errorSubject.next({ apiErrors: [this.mapToErrorsModel({ code: 'error', message: 'error' })] });
    }
  }

  mapToErrorsModel(httpData: Object): ErrorsModel {
    return {
      code: (<any>httpData).name,
      message: (<any>httpData).message
    };
  }

  public resetAnyErrors(): void {
    this.hasErrors = false;
  }

  public resetSpecificError(error: Subject<EmitErrorModel>): void {
    error.next(null);
  }

  destroy() {
  }

}
