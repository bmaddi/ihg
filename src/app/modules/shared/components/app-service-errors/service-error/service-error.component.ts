import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';

import { EmitErrorModel, SERVICE_ERROR_TYPE, ERROR_CARD_TYPE } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';

interface ErrorConfigEntry {
  hideError?: boolean;
  header: string;
  headerValues?: object;
  message: string;
  messageValues?: object;
  maxErrorMessage?: string;
  maxErrorMessageValues?: object;
  icon: string;
  showRefreshBtn: boolean;
  refreshActionText: string;
}

export interface ServiceErrorComponentConfig {
  timeout?: ErrorConfigEntry;
  general?: ErrorConfigEntry;
  noData?: {
    header: string;
    headerValues?: object;
    message: string;
    messageValues?: object;
    showHorizontalLine?: boolean;
  };
}

@Component({
  selector: 'app-service-error',
  templateUrl: './service-error.component.html',
  styleUrls: ['./service-error.component.scss']
})
export class ServiceErrorComponent implements OnInit, OnDestroy {
  private readonly maxErrorsCount = 3;
  private errorCount = 0;
  private events$: Subscription;
  header: string;
  message: string;
  showRefreshBtn: boolean;
  refreshActionText: string;
  icon: string;
  hideError: boolean;
  showHorizontalLine: boolean;

  @Input() showSpinner = false;
  @Input() errorEvent: Observable<EmitErrorModel>;
  @Input() showReportAnIssue = false;
  @Input() config: ServiceErrorComponentConfig;
  @Input() reportIssueAutoFill;
  @Input() cardType: ERROR_CARD_TYPE = ERROR_CARD_TYPE.CONTAINER_ERROR;
  @Output() refreshAction = new EventEmitter();
  @Output() reportIssueAction = new EventEmitter();

  serviceErrorType = SERVICE_ERROR_TYPE;
  public errorType: SERVICE_ERROR_TYPE;
  public errorCardType = ERROR_CARD_TYPE;

  private defaultConfig: ServiceErrorComponentConfig = {
    timeout: {
      hideError: false,
      header: 'LBL_ERR_DATA_TIMEOUT',
      message: 'LBL_ERR_TAKING_TIME',
      icon: 'fal fa-hourglass',
      showRefreshBtn: true,
      refreshActionText: 'LBL_ERR_TRYAGAIN'
    },
    general: {
      hideError: false,
      header: 'LBL_ERR',
      message: 'COM_MSG_SOME_THNG_WRNG',
      icon: 'fas fa-exclamation-triangle',
      showRefreshBtn: true,
      refreshActionText: 'COM_BTN_RFRSH'
    },
    noData: {
      header: 'LBL_ERR_NO_DATA_AVAILABLE',
      message: 'LBL_ERR_TRY_LATER'
    }
  };

  constructor(private appErrorsService: AppErrorsService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.initConfig();
    this.subscribeToErrorEvent();
    this.subscribeToLanguageChange();
  }

  private initConfig(): void {
    this.config = {
      ...this.defaultConfig,
      ...this.config
    };
  }

  private subscribeToErrorEvent(): void {
    this.events$ = this.errorEvent.subscribe((error: EmitErrorModel) => {
      if (error) {
        this.errorCount = error.hasNoData ? 0 : this.errorCount + 1;
        this.initOrRefresh(error);
      } else {
        this.errorCount = 0;
      }
    });
  }

  private subscribeToLanguageChange() {
    this.events$ = this.translate.onLangChange.subscribe(() => {
      this.initView();
    });
  }

  private initOrRefresh(error: EmitErrorModel): void {
    if (error.hasNoData || error.httpError || (error.apiErrors && error.apiErrors.length)) {
      this.errorType = this.appErrorsService.getErrorType(error);
      this.initView();
    }
  }

  initView(): void {
    this.displayReportAnIssue();
    switch (this.errorType) {
      case SERVICE_ERROR_TYPE.TIME_OUT:
        this.initTimeOutViewData();
        break;
      case SERVICE_ERROR_TYPE.GENERAL:
        this.initGeneralErrorViewData();
        break;
      case SERVICE_ERROR_TYPE.NO_DATA:
        this.initNoDataErrorViewData();
        break;
    }
  }

  private displayReportAnIssue(): void {
    if (this.showReportAnIssue !== null) {
      this.showReportAnIssue = this.hasReachedMaxError();
    }
  }

  private initFromErrorConfigEntry(configEntry: ErrorConfigEntry): void {
    this.header = this.checkTranslationValues(configEntry.headerValues, configEntry.header);
    this.message = this.getMessage(configEntry);
    this.icon = configEntry.icon;
    this.showRefreshBtn = configEntry.showRefreshBtn;
    this.refreshActionText = configEntry.refreshActionText;
    this.hideError = configEntry.hideError;
  }

  private getMessage(configEntry: ErrorConfigEntry): string {
    if (this.hasReachedMaxError() && configEntry.maxErrorMessage) {
      return this.checkTranslationValues(configEntry.maxErrorMessageValues, configEntry.maxErrorMessage);
    }
    return this.checkTranslationValues(configEntry.messageValues, configEntry.message);
  }

  private checkTranslationValues(translationValues: object, content: string): string {
    return translationValues ? this.translate.instant(content, translationValues) : content;
  }

  hasReachedMaxError(): boolean {
    return this.errorCount >= this.maxErrorsCount;
  }

  initTimeOutViewData(): void {
    this.initFromErrorConfigEntry(this.config.timeout || this.defaultConfig.timeout);
  }

  initGeneralErrorViewData(): void {
    this.initFromErrorConfigEntry(this.config.general || this.defaultConfig.general);
  }

  initNoDataErrorViewData(): void {
    const headerValues = this.config.noData.headerValues;
    const messageValues = this.config.noData.messageValues;
    this.header = headerValues ? this.translate.instant(this.config.noData.header, headerValues) : this.config.noData.header;
    this.message = messageValues ? this.translate.instant(this.config.noData.message, messageValues) : this.config.noData.message;
    this.showHorizontalLine = this.config.noData.showHorizontalLine;
  }

  onRefreshClick() {
    this.refreshAction.emit(this.errorType);
  }

  getAutoFill(config: any): ReportIssueAutoFillData {
    if (!!config.timeoutError) {
      return this.errorType === SERVICE_ERROR_TYPE.TIME_OUT ? config.timeoutError : config.genericError;
    } else {
      return config;
    }
  }

  onReportIssue() {
    this.reportIssueAction.emit();
    this.appErrorsService.openReportIssueModal(this.getAutoFill(this.reportIssueAutoFill));
  }

  ngOnDestroy() {
    this.events$.unsubscribe();
  }

}
