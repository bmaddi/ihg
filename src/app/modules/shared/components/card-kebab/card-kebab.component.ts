import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'hin-card-kebab',
  templateUrl: './card-kebab.component.html',
  styleUrls: ['./card-kebab.component.scss']
})
export class CardKebabComponent implements OnInit {
  @Output() openChange = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  onOpenChange(value) {
    this.openChange.emit(value);
  }

}