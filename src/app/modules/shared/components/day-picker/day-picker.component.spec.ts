import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayPickerComponent } from './day-picker.component';
import { CardKebabComponent } from '../card-kebab/card-kebab.component';

xdescribe('DayPicker2Component', () => {
  let component: DayPickerComponent;
  let fixture: ComponentFixture<DayPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ],
      declarations: [ CardKebabComponent, DayPickerComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
