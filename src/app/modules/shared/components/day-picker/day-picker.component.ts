import { Component, Input, Output, OnInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

import { DAY_PCKR_CONST } from './day-picker.constants';
import { AppConstants } from '@app/constants/app-constants';

@Component({
  selector: 'app-day-picker',
  templateUrl: './day-picker.component.html',
  styleUrls: ['./day-picker.component.scss']
})
export class DayPickerComponent implements OnInit, OnChanges {
  @Input() selected = 0;
  @Input() count = 8;
  @Input() preventChange = false;
  @Input() startDate: Date;
  @Output() change = new EventEmitter<number>();
  @Output() openChange = new EventEmitter<boolean>();

  keyMap = DAY_PCKR_CONST.KEYMAP;

  offsets;
  days = [];

  constructor() {
    this.offsets = _.range(this.count);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.startDate && changes.startDate.currentValue) {
      this.getDays();
    }
  }

  formatDateMonthUppercase(date: Date) {
    return moment(date).format(AppConstants.VW_DT_FORMAT).toUpperCase();
  }

  getDays() {
    this.days = this.offsets.map((offset) => moment(this.startDate).add(offset, 'days').toDate());
  }

  onChange(selected: number) {
    if (!this.preventChange) {
      this.selected = selected > 0 ? selected : 0;
    }
    this.change.emit(selected);
  }

  onOpenChange(value) {
    this.openChange.emit(value);
  }
}
