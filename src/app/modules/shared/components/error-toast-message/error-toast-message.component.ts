import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { AlertType, Alert, AlertButton, AlertButtonType } from 'ihg-ng-common-core';
import { ReportIssueAutoFillData } from 'ihg-ng-common-pages';

import { AppErrorsService } from '@app/modules/shared/services/app-service-errors/app-errors.service';
import { SERVICE_ERROR_TYPE, EmitErrorModel } from '@app/modules/shared/models/app-service-errors/app-service-errors-models';

interface ErrorTranslation {
  message: string;
  detailMessage: string;
  detailMessageMoreErrors?: string;
}

export interface ErrorToastMessageTranslations {
  retryButton: string;
  reportAnIssueButton: string;
  timeoutError: ErrorTranslation;
  generalError: ErrorTranslation;
}

@Component({
  selector: 'app-error-toast-message',
  templateUrl: './error-toast-message.component.html'
})
export class ErrorToastMessageComponent implements OnInit, OnDestroy {
  alert: Alert;

  @Output() retryAction = new EventEmitter();
  @Output() reportAnIssueAction = new EventEmitter();
  @Input() errorEvent: Observable<EmitErrorModel>;
  @Input() maxNoErrors = 3;
  @Input() animation = false;
  @Input() showActionBtn = true;
  @Input() dismissible = false;
  @Input() translations: ErrorToastMessageTranslations;
  @Input() reportIssueAutoFill;
  @Input() alertButtonType = AlertButtonType.Danger;
  @Input() alertType = AlertType.Danger;

  private defaultTranslations: ErrorToastMessageTranslations = {
    retryButton: 'LBL_RETRY',
    reportAnIssueButton: 'REPORT_AN_ISSUE_MENU',
    timeoutError: {
      message: 'TOAST_TIMEOUT',
      detailMessage: 'TOAST_TIMEOUT'
    },
    generalError: {
      message: 'COM_TOAST_ERR',
      detailMessage: 'COM_ERR_TOAST_TRY_AGN'
    }
  };

  private noErrors = 0;
  private errorSubscription$: Subscription;
  private errorType;

  constructor(private appErrorsService: AppErrorsService) {
  }

  ngOnInit() {
    this.initTranslations();
    this.initAlertHandling();
  }

  private initAlertHandling(): void {
    this.errorSubscription$ = this.errorEvent.subscribe(this.handleError);
    this.updateAlert();
  }

  private initTranslations() {
    this.translations = {
      ...this.defaultTranslations,
      ...this.translations
    };
  }

  private handleError = (error: EmitErrorModel) => {
    const serviceErrorType = this.appErrorsService.getErrorType(error);
    if (serviceErrorType === null) {
      this.noErrors = 0;
    } else {
      this.noErrors++;
    }
    this.updateAlert(serviceErrorType);
  }

  private getRetryButton() {
    return <AlertButton>
      {
        label: this.translations.retryButton,
        type: this.alertButtonType,
        onClick: () => {
          this.retryAction.emit();
        }
      };
  }

  private getReportAnIssueButton() {
    const errorAutoFill = this.getAutoFill(this.reportIssueAutoFill);
    return <AlertButton>
      {
        label: this.translations.reportAnIssueButton,
        type: this.alertButtonType,
        onClick: () => {
          this.reportAnIssueAction.emit();
          this.appErrorsService.openReportIssueModal(errorAutoFill);
        }
      };
  }

  private getTimeoutAlert(tooManyErrors: boolean): Alert {
    const button = tooManyErrors ? this.getReportAnIssueButton() : this.getRetryButton();
    const translation = this.translations.timeoutError;
    const detailMessage = tooManyErrors ? translation.detailMessageMoreErrors || translation.detailMessage : translation.detailMessage;
    return {
      type: this.alertType,
      message: translation.message,
      detailMessage: detailMessage,
      dismissible: false,
      buttons: this.showActionBtn ? [button] : null,
      animation: this.animation
    };
  }

  private getGeneralAlert(tooManyErrors: boolean): Alert {
    const button = tooManyErrors ? this.getReportAnIssueButton() : this.getRetryButton();
    const translation = this.translations.generalError;
    const detailMessage = tooManyErrors ?
      translation.detailMessageMoreErrors || translation.detailMessage :
      translation.detailMessage;
    return {
      type: this.alertType,
      message: translation.message,
      detailMessage: detailMessage,
      dismissible: this.dismissible,
      buttons: this.showActionBtn ? [button] : null,
      animation: this.animation
    };
  }

  private createAlert(serviceErrorType?: SERVICE_ERROR_TYPE) {
    const tooManyErrors = this.noErrors >= this.maxNoErrors;
    switch (serviceErrorType) {
      case SERVICE_ERROR_TYPE.TIME_OUT:
        return this.getTimeoutAlert(tooManyErrors);
      case SERVICE_ERROR_TYPE.GENERAL:
        return this.getGeneralAlert(tooManyErrors);
      default:
        return null;
    }
  }

  private getAutoFill(config: any): ReportIssueAutoFillData {
    if (!!config.timeoutError) {
      return this.errorType === SERVICE_ERROR_TYPE.TIME_OUT ? config.timeoutError : config.genericError;
    } else {
      return config;
    }
  }

  private updateAlert(serviceErrorType?: SERVICE_ERROR_TYPE) {
    this.alert = null;
    this.errorType = serviceErrorType;
    setTimeout(() => this.alert = this.createAlert(serviceErrorType), 0); // reset component when fade animation is used .
  }

  public dismissError() {
    this.noErrors = 0;
    this.updateAlert(null);
  }

  ngOnDestroy() {
    this.errorSubscription$.unsubscribe();
  }
}
