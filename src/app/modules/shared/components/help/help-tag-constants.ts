export const HELP_TAGS = [
  {
    name: 'GUEST_LIST_PREPARE',
    link: 'guest-list-details',
    tag: 1021
  },
  {
    name: 'GUEST_LIST_ARRIVALS',
    link: 'guest-list-details',
    tag: 1022
  },
  {
    name: 'CHECK_IN',
    link: 'check-in',
    tag: 1023
  },
  {
    name: 'GUEST_PROFILE',
    link: 'guest-profile',
    tag: 1024
  },
  {
    name: 'GUEST_LIST_IN_HOUSE',
    link: 'guest-list-details',
    tag: 1025
  },
  {
    name: 'GUEST_LIST_ALL_GUESTS',
    link: 'guest-list-details',
    tag: 1028
  },
  {
    name: 'LBL_MANAGE_STAY',
    link: 'manage-stay',
    tag: 1026
  },
  {
    name: 'WALK_INS',
    link: 'walk-ins',
    tag: 1027
  }
];
