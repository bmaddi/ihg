import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { GoogleAnalyticsService } from 'ihg-ng-common-core';

import { HELP_TAGS } from './help-tag-constants';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';
import { HelpService } from './help.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit, OnDestroy {
  private readonly HELP_LINK = 'assets/help-content/index.htm#cshid=';
  private readonly defaultHelpId = 1000;
  private tabLevelHelpId: number;
  private rootSubscription = new Subscription();
  private ga = GUEST_CONST.GA;
  @Input() helpId;

  constructor(
    private activatedRoute: ActivatedRoute,
    private helpService: HelpService,
    private router: Router,
    private gaService: GoogleAnalyticsService) {
  }

  ngOnInit() {
    this.handleRouteChange();
  }

  openHelp() {
    const helpTagId = this.helpId ? this.helpId : (this.tabLevelHelpId ? this.tabLevelHelpId
      : this.getPageLevelHelpId());
    const helpLink = this.HELP_LINK + helpTagId;
    if (helpTagId && this.ga.HELP_CONSTANT[helpTagId]) {
      this.gaService.trackEvent(this.ga.HELP_CONSTANT[helpTagId].category, this.ga.HELP, this.ga.SELECTED);
    }
    setTimeout(() => {
      window.open(helpLink);
    });
  }

  private getHelpId(key: string, match: string): number {
    const currentLink = HELP_TAGS.find((item) => item[key] === match);
    return currentLink ? currentLink.tag : this.defaultHelpId;
  }

  private getPageLevelHelpId(): number {
    const stateName = this.activatedRoute.root.children[0].snapshot.data.stateName;
    return this.getHelpId('link', stateName);
  }

  private subscribeToTabbedStateHelpIdChange(): void {
    this.rootSubscription.add(this.helpService.getTabStateChange().subscribe((helpId: string) => {
      this.tabLevelHelpId = this.getHelpId('name', helpId);
    }));
  }

  private handleRouteChange(): void {
    this.rootSubscription.add(this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.tabLevelHelpId = null;
      }
    }));
    this.subscribeToTabbedStateHelpIdChange();
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }
}
