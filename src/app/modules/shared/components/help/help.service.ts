import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelpService {
  private tabStateChange = new Subject<string>();

  constructor() { }

  getTabStateChange(): Observable<string> {
    return this.tabStateChange.asObservable();
  }

  setTabStateChange(helpTagName: string): void {
    this.tabStateChange.next(helpTagName);
  }
}
