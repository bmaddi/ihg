import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookUpFieldComponent } from './look-up-field.component';

xdescribe('LookUpFieldComponent', () => {
  let component: LookUpFieldComponent;
  let fixture: ComponentFixture<LookUpFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookUpFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookUpFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
