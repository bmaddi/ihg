import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-lookup-field',
  templateUrl: './look-up-field.component.html',
  styleUrls: ['./look-up-field.component.scss']
})
export class LookUpFieldComponent implements OnInit {

  @ViewChild('tooltip') tooltip: NgbTooltip;
  @Input() tooltipText: string;
  @Output() openLookUpModalEvt: EventEmitter<null> = new EventEmitter();

  constructor() { }

  ngOnInit() { 
  }

  public openLookUpModal(): void {
    this.tooltip.close();
    this.openLookUpModalEvt.emit();
  }

}
