import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TitleCasePipe } from '@angular/common';
import { By } from '@angular/platform-browser';

import { MaxHeightViewMoreComponent } from './max-height-view-more.component';
import { guestComplaintDescription1, guestComplaintDescription3 } from '@modules/guest-relations/mocks/guest-relations.mock';

describe('MaxHeightViewMoreComponent', () => {
  let component: MaxHeightViewMoreComponent;
  let fixture: ComponentFixture<MaxHeightViewMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MaxHeightViewMoreComponent],
      providers: [TitleCasePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaxHeightViewMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check for content overflow on #ngAfterViewInit hook', () => {
    spyOn<any>(component, 'checkForElementOverflow').and.callThrough();
    component.content = guestComplaintDescription1 + guestComplaintDescription3;
    component.maxHeight = '25';
    fixture.detectChanges();
    expect(component.displayViewMore).toEqual(false);
    jasmine.clock().install();

    component.ngAfterViewInit();
    jasmine.clock().tick(100);

    expect(component['checkForElementOverflow']).toHaveBeenCalledTimes(1);
    expect(component['checkForElementOverflow']).toHaveBeenCalledTimes(1);
    expect(component.displayViewMore).toEqual(true);
    jasmine.clock().uninstall();
  });

  it('should add desired height styles to content div', () => {
    component.content = guestComplaintDescription1;
    component.maxHeight = '100';
    fixture.detectChanges();

    const content = fixture.debugElement.query(By.css('[data-slnm-ihg="ContentContainer-SID"]'));

    expect(content).not.toBeNull();
    console.log(content.nativeElement.style.height);
    expect(content.nativeElement.style.height).toEqual('100px');
    expect(content.nativeElement.style.maxHeight).toEqual('100px');
  });
});
