import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { delay, tap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

import { UtilityService } from '@services/utility/utility.service';

@Component({
  selector: 'app-max-height-view-more',
  templateUrl: './max-height-view-more.component.html',
  styleUrls: ['./max-height-view-more.component.scss']
})
export class MaxHeightViewMoreComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChild('heightContainer') hc: ElementRef;
  @Input() contentSlmnTag = 'ContentContainer-SID';
  @Input() viewMoreSlmnTag = 'ViewMore-SID';
  @Input() maxHeight = '200';
  @Input() content: string;
  @Input() viewMoreText = 'LBL_VIEW_MORE';
  @Output() viewMoreAction = new EventEmitter<void>();

  displayViewMore = false;

  private rootSubscription$ = new Subscription();

  constructor(private utilityService: UtilityService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.checkForElementOverflow();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.utilityService.hasSimpleParamChanges(changes, 'content')) {
      this.checkForElementOverflow();
    }
  }

  private checkForElementOverflow() {
    this.rootSubscription$.add(
      of(true)
        .pipe(
          delay(0),
          tap(() => this.checkIfShowViewMore())
        ).subscribe()
    );
  }

  private checkIfShowViewMore() {
    this.displayViewMore = this.hc &&
      this.maxHeight && (this.hc.nativeElement.scrollHeight > +this.maxHeight);
  }
}
