import { Component, OnInit } from '@angular/core';
import { NavigationEnd, ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FeedbackTopic, FeedbackService, FeedbackModalData, GoogleAnalyticsService } from 'ihg-ng-common-core';
import { FEEDBACK_KEYS } from '@app/modules/shared/enums/feedback-key-constants';
import { HelpService } from '@modules/shared/components/help/help.service';
import { GUEST_CONST } from '@app/modules/guests/guests.constants';

@Component({
  selector: 'app-page-level-kebab',
  templateUrl: './page-level-kebab.component.html',
  styleUrls: ['./page-level-kebab.component.scss']
})
export class PageLevelKebabComponent implements OnInit {
  private readonly defaultTabId = 1;
  private tabId: number;
  private rootSubscription = new Subscription();
  private ga = GUEST_CONST.GA;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private helpService: HelpService,
    private feedbackService: FeedbackService,
    private gaService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    this.subscribeToRouteChange();
    this.subscribeToTabbedStateChange();
  }

  private subscribeToTabbedStateChange(): void {
    this.rootSubscription.add(this.helpService.getTabStateChange().subscribe((name: string) => {
      this.tabId = this.getTabId('key', name) || null;
    }));
  }

  private subscribeToRouteChange(): void {
    this.rootSubscription.add(this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.tabId = null;
      }
    }));
  }

  onFeedbackClicked() {
    if (this.tabId) {
      this.feedbackService.openAddFeedbackModal(<FeedbackModalData>{
        title: this.getFeedbackTitle('id', this.tabId),
        topic: this.getFeedbackTopic('id', this.tabId),
      });
    }
    else {
      this.feedbackService.openAddFeedbackModal(<FeedbackModalData>{
        title: this.getPageLevelFeedbackTitle(),
        topic: this.getPageLevelFeedbackTopic()
      });
      {
        // Code changes to find tabId from StateName
        const stateName = this.activatedRoute.root.children[0].snapshot.data.stateName;
        const tabData = FEEDBACK_KEYS.find(item => item['stateName'] === stateName);
        this.tabId = tabData ? tabData.id : null;
      }
    }
    if (this.tabId) {
      this.gaService.trackEvent(this.ga.HELP_CONSTANT[this.tabId].category, this.ga.SEND_FEEDBACK, this.ga.SELECTED);
    }
  }

  private getTabId(key: string, match: string): number {
    const currentTab = FEEDBACK_KEYS.find((item) => item[key] === match);
    return currentTab ? currentTab.id : this.defaultTabId;
  }

  private getPageLevelFeedbackTopic(): string {
    const stateName = this.activatedRoute.root.children[0].snapshot.data.stateName;
    return this.getFeedbackTopic('stateName', stateName);
  }

  private getPageLevelFeedbackTitle(): string {
    const stateName = this.activatedRoute.root.children[0].snapshot.data.stateName;
    return this.getFeedbackTitle('stateName', stateName);
  }

  private getFeedbackTopic(key: string, match: any): string {
    const currentPage = FEEDBACK_KEYS.find((item) => item[key] === match);
    return currentPage ? currentPage.topic : "";
  }

  private getFeedbackTitle(key: string, match: any): string {
    const currentPage = FEEDBACK_KEYS.find((item) => item[key] === match);
    return currentPage ? currentPage.name : "";
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

}
