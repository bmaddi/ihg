import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallErrorCardComponent } from './small-error-card.component';

xdescribe('SmallErrorCardComponent', () => {
  let component: SmallErrorCardComponent;
  let fixture: ComponentFixture<SmallErrorCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallErrorCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallErrorCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
