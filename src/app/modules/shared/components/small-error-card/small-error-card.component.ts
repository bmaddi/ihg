import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MenuService } from 'ihg-ng-common-core';

@Component({
  selector: 'app-small-error-card',
  templateUrl: './small-error-card.component.html',
  styleUrls: ['./small-error-card.component.scss']
})
export class SmallErrorCardComponent implements OnInit {
  header = 'COM_TOAST_ERR';
  body = 'COM_MSG_SOME_THNG_WRNG';
  initialized = false;

  @Input()
  set errorHeader(inputHeader: string) {
    this.header = inputHeader;
  }
  @Input()
  set errorBody(inputBodyTest: string) {
  this.body = inputBodyTest;
  }
  @Input() showRefreshBtn: boolean;
  @Input() spinnerEnabled: boolean;
  @Input() showSpinner: boolean;
  @Input() showReportAnIssue: boolean;
  @Input() refreshActionText = 'COM_BTN_RFRSH';
  @Input() icon = 'fas fa-exclamation-triangle';
  @Input() handleReportAnIssue = true;
  @Output() refreshAction = new EventEmitter();
  @Output() reportAnIssue = new EventEmitter();

  constructor(private menuService: MenuService) { }

  ngOnInit() {
    this.initialized = true;
  }

  handleRefresh(): void {
    if (this.spinnerEnabled) {
      this.triggerSpinner();
    }
    this.refreshAction.emit();
  }

  private triggerSpinner(): void {
    this.showSpinner = true;
  }

  onReportIssue() {
    if (this.handleReportAnIssue) {
      this.menuService.openReportIssueModal();
    } else {
      this.reportAnIssue.emit();
    }
  }

}
