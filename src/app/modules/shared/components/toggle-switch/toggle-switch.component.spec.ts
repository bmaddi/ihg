import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { TranslateModule, TranslateStore } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { ToggleSwitchComponent } from './toggle-switch.component';

describe('ToggleSwitchComponent', () => {
  let component: ToggleSwitchComponent;
  let fixture: ComponentFixture<ToggleSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleSwitchComponent ],
      imports: [JWBootstrapSwitchModule, TranslateModule.forChild(), FormsModule],
      providers: [TranslateStore]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create with defaults', () => {
    expect(component.toggleState).toBeFalsy();
    expect(component.disabled).toBeFalsy();
    expect(component.width).toEqual(30);
  });

  it('should create and contain proper label & toggle with default inputs', () => {
    const container = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPost-SID"]'));
    const label = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostLabel-SID"]'));
    const toggle = fixture.debugElement.query(By.css('[data-slnm-ihg="NoPostToggle-SID"]'));
    expect(container).not.toBeNull();
    expect(label).not.toBeNull();
    expect(label.nativeElement.innerText).toEqual('LBL_POST');
    expect(toggle).not.toBeNull();
  });


  it('should update toggle to reflect toggle state change', () => {
    expect(component.toggleState).toBeFalsy();
    fixture.detectChanges();

    const off = fixture.debugElement.query(By.css('.bootstrap-switch-off'));
    expect(off).not.toBeNull();

    const spy = spyOn(component.toggleChange, 'emit');

    const toggle = fixture.debugElement.query(By.css('.bootstrap-switch'));
    toggle.nativeElement.click();

    fixture.whenRenderingDone().then(() => {
      fixture.detectChanges();
      const on = fixture.debugElement.query(By.css('.bootstrap-switch-on'));
      expect(on).toBeDefined();
      expect(component.toggleState).toBeTruthy();
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should prevent ngModel toggle update if preventAutomaticUpdate is true', () => {
    const updateParentSpy = spyOn(component.toggleChange, 'emit').and.callThrough();
    component.preventAutomaticUpdate = true;
    expect(component.toggleState).toBeFalsy();
    component.ngOnInit();
    fixture.detectChanges();

    component.switch.container.nativeElement.click();
    fixture.detectChanges();

    expect(component.toggleState).toBeFalsy();
    expect(updateParentSpy).toHaveBeenCalledWith(true);
  });

});
