import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { JWBootstrapSwitchDirective } from 'jw-bootstrap-switch-ng2/dist/directive';

@Component({
  selector: 'app-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.scss']
})
export class ToggleSwitchComponent implements OnInit, OnDestroy {
  @ViewChild(JWBootstrapSwitchDirective) switch: JWBootstrapSwitchDirective;
  @Input() toggleState = false;
  @Input() disabled = false;
  @Input() width = 30;
  @Input() slnmTag = 'NoPost';
  @Input() label = 'LBL_POST';
  @Input() preventAutomaticUpdate = false;
  @Output() toggleChange: EventEmitter<boolean> = new EventEmitter();

  private eventListener: Function[] = [];

  constructor(private renderer: Renderer2) {
  }

  ngOnInit() {
    if (this.preventAutomaticUpdate) {
      this.addSwitchListener();
    }
  }

  updateToggle() {
    this.toggleChange.emit(this.toggleState);
  }

  ngOnDestroy() {
    if (this.preventAutomaticUpdate) {
      this.eventListener.forEach(func => func());
    }
  }

  private addSwitchListener() {
    this.eventListener.push(
      this.renderer.listen(this.switch.container.nativeElement, 'click', (event: Event) => {
        event.preventDefault();
        event.stopPropagation();
        this.toggleChange.emit(!this.toggleState);
       }),
       this.renderer.listen(this.switch.container.nativeElement, 'mousedown', (event: Event) => {
        event.preventDefault();
        event.stopPropagation();
      })
    );
  }
}
