import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { find } from 'lodash';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';

import { TruncatedTooltipComponent } from './truncated-tooltip.component';

describe('TruncatedTooltipComponent', () => {
  let component: TruncatedTooltipComponent;
  let fixture: ComponentFixture<TruncatedTooltipComponent>;
  // tslint:disable-next-line:max-line-length
  const mockText = 'Alteration literature to or an sympathize mr imprudence. Of is ferrars subject as enjoyed or tedious cottage. Procuring as in resembled by in agreeable. Next long no gave mr eyes. Admiration advantages no he celebrated so pianoforte unreserved';

  const setUpMockData = () => {
    component.text = mockText;
    component.widthClass = 'test-max-width';
    component.slnmTag = 'Test';
  };

  beforeEach(async(() => {
    TestBed
      .overrideComponent(TruncatedTooltipComponent, {
        add: {
          styles: ['.test-max-width{max-width: 50px; width: 50px;}']
        }
      })
      .configureTestingModule({
        imports: [NgbTooltipModule],
        declarations: [TruncatedTooltipComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TruncatedTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add selenium tag if preset', () => {
    component.slnmTag = 'TestTag-SID';

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[data-slnm-ihg="TestTag-SID"]'))).not.toBeNull();
  });

  it('should truncate if text span exceeds element width', fakeAsync(() => {
    setUpMockData();
    const checkContainerOverflowSpy = spyOn<any>(component, 'checkContainerOverflow').and.callThrough();

    fixture.detectChanges();
    component.ngAfterViewInit();
    tick(1000);

    expect(checkContainerOverflowSpy).toHaveBeenCalled();
    expect(component.truncatedText).toEqual(true);
  }));

  it('should show truncated text on hover of truncated element', fakeAsync(() => {
    setUpMockData();
    const checkContainerOverflowSpy = spyOn<any>(component, 'checkContainerOverflow').and.callThrough();

    fixture.detectChanges();
    component.ngAfterViewInit();
    tick(1000);
    const el = fixture.debugElement.query(By.css('.test-max-width'));
    el.triggerEventHandler('mouseover', null);
    fixture.detectChanges();
    const tooltipAttribute = find(el.nativeElement.getAttributeNames(), e => e.indexOf('ngb-tooltip') > -1);

    expect(mockText.indexOf(el.nativeElement.getAttribute(tooltipAttribute)) > -1).toBeTruthy();
    expect(checkContainerOverflowSpy).toHaveBeenCalled();
    expect(component.truncatedText).toEqual(true);
  }));

  it('should check for overflow on window size event and truncate if necessary', fakeAsync(() => {
    setUpMockData();
    const checkContainerOverflowSpy = spyOn<any>(component, 'checkContainerOverflow').and.callThrough();

    fixture.detectChanges();
    component.ngOnInit();
    fixture.detectChanges();
    window.dispatchEvent(new Event('resize'));
    tick(1000);
    fixture.detectChanges();

    expect(checkContainerOverflowSpy).toHaveBeenCalledWith(component.truncateTextContainer);
    expect(component.truncatedText).toEqual(true);
  }));
});
