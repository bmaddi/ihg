import { AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { TruncatedTooltipService } from './truncated-tooltip.service';

@Component({
  selector: 'app-truncated-tooltip',
  templateUrl: './truncated-tooltip.component.html',
  styleUrls: ['./truncated-tooltip.component.scss']
})
export class TruncatedTooltipComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('ttc') truncateTextContainer: ElementRef;

  @Input() widthClass: string;
  @Input() text: string;
  @Input() slnmTag: string;
  @Input() debounceTime = 250;
  @Input() innerSpanTemplate: TemplateRef<any>;

  truncatedText = false;

  private windowResizeSubject = new Subject<void>();
  private windowResizeObservable = this.windowResizeSubject.asObservable().pipe(debounceTime(this.debounceTime));
  private subscriptions = new Subscription();

  constructor(private truncatedTooltipService: TruncatedTooltipService) {
  }

  ngOnInit() {
    this.subscriptions.add(this.windowResizeObservable.subscribe(() => this.checkContainerOverflow(this.truncateTextContainer)));
    this.subscriptions.add(this.truncatedTooltipService.contentChange$.subscribe(data => this.text = data));
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.checkContainerOverflow(this.truncateTextContainer);
    }, 0);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.windowResizeSubject.next();
  }

  @HostListener('mouseover')
  onMouseOver() {
    this.checkContainerOverflow(this.truncateTextContainer);
  }

  private checkContainerOverflow(element: ElementRef) {
    this.truncatedText = element.nativeElement.scrollWidth > element.nativeElement.clientWidth;
  }
}
