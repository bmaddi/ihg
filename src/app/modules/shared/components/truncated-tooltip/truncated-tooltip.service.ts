import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TruncatedTooltipService {
  private contentChangeSubject: Subject<string> = new Subject<string>();
  contentChange$: Observable<string> = this.contentChangeSubject.asObservable();

  constructor() { }

  updateContent(content: string): void {
    this.contentChangeSubject.next(content);
  }
}