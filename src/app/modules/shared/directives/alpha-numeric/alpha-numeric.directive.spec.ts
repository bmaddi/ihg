import { AlphanumericDirective } from './alpha-numeric.directive';

const elRefMock = {
  nativeElement: document.createElement('input')
};

describe('AlphanumericDirective', () => {
  it('should create an instance', () => {
    const directive = new AlphanumericDirective(elRefMock);
    expect(directive).toBeTruthy();
  });
});
