export interface ViewportModel {
  inViewport: boolean;
  elementBoundary: ClientRect;
};