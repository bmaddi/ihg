import { ViewportCheckDirective } from './viewport-check.directive';

const elRefMock = {
  nativeElement: document.createElement('div')
};

describe('ViewportCheckDirective', () => {
  it('should create an instance', () => {
    const directive = new ViewportCheckDirective(elRefMock);
    expect(directive).toBeTruthy();
  });
});
