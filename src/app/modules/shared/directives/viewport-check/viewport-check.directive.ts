import { Directive, Input, Output, EventEmitter, OnInit, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ViewportModel } from './models/viewport.model';

@Directive({
  selector: '[appViewportCheck]'
})
export class ViewportCheckDirective implements OnInit, OnDestroy, AfterViewInit {
  private readonly delay = 200;
  private eventsSubscriptions = new Subscription();

  @Input() partialVisibility = true;
  @Output() viewportCheck = new EventEmitter<ViewportModel>();

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    this.checkInViewportStatus();
    this.initEventHandler('scroll');
    this.initEventHandler('resize');
  }

  ngAfterViewInit() {
    this.checkInViewportStatus();
  }

  private initEventHandler(eventType: string): void {
    this.eventsSubscriptions.add(
      fromEvent(window, eventType).pipe(
        debounceTime(this.delay)
      ).subscribe(() => this.checkInViewportStatus())
    );
  }

  private checkInViewportStatus(): void {
    const elem = this.elementRef.nativeElement;
    const elementBoundary = elem.getBoundingClientRect();
    const winHeight = window.innerHeight;
    const topVisible = elementBoundary.top >= 0 && elementBoundary.top < winHeight;
    const bottomVisible = elementBoundary.bottom > 0 && elementBoundary.bottom <= winHeight;
    const inViewport = this.partialVisibility ? topVisible || bottomVisible : topVisible && bottomVisible;

    this.viewportCheck.emit({
      inViewport,
      elementBoundary
    });
  }

  ngOnDestroy() {
    this.eventsSubscriptions.unsubscribe();
  }
}
