export enum AppHttpStatuses {

  // 2×× Success
  OK = 200,
  NO_CONTENT = 204,

  // 5×× Server Error
  GATEWAY_TIMEOUT = 504,
  INTERNAL_SERVER_ERROR = 500

}
