export enum AppStateNames {
  manageStay = 'manage-stay',
  editStay = 'edit-stay',
  checkIn = 'check-in',
  guestListDetails = 'guest-list-details',
  guestProfile = 'guest-profile',
}
