import { FeedbackTopic } from 'ihg-ng-common-core';

export const FEEDBACK_KEYS = [
  {
    id: 1021,
    key: 'GUEST_LIST_PREPARE',
    name: 'FDK_TITLE_GUEST_LIST_PREPARE',
    stateName: 'guest-list-details',
    topic: FeedbackTopic.frontDeskPrepare
  },
  {
    id: 1022,
    key: 'GUEST_LIST_ARRIVALS',
    name: 'FDK_TITLE_GUEST_LIST_ARRIVALS',
    stateName: 'guest-list-details',
    topic: FeedbackTopic.frontDeskArrivals
  },
  {
    id: 1023,
    key: 'CHECK_IN',
    name: 'FDK_TITLE_CHECK_IN',
    stateName: 'check-in',
    topic: FeedbackTopic.frontDeskCheckIn
  },
  {
    id: 1024,
    key: 'GUEST_PROFILE',
    name: 'COM_TITLE_GUEST_PROFILE',
    stateName: 'guest_profile',
    topic: FeedbackTopic.frontDeskGuestProfile
  },
  {
    id: 1025,
    key: 'GUEST_LIST_IN_HOUSE',
    name: 'FDK_TITLE_GUEST_LIST_IN_HOUSE',
    stateName: 'guest-list-details',
    topic: FeedbackTopic.frontDeskGuestListInHouse
  },
  {
    id: 1028,
    key: 'GUEST_LIST_ALL_GUESTS',
    name: 'FDK_TITLE_GUEST_LIST_ALL_GUESTS',
    stateName: 'all-guests',
    topic: FeedbackTopic.frontDeskGuestListAllGuest
  },
  {
    id: 82012,
    key: 'MANAGE_STAY',
    name: 'FDK_TITLE_MANAGE_STAY',
    stateName: 'manage-stay',
    topic: FeedbackTopic.manageStay
  },
  {
    id: 5,
    key: 'WALK_IN',
    name: 'FDK_TITLE_WALK_IN',
    stateName: 'walk-in',
    topic: FeedbackTopic.frontDeskWalkIn
  }
];
