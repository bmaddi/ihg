import { HttpErrorResponse } from '@angular/common/http';

export enum SERVICE_ERROR_TYPE {
  TIME_OUT,
  NO_DATA,
  GENERAL
}

export enum ERROR_CARD_TYPE {
  CONTAINER_ERROR,
  LINE_ERROR,
  CARD_ERROR
}

export interface ErrorsModel {
  code: string;
  message: string;
}

export interface EmitErrorModel {
  apiErrors?: Array<ErrorsModel>;
  httpError?: HttpErrorResponse;
  hasNoData?: boolean;
}