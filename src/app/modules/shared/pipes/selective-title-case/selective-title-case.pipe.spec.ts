import { SelectiveTitleCasePipe } from './selective-title-case.pipe';
import { TitleCasePipe } from '@angular/common';

describe('SelectiveTitleCasePipe', () => {
  let pipe: SelectiveTitleCasePipe;

  beforeEach(() => {
    pipe = new SelectiveTitleCasePipe(new TitleCasePipe());
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform a string in all upper case to title case', () => {
    const actual = 'THIS IS A SENTENCE IN ALL CAPS. CONVERT THIS.';
    const expected = 'This Is A Sentence In All Caps. Convert This.';
    expect(pipe.transform(actual)).toEqual(expected);
  });

  it('should transform a string in all lower case to title case', () => {
    const actual = 'this is a sentence in all small. convert this.';
    const expected = 'This Is A Sentence In All Small. Convert This.';
    expect(pipe.transform(actual)).toEqual(expected);
  });

  it('should transform a string to title case except the all caps words', () => {
    const allCaps = ['IHG', 'RC'];
    const actual = 'this is a sentence in all small. convert this except IHG and RC.';
    const expected = 'This Is A Sentence In All Small. Convert This Except IHG And RC.';
    expect(pipe.transform(actual, allCaps)).toEqual(expected);
  });
}); 