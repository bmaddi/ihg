import { Pipe, PipeTransform } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

@Pipe({
  name: 'selectiveTitleCase'
})
export class SelectiveTitleCasePipe implements PipeTransform {

  constructor(private titlecasePipe: TitleCasePipe) { }

  transform(value: string, allCapsWords?: string[]): string {
    const str = this.titlecasePipe.transform(value);
    return allCapsWords ? this.handleAllCapsWords(str, allCapsWords) : str;
  }

  handleAllCapsWords(str: string, allCapsWords: string[]): string{
    allCapsWords.forEach(word => {
      const search = new RegExp(word, 'ig');
      str = str.replace(search, word.toLocaleUpperCase());
    });
    return str;
  }
}