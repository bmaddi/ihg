import { SentenceCasePipe } from './sentence-case.pipe';

describe('SentenceCasePipe', () => {
  let pipe: SentenceCasePipe;

  beforeEach(() => {
    pipe = new SentenceCasePipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should convert a sentence in all uppercase to sentence case', () => {
    const actual = 'THIS IS A SENTENCE IN ALL CAPS. CONVERT THIS.';
    const expected = 'This is a sentence in all caps. Convert this.';
    expect(pipe.transform(actual)).toEqual(expected);
  });

  it('should convert a sentence in all lower case to sentence case', () => {
    const actual = 'this is a sentence in all small! convert this.';
    const expected = 'This is a sentence in all small! Convert this.';
    expect(pipe.transform(actual)).toEqual(expected);
  });

  it('should convert an input to sentence case except leaving words which should be in all caps', () => {
    const actual = 'THIS IS A SENTENCE IN ALL CAPS? CONVERT this except IHG and RC.';
    const expected = 'This is a sentence in all caps? Convert this except IHG and RC.';
    const allCaps = ['IHG', 'RC'];
    expect(pipe.transform(actual, allCaps)).toEqual(expected);
  });
});