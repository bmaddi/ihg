import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sentenceCase'
})
export class SentenceCasePipe implements PipeTransform {

  transform(value: string, allCapsWords?: string[]): string {
    const str = this.transformToSentenceCase(value);
    return allCapsWords ? this.handleAllCapsWords(str, allCapsWords) : str;
  }

  transformToSentenceCase(value: string): string {
    return value.toLocaleLowerCase().replace(/(^\s*\w|[\.\!\?]\s*\w)/g, (substr) => {
      return substr.toLocaleUpperCase();
    });
  }

  handleAllCapsWords(str: string, allCapsWords: string[]): string{
    allCapsWords.forEach(word => {
      const search = new RegExp(word, 'ig');
      str = str.replace(search, word.toLocaleUpperCase());
    });
    return str;
  }
}