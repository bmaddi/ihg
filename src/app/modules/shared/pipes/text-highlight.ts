import { PipeTransform, Pipe } from '@angular/core';
import { BrowserModule, SafeHtml } from '@angular/platform-browser';

@Pipe({ name: 'textHighlight' })
export class HighlightPipe implements PipeTransform {

  constructor() {
  }

  transform(text: string, value: string): any|SafeHtml {
    if (!value) {
      return text;
    }
    let pattern = value.replace(/[,'\-]+$/g, '\\$&');
    let regex = new RegExp(pattern, 'gi');
    let match = text.match(regex);

    if (!match) {
      if (pattern != null && pattern.includes(',')) {
        pattern = pattern.split(',').filter((t) => {
          return t.length > 0;
        }).join(', ');
        regex = new RegExp(pattern, 'gi');
        match = text.match(regex);
      } else {
        return text;
      }
    }

    if ( match != null ) {
      const replacedValue = text.replace(regex, '<strong>' + match[0] + '</strong>');
      return replacedValue;
    } else {
      return text;
    }
  }
}
