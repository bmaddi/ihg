import { TestBed } from '@angular/core/testing';

import { AppCommonService } from './app-common.service';

describe('AppCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppCommonService = TestBed.get(AppCommonService);
    expect(service).toBeTruthy();
  });

  it('check reservation number after sets', () => {
    const service: AppCommonService = TestBed.get(AppCommonService);
    service.setReservationNumber('54545454');
    expect(service.getReservationNumber()).toBe('54545454');
  });

  it('check reservation number is null after flushed', () => {
    const service: AppCommonService = TestBed.get(AppCommonService);
    service.flushReservationNumber();
    expect(service.getReservationNumber()).toBe(null);
  });
});
