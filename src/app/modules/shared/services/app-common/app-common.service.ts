import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppCommonService {

  private reservationNumber: string;
  constructor() { }

  setReservationNumber(reservationNumber: string){
    this.reservationNumber = reservationNumber;
  }

  getReservationNumber(){
    return this.reservationNumber;
  }

  flushReservationNumber(){
    this.reservationNumber = null;
  }

}
