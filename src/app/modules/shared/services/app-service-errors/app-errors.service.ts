import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { ReportIssueService, ReportIssueComponent, ReportIssueAutoFillData } from 'ihg-ng-common-pages';

import { ErrorsModel, SERVICE_ERROR_TYPE, EmitErrorModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { AppHttpStatuses } from '@app/modules/shared/enums/app-http-statuses.enum';

@Injectable({
  providedIn: 'root'
})
export class AppErrorsService {

  constructor(private reportIssueService: ReportIssueService) { }

  public getErrorType(error: EmitErrorModel): SERVICE_ERROR_TYPE {
    if (error) {
      if (error.httpError) {
        return this.getHttpErrorType(error.httpError);
      } else if (error.apiErrors && error.apiErrors.length > 0) {
        return this.getApiErrorType(error.apiErrors);
      } else if (error.hasNoData) {
        return SERVICE_ERROR_TYPE.NO_DATA;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  private getHttpErrorType(httpError: HttpErrorResponse): SERVICE_ERROR_TYPE {
    if (httpError.status === AppHttpStatuses.GATEWAY_TIMEOUT) {
      return SERVICE_ERROR_TYPE.TIME_OUT;
    } else {
      return SERVICE_ERROR_TYPE.GENERAL;
    }
  }

  private getApiErrorType(apiErrors: Array<ErrorsModel>): SERVICE_ERROR_TYPE {
    const timeOutErrors: Array<ErrorsModel> =
      apiErrors.filter(error => (error.message && error.message.toLowerCase().indexOf('timeout') !== -1));
    if (timeOutErrors && timeOutErrors.length > 0) {
      return SERVICE_ERROR_TYPE.TIME_OUT;
    } else {
      return SERVICE_ERROR_TYPE.GENERAL;
    }
  }

  public openReportIssueModal(reportIssueAutoFill): void {
    if (reportIssueAutoFill) {
      this.reportIssueService.openAutofilledReportIssueModal(ReportIssueComponent, reportIssueAutoFill);
    } else {
      this.reportIssueService.openReportIssueModal(ReportIssueComponent);
    }
  }

}
