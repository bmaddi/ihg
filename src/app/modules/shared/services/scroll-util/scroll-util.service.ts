import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollUtilService {

  constructor() {
  }

  getOffset(el: HTMLElement): { top: number, left: number } {
    const rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
  }

  scrollWindow(x: number, y: number): void {
    window.scrollTo(x, y);
  }

  scrollWindowWithOptions(top: number = 0, left: number = 0, behavior: 'smooth' | 'auto' = 'smooth'): void {
    window.scrollTo({top, left, behavior});
  }

  public scrollIntoBottomView(elementClassName: string) {
    const element = document.getElementsByClassName(elementClassName)[0];
    element.scrollIntoView(false);
  }
}
