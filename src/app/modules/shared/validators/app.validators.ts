import { AbstractControl, ValidationErrors, ValidatorFn, FormControl } from '@angular/forms';

export class FdkValidators {

  static validateMinWithTrimmedSpaces = (min: number, whiteSpaceValidation: Function): ValidatorFn => {
    return (control: AbstractControl): ValidationErrors | null => {
      if (whiteSpaceValidation(control.value, min)) {
        return { 'emptySpaces': true };
      }
      return null;
    };
  }

  static validateMinMaxDate =
    (validRange: { min: Date, max: Date }, minValidator: Function, maxValidator: Function): ValidatorFn => {
      return (control: AbstractControl): ValidationErrors | null => {
        if (minValidator(control.value, validRange.min)) {
          return { 'beforeMindate': true };
        }
        if (maxValidator(control.value, validRange.max)) {
          return { 'afterMaxDate': true };
        }
        return null;
      };
    }

  static EmptySpaceValidation(control: FormControl) {
    if (control.value) {
      const value = control.value.trim();
      return value !== '' ? null : { 'emptySpaces': true };;
    } else {
      return null;
    }
  }
}

