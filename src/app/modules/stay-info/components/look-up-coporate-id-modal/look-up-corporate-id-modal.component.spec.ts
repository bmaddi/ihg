import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookUpCoprorateIDModalComponent } from './look-up-corporate-id-modal.component';

xdescribe('LookUpCoprorateIDModalComponent', () => {
  let component: LookUpCoprorateIDModalComponent;
  let fixture: ComponentFixture<LookUpCoprorateIDModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookUpCoprorateIDModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookUpCoprorateIDModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
