import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-look-up-corporate-id-modal',
  templateUrl: './look-up-corporate-id-modal.component.html',
  styleUrls: ['./look-up-corporate-id-modal.component.scss']
})
export class LookUpCoprorateIDModalComponent implements OnInit {  

  @Input() modalName: string;
  @Output() passCorporateId: EventEmitter<number> = new EventEmitter();

  constructor(
    private activeModal: NgbActiveModal    
  ) {}

  ngOnInit() {
  }  

  closeModal() {
    this.activeModal.close();
  }

  public handleEmittedCoprporateId(crprtId): void {
    this.passCorporateId.emit(crprtId);
    this.closeModal();
  }

}
