import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { State } from '@progress/kendo-data-query';
import { PageChangeEvent } from '@progress/kendo-angular-grid';

import { CorporateIDLookUpModel } from '@modules/stay-info/models/stay-information.models';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit { 

  @Input() corporateAccounts: CorporateIDLookUpModel;
  @Input() gridState: State;
  @Output() emitAddCorporateIdToForm: EventEmitter<number>  = new EventEmitter();
  @Output() handlePageChangeEmitter: EventEmitter<PageChangeEvent> = new EventEmitter();

  constructor() { }

  public addCorporateIdToForm(crprtId): void {
    this.emitAddCorporateIdToForm.emit(crprtId);
  }

  ngOnInit() {
  }

  public handlePageChange(evt: PageChangeEvent): void {
    this.handlePageChangeEmitter.emit(evt);
  }

}
