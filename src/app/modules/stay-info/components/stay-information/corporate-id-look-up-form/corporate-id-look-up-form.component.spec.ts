import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateIdLookUpFormComponent } from './corporate-id-look-up-form.component';

xdescribe('CorporateIdLookUpFormComponent', () => {
  let component: CorporateIdLookUpFormComponent;
  let fixture: ComponentFixture<CorporateIdLookUpFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateIdLookUpFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateIdLookUpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
