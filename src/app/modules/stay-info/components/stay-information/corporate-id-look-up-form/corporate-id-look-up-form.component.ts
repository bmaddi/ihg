import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { State } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

import { StayInformationService } from '../../../services/stay-information.service';
import { searchByDropDownData } from '../../../constants/stay-information-constants';
import { CorporateIDLookUpModel } from '@modules/stay-info/models/stay-information.models';


@Component({
  selector: 'app-corporate-id-look-up-form',
  templateUrl: './corporate-id-look-up-form.component.html',
  styleUrls: ['./corporate-id-look-up-form.component.scss']
})
export class CorporateIdLookUpFormComponent implements OnInit, OnDestroy {

  public corporateIdLookUpForm: FormGroup;
  public submitted: boolean = false;
  public searchByData: Array<{ text: string, value: string }> = searchByDropDownData;
  public onFocusCombobox: boolean = false;
  private rootSubscription: Subscription = new Subscription();
  public panelCollapsed: boolean = false;
  public showSearchResults: boolean = false;
  public searchcount: number = 0;
  public gridData: GridDataResult;
  public gridState: State = {
    take: 10,
    skip: 0
  };

  @Output() emitAddCrprtIdToForm: EventEmitter<number> = new EventEmitter();
  @Output() emitCancelSearch: EventEmitter<null> = new EventEmitter();
  

  constructor(
    private fb: FormBuilder,
    private stayInfoService: StayInformationService
  ) {
    this.createForm();
    this.enableControl('corporateName');
    this.disableControl('corporateId');
  }

  ngOnInit() {
  }

  private createForm() {
    this.corporateIdLookUpForm = this.fb.group({
      'searchBy': [this.searchByData[0].value],
      'corporateName': [null, Validators.required],
      'corporateId': [null, [Validators.required, Validators.pattern(new RegExp(/^\d{9}$/))]]
    });
    this.onSearchByValueChange();
  }

  public get formRef(): FormGroup {
    return this.corporateIdLookUpForm;
  }

  private onSearchByValueChange(): void {
    this.rootSubscription.add(this.formRef.get('searchBy').valueChanges.subscribe((data: String) => {
      this.onOutOfFocus();
      if (data == 'corporateNumber') {
        this.clearFormControlValue('corporateName');
        this.disableControl('corporateName');
        this.enableControl('corporateId');
      } else {
        this.clearFormControlValue('corporateId');
        this.disableControl('corporateId');
        this.enableControl('corporateName');
      }
    }));
  }

  private enableControl(controlName: string): void {
    this.formRef.get(controlName).enable();
  }

  private disableControl(controlName: string): void {
    this.formRef.get(controlName).disable();
  }

  private clearFormControlValue(controlName: string): void {
    this.formRef.get(controlName).patchValue(null)
  }

  public onFocus(): void {
    this.onFocusCombobox = true;
  }

  public onOutOfFocus(): void {
    this.onFocusCombobox = false;
  }

  public submitForm(): void { 
    if (this.corporateIdLookUpForm.valid) {
      this.showSearchResults = false;      
      this.searchcount++;
      this.panelCollapsed = true;      
      this.getCorporateIdInfo();      
    }
    return;
  }

  private getCorporateIdInfo() {
    this.rootSubscription.add(this.stayInfoService.getCorporateIdInformation(Object.values(this.formRef.value)[1],Object.values(this.formRef.value)[0],this.gridState).subscribe((resp: CorporateIDLookUpModel) => {
      this.setGridView(resp);
      
    }));
  }

  private setGridView(response: CorporateIDLookUpModel): void {
    this.gridData = {
      data: response.items,
      total: response.totalItems
    };
    this.showSearchResults = true;
  }

  public handlePageChange(event: PageChangeEvent): void {
    this.gridState.skip = event.skip;
    this.gridState.take = event.take;
    this.getCorporateIdInfo();
  }

  public handleToggle(isCollapsed: boolean): void {
    if (typeof isCollapsed === 'boolean') {
      this.panelCollapsed = isCollapsed;
    }
  }

  public handleAddCrptId(crprtId: number): void {
    this.emitAddCrprtIdToForm.emit(crprtId);
  }

  public cancelSearch(): void {
    this.emitCancelSearch.emit();
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

}
