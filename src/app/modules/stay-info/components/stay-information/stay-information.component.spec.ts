import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import {ConfirmationModalService} from 'ihg-ng-common-components';
import {TranslateService} from '@ngx-translate/core';
import { Router } from '@angular/router';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommonModule, TitleCasePipe } from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { TransitionGuardService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { CommonTestModule } from '../../../common-test/common-test.module';
import {setTestEnvironment} from '../../../common-test/functions/common-test.function';

import { StayInformationService } from '../../services/stay-information.service';
import { GuestInfoService } from '../../../guest-info/services/guest-information.service';
import { OffersService } from '../../../offers/services/offers.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { MOCK_RESERVATION, MOCK_RESERVATION_NUMBER, MOCK_ROOM_TYPES, ROUTE_PATHS } from '../../mocks/stay-information-mock';
import { Observable, Observer, of } from 'rxjs';
import { RoomTypeInformationModel, StayInformationModel } from '../../models/stay-information.models';
import { dropDownDefaultData, MOCK_STAY_DATA } from '../../constants/stay-information-constants';
import { CheckInComponent } from '../../../check-in/components/check-in.component';
import { GuestListComponent } from '../../../guests/components/guest-list/guest-list.component';
import { StayInformationComponent } from './stay-information.component';

import * as moment from 'moment';
import * as cloneDeep from 'lodash/cloneDeep';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';



describe('StayInformationComponent', () => {
  let component: StayInformationComponent;
  let fixture: ComponentFixture<StayInformationComponent>;
  let StayServ: StayInformationService;
  let guestServ: GuestInfoService;
  let httpTestingController: HttpTestingController;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        CommonTestModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        DatePickerModule,
        DropDownsModule,
        RouterTestingModule.withRoutes(ROUTE_PATHS),
      ],
      declarations: [ StayInformationComponent, CheckInComponent, GuestListComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        StayInformationService,
        GuestInfoService,
        OffersService,
        ConfirmationModalService,
        ReportIssueService
      ]
    })
    .compileComponents().then(() => {
      setTestEnvironment();
      fixture = TestBed.createComponent(StayInformationComponent);
      component = fixture.componentInstance;
      StayServ = TestBed.get(StayInformationService);
      guestServ = TestBed.get(GuestInfoService);
      httpTestingController = TestBed.get(HttpTestingController);
      router = TestBed.get(Router);
      fixture.detectChanges();
    });
  }));

  beforeEach(async(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 200000;
    component.reservationData = cloneDeep(MOCK_RESERVATION);
    component.reservationNumber = cloneDeep(MOCK_RESERVATION_NUMBER);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should Test if Group Code field is readonly if it is not a group reservation',() => {
    component.reservationData.groupCode = "";
    fixture.detectChanges();
    const groupCodeField = fixture.debugElement.query(By.css('[data-slnm-ihg="GroupCode-SID"]'));
    expect(groupCodeField.nativeElement.attributes.readonly).not.toBeNull();
  });

  it('Should Test if Group Code field is not readonly if it is a group reservation',() => {
    component.reservationData.groupCode = "123456";
    fixture.detectChanges();
    const groupCodeField = fixture.debugElement.query(By.css('[data-slnm-ihg="GroupCode-SID"]'));
    expect(groupCodeField.nativeElement.attributes.readonly).not.toBeDefined();
  });

  it('Should Test Form is invalid when adults field is empty',() => {
    component.stayInformationForm.get('adults').patchValue('');
    fixture.detectChanges();
    expect(component.stayInformationForm.valid).toBeFalsy();
  });

  it('should check if proper value is emitted on ihgRcNumber population', () => {

    spyOn(guestServ.updatedIHGNo, 'next');

    component.stayInformationForm.get('ihgRcNumber').patchValue('123456');
    component.updateIHGClubNo();

    fixture.detectChanges();

    expect(guestServ.updatedIHGNo.next).toHaveBeenCalledWith('123456');
 });

 it('should check rewards field invalid validation', () => {
    guestServ.isInValidIHGNo.next(true);
    const errorVal = component.stayInformationForm.get('ihgRcNumber').errors;
    expect(errorVal).toEqual({ 'isValid': false })
 });

 it('should check populateForm method functionality', () => {
    component.populateForm();
    fixture.detectChanges();
    expect(component.getControl('ihgRcNumber').value).toEqual(component.reservationData['loyaltyMembershipId']);
 });

 it('Should test handleStayFormData functionality', () => {
    //@ts-ignore
    const mockRateCat = [...dropDownDefaultData, ...component.generateDropDownData(component.reservationData['rateCategories'])];
    //@ts-ignore
    component.handleStayFormData();
    fixture.detectChanges();
    expect(component.rateCatData).toEqual(mockRateCat);
 });

 it('Should test rateCategory field is not disabled', () => {
    //@ts-ignore
    component.handleStayFormData();
    fixture.detectChanges();
    const checkIfFieldIsEnabled = component.stayInformationForm.controls['rateCategory'].disabled;
    expect(checkIfFieldIsEnabled).toBeFalsy();
 });

 it('Should test onDepartureDateChange', () => {
    //@ts-ignore
    component.stayInformationForm.get('departure').patchValue(component.getZeroHourDate("2019-10-30").toDate());
    component.onDepartureDateChange();
    fixture.detectChanges();
    expect(component.reservationData.numberOfNights).toEqual(12);
 });

 it('Should test getRoomTypeDropDownInformation when reservationNumber is set', async(() => {
    spyOn(StayServ,'getRoomTypeDropDownInformation').and.returnValue(of(MOCK_ROOM_TYPES));
    spyOn(StayServ,'getStayInformation').and.returnValue(of({"reservation": MOCK_RESERVATION}));
    //@ts-ignore
    component.getStayInformation();
    fixture.detectChanges();
    expect(component.roomTypeDropDownRespData).toEqual(MOCK_ROOM_TYPES.roomTypes);
    expect(StayServ.getStayInformation).toHaveBeenCalled();
 }));

 it('Should test getRoomTypeDropDownInformation when reservationNumber is not set', async(() => {
  spyOn(StayServ,'getStayInformation').and.returnValue(of({"reservation": MOCK_RESERVATION}));
  spyOn(StayServ,'getRoomTypeDropDownInformation').and.returnValue(of(MOCK_ROOM_TYPES));
  component.reservationNumber = null;
  //@ts-ignore
  component.getStayInformation();
  fixture.detectChanges();
  expect(StayServ.getStayInformation).not.toHaveBeenCalled();
  expect(StayServ.getRoomTypeDropDownInformation).toHaveBeenCalled();
}));

 it('Should test updateValidDateRange with arrv date as hotelCurrentDate + 1 and max date as 99', () => {
    //@ts-ignore
    const arrvDate = component.getZeroHourDate(MOCK_RESERVATION.arrivalDate).toDate();
    const validRange = {
      min: moment(MOCK_RESERVATION.hotelCurrentDate).add(1, 'days').toDate(),
      max: moment(MOCK_RESERVATION.arrivalDate).add(component.maxAllowedNights, 'days').toDate()
    };
    //@ts-ignore
    component.updateValidDateRange(arrvDate);
    //@ts-ignore
    expect(component.validDateRange).toEqual(validRange);
 });

 it('Should test resetGuestInfoForm subscription value', async(() => {
    //@ts-ignore
    guestServ.resetGuestInfoForm.next(true);
    fixture.detectChanges();
    guestServ.resetGuestInfoForm.subscribe((val) => {
      expect(val).toBeTruthy();
    })
 }));

 it('Should test cancelChanges method when reservationNumber is set', () => {
    spyOn(guestServ.guestInfoChangeSubject, 'next');
    component.cancelChanges();
    expect(guestServ.guestInfoChangeSubject.next).toHaveBeenCalledWith(false);
 });

  it('Should check if state is ManageStay and emit stayInformation event on Cancel Button click', () => {
    component.reservationData = MOCK_STAY_DATA;
    component.state = AppStateNames.manageStay;

    spyOn(TestBed.get(GuestInfoService), 'triggerCancelEditMode');
    component.cancelChanges();
    fixture.detectChanges();

    expect(TestBed.get(GuestInfoService).triggerCancelEditMode).toHaveBeenCalled();
  });

  xit('Should check apply button is clicked function is called', () => {
    const element = fixture.debugElement.query(By.css('[data-slnm-ihg="Apply-SID"]'));
    expect(element).toBeDefined();
    const applySpy = spyOn(component, 'onSubmit');

    const submitEl = fixture.debugElement.query(By.css('.btn.btn-primary'));
    expect(submitEl.nativeElement.disabled).toBeTruthy();
    expect(component.stayInformationForm.valid).toBeFalsy();

    component.stayInformationForm.get('adults').patchValue('2');
    fixture.detectChanges();

    expect(component.stayInformationForm.valid).toBeTruthy();
    element.triggerEventHandler('click', null);

    fixture.detectChanges();
    expect(applySpy).toHaveBeenCalled();
  });

});
