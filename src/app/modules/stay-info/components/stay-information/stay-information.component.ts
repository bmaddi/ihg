import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, forkJoin, Subject, Observable } from 'rxjs';
import * as moment from 'moment';
import { cloneDeep } from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { GoogleAnalyticsService, TransitionGuardService, GuardRegistrationParams } from 'ihg-ng-common-core';

import { StayInformationModel, StayInformationReservationModel, StayInfoSearchModel, RoomTypeInformationModel, } from '../../models/stay-information.models';
import { StayInformationService } from '../../services/stay-information.service';
import { AppConstants } from '@app/constants/app-constants';
import { FdkValidators } from '@app-shared/validators/app.validators';
import { UtilityService } from '@app/services/utility/utility.service';
import { smokingPrefDropDownData, dropDownDefaultData, gaConstants } from '../../constants/stay-information-constants';
import { LookUpCoprorateIDModalComponent } from '../look-up-coporate-id-modal/look-up-corporate-id-modal.component';
import { LookUpRewardsMemberModalComponent } from '@modules/rewards-number-look-up/components/look-up-rewards-member-modal/look-up-rewards-member-modal.component';
import { GuestInfoService } from '@app/modules/guest-info/services/guest-information.service';
import { OffersService } from '@app/modules/offers/services/offers.service';
import { IHGMemberDetail } from '@app/modules/guest-info/models/guest-info.model';
import { MANAGE_STAY_CONST } from '@app/modules/manage-stay/manage-stay.constants';
import { AppStateNames } from '@app-shared/enums/app-state-names.enum';
import { ChangeStayService } from '@modules/offers/services/change-stay/change-stay.service';

@Component({
  selector: 'app-stay-information',
  templateUrl: './stay-information.component.html',
  styleUrls: ['./stay-information.component.scss']
})
export class StayInformationComponent implements OnInit, OnDestroy {
  public readonly maxAllowedNights = 99;
  public stayInformationForm: FormGroup;
  public rateCatData: Array<{ text: string, value: string }> = [];
  public roomTypeCodeData: Array<{ text: string, value: string }> = [];
  public smokingPrefData: Array<{ text: string, value: string | null }> = smokingPrefDropDownData;
  public arrivalDateDisplay: string;
  public roomTypeDropDownRespData: string[];
  private rootSubscription = new Subscription();
  private validDateRange = {
    min: null,
    max: null
  };

  @Input() reservationNumber: string;
  @Input() reservationData: StayInformationModel;
  @Input() panelCollapsed = false;
  @Input() state: string;

  @Output() search = new EventEmitter<StayInfoSearchModel>();
  @Output() originalReservationData = new EventEmitter<StayInformationModel>();

  constructor(
    private fb: FormBuilder,
    private stayInfoService: StayInformationService,
    private guestInfoService: GuestInfoService,
    private util: UtilityService,
    private modalService: NgbModal,
    private ga: GoogleAnalyticsService,
    private router: Router,
    private offersService: OffersService,
    private transitionGuardService: TransitionGuardService,
    private changeStayService: ChangeStayService
  ) { }

  ngOnInit() {
    this.createForm();
    this.getStayInformation();
    this.subscribeRewardsClubError();
    this.subscribeToOffersVisibility();
    this.subscribeToIhgRcNumberValidation();
    this.subscribeNecessaryValues();
    this.resetFormValues();
    this.disableLoyaltyEnrollmentForWalkin();
  }


  subscribeNecessaryValues() {
    this.registerInTransitionGuard();
    this.subscribeValueChanges();
  }

  private disableLoyaltyEnrollmentForWalkin() {
    if (!this.reservationNumber) {
      this.stayInformationForm.controls['ihgRcNumber'].disable();
    }
  }

  subscribeValueChanges() {
    this.rootSubscription.add(this.stayInformationForm.valueChanges.subscribe(val => {
      if (val && (this.stayInformationForm.dirty || this.stayInformationForm.touched)) {
        this.guestInfoService.guestInfoChangeSubject.next(true);
        this.registerInTransitionGuard();
      }
    }));
  }

  resetFormValues() {
    this.rootSubscription.add(this.guestInfoService.resetGuestInfoForm.subscribe(val => {
      if (val === true) {
        this.createForm();
        this.populateForm();
        this.emitDepartureRateChange();
      }
    }));
  }
  private subscribeToIhgRcNumberValidation(): void {
    this.rootSubscription.add(this.guestInfoService.ihgRcNumberValidation.subscribe((guestInfo: IHGMemberDetail) => {
      if (guestInfo && guestInfo.memberDetails) {
        this.reservationData.guestInfo.firstName = guestInfo.memberDetails.firstName;
        this.reservationData.guestInfo.middleName = guestInfo.memberDetails.middleName;
        this.reservationData.guestInfo.lastName = guestInfo.memberDetails.lastName;
        this.getControl('ihgRcNumber').patchValue(guestInfo.memberDetails.ihgRcNumber);
      }
    }));
  }

  private subscribeToOffersVisibility() {
    this.rootSubscription.add(this.offersService.isShowOffers.subscribe((showOffers) => {
      this.panelCollapsed = showOffers;
    }));
  }

  private getOnlyStayInformation() {
    return this.stayInfoService.getStayInformation(this.reservationNumber);
  }

  private getOnlyRoomTypes() {
    return this.stayInfoService.getRoomTypeDropDownInformation();
  }

  private getStayInformation(): void {
    let observableArray = [];
    if (this.reservationNumber) {
      const stayInfoObservable = this.getOnlyStayInformation();
      const roomTypeObs = this.getOnlyRoomTypes();
      observableArray = [roomTypeObs, stayInfoObservable];
    } else {
      const roomTypeObs = this.getOnlyRoomTypes();
      observableArray = [roomTypeObs];
    }
    this.rootSubscription.add(forkJoin(observableArray).subscribe((response: [RoomTypeInformationModel, StayInformationReservationModel] | [RoomTypeInformationModel]) => {
      this.reservationData = (response[1]) ? response[1]['reservation'] : this.reservationData;
      this.roomTypeDropDownRespData = response[0]['roomTypes'];
      this.handleStayFormData();
    }));
  }

  private handleStayFormData(): void {
    this.originalReservationData.emit(cloneDeep(this.reservationData));
    this.rateCatData = [...dropDownDefaultData, ...this.generateDropDownData(this.reservationData['rateCategories'] || [])];
    this.roomTypeCodeData = [...dropDownDefaultData, ...this.generateDropDownData(this.roomTypeDropDownRespData || [])];
    this.populateForm();
    this.disableRateCategory();
    this.emitDepartureRateChange();
  }

  private disableRateCategory() {
    if (this.rateCatData.length < 2) {
      this.stayInformationForm.controls['rateCategory'].disable();
    }
  }

  private generateDropDownData(dataToPopulateFrom: string[]): { text: string, value: string }[] {
    return dataToPopulateFrom.map((val) => {
      return {
        text: val,
        value: val
      };
    });
  }

  private getIndexOfSelectedValue(dataArray: { text: string, value: string }[], selectedValue: string): number {
    return dataArray.findIndex(data => data.value === selectedValue);
  }

  private createForm(): void {
    this.stayInformationForm = this.fb.group({
      departure: [moment().toDate(), Validators.required],
      ihgRcNumber: [],
      adults: [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      children: [null , Validators.pattern('^[0-9]*$')],
      rateCategory: [],
      roomType: [],
      corporateID: [],
      groupCode: [],
      smokingPreference: [this.smokingPrefData[0].value],
      accessible: []
    });
    this.onDepartureDateChange();
  }

  public getControl(controlName: string): FormControl {
    return this.stayInformationForm.get(controlName) as FormControl;
  }

  private updateDepartureDateValidator(): void {
    const depDateField = this.getControl('departure');
    depDateField.setValidators([
      Validators.required,
      FdkValidators.validateMinMaxDate(this.validDateRange, this.util.isBeforeMinDate, this.util.isAfterMaxDate)
    ]);
    depDateField.updateValueAndValidity();
  }

  private updateValidDateRange(arrDate: Date): void {
    this.validDateRange = {
      min: moment(this.reservationData.hotelCurrentDate).add(1, 'days').toDate(),
      max: moment(arrDate).add(this.maxAllowedNights, 'days').toDate()
    };
  }

  public populateForm(): void {
    if (this.reservationData) {
      const arrivaldate = this.getZeroHourDate(this.reservationData.arrivalDate).toDate();
      const departureDate = this.getZeroHourDate(this.reservationData.checkOutDate).toDate();
      this.arrivalDateDisplay = this.getZeroHourDate(this.reservationData.arrivalDate).format(AppConstants.VW_DT_FORMAT).toUpperCase();
      this.updateValidDateRange(arrivaldate);
      this.updateDepartureDateValidator();
      this.getControl('departure').patchValue(departureDate);
      this.getControl('ihgRcNumber').patchValue(this.reservationData['loyaltyMembershipId'] || null);
      if (this.reservationData.loyaltyMembershipId && this.reservationData.loyaltyMembershipId !== '') {
        this.getControl('ihgRcNumber').disable();
      }
      this.getControl('adults').patchValue((this.reservationData['adultQuantity']));
      this.getControl('children').patchValue(this.reservationData['childQuantity']);
      const selectedRateCatIndex = this.getIndexOfSelectedValue(this.rateCatData, this.reservationData['rateCategoryCode']);
      this.getControl('rateCategory').patchValue((selectedRateCatIndex !== -1) ? this.rateCatData[selectedRateCatIndex].value : null);
      const selectedRoomTypeIndex = this.getIndexOfSelectedValue(this.roomTypeCodeData, this.reservationData['roomTypeCode']);
      this.getControl('roomType').patchValue((selectedRoomTypeIndex != -1) ? this.roomTypeCodeData[selectedRoomTypeIndex].value : null);
      this.getControl('corporateID').patchValue((this.reservationData['corporateId']) || null);
      this.getControl('groupCode').patchValue((this.reservationData['groupCode']) || null);
        this.getControl('groupCode').disable();
      this.getControl('accessible').patchValue((this.reservationData['accessible']) || false);
    }
  }

  public onDepartureDateChange(): void {
    this.rootSubscription.add(this.stayInformationForm.get('departure').valueChanges.subscribe((date: Date) => {
      const departure = this.getZeroHourDate(date);
      const arrival = this.getZeroHourDate(this.reservationData.arrivalDate);
      this.reservationData.numberOfNights = isNaN(departure.diff(arrival, 'days')) ? null : departure.diff(arrival, 'days');
    }));
  }

  private getZeroHourDate(date: string | Date): moment.Moment {
    return moment(date).startOf('day');
  }

  public handleDepartureChange() {
    this.changeStayService.handleDepartureChange(this.getSearchCriteria(), this.reservationData.checkOutDate, this.stayInformationForm);
  }

  public emitDepartureRateChange() {
    this.changeStayService.emitDepartureRateChange(this.getSearchCriteria());
  }

  public onSubmit(): void {
    this.search.emit(this.getSearchCriteria());
    this.ga.trackEvent(gaConstants.CHCKIN_MNG_STAY, gaConstants.APPLY, gaConstants.SELECTED);
  }

  private getSearchCriteria(): StayInfoSearchModel {
    const formValue = this.stayInformationForm.getRawValue();
    return {
      accessible: formValue.accessible,
      adultsCount: formValue.adults,
      arrivalDate: this.getZeroHourDate(this.reservationData.arrivalDate).format(AppConstants.DB_DT_FORMAT),
      childCount: (formValue.children === "") ? 0 : formValue.children,
      corporateId: formValue.corporateID,
      departureDate: this.getZeroHourDate(formValue.departure).format(AppConstants.DB_DT_FORMAT),
      groupCode: formValue.groupCode,
      guestInfo: this.reservationData.guestInfo,
      ihgRcNumber: formValue.ihgRcNumber || this.reservationData.loyaltyMembershipId || '',
      numberOfNights: this.reservationData.numberOfNights,
      numberOfRooms: this.reservationData.numberOfRooms,
      preference: formValue.smokingPreference,
      rateCategory: formValue.rateCategory,
      roomType: formValue.roomType,
      rateInfo: this.reservationData.rateInfo,
      hotelCode: this.reservationData.hotelCode,
      confirmationNumber: this.reservationData.confirmationNumber
    };
  }

  public resetForm(): void {
  }

  public handleToggle(isCollapsed: boolean): void {
    if (typeof isCollapsed === 'boolean') {
      this.panelCollapsed = isCollapsed;
    }
  }

  public onDatepickerOpen(): void {
    this.util.handleDisabledDates(this.validDateRange);
  }

  public openModal(): void {
    this.ga.trackEvent(MANAGE_STAY_CONST.GA.CHCKIN_MNG_STAY, MANAGE_STAY_CONST.GA.CORPORATE_ID_LOOKUP , MANAGE_STAY_CONST.GA.SLCTD);
    const modalRef = this.modalService.open(LookUpCoprorateIDModalComponent, { windowClass: 'modal-lookup' });
    this.rootSubscription.add(modalRef.componentInstance.passCorporateId.subscribe((crprtId: number) => {
      this.getControl('corporateID').patchValue(crprtId);
      this.stayInformationForm.updateValueAndValidity();
      this.stayInformationForm.markAsTouched();
    }));
  }

  public openRewardsMemberModal(): void {
    this.ga.trackEvent(MANAGE_STAY_CONST.GA.CHCKIN_MNG_STAY, MANAGE_STAY_CONST.GA.REWARD_MEMBER_LOOKUP , MANAGE_STAY_CONST.GA.SLCTD);
    const modalRef = this.modalService.open(LookUpRewardsMemberModalComponent, { windowClass: 'modal-lookup' });
    this.rootSubscription.add(modalRef.componentInstance.ihgRcNumber.subscribe((ihgRcNumber: string) => {
      this.stayInformationForm.get('ihgRcNumber').patchValue(ihgRcNumber);
      this.getControl('ihgRcNumber').markAsDirty();
      this.updateIHGClubNo();
    }));
  }

  public cancelChanges() {
    if (this.state === AppStateNames.manageStay) {
        this.guestInfoService.triggerCancelEditMode();
    } else {
      if (this.reservationNumber) {
        this.guestInfoService.guestInfoChangeSubject.next(false);
        this.router.navigate(['check-in']);
      } else {
        this.router.navigate(['guest-list-details']);
      }
    }
  }

  public updateIHGClubNo() {
    const rewardsClubNumber = this.stayInformationForm.get('ihgRcNumber') as FormControl;
    this.guestInfoService.updatedIHGNo.next(rewardsClubNumber.value);
  }

  public subscribeRewardsClubError() {
    this.rootSubscription.add(this.guestInfoService.isInValidIHGNo
      .subscribe(response => {
        const rewardsClubNumber = this.stayInformationForm.get('ihgRcNumber') as FormControl;
        if (response != null) {
          rewardsClubNumber.setErrors({ 'isValid': false });
        } else {
          rewardsClubNumber.setErrors(null);
        }
      }));
  }

  private registerInTransitionGuard() {
    this.transitionGuardService.register(<GuardRegistrationParams>{
      dataChangesObserver: this.guestInfoService.guestInfoChangeSubject.asObservable(),
      useCancelSaveDiscardModal: false,
      userConfirmModalConfig: this.guestInfoService.getConfirmModalConfig(),
      noToastMessage: true,
      onSave: null,
      onDiscard: () => {
        return this.handleConfirmActions(true);
      },
      onCancel: () => {
        return this.handleConfirmActions(false);
      }
    });
  }

  private handleConfirmActions(shouldDiscard: boolean): Observable<boolean> {
    const subject = new Subject<boolean>();
    if (shouldDiscard) {
      this.populateForm();
      this.emitDepartureRateChange();
    }
    setTimeout(() => {
      subject.next(shouldDiscard);
      this.setDataChange(!shouldDiscard);
    });
    return subject.asObservable();
  }

  private setDataChange(dataChanged: boolean): void {
    this.guestInfoService.guestInfoChangeSubject.next(dataChanged);
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
    this.changeStayService.clearSessionData();
  }
}
