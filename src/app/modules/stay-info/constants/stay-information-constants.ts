import { StayInformationModel } from '@modules/stay-info/models/stay-information.models';
import { RateInfoModel } from '@modules/offers/models/offers.models';

export const smokingPrefDropDownData= [{
    text: 'No Preference',
    value: null
  },{
    text: 'Non-Smoking',
    value: 'NSMK'
  },{
    text: 'Smoking',
    value: 'SMK'
}];

export const dropDownDefaultData = [{
  text: 'Select',
  value: null
}];

export const searchByDropDownData = [{
    text: 'Corporate Name',
    value: 'corporateName'
  },{
    text: 'Corporate ID',
    value: 'corporateNumber'
}];


export const gaConstants = {
  CHCKIN_MNG_STAY: 'Check In - Manage Stay',
  APPLY: 'Apply',
  SELECTED: 'Selected',
  BACK_CHECK_IN:'Back to Check In'
};

export const MOCK_STAY_DATA: StayInformationModel = {
  hotelCode: 'ATLCP',
  confirmationNumber: '',
  adultQuantity: 0,
  childQuantity: 0,
  corporateId: '',
  groupCode: '',
  checkOutDate: '',
  rateCategoryCode: '',
  roomTypeCode: '',
  arrivalDate: '',
  numberOfRooms: 1,
  numberOfNights: 2,
  loyaltyMembershipId: '',
  guestInfo: {
    firstName: 'debasmita',
    lastName: '',
    middleName: ''
  }
};
