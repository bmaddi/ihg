import { StayInformationModel, RoomTypeInformationModel } from '../models/stay-information.models';
import { GuestListComponent } from '../../guests/components/guest-list/guest-list.component';
import { TransitionGuardService } from 'ihg-ng-common-core';
import { CheckInComponent } from '../../check-in/components/check-in.component';

export const MOCK_ROOM_TYPES: RoomTypeInformationModel = {
  "roomTypes":[  
      "CSTN",
      "DUMMY"
  ]
}

export const MOCK_RESERVATION_NUMBER = '47593592';

export const MOCK_RESERVATION: StayInformationModel = {
  "hotelCode": "GRVAB",
  "loyaltyMembershipId": "123456",
  "confirmationNumber": "47593592",
  "corporateId": "",
  "checkOutDate": "2019-10-19",
  "arrivalDate": "2019-10-18",
  "numberOfNights": 1,
  "numberOfRooms": 1,
  "adultQuantity": 1,
  "childQuantity": 0,
  "rateCategoryCode": "IGCOR",
  "roomTypeCode": "KDXG",
  "groupCode": "",
  "rateInfo": {
    "averageRate": "100.00",
    "currency": "GBP",
    "totalTaxAmount": "16.67",
    "totalAmountAfterTax": "100.00",
    "totalExtraPersonAmount": "0.00",
    "totalServiceChargeAmount": "0.00",
    "dailyRates": [
      {
        "date": "2019-10-18",
        "amountBeforeTax": "83.33",
        "amountAfterTax": "100.00",
        "baseAmount": "100.00"
      }
    ],
    "rateRules": [
      {
        "roomDesc": "",
        "rateRuleDesc": "",
        "noShowPolicyDesc": "",
        "extraPersonCharge": "0.00",
        "earlyDeparture": "",
        "guaranteePolicy": "",
        "checkinTime": "",
        "checkoutTime": "",
        "tax": "16.67",
        "serviceCharge": "0.00"
      }
    ],
    "serviceChargeIncluded": false,
    "taxesChargeIncluded": true,
    "extraPersonChargeIncluded": true
  },
  "guestInfo": {
    "firstName": "debasmita",
    "lastName": "pradhan",
    "middleName": "",
  },
  "rateCategories": [
    "ADAVP",
    "AG000",
  ],
  "hotelCurrentDate": "2020-02-28"
}

export const ROUTE_PATHS = [{
  path: 'check-in',
  component: CheckInComponent,
  canDeactivate: [
    TransitionGuardService
  ],
  data: {
    crumb: ['dashboard', 'guest', 'guest-list-details'],
    pageTitle: 'LBL_CHK_IN',
    stateName: 'check-in',
    slnmId: 'CheckIn-SID',
    maxWidth: true,
    noSubtitle: true,
    showHelp: true
  }
},{
  path: 'guest-list-details',
  component: GuestListComponent,
  canDeactivate: [
    TransitionGuardService
  ],
  data: {
    crumb: ['dashboard', 'guest'],
    pageTitle: 'LBL_GST_LST',
    stateName: 'guest-list-details',
    slnmId: 'GuestList-SID',
    maxWidth: true,
    noSubtitle: true,
    showHelp: true
  }
}]