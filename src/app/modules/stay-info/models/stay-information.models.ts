import { ErrorsModel } from '@app-shared/models/app-service-errors/app-service-errors-models';
import { RateInfoModel } from '../../offers/models/offers.models';

export interface StayInformationModel {
  hotelCode?: string;
  confirmationNumber?: string;
  adultQuantity?: number;
  childQuantity?: number;
  corporateId?: string;
  groupCode?: string;
  checkOutDate?: string;
  rateCategoryCode?: string;
  rateCategories?: Array<string>;
  roomTypeCode?: string;
  arrivalDate: string;
  numberOfRooms: number;
  numberOfNights?: number;
  loyaltyMembershipId?: string;
  rateInfo?: RateInfoModel;
  guestInfo?: {
    firstName: string;
    lastName: string;
    middleName?: string;
  };
  hotelCurrentDate?: string;
}

export interface RoomTypeInformationModel {
  roomTypes: string[];
}

export interface StayInformationReservationModel {
  reservation: StayInformationModel;
}

export interface StayInfoSearchModel {
  accessible?: boolean;
  adultsCount: string | number;
  arrivalDate: string;
  childCount: string | number;
  corporateId: string;
  departureDate: string;
  groupCode: string;
  taxesChargeIncluded?: boolean;
  serviceChargeIncluded?: boolean;
  productIncludedInOffer?: boolean;
  extraPersonChargeIncluded?: boolean;
  guestInfo: {
    firstName: string;
    lastName: string;
    middleName?: string;
  };
  ihgRcNumber: string | number;
  numberOfNights: string | number;
  numberOfRooms: string | number;
  preference?: string;
  rateCategory: string;
  roomType: string;
  rateInfo: RateInfoModel;
  hotelCode: string;
  confirmationNumber: string;
  searchType?: string;
  doNotMoveRoom?: boolean;
  roomNumber?: string;
  checkOutDate?: string;
  roomTypeCode?: string;
  allowPropertyForceSell?: boolean;
}

export interface CorporateAccountsModel {
  corporateId: string;
  corporateName: string;
  accountType: string;
  corporateSalesProgram: string;
  address: string;
  aliasName: string;
}

export interface CorporateIDLookUpModel {
  pageNumber: number;
  pageSize: number;
  startItem: number;
  endItem: number;
  totalItems: number;
  totalPages: number;
  items: CorporateAccountsModel[];
  errors?: ErrorsModel[];
}
