import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { timeout } from 'rxjs/operators';

import { UserService } from 'ihg-ng-common-core';
import { State } from '@progress/kendo-data-query';

import { environment } from '@env/environment';
import { StayInformationReservationModel, RoomTypeInformationModel, StayInformationModel, CorporateAccountsModel, CorporateIDLookUpModel } from '@modules/stay-info/models/stay-information.models';

@Injectable({
  providedIn: 'root'
})
export class StayInformationService {

  private readonly API_URL = environment.fdkAPI;


  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  private getLocationId(): string {
    return this.userService.getCurrentLocationId();
  }

  public getStayInformation(reservationNumber: string): Observable<StayInformationReservationModel> {
    const apiURl = `${this.API_URL}reservation/fetchDetails/${this.getLocationId()}?reservationNumber=${reservationNumber}`;
    return this.http.get(apiURl).pipe(map((response: StayInformationReservationModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  private getPaginationParams(state: State): string {
    const page = (state.skip / state.take) + 1;
    return `&page=${page}&pageSize=${state.take}`;
  }

  public getRoomTypeDropDownInformation(): Observable<RoomTypeInformationModel> {
    const apiURl = `${this.API_URL}rooms/fetchPropertyRoomTypes/${this.getLocationId()}`;
    return this.http.get(apiURl).pipe(map((response: RoomTypeInformationModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public getCorporateIdInformation(corporateInput, searchType, state): Observable<CorporateIDLookUpModel> {
    const location = this.userService.getCurrentLocationId();
    const apiURl = `${this.API_URL}lookup/fetchCorporateAccount/${this.getLocationId()}?corporateInput=${corporateInput}&searchType=${searchType}${this.getPaginationParams(state)}`;
    return this.http.get(apiURl).pipe(map((response: CorporateIDLookUpModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

}
