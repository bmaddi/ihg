import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppSharedModule } from '@modules/shared/app-shared.module';
import { IhgNgCommonComponentsModule } from 'ihg-ng-common-components';
import { IhgNgCommonKendoModule } from 'ihg-ng-common-kendo';
import { RewardsNumberLookUpModule } from '@modules/rewards-number-look-up/rewards-number-look-up.module';

import { StayInformationComponent } from './components/stay-information/stay-information.component';
import { LookUpCoprorateIDModalComponent } from './components/look-up-coporate-id-modal/look-up-corporate-id-modal.component';;
import { CorporateIdLookUpFormComponent } from './components/stay-information/corporate-id-look-up-form/corporate-id-look-up-form.component';
import { SearchResultsComponent } from './components/stay-information/corporate-id-look-up-form/components/search-results/search-results.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IhgNgCommonComponentsModule,
    IhgNgCommonKendoModule,
    AppSharedModule, 
    ReactiveFormsModule,
    TranslateModule,
    DatePickerModule,
    DropDownsModule,
    GridModule,
    NgbModule,
    RewardsNumberLookUpModule
  ],
  declarations: [
    StayInformationComponent,
    LookUpCoprorateIDModalComponent,
    CorporateIdLookUpFormComponent,
    SearchResultsComponent,
  ],
  exports: [
    StayInformationComponent
  ],
  entryComponents: [
    LookUpCoprorateIDModalComponent
  ]
})
export class StayInfoModule { }
