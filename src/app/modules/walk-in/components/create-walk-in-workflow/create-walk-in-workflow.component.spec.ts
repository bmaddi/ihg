import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationModalService } from 'ihg-ng-common-components';

import { CreateWalkInWorkflowComponent } from './create-walk-in-workflow.component';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CreateWalkInWorkflowComponent', () => {
  let component: CreateWalkInWorkflowComponent;
  let fixture: ComponentFixture<CreateWalkInWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonTestModule],
      declarations: [CreateWalkInWorkflowComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [ArrivalsService, ConfirmationModalService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWalkInWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
