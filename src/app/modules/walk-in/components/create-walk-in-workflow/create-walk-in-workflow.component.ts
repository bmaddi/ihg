import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

import { WalkInWorkflow } from '../../enums/walk-in.enums';
import { ArrivalsService } from '@modules/prepare/services/arrivals-service/arrivals.service';
import { StayInformationModel } from '@app/modules/manage-stay/models/stay-information.models';
import { WorkflowTab } from '@modules/walk-in/interfaces/workflow-tab.interface';
import { GuestWalkInModel } from '@modules/offers/models/offers.models';

@Component({
  selector: 'app-create-walk-in-workflow',
  templateUrl: './create-walk-in-workflow.component.html',
  styleUrls: ['./create-walk-in-workflow.component.scss']
})
export class CreateWalkInWorkflowComponent implements OnInit {
  @Input() selectedTab: WalkInWorkflow;
  @Input() workflowTabs: WorkflowTab[];
  @Input() walkInModelData: GuestWalkInModel;
  @Output() tabSelect = new EventEmitter<string>();
  @Output() originalReservationData = new EventEmitter<StayInformationModel>();

  walkInWorkflowTabs = WalkInWorkflow;
  offerSelected = false;

  constructor(private prepareArrivalsService: ArrivalsService) {
  }

  ngOnInit() {
  }

  public handleTabChange(event: NgbTabChangeEvent): void {
    event.preventDefault();
    this.checkUnsavedTasks(() => {
      this.changeTab(event);
    });
  }

  private checkUnsavedTasks(callbackFn: Function): void {
    this.prepareArrivalsService.handleUnsavedTasks(callbackFn, null);
  }

  private changeTab(event: NgbTabChangeEvent): void {
    this.selectedTab = event.nextId as WalkInWorkflow;
    this.tabSelect.emit(this.selectedTab);
  }

  public handleOriginalReservationData(reservation) {
    this.originalReservationData.emit(reservation);
  }

  public disableGuestInfoTab(tabId): boolean {
    if (tabId == 'guestInfo') {
      return !this.enableGuestInfoTab();
    }
  }

  private enableGuestInfoTab(): boolean {
    return this.walkInModelData ? !this.offerSelected : this.offerSelected;
  }

}