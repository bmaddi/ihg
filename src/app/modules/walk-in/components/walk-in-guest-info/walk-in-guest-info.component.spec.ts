import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvironmentService } from 'ihg-ng-common-core';
import { ConfirmationModalService } from 'ihg-ng-common-components';

import { WalkInGuestInfoComponent } from './walk-in-guest-info.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { WalkInService } from '@modules/walk-in/services/walk-in.service';
import { mockEnvs } from '@app/constants/app-test-constants';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';

describe('WalkInGuestInfoComponent', () => {
  let component: WalkInGuestInfoComponent;
  let fixture: ComponentFixture<WalkInGuestInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CommonTestModule],
      declarations: [WalkInGuestInfoComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        WalkInService,
        GuestInfoService,
        EnvironmentService,
        ConfirmationModalService
      ]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(WalkInGuestInfoComponent);
      component = fixture.componentInstance;
    });
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
