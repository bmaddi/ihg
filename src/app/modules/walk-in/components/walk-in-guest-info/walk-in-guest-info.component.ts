import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { GuestCheckInModel, GuestWalkInModel, ReservationDataModel } from '@modules/offers/models/offers.models';
import { WalkInService } from '@modules/walk-in/services/walk-in.service';
import { GuestInfo } from '@modules/guest-info/models/guest-info.model';
import { AppErrorBaseComponent } from '@app-shared/components/app-service-errors/app-error-base-component';
import { CreateReservationData } from '@modules/walk-in/models/walk-in.models';
import { GuestInfoService } from '@modules/guest-info/services/guest-information.service';

@Component({
  selector: 'app-walk-in-guest-info',
  templateUrl: './walk-in-guest-info.component.html',
  styleUrls: ['./walk-in-guest-info.component.scss']
})

export class WalkInGuestInfoComponent extends AppErrorBaseComponent implements OnInit, OnDestroy {
  private walkIn$: Subscription = new Subscription();
  private reservationPayload: CreateReservationData;

  @Input() walkInModelData: GuestWalkInModel;

  constructor(private walkInService: WalkInService, private guestInfoService: GuestInfoService) {
    super();
  }

  ngOnInit(): void {
  }

  onBook(guestInfo: GuestInfo) {
    this.reservationPayload = { ...this.walkInModelData, ...guestInfo };
    this.mapReservationPayload();
    this.bookReservation(this.reservationPayload);
  }

  private mapReservationPayload(): void {
    this.reservationPayload.ihgRcNumber = null;
    this.reservationPayload.groupCode = '';
    delete this.reservationPayload.accessible;
    delete this.reservationPayload.confirmationNumber;
    delete this.reservationPayload.numberOfNights;
    delete this.reservationPayload.ratesInfo;
    delete this.reservationPayload.cardNumber;
    delete this.reservationPayload.nameOnCard;
    delete this.reservationPayload.expirationDate;
  }

  bookReservation(payload: CreateReservationData) {
    this.walkIn$.add(this.walkInService.createReservation(payload).subscribe((response: GuestCheckInModel) => {
      if (!super.checkIfAnyApiErrors(response)) {
        this.setDataChange();
        this.walkInService.openReservationConfirmationModal(this.addGuestDetails(response));
      }
    }, (error) => {
      super.processHttpErrors(error);
    }));
  }

  private setDataChange(): void {
    this.guestInfoService.guestInfoChangeSubject.next(false);
  }

  private addGuestDetails(response: GuestCheckInModel): ReservationDataModel {
    if (response.reservationData) {
      response.reservationData.firstName = this.reservationPayload.firstName;
      response.reservationData.lastName = this.reservationPayload.lastName;
      response.reservationData.checkInDate = this.reservationPayload.checkInDate;
      response.reservationData.checkOutDate = this.reservationPayload.checkOutDate;
    }
    return response.reservationData;
  }

  ngOnDestroy() {
    this.walkIn$.unsubscribe();
  }
}
