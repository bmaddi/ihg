import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SessionStorageService, EnvironmentService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { WalkInStayInfoComponent } from './walk-in-stay-info.component';
import { OffersService } from '@modules/offers/services/offers.service';
import { WalkInService } from '@modules/walk-in/services/walk-in.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { mockEnvs } from '@app/constants/app-test-constants';

describe('WalkInStayInfoComponent', () => {
  let component: WalkInStayInfoComponent;
  let fixture: ComponentFixture<WalkInStayInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CommonTestModule],
      declarations: [WalkInStayInfoComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        WalkInService,
        SessionStorageService,
        OffersService,
        EnvironmentService,
        ReportIssueService
      ]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(WalkInStayInfoComponent);
      component = fixture.componentInstance;
    });
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
