import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { AppConstants } from '@app/constants/app-constants';
import { SessionStorageService } from 'ihg-ng-common-core';
import { StayInformationModel, StayInfoSearchModel } from '@modules/stay-info/models/stay-information.models';
import { OffersService } from '@app/modules/offers/services/offers.service';
import { WalkInService } from '../../services/walk-in.service';
import { RateCategoryResponseModel } from '../../models/walk-in.models';

@Component({
  selector: 'app-walk-in-stay-info',
  templateUrl: './walk-in-stay-info.component.html',
  styleUrls: ['./walk-in-stay-info.component.scss']
})

export class WalkInStayInfoComponent implements OnInit {
  isInitialized = false;

  public reservationData: StayInformationModel;
  public searchCriteria: StayInfoSearchModel;
  private rootSubscription = new Subscription();

  public showAllOffers = false;

  @Output() originalReservationData = new EventEmitter<StayInformationModel>();

  constructor(
    private offersService: OffersService,
    private storageService: SessionStorageService,
    private walkInService: WalkInService) {
  }

  ngOnInit() {
    this.setReservationDataForRateCategories();
    this.collapsePanel();
  }

  private collapsePanel() {
    this.rootSubscription.add(this.offersService.isShowOffers.subscribe((data) => {
      this.showAllOffers = data;
    }));
  }

  private setReservationData(rateCategoryData?: RateCategoryResponseModel) {
    const localHotelDate = this.storageService.getSessionStorage('localHotelTime') || moment().startOf('day');
    this.reservationData = {
      arrivalDate: moment(localHotelDate).format(AppConstants.DB_DT_FORMAT),
      numberOfRooms: 1,
      checkOutDate: moment(localHotelDate).add(1, 'days').format(AppConstants.DB_DT_FORMAT),
      rateCategories: rateCategoryData ? rateCategoryData.rateCategory.rateCategories : []
    };
  }

  private setReservationDataForRateCategories() {
    this.walkInService.getRateCategory().pipe(finalize(() => {
      this.isInitialized = true
    }))
      .subscribe((response: RateCategoryResponseModel) => {
        this.setReservationData(response);
      },
        (error) => {
          this.setReservationData();
        });
  }

  public handleStayInfoSearch(searchCriteria?: StayInfoSearchModel): void {
    this.searchCriteria = searchCriteria;
    this.offersService.showAllOffers(true);
    this.walkInService.searchCriteria.next(this.searchCriteria);
  }

  handleReservationData(reservationData) {
    this.originalReservationData.next(reservationData);
  }
}
