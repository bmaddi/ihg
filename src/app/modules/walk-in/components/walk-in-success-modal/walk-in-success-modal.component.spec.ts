import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { WalkInSuccessModalComponent } from './walk-in-success-modal.component';
import { GuestListService } from '@modules/guests/services/guest-list.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { MOCK_RESERVATION_DATA_RESPONSE } from '@modules/manage-stay/mocks/manage-stay-mock';

describe('WalkInSuccessModalComponent', () => {
  let component: WalkInSuccessModalComponent;
  let fixture: ComponentFixture<WalkInSuccessModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WalkInSuccessModalComponent],
      imports: [CommonTestModule, HttpClientTestingModule],
      providers: [GuestListService, NgbActiveModal]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalkInSuccessModalComponent);
    component = fixture.componentInstance;
    component.data = MOCK_RESERVATION_DATA_RESPONSE.reservationData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
