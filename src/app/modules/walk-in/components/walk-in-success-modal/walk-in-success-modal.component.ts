import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { GuestListService } from '@modules/guests/services/guest-list.service';
import { ReservationDataModel } from '@modules/offers/models/offers.models';

@Component({
  selector: 'app-walk-in-success-modal',
  templateUrl: './walk-in-success-modal.component.html',
  styleUrls: ['./walk-in-success-modal.component.scss']
})
export class WalkInSuccessModalComponent implements OnInit {

  @Input() data: ReservationDataModel;

  constructor(private activeModal: NgbActiveModal, private router: Router, private guestListService: GuestListService) {
  }

  ngOnInit() {
  }

  public dismissModal(): void {
    this.activeModal.dismiss();
  }

  backToGuest() {
    this.dismissModal();
    this.router.navigate(['guest-list-details']);
  }

  continueCheckIn() {
    this.dismissModal();
    this.guestListService.userDataSource.next(this.data);
    this.router.navigate(['check-in']);
  }
}
