import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { EnvironmentService } from 'ihg-ng-common-core';
import { ReportIssueService } from 'ihg-ng-common-pages';

import { WalkInComponent } from './walk-in.component';
import { WalkInService } from '@modules/walk-in/services/walk-in.service';
import { OffersService } from '@modules/offers/services/offers.service';
import { ScrollUtilService } from '@app-shared/services/scroll-util/scroll-util.service';
import { CommonTestModule } from '@modules/common-test/common-test.module';
import { mockEnvs } from '@app/constants/app-test-constants';

describe('WalkInComponent', () => {
  let component: WalkInComponent;
  let fixture: ComponentFixture<WalkInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, CommonTestModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [WalkInComponent],
      providers: [WalkInService, OffersService, ScrollUtilService, EnvironmentService, ReportIssueService]
    })
      .compileComponents();
    const environmentService = TestBed.get(EnvironmentService);
    environmentService.setEnvironmentConstants(mockEnvs);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalkInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
