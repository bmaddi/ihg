import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { BreadcrumbsService } from 'ihg-ng-common-core';
import { Subscription } from 'rxjs';
import { cloneDeep } from 'lodash';

import { StayInfoSearchModel } from '@app/modules/stay-info/models/stay-information.models';
import { StayInformationModel } from '@app/modules/manage-stay/models/stay-information.models';
import { WalkInService } from '../../services/walk-in.service';
import { WalkInWorkflow } from '../../enums/walk-in.enums';
import { OffersService } from '@app/modules/offers/services/offers.service';
import { GuestWalkInModel } from '@modules/offers/models/offers.models';
import { walkInWorkflowTabs } from '@modules/walk-in/constants/walk-in.constants';
import { WorkflowTab } from '@modules/walk-in/interfaces/workflow-tab.interface';
import { ScrollUtilService } from '@app-shared/services/scroll-util/scroll-util.service';

@Component({
  selector: 'app-walk-in',
  templateUrl: './walk-in.component.html',
  styleUrls: ['./walk-in.component.scss'],
  providers: [OffersService]
})
export class WalkInComponent implements OnInit, OnDestroy {
  @Input() searchCriteria: StayInfoSearchModel;
  @Input() reservationNumber: string;

  searchCriteriaParent: StayInfoSearchModel;
  walkInModelData: GuestWalkInModel;
  showAllOffers = false;
  estimatedTotal: string;
  walkInWorkflow = WalkInWorkflow;
  originalReservationData: StayInformationModel;
  tabs: WorkflowTab[] = cloneDeep(walkInWorkflowTabs);
  selectedTab = this.tabs[0].tabId;
  showPanel = false;

  private rootSubscription = new Subscription();

  constructor(
    private breadcrumbs: BreadcrumbsService,
    private walkInService: WalkInService,
    private offersService: OffersService,
    private scrollUtilService: ScrollUtilService
  ) {
    this.setBreadCrumbLabel();
  }

  ngOnInit() {
    this.searchCriteriaBinding();
    this.displayAllOffers();
    this.getOfferSelectionMade();
  }

  ngOnDestroy() {
    this.rootSubscription.unsubscribe();
  }

  private displayAllOffers() {
    this.rootSubscription.add(this.offersService.isShowOffers.subscribe((showAllOffers) => {
      this.showAllOffers = showAllOffers;
    }));
  }

  private getOfferSelectionMade() {
    this.rootSubscription.add(this.offersService.newReservationData
      .subscribe((selection) => this.handleOfferSelection(selection)));
  }


  private searchCriteriaBinding() {
    this.rootSubscription.add(this.walkInService.searchCriteria.subscribe(
      (searchCriteriaData: StayInfoSearchModel) => {
        this.searchCriteriaParent = searchCriteriaData;
      }));
  }

  public onTabSelect(selectedTab) {
    this.selectedTab = selectedTab;
  }

  private setBreadCrumbLabel() {
    this.breadcrumbs.setLabel('guest-list-details', 'LBL_GST_LST_ARR');
  }

  public handleOriginalReservationData(reservation) {
    this.originalReservationData = reservation;
  }

  public setEstimatedTotal(estTotal: string) {
    this.estimatedTotal = estTotal;
  }

  public handleOfferSelection(selection: GuestWalkInModel) {
    if (selection) {
      this.walkInModelData = selection;
      this.selectedTab = WalkInWorkflow.guestInfo;
      this.showPanel = true;
    }
    this.tabs[0].tabComplete = !!this.walkInModelData;
    if (!!this.walkInModelData) {
      this.scrollUtilService.scrollWindowWithOptions();
    }
  }
}
