import { WalkInWorkflow } from '../enums/walk-in.enums';
import { WorkflowTab } from '../interfaces/workflow-tab.interface';

export const walkInWorkflowTabs: WorkflowTab[] = [
  {
    tabId: WalkInWorkflow.stayInfo,
    tabName: 'LBL_STAY_INFORMATION',
    slmTag: 'StayInfoTab-SID',
    tabComplete: false
  },
  {
    tabId: WalkInWorkflow.guestInfo,
    tabName: 'LBL_GUEST_INFORMATION',
    slmTag: 'GuestInfoTab-SID',
    tabComplete: false
  }
];
