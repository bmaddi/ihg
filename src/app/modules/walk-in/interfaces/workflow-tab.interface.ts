import { WalkInWorkflow } from '@modules/walk-in/enums/walk-in.enums';

export interface WorkflowTab {
  tabId: WalkInWorkflow;
  tabName: string;
  slmTag: string;
  tabComplete: boolean;
}
