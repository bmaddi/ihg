import { ReservationDataModel } from '@modules/offers/models/offers.models';

export interface CreateReservationData extends ReservationDataModel {
  prefixName?: string;
  checkInDate: string;
  checkOutDate: string;
  ihgRcNumber?: string;
  ihgRcMembership?: string;
  ihgRcPoints?: string;
  rateCategoryCode?: string;
  corporateId?: string;
  groupCode?: string;
  hotelCode: string;
  roomTypeCode: string;
  numberOfNights?: number;
  numberOfRooms?: number;
  numberOfAdults: number;
  numberOfChildren?: number;
  smokingPreference?: string;
  accessible?: boolean;
  guestCount?: number;
  firstName: string;
  lastName: string;
  middleName?: string;
  addressLine1?: string;
  addressLine2?: string;
  countryCode?: string | number;
  countryName?: string;
  zipCode?: string | number;
  stateProvince?: string;
  cityName?: string;
  phoneNumber: string;
  phoneType: string;
  email: string;
  cardNumber?: string;
  expirationDate?: string;
  nameOnCard?: string;
  confirmationNumber?: string;
  brandCode?:string;
}

export interface RateCategoryResponseModel {
  rateCategory: RateCategoryListModel;
}

export interface RateCategoryListModel {
  hotelCode: string;
  activationStatus: string;
  rateCategories: string[];
}

