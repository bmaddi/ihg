import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from 'ihg-ng-common-core';

import { WalkInService } from './walk-in.service';

describe('WalkInService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, NgbModule],
    providers: [UserService],
  }));

  it('should be created', () => {
    const service: WalkInService = TestBed.get(WalkInService);
    expect(service).toBeTruthy();
  });
});
