import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from 'ihg-ng-common-core';

import { environment } from '@env/environment';
import { StayInfoSearchModel } from '@app/modules/stay-info/models/stay-information.models';
import { GuestCheckInModel, GuestWalkInModel, ReservationDataModel, RateCategoryModel } from '@modules/offers/models/offers.models';
import { WalkInSuccessModalComponent } from '@modules/walk-in/components/walk-in-success-modal/walk-in-success-modal.component';
import { CreateReservationData, RateCategoryResponseModel } from '@modules/walk-in/models/walk-in.models';

@Injectable({
  providedIn: 'root'
})

export class WalkInService {
  private readonly API_URL = environment.fdkAPI;
  public searchCriteria = new Subject<StayInfoSearchModel>();

  constructor(private http: HttpClient,
    private userService: UserService,
    private modalService: NgbModal) {
  }

  public createReservation(payload: CreateReservationData): Observable<GuestCheckInModel> {
    const location = this.userService.getCurrentLocationId();
    payload.hotelCode = location;
    const apiURl = `${this.API_URL}reservation/walkin/${location}`;
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post(apiURl, JSON.stringify(payload), httpOptions).pipe(map((response: GuestCheckInModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  public openReservationConfirmationModal(data: ReservationDataModel): NgbModalRef {
    const modal = this.modalService
      .open(WalkInSuccessModalComponent, { windowClass: 'description-modal', backdrop: 'static', keyboard: false });
    modal.componentInstance.data = data;
    return modal;
  }

  public getRateCategory(): Observable<RateCategoryResponseModel> {
    const location = this.userService.getCurrentLocationId();
    const apiUrl = `${this.API_URL}lookup/fetchRateCategories/${location}`;
    return this.http.get(apiUrl).pipe(map((response: RateCategoryResponseModel) => {
      return response;
    }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}
