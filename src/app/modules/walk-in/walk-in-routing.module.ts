import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TransitionGuardService } from 'ihg-ng-common-core';

import { WalkInComponent } from '@modules/walk-in/components/walk-in/walk-in.component';

const routes: Routes = [
  {
    path: 'walk-in',
    component: WalkInComponent,
    canDeactivate: [
      TransitionGuardService
    ],
    data: {
      crumb: ['dashboard', 'guest', 'guest-list-details'],
      pageTitle: 'LBL_CREATE_WALK_IN',
      stateName: 'walk-in',
      slnmId: 'WalkIn-SID',
      maxWidth: true,
      noSubtitle: true,
      showHelp: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalkInRoutingModule { }
