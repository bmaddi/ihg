import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

import { CardContainerModule } from 'ihg-ng-common-components';
import { AppSharedModule } from '@app-shared/app-shared.module';
import { PrepareArrivalsDetailsModule } from '@modules/prepare/components/prepare-arrivals-details/prepare-arrivals-details.module';

import { WalkInComponent } from './components/walk-in/walk-in.component';
import { WalkInRoutingModule } from './walk-in-routing.module';
import { CreateWalkInWorkflowComponent } from './components/create-walk-in-workflow/create-walk-in-workflow.component';
import { WalkInStayInfoComponent } from './components/walk-in-stay-info/walk-in-stay-info.component';
import { StayInfoModule } from '../stay-info/stay-info.module';
import { WalkInGuestInfoComponent } from './components/walk-in-guest-info/walk-in-guest-info.component';
import { GuestInfoModule } from '../guest-info/guest-info.module';
import { OffersModule } from '../offers/offers.module';
import { WalkInSuccessModalComponent } from './components/walk-in-success-modal/walk-in-success-modal.component';

@NgModule({
  imports: [
    CommonModule,
    WalkInRoutingModule,
    CardContainerModule,
    NgbTabsetModule,
    TranslateModule.forChild(),
    RouterModule,
    NgbModule,
    AppSharedModule,
    PrepareArrivalsDetailsModule,
    StayInfoModule,
    GuestInfoModule,
    OffersModule
  ],
  declarations: [
    WalkInComponent,
    CreateWalkInWorkflowComponent,
    WalkInStayInfoComponent,
    WalkInGuestInfoComponent,
    WalkInSuccessModalComponent
  ],
  entryComponents: [
    WalkInSuccessModalComponent
  ]
})
export class WalkInModule {
}
