import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { get } from 'lodash';

import { EnvironmentService, MenuService } from 'ihg-ng-common-core';

@Injectable({
  providedIn: 'root'
})
export class DashboardGuardService implements CanActivate {

  constructor(private environmentService: EnvironmentService,
              private menuService: MenuService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const devOnlyRoute = get(route, 'data.devOnlyRoute', true);
    const redirectApp = get(route, 'data.redirectApp', 'hin');

    if (!devOnlyRoute) {
      return true;
    }
    if (!this.isLocalHostEnv()) {
      this.menuService.openInSameWindow(this.environmentService.getAppLinks()[redirectApp]);
      return false;
    }
    return true;
  }

  private isLocalHostEnv(): boolean {
    const currentLocation = window.location.href;
    return ['localhost']
      .some(item => !!currentLocation.match(item));
  }
}
