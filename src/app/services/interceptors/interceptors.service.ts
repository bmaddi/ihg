import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, timeout } from 'rxjs/operators';
import { UserService, SpinnerService, EnvironmentService } from 'ihg-ng-common-core';
import { PromptModalComponent } from '@components/prompt-modal/prompt-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppCommonService } from '@app/modules/shared/services/app-common/app-common.service';
import { COR_ID_ALLOW_LIST, CORE_ID_NOT_ALLOWLIST } from '../guards/constants/cor-id-constant';

@Injectable()
export class InterceptorsService implements HttpInterceptor {
  private APP_LOGOUT_URL: string;

  constructor(
    private userService: UserService,
    private spinnerService: SpinnerService,
    private envService: EnvironmentService,
    private modalService: NgbModal,
    private appCommonService: AppCommonService
  ) {
    this.APP_LOGOUT_URL = this.envService.getAppLogoutUrl();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const started = Date.now();
    const currentlocation = this.userService.getCurrentLocationId();
    const userName = this.userService.getUserName();
    const corId = this.appCommonService.getReservationNumber()
    const headers = {
      Pragma: 'no-cache',
      applCd: 'hin',
      APP_ID: '1'
    };

    // If has currentlocation add location in header
    if (currentlocation) {
      headers['currentLocationId'] = currentlocation;
    }

    // If has user name add username in header
    if (userName) {
      headers['userName'] = userName;
      headers['APP_USER'] = userName;
      headers['MerlinId'] = userName;
    }

    const allowCORID = this.corIdAllowCheck(req.url);
    if(corId && allowCORID){
      headers['CORRELATION_ID'] =corId;
    }

    req = req.clone({
      setHeaders: headers
    });

    if (!this.shouldNotShowSpinner(req)) {
      // Send spinnerConfig as 'N' to avoid interceptor automatically showing and stopping the spinner...See example below//
      // const httpOptions = {
      //   headers: new HttpHeaders({
      //     spinnerConfig: 'N'
      //   })
      // };
      const url = req.url;
      this.spinnerService.show(url);
    }

    return next.handle(req).pipe(timeout(60000)).pipe(tap(event => {
      // Logging the requests
      if (event instanceof HttpResponse) {
        if (!this.shouldNotShowSpinner(req)) {
          const url = req.url;
          this.spinnerService.hide(url);
        }
        const elapsed = Date.now() - started;
        console.log(`Request for ${req.urlWithParams} took ${elapsed} ms.`);
      }
    })).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse || error instanceof Object) {
        if (!this.shouldNotShowSpinner(req)) {
          const url = req.url;
          this.spinnerService.hide(url);
        }

        if (!(error instanceof HttpErrorResponse) && this.checkLoginRequest(error)) {
          return;
        }
      }
      return throwError(error);
    }));
  }

  private shouldNotShowSpinner(req: any): boolean {
    return req.headers.get('spinnerConfig') === 'N';
  }

  private checkLoginRequest(error): Boolean {
    let logout = false;

    if (error.error && /DOCTYPE/.test(error.error.text)) {
      console.log('User relogin required from api response');
      logout = true;
    }

    if (error.headers && /unknown url/.test(error.headers.message)) {
      console.log('User relogin required unknown url');
      logout = true;
    }

    if (logout) {
      this.logoutUser();
      return true;
    }
    return false;
  }

  private logoutUser() {
    const activeModal = this.modalService.open(PromptModalComponent, { backdrop: 'static', windowClass: 'elegant-logout' });
    activeModal.componentInstance.resolve = {
      titleKey: 'LO_TITLE',
      textKey: 'LO_MESSAGE',
      cancelKey: null,
      continueKey: 'LO_CONTINUE'
    };

    activeModal.result
      .then(() => {
        window.location.href = this.APP_LOGOUT_URL;
      }, () => {
        window.location.href = this.APP_LOGOUT_URL;
      });
  }

  private corIdAllowCheck(url: string){
    let urlExists = false;
    COR_ID_ALLOW_LIST.forEach(api => {
      const currentAPIService= this.envService.getEnvironmentConstants()[api];
      if(url.toString().indexOf(currentAPIService) > -1){
         urlExists = true;
      }
    });

    CORE_ID_NOT_ALLOWLIST.forEach(api => {
      if(url.toString().indexOf(api) > -1){
         urlExists = false;
      }
    });
    return urlExists;
  }
}
