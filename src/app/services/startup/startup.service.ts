import { Injectable } from '@angular/core';
import { from } from 'rxjs';

import { EnvironmentService, LoginService } from 'ihg-ng-common-core';
import { environment } from '../../../environments/environment';

@Injectable()
export class StartupService {

    constructor(private environment: EnvironmentService,
                private loginService: LoginService) { }

    // This is the method you want to call at bootstrap
    // Important: It should return a Promise
    load(): Promise<any> {
        return  new Promise((resolve)=> {
            this.environment.setEnvironmentConstants(environment);
            this.loginService.login().subscribe(() => {
                resolve(true);
            });
        });
    }
}
