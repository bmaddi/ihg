import { TestBed } from '@angular/core/testing';
import { TitleCasePipe } from '@angular/common';

import { UtilityService } from './utility.service';

describe('UtilityService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ TitleCasePipe ]
  }));

  it('should be created', () => {
    const service: UtilityService = TestBed.get(UtilityService);
    expect(service).toBeTruthy();
  });
});
