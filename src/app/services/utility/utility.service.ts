import { Injectable, SimpleChanges } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { TitleCasePipe } from '@angular/common';
import * as moment from 'moment';
import { get, isDate } from 'lodash';

import { AppConstants } from '@app/constants/app-constants';

export interface GenericUpdateEvent {
  [key: string]: object | number | string | boolean | Date;
}

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  private genericUpdateSubject = new Subject<GenericUpdateEvent>();

  constructor(private titleCasePipe: TitleCasePipe) {
  }

  public notifyUtilitySubjectUpdate(value: GenericUpdateEvent): void {
    this.genericUpdateSubject.next(value);
  }

  public getUtilitySubjectUpdate(property: string): Observable<GenericUpdateEvent> {
    return this.genericUpdateSubject.asObservable()
      .pipe(
        filter(data => data !== null && get(data, property, null) !== null)
      );
  }

  public truncateWithEllipsis(inputStr: string, maxChar: number): string {
    return inputStr.length > maxChar ? `${inputStr.substr(0, maxChar)}...` : inputStr;
  }

  public convertToTitleCase(value: string): string {
    return this.titleCasePipe.transform(value);
  }

  public concatNameWithPrefix(prefix: string, firstName: string, lastName: string): string {
    if (prefix) {
      const prefixDot = prefix.endsWith('.') ? '' : '.';
      return `${this.convertToTitleCase(prefix)}${prefixDot} ${this.convertToTitleCase(lastName)}`;
    }
    return `${this.convertToTitleCase(firstName)} ${this.convertToTitleCase(lastName)}`;
  }

  public formatFloatValue(value): string {
    return this.isEmpty(value) ? AppConstants.EMDASH : parseFloat(value).toFixed(2);
  }

  public formatToFloatValue(value) {
    return this.isEmpty(value) ? AppConstants.EMDASH : parseFloat(value.toFixed(2));
  }

  public isEmpty(value) {
    return value === undefined || value === '' || value === null || value !== value;
  }

  public isBeforeMinDate(selectedDate: Date, minAllowedDate: Date): boolean {
    return moment(selectedDate).startOf('day').isBefore(moment(minAllowedDate).startOf('day'));
  }

  public isAfterMaxDate(selectedDate: Date, maxAllowedDate: Date): boolean {
    return moment(selectedDate).startOf('day').isAfter(moment(maxAllowedDate).startOf('day'));
  }

  public handleDisabledDates(validDateRange: { min: Date, max: Date }): void {
    setTimeout(() => {
      document.querySelector('.k-widget.k-calendar.k-calendar-infinite .k-calendar-view .k-content.k-scrollable')
        .addEventListener('scroll', () => {
          this.disableDates(validDateRange);
        });
      this.disableDates(validDateRange);
    });
  }

  public disableDates(validDateRange: { min: Date, max: Date }): void {
    const selector = '.k-widget.k-calendar.k-calendar-infinite .k-calendar-view .k-content.k-scrollable tbody tr td';
    Array.from(document.querySelectorAll(selector)).forEach(td => {
      const date = moment(td.getAttribute('title'), 'LLLL').toDate();
      if (this.isDateWithinRange(date, validDateRange)) {
        td.classList.remove('disabled-date');
      } else {
        td.classList.add('disabled-date');
      }
    });
  }

  public isDateWithinRange(date: Date, validDateRange: { min: Date, max: Date }): boolean {
    return moment(date).isBetween(validDateRange.min, validDateRange.max, null, '[]');
  }

  public checkNonWhiteSpaceCharacterWithMinLength(searchText: string, minCharLength: number): boolean {
    return !(searchText && minCharLength !== undefined && searchText.trim().length >= minCharLength);
  }

  public hasSimpleChanges(changes: SimpleChanges): boolean {
    return Object.keys(changes).some(item => changes[item].previousValue !== changes[item].currentValue && !changes[item].firstChange);
  }

  public hasSimpleParamChanges(changes: SimpleChanges, propertyName: string): boolean {
    return changes[propertyName] && !changes[propertyName].firstChange &&
      changes[propertyName].previousValue !== changes[propertyName].currentValue;
  }

  public hasSimpleDateChanges(changes: SimpleChanges, propertyName: string): boolean {
    return changes[propertyName] && isDate(changes[propertyName].currentValue) ?
      this.isNotSameDate(changes[propertyName].previousValue, changes[propertyName].currentValue) && !changes[propertyName].firstChange :
      changes[propertyName] && !changes[propertyName].firstChange;

  }

  public isNotSameDate(previous: Date, current: Date): boolean {
    return !moment(previous).isSame(moment(current), 'date');
  }

  public getGAString(firstStr: string, secondStr: string, symbol: string): string {
    return (firstStr && secondStr && symbol) ? firstStr.concat(symbol).concat(secondStr) : '';
  }
}
