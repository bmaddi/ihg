﻿// Get user language
var lang = navigator.language || navigator.userLanguage;

// Overwrite it with the localStorage value if there is one
if (localStorage.getItem('lang') !== null) {lang = localStorage.getItem('lang');}

// The list of supported languages.
var langs = [ 'en-us', 'ja', 'zh-CN' ];

// Redirect various language/locales to specific supported language
var redirects = {
	//"es-ES": "es",
	//"es-MX": "es"
	};
	var path = location.pathname;
var curLang;

sanitize_lang();
use_lang();

// Ensure that the language matches the supported languages. Default to 'en-us'.
function sanitize_lang() {
	// We work with lower case version of the language code
	if (typeof redirects[lang] != 'undefined') {lang = redirects[lang];}
	// Use en-us if user language matches none of the listed languages
	if (langs.indexOf(lang) == -1) {lang = 'en-us';}
}

// Redirect according to the identified language, if needed
function use_lang() {
	var url = window.location.toString();
	if (!lang_in_path()) {
		// EN stays in main folder
		if (lang != 'en-us') window.location = url.replace('/Content/', '/' + lang + '/Content/');
	} 
	else {
		var filename = path.substring(path.lastIndexOf('/') + 1);
		if (path.indexOf('/' + lang + '/') == -1) {
			if (lang == 'en-us') { window.location = url.replace(curLang + '/Content/', 'Content/'); }
			else { window.location = url.replace(curLang, lang); }
		}
	}
}

function lang_in_path() {
	for (var i = 0; i < langs.length; i++) {
		if (path.indexOf('/' + langs[i] + '/') != -1) {
			curLang = langs[i];
			return true;
		}
	}
	return false;
}

$(function(){
// Set the language selector to the identified value
	$('select#lang').val(lang);
	// Process language change
	$('select#lang').change(function(){
		lang = $(this).val();
		sanitize_lang();
		localStorage.setItem('lang', lang);
		use_lang();
	});
})
