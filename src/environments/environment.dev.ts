export const environment = {
  production: true,
  hinAPI: `${(<any>window).location.origin}/hin/hinservices/v1/`,
  fdkAPI: `${(<any>window).location.origin}/fdk/fdkservices/v1/`,
  apiUrl: `${(<any>window).location.origin}/fdk/concertoservices/v1/`,
  appLogout: `${(<any>window).location.origin}/hin/logout.html?nocache=${new Date().getTime()}`,
  applCd: 'fdk',
  trackingCode: 'UA-32656916-14',
  gMaps: {
    clientId: 'gme-sixch',
    get apiUrl() {
      return `https://maps.googleapis.com/maps/api/js?client=${this.clientId}`;
    },
    directionsUrl: 'https://www.google.com/maps/dir/'
  },
  help: {
    url: (helpId) => 'hin-help/index.htm?cshid=' + helpId,
  },
  heartBeat: 'https://ihg.medallia.com/sso/ihg',
  ihgReporting: {
    baseUrl: 'https://reporting-qa.ihg.com/index.html',
    arrivalsDetailsId: '12420001',
    arrivalsSummaryId: '12670001',
    specialRequestsId: '12670002'
  }
};
(<any>window).env = environment;
