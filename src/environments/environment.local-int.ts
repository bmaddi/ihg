export const environment = {
  production: true,
  rpcpAPI: 'https://concertoint.ihgext.global/rpcp/hinservices/v1/',
  hinAPI: 'https://concertoint.ihgext.global/hin/hinservices/v1/',
  rmhAPI: 'https://concertoint.ihgext.global/rmh/rmhservices/v1/',
  fdkAPI: 'https://concertoint.ihgext.global/fdk/fdkservices/v1/',
  apiUrl: 'https://concertoint.ihgext.global/fdk/concertoservices/v1/',
  appLogout: `https://concertoint.ihgext.global/hin/logout.html?nocache=${new Date().getTime()}`,
  applCd: 'fdk',
  trackingCode: 'UA-32656916-11',
  gMaps: {
    clientId: 'gme-sixch',
    get apiUrl() { return `https://maps.googleapis.com/maps/api/js?client=${this.clientId}`; },
    directionsUrl: 'https://www.google.com/maps/dir/'
  },
  help: {
    url: (helpId) => 'hin-help/index.htm?cshid=' + helpId,
  },
  heartBeat: 'https://ihg.medallia.com/sso/ihg',
  ihgReporting: {
    baseUrl: 'https://reporting-qa.ihg.com/index.html',
    arrivalsDetailsId: '12420001',
    arrivalsSummaryId: '12670001',
    specialRequestsId: '12670002'
  },

  appLinks: {
    'hin': 'https://concertoint.ihgext.global/hin/#/',
    'rms': 'https://concertoint.ihgext.global/rms/#/',
    'rmh': 'https://concertoint.ihgext.global/rmh/#/',
    'gpo': 'https://concertoint.ihgext.global/gpo/#/',
    'rpcp': 'https://concertoint.ihgext.global/rpcp/#/',
    'rms5': 'https://concertoint.ihgext.global/rms/rms5/#/',
    'rmh6': 'https://concertoint.ihgext.global/rmh/rmh6/#/',
    'hcm': 'https://concertoint.ihgext.global/hcm/#/',
    'fdk': 'https://concertoint.ihgext.global/hin/fdk/#/'
  }
};
(<any> window).env = environment;
