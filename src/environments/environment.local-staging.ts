export const environment = {
  production: true,
  rpcpAPI: 'https://concertostaging.ihg.com/rpcp/hinservices/v1/',
  hinAPI: 'https://concertostaging.ihg.com/hin/hinservices/v1/',
  rmhAPI: 'https://concertostaging.ihg.com/rmh/rmhservices/v1/',
  fdkAPI: 'https://concertostaging.ihg.com/fdk/fdkservices/v1/',
  apiUrl: 'https://concertostaging.ihg.com/fdk/concertoservices/v1/',
  appLogout: `https://concertostaging.ihg.com/hin/logout.html?nocache=${new Date().getTime()}`,
  applCd: 'fdk',
  trackingCode: 'UA-32656916-11',
  gMaps: {
    clientId: 'gme-sixch',
    get apiUrl() { return `https://maps.googleapis.com/maps/api/js?client=${this.clientId}`; },
    directionsUrl: 'https://www.google.com/maps/dir/'
  },
  help: {
    url: (helpId) => 'hin-help/index.htm?cshid=' + helpId,
  },
  heartBeat: 'https://ihg.medallia.com/sso/ihg',
  ihgReporting: {
    baseUrl: 'https://reporting-qa.ihg.com/index.html',
    arrivalsDetailsId: '12420001',
    arrivalsSummaryId: '12670001',
    specialRequestsId: '12670002'
  },

  appLinks: {
    'hin': 'https://concertostaging.ihg.com/hin/#/',
    'rms': 'https://concertostaging.ihg.com/rms/#/',
    'rmh': 'https://concertostaging.ihg.com/rmh/#/',
    'gpo': 'https://concertostaging.ihg.com/gpo/#/',
    'rpcp': 'https://concertostaging.ihg.com/rpcp/#/',
    'rms5': 'https://concertostaging.ihg.com/rms/rms5/#/',
    'rmh6': 'https://concertostaging.ihg.com/rmh/rmh6/#/',
    'hcm': 'https://concertostaging.ihg.com/hcm/#/',
    'fdk': 'https://concertostaging.ihg.com/hin/fdk/#/'
  }
};
(<any> window).env = environment;
