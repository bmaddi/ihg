export const environment = {
  production: true,
  hinAPI: `${(<any>window).location.origin}/hin/hinservices/v1/`,
  fdkAPI: `${(<any>window).location.origin}/fdk/fdkservices/v1/`,
  apiUrl: `${(<any>window).location.origin}/fdk/concertoservices/v1/`,
  appLogout: `${(<any>window).location.origin}/hin/logout.html?nocache=${new Date().getTime()}`,
  applCd: 'fdk',
  trackingCode: 'UA-32656916-13',
  gMaps: {
    clientId: 'gme-sixch',
    get apiUrl() {
      return `https://maps.googleapis.com/maps/api/js?client=${this.clientId}`;
    },
    directionsUrl: 'https://www.google.com/maps/dir/'
  },
  help: {
    url: (helpId) => 'hin-help/index.htm?cshid=' + helpId,
  },
  heartBeat: 'https://ihg.medallia.com/sso/ihg',
  ihgReporting: {
    baseUrl: 'https://reporting.ihg.com/index.html',
    arrivalsDetailsId: '5490001',
    arrivalsSummaryId: '5390002',
    specialRequestsId: '5390001'
  }
};
(<any>window).env = environment;
