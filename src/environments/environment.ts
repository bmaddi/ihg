// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export function envApiUrl(): Function {
//  return () => `https://maps.googleapis.com/maps/api/js?client=${this.environment.gMaps.clientId}`;
// }

export const environment = {
  production: false,
  hinAPI: '/hinservices/v1/',
  rcpcAPI: '/rmsservices/v1/',
  rmhAPI: '/rmhservices/v1/',
  fdkAPI: '/fdkservices/v1/',
  apiUrl: '/concertoservices/v1/',
  applCd: 'fdk',
  appLogout: `/`,
  trackingCode: 'UA-32656916-11',
  gMaps: {
    clientId: 'gme-sixch',
    apiUrl: `https://maps.googleapis.com/maps/api/js?client=gme-sixch`,
    directionsUrl: 'https://www.google.com/maps/dir/'
  },
  help: {
    url: (helpId) => 'hin-help/index.htm?cshid=' + helpId,
  },
  heartBeat: 'https://ihg.medallia.com/sso/ihg',
  ihgReporting: {
    baseUrl: 'https://reporting-qa.ihg.com/index.html',
    arrivalsDetailsId: '12420001',
    arrivalsSummaryId: '12670001',
    specialRequestsId: '12670002'
  }
};
(<any>window).env = environment;

