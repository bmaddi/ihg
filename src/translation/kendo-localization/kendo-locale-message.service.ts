import { Injectable } from '@angular/core';
import { MessageService } from '@progress/kendo-angular-l10n';
import { kendoMessageLocale } from './locales/kendo-message-locale';

@Injectable()
export class KendoLocaleMessageService extends MessageService {
  private localeId = 'en';
  
  public set language(value: string) {
    const lang = kendoMessageLocale[value];
    if (lang) {
      this.localeId = value;
      this.notify();
    }
  }

  public get language(): string {
    return this.localeId;
  }
  
  private get messages(): any {
    const lang = kendoMessageLocale[this.localeId];
    if (lang) {
      return lang.messages;
    }
  }

  public get(key: string): string {
    return this.messages[key];
  }
}

