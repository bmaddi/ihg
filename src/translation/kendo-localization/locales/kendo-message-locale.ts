export const kendoMessageLocale = {
	en: {   
    messages: {
      'kendo.datepicker.today': 'Today',
      'kendo.calendar.today': 'Today',
      'kendo.grid.noRecords': 'No records available.',
      'kendo.grid.pagerPage': 'Page',
      'kendo.grid.pagerOf': 'of',
      'kendo.grid.pagerItems': 'items'
    }
  },
  zh: {
    messages: {
      'kendo.datepicker.today': '今天',
      'kendo.calendar.today': '今天',
      'kendo.grid.noRecords': '没有可用的记录。',
      'kendo.grid.pagerPage': '页',
      'kendo.grid.pagerOf': '共',
      'kendo.grid.pagerItems': '每页'
    }
  }
}